<?php
/**
 * DAO -> NogaNotificationDAO
 * Thao tác với bảng ci_noga_notification
 * 
 * @package ConIu
 * @author Coniu
 */

class NogaNotificationDAO {
    /**
     * __construct
     *
     */
    public function __construct() {
    }

    /**
     * Tạo 1 thông báo mới của hệ thống
     *
     * @param $child_id
     */
    public function createNotification(array $args = array()) {
        global $db, $user, $date;

        $args['time'] = toDBDatetime($args['time']);
        $strSql = sprintf("INSERT INTO ci_noga_notification (time, type_user, city_id, district_slug, gender, from_age, to_age, is_repeat, repeat_time, type, title, content, link, description, created_user_id, created_at) 
                           VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
            secure($args['time']), secure($args['type_user'], 'int'), secure($args['city_id'], 'int'), secure($args['district_slug']), secure($args['gender'], 'int'), secure($args['from_age'], 'int'),secure($args['to_age'], 'int'), secure($args['is_repeat'], 'int'), secure($args['repeat_time'], 'int'), secure($args['type'], 'int'), secure($args['title']), secure($args['content']), secure($args['link']), secure($args['description']), secure($user->_data['user_id'], 'int'), secure($date));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Tạo 1 thông báo mới của hệ thống
     *
     * @param $child_id
     */
    public function updateNotification(array $args = array()) {
        global $db, $date;

        $args['time'] = toDBDatetime($args['time']);
        $strSql = sprintf("UPDATE ci_noga_notification SET time = %s, type_user = %s, city_id = %s, district_slug = %s, gender = %s, from_age = %s, to_age = %s, is_repeat = %s, repeat_time = %s, type = %s, title = %s, content = %s, link = %s, description = %s, updated_at = %s WHERE noga_notification_id = %s",
            secure($args['time']), secure($args['type_user'], 'int'), secure($args['city_id'], 'int'), secure($args['district_slug']), secure($args['gender'], 'int'), secure($args['from_age'], 'int'),secure($args['to_age'], 'int'), secure($args['is_repeat'], 'int'), secure($args['repeat_time'], 'int'), secure($args['type'], 'int'), secure($args['title']), secure($args['content']), secure($args['link']), secure($args['description']), secure($date), secure($args['noga_notification_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }


    /**
     * Tạo 1 thông báo mới của hệ thống
     *
     * @param $child_id
     */
    public function deleteNotification($notification_id) {
        global $db;

        $strSql = sprintf("DELETE FROM ci_noga_notification WHERE noga_notification_id = %s", secure($notification_id, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Tạo 1 thông báo mới của hệ thống
     *
     * @param $child_id
     */
    public function getAllNotification() {
        global $db;


        $notifications = array();
        $strSql = sprintf("SELECT * FROM ci_noga_notification ORDER BY status ASC, time ASC");
        $get_notifications = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_notifications->num_rows > 0) {
            while($notification = $get_notifications->fetch_assoc()) {
                $notification['time'] = toSysDatetime($notification['time']);
                $notifications[] = $notification;
            }
        }
        return $notifications;
    }

    /**
     * Lấy ra danh sách thông báo theo thời gian
     *
     * @param $child_id
     */
    public function getUnsentNotifications() {
        global $db;

        $date= date('Y-m-d');
        $hours = date('H');

        $notifications= array();
        $strSql = sprintf("SELECT * FROM ci_noga_notification WHERE status = 0 AND DATE(time) = %s AND HOUR(time) = %s ORDER BY time ASC", secure($date), secure($hours));
        $get_notifications = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_notifications->num_rows > 0) {
            while($notification = $get_notifications->fetch_assoc()) {
                $notifications[] = $notification;
            }
        }
        return $notifications;
    }


    /**
     * Lấy ra danh sách thông báo theo thời gian
     *
     * @param $child_id
     */
    public function getNotificationReceiver(array $args = array()) {
        global $db, $cities;

        $users = array();
        $strSql = "";
        $where_query = "";

        switch ($args['type_user']) {
            case ALL_USER:
                $query = "SELECT user_id, user_name, user_fullname, user_token  FROM users WHERE ";

                if ($args['city_id'] > 0) {
                    $where_query .= " AND city_id = ". secure($args['city_id'], 'int');
                }
                if ($args['gender'] == 1) {
                    $where_query .= " AND user_gender = 'male'" ;
                } elseif ($args['gender'] == 2) {
                    $where_query .= " AND user_gender = 'female'" ;
                }

                if ($args['to_age'] > 0 && $args['to_age'] >= $args['from_age']) {
                    $where_query .= " AND (YEAR(CURRENT_DATE()) - YEAR(user_birthdate) BETWEEN ". $args['from_age'] . " AND " . $args['to_age'] .")";
                }

                $where_query = preg_replace('/AND /', '', $where_query, 1);
                $strSql = $query.$where_query;


                break;

            case SCHOOL_MANAGER:
                $query = "SELECT U.user_id, U.user_name, U.user_fullname, U.user_token FROM ci_user_manage UM INNER JOIN users U 
                          ON UM.object_type = '2' AND UM.user_id = U.user_id ";

                if ($args['gender'] == 1) {
                    $where_query .= " AND U.user_gender = 'male'" ;
                } elseif ($args['gender'] == 2) {
                    $where_query .= " AND U.user_gender = 'female'" ;
                }

                if ($args['to_age'] > 0 && $args['to_age'] >= $args['from_age']) {
                    $where_query .= " AND (YEAR(CURRENT_DATE()) - YEAR(U.user_birthdate) BETWEEN ". $args['from_age'] . " AND " . $args['to_age'] .")";
                }

                if ($args['city_id'] > 0) {
                    foreach ($cities as $city) {
                        if($city['city_id'] == $args['city_id']) {
                            $where_query .= " INNER JOIN pages P ON P.page_admin = UM.user_id INNER JOIN ci_school_configuration SC ON SC.school_id = P.page_id AND SC.city_slug = ". secure($city['city_slug']);
                            break;
                        }
                    }
                }

                $strSql = $query.$where_query;
                //die($strSql);
                break;

            case TEACHER:
                $query = "SELECT U.user_id, U.user_name, U.user_fullname, U.user_token FROM ci_teacher T 
                          INNER JOIN users U ON T.status = '1' AND T.user_id = U.user_id";

                if ($args['city_id'] > 0) {
                    $where_query .= " AND U.city_id = ". secure($args['city_id'], 'int');
                }
                if ($args['gender'] == 1) {
                    $where_query .= " AND U.user_gender = 'male'" ;
                } elseif ($args['gender'] == 2) {
                    $where_query .= " AND U.user_gender = 'female'" ;
                }

                if ($args['to_age'] > 0 && $args['to_age'] >= $args['from_age']) {
                    $where_query .= " AND (YEAR(CURRENT_DATE()) - YEAR(U.user_birthdate) BETWEEN ". $args['from_age'] . " AND " . $args['to_age'] .")";
                }

                $strSql = $query.$where_query;
                break;

            case PARENT:
                $query = "SELECT U.user_id, U.user_name, U.user_fullname, U.user_token FROM ci_parent_manage PM 
                          INNER JOIN users U ON PM.user_id = U.user_id";

                if ($args['city_id'] > 0) {
                    $where_query .= " AND U.city_id = ". secure($args['city_id'], 'int');
                }
                if ($args['gender'] == 1) {
                    $where_query .= " AND U.user_gender = 'male'" ;
                } elseif ($args['gender'] == 2) {
                    $where_query .= " AND U.user_gender = 'female'" ;
                }

                if ($args['to_age'] > 0 && $args['to_age'] >= $args['from_age']) {
                    $where_query .= " AND (YEAR(CURRENT_DATE()) - YEAR(U.user_birthdate) BETWEEN ". $args['from_age'] . " AND " . $args['to_age'] .")";
                }

                $strSql = $query.$where_query;
                break;
        }

        if (!is_empty($strSql)) {
            $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            if($get_users->num_rows > 0) {
                while($user = $get_users->fetch_assoc()) {
                    $users[] = $user;
                }
            }
        }

        return $users;
    }



 /**
     * Lấy ra danh sách thông báo theo thời gian
     *
     * @param $child_id
     */
    public function postNogaNotification(array $args = array()) {
        global $db;


    }


    /**
     * Lấy ra 1 thông báo mới của hệ thống
     *
     * @param $child_id
     */
    public function getNotification($noga_notification_id) {
        global $db;
        $strSql = sprintf("SELECT * FROM ci_noga_notification WHERE noga_notification_id = %s", secure($noga_notification_id, 'int'));
        $get_notification = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_notification->num_rows > 0) {
            $notification = $get_notification->fetch_assoc();
            $notification['time'] = toSysDatetime($notification['time']);
            return $notification;
        }
        return null;
    }

    /**
     * Validate dữ liệu nhập vào
     *
     * @param $args
     * @throws Exception
     */
    private function _validateInput($args) {
        if(is_empty($args['time'])) {
            throw new Exception(__("Time can not be empty"));
        }
    }


}
?>