<?php

/**
 * DAO -> Pickup
 * Thao tác với các bảng service
 *
 * @package ConIu
 * @author Coniu
 */
class PickupDAO
{
    /**
     * __construct
     *
     */
    public function __construct()
    {
    }

    /**
     * Tạo template trông muộn
     *
     * @param array $args
     * @throws Exception
     */
    public function insertPickupTemplate(array $args = array())
    {
        global $db, $date, $user;

        $strSql = sprintf("INSERT INTO ci_pickup_template (school_id, created_at, created_user_id) VALUES (%s, %s, %s)",
            secure($args['school_id'], 'int'),  secure($date), secure($user->_data['user_id'], 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        //$template_id = $db->insert_id;

        if ((count($args['price_list'])) > 0) {

            $strSql = sprintf("DELETE FROM ci_pickup_price_list WHERE school_id = %s", secure($args['school_id'], 'int'));
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);

            // Thêm mới các đơn giá
            $strValues = "";

            for ($idx = 0; $idx < count($args['price_list']); $idx++) {
                $unitPrice = str_replace(',', '', $args['price_list'][$idx]['unit_price']);
                $strValues .= "(" . secure($args['school_id'], 'int') . "," . secure($args['price_list'][$idx]['beginning_time']) . "," . secure($args['price_list'][$idx]['ending_time']) . "," .secure($unitPrice, 'int') . "),";
            }

            $strValues = trim($strValues, ",");
            if (!is_empty($strValues)) {
                $strSql = "INSERT INTO ci_pickup_price_list (school_id, beginning_time, ending_time, unit_price) VALUES " . $strValues;
                $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            }
        }

        if ((count($args['service_name'])) > 0) {
            //Nhập danh sách các khoản phí cấu thành nên học phí vào hệ thống
            $strValues = "";
            //Học phí
            for ($idx = 0; $idx < count($args['service_name']); $idx++) {
                //school_id, service_name, fee, description, created_at, created_user_id
                $servicePrice = str_replace(',', '', $args['service_price'][$idx]);
                $strValues .= "(" . secure($args['school_id'], 'int') . "," . secure($args['service_name'][$idx]) . "," . secure($servicePrice, 'int') . "," . secure($args['service_description'][$idx]) . "," . secure($date) . "," . secure($user->_data['user_id'], 'int') . "),";
            }

            $strValues = trim($strValues, ",");
            if (!is_empty($strValues)) {
                $strSql = "INSERT INTO ci_pickup_service (school_id, service_name, fee, description, created_at, created_user_id) VALUES " . $strValues;
                $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            }
        }

        if ((count($args['class_name'])) > 0) {
            $strValues = "";
            for ($idx = 0; $idx < count($args['class_name']); $idx++) {
                //school_id, class_name, status, description, created_at, created_user_id
                $strValues .= "(" . secure($args['school_id'], 'int') . "," . secure($args['class_name'][$idx]) . "," . secure(1, 'int') . "," . secure($args['class_description'][$idx]) . "," . secure($date) . "," . secure($user->_data['user_id'], 'int') . "),";
            }

            $strValues = trim($strValues, ",");
            if (!is_empty($strValues)) {
                $strSql = "INSERT INTO ci_pickup_class (school_id, class_name, status, description, created_at, created_user_id) VALUES " . $strValues;
                $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            }
        }

        if ((count($args['teacher'])) > 0) {
            $strSql = sprintf("DELETE FROM ci_pickup_teacher WHERE school_id = %s", secure($args['school_id'], 'int'));
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);

            //Nhập danh sách giáo viên vào nhóm giáo viên đón muộn
            $strValues = "";
            //Học phí
            for ($idx = 0; $idx < count($args['teacher']); $idx++) {
                $strValues .= "(" . secure($args['school_id'], 'int') . "," . secure($args['teacher'][$idx], 'int') . "," . secure($args['teacher_fullname_' . $args['teacher'][$idx]]) . "," . secure($args['teacher_name_' . $args['teacher'][$idx]]) . "),";
            }

            $strValues = trim($strValues, ",");
            if (!is_empty($strValues)) {
                $strSql = "INSERT INTO ci_pickup_teacher (school_id, user_id, user_fullname, user_name) VALUES " . $strValues;
                $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            }
        }
    }

    /**
     * Update template trông muộn
     *
     * @param array $args
     * @throws Exception
     */
    public function updatePickupTemplate(array $args = array())
    {
        global $db, $date, $user;
        //$this->_validateServiceInput($args);

        $strSql = sprintf("UPDATE ci_pickup_template SET updated_at = %s WHERE school_id = %s",
            secure($date), secure($args['school_id'], 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        // Thêm mới các đơn giá
        $strSql = sprintf("DELETE FROM ci_pickup_price_list WHERE school_id = %s", secure($args['school_id'], 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $strValues = "";
        if ((count($args['price_list'])) > 0) {
            for ($idx = 0; $idx < count($args['price_list']); $idx++) {
                $unitPrice = str_replace(',', '', $args['price_list'][$idx]['unit_price']);
                $strValues .= "(" . secure($args['school_id'], 'int') . "," . secure($args['price_list'][$idx]['beginning_time']) . "," . secure($args['price_list'][$idx]['ending_time']) . "," .secure($unitPrice, 'int') . "),";
            }

            $strValues = trim($strValues, ",");
            $strSql = "INSERT INTO ci_pickup_price_list (school_id, beginning_time, ending_time, unit_price) VALUES " . $strValues;
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }

        // Update các dịch vụ đã có
        for ($idx = 0; $idx < count($args['service_id']); $idx++) {
            $servicePrice = str_replace(',', '', $args['service_price'][$idx]);

            $strSql = sprintf("UPDATE ci_pickup_service SET service_name = %s, fee = %s, description = %s, updated_at = %s WHERE service_id = %s AND school_id = %s",
                secure($args['service_name'][$idx]), secure($servicePrice, 'int'), secure($args['service_description'][$idx]), secure($date),
                secure($args['service_id'][$idx], 'int'), secure($args['school_id'], 'int'));
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }

        // Thêm mới các dịch vụ
        $strValues = "";
        for ($idx = count($args['service_id']); $idx < count($args['service_name']); $idx++) {
            $servicePrice = str_replace(',', '', $args['service_price'][$idx]);
            //school_id, service_name, fee, description, created_at, created_user_id
            $strValues .= "(" . secure($args['school_id'], 'int') . "," .secure($args['service_name'][$idx]) . "," . secure($servicePrice, 'int') . "," . secure($args['service_description'][$idx]) . "," . secure($date) . "," . secure($user->_data['user_id'], 'int') . "),";
        }
        $strValues = trim($strValues, ",");
        if (!is_empty($strValues)) {
            $strSql = "INSERT INTO ci_pickup_service (school_id, service_name, fee, description, created_at, created_user_id) VALUES " . $strValues;
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }

        //Xóa các dịch vụ
        for ($idx = 0; $idx < count($args['service_delete_id']); $idx++) {
            $strSql = sprintf("UPDATE ci_pickup_service SET is_used = '0', ending_date = %s, updated_at = %s WHERE service_id = %s AND school_id = %s",
                secure($date), secure($date), secure($args['service_delete_id'][$idx], 'int'), secure($args['school_id'], 'int'));
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }


        // Update các lớp đã có
        for ($idx = 0; $idx < count($args['class_id']); $idx++) {
            $strSql = sprintf("UPDATE ci_pickup_class SET class_name = %s, description = %s, updated_at = %s WHERE pickup_class_id = %s AND school_id = %s",
                secure($args['class_name'][$idx]), secure($args['class_description'][$idx]), secure($date),
                secure($args['class_id'][$idx], 'int'), secure($args['school_id'], 'int'));
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
        // Thêm mới các lớp
        $strValues = "";
        for ($idx = count($args['class_id']); $idx < count($args['class_name']); $idx++) {
            //school_id, service_name, fee, description, created_at, created_user_id
            $strValues .= "(" . secure($args['school_id'], 'int') . "," .secure($args['class_name'][$idx]) . "," . secure(1, 'int') . "," . secure($args['class_description'][$idx]) . "," . secure($date) . "," . secure($user->_data['user_id'], 'int') . "),";
        }
        $strValues = trim($strValues, ",");
        if (!is_empty($strValues)) {
            $strSql = "INSERT INTO ci_pickup_class (school_id, class_name, status, description, created_at, created_user_id) VALUES " . $strValues;
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }

        //Xóa các lớp
        for ($idx = 0; $idx < count($args['class_delete_id']); $idx++) {
            $strSql = sprintf("UPDATE ci_pickup_class SET status = '0', updated_at = %s WHERE pickup_class_id = %s AND school_id = %s",
                secure($date), secure($args['class_delete_id'][$idx], 'int'), secure($args['school_id'], 'int'));
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }


        //Nhập danh sách giáo viên vào nhóm giáo viên đón muộn
        $strSql = sprintf("DELETE FROM ci_pickup_teacher WHERE school_id = %s", secure($args['school_id'], 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $strValues = "";
        if ((count($args['teacher'])) > 0) {
            //Học phí
            for ($idx = 0; $idx < count($args['teacher']); $idx++) {
                $strValues .= "(" . secure($args['school_id'], 'int') . "," . secure($args['teacher'][$idx], 'int') . "," . secure($args['teacher_fullname_' . $args['teacher'][$idx]]) . "," . secure($args['teacher_name_' . $args['teacher'][$idx]]) . "),";
            }

            $strValues = trim($strValues, ",");
            $strSql = "INSERT INTO ci_pickup_teacher (school_id, user_id, user_fullname, user_name) VALUES " . $strValues;
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * Lưu danh sách giáo viên vào nhóm giáo viên đón muộn
     *
     * @param array $args
     * @throws Exception
     */
    public function updatePickupTeacherGroup(array $args = array())
    {
        global $db;

        //Nhập danh sách giáo viên vào nhóm giáo viên đón muộn
        $strSql = sprintf("DELETE FROM ci_pickup_teacher WHERE school_id = %s", secure($args['school_id'], 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $strValues = "";
        if ((count($args['teacher'])) > 0) {
            //Học phí
            for ($idx = 0; $idx < count($args['teacher']); $idx++) {
                $strValues .= "(" . secure($args['school_id'], 'int') . "," . secure($args['teacher'][$idx], 'int') . "," . secure($args['teacher_fullname_' . $args['teacher'][$idx]]) . "," . secure($args['teacher_name_' . $args['teacher'][$idx]]) . "),";
            }

            $strValues = trim($strValues, ",");
            $strSql = "INSERT INTO ci_pickup_teacher (school_id, user_id, user_fullname, user_name) VALUES " . $strValues;
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * Tạo template trông muộn
     *
     * @param array $args
     * @throws Exception
     */
    public function assignPickup(array $args = array())
    {
        global $db;

        $strSql = sprintf("INSERT INTO ci_pickup_assign (pickup_id, school_id, user_id, assign_time, pickup_class_id, assign_day) VALUES(%s, %s, %s, %s, %s, %s)",
            secure($args['pickup_id'], 'int'), secure($args['school_id'], 'int'), secure($args['user_id'], 'int'), secure($args['pickup_time']), secure($args['pickup_class_id'], 'int'), secure($args['pickup_day'], 'int'));
        $db->query($strSql) or _error('error', $strSql);
    }


    /**
     * Thêm thông tin lặp lịch
     *
     * @param array $args
     * @throws Exception
     */
    public function updateRepeatInfo(array $args = array())
    {
        global $db;

        $strSql = sprintf('UPDATE ci_pickup_template SET is_repeat = %s, beginning_repeat_date = %s WHERE school_id = %s', secure($args['is_repeat'], 'int'), secure($args['beginning_repeat_date']), secure($args['school_id'], 'int'));
        $db->query($strSql)  or _error(SQL_ERROR_THROWEN);
    }


    /**
     * Lưu thông tin phân công
     *
     * @param array $args
     * @throws Exception
     */
    public function updateAssignType($school_id, $assign_type)
    {
        global $db;

        $strSql = sprintf('UPDATE ci_pickup_template SET assign_type = %s WHERE school_id = %s', secure($assign_type), secure($school_id, 'int'));
        $db->query($strSql)  or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Tạo 1 lần trông muộn
     *
     * @param array $args
     * @throws Exception
     */
    public function insertPickup(array $args = array())
    {
        global $db;

        $strSql = sprintf("INSERT INTO ci_pickup (pickup_class_id, school_id, pickup_time, pickup_day) VALUES (%s, %s, %s, %s)",
            secure($args['pickup_class_id'], 'int'), secure($args['school_id'], 'int'), secure($args['pickup_time']), secure($args['pickup_day'], 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        return $db->insert_id;
    }

    /**
     * Cập nhật số trẻ, tổng tiền của lớp đón muộn
     *
     * @param array $args
     * @throws Exception
     */
    public function updatePickup(array $args = array())
    {
        global $db, $user, $date;

        $args['pickup_time'] = is_empty($args['pickup_time']) ? 'null' : toDBTime($args['pickup_time']);

        //Cập nhật thông tin ci_pickup_child
        $strSql = sprintf('UPDATE ci_pickup_child SET total_amount = %s, late_pickup_fee = %s, using_service_fee = %s, pickup_at = %s, type = %s, status = %s, description = %s WHERE pickup_id = %s AND child_id = %s', secure($args['total_child'], 'int'), secure($args['pickup_fee'], 'int'), secure($args['service_fee'], 'int'), secure($args['pickup_time']), secure($args['type'], 'int'), secure($args['status'], 'int'), secure($args['note']), secure($args['pickup_id'], 'int'), secure($args['child_id'], 'int'));
        $db->query($strSql)  or _error(SQL_ERROR_THROWEN);

        $strSql = sprintf("UPDATE ci_pickup SET total = %s, recorded_at = %s, recorded_user_id = %s WHERE pickup_id = %s", secure($args['total'], 'int'), secure($date), secure($user->_data['user_id'], 'int'), secure($args['pickup_id'], 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }


    /**
     * API
     * Cập nhật số trẻ, tổng tiền của 1 trẻ lớp đón muộn
     *
     * @param array $args
     * @throws Exception
     */
    public function updatePickupChildApi(array $args = array())
    {
        global $db;

        $args['pickup_time'] = is_empty($args['pickup_time']) ? 'null' : toDBTime($args['pickup_time']);

        //Cập nhật thông tin ci_pickup_child
        $strSql = sprintf('UPDATE ci_pickup_child SET total_amount = %s, late_pickup_fee = %s, pickup_at = %s, type = %s, status = %s, description = %s WHERE pickup_id = %s AND child_id = %s', secure($args['total_child'], 'int'), secure($args['pickup_fee'], 'int'), secure($args['pickup_time']), secure($args['type'], 'int'), secure($args['status'], 'int'), secure($args['description']), secure($args['pickup_id'], 'int'), secure($args['child_id'], 'int'));
        $db->query($strSql)  or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Cập nhật tổng tiền của lớp đón muộn
     *
     * @param array $args
     * @throws Exception
     */
    public function updateTotalPickup($pickup_id, $total)
    {
        global $db, $user, $date;

        $strSql = sprintf("UPDATE ci_pickup SET total = %s, recorded_at = %s, recorded_user_id = %s WHERE pickup_id = %s", secure($total, 'int'), secure($date), secure($user->_data['user_id'], 'int'), secure($pickup_id, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Nhập cả danh sách trông muộn vào hệ thống. Hàm này chỉ dành cho GIÁO VIÊN
     *
     * @param array $args
     * @throws Exception
     */
    public function insertPickupChild(array $args = array())
    {
        global $db, $user, $date;

        $strValues = "";
        foreach ($args['add_child'] as $presentChildId) {
            $args['child_id'] = $presentChildId;
            $strValues .= "(" . secure($args['pickup_id'], 'int') . "," . secure($args['pickup_class_id'], 'int') . "," . secure($args['school_id'], 'int') . "," . secure($args['class_id'], 'int') . "," . secure($presentChildId, 'int') . "," . secure($args['type'], 'int') . ",'0'," . secure($args['description_'.$presentChildId]) . "," . secure(convertText4Web($user->_data['user_fullname'])) . "," . secure($date) . "),";
        }
        $strValues = trim($strValues, ",");

        $strSql = sprintf("INSERT INTO ci_pickup_child (pickup_id, pickup_class_id, school_id, class_id, child_id, type, status, description, subscriber, registered_time) VALUES " . $strValues);
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $strSql = sprintf("UPDATE ci_pickup SET total_child = total_child + %s WHERE pickup_id = %s", secure(count($args['add_child']), 'int'), secure($args['pickup_id'], 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

    }

    /**
     * Update ghi chú cho từng trẻ khi đăng ký
     *
     * @param array $args
     * @throws Exception
     */
    /*public function updatePickupChild(array $args = array())
    {
        global $db;
        //Cập nhật thông tin ci_pickup_child
        $strSql = sprintf('UPDATE ci_pickup_child SET type = 2, subscriber = %s WHERE pickup_id = %s AND child_id = %s', secure($args['registered_user_fullname']), secure($args['pickup_id'], 'int'), secure($args['child_id'], 'int'));
        $db->query($strSql)  or _error(SQL_ERROR_THROWEN);
    }*/

    /**
     * Update ghi chú cho từng trẻ khi đăng ký
     *
     * @param array $args
     * @throws Exception
     */
    public function updateNoteOfChildApi(array $args = array())
    {
        global $db;

        if ($args['is_parent_register']) {
            //Cập nhật thông tin ci_pickup_child
            $strSql = sprintf('UPDATE ci_pickup_child SET type = 2, subscriber = %s, description = %s WHERE pickup_id = %s AND child_id = %s', secure($args['registered_user_fullname']), secure($args['description']), secure($args['pickup_id'], 'int'), secure($args['child_id'], 'int'));
            $db->query($strSql)  or _error(SQL_ERROR_THROWEN);
        } else {
            //Cập nhật thông tin ci_pickup_child
            $strSql = sprintf('UPDATE ci_pickup_child SET description = %s WHERE pickup_id = %s AND child_id = %s', secure($args['description']), secure($args['pickup_id'], 'int'), secure($args['child_id'], 'int'));
            $db->query($strSql)  or _error(SQL_ERROR_THROWEN);
        }

    }



    /**
     * Lấy ra template
     *
     * @param $serviceId
     * @param $childIds
     * @return integer
     * @throws Exception
     */
    public function checkPickupConfiguration($schoolId)
    {
        global $db;

        $strSql = sprintf("SELECT school_id FROM ci_pickup_template WHERE school_id = %s ", secure($schoolId, 'int'));

        $get_template = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_template->num_rows > 0) {
            return true;
        }
        return false;
    }


    /**
     * Lấy ra 1 lần đón muộn
     *
     * @param $pickupId
     * @return array
     * @throws Exception
     */
    public function getPickup($pickupId, $getAssign = false)
    {
        global $db;

        $strSql = sprintf("SELECT P.*, PC.class_name FROM ci_pickup P
                  LEFT JOIN ci_pickup_class PC ON P.pickup_class_id = PC.pickup_class_id 
                  WHERE P.pickup_id = %s", secure($pickupId, 'int'));

        $get_pickup = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $pickup = null;
        if ($get_pickup->num_rows > 0) {
            $pickup =  $get_pickup->fetch_assoc();
            if ($getAssign) {
                $pickup['assign'] = $this->getAssign($pickup['pickup_id']);
            }

        }
        return $pickup;
    }

    /**
     * Lấy ra 1 lần đón muộn từ school_id, date
     *
     * @param $serviceId
     * @param $childIds
     * @return array
     * @throws Exception
     */
    public function getPickupAssign($schoolId, $date, $getDetail = false)
    {
        global $db, $user;

        $strSql = sprintf("SELECT P.*, PC.class_name FROM ci_pickup P 
                          INNER JOIN ci_pickup_assign PA ON P.pickup_id = PA.pickup_id AND PA.school_id = %s AND PA.assign_time = %s AND PA.user_id = %s
                          LEFT JOIN ci_pickup_class PC ON PA.pickup_class_id = PC.pickup_class_id",
                          secure($schoolId, 'int'), secure($date), secure($user->_data['user_id'], 'int'));

        $get_pickup = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $pickup = null;
        if ($get_pickup->num_rows > 0) {
            $pickup =  $get_pickup->fetch_assoc();
            if ($getDetail) {
                $pickup['assign'] = $this->getAssign($pickup['pickup_id']);
            }
        }
        return $pickup;
    }

    /**
     * Lấy ra template
     *
     * @param $serviceId
     * @param $childIds
     * @return array
     * @throws Exception
     */
    public function getTemplate($schoolId, $getClasses = false, $getServices = false, $getPriceList = false)
    {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_pickup_template WHERE school_id = %s ", secure($schoolId, 'int'));

        $get_template = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $template = null;
        if ($get_template->num_rows > 0) {
            $template =  $get_template->fetch_assoc();

            if ($getClasses) {
                $template['classes'] = $this->getPickupClasses($schoolId);
            }
            if ($getServices) {
                $template['services'] = $this->getServices($schoolId);
            }
            if ($getPriceList) {
                $template['price_list'] = $this->getPriceList($schoolId);
            }
        }
        return $template;
    }


    /**
     * Lấy ra bảng giá của 1 template
     *
     * @param $serviceId
     * @param $childIds
     * @return array
     * @throws Exception
     */
    public function getPriceList($schoolId)
    {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_pickup_price_list WHERE school_id = %s ORDER BY ending_time ASC", secure($schoolId, 'int'));

        $get_prices = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $prices = null;
        if ($get_prices->num_rows > 0) {
            while ($price = $get_prices->fetch_assoc()) {
                $price['beginning_time'] = date('H:i', strtotime($price['beginning_time']));
                $price['ending_time'] = date('H:i', strtotime($price['ending_time']));
                $prices[] = $price;
            }
        }
        return $prices;
    }

    /**
     * Lấy ra dịch vụ của 1 template
     *
     * @param $serviceId
     * @param $childIds
     * @return array
     * @throws Exception
     */
    public function getServices($schoolId)
    {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_pickup_service WHERE school_id = %s AND is_used = '1'", secure($schoolId, 'int'));

        $get_services = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $services = null;
        if ($get_services->num_rows > 0) {
            while ($service = $get_services->fetch_assoc()) {
                $services[] = $service;
            }
        }
        return $services;
    }


    /**
     * Lấy ra các lớp đón muộn của 1 template
     *
     * @param $serviceId
     * @param $childIds
     * @return array
     * @throws Exception
     */
    public function getPickupClasses($schoolId)
    {
        global $db;

        $strSql = sprintf("SELECT pickup_class_id, school_id, class_name, description, status 
                           FROM ci_pickup_class WHERE school_id = %s AND status ='1'", secure($schoolId, 'int'));

        $get_classes = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $classes = array();
        if ($get_classes->num_rows > 0) {
            while ($class = $get_classes->fetch_assoc()) {
                $classes[] = $class;
            }
        }
        return $classes;
    }

    /**
     * Lấy ra danh sách tất cả giáo viên của một trường và trạng thái chọn vào nhóm giáo viên trông muộn
     *
     * @param int $school_id
     * @return array
     * @throws Exception
     */
    public function getPickupTeachers($school_id) {
        global $db;

        $strSql = sprintf("SELECT U.user_id, U.user_name, U.user_phone, U.created_id, U.user_email, U.user_fullname, U.user_gender, U.user_birthdate, PT.user_id as uid FROM users U 
            INNER JOIN ci_teacher T ON U.user_id = T.user_id
            LEFT JOIN ci_pickup_teacher PT ON T.school_id = PT.school_id AND U.user_id = PT.user_id
            WHERE T.school_id = %s ORDER BY T.status DESC, U.user_firstname ASC", secure($school_id, 'int'));
        $teachers = array();
        $get_teachers = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_teachers->num_rows > 0) {
            while($teacher = $get_teachers->fetch_assoc()) {
                $teacher['selected'] = ($teacher['uid'] > 0) ? 1 : 0;
                unset($teacher['uid']);
                $teachers[] = $teacher;
            }
        }
        return $teachers;
    }

    /**
     * Lấy ra danh sách nhóm giáo viên trông muộn của 1 trường
     *
     * @param int $school_id
     * @return array
     * @throws Exception
     */
    public function getPickupTeachersGroup($school_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_pickup_teacher WHERE school_id = %s ORDER BY user_fullname ASC", secure($school_id, 'int'));
        $teachers = array();
        $get_teachers = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_teachers->num_rows > 0) {
            while($teacher = $get_teachers->fetch_assoc()) {
                $teachers[] = $teacher;
            }
        }
        return $teachers;
    }


    /**
     * Lấy ra thông tin của 1 lớp đón muộn
     *
     * @param $pickupClassId
     * @return array
     * @throws Exception
     */
    public function getPickupClass($pickupClassId)
    {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_pickup_class WHERE pickup_class_id = %s AND status = '1'", secure($pickupClassId, 'int'));

        $get_class = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_class->num_rows > 0) {
            return $get_class->fetch_assoc();
        }
        return null;
    }

    /**
     * Lấy ra ID lần trông muộn
     *
     * @param $serviceId
     * @param $childIds
     * @return integer
     * @throws Exception
     */
    public function getPickupId($schoolId, $date, $classId = 0)
    {
        global $db;

        $strSql = sprintf("SELECT pickup_id FROM ci_pickup WHERE school_id = %s AND pickup_class_id = %s AND pickup_time = %s", secure($schoolId, 'int'), secure($classId, 'int'), secure($date));
        $get_pickupId = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_pickupId->num_rows > 0) {
            return $get_pickupId->fetch_assoc()['pickup_id'];
        }
        return 0;
    }

    /**
     * Kiểm tra xem giáo viên có được phân công không?
     *
     * @param $schoolId
     * @return boolean
     * @throws Exception
     */
    public function checkPickupPermission($schoolId)
    {
        global $db, $user;
        $assignTime = date("Y-m-d");

        $strSql = sprintf("SELECT user_id FROM ci_pickup_assign WHERE user_id = %s AND assign_time = %s
                           AND school_id = %s", secure($user->_data['user_id'], 'int'), secure($assignTime), secure($schoolId, 'int'));

        $get_assign = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_assign->num_rows > 0) {
            return true;
        }
        return false;
    }

    /**
     * Lấy ra thông tin người được phân công theo tuần
     *
     * @param $serviceId
     * @param $childIds
     * @return array
     * @throws Exception
     */
    public function getAssignInWeek($schoolId, $begin)
    {
        global $db;
        $end = date("Y-m-d", strtotime("+6 day", strtotime($begin)));

        $strSql = sprintf("SELECT PA.*, PC.class_name, U.user_fullname, U.user_name, U.user_email, U.user_phone, U.user_gender, U.user_picture FROM ci_pickup_assign PA
                           INNER JOIN users U ON PA.user_id = U.user_id AND PA.school_id = %s                        
                           AND PA.assign_time >= %s AND PA.assign_time <= %s
                           LEFT JOIN ci_pickup_class PC ON PC.pickup_class_id = PA.pickup_class_id ORDER BY PA.pickup_class_id", secure($schoolId, 'int'), secure($begin), secure($end));

        $get_assign = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $assigns['teacher_list'] = null;
        $assigns['teacher_id'] = null;
        if ($get_assign->num_rows > 0) {
            while ($assign = $get_assign->fetch_assoc()) {
                $assign['assign_time'] = toSysDate($assign['assign_time']);
                $assigns['teacher_list'][$assign['assign_day']][$assign['pickup_class_id']][] = $assign;
                $assigns['teacher_id'][$assign['assign_day']][$assign['pickup_class_id']][] = $assign['user_id'];
            }
        }
        return $assigns;
    }

    /**
     * Lấy ra thông tin người được phân công theo tuần
     *
     * @param $serviceId
     * @param $childIds
     * @return array
     * @throws Exception
     */
    public function getAssignInWeekForApi($schoolId, $begin)
    {
        global $db, $user;
        $end = date("Y-m-d", strtotime("+6 day", strtotime($begin)));

        $strSql = sprintf("SELECT PA.*, PC.class_name, U.user_fullname, U.user_name, U.user_email, U.user_phone, U.user_gender, U.user_picture FROM ci_pickup_assign PA
                           INNER JOIN users U ON PA.user_id = U.user_id AND PA.school_id = %s AND PA.user_id = %s                        
                           AND PA.assign_time >= %s AND PA.assign_time <= %s
                           LEFT JOIN ci_pickup_class PC ON PC.pickup_class_id = PA.pickup_class_id ORDER BY PA.pickup_class_id", secure($schoolId, 'int'), secure($user->_data['user_id'], 'int'), secure($begin), secure($end));

        $get_assign = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $assigns = null;
        if ($get_assign->num_rows > 0) {
            while ($assign = $get_assign->fetch_assoc()) {
                $assign['assign_time'] = toSysDate($assign['assign_time']);
                $assigns[$assign['assign_day']][] = $assign;
            }
        }
        return $assigns;
    }

    /**
     * Lấy ra thông tin người được phân công theo tuần
     *
     * @param $serviceId
     * @param $childIds
     * @return array
     * @throws Exception
     */
    public function getAssignInMonthForNotification($schoolId, $begin)
    {
        global $db;
        $end = date('Y-m-t', strtotime($begin));

        $strSql = sprintf("SELECT user_id, assign_time, pickup_class_id FROM ci_pickup_assign 
                           WHERE school_id = %s AND assign_time >= %s AND assign_time <= %s", secure($schoolId, 'int'), secure($begin), secure($end));

        $get_assign = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $result = array();
        if ($get_assign->num_rows > 0) {
            while ($assign = $get_assign->fetch_assoc()) {
                $result[] = array(
                    $assign['user_id'],
                    $assign['assign_time'],
                    $assign['pickup_class_id']
                );
            }
        }
        return $result;
    }

    /**
     * Lấy ra thông tin người được phân công theo tuần
     *
     * @param $serviceId
     * @param $childIds
     * @return array
     * @throws Exception
     */
    public function getAssignInWeekForNotification($schoolId, $begin)
    {
        global $db;
        $end = date("Y-m-d", strtotime("+6 day", strtotime($begin)));

        $strSql = sprintf("SELECT user_id, assign_time, pickup_class_id FROM ci_pickup_assign 
                           WHERE school_id = %s AND assign_time >= %s AND assign_time <= %s ", secure($schoolId, 'int'), secure($begin), secure($end));

        $get_assign = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $result = array();
        if ($get_assign->num_rows > 0) {
            while ($assign = $get_assign->fetch_assoc()) {
                $result[] = array(
                    $assign['user_id'],
                    $assign['assign_time'],
                    $assign['pickup_class_id']
                );
            }
        }
        return $result;
    }


    /**
     * Lấy ra thông tin người được phân công theo tuần
     *
     * @param $serviceId
     * @param $childIds
     * @return array
     * @throws Exception
     */
    public function getAssignInMonthForSchedule($schoolId, $begin)
    {
        global $db;
        $end = date('Y-m-t', strtotime($begin));

        $strSql = sprintf("SELECT * FROM ci_pickup_assign                                               
                           WHERE school_id = %s AND assign_time >= %s AND assign_time <= %s",
            secure($schoolId, 'int'), secure($begin), secure($end));

        $get_assign = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $assigns = array();
        if ($get_assign->num_rows > 0) {
            while ($assign = $get_assign->fetch_assoc()) {
                $assigns[] = $assign;
            }
        }
        return $assigns;
    }

    /**
     * Lấy ra thông tin người được phân công theo tuần
     *
     * @param $serviceId
     * @param $childIds
     * @return array
     * @throws Exception
     */
    public function getAssignInWeekForSchedule($schoolId, $begin)
    {
        global $db;
        $end = date("Y-m-d", strtotime("+6 day", strtotime($begin)));

        $strSql = sprintf("SELECT * FROM ci_pickup_assign                                               
                           WHERE school_id = %s AND assign_time >= %s AND assign_time <= %s",
            secure($schoolId, 'int'), secure($begin), secure($end));

        $get_assign = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $assigns = array();
        if ($get_assign->num_rows > 0) {
            while ($assign = $get_assign->fetch_assoc()) {
                $assigns[] = $assign;
            }
        }
        return $assigns;
    }

    /**
     * Lấy ra lịch sử người được phân công
     *
     * @param $serviceId
     * @param $childIds
     * @return array
     * @throws Exception
     */
    public function getAssignHistory($schoolId, $pickupClassId, $fromDate, $toDate)
    {
        global $db;

        $strSql = sprintf('SELECT U.user_id , U.user_name, U.user_fullname, PA.pickup_id, PA.assign_time, PA.pickup_class_id, PA.school_id, PA.assign_day
                           FROM ci_teacher T INNER JOIN users U ON T.school_id = %1$s AND T.user_id = U.user_id
                           LEFT JOIN ci_pickup_assign PA ON PA.user_id = U.user_id AND PA.school_id = %1$s AND PA.pickup_class_id = %2$s 
                           AND PA.assign_time >= %3$s AND PA.assign_time <= %4$s
                           ORDER BY U.user_firstname ASC, U.user_id ASC, PA.assign_time ASC', secure($schoolId, 'int'), secure($pickupClassId, 'int'), secure($fromDate), secure($toDate));

        $get_assign = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $assigns = array();
        if ($get_assign->num_rows > 0) {
            while ($assign = $get_assign->fetch_assoc()) {
                //$assign['is_checked'] = $assign['pickup_id'] ? 1 : 0;
                $assigns[] = $assign;
            }
        }

        return $assigns;
    }

    /**
     * Lấy ra chi tiết phân công của 1 giáo viên, từ ngày -> ngày
     *
     * @param $serviceId
     * @param $childIds
     * @return array
     * @throws Exception
     */
    public function getAssignDetail($userId, $schoolId, $fromDate, $toDate)
    {
        global $db;

        $strSql = sprintf("SELECT PA.*, U.user_fullname, U.user_name, U.user_email, U.user_phone, U.user_gender, U.user_picture FROM ci_pickup_assign PA
                           INNER JOIN users U ON PA.user_id = U.user_id AND PA.user_id = %s AND PA.school_id = %s 
                           AND PA.assign_time >= %s AND PA.assign_time <= %s", secure($userId, 'int'), secure($schoolId, 'int'), secure($fromDate), secure($toDate));

        $get_assign = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $assigns = array();
        if ($get_assign->num_rows > 0) {
            while ($assign = $get_assign->fetch_assoc()) {
                $assigns[] = $assign;
            }
        }

        return $assigns;
    }

    /**
     * Xóa các ngày trông muộn của toàn trường đã phân công trong tháng
     *
     * @param array $args
     * @throws Exception
     */
    public function deletePickupOfSchoolInMonth($schoolId, $begin)
    {
        global $db;
        $end = date('Y-m-t', strtotime($begin));

        $strSql = sprintf("DELETE FROM ci_pickup_assign WHERE school_id = %s AND assign_time >= %s AND assign_time <= %s", secure($schoolId, 'int'), secure($begin), secure($end));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Xóa các ngày trông muộn đã phân công trong tháng
     *
     * @param array $args
     * @throws Exception
     */
    public function deletePickupAssignInMonth($schoolId, $pickupClassId, $begin)
    {
        global $db;

        $end = date('Y-m-t', strtotime($begin));

        $strSql = sprintf("DELETE FROM ci_pickup_assign WHERE school_id = %s AND pickup_class_id = %s AND assign_time >= %s AND assign_time <= %s", secure($schoolId, 'int'), secure($pickupClassId, 'int'), secure($begin), secure($end));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Xóa các ngày trông muộn đã phân công trong tuần
     *
     * @param array $args
     * @throws Exception
     */
    public function deletePickupAssignInWeek($schoolId, $pickupClassId, $beginDate)
    {
        global $db;

        $endDate = date("Y-m-d", strtotime("+6 day", strtotime($beginDate)));

        $strSql = sprintf("DELETE FROM ci_pickup_assign WHERE school_id = %s AND pickup_class_id = %s AND assign_time >= %s AND assign_time <= %s", secure($schoolId, 'int'), secure($pickupClassId, 'int'), secure($beginDate), secure($endDate));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Xóa các ngày trông muộn đã phân công trong tương lai
     *
     * @param array $args
     * @throws Exception
     */
    public function deletePickupAssign($schoolId, $beginDate)
    {
        global $db;
        $strSql = sprintf("DELETE FROM ci_pickup_assign WHERE school_id = %s AND assign_time >= %s", secure($schoolId, 'int'), secure($beginDate));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy ra danh sách trẻ của lớp (bao gồm cả thông tin có đón muộn hay không). Trường hợp trẻ không tham gia cũng nằm trong danh sách trả về nhưng
     * các thông tin tham gia là rỗng.
     *
     * @param $class_id
     * @param $pickup_id
     * @return array
     * @throws Exception
     */
    function getChildOfClass($class_id, $pickup_id, $pickup_class_id, &$child_count, $dateRegister)
    {
        global $db;

        $results = array();
        $strSql = sprintf('SELECT C.child_id as child, C.child_name, C.birthday, C.gender, C.parent_phone, CC.status AS child_status, PC.*, PCL.class_name FROM ci_class_child CC
                    INNER JOIN ci_child C ON CC.child_id = C.child_id
                    LEFT JOIN ci_pickup_child PC ON PC.pickup_id = %1$s AND PC.pickup_class_id = %2$s AND PC.class_id = %3$s AND C.child_id = PC.child_id
                    LEFT JOIN ci_pickup_class PCL ON PC.pickup_class_id = PCL.pickup_class_id                           
                    WHERE CC.class_id = %3$s AND CC.begin_at <= %4$s AND (CC.end_at IS NULL OR CC.end_at >= %4$s) ORDER BY C.name_for_sort ASC', secure($pickup_id, 'int'), secure($pickup_class_id, 'int'), secure($class_id, 'int'), secure($dateRegister));

        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_children->num_rows > 0) {
            $count = 0;
            while ($child = $get_children->fetch_assoc()) {
                $child['child_id'] = $child['child'];
                unset($child['child']);
                $child['birthday'] = toSysDate($child['birthday']);
                if (is_numeric($child['status'])) $count++;
                //$child['register'] =  $this->getInfoRegister($child['child_id'], $dateRegister);
                $results[] = $child;
            }
            $child_count = $count;
        }
        return $results;
    }


    /**
     * Lấy ra danh sách trẻ của lớp (bao gồm cả thông tin có đón muộn hay không). Trường hợp trẻ không tham gia cũng nằm trong danh sách trả về nhưng
     * các thông tin tham gia là rỗng.
     *
     * @param $class_id
     * @param $pickup_id
     * @return array
     * @throws Exception
     */
    function getChildOfClassApi($class_id, $pickup_id, $pickup_class_id, &$child_count, $dateRegister, $childrenRegistered)
    {
        global $db;

        $results = array();
        $strSql = sprintf('SELECT C.child_id as child, C.child_name, C.birthday, C.gender, C.parent_phone, CC.status AS child_status, PC.*, PCL.class_name FROM ci_class_child CC
                    INNER JOIN ci_child C ON CC.child_id = C.child_id
                    LEFT JOIN ci_pickup_child PC ON PC.pickup_id = %1$s AND PC.pickup_class_id = %2$s AND PC.class_id = %3$s AND C.child_id = PC.child_id
                    LEFT JOIN ci_pickup_class PCL ON PC.pickup_class_id = PCL.pickup_class_id                           
                    WHERE CC.class_id = %3$s AND CC.begin_at <= %4$s AND (CC.end_at IS NULL OR CC.end_at >= %4$s) ORDER BY C.name_for_sort ASC', secure($pickup_id, 'int'), secure($pickup_class_id, 'int'), secure($class_id, 'int'), secure($dateRegister));

        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_children->num_rows > 0) {
            $count = 0;
            while ($child = $get_children->fetch_assoc()) {
                $child['child_id'] = $child['child'];
                unset($child['child']);
                if (array_key_exists($child['child_id'], $childrenRegistered)) {
                    $child['pickup_class_id'] = $childrenRegistered[$child['child_id']]['pickup_class_id'];
                    $child['class_name'] = $childrenRegistered[$child['child_id']]['class_name'];
                }
                $child['birthday'] = toSysDate($child['birthday']);
                if (is_numeric($child['status'])) $count++;
                if (!is_null($dateRegister)) {
                    //$register =  $this->getInfoRegister($child['child_id'], $dateRegister);
                    $child['register'] =  $this->getInfoRegister($child['child_id'], $dateRegister);
                }
                $results[] = $child;
            }
            $child_count = $count;
        }
        return $results;
    }

    /**
     * Lấy ra danh sách trẻ của lớp giáo viên thường(bao gồm cả thông tin có đón muộn hay không). Trường hợp trẻ không tham gia cũng nằm trong danh sách trả về nhưng
     * các thông tin tham gia là rỗng.
     *
     * @param $class_id
     * @param $pickup_id
     * @return array
     * @throws Exception
     */
    function getChildOfClassInDay($class_id, $pickup_ids, $dateRegister, &$child_count)
    {
        global $db;

        $strCon = implode(',', $pickup_ids);

        $results = array();
        $strSql = sprintf('SELECT C.child_id as child, C.child_name, C.birthday, C.gender, C.parent_phone, CC.status AS child_status, PC.*, PCL.class_name FROM ci_class_child CC
                    INNER JOIN ci_child C ON CC.child_id = C.child_id                  
                    LEFT JOIN ci_pickup_child PC ON PC.pickup_id IN (%1$s) AND PC.child_id = C.child_id
                    LEFT JOIN ci_pickup_class PCL ON PC.pickup_class_id = PCL.pickup_class_id                        
                    WHERE CC.class_id = %2$s AND CC.begin_at <= %3$s AND (CC.end_at IS NULL OR CC.end_at >= %3$s) ORDER BY C.name_for_sort ASC', $strCon, secure($class_id, 'int'), secure($dateRegister));

        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_children->num_rows > 0) {
            $count = 0;
            while ($child = $get_children->fetch_assoc()) {
                $child['child_id'] = $child['child'];
                unset($child['child']);
                $child['birthday'] = toSysDate($child['birthday']);
                if (is_numeric($child['status'])) $count++;
                if (!is_null($dateRegister)) {
                    $child['register'] =  $this->getInfoRegister($child['child_id'], $dateRegister);
                }
                $results[] = $child;
            }
            $child_count = $count;
        }
        return $results;
    }

    /**
     * Lấy ra thông tin trẻ có đăng ký đón muộn hay không.
     *
     * @param $class_id
     * @param $pickup_id
     * @return array
     * @throws Exception
     */
    function getInfoRegister($childId, $dateRegister)
    {
        global $db;

        $results = null;
        $strSql = sprintf("SELECT registered_user_id, registered_user_fullname, description FROM ci_pickup_register
                    WHERE child_id = %s AND registered_date = %s", secure($childId, 'int'), secure($dateRegister));

        $get_register = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_register->num_rows > 0) {
            $results = $get_register->fetch_assoc();
            $results['service'] = $this->getServiceRegister4Class($childId, $dateRegister);
        }
        return $results;
    }

    /**
     * Lấy ra thông tin trẻ có đăng ký đón muộn hay không.
     *
     * @param $class_id
     * @param $pickup_id
     * @return array
     * @throws Exception
     */
    function getServiceRegister4Class($childId, $dateRegister)
    {
        global $db;

        $results = array();
        $strSql = sprintf("SELECT SR.* FROM ci_pickup_register PR 
                           INNER JOIN ci_pickup_service_register SR ON SR.pickup_register_id = PR.pickup_register_id
                           AND PR.child_id = %s AND PR.registered_date = %s", secure($childId, 'int'), secure($dateRegister));

        $get_register = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_register->num_rows > 0) {
            //$results = $get_register->fetch_assoc();
            while ($result = $get_register->fetch_assoc()) {
                $results[] = $result;
            }
        }
        return $results;
    }

    /**
     * Lấy ra ID tất cả các lần trông muộn của trường trong ngày
     *
     * @param $serviceId
     * @param $childIds
     * @return
     * @throws Exception
     */
    public function getPickupIds($schoolId, $date)
    {
        global $db;
        $results = null;

        $strSql = sprintf("SELECT pickup_id FROM ci_pickup WHERE school_id = %s AND pickup_time = %s", secure($schoolId, 'int'), secure($date));
        $get_pickup = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_pickup->num_rows > 0) {
            while ($pickup = $get_pickup->fetch_assoc()) {
                $results[] = $pickup['pickup_id'];
            }
        }
        return $results;
    }


    /**
     * Lấy ra danh sách trẻ của lớp (bao gồm cả thông tin có đón muộn hay không). Trường hợp trẻ không tham gia cũng nằm trong danh sách trả về nhưng
     * các thông tin tham gia là rỗng.
     *
     * @param $class_id
     * @param $pickup_id
     * @return array
     * @throws Exception
     */
//    function getChildRegistered($class_id, $pickup_id, $pickup_class_id)
    function getChildRegistered($class_id, $pickup_ids, $pickup_class_id)
    {
        global $db;

        $results = array();
        $pickup_list = implode(',',$pickup_ids);

        $strSql = sprintf("SELECT PC.child_id, PCL.class_name FROM ci_pickup_child PC
                    LEFT JOIN ci_pickup_class PCL ON PC.pickup_class_id = PCL.pickup_class_id
                    WHERE PC.pickup_id IN (%s) AND PC.pickup_class_id <> %s AND PC.class_id = %s",
                    $pickup_list, secure($pickup_class_id, 'int'), secure($class_id, 'int'));

        //_error('error', $strSql);
        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_children->num_rows > 0) {
            while ($child = $get_children->fetch_assoc()) {
                $results[$child['child_id']] = $child['class_name'];
            }
        }
        return $results;
    }

    /**
     * Lấy ra danh sách trẻ của lớp (bao gồm cả thông tin có đón muộn hay không). Trường hợp trẻ không tham gia cũng nằm trong danh sách trả về nhưng
     * các thông tin tham gia là rỗng.
     *
     * @param $class_id
     * @param $pickup_id
     * @return array
     * @throws Exception
     */
//    function getChildRegistered($class_id, $pickup_id, $pickup_class_id)
    function getChildRegisteredApi($class_id, $pickup_ids, $pickup_class_id)
    {
        global $db;

        $results = array();
        $pickup_list = implode(',',$pickup_ids);

        $strSql = sprintf("SELECT PC.child_id, PC.pickup_class_id, PCL.class_name FROM ci_pickup_child PC
                    LEFT JOIN ci_pickup_class PCL ON PC.pickup_class_id = PCL.pickup_class_id
                    WHERE PC.pickup_id IN (%s) AND PC.pickup_class_id <> %s AND PC.class_id = %s",
            $pickup_list, secure($pickup_class_id, 'int'), secure($class_id, 'int'));

        //_error('error', $strSql);
        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_children->num_rows > 0) {
            while ($child = $get_children->fetch_assoc()) {
                $results[$child['child_id']] = array(
                    'pickup_class_id' => $child['pickup_class_id'],
                    'class_name' => $child['class_name']
                );
            }
        }
        return $results;
    }


    /**
     * Lấy ra tiền sử dụng của 1 lớp
     *
     * @param $class_id
     * @param $pickup_id
     * @return array
     * @throws Exception
     */
    function getPickupChildsFee($pickup_id)
    {
        global $db;

        $results = array();
        $strSql = sprintf("SELECT child_id, late_pickup_fee, total_amount FROM ci_pickup_child WHERE pickup_id = %s", secure($pickup_id, 'int'));
        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_children->num_rows > 0) {
            while ($child = $get_children->fetch_assoc()) {
                $results[$child['child_id']]['late_pickup_fee'] = $child['late_pickup_fee'];
                $results[$child['child_id']]['total_amount'] = $child['total_amount'];
            }
        }
        return $results;
    }

    /**
     * Lấy ra thoong tin tiền sử dụng của 1 trẻ
     *
     * @param $class_id
     * @param $pickup_id
     * @return array
     * @throws Exception
     */
    function getPickupChildFee($pickup_id, $child_id)
    {
        global $db;

        $result = null;
        $strSql = sprintf("SELECT child_id, late_pickup_fee, total_amount FROM ci_pickup_child WHERE pickup_id = %s AND child_id = %s", secure($pickup_id, 'int'), secure($child_id, 'int'));
        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_children->num_rows > 0) {
            $result = $get_children->fetch_assoc();
        }
        return $result;
    }


    /**
     * Lấy ra danh sách trẻ của lớp đón muộn
     *
     * @param $class_id
     * @param $pickup_id
     * @return array
     * @throws Exception
     */
    function getChildOfPickup($pickup_id, &$child_count)
    {
        global $db;

        $results = array();
        $strSql = sprintf('SELECT C.child_name, C.birthday, C.gender, C.parent_phone, G.group_title, CC.status as child_status, P.school_id, PC.*, P.pickup_time FROM ci_pickup_child PC
                    INNER JOIN ci_child C ON PC.child_id = C.child_id                  
                    INNER JOIN ci_class_child CC ON CC.child_id = PC.child_id
                    INNER JOIN ci_pickup P ON P.pickup_id = PC.pickup_id AND CC.begin_at <= P.pickup_time AND (CC.end_at IS NULL OR CC.end_at >= P.pickup_time)
                    INNER JOIN groups G ON G.group_id = CC.class_id 
                    WHERE PC.pickup_id = %s ORDER BY C.name_for_sort ASC, PC.class_id ASC', secure($pickup_id, 'int'));
        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_children->num_rows > 0) {
            $count = 0;
            while ($child = $get_children->fetch_assoc()) {
                $child['birthday'] = toSysDate($child['birthday']);
                $child['pickup_at'] = toSysTime($child['pickup_at']);
                $child['registered_time'] = toSysDatetime($child['registered_time']);
                if (($child['status'])) $count++;
                $child['services'] = $this->getServiceUsageOfChild($child['school_id'], $child['pickup_id'], $child['child_id']);
                //$child['register'] =  $this->getInfoRegister($child['child_id'], $child['pickup_time']);

                $results[] = $child;
            }
            $child_count = $count;
        }
        return $results;
    }

    /**
     * Lấy ra danh sách ID trẻ của lớp đón muộn
     *
     * @param $class_id
     * @param $pickup_id
     * @return array
     * @throws Exception
     */
    function getChildIdOfPickup($pickup_id, $class_id = 0)
    {
        global $db;

        $results = array();
        $strSql = sprintf("SELECT child_id FROM ci_pickup_child WHERE pickup_id = %s AND class_id = %s", secure($pickup_id, 'int'), secure($class_id, 'int'));

        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_children->num_rows > 0) {
            while ($child = $get_children->fetch_assoc()) {
                $results[] = $child['child_id'];
            }
        }
        return $results;
    }

    /**
     * Lấy ra thông tin sử dụng dịch vụ của 1 trẻ
     *
     * @param $child_Id
     * @param $pickup_id
     * @return array
     * @throws Exception
     */
    function getServiceUsageOfChild($school_id, $pickup_id, $child_id)
    {
        global $db;

        $results = array();
        $strSql = sprintf("SELECT SU.*, PS.service_id as sv_id, PS.service_name as sv_name, PS.fee FROM ci_pickup_child PC
                    INNER JOIN ci_pickup P ON P.pickup_id = PC.pickup_id
                    INNER JOIN ci_pickup_service PS ON PS.school_id = %s AND P.pickup_time >= PS.beginning_date AND (P.pickup_time <= PS.ending_date OR PS.ending_date IS NULL)
                    LEFT JOIN ci_pickup_service_usage SU ON PC.pickup_id = SU.pickup_id AND PC.child_id = SU.child_id AND PS.service_id = SU.service_id
                    WHERE PC.pickup_id = %s AND PC.child_id = %s ORDER BY PS.service_id ASC", secure($school_id, 'int'), secure($pickup_id, 'int'), secure($child_id, 'int'));
        $get_service = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_service->num_rows > 0) {
            while ($service = $get_service->fetch_assoc()) {
                if (is_null($service['using_at'])) {
                    $service['service_id'] = $service['sv_id'];
                    $service['service_name'] = $service['sv_name'];
                    $service['price'] = $service['fee'];
                }
                unset($service['sv_id']);
                unset($service['sv_name']);
                unset($service['fee']);
                $results[] = $service;
            }
        }

        return $results;
    }

    /**
     * Lưu thông tin sử dụng dịch vụ của trẻ
     *
     * @param $serviceId
     * @param $newUserIds
     * @param $cancelUserIds
     * @param $usingAt
     * @throws Exception
     */
    public function recordPickupServiceOfMan(array $args = array())
    {
        global $db, $date, $user;

        $total_service = 0;
        $strValues = "";
        foreach ($args['recordServiceIds'] as $service_id) {
            // 1.Lấy ra thông tin của dịch vụ
            $service = $this->getService($service_id);
            if (is_null($service)) {
                throw new Exception(__("You must select an existing service"));
            }
            $service_name = convertText4Web($service['service_name']);
            $price = $service['fee'];

            // 2.Kiểm tra dịch vụ này đã được ghi sử dụng chưa, nếu chưa lưu lại
            $service_usage = $this->getServiceUsage($service_id, $args['pickup_id'], $args['child_id']);

            if (is_null($service_usage)) {
                $total_service = $total_service + $price;
                $strValues .= "(" . secure($args['pickup_id'], 'int') . "," . secure($service_id, 'int'). "," . secure($service_name) . "," . secure($price, 'int') . "," . secure($args['child_id'], 'int') . "," . secure($args['using_at']) . "," . secure($user->_data['user_id'], 'int') . "," . secure($date) . "),";
            }
        }

        $strValues = trim($strValues, ",");
        if (is_empty($strValues)) return;

        $strSql = "INSERT INTO ci_pickup_service_usage (pickup_id, service_id, service_name, price, child_id, using_at, recorded_user_id, recorded_at)  VALUES " . $strValues;
        $db->query($strSql)  or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lưu thông tin sử dụng dịch vụ của trẻ
     *
     * @param $serviceId
     * @param $newUserIds
     * @param $cancelUserIds
     * @param $usingAt
     * @throws Exception
     */
    public function recordPickupServiceUsage(array $args = array())
    {
        global $db, $date, $user;

        $total_service = 0;
        $strValues = "";
        foreach ($args['recordServiceIds'] as $service_id) {
            // 1.Lấy ra thông tin của dịch vụ
            $service = $this->getService($service_id);
            if (is_null($service)) {
                throw new Exception(__("You must select an existing service"));
            }
            $service_name = convertText4Web($service['service_name']);
            $price = $service['fee'];

            // 2.Kiểm tra dịch vụ này đã được ghi sử dụng chưa, nếu chưa lưu lại
            $service_usage = $this->getServiceUsage($service_id, $args['pickup_id'], $args['child_id']);

            if (is_null($service_usage)) {
                $total_service = $total_service + $price;
                $strValues .= "(" . secure($args['pickup_id'], 'int') . "," . secure($service_id, 'int'). "," . secure($service_name) . "," . secure($price, 'int') . "," . secure($args['child_id'], 'int') . "," . secure($args['using_at']) . "," . secure($user->_data['user_id'], 'int') . "," . secure($date) . "),";

                //Cập nhật thông tin ci_pickup_child
                $strSql = sprintf('UPDATE ci_pickup_child SET using_service_fee = using_service_fee + %1$s, total_amount = total_amount + %1$s, type = %2$s WHERE pickup_id = %3$s AND child_id = %4$s', secure($price, 'int'), secure($args['type'], 'int'), secure($args['pickup_id'], 'int'), secure($args['child_id'], 'int'));
                $db->query($strSql)  or _error(SQL_ERROR_THROWEN);
            } else {
                //3. Nếu ghi sử dụng rồi thì update
                $servicePrice = $price - $service_usage['price'];
                $total_service = $total_service + $price - $service_usage['price'];
                //$strValues .= "(" . secure($args['pickup_id'], 'int') . "," . secure($service_id, 'int'). "," . secure($service_name) . "," . secure($price, 'int') . "," . secure($args['child_id'], 'int') . "," . secure($args['using_at']) . "," . secure($user->_data['user_id'], 'int') . "," . secure($date) . "),";

                //Cập nhật thông tin ci_pickup_service_usage
                $strSql = sprintf('UPDATE ci_pickup_service_usage SET service_name = %s, price = %s, using_at = %s, recorded_user_id = %s, recorded_at = %s WHERE pickup_id = %s AND child_id = %s AND service_id = %s', secure($service_name), secure($price, 'int'), secure($args['using_at']), secure($user->_data['user_id'], 'int'), secure($date), secure($args['pickup_id'], 'int'), secure($args['child_id'], 'int'), secure($service_id, 'int'));
                $db->query($strSql)  or _error(SQL_ERROR_THROWEN);

                if ($servicePrice >= 0) {
                    //Cập nhật thông tin ci_pickup_child
                    $strSql = sprintf('UPDATE ci_pickup_child SET using_service_fee = using_service_fee + %1$s, total_amount = total_amount + %1$s, type = %2$s WHERE pickup_id = %3$s AND child_id = %4$s', secure($servicePrice, 'int'), secure($args['type'], 'int'), secure($args['pickup_id'], 'int'), secure($args['child_id'], 'int'));
                    $db->query($strSql)  or _error(SQL_ERROR_THROWEN);
                } else {
                    //Cập nhật thông tin ci_pickup_child
                    $strSql = sprintf('UPDATE ci_pickup_child SET using_service_fee = using_service_fee - %1$s, total_amount = total_amount - %1$s, type = %2$s WHERE pickup_id = %3$s AND child_id = %4$s', secure(abs($servicePrice), 'int'), secure($args['type'], 'int'), secure($args['pickup_id'], 'int'), secure($args['child_id'], 'int'));
                    $db->query($strSql)  or _error(SQL_ERROR_THROWEN);
                }

            }
        }

        $strValues = trim($strValues, ",");
        if (!is_empty($strValues)) {
            $strSql = "INSERT INTO ci_pickup_service_usage (pickup_id, service_id, service_name, price, child_id, using_at, recorded_user_id, recorded_at)  VALUES " . $strValues;
//            print_r($strSql); die('qqq');
            $db->query($strSql)  or _error(SQL_ERROR_THROWEN);
        }

        //Cập nhật thông tin ci_pickup
        /*$strSql = sprintf('UPDATE ci_pickup SET total = total + %s, recorded_user_id = %s, recorded_at = %s WHERE pickup_id = %s', secure($total_service, 'int'), secure($user->_data['user_id'], 'int'), secure($date), secure($args['pickup_id'], 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);*/
        return $total_service;

    }

    /**
     * Xóa thông tin sử dụng dịch vụ của trẻ
     *
     * @param $serviceId
     * @param $newUserIds
     * @param $cancelUserIds
     * @param $usingAt
     * @throws Exception
     */
    public function deletePickupServiceOfMan(array $args = array())
    {
        global $db, $date;

        $total_service = 0;
        foreach ($args['deleteServiceIds'] as $service_id) {
            // 1.Lấy ra thông tin của dịch vụ
            $service = $this->getService($service_id);
            if (is_null($service)) {
                throw new Exception(__("You must select an existing service"));
            }

            // 2.Kiểm tra dịch vụ này đã được ghi sử dụng chưa, nếu rồi thì xóa đi
            $service_usage = $this->getServiceUsage($service_id, $args['pickup_id'], $args['child_id']);

            if (!is_null($service_usage)) {
                $strSql = sprintf("DELETE FROM ci_pickup_service_usage WHERE service_id = %s AND pickup_id = %s AND child_id = %s", secure($service_id, 'int'), secure($args['pickup_id'], 'int'), secure($args['child_id'], 'int'));
                $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            }
        }
    }

    /**
     * Xóa thông tin sử dụng dịch vụ của trẻ
     *
     * @param $serviceId
     * @param $newUserIds
     * @param $cancelUserIds
     * @param $usingAt
     * @throws Exception
     */
    public function deletePickupServiceUsage(array $args = array())
    {
        global $db;

        $total_service = 0;
        foreach ($args['deleteServiceIds'] as $service_id) {
            // 1.Lấy ra thông tin của dịch vụ
            $service = $this->getService($service_id);
            if (is_null($service)) {
                throw new Exception(__("You must select an existing service"));
            }

            // 2.Kiểm tra dịch vụ này đã được ghi sử dụng chưa, nếu rồi thì xóa đi
            $service_usage = $this->getServiceUsage($service_id, $args['pickup_id'], $args['child_id']);

            if (!is_null($service_usage)) {
                $total_service = $total_service + $service_usage['price'];

                $strSql = sprintf("DELETE FROM ci_pickup_service_usage WHERE service_id = %s AND pickup_id = %s AND child_id = %s", secure($service_id, 'int'), secure($args['pickup_id'], 'int'), secure($args['child_id'], 'int'));
                $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            }
        }

        if ($total_service > 0) {
            //Cập nhật thông tin ci_pickup_child
            $strSql = sprintf('UPDATE ci_pickup_child SET using_service_fee = using_service_fee - %1$s, total_amount = total_amount - %1$s WHERE pickup_id = %2$s AND child_id = %3$s', secure($total_service, 'int'), secure($args['pickup_id'], 'int'), secure($args['child_id'], 'int'));
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }

        //Cập nhật thông tin ci_pickup
        /*$strSql = sprintf('UPDATE ci_pickup SET total = total - %s, recorded_user_id = %s, recorded_at = %s WHERE pickup_id = %s', secure($total_service, 'int'), secure($user->_data['user_id'], 'int'), secure($date), secure($args['pickup_id'], 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);*/

        return $total_service;
    }


    /**
     * Xóa trẻ khỏi lớp trông muộn
     *
     * @param $serviceId
     * @param $newUserIds
     * @param $cancelUserIds
     * @param $usingAt
     * @throws Exception
     */
    public function deletePickupChild(array $args = array())
    {
        global $db, $date;

        $strCon = implode(',', $args['remove_child']);

        // Xóa thông tin xử dụng dịch vụ nếu đã ghi vào
        $strSql = sprintf("DELETE FROM ci_pickup_service_usage WHERE pickup_id = %s AND child_id IN (%s)", secure($args['pickup_id'], 'int'), $strCon);
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        // Xóa thông tin trẻ khỏi lớp trông muộn
        $strSql = sprintf("DELETE FROM ci_pickup_child WHERE pickup_id = %s AND child_id IN (%s)", secure($args['pickup_id'], 'int'), $strCon);
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        // Xóa thông tin trẻ khỏi lớp trông muộn
        $strSql = sprintf("UPDATE ci_pickup SET total_child = total_child - %s, total = total - %s, recorded_at = %s WHERE pickup_id = %s", secure($args['count_remove'], 'int'), secure($args['deduction'], 'int'), secure($date), secure($args['pickup_id'], 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy ra tổng tiền sử dụng dịch vụ của trẻ bị xóa
     *
     * @param $serviceId
     * @param $newUserIds
     * @param $cancelUserIds
     * @param $usingAt
     * @throws Exception
     */
    public function getTotalAmountPickupChild(array $args = array())
    {
        global $db;
        // Lấy ra tổng số tiền trẻ bị xóa đã sử dụng
        $strCon = implode(',', $args['remove_child']);

        $strSql = sprintf("SELECT SUM(total_amount) AS total FROM ci_pickup_child WHERE pickup_id = %s AND child_id IN (%s)", secure($args['pickup_id'], 'int'), $strCon);
        $get_total = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        return $get_total->fetch_assoc()['total'];
    }

    /**
     * Lấy ra thông tin của 1 dịch vụ
     *
     * @param $serviceId
     * @throws Exception
     */
    public function getService($serviceId)
    {
        global $db;

        if ($serviceId <= 0) {
            throw new Exception(__("You must select an existing service"));
        }
        $service = null;

        //Lấy ra thông tin của dịch vụ
        $strSql = sprintf("SELECT service_id, school_id, service_name, fee, description FROM ci_pickup_service WHERE service_id = %s", secure($serviceId, 'int'));
        $get_service = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_service->num_rows > 0) {
            $service = $get_service->fetch_assoc();
        }
        return $service;
    }

    /**
     * Lấy ra thông tin của 1 dịch vụ
     *
     * @param $serviceId
     * @throws Exception
     */
    public function getServiceUsage($serviceId, $pickupId, $childId)
    {
        global $db;

        $service_usage = null;

        //Lấy ra thông tin sử dụng dịch vụ
        $strSql = sprintf("SELECT * FROM ci_pickup_service_usage WHERE service_id = %s AND pickup_id = %s AND child_id = %s", secure($serviceId, 'int'), secure($pickupId, 'int'), secure($childId, 'int'));
        $get_service_usage = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_service_usage->num_rows > 0) {
            $service_usage = $get_service_usage->fetch_assoc();
        }
        return $service_usage;
    }


    /**
     * Lấy ra mảng các ID của dịch vụ đã sử dụng
     *
     * @param $serviceId
     * @throws Exception
     */
    public function getIdServiceUsage($pickupId, $childId)
    {
        global $db;

        $services = array();

        //Lấy ra thông tin sử dụng dịch vụ
        $strSql = sprintf("SELECT service_id FROM ci_pickup_service_usage WHERE pickup_id = %s AND child_id = %s", secure($pickupId, 'int'), secure($childId, 'int'));
        $get_services = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_services->num_rows > 0) {
            while ($service = $get_services->fetch_assoc()) {
                $services[] = $service['service_id'];
            }
        }
        return $services;
    }

    /**
     * Lưu chi tiết khi trả trẻ
     *
     * @param $serviceId
     * @throws Exception
     */
    public function recordPickupOfChild($args = array())
    {
        global $db, $user, $date;
        //Cập nhật thông tin ci_pickup_child
        $strSql = sprintf("UPDATE ci_pickup_child SET late_pickup_fee = %s, total_amount = %s, pickup_at = %s, description = %s, status = 1, giveback_person = %s WHERE pickup_id = %s AND child_id = %s",
                           secure($args['late_pickup_fee'], 'int'), secure($args['total_child'], 'int'), secure($args['pickup_at']), secure($args['pickup_note']), secure(convertText4Web($user->_data['user_fullname'])), secure($args['pickup_id'], 'int'), secure($args['child_id'], 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        //Cập nhật thông tin ci_pickup
        $strSql = sprintf("UPDATE ci_pickup SET total = %s, recorded_at = %s, recorded_user_id = %s WHERE pickup_id = %s", secure($args['total'], 'int'), secure($date), secure($user->_data['user_id'], 'int'), secure($args['pickup_id'], 'int'));
//        print_r($strSql); die("qqq");
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy ra giá tiền trông muộn theo thời gian trả (Chưa cộng sử sụng dịch vụ)
     *
     * @param $schoolId
     * @param $pickupTime
     * @throws Exception
     */
    public function getPickupFee($schoolId, $pickup_time)
    {
        global $db;
        $pickup_time = toDBTime($pickup_time);

        $strSql = sprintf('SELECT unit_price FROM ci_pickup_price_list 
                           WHERE school_id = %1$s AND beginning_time <= %2$s AND ending_time >= %2$s LIMIT 1', secure($schoolId, 'int'), secure($pickup_time));
        $get_pickup = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        if ($get_pickup->num_rows > 0) {
            return $get_pickup->fetch_assoc()['unit_price'];
        }
        return 0;
    }

    /**
     * Lưu chi tiết khi trả trẻ
     *
     * @param $serviceId
     * @throws Exception
     */
    public function getPickupInMonth($schoolId, $from, $to)
    {
        global $db;
        $time = strtotime(date('Y-m-d'));

        $strSql = sprintf("SELECT P.*, PC.class_name FROM ci_pickup P
                           LEFT JOIN ci_pickup_class PC ON P.pickup_class_id = PC.pickup_class_id
                           WHERE P.school_id = %s AND P.pickup_time >= %s AND P.pickup_time <= %s 
                           ORDER BY P.pickup_time ASC", secure($schoolId, 'int'), secure($from), secure($to));
        $get_pickup = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $pickups = array();

        $arrPickedTime = array();
        $timeFrom = strtotime($from);
        $timeTo = strtotime($to);

        if ($get_pickup->num_rows > 0) {
            while ($pickup = $get_pickup->fetch_assoc()) {
                $pickup['msPickup'] = strtotime($pickup['pickup_time']);
                $arrPickedTime[] = $pickup['msPickup'];
                if ($pickup['msPickup'] <= $time) {
                    $pickup['action'] = 1;
                } else {
                    $pickup['action'] = 0;
                }

                $pickup['pickup_assign'] = $this->getAssign($pickup['pickup_id']);
                $pickup['pickup_time'] = toSysDate($pickup['pickup_time']);
                $pickup['pickup_day'] = $this->getDayString($pickup['pickup_day']);

                $pickups[$pickup['msPickup']][$pickup['pickup_class_id']] = $pickup;
            }
        }

        for ($i = $timeFrom; $i <= $timeTo; $i = strtotime("+1 day", $i)) {
            if (!in_array($i, $arrPickedTime)) {
                $action = $i <= $time ? 2 : 0;
                $pickupTimeDB = date('Y-m-d', $i);
                $pickups[$i][0] = array(
                    'total_child' => 0,
                    'total' => 0,
                    'pickup_time' => toSysDate($pickupTimeDB),
                    'pickup_assign' => [],
                    'action' => $action,
                    'pickup_day' => $this->getDayString($this->getDayOfCurrentDate($pickupTimeDB))
                );
            }
        }
        krsort($pickups);

        return $pickups;
    }

    /**
     * Lấy ra lịch sử phân công của giáo viên trong tháng
     *
     * @param $serviceId
     * @throws Exception
     */
    public function getPickupTeacherInMonth($schoolId, $userId, $from, $to)
    {
        global $db;
        $time = strtotime(date('Y-m-d'));

        $strSql = sprintf("SELECT P.pickup_id, P.school_id, P.total_child, P.pickup_time, P.pickup_day, P.status, P.description, PC.class_name, PA.* FROM ci_pickup P
                           LEFT JOIN ci_pickup_class PC ON P.pickup_class_id = PC.pickup_class_id
                           INNER JOIN ci_pickup_assign PA
                           ON P.pickup_id = PA.pickup_id AND PA.user_id = %s AND P.school_id = %s AND P.pickup_time >= %s AND P.pickup_time <= %s                         
                           ORDER BY P.pickup_time ASC", secure($userId, 'int'),secure($schoolId, 'int'), secure($from), secure($to));
        //print_r($strSql); die;
        $get_pickup = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $pickups = array();

        $arrPickedTime = array();

        if ($get_pickup->num_rows > 0) {
            while ($pickup = $get_pickup->fetch_assoc()) {
                $pickup['msPickup'] = strtotime($pickup['pickup_time']);
                $arrPickedTime[] = $pickup['msPickup'];
                if ($pickup['msPickup'] <= $time) {
                    $pickup['action'] = 1;
                } else {
                    $pickup['action'] = 0;
                }
                unset($pickup['msPickup']);

                $pickup['pickup_assign'] = $this->getAssign($pickup['pickup_id']);
                $pickup['pickup_time'] = toSysDate($pickup['pickup_time']);
                $pickup['pickup_day'] = $this->getDayString($pickup['pickup_day']);

                $pickups[] = $pickup;
            }
        }

        return $pickups;
    }

    /**
     * Lấy ra người được phân công của trường, trong ngày
     *
     * @param $schoolId
     * @param $day
     * @throws Exception
     */
    public function getAssign($pickupId)
    {
        global $db;

        $strSql = sprintf("SELECT PA.*, U.user_fullname, U.user_name, U.user_gender, U.user_picture FROM ci_pickup_assign PA
                           INNER JOIN users U ON PA.user_id = U.user_id AND PA.pickup_id = %s", secure($pickupId, 'int'));

        $get_assign = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $assigns = array();
        if ($get_assign->num_rows > 0) {
            while ($assign = $get_assign->fetch_assoc()) {
                $assigns[] = $assign;
            }
        }
        return $assigns;
    }

    /**
     * Lấy ra thông tin trẻ trong lớp đón muộn
     *
     * @param $schoolId
     * @param $day
     * @throws Exception
     */
    public function getChildRegisteredByParent($childId, $schoolId, $date)
    {
        global $db;

        $strSql = sprintf("SELECT pickup_register_id, child_id, description FROM ci_pickup_register                           
                           WHERE child_id = %s AND school_id = %s AND registered_date = %s", secure($childId, 'int'), secure($schoolId, 'int'), secure($date));

        $get_register = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $register = null;
        if ($get_register->num_rows > 0) {
            $register = $get_register->fetch_assoc();
        }
        return $register;
    }


    /**
     * Lấy ra thông tin trẻ đăng ký đón muộn
     *
     * @param $schoolId
     * @param $day
     * @throws Exception
     */
    public function getServiceRegister($childId, $schoolId, $date)
    {
        global $db;

        $results = array();

        $strSql = sprintf("SELECT PS.*, SR.service_id as sv_id, SR.service_name as sv_name, SR.price as sv_price, SR.registered_at FROM ci_pickup_service PS
                    LEFT JOIN ci_pickup_register PR ON PS.school_id = PR.school_id AND PR.child_id = %s  AND PR.registered_date = %s
                    LEFT JOIN ci_pickup_service_register SR ON SR.pickup_register_id = PR.pickup_register_id AND SR.child_id = PR.child_id AND PS.service_id = SR.service_id
                    WHERE PS.school_id = %s",
                    secure($childId, 'int'), secure($date), secure($schoolId, 'int'));

        $get_service = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_service->num_rows > 0) {
            while ($service = $get_service->fetch_assoc()) {
                if (!is_empty($service['registered_at'])) {
                    $service['service_id'] = $service['sv_id'];
                    if (isset($system['is_mobile']) && $system['is_mobile']) {
                        $service['service_name'] = convertText4Web($service['sv_name']);
                    } else {
                        $service['service_name'] = $service['sv_name'];
                    }

                    $service['fee'] = $service['sv_price'];
                }
                unset($service['sv_id']);
                unset($service['sv_name']);
                unset($service['sv_price']);
                $results[] = $service;
            }
        }
        return $results;
    }


    /**
     * Insert thông tin trẻ đăng ký đón muộn
     *
     * @param $args
     * @throws Exception
     */
    public function insertPickupRegister(array $args = array())
    {
        global $db, $user, $date;

        $strSql = sprintf("INSERT INTO ci_pickup_register (school_id, class_id, child_id, registered_date, registered_user_id, registered_user_fullname, created_at) VALUES (%s, %s, %s, %s, %s, %s, %s)",
            secure($args['school_id'], 'int'), secure($args['class_id'], 'int'), secure($args['child_id'], 'int'), secure($args['registered_date']), secure($user->_data['user_id'], 'int'), secure(convertText4Web($user->_data['user_fullname'])), secure($date));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $pickup_register_id = $db->insert_id;

        $strValues = "";
        foreach ($args['service_ids'] as $serviceId) {
            $service = $this->getService($serviceId);
            if (!is_null($service)) {
                //service_id, child_id, begin, status, created_at, created_user_id
                $strValues .= "(" . secure($pickup_register_id, 'int') . "," . secure($args['school_id'], 'int') . "," . secure($args['child_id'], 'int') . "," . secure($service['service_id'], 'int') . "," . secure($args['registered_date']) . "," . secure(convertText4Web($service['service_name'])) . "," . secure($service['fee'], 'int') . "),";
            }

        }
        $strValues = trim($strValues, ",");

        if (!is_empty($strValues)) {
            $strSql = sprintf("INSERT INTO ci_pickup_service_register (pickup_register_id, school_id, child_id, service_id, registered_at, service_name, price) VALUES " . $strValues);
            $db->query($strSql) or _error(SQL_ERROR_THROWEN) ;
        }

        return $pickup_register_id;

    }

    /**
     * Insert thông tin trẻ đăng ký đón muộn
     *
     * @param $args
     * @throws Exception
     */
    public function updatePickupRegister(array $args = array())
    {
        global $db;

        /*$strSql = sprintf("UPDATE ci_pickup_register SET description = %s, updated_at = %s
                           WHERE pickup_register_id = %s", secure($args['description']), secure($date), secure($args['pickup_register_id'], 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);*/

        $strSql = sprintf("DELETE FROM ci_pickup_service_register WHERE pickup_register_id = %s", secure($args['pickup_register_id'], 'int'));
        /* update class level */
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $strValues = "";
        foreach ($args['service_ids'] as $serviceId) {
            $service = $this->getService($serviceId);
            if (!is_null($service)) {
                //service_id, child_id, begin, status, created_at, created_user_id
                $strValues .= "(" . secure($args['pickup_register_id'], 'int') . "," . secure($args['school_id'], 'int') . "," . secure($args['child_id'], 'int') . "," . secure($service['service_id'], 'int') . "," . secure($args['registered_date']) . "," . secure($service['service_name']) . "," . secure($service['fee'], 'int') . "),";
            }

        }
        $strValues = trim($strValues, ",");
        if (!is_empty($strValues)) {
            $strSql = sprintf("INSERT INTO ci_pickup_service_register (pickup_register_id, school_id, child_id, service_id, registered_at, service_name, price) VALUES " . $strValues);
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }

    }


    /**
     * Lấy ra lịch sử trẻ sử dụng đón muộn
     *
     * @param $schoolId
     * @param $day
     * @throws Exception
     */
    public function getChildPickupHistory($schoolId, $childId, $fromDate, $toDate, &$total)
    {
        global $db;

        $strSql = sprintf("SELECT PC.*, P.pickup_time, P.pickup_day, P.school_id, PCL.class_name FROM ci_pickup_child PC 
                           INNER JOIN ci_pickup P ON PC.pickup_id = P.pickup_id AND PC.school_id = %s AND PC.child_id = %s AND P.pickup_time >= %s AND P.pickup_time <= %s                    
                           LEFT JOIN ci_pickup_class PCL ON PCL.pickup_class_id = PC.pickup_class_id
                           ORDER BY P.pickup_time ASC",
                           secure($schoolId, 'int'), secure($childId, 'int'), secure($fromDate), secure($toDate));

        $get_history = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $results = array();
        $total = 0;
        if ($get_history->num_rows > 0) {
            while ($pickup = $get_history->fetch_assoc()) {
                $total += $pickup['total_amount'];
                $pickup['pickup_at'] = toSysTime($pickup['pickup_at']);
                $pickup['pickup_time'] = toSysDate($pickup['pickup_time']);
                $pickup['pickup_day'] = $this->getDayString($pickup['pickup_day']);
                $pickup['assigns'] = $this->getAssign($pickup['pickup_id']);
                $pickup['services'] = $this->getServiceUsageOfChild($pickup['school_id'], $pickup['pickup_id'], $pickup['child_id']);
                $results[] = $pickup;
            }
        }
        return $results;
    }



    /**
     * Kiểm tra điều kiện đầu vào.
     *
     * @param array $args
     * @throws Exception
     */
    private function _validateServiceInput(array $args = array())
    {
        if (!isset($args['begin_pickup_time'])) {
            throw new Exception(__("You must enter begin pickup time"));
        }
    }


    /**
     * Lấy ra thứ theo ngày
     *
     * @param $date
     * @return integer
     * @throws Exception
     */
    public function getDayOfCurrentDate($date)
    {
        $day = date('w', strtotime($date));
        switch ($day) {
            case '0':
                return 8;
            case '1':
                return 2;
            case '2':
                return 3;
            case '3':
                return 4;
            case '4':
                return 5;
            case '5':
                return 6;
            case '6':
                return 7;
            default:
                return 0;
        }
    }
    
    /**
     * Lấy ra thứ theo ngày
     *
     * @param $date
     * @throws Exception
     */
    public function getDayString($day) {
        switch ($day) {
            case 2:
                return "Monday";
            case 3:
                return "Tuesday";
            case 4:
                return "Wednesday";
            case 5:
                return "Thursday";
            case 6:
                return "Friday";
            case 7:
                return "Saturday";
            case 8:
                return "Sunday";
            default:
                _error(404);
        }
    }

    /**
     * Lấy ra thứ theo ngày
     *
     * @param $date
     * @return integer
     * @throws Exception
     */
    public function getDayShortOfDate($strTodate)
    {
        $day = date('w', $strTodate);
        switch ($day) {
            case '0':
                return 'CN';
            case '1':
                return 'H';
            case '2':
                return 'B';
            case '3':
                return 'T';
            case '4':
                return 'N';
            case '5':
                return 'S';
            case '6':
                return 'B';
            default:
                return 0;
        }
    }

    /* ---------- LẶP LỊCH PHÂN CÔNG ---------- */

    /**
     * Lấy ra những trường lặp lịch phân công
     * return array
     */
    public function getAllschoolHasRepeated() {
        global $db;

        $strSql = sprintf("SELECT T.school_id, T.beginning_repeat_date, P.page_name FROM ci_pickup_template T
               INNER JOIN pages P ON T.school_id = P.page_id AND T.is_repeat = '1'");

        $get_schools = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $schools = array();
        if ($get_schools->num_rows > 0) {
            while ($school = $get_schools->fetch_assoc()) {
                $schools[] = $school;
            }
        }
        return $schools;
    }

    /* ---------- END - LẶP LỊCH PHÂN CÔNG ---------- */


    /* ---------- MEMCACHED ---------- */

    /**
     * Lấy ra template
     *
     * @param $serviceId
     * @param $childIds
     * @return array
     * @throws Exception
     */
    public function getLatePickup4Memcached($schoolId)
    {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_pickup_template WHERE school_id = %s ", secure($schoolId, 'int'));

        $get_template = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $template = array();
        if ($get_template->num_rows > 0) {
            $template =  $get_template->fetch_assoc();
            $template['classes'] = $this->getPickupClasses($schoolId);
            $template['services'] = $this->getServices($schoolId);
            $template['price_list'] = $this->getPriceList($schoolId);
            $template['teachers'] = $this->getTeachersAssigned($schoolId);
        }
        return $template;
    }

    /**
     * Lấy ra id tất cả giáo viên đã được phân công trong trường
     *
     * @param $serviceId
     * @param $childIds
     * @return array
     * @throws Exception
     */
    public function getTeachersAssigned($schoolId)
    {
        global $db;

        $strSql = sprintf("SELECT DISTINCT user_id FROM ci_pickup_assign WHERE school_id = %s", secure($schoolId, 'int'));

        $get_teacher = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $teachers = array();
        if ($get_teacher->num_rows > 0) {
            while ($teacher = $get_teacher->fetch_assoc()) {
                $teachers[] = $teacher['user_id'];
            }
        }
        return $teachers;
    }
    /* ---------- END - MEMCACHED ---------- */

}

?>