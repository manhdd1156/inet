<?php
/**
 * DAO -> BackgroundFirebase
 * Thao tác với bảng ci_background_firebase
 * 
 * @package Coniu
 * @author Coniu
 */

class FirebaseDAO {
    /**
     * __construct
     *
     */
    public function __construct() {
    }

    /**
     * Lây ra danh sách tác vụ với firebase sắp xếp theo Id tăng dần
     *
     * @return array
     */
    public function getActionFirebases() {
        global $db;

        $actionFirebases = array();
        $get_actions = $db->query("SELECT * FROM ci_background_firebase WHERE is_completed = 0 ORDER BY firebase_id ASC") or _error(SQL_ERROR_THROWEN);
        if($get_actions->num_rows > 0) {
            while($actionFirebase = $get_actions->fetch_assoc()) {
                $actionFirebase['user_fullname'] = convertText4Web($actionFirebase['user_fullname']);
                $actionFirebases[] = $actionFirebase;
            }
        }
        return $actionFirebases;
    }

    /**
     * Lấy ra danh sách tác vụ với Firebase đã thực hiện nhưng quá hạn lưu trữ.
     *
     * @param $interval
     * @return array
     * @throws Exception
     */
    public function getExpiredActionFirebaseIds($interval) {
        global $db, $date;

        $strSql = sprintf("SELECT firebase_id FROM ci_background_firebase WHERE is_completed = 1 AND
                        DATE_ADD(completed_at, INTERVAL %s DAY) < %s", secure($interval, 'int'), secure($date));

        $expiredFirebaseIds = array();
        $get_actions = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_actions->num_rows > 0) {
            while($expiredFirebase = $get_actions->fetch_assoc()) {
                $expiredFirebaseIds[] = $expiredFirebase['firebase_id'];
            }
        }
        return $expiredFirebaseIds;
    }

    /* Insert thông tin chạy background Firebase vào DB cua firebase */
    public function insertFirebase(array $args = array()) {
        global $db;
        $args['delete_after_sending'] = DELETE_AFTER_SENDING;

        switch ($args['action']) {
            case CREATE_USER:
                /* Khi tạo tài khoản */
                $strSql = sprintf("INSERT INTO ci_background_firebase (user_id, action, user_name, user_email, user_fullname, user_picture, is_online, count_chat, last_login, provider, delete_after_sending)
                            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                    secure($args['user_id'], 'int'), secure($args['action']), secure($args['user_name']), secure($args['user_email']), secure($args['user_fullname']),
                    secure($args['user_picture']), secure($args['is_online'], 'int'), secure($args['count_chat'], 'int'), secure($args['last_login']), secure($args['provider']), secure($args['delete_after_sending'], 'int'));
                $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                break;

            case DELETE_USER:
                /* Khi cập nhật username */
                $strSql = sprintf("INSERT INTO ci_background_firebase (user_id, action, delete_after_sending)
                            VALUES (%s, %s, %s)",
                    secure($args['user_id'], 'int'), secure($args['action']), secure($args['delete_after_sending'],'int'));
                $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                break;

            case UPDATE_USER:
                $args['user_name'] = (isset($args['user_name'])) ? $args['user_name']: 'null';
                $args['user_email'] = (isset($args['user_email'])) ? $args['user_email']: 'null';
                $args['user_fullname'] = (isset($args['user_fullname'])) ? $args['user_fullname']: 'null';
                $args['user_picture'] = (isset($args['user_picture'])) ? $args['user_picture']: 'null';
                /* Khi cập nhật username */
                $strSql = sprintf("INSERT INTO ci_background_firebase (user_id, action, user_name, user_email, user_fullname, user_picture, delete_after_sending)
                            VALUES (%s, %s, %s, %s, %s, %s, %s)",
                    secure($args['user_id'], 'int'), secure($args['action']), secure($args['user_name']),
                    secure($args['user_email']), secure($args['user_fullname']), secure($args['user_picture']), secure($args['delete_after_sending'], 'int'));
                $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                break;

            case ADD_FRIEND:
            case REMOVE_FRIEND:
                /* Khi trở thành bạn bè */
                $strSql = sprintf("INSERT INTO ci_background_firebase (user_id, action, user_one_id, user_two_id, delete_after_sending)
                            VALUES (%s, %s, %s, %s, %s)",
                    secure($args['user_id'], 'int'), secure($args['action']), secure($args['user_one_id'], 'int'), secure($args['user_two_id'], 'int'), secure($args['delete_after_sending'], 'int'));
                $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                break;
        }

    }

    /**
     * Tăng số lần retry tác vụ lên 1.
     *
     * @param $firebaseId
     * @throws Exception
     */
    public function increaseRetryCountOneAction($firebaseId) {
        global $db;

        $strSql = sprintf("UPDATE ci_background_firebase SET retry_count=retry_count+1 WHERE firebase_id = %s", secure($firebaseId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Tăng số lần thử lại của những tác vụ bị lỗi
     *
     * @param $firebaseIds
     * @throws Exception
     */
    public function increaseRetryCountSomeAction($firebaseIds) {
        global $db;

        if (count($firebaseIds) == 0) return;
        $strCon = implode(',', $firebaseIds);
        $strSql = sprintf("UPDATE ci_background_firebase SET retry_count=retry_count+1 WHERE firebase_id IN (%s)", $strCon);
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Cập nhật trạng thái của tác vụ đã hoàn thành mà ko xóa
     *
     * @param $firebaseIds
     * @throws Exception
     */
    public function setCompletedStatus($firebaseIds) {
        global $db, $date;

        if (count($firebaseIds) == 0) return;
        $strCon = implode(',', $firebaseIds);
        $strSql = sprintf("UPDATE ci_background_firebase SET is_sent = 1, sent_at = %s WHERE firebase_id IN (%s)", secure($date), $strCon);
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Kiểm tra điều kiện đầu vào.
     *
     * @param array $args
     * @throws Exception
     */
    private function _validateInput(array $args = array()) {
        if($args['user_id'] < 0) {
            throw new Exception(__("Incorrect email information"));
        }

        if (is_empty($args['action'])) {
            throw new Exception(__("Action value is missing"));
        }
    }

    /**
     * Xóa 1 action
     *
     * @param $firebaseId
     * @throws Exception
     */
    public function deleteOneAction($firebaseId) {
        global $db;
        $db->query(sprintf("DELETE FROM ci_background_firebase WHERE firebase_id = %s", secure($firebaseId, 'int'))) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Xóa nhiều email cùng một lúc
     *
     * @param $mailIds
     * @throws Exception
     */
    public function deleteSomeActions($firebaseIds) {
        global $db;

        if (count($firebaseIds) == 0) return;

        $strCon = implode(',', $firebaseIds);
        $db->query(sprintf("DELETE FROM ci_background_firebase WHERE firebase_id IN (%s)", $strCon)) or _error(SQL_ERROR_THROWEN);
    }
}
?>