<?php
/**
 * DAO -> Review
 * Thao tác với bảng ci_review
 * 
 * @package Coniu
 * @author Coniu
 */

class ReviewDAO {
    /**
     * __construct
     *
     */
    public function __construct() {
    }

    /**
     * Lấy ra thông tin đánh giá tổng quan ở màn hình quản lý trường
     *
     * @return array
     */
    public function getReviewInfo($school_id) {
        global $db;

        $result = array(
            'school' => array(
                'total_review' => 0,
                'average_review' => 0
            ),
            'teacher' => array(
                'total_review' => 0,
                'average_review' => 0
            )
        );
        $cnt = 0;
        $totalReview4Teacher = 0;
        $averageReview4Teacher = 0;
        $strSql = sprintf("SELECT * FROM ci_review WHERE school_id = %s", secure($school_id, 'int'));
        $get_reviews = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_reviews->num_rows > 0) {
            while($review = $get_reviews->fetch_assoc()) {
                if ($review['type'] == 'school') {
                    $result['school']['total_review'] = $review['total_review'];
                    $result['school']['average_review'] = round($review['average_review'], 1, PHP_ROUND_HALF_UP);
                }
                if ($review['type'] == 'teacher') {
                    $totalReview4Teacher = $totalReview4Teacher + $review['total_review'];
                    $averageReview4Teacher = $averageReview4Teacher + $review['average_review'];
                    $cnt++;
                }
            }
        }

        if ($cnt > 0) {
            $result['teacher']['total_review'] = $totalReview4Teacher;
            $result['teacher']['average_review'] = round($averageReview4Teacher/$cnt, 1, PHP_ROUND_HALF_UP);
        }
        $cntNewReview = $this->getCntNewReview($school_id);
        $result['school']['cnt_new_review'] = $cntNewReview['school'];
        $result['teacher']['cnt_new_review'] = $cntNewReview['teacher'];

        return $result;
    }

    /**
     * Lấy ra số lượng đánh giá mới
     *
     * @return array
     */
    public function getCntNewReview($school_id) {
        global $db;

        $result = array(
            'school' => 0,
            'teacher' => 0
        );

        $strSql = sprintf("SELECT COUNT(user_review_id) as cnt FROM ci_user_review WHERE type = 'school' AND school_id = %s AND created_at >= DATE_SUB(NOW(), INTERVAL 14 DAY)", secure($school_id, 'int'));
        $getSchoolCnt = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($getSchoolCnt->num_rows > 0) {
            $result['school'] = $getSchoolCnt->fetch_assoc()['cnt'];
        }

        $strSql = sprintf("SELECT COUNT(user_review_id) as cnt FROM ci_user_review WHERE type = 'teacher' AND school_id = %s AND created_at >= DATE_SUB(NOW(), INTERVAL 14 DAY)", secure($school_id, 'int'));
        $getTeacherCnt = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($getTeacherCnt->num_rows > 0) {
            $result['teacher'] = $getTeacherCnt->fetch_assoc()['cnt'];
        }
        return $result;
    }

    /**
     * Lấy ra số lượng đánh giá mới
     *
     * @return integer
     */
    public function getCntNewSchoolReview($school_id ) {
        global $db;

        $strSql = sprintf("SELECT COUNT(user_review_id) as cnt FROM ci_user_review WHERE type = 'school' AND school_id = %s  AND created_at >= DATE_SUB(NOW(), INTERVAL 14 DAY)", secure($school_id, 'int'));
        $get_reviews = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_reviews->num_rows > 0) {
            return $get_reviews->fetch_assoc()['cnt'];
        }
        return 0;
    }

    /**
     * Lấy ra số lượng đánh giá mới
     *
     * @return integer
     */
    public function getCntNewTeacherReview($school_id ) {
        global $db;

        $strSql = sprintf("SELECT COUNT(user_review_id) as cnt FROM ci_user_review WHERE type = 'teacher' AND school_id = %s  AND created_at >= DATE_SUB(NOW(), INTERVAL 14 DAY)", secure($school_id, 'int'));
        $get_reviews = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_reviews->num_rows > 0) {
            return $get_reviews->fetch_assoc()['cnt'];
        }
        return 0;
    }

    /**
     * Lấy ra danh sách chi tiết phụ huynh đánh giá trường cho app
     *
     * @return array
     */
    public function getSchoolReview($school_id, $offset = 0) {
        global $system, $db;

        $result = array();
        $offset *= $system['max_results'];

        $strSql = sprintf("SELECT total_review, average_review FROM ci_review WHERE type = 'school' AND school_id = %s", secure($school_id, 'int'));
        $get_reviews = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_reviews->num_rows > 0) {
            $result['review_info'] = $get_reviews->fetch_assoc();
            $result['review_info']['cntNewReview'] = $this->getCntNewSchoolReview($school_id);
        }

        $strSql = sprintf("SELECT R.user_id, R.time, R.school_id, R.rating, R.comment, R.created_at, U.user_id, U.user_name, U.user_fullname FROM ci_user_review R
                  INNER JOIN users U ON U.user_id = R.user_id 
                  WHERE R.type = 'school' AND R.school_id = %s ORDER BY R.created_at DESC LIMIT %s, %s", secure($school_id, 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false));
        $get_reviews = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_reviews->num_rows > 0) {
            while($review = $get_reviews->fetch_assoc()) {
                //$review['time'] = toSysDate($review['time']);
                $review['created_at'] = toSysDate($review['created_at']);
                $result['review_detail'][] = $review;
            }
        }
        return $result;
    }

    /**
     * Lấy ra danh sách chi tiết phụ huynh đánh giá trường cho web
     *
     * @return array
     */
    public function getSchoolReviewForWeb($school_id) {
        global $system, $db;

        $result = array();

        $strSql = sprintf("SELECT total_review, average_review FROM ci_review WHERE type = 'school' AND school_id = %s", secure($school_id, 'int'));
        $get_reviews = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_reviews->num_rows > 0) {
            $result['review_info'] = $get_reviews->fetch_assoc();
            $result['review_info']['cntNewReview'] = $this->getCntNewSchoolReview($school_id);
        }

        $strSql = sprintf("SELECT R.user_id, R.time, R.school_id, R.rating, R.comment, R.created_at, U.user_id, U.user_name, U.user_fullname FROM ci_user_review R
                  INNER JOIN users U ON U.user_id = R.user_id 
                  WHERE R.type = 'school' AND R.school_id = %s ORDER BY R.created_at DESC", secure($school_id, 'int'));
        $get_reviews = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_reviews->num_rows > 0) {
            while($review = $get_reviews->fetch_assoc()) {
                //$review['time'] = toSysDate($review['time']);
                $review['created_at'] = toSysDate($review['created_at']);
                $result['review_detail'][] = $review;
            }
        }
        return $result;
    }


    /**
     * Lấy ra danh sách chi tiết phụ huynh đánh giá trường
     *
     * @return array
     */
    public function getTeacherReview($class_id, $teacher_id) {
        global $db;


        $cnt = 0; $totalReview = 0;
        $strSql = sprintf("SELECT rating FROM ci_user_review
                  WHERE type = 'teacher' AND class_id = %s AND teacher_id = %s", secure($class_id, 'int'), secure($teacher_id, 'int'));
        $get_reviews = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_reviews->num_rows > 0) {
            while($review = $get_reviews->fetch_assoc()) {
                $totalReview = $totalReview + $review['rating'];
                $cnt++;
            }
        }
        $averageReview = $cnt > 0 ? round(($totalReview/$cnt), 1) : 0;
        $result = array(
            'total_review' => $cnt,
            'average_review' => $averageReview
        );
        return $result;
    }


    /**
     * Lấy ra danh sách chi tiết phụ huynh đánh giá giáo viên
     *
     * @return array
     */
    public function getTeacherReviewInSchool($school_id) {
        global $db;

        $result = array();
        $strSql = sprintf("SELECT * FROM ci_review WHERE type = 'teacher' AND school_id = %s", secure($school_id, 'int'));
        $get_reviews = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_reviews->num_rows > 0) {
            $cnt = 0; $totalReview = 0; $averageReview = 0;
            while($review = $get_reviews->fetch_assoc()) {
                $totalReview = $totalReview + $review['total_review'];
                $averageReview = $averageReview + $review['average_review'];
                $cnt++;
            }
            $result['review_info']['total_review'] = $totalReview;
            $result['review_info']['average_review'] = $cnt > 0 ? round(($averageReview/$cnt), 1) : 0;
            $result['review_info']['cntNewReview'] = $this->getCntNewTeacherReview($school_id);
        }

        $teachersArr = array();
        $strSql = sprintf("SELECT C.group_id, C.group_title FROM groups C INNER JOIN ci_class_level CL ON C.class_level_id = CL.class_level_id
                    WHERE CL.school_id = %s ORDER BY C.class_level_id, C.group_title ASC", secure($school_id, 'int'));

        $get_classes = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_classes->num_rows > 0) {
            while($class = $get_classes->fetch_assoc()) {
                $teachers = $this->getTeachers($class['group_id']);
                foreach ($teachers as $teacher) {
                    $teacher['group_id'] = $class['group_id'];
                    $teacher['group_title'] = $class['group_title'];
                    $reviewInfo = $this->getTeacherReview($teacher['group_id'], $teacher['user_id']);
                    $teacher['total_review'] = $reviewInfo['total_review'];
                    $teacher['average_review'] = $reviewInfo['average_review'];
                    $teachersArr[] = $teacher;
                }
            }
        }
        $result['review_detail'] = $teachersArr;

        return $result;
    }


    /**
     * Lấy ra chi tiết phụ huynh đánh giá của 1 giáo viên
     *
     * @return array
     */
    public function getTeacherReviewDetail($teacher_id, $class_id) {
        global $db;

        $result = array();
        $strSql = sprintf("SELECT R.user_id, R.time, R.school_id, R.rating, R.comment, R.created_at, U.user_id, U.user_fullname FROM ci_user_review R 
            INNER JOIN users U ON R.user_id = U.user_id 
            WHERE type = 'teacher' AND teacher_id = %s AND class_id = %s", secure($teacher_id, 'int'), secure($class_id, 'int'));
        $get_reviews = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_reviews->num_rows > 0) {
            while($review = $get_reviews->fetch_assoc()) {
                //$review['time'] = toSysDate($review['time']);
                $review['created_at'] = toSysDate($review['created_at']);
                $result[] = $review;
            }
        }

        return $result;
    }


    /**
     * Lấy ra danh sách giáo viên của một lớp.
     *
     * @param $class_id
     * @return array
     * @throws Exception
     */
    private function getTeachers($class_id) {
        global $db;

        $strSql = sprintf("SELECT T.user_id, T.user_name, T.user_fullname, T.user_gender FROM users T INNER JOIN ci_user_manage UM ON T.user_id = UM.user_id
                    WHERE UM.object_type = %s AND UM.role_id = %s AND UM.object_id = %s ORDER BY T.user_fullname ASC", MANAGE_CLASS, PERMISSION_MANAGE, secure($class_id, 'int'));

        $teachers = array();
        $get_teachers = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_teachers->num_rows > 0) {
            while($teacher = $get_teachers->fetch_assoc()) {
                $teachers[] = $teacher;
            }
        }
        return $teachers;
    }


    /**
     * Lấy ra danh sách phụ huynh và thời gian mới nhất đã đánh giá (theo tháng)
     *
     * @return array
     * @throws Exception
     */
    public function getParentHasReviewed() {
        global $db;

        $strSql = sprintf("SELECT DISTINCT R1.user_id, R1.time FROM ci_user_review R1 WHERE R1.time = (SELECT MAX(R2.time) FROM ci_user_review R2 WHERE R2.user_id = R1.user_id) GROUP BY R1.user_id, R1.time");

        $parents = null;
        $get_parents = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_parents->num_rows > 0) {
            while($parent = $get_parents->fetch_assoc()) {
                $parents[$parent['user_id']] = $parent['time'];
            }
        }
        return $parents;
    }

}
?>