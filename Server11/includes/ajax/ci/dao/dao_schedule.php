<?php
/**
 * DAO -> Services
 * Thao tác với các bảng ci_schedule
 *
 * @package ConIu
 * @author TaiLa
 */

class ScheduleDAO
{
    /**
     * __construct
     *
     */
    public function __construct()
    {
    }
    //////////////////////////////// SCHEDULE ////////////////////////
    public function insertCate($args) {
        global $db, $date, $user;
        $this->_validateCateInput($args);

        $strSql = sprintf("INSERT INTO ci_category (category_name, type, school_id, class_level_id, class_id, status, description, created_at, created_user_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)", secure($args['category_name']), secure($args['type'], 'int'), secure($args['school_id'], 'int'), secure($args['class_level_id'], 'int'), secure($args['class_id'], 'int'), secure($args['status']), secure($args['description_cate']), secure($date), secure($user->_data['user_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $cate_id = $db->insert_id;
        return $cate_id;
    }

    /*
     * Sửa catalogy lịch học
     */
    public function editCate($cateId, array $args = array()) {
        global $db, $date;
        $this->_validateCateInput($args);

        $strSql = sprintf("UPDATE ci_category SET category_name = %s, type = %s, school_id = %s, class_level_id = %s, class_id = %s, status = %s, description = %s, updated_at = %s WHERE category_id = %s", secure($args['category_name']), secure($args['type'], 'int'), secure($args['school_id'], 'int'), secure($args['class_level_id'], 'int'), secure($args['class_id'], 'int'), secure($args['status']), secure($args['description_cate']), secure($date), secure($cateId, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $cate_id = $db->insert_id;
        return $cate_id;
    }

    /*
    * Kiểm tra điều kiện đầu vào khi thêm chương trình học
    */
    private function _validateCateInput(array $args = array()) {
        if (!isset($args['category_name'])) {
            throw new Exception(__("You must enter category name"));
        }

        if (strlen($args['category_name']) > 100) {
            throw new Exception(__("Fee name must be less than 100 characters long"));
        }

        if (isset($args['description_cate']) && (strlen($args['description_cate'])) > 300) {
            throw new Exception(__("Description length must be less than 300 characters long"));
        }
    }

    /*
    * Lấy danh sách chương trình học của trường
    */
    public function getCateOfSchool($school_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_category WHERE school_id = %s ORDER BY created_at DESC", secure ($school_id, 'int'));
        $get_category = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $categorys = array();
        if($get_category->num_rows > 0) {
            while ($category = $get_category->fetch_assoc()) {
                $categorys[] = $category;
            }
        }
        return $categorys;
    }

    /*
     * Lấy catalogy theo category_id
     */
    public function getCategory($cate_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_category WHERE category_id = %s", secure($cate_id));

        $get_category = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $category = array();
        if ($get_category->num_rows > 0) {
            if ($get_category->num_rows > 1) {
                throw new Exception(__("Existing two schedule for this object"));
            }
            $cate = $get_category->fetch_assoc();
            $subjects = $this->_getSubjectByCateId($cate['category_id']);
            $cate['subjects'] = $subjects;
            $category = $cate;
        }
        return $category;
    }
    /*
     * Lấy danh sách catelogy áp dụng cho trường
     */
    public function getScheduleOfSchool($school_id) {
        global $db;

        // Lấy danh sách lịch học của toàn trường
        $strSql = sprintf("SELECT * FROM ci_schedule 
                      WHERE school_id = %s ORDER BY begin DESC", secure($school_id, 'int'));

        $get_schedule = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $schedules = array();

        if ($get_schedule->num_rows > 0) {
            while ($schedule = $get_schedule->fetch_assoc()){
                $schedule['begin'] = toSysDate($schedule['begin']);
                $schedule['end'] = toSysDate($schedule['end']);
                $schedules[] = $schedule;
            }
        }
        return $schedules;
    }

    /**
     * Lấy danh sách schedule của trường API
     * @param $school_id
     * @return array
     */
    public function getScheduleOfSchoolForAPI($school_id, $offset = 0) {
        global $db, $system;

        $offset *= $system['max_results'];
        // Lấy danh sách lịch học của toàn trường
        $strSql = sprintf("SELECT schedule_id, schedule_name, begin, applied_for, school_id, class_level_id, class_id, is_notified FROM ci_schedule 
                      WHERE school_id = %s ORDER BY begin DESC LIMIT %s, %s", secure($school_id, 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false));

        $get_schedule = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $schedules = array();

        if ($get_schedule->num_rows > 0) {
            while ($schedule = $get_schedule->fetch_assoc()){
                $schedule['begin'] = toSysDate($schedule['begin']);
                $schedules[] = $schedule;
            }
        }
        return $schedules;
    }

    /**
     * Lấy danh sách lịch học app dụng cho 1 lớp
     *
     * @param $school_id
     * @param $class_level_id
     * @param $class_id
     * @return array
     * @throws Exception
     */
    public function getScheduleOfSchoolById($school_id, $class_level_id, $class_id) {
        global $db;

        // Lấy danh sách lịch học của toàn trường
        $strSql = sprintf("SELECT * FROM ci_schedule
                      WHERE school_id = %s AND is_notified = 1 AND ((class_level_id = 0 AND class_id = %s) OR (class_id = 0 AND class_level_id = %s) OR (class_id = 0 AND class_level_id = 0)) ORDER BY begin DESC", secure($school_id, 'int'), secure($class_id, 'int'), secure($class_level_id, 'int'));

        $get_schedule = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $schedules = array();
        if ($get_schedule->num_rows > 0) {

            while ($schedule = $get_schedule->fetch_assoc()){
                $schedule['begin'] = toSysDate($schedule['begin']);
                $schedules[] = $schedule;
            }
        }
        return $schedules;
    }

    /**
     * Lấy lịch học trong tuần của lớp
     *
     * @param $school_id
     * @param $class_level_id
     * @param $class_id
     * @param $monDate
     * @return array
     * @throws Exception
     */
    public function getScheduleOfSchoolByIdOnDate($school_id, $class_level_id, $class_id, $monDate) {
        global $db;

        $monDate = toDBDate($monDate);

        // Lấy danh sách lịch học của toàn trường
        $strSql = sprintf("SELECT * FROM ci_schedule
                      WHERE school_id = %s AND begin = %s AND is_notified = 1 AND ((class_level_id = 0 AND class_id = %s) OR (class_id = 0 AND class_level_id = %s) OR (class_id = 0 AND class_level_id = 0)) ORDER BY begin DESC", secure($school_id, 'int'), secure($monDate), secure($class_id, 'int'), secure($class_level_id, 'int'));

        $get_schedule = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $schedules = array();
        if ($get_schedule->num_rows > 0) {
            while ($schedule = $get_schedule->fetch_assoc()){
                $schedule['begin'] = toSysDate($schedule['begin']);
                $schedules[] = $schedule;
            }
        }
        return $schedules;
    }

    /*
     * Insert lịch học
     */
    public function insertSchedule(array $args = array()) {
        global $db, $date, $user;
        $this->_validateScheduleInput($args);
        $begin = toDBDate($args['begin']);


        $end = toDBDate($args['end']);
        $strSql = sprintf("INSERT INTO ci_schedule (schedule_name, applied_for, school_id, class_level_id, class_id, begin, description, is_category, is_saturday, is_notified, created_at, created_user_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", secure($args['schedule_name']), secure($args['applied_for'], 'int'), secure($args['school_id'], 'int'), secure($args['class_level_id'], 'int'), secure($args['class_id'], 'int'), secure($begin), secure($args['description']), secure($args['is_category'], 'int'), secure($args['is_saturday'], 'int'), secure($args['is_notified'], 'int'), secure($date), secure($user->_data['user_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        return $db->insert_id;

    }

    /*
     * validate insert lịch học add
     */
    private function _validateScheduleInput($args) {
        if(!isset($args['schedule_name'])) {
            throw new Exception(__("You must enter schedule name"));
        }
        if (strlen($args['schedule_name']) < 10 && strlen($args['schedule_name']) > 100) {
            throw new Exception(__("Schedule name must be greater than 10 and less than 100 characters"));
        }

        if (isset($args['description']) && (strlen($args['description'])) > 300) {
            throw new Exception(__("Description length must be less than 300 characters long"));
        }
        $begin = toDBDate($args['begin']);
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $dateBegin = strtotime($begin);
        $weekday =  strtolower(date('D', $dateBegin));
        if(!($weekday === "mon")) {
            throw new Exception(__("Start date must be Monday"));
        }
        global $db;
        $begin = toDBDate($args['begin']);
        $strSql = sprintf("SELECT * FROM ci_schedule WHERE school_id = %s AND class_level_id = %s AND class_id = %s AND begin = %s", secure($args['school_id'], 'int'), secure($args['class_level_id'], 'int'), secure($args['class_id'], 'int'), secure($begin));

        $getSchedule = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($getSchedule->num_rows > 0) {
            throw new Exception(__("There are schedules for this week, please check again!"));
        }
    }

    /*
     * validate insert lịch học edit
     */
    private function _validateScheduleInputEdit($args, $data) {
        if(!isset($args['schedule_name'])) {
            throw new Exception(__("You must enter schedule name"));
        }
        if (strlen($args['schedule_name']) < 10 && strlen($args['schedule_name']) > 100) {
            throw new Exception(__("Schedule name must be greater than 10 and less than 100 characters"));
        }

        if (isset($args['description']) && (strlen($args['description'])) > 300) {
            throw new Exception(__("Description length must be less than 300 characters long"));
        }
        $begin = toDBDate($args['begin']);
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $dateBegin = strtotime($begin);
        $weekday =  strtolower(date('D', $dateBegin));
        if(!($weekday === "mon")) {
            throw new Exception(__("Start date must be Monday"));
        }
        if(!(($data['school_id'] == $args['school_id']) && ($data['class_level_id'] == $args['class_level_id']) && ($data['class_id'] == $args['class_id']) && ($args['begin'] == $data['begin']))) {
            global $db;
            $begin = toDBDate($args['begin']);
            $strSql = sprintf("SELECT * FROM ci_schedule WHERE school_id = %s AND class_level_id = %s AND class_id = %s AND begin = %s", secure($args['school_id'], 'int'), secure($args['class_level_id'], 'int'), secure($args['class_id'], 'int'), secure($begin));

            $getSchedule = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            if($getSchedule->num_rows > 0) {
                throw new Exception(__("There are schedules for this week, please check again!"));
            }
        }
    }

    /*
     * Insert vào bảng ci_schedule_cata
     */
    public function insertScheduleCate ($schedule_id, array $cateIds = array()) {
        global $db;

        $strValues = "";
        //catelogy
        for ($idx = 0; $idx < count($cateIds); $idx++) {
            //service_id, fee_id, tuition_child_id, quantity, quantity_deduction, unit_price, unit_price_deduction, type
            $strValues .= "(" . secure($schedule_id, 'int') . "," . secure($cateIds[$idx], 'int') . "),";
        }
        $strValues = trim($strValues, ",");
        $strSql = "INSERT INTO ci_schedule_cate (schedule_id, category_id) VALUES " . $strValues;
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /*
     * Insert vào bảng ci_schedule_detail
     */
    public function insertScheduleDetail ($schedule_id, $starts, $categorys, $subject_detail_mon, $subject_detail_tue, $subject_detail_wed,$subject_detail_thu, $subject_detail_fri, $subject_detail_sat) {
        global $db;
        if(count($starts) == 0 || ((count($categorys) > 0)) && (count($starts) != count($categorys))) {
            throw new Exception(__('Please enter the full information'));
        }
        $strValues = "";
        // subject
        for ($idx = 0; $idx < count($starts); $idx++) {
            $strValues .= ",(" . $schedule_id . "," . secure($starts[$idx]). "," . secure($categorys[$idx]) . "," . secure($subject_detail_mon[$idx]) . "," . secure($subject_detail_tue[$idx]) . "," . secure($subject_detail_wed[$idx]) . "," . secure($subject_detail_thu[$idx]) . "," . secure($subject_detail_fri[$idx]) . "," . secure($subject_detail_sat[$idx]) . "),";
            $strValues = trim($strValues, ",");
            $strSql = "INSERT INTO ci_schedule_detail (schedule_id, subject_time, subject_name, monday, tuesday, wednesday, thursday, friday, saturday) VALUES " . $strValues;
        }
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /*
     * Insert vào bảng ci_schedule_detail (Hàm này chưa dùng tới, lưu lại cho tiện sửa, phòng lỗi)! Hì!
     */
    public function insertScheduleDetail2 ($schedule_id, $starts, $categorys, $subject_detail_mon, $subject_detail_tue, $subject_detail_wed,$subject_detail_thu, $subject_detail_fri, $subject_detail_sat) {
        global $db;
        if(count($starts) == 0 || ((count($categorys) > 0)) && (count($starts) != count($categorys))) {
            throw new Exception(__('Please enter the full information'));
        }
        $strValues = "";
        // subject
        for ($idx = 0; $idx < count($starts); $idx++) {
            if(empty($categorys) && empty($subject_detail_sat)) {
                $strValues .= "(" . secure($schedule_id, 'int') . "," . secure($starts[$idx]) . "," . secure($subject_detail_mon[$idx]) . "," . secure($subject_detail_tue[$idx]) . "," . secure($subject_detail_wed[$idx]) . "," . secure($subject_detail_thu[$idx]) . "," . secure($subject_detail_fri[$idx]) . "),";

                $strValues = trim($strValues, ",");
                $strSql = "INSERT INTO ci_schedule_detail (schedule_id, subject_time, monday, tuesday, wednesday, thursday, friday) VALUES " . $strValues;
            } elseif (!empty($categorys) && empty($subject_detail_sat)) {
                $strValues .= "(" . $schedule_id . "," . "'" . $starts[$idx]. "'" . "," . "'" . $categorys[$idx]. "'" . "," . "'" . $subject_detail_mon[$idx] . "'" . "," . "'" . $subject_detail_tue[$idx] . "'" . "," . "'" . $subject_detail_wed[$idx] . "'" . "," . "'" . $subject_detail_thu[$idx] . "'" . "," . "'" . $subject_detail_fri[$idx] . "'" . "),";
                $strValues = trim($strValues, ",");
                $strSql = "INSERT INTO ci_schedule_detail (schedule_id, subject_time, subject_name, monday, tuesday, wednesday, thursday, friday) VALUES " . $strValues;
            } elseif (empty($categorys) && !empty($subject_detail_sat)) {
                $strValues .= "(" . $schedule_id . "," . "'" . $starts[$idx]. "'" . "," . "'" . $subject_detail_mon[$idx] . "'" . "," . "'" . $subject_detail_tue[$idx] . "'" . "," . "'" . $subject_detail_wed[$idx] . "'" . "," . "'" . $subject_detail_thu[$idx] . "'" . "," . "'" . $subject_detail_fri[$idx] . "'" . "," . "'" . $subject_detail_sat[$idx] . "'" . "),";
                $strValues = trim($strValues, ",");
                $strSql = "INSERT INTO ci_schedule_detail (schedule_id, subject_time, monday, tuesday, wednesday, thursday, friday, saturday) VALUES " . $strValues;
            } else {
                $strValues .= "(" . $schedule_id . "," . "'" . $starts[$idx]. "'" . "," . "'" . $categorys[$idx]. "'" . "," . "'" . $subject_detail_mon[$idx] . "'" . "," . "'" . $subject_detail_tue[$idx] . "'" . "," . "'" . $subject_detail_wed[$idx] . "'" . "," . "'" . $subject_detail_thu[$idx] . "'" . "," . "'" . $subject_detail_fri[$idx] . "'" . "," . "'" . $subject_detail_sat[$idx] . "'" . "),";
                $strValues = trim($strValues, ",");
                $strSql = "INSERT INTO ci_schedule_detail (schedule_id, subject_time, subject_name, monday, tuesday, wednesday, thursday, friday, saturday) VALUES " . $strValues;
            }
        }
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /*
     * Insert vào bảng ci_schedule_subject
     */
    public function insertSubject ($cateId, $starts, $subjectNames, $descriptions) {
        global $db;
        if(count($starts) == 0 || count($subjectNames) == 0 || count($starts) != count($subjectNames)) {
            _error(400);
        }

        $strValues = "";
        //catelogy
        for ($idx = 0; $idx < count($starts); $idx++) {

            $strValues .= "(" . secure($cateId, 'int') . "," . secure($subjectNames[$idx]) . "," . secure($starts[$idx]) . "," . secure($descriptions[$idx]) . "),";
        }
        $strValues = trim($strValues, ",");
        $strSql = "INSERT INTO ci_schedule_subject (category_id, subject_name, begin, description) VALUES " . $strValues;

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $firstId = $db->insert_id;
        $lastId = $firstId + count($starts) - 1;
        $subjectIds = array();
        for($i = $firstId; $i <= $lastId; $i++) {
            $subjectIds[] = $i;
        }
        return $subjectIds;
    }

    /*
     * LẤy category theo lựa chợn
     */
    public function getCateById ($school_id, $class_level_id, $class_id) {
        global $db;
        if(!isset($school_id) || $school_id == 0 || !isset($class_level_id) || !isset($class_id)) {
            _error(400);
        }
        $strSql = sprintf("SELECT * FROM ci_category WHERE school_id = %s AND class_level_id = %s AND class_id = %s AND status = %s", secure($school_id, 'int'), secure($class_level_id, 'int'), secure($class_id, 'int'), 1);

        $get_category = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $category = array();
        if ($get_category->num_rows > 0) {
            while($cate = $get_category->fetch_assoc()) {
                $subjects = $this->_getSubjectByCateId($cate['category_id']);
                $cate['subjects'] = $subjects;
                $category[] = $cate;
            }
//            if(count($category) > 1) {
//                throw new Exception("Có " . count($category) . " chương trình học đang được áp dụng cho đối tượng này, vui lòng kiểm tra lại!");
//            }
        }
        return $category;
    }

    /*
     * Chuyển trạng thái cho những chương trình học trước đó về không sử dụng
     */
    public function updateCategoryStatus(array $categoryOldId = array()) {
        global $db, $date;

        for($i = 0; $i < count($categoryOldId); $i++) {
            $strSql = sprintf("UPDATE ci_category SET status = %s, updated_at = %s WHERE %s", 0, secure($date), secure($categoryOldId[$i]));
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }

    /*
     * LẤy Schedule theo lựa chọn
     */
    public function getScheduleById($school_id, $class_level_id, $class_id) {
        global $db;
        if(!isset($school_id) || $school_id == 0 || !isset($class_level_id) || !isset($class_id)) {
            _error(400);
        }
        $strSql = sprintf("SELECT * FROM ci_schedule
            WHERE school_id = %s AND class_level_id = %s AND class_id = %s ORDER BY begin DESC", secure($school_id, 'int'), secure($class_level_id, 'int'), secure($class_id, 'int'));

        $get_schedule = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $schedules = array();

        if ($get_schedule->num_rows > 0) {
            while($sche = $get_schedule->fetch_assoc()) {
                $sche['begin'] = toSysDate($sche['begin']);
                $schedules[] = $sche;
            }
        }
        return $schedules;
    }

    /*
     * LẤy danh sách môn học theo category
     */
    private function _getSubjectByCateId($category_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_schedule_subject WHERE category_id = %s", secure($category_id, 'int'));
        $get_subject = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $subjects = array();
        if($get_subject->num_rows > 0) {
            while($subject = $get_subject->fetch_assoc()) {
                $subjects[] = $subject;
            }
        }
        return $subjects;
    }
    /*
     * Update subject
     */
    public function updateSubject($cateId, $subjectIds, $starts, $subjectNames, $descriptions) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_schedule_subject WHERE category_id = %s", secure($cateId, 'int'));

        $get_subject = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $subjectIdDbs = array();
        if($get_subject->num_rows > 0) {
            while($sub = $get_subject->fetch_assoc()['subject_id']) {
                $subjectIdDbs[] = $sub;
            }
        }

        if(count($subjectIds) == 0) {
            $subjectIdDeletes = $subjectIdDbs;
        } else {
            $subjectIdDeletes = array_diff($subjectIdDbs, $subjectIds);
        }
        foreach($subjectIdDeletes as $id) {
            $strSqlDelete = sprintf("DELETE FROM ci_schedule_subject WHERE subject_id = %s", secure($id, 'int'));

            $db->query($strSqlDelete) or _error(SQL_ERROR_THROWEN);
        }
        $strValuesInsert = "";
        //catelogy
        for ($idx = 0; $idx < count($subjectIds); $idx++) {

            if($subjectIds[$idx] > 0) {
                $strSqlUpdate = sprintf("UPDATE ci_schedule_subject SET subject_name = %s, begin = %s, description = %s WHERE subject_id = %s", secure($subjectNames[$idx]), secure($starts[$idx]), secure($descriptions[$idx]), secure($subjectIds[$idx], 'int'));

                $db->query($strSqlUpdate) or _error(SQL_ERROR_THROWEN);
            } else {
                $strValuesInsert .= "(" . secure($cateId, 'int') . "," . secure($subjectNames[$idx]) . ","  . secure($starts[$idx]) . "," . secure($descriptions[$idx]) . "),";
            }
        }

        $strValuesInsert = trim($strValuesInsert, ",");
        if($strValuesInsert != null) {
            $strSqlInsert = "INSERT INTO ci_schedule_subject (category_id, subject_name, begin, description) VALUES " . $strValuesInsert;

            $db->query($strSqlInsert) or _error(SQL_ERROR_THROWEN);
        }

    }
    /*
     * Edit subject khi edit lịch học
     */
    public function editSubject($subjectIds, $subjectNames, $subjectTimes) {
        global $db;

        for($i = 0; $i < count($subjectIds); $i++) {
            $strSql = sprintf("UPDATE ci_schedule_subject SET subject_name = %s, begin = %s WHERE subject_id = %s", secure($subjectNames[$i]), secure($subjectTimes[$i]), secure($subjectIds[$i], 'int'));

            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }

    /*
     * lấy chi tiết lịch học
     */
    public function getScheduleDetailById($scheduleId) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_schedule 
                    WHERE schedule_id = %s", secure($scheduleId, 'int'));
        $get_schedule = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $schedule = array();
        if($get_schedule->num_rows > 0) {
            $sche = $get_schedule->fetch_assoc();
            $sche['begin'] = toSysDate($sche['begin']);
            $details = $this->_getScheduleDetail($sche['schedule_id']);
            $sche['details'] = $details;
            $schedule = $sche;
        }

        return $schedule;
    }
    /*
     * Lấy chi tiết các môn học của lịch học
     */
    private function _getScheduleDetail($scheduleId) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_schedule_detail
                      WHERE schedule_id = %s", secure($scheduleId, 'int'));

        $get_shedule_detail = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $schedule_detail =  array();
        if($get_shedule_detail->num_rows > 0) {
            while ($sub = $get_shedule_detail->fetch_assoc()) {
                $sub['monday'] = trim($sub['monday'], ' ');
                $sub['tuesday'] = trim($sub['tuesday'], ' ');
                $sub['wednesday'] = trim($sub['wednesday'], ' ');
                $sub['thursday'] = trim($sub['thursday'], ' ');
                $sub['friday'] = trim($sub['friday'], ' ');
                $sub['saturday'] = trim($sub['saturday'], ' ');
                $schedule_detail[] = $sub;
            }
        }

        return $schedule_detail;
    }

    /*
     * Update schedule
     */
    public function updateSchedule(array $args = array(), $data) {
        global $db, $date;
        $this->_validateScheduleInputEdit($args, $data);

        $begin = toDBDate($args['begin']);
        $strSql = sprintf("UPDATE ci_schedule SET schedule_name = %s, begin = %s, description = %s, applied_for = %s, class_level_id = %s, class_id = %s, is_category = %s, is_saturday = %s, is_notified = %s, updated_at = %s WHERE schedule_id = %s", secure($args['schedule_name']), secure($begin), secure($args['description']), secure($args['applied_for'], 'int'), secure($args['class_level_id'], 'int'), secure($args['class_id'], 'int'), secure($args['is_category'], 'int'), secure($args['is_saturday'], 'int'), secure($args['is_notified'], 'int'), secure($date), secure($args['schedule_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }
    /*
     * Update category
     */
    public function updateCategory(array $args = array()) {
        global $db, $date;

        $strSql = sprintf("UPDATE ci_category SET class_level_id = %s, class_id = %s, updated_at = %s, type = %s WHERE category_id = %s", secure($args['class_level_id']), secure($args['class_id']), secure($date), secure($args['type']), secure($args['category_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /*
     * Update subject_detail
     */
    public function updateSubjectDetail($scheduleId, $subjectIds, $subject_detail_mon, $subject_detail_tue, $subject_detail_wed, $subject_detail_thu, $subject_detail_fri, $subject_detail_sat) {
        global $db;
        // subject
        for ($idx = 0; $idx < count($subjectIds); $idx++) {
            $strSql = sprintf("UPDATE ci_subject_detail 
                    SET monday = %s, tuesday = %s, wednesday = %s, thursday = %s, friday = %s, saturday = %s 
                    WHERE schedule_id = %s AND subject_id = %s", secure($subject_detail_mon[$idx]), secure($subject_detail_tue[$idx]), secure($subject_detail_wed[$idx]), secure($subject_detail_thu[$idx]), secure($subject_detail_fri[$idx]), secure($subject_detail_sat[$idx]), secure($scheduleId, 'int'), secure($subjectIds[$idx], 'int'));

            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }

    /*
     * Delete lịch học (ci_schedule)
     */
    public function deleteSchedule($id) {
        global $db;

        $strSql = sprintf("DELETE FROM ci_schedule WHERE schedule_id = %s", secure($id, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $strSql = sprintf("DELETE FROM ci_schedule_detail WHERE schedule_id = %s", secure($id, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Delete schedule detail
     */
    public function deleteScheduleDetail($scheduleId) {
        global $db;

        $strSql = sprintf("DELETE FROM ci_schedule_detail WHERE schedule_id = %s", secure($scheduleId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /*
     * Delete category (ci_schedule)
     */
    public function deleteCategory($id) {
        global $db;

        // Xóa category
        $strSql = sprintf("DELETE FROM ci_category WHERE category_id = %s", secure($id, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        // Lấy những schedule áp dụng category bị xóa
        $strSql = sprintf("SELECT * FROM ci_schedule WHERE category_id = %s", secure($id, 'int'));
        $get_schedules = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $scheduleIds = array();
        if($get_schedules->num_rows > 0) {
            while($sche = $get_schedules->fetch_assoc()['schedule_id']) {
                $scheduleIds[] = $sche;
            }
        }
        for($i = 0; $i < count($scheduleIds); $i++) {
            // Delete ci_schedule
            $strSql = sprintf("DELETE FROM ci_schedule WHERE schedule_id = %s", secure($scheduleIds[$i], 'int'));
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            // Delete ci_subject_detail
            $strSql = sprintf("DELETE FROM ci_subject_detail WHERE schedule_id = %s", secure($scheduleIds[$i], 'int'));
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
        // Delete ci_schedule_subject

        $strSql = sprintf("DELETE FROM ci_schedule_subject WHERE category_id = %s", secure($id));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    public function updateStatusToNotified($schedule_id) {
        global $db, $date;

        $strSql = sprintf("UPDATE ci_schedule SET is_notified = %s, updated_at = %s WHERE schedule_id = %s", 1, secure($date), secure($schedule_id, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Thêm chi tiết lịch học bằng excel
     *
     * @param $args
     * @return mixed
     */
    public function insertScheduleDetailImport($args) {
        global $db;

        $strSql = sprintf("INSERT INTO ci_schedule_detail (schedule_id, subject_name, subject_time, monday, tuesday, wednesday, thursday, friday, saturday) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)", secure($args['schedule_id'], 'int'), secure($args['category']), secure($args['start']), secure($args['mon']), secure($args['tue']), secure($args['wed']), secure($args['thu']), secure($args['fri']), secure($args['sat']));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        return $db->insert_id;
    }

    /**
     * Lấy danh sách lịch học theo level
     *
     * @param $schoolId
     * @param $level
     * @return array
     * @throws Exception
     */
    public function getAllScheduleByLevel($schoolId, $level) {
        global $db;

        // Lấy danh sách lịch học theo level
        $strSql = sprintf("SELECT * FROM ci_schedule 
                      WHERE school_id = %s AND applied_for = %s ORDER BY begin DESC", secure($schoolId, 'int'), secure($level, 'int'));

        $get_schedule = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $schedules = array();

        if ($get_schedule->num_rows > 0) {
            while ($schedule = $get_schedule->fetch_assoc()){
                $schedule['begin'] = toSysDate($schedule['begin']);
                $schedule['end'] = toSysDate($schedule['end']);
                $schedules[] = $schedule;
            }
        }
        return $schedules;
    }

    /* ---------- SCHEDULE - API ---------- */

    /*
     * Lấy danh sách lịch học toàn cho trường
     */
    public function getScheduleSchoolForApi($school_id, $class_level_id, $class_id, $offset = 0) {
        global $db, $system;

        $offset *= $system['max_results'];

        // Lấy danh sách lịch học của toàn trường
        $strSql = sprintf("SELECT class_level_id, class_id, schedule_id, schedule_name, applied_for, begin, status, description FROM ci_schedule
                      WHERE school_id = %s AND is_notified = 1 AND ((class_level_id = 0 AND class_id = %s) OR (class_id = 0 AND class_level_id = %s) OR (class_id = 0 AND class_level_id = 0)) ORDER BY begin DESC LIMIT %s, %s", secure($school_id, 'int'), secure($class_id, 'int'), secure($class_level_id, 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false));

        $get_schedule = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $schedules = array();
        if ($get_schedule->num_rows > 0) {

            while ($schedule = $get_schedule->fetch_assoc()){
                $schedule['begin'] = toSysDate($schedule['begin']);
                $schedules[] = $schedule;
            }
        }
        return $schedules;
    }

    /*
     * Lấy danh sách lịch học trong tuần này
     */
    public function getScheduleSchoolInWeekForApi($school_id, $class_level_id, $class_id, $begin) {
        global $db;

        // Lấy danh sách lịch học của toàn trường
        $strSql = sprintf("SELECT schedule_id, applied_for, begin FROM ci_schedule
                      WHERE school_id = %s AND begin = %s AND is_notified = 1 AND ((class_level_id = 0 AND class_id = %s) OR (class_id = 0 AND class_level_id = %s) OR (class_id = 0 AND class_level_id = 0)) ORDER BY begin DESC", secure($school_id, 'int'),secure($begin), secure($class_id, 'int'), secure($class_level_id, 'int'));

        $get_schedule = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $schedules = array();
        if ($get_schedule->num_rows > 0) {

            while ($schedule = $get_schedule->fetch_assoc()){
                $schedule['begin'] = toSysDate($schedule['begin']);
                $schedules[] = $schedule;
            }
        }
        return $schedules;
    }


    /*
     * lấy chi tiết lịch học
     */
    public function getScheduleDetailForApi($scheduleId) {
        global $db;

        $strSql = sprintf("SELECT schedule_id, schedule_name, applied_for, begin, status, is_category, is_saturday, is_notified, description FROM ci_schedule 
                    WHERE schedule_id = %s", secure($scheduleId, 'int'));
        $get_schedule = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $schedule = null;
        if($get_schedule->num_rows > 0) {
            $sche = $get_schedule->fetch_assoc();
            $sche['begin'] = toSysDate($sche['begin']);
            $details = $this->_getScheduleDetailForApi($sche['schedule_id']);
            $sche['details'] = $details;
            $schedule = $sche;
        }

        return $schedule;
    }


    /*
     * Lấy chi tiết các môn học của lịch học
     */
    private function _getScheduleDetailForApi($scheduleId) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_schedule_detail WHERE schedule_id = %s", secure($scheduleId, 'int'));

        $get_shedule_detail = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $schedule_detail =  array();
        if($get_shedule_detail->num_rows > 0) {
            while ($sub = $get_shedule_detail->fetch_assoc()) {
                $sub['monday'] = trim($sub['monday'], ' ');
                $sub['tuesday'] = trim($sub['tuesday'], ' ');
                $sub['wednesday'] = trim($sub['wednesday'], ' ');
                $sub['thursday'] = trim($sub['thursday'], ' ');
                $sub['friday'] = trim($sub['friday'], ' ');
                $sub['saturday'] = trim($sub['saturday'], ' ');
                $schedule_detail[] = $sub;
            }
        }

        return $schedule_detail;
    }

    /* ---------- END - API ---------- */

}
?>