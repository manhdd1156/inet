<?php
/**
 * DAO -> Point
 * Thao tác với điểm
 *
 * @package Coniu
 * @author Coniu
 * MODIFY table ci_point_c2 -> ci_point_c2 new BY MANHDD 04/01/2021
 */

class PointDAO
{
    /**
     * __construct
     *
     */
    public function __construct()
    {
    }

    /**
     * Lấy danh sách môn học của một khối
     *
     * @param $schoolId
     * @param $govClassLevel
     * @return array
     * @throws Exception
     */
    public function getClassLevelSubject($schoolId, $govClassLevel, $school_year)
    {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_class_level_subject where school_id = %s AND gov_class_level = %s AND school_year = %s", secure($schoolId, 'int'), secure($govClassLevel, 'int'), secure($school_year));

        $subjects = array();
        $get_subject = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        if ($get_subject->num_rows > 0) {
            while ($subject = $get_subject->fetch_assoc()) {
                $subjects[] = $subject;
            }
        }

        return $subjects;
    }

    /**
     * Thêm bản ghi vào bảng ci_class_subject_teacher
     *
     * @param $class_id
     * @param $semester
     * @param $subject_ids
     * @param $teacher_ids
     * @param $school_year
     * @throws Exception
     */
    public function createClassSubjectTeacher($class_id, $semester, $subject_ids, $teacher_ids, $school_year)
    {
        global $db;

        // Delete toàn bộ dữ liệu cũ phân công cũ
        $strSql = sprintf("DELETE FROM ci_class_subject_teacher WHERE class_id = %s AND semester = %s AND school_year = %s", secure($class_id, 'int'), secure($semester, 'int'), secure($school_year));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $strValues = "";
        foreach ($subject_ids as $key => $subject_id) {
            //class_id, semester, subject_id, teacher_id, school_year
            $strValues .= "(" . secure($class_id, 'int') . "," . secure($semester, 'int') . "," . secure($subject_id, 'int') . "," . secure($teacher_ids[$key], 'int') . "," . secure($school_year) . "),";
        }
        $strValues = trim($strValues, ",");

        $strSql = "INSERT INTO ci_class_subject_teacher (class_id, semester, subject_id, teacher_id, school_year) VALUES " . $strValues;

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Check xem trẻ đã được insert điểm trong học kỳ này chưa
     *
     * @param $classId
     * @param $schoolYear
     * @param $childCode
     * @param $grade
     * @return bool
     * @throws Exception
     */
    public function isCreatedPointChild($classId, $schoolYear, $childCode, $grade)
    {
        global $db;
        if ($grade == 1) {
            $strSql = sprintf("SELECT * FROM ci_point_c1 WHERE class_id = %s AND school_year = %s AND child_code = %s", secure($classId, 'int'), secure($schoolYear), secure($childCode));
        } elseif ($schoolYear == 2) {
            /* UPDATE START - ManhDD 01/04/2021 */
//            $strSql = sprintf("SELECT * FROM ci_point_c2 WHERE class_id = %s AND school_year = %s AND child_code = %s", secure($classId, 'int'), secure($schoolYear), secure($childCode));
            $strSql = sprintf("SELECT * FROM ci_point_c2 WHERE class_id = %s AND school_year = %s AND child_code = %s", secure($classId, 'int'), secure($schoolYear), secure($childCode));
            /* UPDATE END - ManhDD 01/04/2021 */
        }

        $get_point = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_point->num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Lấy danh sách điểm của học sinh trong lớp trong 1 năm học theo subjectId
     *
     * @param $classId
     * @param $schoolyear
     * @param $subjectId
     * @return array
     * @throws Exception
     */
    public function getChildrenPointsBySubjectId($classId, $schoolyear, $subjectId)
    {
        global $db;
        //UPDATE START MANHDD 06/07/2021
//        if($grade == 1) {
//            $strSql = sprintf("SELECT * FROM ci_point_c1 WHERE class_id = %s AND school_year = %s AND subject_id = %s", secure($classId, 'int'), secure($schoolyear), secure($subjectId, 'int'));
//        } elseif ($grade == 2) {
//            /* UPDATE START - ManhDD 01/04/2021 */
////            $strSql = sprintf("SELECT * FROM ci_point_c2 WHERE class_id = %s AND school_year = %s AND subject_id = %s", secure($classId, 'int'), secure($schoolyear), secure($subjectId, 'int'));
//            $strSql = sprintf("SELECT * FROM ci_point_c2 WHERE class_id = %s AND school_year = %s AND subject_id = %s", secure($classId, 'int'), secure($schoolyear), secure($subjectId, 'int'));
//            /* UPDATE END - ManhDD 01/04/2021 */
//        }
        $strSql = sprintf("SELECT * FROM ci_point WHERE class_id = %s AND school_year = %s AND subject_id = %s", secure($classId, 'int'), secure($schoolyear), secure($subjectId, 'int'));
        //UPDATE END MANHDD 06/07/2021
        $get_children_point = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $children_point = array();
        if ($get_children_point->num_rows > 0) {
            while ($point = $get_children_point->fetch_assoc()) {
                $children_point[] = $point;
            }
        }

        return $children_point;
    }

    /**
     * Lấy danh sách điểm của học sinh trong lớp trong 1 năm học theo studentId
     *
     * @param $classId
     * @param $schoolyear
//     * @param $grade // Delete MANHDD 11/06/2021
     * @param $studentId
     * @return array
     * @throws Exception
     */
    public function getChildrenPointsByStudentId($classId, $schoolyear, $studentId)
    {
        global $db;
        /* UPDATE START - ManhDD 11/06/2021 */
//        if ($grade == 1) {
//            $strSql = sprintf("SELECT cpc.* FROM `ci_point_c1` cpc JOIN `ci_child` cc ON cpc.child_code = cc.child_code WHERE cpc.class_id = %s AND cpc.school_year = %s AND cc.child_id = %s", secure($classId, 'int'), secure($schoolyear), secure($studentId));
//        } elseif ($grade == 2) {
//            /* UPDATE START - ManhDD 01/04/2021 */
//            $strSql = sprintf("SELECT cpc.* FROM `ci_point_c2` cpc JOIN `ci_child` cc ON cpc.child_code = cc.child_code WHERE cpc.class_id = %s AND cpc.school_year = %s AND cc.child_id = %s", secure($classId, 'int'), secure($schoolyear), secure($studentId));
////            $strSql = sprintf("SELECT * FROM ci_point_c2 WHERE class_id = %s AND school_year = %s AND subject_id = %s", secure($classId, 'int'), secure($schoolyear), secure($subjectId, 'int'));
//            /* UPDATE END - ManhDD 01/04/2021 */
//        }
        $strSql = sprintf("SELECT cp.* FROM `ci_point` cp JOIN `ci_child` cc ON cp.child_code = cc.child_code WHERE cp.class_id = %s AND cp.school_year = %s AND cc.child_id = %s", secure($classId, 'int'), secure($schoolyear), secure($studentId));

        /* UPDATE END - ManhDD 11/06/2021 */
        $get_children_point = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $children_point = array();
        if ($get_children_point->num_rows > 0) {
            while ($point = $get_children_point->fetch_assoc()) {
                $children_point[] = $point;
            }
        }

        return $children_point;
    }

    /**
     * Lấy danh sách điểm của học sinh trong lớp trong 1 năm học theo studentCode
     *
     * @param $classId
     * @param $schoolyear
//     * @param $grade
     * @param $studentCode
     * @return array
     * @throws Exception
     */
    public function getChildrenPointsByStudentCode($classId, $schoolyear, $studentCode)
    {
        global $db;
        //UPDATE START MANHDD 07/06/2021
//        if ($grade == 1) {
//            $strSql = sprintf("SELECT * FROM ci_point_c1 WHERE class_id = %s AND school_year = %s AND child_code = %s", secure($classId, 'int'), secure($schoolyear), secure($studentCode));
//        } elseif ($grade == 2) {
//            /* UPDATE START - ManhDD 01/04/2021 */
//            $strSql = sprintf("SELECT * FROM ci_point_km_c2 WHERE class_id = %s AND school_year = %s AND child_code = %s", secure($classId, 'int'), secure($schoolyear), secure($studentCode));
////            $strSql = sprintf("SELECT * FROM ci_point_c2 WHERE class_id = %s AND school_year = %s AND subject_id = %s", secure($classId, 'int'), secure($schoolyear), secure($subjectId, 'int'));
//            /* UPDATE END - ManhDD 01/04/2021 */
//        }

            $strSql = sprintf("SELECT * FROM ci_point WHERE class_id = %s AND school_year = %s AND child_code = %s", secure($classId, 'int'), secure($schoolyear), secure($studentCode));
        //UPDATE END MANHDD 07/06/2021
        $get_children_point = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $children_point = array();
        if ($get_children_point->num_rows > 0) {
            while ($point = $get_children_point->fetch_assoc()) {
                $children_point[] = $point;
            }
        }

        return $children_point;
    }

    /**
     * Cập nhật điểm của 1 trẻ trong 1 học kỳ
     *
     * @param $class_id
     * @param $subject_id
     * @param $semester
     * @param $school_year
     * @param $child
     * @param $score_fomula
     * @throws Exception
     */
    public function updateChildPoint($class_id, $subject_id, $school_year, $child)
    {
        global $db;
        //UPDATE START MANHDD 09/06/2021
//        if ($grade == 1) {
//            // Update vào bảng điểm cấp 1
//            if ($semester == 1) {
//                if (!$is_last_semester) {
//                    $strSql = sprintf("UPDATE ci_point_c1 SET gk1nx = %s, gk1nl = %s, gk1pc = %s, gk1kt = %s, gk1xl = %s WHERE class_id = %s AND subject_id = %s AND school_year = %s AND child_code = %s", secure($child['nx']), secure($child['nl']), secure($child['pc']), secure($child['kt']), secure($child['xl']), secure($class_id, 'int'), secure($subject_id, 'int'), secure($school_year), secure($child['child_code']));
//                } else {
//                    $strSql = sprintf("UPDATE ci_point_c1 SET ck1nx = %s, ck1nl = %s, ck1pc = %s, ck1kt = %s, ck1xl = %s WHERE class_id = %s AND subject_id = %s AND school_year = %s AND child_code = %s", secure($child['nx']), secure($child['nl']), secure($child['pc']), secure($child['kt']), secure($child['xl']), secure($class_id, 'int'), secure($subject_id, 'int'), secure($school_year), secure($child['child_code']));
//                }
//            } elseif ($semester == 2) {
//                if (!$is_last_semester) {
//                    $strSql = sprintf("UPDATE ci_point_c1 SET gk2nx = %s, gk2nl = %s, gk2pc = %s, gk2kt = %s, gk2xl = %s WHERE class_id = %s AND subject_id = %s AND school_year = %s AND child_code = %s", secure($child['nx']), secure($child['nl']), secure($child['pc']), secure($child['kt']), secure($child['xl']), secure($class_id, 'int'), secure($subject_id, 'int'), secure($school_year), secure($child['child_code']));
//                } else {
//                    $strSql = sprintf("UPDATE ci_point_c1 SET ck2nx = %s, ck2nl = %s, ck2pc = %s, ck2kt = %s, ck2xl = %s WHERE class_id = %s AND subject_id = %s AND school_year = %s AND child_code = %s", secure($child['nx']), secure($child['nl']), secure($child['pc']), secure($child['kt']), secure($child['xl']), secure($class_id, 'int'), secure($subject_id, 'int'), secure($school_year), secure($child['child_code']));
//                }
//            }
//        } elseif ($grade == 2) {
//            // Update vào ci_point_c2
//            /* UPDATE START - ManhDD 01/04/2021 */
//            if ($semester == 1 || $semester == 2) {
//                $strSql = sprintf("UPDATE ci_point_c2 SET m11 = %s, m12 = %s, m13 = %s, d1 = %s WHERE class_id = %s AND subject_id = %s AND school_year = %s AND child_code = %s", $child['m1'] ? secure($child['m1']) : 'NULL', $child['m2'] ? secure($child['m2']) : 'NULL', $child['m3'] ? secure($child['m3']) : 'NULL', $child['d'] ? secure($child['d']) : 'NULL', secure($class_id, 'int'), secure($subject_id, 'int'), secure($school_year), secure($child['child_code']));
//            } elseif ($semester == 0) {
//                $strSql = sprintf("UPDATE ci_point_c2 SET m11 = %s, m12 = %s, m13 = %s, d1 = %s,m21 = %s, m22 = %s, m23 = %s, d2 = %s, re_exam = %s WHERE class_id = %s AND subject_id = %s AND school_year = %s AND child_code = %s", $child['m11'] ? secure($child['m11']) : 'NULL', $child['m12'] ? secure($child['m12']) : 'NULL', $child['m13'] ? secure($child['m13']) : 'NULL', $child['d1'] ? secure($child['d1']) : 'NULL', $child['m21'] ? secure($child['m21']) : 'NULL', $child['m22'] ? secure($child['m22']) : 'NULL', $child['m23'] ? secure($child['m23']) : 'NULL', $child['d2'] ? secure($child['d2']) : 'NULL', $child['reexam'] ? secure($child['reexam']) : 'NULL', secure($class_id, 'int'), secure($subject_id, 'int'), secure($school_year), secure($child['child_code']));
//            } elseif ($semester == 3) { // thi lại ( dành cho mobile )
//                $strSql = sprintf("UPDATE ci_point_c2 SET re_exam = %s WHERE class_id = %s AND subject_id = %s AND school_year = %s AND child_code = %s", $child['reexam'] ? secure($child['reexam']) : 'NULL', secure($class_id, 'int'), secure($subject_id, 'int'), secure($school_year), secure($child['child_code']));
//            }
//            /* UPDATE END - ManhDD 01/04/2021 */
//        }

                $strSql = "UPDATE ci_point SET ";
                foreach($child as $key => $value) {
                    if(in_array(substr($key,0,2),array("hs","gk","ck"))) {
                        $strSql.= $key .'='.($value=='' ? 'NULL' : secure($value)).',';
                    }else if(substr($key,0,3) =="re_"){
                        $strSql.= $key .'='.($value=='' ? 'NULL' : secure($value)).',';
                    }
                }
                $strSql =  trim($strSql, ',');
                $strSql.= ' WHERE class_id ='.secure($class_id, 'int').' AND subject_id ='.secure($subject_id, 'int').' AND school_year ='.secure($school_year).' AND child_code ='.secure($child['child_code']);
        //UPDATE END MANHDD 09/06/2021
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Tạo bản ghi điểm mới trong 1 kỳ
     *
     * @param $class_id
     * @param $subject_id
//     * @param $semester // DELETE MANHDD 11/06/2021
//     * @param $grade   // DELETE MANHDD 11/06/2021
     * @param $school_year
     * @param $child
//     * @param $is_last_semester // DELETE MANHDD 11/06/2021
     * @return mixed
     * @throws Exception
     */
    public function createChildPoint($class_id, $subject_id, $school_year, $child)
    {
        global $db;
        /* UPDATE START - ManhDD 11/06/2021 */
//        if ($grade == 1) {
//            if ($semester == 1) {
//                if (!$is_last_semester) {
//                    $strSql = sprintf("INSERT INTO ci_point_c1 (class_id, subject_id, child_code, school_year, gk1nx, gk1nl, gk1pc, gk1kt, gk1xl) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)", secure($class_id, 'int'), secure($subject_id, 'int'), secure($child['child_code']), secure($school_year), secure($child['nx']), secure($child['nl']), secure($child['pc']), secure($child['kt']), secure($child['xl']));
//                } else {
//                    $strSql = sprintf("INSERT INTO ci_point_c1 (class_id, subject_id, child_code, school_year, ck1nx, ck1nl, ck1pc, ck1kt, ck1xl) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)", secure($class_id, 'int'), secure($subject_id, 'int'), secure($child['child_code']), secure($school_year), secure($child['nx']), secure($child['nl']), secure($child['pc']), secure($child['kt']), secure($child['xl']));
//                }
//            } else {
//                if (!$is_last_semester) {
//                    $strSql = sprintf("INSERT INTO ci_point_c1 (class_id, subject_id, child_code, school_year, gk2nx, gk2nl, gk2pc, gk2kt, gk2xl) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)", secure($class_id, 'int'), secure($subject_id, 'int'), secure($child['child_code']), secure($school_year), secure($child['nx']), secure($child['nl']), secure($child['pc']), secure($child['kt']), secure($child['xl']));
//                } else {
//                    $strSql = sprintf("INSERT INTO ci_point_c1 (class_id, subject_id, child_code, school_year, ck2nx, ck2nl, ck2pc, ck2kt, ck2xl) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)", secure($class_id, 'int'), secure($subject_id, 'int'), secure($child['child_code']), secure($school_year), secure($child['nx']), secure($child['nl']), secure($child['pc']), secure($child['kt']), secure($child['xl']));
//                }
//            }
//        } elseif ($grade == 2) {
//            /* UPDATE START - ManhDD 01/04/2021 */
////            if($semester == 1) {
////                $strSql = sprintf("INSERT INTO ci_point_c2 (class_id, subject_id, child_code, school_year, m11, m12, m13, p11, p12, p13, v11, v12, v13, v14, v15, v16, v17, v18, hk1, tbhk1) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", secure($class_id, 'int'), secure($subject_id, 'int'), secure($child['child_code']), secure($school_year), secure($child['m1']), secure($child['m2']), secure($child['m3']), secure($child['p1']), secure($child['p2']), secure($child['p3']), secure($child['v1']), secure($child['v2']), secure($child['v3']), secure($child['v4']), secure($child['v5']), secure($child['v6']), secure($child['v7']), secure($child['v8']), secure($child['hk']), secure($child['tbhk']));
////            } elseif($semester == 2) {
////                $strSql = sprintf("INSERT INTO ci_point_c2 (class_id, subject_id, child_code, school_year, m21, m22, m23, p21, p22, p23, v21, v22, v23, v24, v25, v26, v27, v28, hk2, tbhk2) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", secure($class_id, 'int'), secure($subject_id, 'int'), secure($child['child_code']), secure($school_year), secure($child['m1']), secure($child['m2']), secure($child['m3']), secure($child['p1']), secure($child['p2']), secure($child['p3']), secure($child['v1']), secure($child['v2']), secure($child['v3']), secure($child['v4']), secure($child['v5']), secure($child['v6']), secure($child['v7']), secure($child['v8']), secure($child['hk']), secure($child['tbhk']));
////            } elseif ($semester == 0) {
////                $strSql = sprintf("INSERT INTO ci_point_c2 (class_id, subject_id, child_code, school_year, tbyear, tl) VALUES (%s, %s)", secure($class_id, 'int'), secure($subject_id, 'int'), secure($child['child_code']), secure($school_year), secure($child['tbyear']), secure($child['tl']));
////            }
//            if ($semester == 1) {
//                $strSql = sprintf("INSERT INTO ci_point_c2 (class_id, subject_id, child_code, school_year, m11, m12, m13, d1) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)", secure($class_id, 'int'), secure($subject_id, 'int'), secure($child['child_code']), secure($school_year), secure($child['m11']), secure($child['m12']), secure($child['13']), secure($child['d1']));
//            } elseif ($semester == 2) {
//                $strSql = sprintf("INSERT INTO ci_point_c2 (class_id, subject_id, child_code, school_year, m21, m22, m23, d2) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)", secure($class_id, 'int'), secure($subject_id, 'int'), secure($child['child_code']), secure($school_year), secure($child['m21']), secure($child['m22']), secure($child['m23']), secure($child['d2']));
//            } elseif ($semester == 0) {
//                $strSql = sprintf("INSERT INTO ci_point_c2 (class_id, subject_id, child_code, school_year,m11, m12, m13, d1,m21, m22, m23, d2, re_exam) VALUES (%s, %s)", secure($class_id, 'int'), secure($subject_id, 'int'), secure($child['child_code']), secure($school_year), secure($child['m11']), secure($child['m12']), secure($child['13']), secure($child['d1']), secure($child['m21']), secure($child['m22']), secure($child['m23']), secure($child['d2']), secure($child['reexam']));
//            }
//            /* UPDATE END - ManhDD 01/04/2021 */
//        }
        $strSql = "INSERT INTO ci_point (class_id, subject_id, child_code, school_year,";
        foreach($child as $key => $value) {
            if(in_array(substr($key,0,2),array("hs","gk","ck","re"))) {
                $strSql.= $key .',';
            }
        }
        $strSql =  trim($strSql, ',').') VALUES ('.secure($class_id, 'int').','. secure($subject_id, 'int').','. secure($child['child_code']).','. secure($school_year).',';
        foreach($child as $key => $value) {
            if(in_array(substr($key,0,2),array("hs","gk","ck","re"))) {
                $strSql.= $value=='' ? 'NULL' : secure($value).',';
            }
        }
        $strSql =  trim($strSql, ',') . ') ';
        /* UPDATE END - ManhDD 11/06/2021 */
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        //Trả về ID vừa mới được insert vào DB
        return $db->insert_id;
    }

    /**
     * Lấy dánh sách điểm của 1 trẻ
     *
     * @param $class_id
     * @param $child_code
     * @param $subject_id
     * @param $school_year
     * @param $grade
     * @return array
     * @throws Exception
     */
    public function getChildPoints($class_id, $child_code, $subject_id, $school_year, $grade)
    {
        global $db;

        if ($grade == 1) {
            $strSql = sprintf("SELECT * FROM ci_point_c1 WHERE class_id = %s AND child_code = %s AND subject_id = %s AND school_year = %s", secure($class_id, 'int'), secure($child_code), secure($subject_id, 'int'), secure($school_year));
        } elseif ($grade == 2) {
            /* UPDATE START - ManhDD 01/04/2021 */
//            $strSql = sprintf("SELECT * FROM ci_point_c2 WHERE class_id = %s AND child_code = %s AND subject_id = %s AND school_year = %s", secure($class_id, 'int'), secure($child_code), secure($subject_id, 'int'), secure($school_year));
            $strSql = sprintf("SELECT * FROM ci_point_c2 WHERE class_id = %s AND child_code = %s AND subject_id = %s AND school_year = %s", secure($class_id, 'int'), secure($child_code), secure($subject_id, 'int'), secure($school_year));
            /* UPDATE END - ManhDD 01/04/2021 */
        }

        $get_point = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $points = array();

        if ($get_point->num_rows > 0) {
            $points = $get_point->fetch_assoc();
        }

        return $points;
    }

    /**
     * Chuyển thông tin điểm của trẻ từ lớp cũ sang lớp mới
     *
     * @param $childCode
     * @param $oldClassId
     * @param $newClassId
     * @param $grade
     * @throws Exception
     */
    public function updateMoveClassId($childCode, $oldClassId, $newClassId, $grade)
    {
        global $db;

        if ($grade == 1) {
            $strSql = sprintf("UPDATE ci_point_c1 SET class_id = %s WHERE child_code = %s AND class_id = %s", secure($newClassId, 'int'), secure($childCode), secure($oldClassId, 'int'));
        } elseif ($grade == 2) {
            /* UPDATE START - ManhDD 01/04/2021 */
//            $strSql = sprintf("UPDATE ci_point_c2 SET class_id = %s WHERE child_code = %s AND class_id = %s", secure($newClassId,'int'), secure($childCode), secure($oldClassId, 'int'));
            $strSql = sprintf("UPDATE ci_point_c2 SET class_id = %s WHERE child_code = %s AND class_id = %s", secure($newClassId, 'int'), secure($childCode), secure($oldClassId, 'int'));
            /* UPDATE END - ManhDD 01/04/2021 */
        }

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy id giáo viên dạy môn tương ứng theo lớp và học kỳ
     *
     * @param $schoolYear
     * @param $classId
     * @param $subjectId
     * @param $semester
     * @return |null
     * @throws Exception
     */
    public function getSubjectTeacherId($schoolYear, $classId, $subjectId, $semester)
    {
        global $db;

        $strSql = sprintf("SELECT teacher_id FROM ci_class_subject_teacher WHERE school_year = %s AND class_id = %s AND subject_id = %s AND semester = %s", secure($schoolYear), secure($classId, 'int'), secure($subjectId, 'int'), secure($semester, 'int'));

        $teacher_id = null;
        $get_teacher_id = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        if ($get_teacher_id->num_rows > 0) {
            $teacher_id = $get_teacher_id->fetch_assoc()['teacher_id'];
        }

        return $teacher_id;
    }

    /**
     * Kiểm tra xem trẻ đã được tạo điểm chưa
     *
     * @param $subject_id
     * @param $class_id
     * @param $child_code
     * @param $school_year
     * @return bool
     * @throws Exception
     */
    public function checkCreatedPoint($subject_id, $class_id, $child_code, $school_year)
    {
        global $db;
        //UPDATE START MANHDD 09/06/2021
//        if ($grade == 1) {
//            $strSql = sprintf("SELECT * FROM ci_point_c1 WHERE school_year = %s AND class_id = %s AND subject_id = %s AND child_code = %s", secure($school_year), secure($class_id, 'int'), secure($subject_id, 'int'), secure($child_code));
//        } elseif ($grade == 2) {
//            /* UPDATE START - ManhDD 01/04/2021 */
////            $strSql = sprintf("SELECT * FROM ci_point_c2 WHERE school_year = %s AND class_id = %s AND subject_id = %s AND child_code = %s", secure($school_year), secure($class_id, 'int'), secure($subject_id, 'int'), secure($child_code));
//            $strSql = sprintf("SELECT * FROM ci_point_c2 WHERE school_year = %s AND class_id = %s AND subject_id = %s AND child_code = %s", secure($school_year), secure($class_id, 'int'), secure($subject_id, 'int'), secure($child_code));
//            /* UPDATE END - ManhDD 01/04/2021 */
//        }
            $strSql = sprintf("SELECT * FROM ci_point WHERE school_year = %s AND class_id = %s AND subject_id = %s AND child_code = %s", secure($school_year), secure($class_id, 'int'), secure($subject_id, 'int'), secure($child_code));
        //UPDATE END MANHDD 09/06/2021
        $is_create_point = false;
        $get_point = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        if ($get_point->num_rows > 0) {
            $is_create_point = true;
        }

        return $is_create_point;
    }

    /**
     * Lấy chi tiết comment của trẻ
     *
     * @param $childId
     * @param $classId
     * @param $school_year
     * @param $semester
     * @param $is_last_semester
     * @param $grade
     * @return |null
     * @throws Exception
     */
    public function getChildComment($childId, $classId, $school_year, $semester, $is_last_semester, $grade)
    {
        global $db;

        if ($grade == 1) {
            $strSql = sprintf("SELECT * FROM ci_comment_c1 WHERE school_year = %s AND class_id = %s AND child_id = %s", secure($school_year), secure($classId, 'int'), secure($childId, 'int'));
        } elseif ($grade == 2) {
            $strSql = sprintf("SELECT * FROM ci_comment_c2 WHERE school_year = %s AND class_id = %s AND child_id = %s", secure($school_year), secure($classId, 'int'), secure($childId, 'int'));
        }

        $get_comment = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $comment = null;
        if ($get_comment->num_rows > 0) {
            $comments = $get_comment->fetch_assoc();
            if ($grade == 1) {
                if ($semester == 1) {
                    if ($is_last_semester) {
                        $comment = $comments['cm1ck'];
                    } else {
                        $comment = $comments['cm1gk'];
                    }
                } else {
                    if ($is_last_semester) {
                        $comment = $comments['cm2ck'];
                    } else {
                        $comment = $comments['cm2gk'];
                    }
                }
            } elseif ($grade == 2) {
                if ($semester == 1) {
                    $comment = $comments['cm1'];
                } elseif ($semester == 2) {
                    $comment = $comments['cm2'];
                } else {
                    $comment = $comments['cmcn'];
                }
            }
        }

        return $comment;
    }

    /**
     * Check xem trẻ đã được tạo nhận xét trước đó chưa
     *
     * @param $childId
     * @param $classId
     * @param $school_year
     * @param $grade
     * @return bool
     * @throws Exception
     */
    public function checkChildComment($childId, $classId, $school_year, $grade)
    {
        global $db;

        if ($grade == 1) {
            $strSql = sprintf("SELECT * FROM ci_comment_c1 WHERE school_year = %s AND class_id = %s AND child_id = %s", secure($school_year), secure($classId, 'int'), secure($childId, 'int'));
        } elseif ($grade == 2) {
            $strSql = sprintf("SELECT * FROM ci_comment_c2 WHERE school_year = %s AND class_id = %s AND child_id = %s", secure($school_year), secure($classId, 'int'), secure($childId, 'int'));
        }

        $get_comment = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_comment->num_rows > 0) {
            return true;
        }

        return false;
    }

    /**
     * Tạo mới nhận xét cho trẻ
     *
     * @param $childId
     * @param $classId
     * @param $school_year
     * @param $semester
     * @param $is_last_semester
     * @param $grade
     * @param $comment
     * @throws Exception
     */
    public function createChildComment($childId, $classId, $school_year, $semester, $is_last_semester, $grade, $comment)
    {
        global $db;

        if ($grade == 1) {
            if ($semester == 1) {
                if ($is_last_semester) {
                    $strSql = sprintf("INSERT INTO ci_comment_c1 (class_id, child_id, school_year, cm1ck) VALUES (%s, %s, %s, %s)", secure($classId, 'int'), secure($childId, 'int'), secure($school_year), secure($comment));
                } else {
                    $strSql = sprintf("INSERT INTO ci_comment_c1 (class_id, child_id, school_year, cm1gk) VALUES (%s, %s, %s, %s)", secure($classId, 'int'), secure($childId, 'int'), secure($school_year), secure($comment));
                }
            } else {
                if ($is_last_semester) {
                    $strSql = sprintf("INSERT INTO ci_comment_c1 (class_id, child_id, school_year, cm2ck) VALUES (%s, %s, %s, %s)", secure($classId, 'int'), secure($childId, 'int'), secure($school_year), secure($comment));
                } else {
                    $strSql = sprintf("INSERT INTO ci_comment_c1 (class_id, child_id, school_year, cm2gk) VALUES (%s, %s, %s, %s)", secure($classId, 'int'), secure($childId, 'int'), secure($school_year), secure($comment));
                }
            }
        } elseif ($grade == 2) {
            if ($semester == 1) {
                $strSql = sprintf("INSERT INTO ci_comment_c2 (class_id, child_id, school_year, cm1) VALUES (%s, %s, %s, %s)", secure($classId, 'int'), secure($childId, 'int'), secure($school_year), secure($comment));
            } elseif ($semester == 2) {
                $strSql = sprintf("INSERT INTO ci_comment_c2 (class_id, child_id, school_year, cm2) VALUES (%s, %s, %s, %s)", secure($classId, 'int'), secure($childId, 'int'), secure($school_year), secure($comment));
            } else {
                $strSql = sprintf("INSERT INTO ci_comment_c2 (class_id, child_id, school_year, cmcn) VALUES (%s, %s, %s, %s)", secure($classId, 'int'), secure($childId, 'int'), secure($school_year), secure($comment));
            }
        }

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Sửa thông tin nhận xét trẻ
     *
     * @param $childId
     * @param $classId
     * @param $school_year
     * @param $semester
     * @param $is_last_semester
     * @param $grade
     * @param $comment
     * @throws Exception
     */
    public function updateChildComment($childId, $classId, $school_year, $semester, $is_last_semester, $grade, $comment)
    {
        global $db;

        if ($grade == 1) {
            if ($semester == 1) {
                if ($is_last_semester) {
                    $strSql = sprintf("UPDATE ci_comment_c1 SET cm1ck = %s WHERE class_id = %s AND child_id = %s AND school_year = %s", secure($comment), secure($classId, 'int'), secure($childId, 'int'), secure($school_year));
                } else {
                    $strSql = sprintf("UPDATE ci_comment_c1 SET cm1gk = %s WHERE class_id = %s AND child_id = %s AND school_year = %s", secure($comment), secure($classId, 'int'), secure($childId, 'int'), secure($school_year));
                }
            } else {
                if ($is_last_semester) {
                    $strSql = sprintf("UPDATE ci_comment_c1 SET cm2ck = %s WHERE class_id = %s AND child_id = %s AND school_year = %s", secure($comment), secure($classId, 'int'), secure($childId, 'int'), secure($school_year));
                } else {
                    $strSql = sprintf("UPDATE ci_comment_c1 SET cm2gk = %s WHERE class_id = %s AND child_id = %s AND school_year = %s", secure($comment), secure($classId, 'int'), secure($childId, 'int'), secure($school_year));
                }
            }
        } elseif ($grade == 2) {
            if ($semester == 1) {
                $strSql = sprintf("UPDATE ci_comment_c2 SET cm1 = %s WHERE class_id = %s AND child_id = %s AND school_year = %s", secure($comment), secure($classId, 'int'), secure($childId, 'int'), secure($school_year));
            } elseif ($semester == 2) {
                $strSql = sprintf("UPDATE ci_comment_c2 SET cm2 = %s WHERE class_id = %s AND child_id = %s AND school_year = %s", secure($comment), secure($classId, 'int'), secure($childId, 'int'), secure($school_year));
            } else {
                $strSql = sprintf("UPDATE ci_comment_c2 SET cmcn = %s WHERE class_id = %s AND child_id = %s AND school_year = %s", secure($comment), secure($classId, 'int'), secure($childId, 'int'), secure($school_year));
            }
        }

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }
}