<?php
/**
 * DAO -> BackgroundNotification
 * Thao tác với bảng ci_background_notifications
 * 
 * @package ConIu
 * @author Tuan Anh
 */

class SendNotificationDAO {
    /**
     * __construct
     *
     */
    public function __construct() {
    }

    /**
     * Lấy ra danh sách notification chưa gửi sắp xếp theo thứ tự tăng dần ID
     *
     * @return array
     */
    public function getUnsentNotifications() {
        global $db;

        $unsentNotifications = array();
        $get_notifications = $db->query("SELECT * FROM ci_background_notifications ORDER BY send_notification_id ASC LIMIT 100") or _error(SQL_ERROR_THROWEN);
        if($get_notifications->num_rows > 0) {
            while($usentNotification = $get_notifications->fetch_assoc()) {
                $unsentNotifications[] = $usentNotification;
            }
        }
        return $unsentNotifications;
    }

    /**
     * Xóa nhiều notifications cùng một lúc
     *
     * @param $sendNotificationIds
     * @throws Exception
     */
    public function deleteSomeNotifications($sendNotificationIds) {
        global $db;

        if (count($sendNotificationIds) == 0) return;

        $strCon = implode(',', $sendNotificationIds);
        $strCon = trim($strCon, ",");
        $db->query(sprintf("DELETE FROM ci_background_notifications WHERE send_notification_id IN (%s)", $strCon)) or _error(SQL_ERROR_THROWEN);
    }

}
?>