<?php
/**
 * DAO -> User
 * Thao tác với bảng Users
 * 
 * @package ConIu
 * @author QuanND
 */

class UserDAO {
    /**
     * __construct
     *
     */
    public function __construct() {
    }

    /**
     * Tìm user trên hệ thống.
     *
     * @param $query
     * @return array
     * @throws Exception
     */
    public function searchUser($query, $userIds)
    {
        global $db, $system;

        $strCon = implode(',', $userIds);

        $results = array();
        /* search users */
        $strSql = sprintf('SELECT user_id, user_name, user_phone, user_fullname, user_gender, user_picture, user_email FROM users
                            WHERE (user_name LIKE %1$s OR user_email LIKE %1$s OR user_firstname LIKE %1$s OR user_lastname LIKE %1$s) AND user_id NOT IN (%2$s) AND user_group != 1
                            ORDER BY user_firstname ASC LIMIT %3$s', secure($query, 'search'), $strCon, secure($system['min_results'], 'int', false));

        $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        if ($get_users->num_rows > 0) {
            while ($user = $get_users->fetch_assoc()) {
                $user['user_picture'] = get_picture($user['user_picture'], $user['user_gender']);
                /* get the connection between the viewer & the target */
                //$user['sort'] = $user['user_fullname'];
                //$user['type'] = 'user';
                $results[] = $user;
            }
        }

        return $results;
    }

    /**
     * Lấy ra danh sách user theo một danh sách ID đã biết.
     *
     * @param $user_ids
     * @return array
     * @throws Exception
     */
    public function getUsers($user_ids)
    {
        global $db;
        $results = array();
        if (count($user_ids) == 0) {
            return $results;
        }

        $user_ids = array_filter($user_ids);
        $strCon = implode(',', $user_ids);
        $strSql = sprintf('SELECT user_id, user_name, user_email, user_phone, user_fullname, user_gender, user_picture FROM users WHERE user_id IN (%s)', $strCon);
        $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_users->num_rows > 0) {
            while ($user = $get_users->fetch_assoc()) {
                $user['user_picture'] = get_picture($user['user_picture'], $user['user_gender']);
                /* get the connection between the viewer & the target */
                //$user['sort'] = $user['user_fullname'];
                //$user['type'] = 'user';
                $results[] = $user;
            }
        }

        return $results;
    }

    /**
     * Lấy ra danh sách user theo một danh sách ID đã biết. (dùng trong noga- user online)
     *
     * @param $user_ids
     * @return array
     * @throws Exception
     */
    public function getUsersForUserOnline($user_ids)
    {
        global $db;
        $results = array();
        if (count($user_ids) == 0) {
            return $results;
        }

        $user_ids = array_filter($user_ids);
        $strCon = implode(',', $user_ids);
        $strSql = sprintf('SELECT user_id, user_name, user_email, user_phone, user_fullname, user_gender, user_picture FROM users WHERE user_id IN (%s)', $strCon);
        $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_users->num_rows > 0) {
            while ($user = $get_users->fetch_assoc()) {
                $user['user_picture'] = get_picture($user['user_picture'], $user['user_gender']);
                $objects = getRelatedObjectsOnUserId($user['user_id']);
                // Lấy những trường đang sử dụng coniu
                $schoolUsing = array();
                foreach ($objects['schools'] as $school) {
                    if($school['school_status'] == SCHOOL_USING_CONIU) {
                        $schoolUsing[] = $school;
                    }
                }
                //$user['schools'] = $schoolUsing;
                $classUsing = array();
                foreach ($objects['classes'] as $class) {
                    if($class['school_status'] == SCHOOL_USING_CONIU) {
                        $classUsing[] = $class;
                    }
                }

                //$user['classes'] = $classUsing;
                $children = $this->__getChildrenOfParent($user['user_id']);

                //$user['children'] = $children;
                $user['of_school'] = 0;
                if(count($schoolUsing) > 0 || count($classUsing) > 0) {
                    $user['of_school'] = 1;
                } else {
                    if(count($children) > 0) {
                        foreach ($children as $child) {
                            if($child['school_id'] > 0) {
                                $user['of_school'] = 1;
                                break;
                            }
                        }
                    }
                }
                // ConIu - END
                /* get the connection between the viewer & the target */
                //$user['sort'] = $user['user_fullname'];
                //$user['type'] = 'user';
                $results[] = $user;
            }
        }

        return $results;
    }

    /**
     * Lấy ra danh sách trẻ được quản lý bởi một user
     *
     * @param $user_id
     * @return array
     * @throws Exception
     */
    private function __getChildrenOfParent($user_id) {
        global $db, $system;


        $children = array();
        $strSql = sprintf("SELECT PM.*, CP.*, SC.school_status FROM ci_parent_manage PM
                    INNER JOIN ci_child_parent CP ON CP.child_parent_id = PM.child_parent_id AND PM.user_id = %s
                    LEFT JOIN ci_school_configuration SC ON SC.school_id = PM.school_id
                    ORDER BY CP.name_for_sort ASC", secure($user_id, 'int'));
        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_children->num_rows > 0) {
            while($child = $get_children->fetch_assoc()) {
                if(!is_null($child['birthday'])) {
                    $child['birthday'] = toSysDate($child['birthday']);
                }
                if(!is_null($child['due_date_of_childbearing'])) {
                    $child['due_date_of_childbearing'] = toSysDate($child['due_date_of_childbearing']);
                }
                if (!is_empty($child['child_picture'])) {
                    $child['child_picture_path'] = $child['child_picture'];
                    $child['child_picture'] = $system['system_uploads'] . '/' . $child['child_picture'];
                }
                $date_now = date("Y-m-d");
                if($child['foetus_begin_date'] != null && $child['foetus_begin_date'] != '0000-00-00 00:00:00') {
                    $child['pregnant_week'] = (strtotime($date_now) - strtotime($child['foetus_begin_date'])) / (60 * 60 * 24 * 7);
                    $child['pregnant_week'] = (int)$child['pregnant_week'];
                } else {
                    $child['pregnant_week'] = null;
                }
                $children[] = $child;
            }
        }
        return $children;
    }

    /**
     * Lấy ra danh sách user theo một danh sách ID đã biết. (full)
     *
     * @param $user_ids
     * @return array
     * @throws Exception
     */
    public function getUsersFullDetail($user_ids)
    {
        global $db;
        $results = array();
        if (count($user_ids) == 0) {
            return $results;
        }

        $strCon = implode(',', $user_ids);
        $strSql = sprintf('SELECT * FROM users WHERE user_id IN (%s)', $strCon);
        $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_users->num_rows > 0) {
            while ($user = $get_users->fetch_assoc()) {
                $user['user_picture'] = get_picture($user['user_picture'], $user['user_gender']);
                /* get the connection between the viewer & the target */
                //$user['sort'] = $user['user_fullname'];
                //$user['type'] = 'user';
                $user['user_birthdate'] = toSysDate($user['user_birthdate']);
                $results[] = $user;
            }
        }

        return $results;
    }

    /**
     * Lấy ra danh sách các đối tượng quản lý của một user
     *
     * @param $userId
     * @return array
     * @throws Exception
     */
    public function getNogaManage($userId)
    {
        global $db;
        $strSql = sprintf('SELECT * FROM ci_noga_manage WHERE user_id = %s', secure($userId, 'int'));
        $getNogaManages = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $nogaManages = array();
        if ($getNogaManages->num_rows > 0) {
            while ($nogaManage = $getNogaManages->fetch_assoc()) {
                $nogaManages[] = $nogaManage;
            }
        }

        return $nogaManages;
    }

    /**
     * Lưu thông tin quản lý của nhân viên NOGA
     *
     * @param $userId
     * @param $roleId
     * @param $manageType
     * @param int $schoolId
     * @param int $cityId
     * @throws Exception
     */
    public function insertNogaManage($userId, $roleId, $manageType, $schoolId = 0, $cityId = 0) {
        global $db;

        $strSql = sprintf("INSERT INTO ci_noga_manage (user_id, role_id, manage_type, school_id, city_id) VALUES (%s, %s, %s, %s, %s)",
            secure($userId, 'int'), secure($roleId, 'int'), secure($manageType, 'int'), secure($schoolId, 'int'), secure($cityId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy ra thông tin user theo username
     *
     * @param $username
     * @return array|null
     * @throws Exception
     */
    public function getUserByUsername($username)
    {
        global $db;

        $username = isset($username)? strtolower(trim($username)): "";
        /* search users */
        $strSql = sprintf('SELECT user_id, user_name, user_fullname, user_gender, user_picture FROM users WHERE user_name = %s', secure($username));
        $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_users->num_rows > 0) {
            $user = $get_users->fetch_assoc();
            $user['user_picture'] = get_picture($user['user_picture'], $user['user_gender']);

            return $user;
        }

        return null;
    }

    /**
     * Lấy thông tin user theo user_id
     *
     * @param $user_id
     * @return array|null
     * @throws Exception
     */
    public function getUserByUserId($user_id)
    {
        global $db;
        if(!is_numeric($user_id) || $user_id == 0){
            return null;
        }
        /* search users */
        $strSql = sprintf('SELECT user_id, user_name, user_fullname, user_gender, user_picture FROM users WHERE user_id = %s', secure($user_id));
        $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_users->num_rows > 0) {
            $user = $get_users->fetch_assoc();
            $user['user_picture'] = get_picture($user['user_picture'], $user['user_gender']);

            return $user;
        }

        return null;
    }

    /**
     * Lấy ra thông tin user theo username (phụ huynh tạo trẻ)
     *
     * @param $username
     * @return array|null
     * @throws Exception
     */
    public function getUserAllByUsername($username)
    {
        global $db;

        $username = isset($username)? strtolower(trim($username)): "";
        /* search users */
        $strSql = sprintf('SELECT user_id, user_name, user_fullname, user_gender, user_picture, user_phone, user_email FROM users WHERE user_name = %s', secure($username));
        $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_users->num_rows > 0) {
            $user = $get_users->fetch_assoc();
            $user['user_picture'] = get_picture($user['user_picture'], $user['user_gender']);

            return $user;
        }

        return null;
    }

    /**
     * Thiết lập 2 danh sách user thành friend của nhau. Trong đó danh sách 02 sẽ following danh sách 01.
     *
     * @param $ids1
     * @param $ids2
     * @throws Exception
     */
    public function becomeFriends ($ids1, $ids2) {
        foreach ($ids1 as $id1) {
            foreach ($ids2 as $id2) {
                $this->becomeFriend($id1, $id2);
            }
        }
    }

    /**
     * Thiết lập để danh sách user thành bạn của nhau.
     *
     * @param $ids
     */
    public function becomeFriendTogether ($ids) {
        if (count($ids) < 2) return;

        for ($i = 0; $i < count($ids) - 1; $i++) {
            for ($j = $i + 1; $j < count($ids); $j++) {
                $this->becomeFriend($ids[$i], $ids[$j]);
            }
        }
    }

    /**
     * Thiết lập để 2 user thành bạn của nhau. Trong đó user có ID2 sẽ following user ID1.
     *
     * @param $id1
     * @param $id2
     * @throws Exception
     */
    public function becomeFriend ($id1, $id2) {
        global $db, $user, $system;

        if ($id1 == $id2) return; //Không là bạn của chính mình

        $strSql = sprintf('SELECT * FROM friends WHERE (user_one_id = %1$s AND user_two_id = %2$s) OR (user_one_id = %2$s AND user_two_id = %1$s)',
            secure($id1, 'int'), secure($id2, 'int'));
        $check = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        if($check->num_rows == 0) {
            $strSql = sprintf("INSERT INTO friends (status, user_one_id, user_two_id) VALUES (%s, %s, %s)", 1, secure($id1, 'int'), secure($id2, 'int'));
        } else {
            $friend = $check->fetch_assoc();
            $strSql = sprintf("UPDATE friends SET status = 1 WHERE user_one_id = %s AND user_two_id = %s",
                secure($friend['user_one_id'], 'int'), secure($friend['user_two_id'], 'int'));
        }
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        /* Follow 2 chiều */
        $this->_follow($id1, $id2);
        $this->_follow($id2, $id1);

        $args = array(
            'user_id' => $user->_data['user_id'],
            'action' => ADD_FRIEND,
            'user_one_id' => $id1,
            'user_two_id' => $id2
        );
        insertBackgroundFirebase($args);
        /* END-CI */
    }

    /**
     * Xóa quan hệ bạn bè của một người.
     *
     * @param $user_id
     */
    public function removeFriend($user_id) {
        global $db;

        $db->query(sprintf('DELETE FROM friends WHERE (user_one_id = %1$s AND status = 1) OR (user_two_id = %1$s AND status = 1)', secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Thiết lập user ID2 following user ID1
     *
     * @param $id1
     * @param $id2
     * @throws Exception
     */
    private function _follow($id1, $id2) {
        global $db;
        $check = $db->query(sprintf("SELECT * FROM followings WHERE user_id = %s AND following_id = %s", secure($id1, 'int'),  secure($id2, 'int'))) or _error(SQL_ERROR_THROWEN);
        if($check->num_rows > 0) return;
        $db->query(sprintf("INSERT INTO followings (user_id, following_id) VALUES (%s, %s)", secure($id1, 'int'),  secure($id2, 'int') )) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Gửi thông báo cho một user
     *
     * @param $to_user_id
     * @param $action
     * @param string $node_type
     * @param string $node_url
     * @param string $extra1
     * @param string $extra2
     * @param string $extra3
     * @param int $from_user_id
     * @throws Exception
     */
    public function postNotification($to_user_id, $action, $node_type = '', $node_url = '', $extra1 = '', $extra2 = '', $extra3 = '', $from_user_id = 0) {
        global $db, $date, $user;
        /* if the viewer is the target */
        $fromUserId = $from_user_id;
        if ($from_user_id == 0) {
            $fromUserId = $user->_data['user_id'];
        }

        if($fromUserId == $to_user_id) {
            return;
        }

        //Nếu đã có thông báo trước đó, mà người được thông báo chưa xem thì không cần thông báo lại
        // Bỏ đoạn check này
        /*$strSql = sprintf("SELECT notification_id FROM notifications WHERE to_user_id = %s
                            AND from_user_id = %s AND action = %s AND node_type = %s AND node_url = %s AND seen = '0' ",
            secure($to_user_id, 'int'), secure($fromUserId, 'int'), secure($action), secure($node_type), secure($node_url));
        $check = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($check->num_rows > 0) return;*/

        $strSql = sprintf("INSERT INTO notifications (to_user_id, from_user_id, action, node_type, node_url, time, extra1, extra2, extra3)
                            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
                            secure($to_user_id, 'int'), secure($fromUserId, 'int'), secure($action), secure($node_type),
                            secure($node_url), secure($date), secure($extra1), secure($extra2), secure($extra3));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $strSql = sprintf("UPDATE users SET user_live_notifications_counter = user_live_notifications_counter + 1 WHERE user_id = %s", secure($to_user_id, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }


    /**
     * Lấy ra thông tin của 1 notification của user hiện tại.
     * LƯU Ý: ĐÂY LÀ NOTIFICATION CỦA HỆ THỐNG CŨ
     *
     * @param $notification_id
     * @return array|null
     * @throws Exception
     */
    public function getSNotification($notification_id) {
        global $db, $user;
        $strSql = sprintf("SELECT * FROM notifications WHERE to_user_id = %s AND notification_id = %s", secure($user->_data['user_id'], 'int'), secure($notification_id, 'int'));
        $get_notification = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        if ($get_notification->num_rows > 0) {
            return $get_notification->fetch_assoc();
        }

        return null;
    }

    /**
     * Gửi thông báo cho danh sách userIds
     *
     * @param $toUserIds
     * @param $action
     * @param string $node_type
     * @param string $node_url
     * @param string $extra1
     * @param string $extra2
     * @param string $extra3
     * @param int $from_user_id
     * @throws Exception
     */
    public function postNotifications($toUserIds, $action, $node_type = '', $node_url = '', $extra1 = '', $extra2 = '', $extra3 = '', $from_user_id = 0) {

        if (is_numeric($toUserIds)) {
            $toUserIds = array($toUserIds);
        }
        if (!empty($toUserIds)) {
            $toUserIds = array_unique($toUserIds); //Bỏ trùng lặp
            foreach ($toUserIds as $userId) {
                $this->postNotification($userId, $action, $node_type, $node_url, $extra1, $extra2, $extra3, $from_user_id);

                $this->insertBackgroundNotification($userId, $action, $node_type, $node_url, $extra1, $extra2, $extra3, $from_user_id);
            }
        }

    }

    /**
     * Hàm gửi thông bảo bởi ADMIN hệ thống
     *
     * @param $toUserIds
     * @param $action
     * @param string $node_type
     * @param string $node_url
     * @param $from_user_id
     * @param $child_id
     * @throws Exception
     */
    public function postSystemNotifications($toUserIds, $action, $node_type, $node_url, $from_user_id, $child_id) {
        $toUserIds = array_unique($toUserIds); //Bỏ trùng lặp
        foreach ($toUserIds as $userId) {
            $this->postNotification($userId, $action, $node_type, $node_url, '', $child_id, '', $from_user_id);

            $this->insertBackgroundNotification($userId, $action, $node_type, $node_url, '', $child_id, '', $from_user_id);
        }
    }


    /**
     * Hàm insert thông báo vào db để chạy ngầm gửi thông báo
     *
     * @param $toUserIds
     * @param $action
     * @param string $node_type
     * @param string $node_url
     * @param string $extra1
     * @param string $extra2
     * @param string $extra3
    */
    public function insertBackgroundNotification($toUserId, $action, $node_type = '', $node_url = '', $extra1 = '', $extra2 = '', $extra3 = '', $from_user_id = 0) {
        global $db, $user, $date;

        $fromUserId = $from_user_id;
        if ($from_user_id == 0) {
            $fromUserId = $user->_data['user_id'];
        }

        if($fromUserId == $toUserId) {
            return;
        }

        if ($toUserId) {
            /* get device tokens */
            $getDeviceTokens = $db->query(sprintf("SELECT device_token FROM users_sessions          
              WHERE user_id = %s AND device_token IS NOT NULL AND device_token != '' ", secure($toUserId, 'int') )) or _error(SQL_ERROR_THROWEN);

            $deviceTokens = array();
            if ($getDeviceTokens->num_rows > 0) {
                while($userInfo = $getDeviceTokens->fetch_assoc()) {
                    $deviceTokens[] = $userInfo['device_token'];
                }
            }

            $notification['time'] = $date;
            $notification['type'] = 'notification';

            $notification['to_user_id'] = $toUserId;
            $notification['count_notifications'] = 1;

            /* get notifications_count */
            $getCntNotification = $db->query(sprintf("SELECT user_live_notifications_counter FROM users
                                                      WHERE user_id = %s", secure($notification['to_user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            if ($getCntNotification->num_rows > 0) {
                $notification['count_notifications'] = $getCntNotification->fetch_assoc()['user_live_notifications_counter'] + 1;
            }

            $notification['from_user_id'] = $fromUserId;
            $notification['action'] = $action;
            $notification['node_type'] = $node_type;
            $notification['node_url'] = $node_url;
            $notification['extra1'] = convertText4Web($extra1);
            $notification['extra2'] = convertText4Web($extra2);
            $notification['extra3'] = convertText4Web($extra3);

            $notification['user_name'] = $user->_data['user_name'];
            $notification['user_fullname'] = convertText4Web($user->_data['user_fullname']);
            $notification['user_picture'] = $user->_data['user_picture'];

            $notification['message'] = convertText4Web(getNotificationMessage($notification));

            /* parse notification */
            switch ($notification['action']) {
                case NOTIFICATION_CHILD_DEVELOPMENT:
                    $notification['user_fullname'] = 'Coniu';
                    $notification['user_picture'] = $notification['extra3'];
                    break;

                case NOTIFICATION_FOETUS_DEVELOPMENT:
                    $notification['user_fullname'] = 'Coniu';
                    $notification['user_picture'] = $notification['extra3'];
                    break;
            }

            foreach ($deviceTokens as $deviceToken) {
                $strSql = sprintf("INSERT INTO ci_background_notifications (type, count_notifications, message, to_user_id, from_user_id, 
                action, node_type, node_url, time, extra1, extra2, extra3, user_name, user_fullname, user_picture, device_token)
                            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                    secure($notification['type']), secure($notification['count_notifications'], 'int'), secure($notification['message']), secure($notification['to_user_id'], 'int'), secure($notification['from_user_id'], 'int'), secure($notification['action']), secure($notification['node_type']),
                    secure($notification['node_url']), secure($notification['time']), secure($notification['extra1']), secure($notification['extra2']), secure($notification['extra3']), secure($notification['user_name']), secure($notification['user_fullname']),
                    secure($notification['user_picture']), secure($deviceToken) );

                $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            }
        }

    }

    /**
     * Xóa notification của một user
     *
     * @param integer $to_user_id
     * @param string $action
     * @return void
     */
    public function deleteNotification($to_user_id, $action, $node_type = '', $node_url = '') {
        global $db, $user;

        $strSql = sprintf("DELETE FROM notifications WHERE to_user_id = %s AND from_user_id = %s AND action = %s AND node_type = %s AND node_url = %s",
            secure($to_user_id, 'int'), secure($user->_data['user_id'], 'int'), secure($action), secure($node_type), secure($node_url));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $strSql = sprintf("UPDATE users SET user_live_notifications_counter = IF(user_live_notifications_counter=0,0,user_live_notifications_counter-1) WHERE user_id = %s", secure($to_user_id, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Xóa thông báo của danh sách users
     *
     * @param $from_user_id
     * @param $toUserIds
     * @param $action
     * @param string $node_type
     * @param string $node_url
     */
    public function deleteNotifications($toUserIds, $action, $node_type = '', $node_url = '') {
        $toUserIds = array_unique($toUserIds); //Bỏ trùng lặp
        foreach ($toUserIds as $userId) {
            $this->deleteNotification($userId, $action, $node_type, $node_url);
        }
    }

    /**
     * Post một bài viết lên trang
     *
     * @param $page
     * @param $content
     * @return int|mixed
     * @throws Exception
     */
    public function postOnPage($page, $content) {
        global $db, $system, $date, $user;

        $post = array();

        $post['in_group'] = '0';
        $post['group_id'] = null;

        $post['user_id'] = $page['page_id'];
        $post['user_type'] = "page";

        $post['post_picture'] = get_picture($page['page_picture'], "page");
        $post['post_author_url'] = $system['system_url'].'/pages/'.$page['page_name'];
        $post['post_author_name'] = $page['page_title'];
        $post['post_author_verified'] = $user->_data['page_verified'];
        $post['post_type'] = 'ci_event';
        $post['text'] = $content;
        $post['time'] = $date;
        $post['privacy'] = 'public';

        $strSql = sprintf("INSERT INTO posts (user_id, user_type, in_group, group_id, post_type, time, privacy, text, school_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
            secure($post['user_id'], 'int'), secure($post['user_type']), secure($post['in_group'], 'int'), secure($post['group_id'], 'int'), secure($post['post_type']),
            secure($post['time']), secure($post['privacy']), secure($post['text']), secure($page['page_id'], 'int'));

        /* insert the post */
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        return $db->insert_id;
    }

    /**
     * Post bài viết lên một group
     *
     * @param $group
     * @param $content
     * @param $school_id
     * @return mixed
     * @throws Exception
     */
    public function postOnGroup($group, $content, $school_id) {
        global $db, $system, $date, $user;

        $post = array();
        $post['user_id'] = $user->_data['user_id'];
        $post['user_type'] = "user";

        $post['post_picture'] = $user->_data['user_picture'];
        $post['post_author_url'] = $system['system_url'].'/'.$user->_data['user_name'];
        $post['post_author_name'] = $user->_data['user_fullname'];
        $post['post_author_verified'] = $user->_data['user_verified'];

        $post['in_group'] = '1';
        $post['group_id'] = $group['group_id'];

        $post['post_type'] = 'ci_event';
        $post['text'] = $content;
        $post['time'] = $date;
        $post['location'] = '';
        $post['privacy'] = 'public';
        $post['school_id'] = $school_id;

        $strSql = sprintf("INSERT INTO posts (user_id, user_type, in_group, group_id, post_type, time, privacy, text, school_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
            secure($post['user_id'], 'int'), secure($post['user_type']), secure($post['in_group'], 'int'),
            secure($post['group_id'], 'int'), secure($post['post_type']), secure($post['time']),
            secure($post['privacy']), secure($post['text']), secure($post['school_id'], 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        return $db->insert_id;
    }

    /**
     * Post bài viết lên một group
     *
     * @param $group
     * @param $content
     * @param $school_id
     * @throws Exception
     */
    public function editPostOnGroup($group, $content, $school_id, $post_ids) {
        global $db, $system, $date, $user;

        $post = array();
        $post['user_id'] = $user->_data['user_id'];
        $post['user_type'] = "user";

        $post['post_picture'] = $user->_data['user_picture'];
        $post['post_author_url'] = $system['system_url'].'/'.$user->_data['user_name'];
        $post['post_author_name'] = $user->_data['user_fullname'];
        $post['post_author_verified'] = $user->_data['user_verified'];

        $post['in_group'] = '1';
        $post['group_id'] = $group['group_id'];

        $post['post_type'] = 'ci_event';
        $post['text'] = $content;
        $post['time'] = $date;
        $post['location'] = '';
        $post['privacy'] = 'public';
        $post['school_id'] = $school_id;

        $postIds = explode(",", $post_ids);
        foreach ($postIds as $post_id) {
            $strSql = sprintf("UPDATE posts SET user_id = %s, user_type = %s, in_group = %s, group_id = %s, post_type = %s, time = %s, privacy = %s, text = %s, school_id = %s
                           WHERE post_id = %s", secure($post['user_id'], 'int'), secure($post['user_type']), secure($post['in_group'], 'int'),
                secure($post['group_id'], 'int'), secure($post['post_type']), secure($post['time']),
                secure($post['privacy']), secure($post['text']), secure($post['school_id'], 'int'), secure($post_id, 'int'));

            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * Kiểm tra username đã tồn tại trong hệ thống chưa
     *
     * @param $username
     * @return bool
     * @throws Exception
     */
    private function check_username($username) {
        global $db;
        $query = $db->query(sprintf("SELECT user_id FROM users WHERE user_name = %s", secure($username))) or _error(SQL_ERROR_THROWEN);
        if($query->num_rows > 0) {
            return true;
        }
        return false;
    }

    /**
     * check_phone
     *
     * @param string $phone
     * @param boolean $return_info
     * @return boolean|array
     *
     */
    public function check_phone($phone, $return_info = false) {
        global $db;

        $arrPhone = getArrayPhone($phone);
        $strCon = implode(',', $arrPhone);
        $strCon = trim($strCon, ",");

        $query = $db->query(sprintf("SELECT * FROM users WHERE user_phone_signin IN (%s)", $strCon )) or _error(SQL_ERROR_THROWEN);
        if($query->num_rows > 0) {
            if($return_info) {
                $info = $query->fetch_assoc();
                return $info;
            }
            return true;
        }
        return false;
    }

    /**
     * Kiểm tra email đã tồn tại trong hệ thống chưa
     *
     * @param $email
     * @return bool
     * @throws Exception
     */
    private function check_email($email) {
        global $db;
        $query = $db->query(sprintf("SELECT user_id FROM users WHERE user_email = %s", secure($email) )) or _error(SQL_ERROR_THROWEN);
        if($query->num_rows > 0) {
            return true;
        }
        return false;
    }

    /**
     * Tạo thông tin tài khoản user
     *
     * @param array $args
     * @return mixed
     * @throws Exception
     */
    public function createUser(array $args = array()) {
        global $db, $user, $date;
        $this->validateInput($args);
        /* generate activation_key */
        $activation_key = get_hash_token();
        $full_name = ucwords($args['full_name']);
        /* create user */
        $strSql = sprintf("INSERT INTO users (user_name, user_email, user_phone, user_password, user_fullname, user_firstname, user_lastname, user_gender, user_registered, user_activation_key, user_activated, user_started, created_id)
                            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, '1', '1', %s)",
            secure($args['username']), secure($args['email']), secure($args['user_phone']), secure(_password_hash($args['password'])),  secure($full_name), secure(ucwords($args['first_name'])),  secure(ucwords($args['last_name'])),
            secure($args['gender']), secure($date), secure($activation_key), secure($user->_data['user_id'], 'int' ));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        /* get user_id */
        $user_id = $db->insert_id;

        if($args['email'] == 'null') {
            $args['email'] = '';
        }
        /* CI - Firebase */
        $arr = array(
            'user_id' => $user_id,
            'action' => CREATE_USER,
            'user_email' => $args['email'],
            'user_fullname' => $full_name,
            'user_name' => $args['username'],
            'user_picture' => get_picture('', $args['gender']),
            'is_online' => 0,
            'count_chat' => 0,
            'last_login' => date(DB_DATETIME_FORMAT_DEFAULT),
            'provider' => 'Coniu'
        );
        insertBackgroundFirebase($arr);
        /* END-CI */
        return $user_id;
    }

    /**
     * Cập nhật thông tin user
     *
     * @param array $args
     * @throws Exception
     */
    public function editUser(array $args = array()) {
        global $system, $db;
        $this->validateInput ($args, false);

        if (strlen($args['password']) > 0) {
            $strSql = sprintf("UPDATE users SET user_phone = %s, user_email = %s, user_password = %s, user_fullname = %s, user_gender = %s
                           WHERE user_id = %s", secure($args['user_phone']), secure($args['email']), secure(_password_hash($args['password'])),
                secure(ucwords($args['full_name'])), secure($args['gender']), secure($args['user_id'], 'int'));
        } else {
            $strSql = sprintf("UPDATE users SET user_phone = %s, user_email = %s, user_fullname = %s, user_gender = %s
                           WHERE user_id = %s", secure($args['user_phone']), secure($args['email']),
                secure(ucwords($args['full_name'])), secure($args['gender']), secure($args['user_id'], 'int'));
        }
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        /* CI - Firebase */
        $arr = array(
            'user_id' => $args['user_id'],
            'action' => UPDATE_USER,
            'user_fullname' => ucwords($args['full_name']),
            'user_email'   => $args['email']
        );
        insertBackgroundFirebase($arr);
        /* END-CI */
    }

    /**
     * Kiểm tra đầu vào cho hàm insert và update
     *
     * @param array $args
     * @param bool|true $isCreate
     * @throws Exception
     */
    private function validateInput (array $args = array(), $isCreate = true) {
        /* validate title */
        error_log('dao_user validateInput() 939');
        if(is_empty($args['full_name'])) {
            throw new Exception(__("You must fill in all of the fields"));
        }
        error_log('dao_user validateInput() 943 ');
        if($isCreate) {
            if (!valid_email($args['email']) && $args['email'] != 'null') {
                throw new Exception(__("Please enter a valid email address or let it empty"));
            }
            if ($this->check_email($args['email']) && $args['email'] != 'null') {
                throw new Exception(__("Sorry, it looks like") ." ". $args['email'] ." ". __("belongs to an existing account"));
            }
        } else {
            if (!valid_email($args['email'])) {
                throw new Exception(__("Please enter a valid email address"));
            }

            if ($args['email'] != $args['email_old']) {
                if ($this->check_email($args['email'])) {
                    throw new Exception(__("Sorry, it looks like") ." ". $args['email'] ." ". __("belongs to an existing account"));
                }
            }

        }
        error_log('dao_user validateInput() 963');
        if ($isCreate && (strlen($args['password']) < 6)) {
            throw new Exception(__("Your password must be at least 6 characters long. Please try another"));
        }
        error_log('dao_user validateInput() 967');
        /* validate fullname */
        if(!valid_name($args['full_name'])) {
            throw new Exception(__("Your name contains invalid characters"));
        }
        error_log('dao_user validateInput() 972');
        if(!in_array($args['gender'], array('male', 'female'))) {
            throw new Exception(__("Please select a valid gender"));
        }
        error_log('dao_user validateInput() 976');
//        if (!valid_phone($args['user_phone'])) {
//            throw new Exception(__("Please enter a valid mobile number"));
//        }

    }

    /**
     * Cho user like nhiều trang của hệ thống
     *
     * @param $pageId
     * @param $userIds
     */
    public function addUsersLikeSomePages($pageIds, $userIds) {
        if (count($pageIds) > 0) {
            foreach ($pageIds as $pageId) {
                $this->addUsersLikePage($pageId, $userIds);
            }
        }
    }

    /**
     * Nhập danh sách user like trang
     *
     * @param $pageId
     * @param $userIds
     * @throws Exception
     */
    public function addUsersLikePage($pageId, $userIds) {
        global $db;

        $userIds = array_unique($userIds); //Bỏ đi giá trị bị lặp
        $likedUserIds = $this->_getLikedUserId($pageId, $userIds);

        //Bỏ đi những user đã like page rồi
        $userIds = array_diff($userIds, $likedUserIds);

        if (count($userIds) == 0) return;

        //1. Đưa danh sách user vào nhóm
        //Câu sql sẽ có dạng: INSERT INTO tbl_name (col_A,col_B,col_C) VALUES (1,2,3), (4,5,6), (7,8,9)
        //Build lên phần giá trị truyền vào.
        $strValues = "";
        foreach ($userIds as $id) {
            //page_id, user_id
            $strValues .= "(". secure($pageId, 'int') ."," . secure($id, 'int') ."),";
        }
        $strValues = trim($strValues, ",");

        $strSql = "INSERT INTO pages_likes (page_id, user_id) VALUES ".$strValues;
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        //2. Tăng số thành viên like page
        $strSql = sprintf("UPDATE pages SET page_likes = page_likes + %s WHERE page_id = %s", count($userIds), $pageId);
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy ra danh sách ID của user đã like page từ một tập ID cho trước
     *
     * @param $pageId
     * @param $userIds
     * @return array
     * @throws Exception
     */
    private function _getLikedUserId($pageId, $userIds) {
        global $db;

        $strCon = implode(',', $userIds);
        $strSql = sprintf("SELECT user_id FROM pages_likes WHERE page_id = %s AND user_id IN (%s)", secure($pageId, 'int'), $strCon);

        $likedUserIds = array();
        $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_users->num_rows > 0) {
            while($user = $get_users->fetch_assoc()) {
                $likedUserIds[] = $user['user_id'];
            }
        }
        return $likedUserIds;
    }

    /**
     * Cho user join vào nhiều group một lúc.
     *
     * @param $groupIds
     * @param $userIds
     */
    public function addUserToSomeGroups($groupIds, $userIds) {
        if (count($groupIds) > 0) {
            foreach ($groupIds as $groupId) {
                $this->addUserToGroup($groupId, $userIds);
            }
        }
    }

    /**
     * Đưa danh sách user thành thành viên của nhóm (group member)
     *
     * @param $groupId
     * @param $userIds
     * @throws Exception
     */
    public function addUserToGroup($groupId, $userIds) {
        global $db;

        if ($groupId == 0) return;
        // Xóa user cũ tránh bị lỗi duplicate
        $userIds = array_unique($userIds); //Bỏ đi giá trị bị lặp
        $memberIds = $this->_getMemberIdOnGroup($groupId, $userIds);

        //Bỏ đi những người đã là thành viên của nhóm
        $userIds = array_diff($userIds, $memberIds);

        if (count($userIds) == 0) return;

        //1. Đưa danh sách user vào nhóm
        //Câu sql sẽ có dạng: INSERT INTO tbl_name (col_A,col_B,col_C) VALUES (1,2,3), (4,5,6), (7,8,9)
        //Build lên phần giá trị truyền vào.
        $strValues = "";
        foreach ($userIds as $id) {
            //group_id, user_id, approved
            $strValues .= "(". secure($groupId, 'int') .",". secure($id, 'int') . ",'1'),";
        }
        $strValues = trim($strValues, ",");

        $strSql = "INSERT INTO groups_members (group_id, user_id, approved) VALUES ".$strValues;
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        //2. Tăng số thành viên của group
        $strSql = sprintf("UPDATE groups SET group_members = group_members + %s WHERE group_id = %s", secure(count($userIds), 'int'), $groupId);
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy ra danh sách ID đã là thành viên nhớm từ một tập ID cho trước.
     *
     * @param $groupId
     * @param $userIds
     * @return array
     * @throws Exception
     */
    private function _getMemberIdOnGroup($groupId, $userIds) {
        global $db;

        $strCon = implode(',', $userIds);
        $strSql = sprintf("SELECT user_id FROM groups_members WHERE group_id = %s AND user_id IN (%s)", secure($groupId, 'int'), $strCon);

        $memberIds = array();
        $get_members = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_members->num_rows > 0) {
            while($member = $get_members->fetch_assoc()) {
                $memberIds[] = $member['user_id'];
            }
        }
        return $memberIds;
    }

    /**
     * Xóa danh sách posts liên quan đến sụ kiên/thông báo
     *
     * @param $postIds
     * @throws Exception
     */
    public function deletePosts($postIds) {
        global $db;

        if ((!is_null($postIds)) && ($postIds != '')) {
            $db->query(sprintf("DELETE FROM posts WHERE post_id IN (%s)", $postIds)) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * Kiểm tra password của user
     *
     * @param $userId
     * @return null
     */
    public function comparePassword($userId, $userPass)
    {
        global $db;

        if (strlen($userPass) < 6) {
            throw new Exception(__("The password you entered is incorrect"));
        }

        /* search users */
        $strSql = sprintf('SELECT user_password FROM users WHERE user_id = %s', secure($userId, 'int'));
        $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        if($get_users->num_rows > 0) {
            $user_password = $get_users->fetch_assoc()['user_password'];
            if(password_verify($userPass, $user_password)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Update toàn bộ password của user
     *
     * @param $password
     */
    public function changeAllPassword($password) {
        global $db;

        $strSql = sprintf("UPDATE users SET user_password = %s", secure(_password_hash($password)));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Update user last active
     *
     * @param $time
     * @param $user_id
     * @throws Exception
     */
    public function updateUserLastActive($time, $user_id) {
        global $db;

        $strSql = sprintf("UPDATE users SET user_last_active = %s WHERE user_id = %s", secure($time), secure($user_id, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy danh sách user theo thời gian đăng ký mới
     *
     * @param $fromDate
     * @param $toDate
     * @return array
     * @throws Exception
     */
    public function getUserRegistered($fromDate, $toDate) {
        global $db;

        $fromDate = toDBDate($fromDate);
        $toDate = toDBDate($toDate);

        $strSql = sprintf("SELECT * FROM users WHERE DATE(user_registered) <= %s AND DATE(user_registered) >= %s", secure($toDate), secure($fromDate));

        $get_user_reg = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $users = array();
        if($get_user_reg->num_rows > 0) {
            while($user = $get_user_reg->fetch_assoc()) {
                $user['user_birthdate'] = toSysDate($user['user_birthdate']);
                $user['user_registered'] = toSysDatetime($user['user_registered']);
                $user['user_last_login'] = toSysDate($user['user_last_login']);
                $user['user_last_active'] = toSysDate($user['user_last_active']);
                $users[] = $user;
            }
        }

        return $users;
    }

    /**
     * Lấy user theo begin và limit
     *
     * @param $begin
     * @param $limit
     * @return array
     * @throws Exception
     */
    public function getAllUser($begin, $limit) {
        global $db;

        $strSql = sprintf("SELECT user_id, user_email, user_name, user_phone FROM users LIMIT %s, %s", $begin, $limit);
       $get_rows = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $rows = array();
        if($get_rows->num_rows > 0) {
            while($row = $get_rows->fetch_assoc()) {
                $rows[] = $row;
            }
        }

        return $rows;
    }

    public function getAllUsers() {
        global $db;

        $strSql = sprintf("SELECT user_id, user_email FROM users");
        $get_rows = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $rows = array();
        if($get_rows->num_rows > 0) {
            while($row = $get_rows->fetch_assoc()) {
                $rows[] = $row;
            }
        }

        return $rows;
    }

    /**
     * Lấy số lượng user của hệ thống
     *
     * @return int
     * @throws Exception
     */
    public function getCountUser() {
        global $db;

        $strSql = sprintf("SELECT COUNT(user_id) as cnt FROM users");

        $get_count = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $count = 0;

        if($get_count->num_rows > 0) {
            $count = $get_count->fetch_assoc()['cnt'];
        }

        return $count;
    }

    /**
     * Hàm xóa user khỏi hệ thống
     *
     * @param $userId
     * @throws Exception
     */
    public function deleteUser($userId) {
        global $db;

        // 1. Bỏ tất cả các like page có trên hệ thống và giảm số lượng like của page đi 1
        // Lấy những page mà user đã like
        $strSql = sprintf("SELECT page_id FROM pages_likes WHERE user_id = %s", secure($userId, 'int'));
        $get_page_ids = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $pageIds = array();
        if($get_page_ids->num_rows > 0) {
            while($page_id = $get_page_ids->fetch_assoc()['page_id']){
                $pageIds[] = $page_id;
            }
        }

        $pageIds = implode(',', $pageIds);

        // Giảm số lượng like của các page trên đi 1
        $strSql = sprintf("UPDATE pages SET page_likes = page_likes - 1 WHERE page_id IN (%s)", $pageIds);
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        // Xóa like page của user
        $strSql = sprintf("DELETE FROM pages_likes WHERE user_id = %s", secure($userId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        // 2. Bỏ thành viên group
        // Lấy những group mà user đã tham gia
        $strSql = sprintf("SELECT group_id FROM groups_members WHERE user_id = %s", secure($userId, 'int'));
        $get_group_ids = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $groupIds = array();
        if($get_group_ids->num_rows > 0) {
            while($group_id = $get_group_ids->fetch_assoc()['group_id']){
                $groupIds[] = $group_id;
            }
        }

        $groupIds = implode(',', $groupIds);
        // Giảm số lượng thành viên của group
        $strSql = sprintf("UPDATE groups SET group_members = group_members - 1 WHERE group_id IN (%s)", $groupIds);
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        // Xóa user khỏi thành viên của nhóm
        $strSql = sprintf("DELETE FROM groups_members WHERE user_id = %s", secure($userId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        // 3. Bỏ pages_invites
        $strSql = sprintf('DELETE FROM pages_invites WHERE user_id = %1$s OR from_user_id = %1$s', secure($userId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        // 4. Bỏ khỏi bảng friend
        $strSql = sprintf('DELETE FROM friends WHERE user_one_id = %1$s OR user_two_id = %1$s', secure($userId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        // 5. Xóa khỏi bảng ci_user_manage
        $strSql = sprintf('DELETE FROM ci_user_manage WHERE user_id = %s', secure($userId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        // 6. Xóa user khỏi bảng ci_parent_manage
        $strSql = sprintf('DELETE FROM ci_parent_manage WHERE user_id = %s', secure($userId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        // 7. Xóa khỏi bảng ci_user_role
        $strSql = sprintf('DELETE FROM ci_user_role WHERE user_id = %s', secure($userId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        // 8. Xóa khỏi bảng followings
        $strSql = sprintf('DELETE FROM friends WHERE user_id = %1$s OR following_id = %1$s', secure($userId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        // 9. Xóa khỏi bảng notifications
        $strSql = sprintf('DELETE FROM notifications WHERE to_user_id = %s', secure($userId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        // 10. Xóa khỏi bảng posts_likes
        $strSql = sprintf('DELETE FROM posts_likes WHERE user_id = %s', secure($userId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        // 11. Xóa các post đã đăng
        // Lấy ra các post đã đăng
        $strSql = sprintf("SELECT post_id FROM posts WHERE user_id = %s", secure($userId, 'int'));
        $get_post_ids = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $postIds = array();
        if($get_post_ids->num_rows > 0) {
            while($post_id = $get_post_ids->fetch_assoc()['post_id']){
                $postIds[] = $post_id;
            }
        }

        $strPost = implode(',', $postIds);
        //Xóa posts_files
        $strSql = sprintf("DELETE FROM posts_files WHERE post_id IN (%s)", $strPost);
        $db->query($strSql) or _error('error', $strSql);

        //Xóa posts_links
        $strSql = sprintf("DELETE FROM posts_links WHERE post_id IN (%s)", $strPost);
        $db->query($strSql) or _error('error', $strSql);

        //Xóa posts_media
        $strSql = sprintf("DELETE FROM posts_media WHERE post_id IN (%s)", $strPost);
        $db->query($strSql) or _error('error', $strSql);

        // Lấy ra tất cả post photo
        $strSql = sprintf("SELECT photo_id FROM posts_photos WHERE post_id IN (%s)", $strPost);

        $photoIds = array();
        $get_photos = $db->query($strSql) or _error('error', $strSql);
        if($get_photos->num_rows > 0) {
            while($photo = $get_photos->fetch_assoc()) {
                $photoIds[] = $photo['photo_id'];
            }
        }

        //Xóa posts_photos_likes
        $strPhotoIds = implode(',', $photoIds);
        $strSql = sprintf("DELETE FROM posts_photos_likes WHERE photo_id IN (%s)", $strPhotoIds);
        $db->query($strSql) or _error('error', $strSql);


        // Lấy ra tất cả comment cần xóa
        $strSql = sprintf("SELECT comment_id FROM posts_comments WHERE (node_type = 'post' AND node_id IN (%s)) OR
              (node_type = 'photo' AND node_id IN (%s))", $strPost, $strPhotoIds);

        $commentIds = array();
        $get_comments = $db->query($strSql) or _error('error', $strSql);
        if($get_comments->num_rows > 0) {
            while($comment = $get_comments->fetch_assoc()) {
                $commentIds[] = $comment['comment_id'];
            }
        }

        //Xóa posts_comments
        $strCommentIds = implode(',', $commentIds);
        $strSql = sprintf("DELETE FROM posts_comments WHERE comment_id IN (%s)", $strCommentIds);
        $db->query($strSql) or _error('error', $strSql);


        //Xóa posts_photos
        $strSql = sprintf("DELETE FROM posts_photos WHERE post_id IN (%s)", $strPost);
        $db->query($strSql) or _error('error', $strSql);

        //Xóa posts_photos_album
        $strSql = sprintf("DELETE FROM posts_photos_albums WHERE user_type = 'user' AND user_id = %s", secure($userId, 'int'));
        $db->query($strSql) or _error('error', $strSql);

        //Xóa posts_saved
        $strSql = sprintf("DELETE FROM posts_saved WHERE post_id IN (%s)", $strPost);
        $db->query($strSql) or _error('error', $strSql);

        //Xóa posts_videos
        $strSql = sprintf("DELETE FROM posts_videos WHERE post_id IN (%s)", $strPost);
        $db->query($strSql) or _error('error', $strSql);

        // Xóa các post
        $strSql = sprintf('DELETE FROM posts WHERE user_id = %s', secure($userId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        // 12. Xóa các comment đã đăng
        $strSql = sprintf('DELETE FROM posts_comments WHERE user_id = %s', secure($userId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        // 13. Xóa các comment_likes đã đăng
        $strSql = sprintf('DELETE FROM posts_comments_likes WHERE user_id = %s', secure($userId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        // 14. Xóa user
        $strSql = sprintf('DELETE FROM users WHERE user_id = %s', secure($userId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Hàm thêm mới danh sách admin cho page
     *
     * @param $pageId
     * @param $admins
     * @throws Exception
     */
    public function addAdminsToPagesAdmins($pageId, $admins) {
        global $db;

        if(count($admins) == 0) return;
        //1. Đưa danh sách user vào nhóm
        //Câu sql sẽ có dạng: INSERT INTO tbl_name (col_A,col_B,col_C) VALUES (1,2,3), (4,5,6), (7,8,9)
        //Build lên phần giá trị truyền vào.
        $strValues = "";
        foreach ($admins as $id) {
            //group_id, user_id, approved
            $strValues .= "(". secure($pageId, 'int') .",". secure($id, 'int') . "),";
        }
        $strValues = trim($strValues, ",");

        $strSql = "INSERT INTO pages_admins (page_id, user_id) VALUES ".$strValues;
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }
}
?>