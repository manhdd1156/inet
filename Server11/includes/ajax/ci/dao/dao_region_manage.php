<?php
/**
 * DAO -> RegionManage
 * Thao tác với các bảng ci_region_manage
 *
 * @package ConIu
 * @author TaiLA
 */

class RegionManageDAO
{
    /**
     * __construct
     *
     */
    public function __construct()
    {
    }

    /**
     * Insert user quản lý vùng
     *
     * @param $args
     * @throws Exception
     */
    public function createRegionManage($args) {
        global $db, $user;

        if($args['city_id'] && (isset($args['district_slug'])) && $args['district_slug'] == 0) {
            // Xóa toàn bộ những bản ghi có city_id trùng trước đó
            $strSql = sprintf("DELETE FROM ci_region_manage WHERE city_id = %s AND user_id = %s", secure($args['city_id'], 'int'), secure($args['user_id'], 'int'));

            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }

        $strSql = sprintf("INSERT INTO ci_region_manage (user_id, city_id, district_slug, grade, role, provider) VALUES (%s, %s, %s, %s, %s, %s)",
            secure($args['user_id'], 'int'),
            secure($args['city_id'], 'int'), secure($args['district_slug']),
            secure($args['grade']), secure($args['role']), secure($args['provider']));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy tất cả user quản lý với provider tương ứng
     *
     * @param int $provider
     * @return array
     * @throws Exception
     */
    public function getAllUserRegionManageByProvider($cityId, $districtSlug, $grade, $provider = 1) {
        global $db;

        if(!$districtSlug) {
            if(is_null($grade)) {
                $strSql = sprintf("SELECT RM.*, U.user_email FROM ci_region_manage RM INNER JOIN users U ON RM.user_id = U.user_id WHERE RM.provider = %s AND RM.city_id = %s", secure($provider, 'int'), secure($cityId, 'int'));
            } else {
                $strSql = sprintf("SELECT RM.*, U.user_email FROM ci_region_manage RM INNER JOIN users U ON RM.user_id = U.user_id WHERE RM.provider = %s AND RM.city_id = %s AND grade = %s", secure($provider, 'int'), secure($cityId, 'int'), secure($grade));
            }
        } else {
            if(is_null($grade)) {
                $strSql = sprintf("SELECT RM.*, U.user_email FROM ci_region_manage RM INNER JOIN users U ON RM.user_id = U.user_id WHERE RM.provider = %s AND RM.city_id = %s AND RM.district_slug = %s", secure($provider), secure($cityId, 'int'), secure($districtSlug));
            } else {
                $strSql = sprintf("SELECT RM.*, U.user_email FROM ci_region_manage RM INNER JOIN users U ON RM.user_id = U.user_id WHERE RM.provider = %s AND RM.city_id = %s AND RM.district_slug = %s AND grade = %s", secure($provider), secure($cityId, 'int'), secure($districtSlug), secure($grade));
            }
        }


        $get_user = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $users = array();
        if($get_user->num_rows > 0) {
            while ($user = $get_user->fetch_assoc()) {
                $users[] = $user;
            }
        }

        return $users;
    }

    /**
     * Lấy danh sách quản lý do noga cấp
     *
     * @param int $provider
     * @return array
     * @throws Exception
     */
    public function getAllUserRegionManageByNogaProvider($provider = 1) {
        global $db;

            $strSql = sprintf("SELECT RM.*, U.user_email FROM ci_region_manage RM INNER JOIN users U ON RM.user_id = U.user_id WHERE RM.provider = %s", secure($provider, 'int'));


        $get_user = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $users = array();
        if($get_user->num_rows > 0) {
            while ($user = $get_user->fetch_assoc()) {
                $users[] = $user;
            }
        }

        return $users;
    }

    /**
     * Delete quản lý vùng
     *
     * @param $region_manage_id
     * @throws Exception
     */
    public function deleteRegionManage($region_manage_id) {
        global $db;

        $strSql = sprintf("DELETE FROM ci_region_manage WHERE region_manage_id = %s", secure($region_manage_id, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy toàn bộ vùng mà user quản lý
     *
     * @param $userId
     * @return array
     * @throws Exception
     */
    public function getRegionManageByUserId($userId) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_region_manage WHERE user_id = %s ORDER BY district_slug ASC", secure($userId, 'int'));

        $get_region_manage = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $results = array();
        if($get_region_manage->num_rows > 0) {
            while ($result = $get_region_manage->fetch_assoc()) {
                $results[] = $result;
            }
        }

        return $results;
    }

    public function getAllGovSchoolByUserId($userId) {

    }
}