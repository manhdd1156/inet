<?php
/**
 * DAO -> ChildMonth
 * Thao tác với các bảng ci_child_based_on_month
 *
 * @package ConIu
 * @author TaiLa
 */

class ChildMonthDAO
{
    /**
     * __construct
     *
     */
    public function __construct()
    {
    }

    /*
     * Insert vào bảng ci_child_based_on_month
     */
    public function insertChildMonth($months, $links, $titles) {
        global $db;

        $strValues = "";
        // subject
        for ($idx = 0; $idx < count($months); $idx++) {
            $strValues .= ",(" . secure($months[$idx]) . "," . secure($links[$idx]) . "," . secure($titles[$idx]) . "),";
            $strValues = trim($strValues, ",");
            $strSql = "INSERT INTO ci_child_based_on_month (month, link, title) VALUES " . $strValues;
        }
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy toàn bộ thông tin trong ci_child_based_on_month
     *
     * @return array
     */
    public function getAllChildMonth () {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_child_based_on_month ORDER BY month ASC, child_based_on_month_id ASC");
        $get_child_month = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $childMonths = array();
        if($get_child_month->num_rows > 0) {
            while ($childMonth = $get_child_month->fetch_assoc()) {
                $childMonths[] = $childMonth;
            }
        }
        return $childMonths;
    }

    /**
     * Lấy chi tiết thông tin tháng tuổi
     *
     * @param $child_based_on_month_id
     * @return null
     * @throws Exception
     */
    public function getChildMonthDetail($child_based_on_month_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_child_based_on_month WHERE child_based_on_month_id = %s", secure($child_based_on_month_id));

        $get_child_month = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $childMonth = null;
        if($get_child_month->num_rows > 0) {
            $childMonth = $get_child_month->fetch_assoc();
        }

        return $childMonth;
    }

    /**
     * Cập nhật thông tin tháng tuổi
     *
     * @param $args
     */
    public function updateChildMonth($args) {
        global $db;

        $strSql = sprintf("UPDATE ci_child_based_on_month SET month = %s, link = %s, title = %s WHERE child_based_on_month_id = %s", secure($args['month'], 'int'), secure($args['link']), secure($args['title']), secure($args['child_based_on_month_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Xóa thông tin tháng tuổi
     *
     * @param $child_based_on_month_id
     * @throws Exception
     */
    public function deleteChildMonth($child_based_on_month_id) {
        global $db;

        $strSql = sprintf("DELETE FROM ci_child_based_on_month WHERE child_based_on_month_id = %s", secure($child_based_on_month_id, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     *
     * THông tin tuần thai
     *
     */

    /**
     * Thêm thông tin tuần thai
     *
     * @param $weeks
     * @param $links
     * @throws Exception
     */
    public function insertPregnancy($weeks, $links, $titles) {
        global $db;

        $strValues = "";
        // subject
        for ($idx = 0; $idx < count($weeks); $idx++) {
            $strValues .= ",(" . secure($weeks[$idx]) . "," . secure($links[$idx]) . "," . secure($titles[$idx]) . "),";
            $strValues = trim($strValues, ",");
            $strSql = "INSERT INTO ci_foetus_based_on_week (week, link, title) VALUES " . $strValues;
        }
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy toàn bộ thông tin trong bảng ci_foetus_based_on_week
     *
     * @return array
     * @throws Exception
     */
    public function getAllPregnancys () {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_foetus_based_on_week ORDER BY week ASC, foetus_based_on_week_id ASC");
        $get_pregnancy = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $pregnancys = array();
        if($get_pregnancy->num_rows > 0) {
            while ($pregnancy = $get_pregnancy->fetch_assoc()) {
                $pregnancys[] = $pregnancy;
            }
        }
        return $pregnancys;
    }

    /**
     * Lấy thông tin chi tiết tuần thai
     *
     * @param $foetus_based_on_week_id
     * @return null
     * @throws Exception
     */
    public function getPregnancyDetail($foetus_based_on_week_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_foetus_based_on_week WHERE foetus_based_on_week_id = %s", secure($foetus_based_on_week_id));

        $get_pregnancy = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $pregnancy = null;
        if($get_pregnancy->num_rows > 0) {
            $pregnancy = $get_pregnancy->fetch_assoc();
        }

        return $pregnancy;
    }

    /**
     * Cập nhật thông tin tuần thai
     *
     * @param $args
     * @throws Exception
     */
    public function updatePregnancy($args) {
        global $db;

        $strSql = sprintf("UPDATE ci_foetus_based_on_week SET week = %s, link = %s, title = %s WHERE foetus_based_on_week_id = %s", secure($args['week'], 'int'), secure($args['link']), secure($args['title']), secure($args['foetus_based_on_week_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Xóa thông tin tuần thai
     *
     * @param $foetus_based_on_week_id
     * @throws Exception
     */
    public function deletePregnancy($foetus_based_on_week_id) {
        global $db;

        $strSql = sprintf("DELETE FROM ci_foetus_based_on_week WHERE foetus_based_on_week_id = %s", secure($foetus_based_on_week_id, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy link theo tuần
     *
     * @param $week
     * @return null
     * @throws Exception
     */
    public function getLinkBasedOnWeek($week) {
        global $db;

        $strSql = sprintf("SELECT link FROM ci_foetus_based_on_week WHERE week = %s", secure($week, 'int'));

        $link = null;
        $get_link = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_link->num_rows > 0) {
            $link = $get_link->fetch_assoc()['link'];
        }

        return $link;
    }

    /**
     * Lấy link theo tháng
     *
     * @param $month
     * @return null
     * @throws Exception
     */
    public function getLinkBasedOnMonth($month) {
        global $db;

        $strSql = sprintf("SELECT link FROM ci_child_based_on_month WHERE month = %s", secure($month, 'int'));

        $link = null;
        $get_link = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_link->num_rows > 0) {
            $link = $get_link->fetch_assoc()['link'];
        }

        return $link;
    }
}