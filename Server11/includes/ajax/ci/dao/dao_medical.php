<?php

/**
 * Class MedicalDAO: thao tác với bảng ci_child_medical của hệ thống.
 */
class MedicalDAO {
    /**
     * __construct
     *
     */
    public function __construct()
    {
    }

    /**
     * Lấy dánh sách y bạ của 1 trẻ
     *
     * @param $child_parent_id
     * @return array
     */
    public function getChildAllMedical($child_parent_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_child_medical WHERE child_parent_id = %s ORDER BY recorded_at DESC, child_medical_id DESC", secure($child_parent_id, 'int'));

        $get_medicals = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $medicals = array();
        if($get_medicals->num_rows > 0) {
            while($medical = $get_medicals->fetch_assoc()) {
                $medical['recorded_at'] = toSysDate($medical['recorded_at']);
                if(!is_null($medical['medicine_begin'])) {
                    $medical['medicine_begin'] = toSysDate($medical['medicine_begin']);
                }
                if(!is_null($medical['medicine_end'])) {
                    $medical['medicine_end'] = toSysDate($medical['medicine_end']);
                }
                if(!is_null($medical['created_at'])) {
                    $medical['created_at'] = toSysDate($medical['created_at']);
                }
                if(!is_null($medical['updated_at'])) {
                    $medical['updated_at'] = toSysDate($medical['updated_at']);
                }
                $medicals[] = $medical;
            }
        }

        return $medicals;
    }

    /**
     * Lấy dánh sách y bạ của 1 trẻ
     *
     * @param $child_parent_id
     * @return array
     */
    public function getChildAllMedicalForAPI($child_parent_id) {
        global $db;

        $strSql = sprintf("SELECT child_medical_id, child_parent_id, recorded_at, diseased_name, symptom, day_use FROM ci_child_medical WHERE child_parent_id = %s ORDER BY recorded_at DESC, child_medical_id DESC", secure($child_parent_id, 'int'));

        $get_medicals = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $medicals = array();
        if($get_medicals->num_rows > 0) {
            while($medical = $get_medicals->fetch_assoc()) {
                $medical['recorded_at'] = toSysDate($medical['recorded_at']);
                $medicals[] = $medical;
            }
        }

        return $medicals;
    }

    /**
     * Validate dữ liệu nhập vào
     *
     * @param $args
     * @throws Exception
     */
    private function _validateInput($args) {
        if(is_empty($args['recorded_at'])) {
            throw new Exception(__("Data record date can not be empty"));
        }
        if(is_empty($args['diseased_name'])) {
            throw new Exception(__("Please enter diseased name"));
        }
        if(is_empty($args['medicine_list'])) {
            throw new Exception(__("Please enter medicine list"));
        }
        if(is_empty($args['usage_guide'])) {
            throw new Exception(__("Please enter usage guide"));
        }

        if (!validateDate($args['recorded_at'])) {
            throw new Exception(__("You must enter correct recorded_at"));
        }
    }
    /**
     * Thêm thông tin y bạ mới
     *
     * @param $args
     * @return mixed
     */
    public function insertChildMedical($args) {
        global $db, $date, $user;

        // Kiểm tra điều kiện nhập vào
        $this->_validateInput($args);

        $args['recorded_at'] = toDBDate($args['recorded_at']);
        $strSql = sprintf("INSERT INTO ci_child_medical (child_parent_id, recorded_at, diseased_name, medicine_list, usage_guide, symptom, day_use, hospital, hospital_address, doctor_name, doctor_phone, description, created_at, created_user_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", secure($args['child_parent_id'], 'int'), secure($args['recorded_at']), secure($args['diseased_name']), secure($args['medicine_list']), secure($args['usage_guide']), secure($args['symptom']), secure($args['day_use'], 'int'), secure($args['hospital']), secure($args['hospital_address']), secure($args['doctor_name']), secure($args['doctor_phone']), secure($args['description']), secure($date), secure($user->_data['user_id']));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        return $db->insert_id;
    }

    /**
     * Thay đổi thông tin y bạ của trẻ
     *
     * @param $args
     */
    public function updateChildMedical($args) {
        global $db, $date;

        // Kiểm tra điều kiện nhập vào
        $this->_validateInput($args);

        $args['recorded_at'] = toDBDate($args['recorded_at']);

        $strSql = sprintf("UPDATE ci_child_medical SET recorded_at = %s, diseased_name = %s, medicine_list = %s, usage_guide = %s, symptom = %s, day_use = %s, hospital = %s, hospital_address = %s, doctor_name = %s, doctor_phone = %s, description = %s, updated_at = %s WHERE child_medical_id = %s", secure($args['recorded_at']), secure($args['diseased_name']), secure($args['medicine_list']), secure($args['usage_guide']), secure($args['symptom']), secure($args['day_use'],'int'), secure($args['hospital']), secure($args['hospital_address']), secure($args['doctor_name']), secure($args['doctor_phone']), secure($args['description']), secure($date), secure($args['child_medical_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy thông tin chi tiết y bạ của trẻ
     *
     * @param $child_medical_id
     * @return null
     */
    public function getChildMedical($child_medical_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_child_medical WHERE child_medical_id = %s", secure($child_medical_id, 'int'));

        $get_medical = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $medical = null;
        if($get_medical->num_rows > 0) {
            $medical = $get_medical->fetch_assoc();
            $medical['recorded_at'] = toSysDate($medical['recorded_at']);
            if(!is_null($medical['created_at'])) {
                $medical['created_at'] = toSysDate($medical['created_at']);
            }
            if(!is_null($medical['updated_at'])) {
                $medical['updated_at'] = toSysDate($medical['updated_at']);
            }
        }

        return $medical;
    }

    /**
     * Xóa thông tin y bạ
     *
     * @param $child_medical_id
     */
    public function deleteChildMedical($child_medical_id) {
        global $db;

        $strSql = sprintf("DELETE FROM ci_child_medical WHERE child_medical_id = %s", secure($child_medical_id, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Tìm kiếm thông tin y bạ theo năm
     *
     * @param $child_parent_id
     * @param $year
     * @return array
     */
    public function getChildMedicalSearch($child_parent_id, $year) {
        global $db;

        $begin = "01/01/" . $year;
        $end = "31/12/" . $year;

        $begin = toDBDate($begin);
        $end = toDBDate($end);

        $strSql = sprintf("SELECT * FROM ci_child_medical WHERE child_parent_id = %s AND recorded_at >= %s AND recorded_at <= %s ORDER BY recorded_at DESC, created_at DESC", secure($child_parent_id, 'int'), secure($begin), secure($end));

        $get_medical = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $medicals = array();
        if($get_medical->num_rows > 0) {
            while($medical = $get_medical->fetch_assoc()) {
                $medical['recorded_at'] = toSysDate($medical['recorded_at']);

                $medicals[] = $medical;
            }
        }

        return $medicals;
    }

    /**
     * Tìm kiếm thông tin y bạ theo năm vẽ biểu đồ
     *
     * @param $child_parent_id
     * @param $year
     * @return array
     */
    public function getChildMedicalSearchForChart($child_parent_id, $year) {
        global $db;

        $begin = "01/01/" . $year;
        $end = "31/12/" . $year;

        $begin = toDBDate($begin);
        $end = toDBDate($end);

        $strSql = sprintf("SELECT recorded_at FROM ci_child_medical WHERE child_parent_id = %s AND recorded_at >= %s AND recorded_at <= %s ORDER BY recorded_at ASC, created_at ASC", secure($child_parent_id, 'int'), secure($begin), secure($end));

        $get_medical = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $medicals = array();
        if($get_medical->num_rows > 0) {
            while($medical = $get_medical->fetch_assoc()['recorded_at']) {
                $medical = toSysDate($medical);
                $medical = substr($medical, 3, 2);
                $medicals[] = $medical;
            }
        }

        return $medicals;
    }
}
?>