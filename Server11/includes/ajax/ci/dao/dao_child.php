<?php

/**
 * Class ChildDAO: thao tác với bảng ci_child của hệ thống.
 */
class ChildDAO {
    /**
     * __construct
     *
     */
    public function __construct()
    {
    }

    /**
     * Kiểm tra đầu vào cho hàm insert và update
     *
     * @param array $args
     * @throws Exception
     */
    private function validateInput (array $args = array(), $isCreate = false) {
        if ($isCreate && is_empty($args['child_code'])) {
            throw new Exception(__("You must enter child code"));
        }
        if (is_empty($args['first_name'])) {
            throw new Exception(__("You must enter first name"));
        }
        if (is_empty($args['begin_at'])) {
            throw new Exception(__("You must enter study start date"));
        }
        if (strlen($args['first_name']) > 15) {
            throw new Exception(__("First name length must be less than 15 characters long"));
        }
        if (is_empty($args['last_name'])) {
            throw new Exception(__("You must enter last name"));
        }
        if (strlen($args['last_name']) > 34) {
            throw new Exception(__("Last name length must be less than 34 characters long"));
        }
        if (!validateDate($args['birthday'])) {
            throw new Exception(__("You must enter birth date"));
        }
        /*if (!is_empty($args['parent_phone']) && strlen($args['parent_phone']) > 50) {
            throw new Exception(__("Telephone length must be less than 50 characters long"));
        }*/
        $args['parent_phone'] = standardizePhone($args['parent_phone']);
        /*if (!validatePhone($args['parent_phone'])) {
            throw new Exception(__("The phone number you entered is incorrect"));
        }*/
        if (strlen($args['parent_email']) > 100) {
            throw new Exception(__("Email length must be less than 100 characters long"));
        }
        if ((!is_empty($args['address'])) && (strlen($args['address']) > 250)) {
            throw new Exception(__("Address length must be less than 250 characters long"));
        }
        if ((!is_empty($args['description'])) && (strlen($args['description']) > 300)) {
            throw new Exception(__("Description length must be less than 300 characters long"));
        }
    }

    /**
     * Kiểm tra đầu vào cho hàm insert và update từ phía phụ huynh
     *
     * @param array $args
     * @throws Exception
     */
    private function _validateInputByParent (array $args = array(), $isCreate = false) {
        if (is_empty($args['first_name'])) {
            throw new Exception(__("You must enter first name"));
        }
        if (strlen($args['first_name']) > 15) {
            throw new Exception(__("First name length must be less than 15 characters long"));
        }
        if (is_empty($args['last_name'])) {
            throw new Exception(__("You must enter last name"));
        }
        if (strlen($args['last_name']) > 34) {
            throw new Exception(__("Last name length must be less than 34 characters long"));
        }
        if (strlen($args['parent_email']) > 100) {
            throw new Exception(__("Email length must be less than 100 characters long"));
        }
        if ((!is_empty($args['description'])) && (strlen($args['description']) > 300)) {
            throw new Exception(__("Description length must be less than 300 characters long"));
        }
        if(!$args['is_pregnant']) {
//            if (!validateDate($args['birthday'])) {
//                throw new Exception(__("You must enter birth date"));
//            }
        } else {
            if (!isset($args['pregnant_week']) || !is_numeric($args['pregnant_week'])) {
                throw new Exception(__("You must enter pregnant week and pregnant week must be a number"));
            }
        }
    }

    /**
     * Tạo ra một CHILD trong DB
     *
     * @param array $args
     * @return mixed
     * @throws Exception
     */
    public function createChild(array $args = array()) {
        global $db, $user;
        $this->validateInput($args, true);

        //Nếu mã tự nhập thì kiểm tra trùng.
        /* Bỏ check vì các trường có thể tự nhập và khả năng trùng là có.
        if ($args['code_auto'] == 0) {
            $strSql = sprintf("SELECT child_id FROM ci_child WHERE child_code = %s", secure($args['child_code']));
            $get_child = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            if($get_child->num_rows > 0) {
                throw new Exception(__("Student code is existing"));
            }
        }
        */

        $args['birthday'] = toDBDate($args['birthday']);
        $sqlStr = sprintf("INSERT INTO ci_child (child_parent_id, child_code, first_name, last_name, child_name, birthday, gender, parent_phone, parent_email, parent_name, address,
                            parent_img1, parent_img2, parent_img3, parent_img4, description, created_user_id, name_for_sort, parent_job, parent_name_dad, parent_phone_dad, parent_job_dad)
                            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
            secure($args['child_parent_id'], 'int'), secure($args['child_code']), secure($args['first_name']), secure($args['last_name']) , secure($args['child_name']), secure($args['birthday']),
            secure($args['gender']),secure($args['parent_phone']), secure(strtolower($args['parent_email'])), secure($args['parent_name']), secure($args['address']), secure($args['parent_img1']),
            secure($args['parent_img2']), secure($args['parent_img3']), secure($args['parent_img4']), secure($args['description']),
            secure($user->_data['user_id'], 'int'), secure($args['name_for_sort']), secure($args['parent_job']), secure($args['parent_name_dad']), secure($args['parent_phone_dad']), secure($args['parent_job_dad']));
        $db->query($sqlStr) or _error(SQL_ERROR_THROWEN);

        //Trả về ID vừa mới được insert vào DB
        return $db->insert_id;
    }


    /**
     * Tạo ra một thông tin trẻ để phụ huynh quản lý
     *
     * @param array $args
     * @return mixed
     * @throws Exception
     */
    public function createChildForParent(array $args = array()) {
        global $db, $user;
        $this->validateInput($args, true);

        $childParentId =0;
        // Nếu chưa có thông tin trong ci_child_parent thì tạo mới
        $strSql = sprintf("SELECT child_parent_id FROM ci_child_parent WHERE child_code = %s", secure($args['child_code']));
        $get_child = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_child->num_rows == 0) {
            $args['birthday'] = toDBDate($args['birthday']);
            $sqlStr = sprintf("INSERT INTO ci_child_parent (child_admin, child_code, first_name, last_name, child_name, birthday, gender, parent_phone, parent_name, parent_email, address,
                            parent_img1, parent_img2, parent_img3, parent_img4, description, created_user_id, name_for_sort, parent_job, parent_name_dad, parent_phone_dad, parent_job_dad)
                            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                secure($args['child_admin'], 'int'), secure($args['child_code']), secure($args['first_name']), secure($args['last_name']) , secure($args['child_name']), secure($args['birthday']),
                secure($args['gender']), secure($args['parent_phone']), secure($args['parent_name']), secure(strtolower($args['parent_email'])), secure($args['address']), secure($args['parent_img1']),
                secure($args['parent_img2']), secure($args['parent_img3']), secure($args['parent_img4']), secure($args['description']),
                secure($user->_data['user_id'], 'int'), secure($args['name_for_sort']), secure($args['parent_job']), secure($args['parent_name_dad']), secure($args['parent_phone_dad']), secure($args['parent_job_dad']));
            $db->query($sqlStr) or _error(SQL_ERROR_THROWEN);

            //Trả về ID vừa mới được insert vào DB
            $childParentId  = $db->insert_id;

            //4. Cập nhật thôn tin lại bảng ci_child ( Lưu thông tin trẻ trong trường)
            $strSql = sprintf("UPDATE ci_child SET child_parent_id = %s WHERE child_id = %s",  secure($childParentId, 'int'), secure($args['child_id'], 'int'));
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);

            //4. Cập nhật bảng ci_school_child
            $strSql = sprintf("UPDATE ci_school_child SET child_parent_id = %s WHERE child_id = %s AND school_id = %s",  secure($childParentId, 'int'), secure($args['child_id'], 'int'), secure($args['school_id'], 'int'));
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }

        return $childParentId;
    }

    /**
     * Update ra một thông tin trẻ để phụ huynh quản lý
     *
     * @param array $args
     * @throws Exception
     */
    public function updateChildForParent($childParentId, $childAdmin) {
        global $db;

        //Cập nhật thông tin child_admin lại bảng ci_child_parent
        $strSql = sprintf("UPDATE ci_child_parent SET child_admin = %s WHERE child_parent_id = %s",  secure($childAdmin, 'int'),  secure($childParentId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lưu thông tin trẻ mà phụ hunh đang quản lý
     *
     * @param  $args
     * @throws Exception
     */
    public function createParentManageInfo($parentIds, $childParentId, $childId, $schoolId) {
        global $db;

        $strValues = "";
        foreach ($parentIds as $parentId) {
            $strValues .= "(" . secure($parentId, 'int') . "," . secure($childParentId, 'int') . "," . secure($childId, 'int') . "," . secure($schoolId, 'int') . "," . secure(STATUS_ACTIVE, 'int') . "),";
        }
        $strValues = trim($strValues, ",");
        if (!is_empty($strValues)) {
            $strSql = sprintf("INSERT INTO ci_parent_manage (user_id, child_parent_id, child_id, school_id, status) VALUES " . $strValues);
            $db->query($strSql) or _error('error', $strSql);
        }
    }


    /**
     * Cập nhật thông tin trẻ mà phụ hunh đang quản lý
     *
     * @param  $args
     * @throws Exception
     */
    public function updateParentManageInfo($parentIds, $childParentId, $childId, $schoolId) {
        global $db;

        foreach ($parentIds as $parentId) {
            //Cập nhật thông tin child_admin lại bảng ci_child_parent
            $strSql = sprintf("UPDATE ci_parent_manage SET child_id = %s, school_id = %s, status = 1 WHERE user_id = %s AND child_parent_id = %s", secure($childId, 'int'), secure($schoolId, 'int'), secure($parentId, 'int'),  secure($childParentId, 'int'));
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }


    /**
     * Xóa thông tin trẻ mà phụ huynh đang quản lý
     *
     * @param  $args
     * @throws Exception
     */
    public function deleteParentManageInfo($childId, $childParentId) {
        global $db;
        $strSql = sprintf("UPDATE ci_parent_manage SET child_id = 0, school_id = 0 WHERE child_id = %s AND child_parent_id = %s",
            secure($childId, 'int'), secure($childParentId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Phụ huynh Tạo ra một CHILD trong DB
     *
     * @param array $args
     * @return mixed
     * @throws Exception
     */
    public function createChildByParent(array $args = array()) {
        global $db, $user;
        $this->_validateInputByParent($args, true);

        if(isset($args['birthday']) && ($args['birthday'] != 'null')) {
            $args['birthday'] = toDBDate($args['birthday']);
        }
        if(isset($args['due_date_of_childbearing']) && ($args['due_date_of_childbearing'] != "null")) {
            $args['due_date_of_childbearing'] = toDBDate($args['due_date_of_childbearing']);
        }
        if($args['is_pregnant']) {
            $n = $args['pregnant_week'] * 7;
            $now = date("Y-m-d");
            $args['foetus_begin_date'] = date("Y-m-d", strtotime("-" .$n. " day", strtotime($now)));
        }

        $sqlStr = sprintf("INSERT INTO ci_child_parent (child_code, first_name, last_name, child_name, child_nickname, birthday, gender, parent_name, parent_phone, parent_email, address, blood_type, hobby, allergy,
                            parent_img1, parent_img2, parent_img3, parent_img4, description, created_user_id, name_for_sort, is_pregnant, foetus_begin_date, due_date_of_childbearing, child_admin)
                            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
            secure($args['child_code']), secure($args['first_name']), secure($args['last_name']) , secure($args['child_name']), secure($args['child_nickname']), secure($args['birthday']),
            secure($args['gender']), secure($args['parent_name']),secure($args['parent_phone']), secure(strtolower($args['parent_email'])), secure($args['address']), secure($args['blood_type']), secure($args['hobby']), secure($args['allergy']), secure($args['parent_img1']),
            secure($args['parent_img2']), secure($args['parent_img3']), secure($args['parent_img4']), secure($args['description']),
            secure($user->_data['user_id'], 'int'), secure($args['name_for_sort']), secure($args['is_pregnant'], 'int'), secure($args['foetus_begin_date']), secure($args['due_date_of_childbearing']), secure($user->_data['user_id'], 'int'));
        $db->query($sqlStr) or _error(SQL_ERROR_THROWEN);

        //Trả về ID vừa mới được insert vào DB
        return $db->insert_id;
    }

    /**
     * Phân trẻ về một lớp.
     *
     * @param array $args
     * @throws Exception
     */

    public function addChildToClass(array $args = array())
    {
        global $db;
        $args['begin_at'] = toDBDate($args['begin_at']);

        $sqlStr = sprintf("INSERT INTO ci_class_child (class_id, child_id, begin_at, status) VALUES (%s, %s, %s, %s)",
            secure($args['class_id'], 'int'), secure($args['child_id'], 'int'), secure($args['begin_at']), STATUS_ACTIVE);
        $db->query($sqlStr) or _error(SQL_ERROR_THROWEN);
    }
    /**
     * Nhập thông tin trẻ vào bảng điểm
     *
     * @param $subject_Id
     * @param $child_code
     * @param $class_id
     * @param $school_year
     * @throws Exception
     */

    public function addChildPoints($subject_id,$child_code,$class_id,$school_year)
    {
        global $db;
        // UPDATE START MANHDD 03/06/2021
//        $strSql = sprintf("INSERT INTO ci_point_c1 (class_id, subject_id, child_code, school_year) VALUES (%s, %s, %s, %s)", secure($class_id, 'int'), secure($subject_id, 'int'), secure($child_code), secure($school_year) );
        $strSql = sprintf("INSERT INTO ci_point (class_id, subject_id, child_code, school_year) VALUES (%s, %s, %s, %s)", secure($class_id, 'int'), secure($subject_id, 'int'), secure($child_code), secure($school_year) );
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        // UPDATE END MANHDD 03/06/2021
    }
    /**
     * Nhập thông tin trẻ vào bảng điểm
     *
     * @param $class_id
     * @param $child_id
     * @param $school_year
     * @throws Exception
     */

    public function addChildConduct($class_id,$child_id,$school_year)
    {
        global $db;

        $strSql = sprintf("INSERT INTO ci_conduct (class_id, child_id, school_year) VALUES (%s, %s, %s)", secure($class_id, 'int'), secure($child_id, 'int'), secure($school_year) );
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }
    /**
     * Hàm thực hiện khi chuyển lớp cho trẻ.
     *
     * @param $childId
     * @param $oldClassId
     * @param $newClassId
     * @throws Exception
     */
    public function moveClassInClassChild($childId, $oldClassId, $newClassId) {
        global $db, $system;

        $today = date($system['date_format']);
        $today = toDBDate($today);

        if ($oldClassId > 0) {
            //Nếu đã ở một lớp, thì cập nhật thông tin lớp cho trẻ.
            $sqlStr = sprintf("UPDATE ci_class_child SET class_id = %s, begin_at = %s WHERE class_id = %s AND child_id = %s",
                    secure($newClassId, 'int'), secure($today), secure($oldClassId, 'int'), secure($childId, 'int'));
            $db->query($sqlStr) or _error(SQL_ERROR_THROWEN);
        } else {
            //Trường hợp trước đó chưa thuộc lớp nào.
            $sqlStr = sprintf("INSERT INTO ci_class_child (class_id, child_id, begin_at, status) VALUES (%s, %s, %s, %s)",
                secure($newClassId, 'int'), secure($childId, 'int'), secure($today), STATUS_ACTIVE);
            $db->query($sqlStr) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * Hàm thực hiện khi chuyển lớp cho nhiều trẻ.
     *
     * @param $childIds
     * @param $oldClassId
     * @param $newClassId
     * @throws Exception
     */
    public function moveClassInClassChildren($childIds, $oldClassId, $newClassId) {
        global $db, $system;

        $today = date($system['date_format']);
        $today = toDBDate($today);
        if(count($childIds) > 0) {
            $strCon = implode(',', $childIds);

            if ($oldClassId > 0) {
                //Nếu đã ở một lớp, thì cập nhật thông tin lớp cho trẻ.
                $sqlStr = sprintf("UPDATE ci_class_child SET class_id = %s, begin_at = %s WHERE class_id = %s AND child_id IN (%s)",
                    secure($newClassId, 'int'), secure($today), secure($oldClassId, 'int'), $strCon);
                $db->query($sqlStr) or _error(SQL_ERROR_THROWEN);
            } else {
                $strValues = "";
                foreach ($childIds as $childId) {
                    $strValues .= "(" . secure($newClassId, 'int') . "," . secure($childId, 'int') . "," . secure($today, 'int') . "," . secure(STATUS_ACTIVE, 'int') . "),";
                }
                $strValues = trim($strValues, ",");
                if (!is_empty($strValues)) {
                    $strSql = sprintf("INSERT INTO ci_class_child (class_id, child_id, begin_at, status) VALUES " . $strValues);
                    $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                }
            }
        }
    }

    /**
     * Cập nhật bảng ci_school_child khi chuyển lớp cho NHIỀU trẻ.
     *
     * @param $schoolId
     * @param $childIds
     * @param $newClassId
     * @throws Exception
     */
    public function moveClassInSchoolChildren($schoolId, $childIds, $newClassId) {
        global $db;
        if(count($childIds) > 0) {
            $strCon = implode(',', $childIds);
            $sqlStr = sprintf("UPDATE ci_school_child SET class_id = %s WHERE school_id = %s AND child_id IN (%s)",
                secure($newClassId, 'int'), secure($schoolId, 'int'), $strCon);
            $db->query($sqlStr) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * Cập nhật bảng ci_school_child khi trẻ chuyển lớp.
     *
     * @param $schoolId
     * @param $childId
     * @param $newClassId
     * @throws Exception
     */
    public function moveClassInSchoolChild($schoolId, $childId, $newClassId) {
        global $db;
        $sqlStr = sprintf("UPDATE ci_school_child SET class_id = %s WHERE school_id = %s AND child_id = %s",
            secure($newClassId, 'int'), secure($schoolId, 'int'), secure($childId, 'int'));
        $db->query($sqlStr) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Đưa thông tin trẻ vào trường.
     *
     * @param array $args
     * @throws Exception
     */
    public function addChildToSchool(array $args = array())
    {
        global $db;
        $args['begin_at'] = toDBDate($args['begin_at']);

        $sqlStr = sprintf("INSERT INTO ci_school_child (school_id, child_id, child_parent_id, class_id, begin_at, status) VALUES (%s, %s, %s, %s, %s, %s)",
            secure($args['school_id'], 'int'), secure($args['child_id'], 'int'), secure($args['child_parent_id'], 'int') , secure($args['class_id'], 'int'), secure($args['begin_at']), STATUS_ACTIVE);

        $db->query($sqlStr) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Cập nhật thông tin trẻ trong trường
     *
     * @param array $args
     * @throws Exception
     */
    public function updateSchoolChild(array $args = array())
    {
        global $db;
        $args['begin_at'] = toDBDate($args['begin_at']);

        $sqlStr = sprintf("UPDATE ci_school_child SET class_id = %s, begin_at = %s WHERE child_id = %s AND school_id = %s",
            secure($args['class_id'], 'int'), secure($args['begin_at']), secure($args['child_id'], 'int'), secure($args['school_id'], 'int'));

        $db->query($sqlStr) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy ra thông tin ID trường, lớp, giới tính của trẻ.
     *
     * @param $childId
     * @param $schoolId
     * @return array|null
     * @throws Exception
     */
    public function getSchoolChild($childId, $schoolId) {
        global $db;

        $strSql = sprintf("SELECT SC.*, C.gender FROM ci_school_child SC INNER JOIN ci_child C ON SC.child_id = C.child_id AND SC.child_id = %s AND SC.school_id = %s",
                            secure($childId, 'int'), secure($schoolId, 'int'));

        $get_child = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_child->num_rows > 0) {
            return $get_child->fetch_assoc();
        }
        return null;
    }

    /**
     * Lấy ra thông tin trường HIỆN TẠI của trẻ
     *
     * @param $childId
     * @return array|null
     * @throws Exception
     */
    public function getCurrentSchoolOfChild($childId) {
        global $db;

        $strSql = sprintf("SELECT P.page_admin, P.page_id, P.page_name, P.page_title, P.page_picture, SC.class_id FROM pages P INNER JOIN ci_school_child SC ON P.page_id = SC.school_id AND SC.child_id = %s AND SC.status = 1", secure($childId, 'int'));

        $get_school = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_school->num_rows > 0) {
            $school =  $get_school->fetch_assoc();
            $school['page_picture'] = get_picture($school['page_picture'], 'school');
            return $school;
        }
        return null;
    }

    /**
     * Cập nhật thông tin CHILD trong bảng ci_child (Trường cập nhật)
     *
     * @param array $args
     * @throws Exception
     */
    public function editChild(array $args = array())
    {
        global $db;
        $this->validateInput($args);
        $args['birthday'] = toDBDate($args['birthday']);

        /*
        $strSql = sprintf("UPDATE ci_child SET child_name = %s, birthday = %s, gender = %s, parent_phone = %s, address = %s,
                            parent_img1 = %s, parent_img2 = %s, parent_img3 = %s, parent_img4 = %s, description = %s WHERE child_id = %s",
            secure($args['child_name']), secure($birthday), secure($args['gender']),
            secure($args['parent_phone']), secure($args['address']), secure($args['parent_img1']),
            secure($args['parent_img2']), secure($args['parent_img3']), secure($args['parent_img4']),
            secure($args['description']), secure($args['child_id'], 'int'));
        */
        $strSql = sprintf("UPDATE ci_child SET first_name = %s, last_name = %s, child_name = %s, birthday = %s, gender = %s, parent_name = %s, parent_phone = %s, parent_email = %s,
            address = %s, description = %s, name_for_sort = %s, parent_job = %s, parent_name_dad = %s, parent_phone_dad = %s, parent_job_dad = %s WHERE child_id = %s",
            secure($args['first_name']), secure($args['last_name']), secure($args['child_name']), secure($args['birthday']), secure($args['gender']), secure($args['parent_name']), secure($args['parent_phone']),
            secure(strtolower($args['parent_email'])), secure($args['address']), secure($args['description']), secure($args['name_for_sort']), secure($args['parent_job']), secure($args['parent_name_dad']), secure($args['parent_phone_dad']), secure($args['parent_job_dad']), secure($args['child_id'], 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        /* CI-mobile update thêm ngày tham gia cho trẻ */
        if (isset($args['begin_at'])) {
            $args['begin_at'] = toDBDate($args['begin_at']);

            $stSql = sprintf("UPDATE ci_class_child SET begin_at = %s WHERE child_id = %s",
                secure($args['begin_at']), secure($args['child_id'], 'int'));
            $db->query($stSql) or _error(SQL_ERROR_THROWEN);
        }
        /* CI-END */
    }

    /**
     * Cập nhật thông tin CHILD bảng ci_child_parent (Trường cập nhật)
     *
     * @param array $args
     * @throws Exception
     */
    public function editChildBySchool(array $args = array())
    {
        global $db;
        $this->validateInput($args);
        $args['birthday'] = toDBDate($args['birthday']);

        $strSql = sprintf("UPDATE ci_child_parent SET first_name = %s, last_name = %s, child_name = %s, birthday = %s, gender = %s, parent_name = %s, parent_phone = %s, parent_email = %s,
            address = %s, description = %s, name_for_sort = %s, parent_job = %s, parent_name_dad = %s, parent_phone_dad = %s, parent_job_dad = %s  WHERE child_parent_id = %s",
            secure($args['first_name']), secure($args['last_name']), secure($args['child_name']), secure($args['birthday']), secure($args['gender']), secure($args['parent_name']), secure($args['parent_phone']),
            secure(strtolower($args['parent_email'])), secure($args['address']), secure($args['description']), secure($args['name_for_sort']), secure($args['parent_job']), secure($args['parent_name_dad']), secure($args['parent_phone_dad']), secure($args['parent_job_dad']), secure($args['child_parent_id'], 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Cập nhật thông tin CHILD từ phụ huynh
     *
     * @param array $args
     * @throws Exception
     */
    public function editChildByParent(array $args = array())
    {
        global $db;
        $this->_validateInputByParent($args);
        if(isset($args['birthday']) && $args['birthday'] != 'null') {
            $args['birthday'] = toDBDate($args['birthday']);
        }
        if(isset($args['due_date_of_childbearing']) && $args['due_date_of_childbearing'] != 'null') {
            $args['due_date_of_childbearing'] = toDBDate($args['due_date_of_childbearing']);
        }
        if($args['is_pregnant']) {
            $n = $args['pregnant_week'] * 7;
            $now = date("Y-m-d");
            $args['foetus_begin_date'] = date("Y-m-d", strtotime("-" .$n. " day", strtotime($now)));
        }

        $strSql = sprintf("UPDATE ci_child_parent SET first_name = %s, last_name = %s, child_name = %s, child_nickname = %s, is_pregnant = %s, foetus_begin_date = %s, due_date_of_childbearing = %s, birthday = %s, gender = %s, child_picture = %s, parent_name = %s, parent_phone = %s, parent_email = %s,
            address = %s, description = %s, name_for_sort = %s, blood_type = %s, hobby = %s, allergy = %s, parent_job = %s, parent_name_dad = %s, parent_phone_dad = %s, parent_job_dad = %s WHERE child_parent_id = %s",
            secure($args['first_name']), secure($args['last_name']), secure($args['child_name']), secure($args['child_nickname']), secure($args['is_pregnant'], 'int'), secure($args['foetus_begin_date']), secure($args['due_date_of_childbearing']), secure($args['birthday']), secure($args['gender']), secure($args['child_picture']), secure($args['parent_name']), secure($args['parent_phone']),
            secure(strtolower($args['parent_email'])), secure($args['address']), secure($args['description']), secure($args['name_for_sort']), secure($args['blood_type']), secure($args['hobby']), secure($args['allergy']), secure($args['parent_job']), secure($args['parent_name_dad']), secure($args['parent_phone_dad']), secure($args['parent_job_dad']), secure($args['child_parent_id'], 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Cập nhật thông tin CHILD từ phụ huynh vào bảng ci_child
     *
     * @param array $args
     * @throws Exception
     */
    public function editChildByParentOfCiChild(array $args = array())
    {
        global $db;
        $this->_validateInputByParent($args);

        $strSql = sprintf("UPDATE ci_child SET child_nickname = %s, child_picture = %s, parent_name = %s, parent_phone = %s, parent_email = %s,
            address = %s, description = %s, blood_type = %s, hobby = %s, allergy = %s, parent_job = %s, parent_name_dad = %s, parent_phone_dad = %s, parent_job_dad = %s WHERE child_parent_id = %s AND child_id = %s",
            secure($args['child_nickname']), secure($args['child_picture']), secure($args['parent_name']), secure($args['parent_phone']),
            secure(strtolower($args['parent_email'])), secure($args['address']), secure($args['description']), secure($args['blood_type']), secure($args['hobby']), secure($args['allergy']), secure($args['parent_job']), secure($args['parent_name_dad']), secure($args['parent_phone_dad']), secure($args['parent_job_dad']), secure($args['child_parent_id'], 'int'), secure($args['child_id'],'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Thay đổi ảnh đại diện của trẻ
     *
     * @param array $args
     * @throws Exception
     */
    public function editChildAvatarForParent(array $args = array())
    {
        global $db;

        $strSql = sprintf("UPDATE ci_child_parent SET child_picture = %s WHERE child_parent_id = %s",
            secure($args['child_picture']), secure($args['child_parent_id'], 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Thay đổi ảnh đại diện của trẻ
     *
     * @param array $args
     * @throws Exception
     */
    public function editChildAvatar(array $args = array())
    {
        global $db;

        $strSql = sprintf("UPDATE ci_child SET child_picture = %s WHERE child_id = %s",
            secure($args['child_picture']), secure($args['child_id'], 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy ra danh sách trẻ đi học quản lý bởi 1 user
     *
     * @param $user_id
     * @return array
     * @throws Exception
     */
    public function getChildren($user_id) {
        global $db, $system;

        $strSql = "SELECT ci_child.*, ci_school_child.school_id FROM ci_child INNER JOIN ci_user_manage ON ci_child.child_id = ci_user_manage.object_id
                    AND ci_user_manage.object_type = %s AND ci_user_manage.user_id = %s 
                    LEFT JOIN ci_school_child ON ci_school_child.child_id = ci_child.child_id ORDER BY name_for_sort ASC";

        $children = array();
        $get_children = $db->query(sprintf($strSql, MANAGE_CHILD, secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if($get_children->num_rows > 0) {
            while($child = $get_children->fetch_assoc()) {
                if(!is_null($child['birthday'])) {
                    $child['birthday'] = toSysDate($child['birthday']);
                }
                if(!is_null($child['due_date_of_childbearing'])) {
                    $child['due_date_of_childbearing'] = toSysDate($child['due_date_of_childbearing']);
                }
                if (!is_empty($child['child_picture'])) {
                    $child['child_picture_path'] = $child['child_picture'];
                    $child['child_picture'] = $system['system_uploads'] . '/' . $child['child_picture'];
                }
                $date_now = date("Y-m-d");
                if($child['foetus_begin_date'] != null && $child['foetus_begin_date'] != '0000-00-00 00:00:00') {
                    $child['pregnant_week'] = (strtotime($date_now) - strtotime($child['foetus_begin_date'])) / (60 * 60 * 24 * 7);
                    $child['pregnant_week'] = (int)$child['pregnant_week'];
                } else {
                    $child['pregnant_week'] = null;
                }
                $children[] = $child;
            }
        }
        return $children;
    }

    /**
     * Lấy ra danh sách trẻ được quản lý bởi một user
     *
     * @param $user_id
     * @return array
     * @throws Exception
     */
    public function getChildrenOfParent($user_id) {
        global $db, $system;


        $children = array();
        $strSql = sprintf("SELECT PM.*, CP.*, SC.school_status FROM ci_parent_manage PM
                    INNER JOIN ci_child_parent CP ON CP.child_parent_id = PM.child_parent_id AND PM.user_id = %s
                    LEFT JOIN ci_school_configuration SC ON SC.school_id = PM.school_id
                    ORDER BY CP.name_for_sort ASC", secure($user_id, 'int'));
         $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_children->num_rows > 0) {
            while($child = $get_children->fetch_assoc()) {
                if(!is_null($child['birthday'])) {
                    $child['birthday'] = toSysDate($child['birthday']);
                }
                if(!is_null($child['due_date_of_childbearing'])) {
                    $child['due_date_of_childbearing'] = toSysDate($child['due_date_of_childbearing']);
                }
                if (!is_empty($child['child_picture'])) {
                    $child['child_picture_path'] = $child['child_picture'];
                    $child['child_picture'] = $system['system_uploads'] . '/' . $child['child_picture'];
                }
                $date_now = date("Y-m-d");
                if($child['foetus_begin_date'] != null && $child['foetus_begin_date'] != '0000-00-00 00:00:00') {
                    $child['pregnant_week'] = (strtotime($date_now) - strtotime($child['foetus_begin_date'])) / (60 * 60 * 24 * 7);
                    $child['pregnant_week'] = (int)$child['pregnant_week'];
                } else {
                    $child['pregnant_week'] = null;
                }
                $children[] = $child;
            }
        }
        return $children;
    }
    /**
     * Lấy ra info bản thân của user children
     *
     * @param $user_id
     * @return array
     * @throws Exception
     */
    public function getChildrenOfItSelf($user_id) {
        global $db, $system;


        $result = array();
        $strSql = sprintf("SELECT PM.*, CP.*, SC.school_status FROM ci_parent_manage PM
                    INNER JOIN ci_child_parent CP ON CP.child_parent_id = PM.child_parent_id 
                    LEFT JOIN ci_school_configuration SC ON SC.school_id = PM.school_id  JOIN ci_child C ON
                    C.child_code = CP.child_code JOIN ci_user_manage UM ON C.child_id = UM.object_id AND 
                    UM.user_id = %s ORDER BY CP.name_for_sort ASC", secure($user_id, 'int'));
        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_children->num_rows > 0) {
            while($child = $get_children->fetch_assoc()) {
                if(!is_null($child['birthday'])) {
                    $child['birthday'] = toSysDate($child['birthday']);
                }
                if(!is_null($child['due_date_of_childbearing'])) {
                    $child['due_date_of_childbearing'] = toSysDate($child['due_date_of_childbearing']);
                }
                if (!is_empty($child['child_picture'])) {
                    $child['child_picture_path'] = $child['child_picture'];
                    $child['child_picture'] = $system['system_uploads'] . '/' . $child['child_picture'];
                }
                $date_now = date("Y-m-d");
                if($child['foetus_begin_date'] != null && $child['foetus_begin_date'] != '0000-00-00 00:00:00') {
                    $child['pregnant_week'] = (strtotime($date_now) - strtotime($child['foetus_begin_date'])) / (60 * 60 * 24 * 7);
                    $child['pregnant_week'] = (int)$child['pregnant_week'];
                } else {
                    $child['pregnant_week'] = null;
                }
                $result = $child;
            }
        }
        return $result;
    }

    /**
     * Lấy ra danh sách ID các con của một phụ huynh học trong một trường.
     * Phục vụ: chức năng notification (tạo link).
     *
     * @param $school_id
     * @param $parent_id
     * @return array
     * @throws Exception
     */
    public function getChildIdBySchoolAndParent($school_id, $parent_id) {
        global $db;

        $strSql = sprintf("SELECT SC.child_id FROM ci_school_child SC INNER JOIN ci_user_manage UM ON SC.child_id = UM.object_id
                            AND SC.school_id = %s AND UM.object_type = %s AND UM.user_id = %s",
                            secure($school_id, 'int'), MANAGE_CHILD, secure($parent_id, 'int'));

        $childIds = array();
        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_children->num_rows > 0) {
            while($child = $get_children->fetch_assoc()) {
                $childIds[] = $child['child_id'];
            }
        }
        return $childIds;
    }

    /**
     * Lấy danh sách trẻ đang đi học của một trường và được quản lý bởi 1 phụ huynh
     *
     * @param $school_id
     * @param $parent_id
     * @return array
     * @throws Exception
     */
    public function getChildBySchoolAndParent($school_id, $parent_id) {
        global $db;

        $strSql = sprintf("SELECT C.child_id, C.child_name, G.group_title FROM ci_school_child SC INNER JOIN ci_user_manage UM ON SC.child_id = UM.object_id
                            AND SC.school_id = %s AND UM.object_type = %s AND UM.user_id = %s AND SC.status = 1
                            INNER JOIN ci_child C ON C.child_id = SC.child_id
                            INNER JOIN groups G ON G.group_id = SC.class_id",
            secure($school_id, 'int'), MANAGE_CHILD, secure($parent_id, 'int'));

        $childIds = array();
        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_children->num_rows > 0) {
            while($child = $get_children->fetch_assoc()) {
                $childIds[] = $child;
            }
        }
        return $childIds;
    }

    /**
     * Lấy ra danh sách ID các học sinh của  một trường.
     * Phục vụ: chức năng notification (tạo link).
     *
     * @param $school_id
     * @return array
     * @throws Exception
     */
    public function getChildIdBySchool($school_id) {
        global $db;

        $strSql = sprintf("SELECT child_id FROM ci_school_child
                            WHERE school_id = %s",
            secure($school_id, 'int'));

        $childIds = array();
        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_children->num_rows > 0) {
            while($child = $get_children->fetch_assoc()) {
                $childIds[] = $child['child_id'];
            }
        }
        return $childIds;
    }

    /**
     * Lấy số lượng trẻ của trường (không Join với ci_child => Lấy sai???)
     *
     * @param $school_id
     * @return int
     */
    public function getCountChildOfSchool($school_id) {
        global $db;

        $strSql = sprintf("SELECT count(child_id) AS cnt FROM ci_school_child
                            WHERE school_id = %s",
            secure($school_id, 'int'));

        $total_child = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($total_child->num_rows > 0) {
            return $total_child->fetch_assoc()['cnt'];
        }
        return 0;
    }

    /**
     * Lấy tổng trẻ của toàn trường (lấy đúng)
     *
     * @param $school_id
     * @return mixed
     */
    public function getCountChildOfSchoolForAPI($school_id) {
        global $db;

//        $strSql = sprintf('SELECT count(CC.child_id) AS cnt FROM select CC.child_id from ci_class_child CC INNER JOIN ci_school_child SC ON CC.class_id = SC.class_id AND CC.child_id = SC.child_id AND SC.school_id = %s AND SC.status = 1',
//            secure($school_id, 'int'));

        $strSql = sprintf('SELECT count(CC.child_id) AS cnt FROM ci_class_child CC INNER JOIN ci_school_child SC ON CC.class_id = SC.class_id AND CC.child_id = SC.child_id AND SC.school_id = %s AND SC.status = 1',
            secure($school_id, 'int'));

        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        return $get_children->fetch_assoc()['cnt'];
    }

    /**
     * Lấy số lượng trẻ đi học trong tháng
     *
     * @param $schoolId
     * @param $begin
     * @param $end
     * @return mixed
     * @throws Exception
     */
    public function getCountChildOfSchoolBasedMonth($schoolId, $begin, $end) {
        global $db;

        $begin = toDBDate($begin);
        $end = toDBDate($end);
        $strSql = sprintf('SELECT count(SC.child_id) AS cnt FROM ci_school_child SC 
                          INNER JOIN ci_child C ON C.child_id = SC.child_id
                          WHERE SC.school_id = %s AND (SC.begin_at <= %s AND (SC.end_at is null OR SC.end_at >= %s))', secure($schoolId, 'int'), secure($end), secure($begin));

        $get_children = $db->query($strSql) or _error('error', $strSql);

        return $get_children->fetch_assoc()['cnt'];
    }

    /**
     * Lấy số lượng trẻ của class
     *
     * @param $class_id
     * @return int
     */
    public function getCountChildOfClass($class_id) {
        global $db;

        $strSql = sprintf("SELECT count(child_id) AS cnt FROM ci_class_child
                            WHERE class_id = %s",
            secure($class_id, 'int'));

        $total_child = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($total_child->num_rows > 0) {
            return $total_child->fetch_assoc()['cnt'];
        }
        return 0;
    }

    /**
     * Lấy ra danh sách ID các con của một phụ huynh học trong một khối.
     * Phục vụ: chức năng notification (tạo link).
     *
     * @param $class_level_id
     * @param $parent_id
     * @return array
     */
    public function getChildIdByClassLevelAndParent($class_level_id, $parent_id) {
        global $db;

        $strSql = sprintf("SELECT CC.child_id FROM ci_class_child CC
                            INNER JOIN groups G ON CC.class_id = G.group_id AND G.class_level_id = %s
                            INNER JOIN ci_user_manage UM ON CC.child_id = UM.object_id AND UM.object_type = %s AND UM.user_id = %s",
                            secure($class_level_id, 'int'), MANAGE_CHILD, secure($parent_id, 'int'));

        $childIds = array();
        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_children->num_rows > 0) {
            while($child = $get_children->fetch_assoc()) {
                $childIds[] = $child['child_id'];
            }
        }
        return $childIds;
    }

    /**
     * Lấy ra danh sách ID các học sinh trong một khối.
     * Phục vụ: chức năng notification (tạo link).
     *
     * @param $class_level_id
     * @return array
     */
    public function getChildIdByClassLevel($class_level_id) {
        global $db;

        $strSql = sprintf("SELECT CC.child_id FROM ci_class_child CC
                           INNER JOIN groups G ON CC.class_id = G.group_id AND G.class_level_id = %s
                           INNER JOIN ci_school_child SC ON CC.child_id = SC.child_id AND SC.status = %s",
            secure($class_level_id, 'int'), secure(STATUS_ACTIVE, 'int'));

        $childIds = array();
        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_children->num_rows > 0) {
            while($child = $get_children->fetch_assoc()) {
                $childIds[] = $child['child_id'];
            }
        }
        return $childIds;
    }

    /**
     * Lấy ra danh sách ID các con của một phụ huynh học trong một lớp (thường là 1, có thể có sinh đôi).
     * Phục vụ: chức năng notification (tạo link).
     *
     * @param $class_id
     * @param $parent_id
     * @return array
     */
    public function getChildIdByClassAndParent($class_id, $parent_id) {
        global $db;

        $strSql = sprintf("SELECT CC.child_id FROM ci_class_child CC
                            INNER JOIN ci_user_manage UM ON CC.child_id = UM.object_id AND CC.class_id = %s AND CC.status = %s AND UM.object_type = %s AND UM.user_id = %s",
                            secure($class_id, 'int'), secure(STATUS_ACTIVE, 'int'), MANAGE_CHILD, secure($parent_id, 'int'));

        $childIds = array();
        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_children->num_rows > 0) {
            while($child = $get_children->fetch_assoc()) {
                $childIds[] = $child['child_id'];
            }
        }
        return $childIds;
    }

    /**
     * Lấy ra danh sách childId của một lớp
     *
     * @param $class_id
     * @return array
     * @throws Exception
     */
    public function getChildIdOfClass($class_id) {
        global $db;

        $strSql = sprintf("SELECT child_id FROM ci_class_child WHERE class_id = %s", secure($class_id, 'int'));

        $childIds = array();
        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_children->num_rows > 0) {
            while($child = $get_children->fetch_assoc()) {
                $childIds[] = $child['child_id'];
            }
        }
        return $childIds;
    }

    /**
     * Lấy ra danh sách childId của một lớp
     *
     * @param $class_id
     * @return array
     * @throws Exception
     */
    public function checkChildInClass($child_id, $class_id) {
        global $db;

        $strSql = sprintf("SELECT child_id FROM ci_class_child WHERE child_id = %s AND class_id = %s", secure($child_id, 'int'), secure($class_id, 'int'));

        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        return ($get_children->num_rows > 0);
    }

    /**
     * Lấy ra danh sách ID các con của một phụ huynh phải trả một khoản học phí (thường là 1, có thể có sinh đôi).
     * Phục vụ: chức năng notification (tạo link).
     *
     * @param $tuition_child_id
     * @param $parent_id
     * @return array
     */
    public function getChildIdByTuitionChildAndParent($tuition_child_id, $parent_id) {
        global $db;

        $strSql = sprintf("SELECT TC.child_id FROM ci_tuition_child TC
                            INNER JOIN ci_user_manage UM ON TC.child_id = UM.object_id AND TC.tuition_child_id = %s AND UM.object_type = %s AND UM.user_id = %s",
            secure($tuition_child_id, 'int'), MANAGE_CHILD, secure($parent_id, 'int'));

        $childIds = array();
        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_children->num_rows > 0) {
            while($child = $get_children->fetch_assoc()) {
                $childIds[] = $child['child_id'];
            }
        }
        return $childIds;
    }

    /**
     * Kiểm tra user hiện tại có phải phụ huynh của trẻ không
     *
     * @param $child_id
     * @return bool
     * @throws Exception
     */
    public function isParent($child_id) {
        global $db, $user;

        $strSql = sprintf("SELECT object_id FROM ci_user_manage WHERE object_type = %s AND user_id = %s AND object_id = %s", MANAGE_CHILD, secure($user->_data['user_id'], 'int'), secure($child_id, 'int'));

        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        return ($get_children->num_rows > 0);
    }
    /**
     * Lấy ra thông tin của trẻ
     *
     * @param $childId
     * @return array|null
     * @throws Exception
     */
    public function getChildbyChildId($childId) {
        global $db, $system;

        $strSql = "SELECT C.*, SC.*, CP.child_admin FROM ci_child C INNER JOIN ci_school_child SC ON C.child_id = %s AND C.child_id = SC.child_id
                    LEFT JOIN ci_child_parent CP ON C.child_parent_id = CP.child_parent_id";
        $child = null;
        $get_child = $db->query(sprintf($strSql, secure($childId, 'int'))) or _error(SQL_ERROR_THROWEN);
        if($get_child->num_rows > 0) {
            $child = $get_child->fetch_assoc();
            $child['birthday'] = toSysDate($child['birthday']);
            $child['begin_at'] = toSysDate($child['begin_at']);
            $child['end_at'] = toSysDate($child['end_at']);
            if (!is_empty($child['child_picture'])) {
                $child['child_picture_path'] = $child['child_picture'];
                $child['child_picture'] = $system['system_uploads'] . '/' . $child['child_picture'];
            }
        }
        return $child;
    }
    /**
     * Lấy ra thông tin của trẻ
     *
     * @param $childId
     * @return array|null
     * @throws Exception
     */
    public function getChild($childId) {
        global $db, $system;

        $strSql = "SELECT C.*, SC.*, CP.child_admin FROM ci_child C INNER JOIN ci_school_child SC ON C.child_id = %s AND C.child_id = SC.child_id
                    LEFT JOIN ci_child_parent CP ON C.child_parent_id = CP.child_parent_id";
        $child = null;
        $get_child = $db->query(sprintf($strSql, secure($childId, 'int'))) or _error(SQL_ERROR_THROWEN);
        if($get_child->num_rows > 0) {
            $child = $get_child->fetch_assoc();
            $child['birthday'] = toSysDate($child['birthday']);
            $child['begin_at'] = toSysDate($child['begin_at']);
            $child['end_at'] = toSysDate($child['end_at']);
            if (!is_empty($child['child_picture'])) {
                $child['child_picture_path'] = $child['child_picture'];
                $child['child_picture'] = $system['system_uploads'] . '/' . $child['child_picture'];
            }
        }
        return $child;
    }

    /**
     * Lấy ra thông tin trẻ để thực hiện chức năng nghỉ học
     *
     * @param $childId
     * @return array|null
     * @throws Exception
     */
    public function getChild4Leave($childId) {
        global $db, $system;

        $strSql = "SELECT C.*, SC.class_id, SC.end_at, G.group_title FROM ci_child C INNER JOIN ci_school_child SC ON C.child_id = %s AND C.child_id = SC.child_id
                    LEFT JOIN groups G ON SC.class_id = G.group_id";
        $child = null;
        $get_child = $db->query(sprintf($strSql, secure($childId, 'int'))) or _error(SQL_ERROR_THROWEN);
        if($get_child->num_rows > 0) {
            $child = $get_child->fetch_assoc();
            $child['birthday'] = toSysDate($child['birthday']);
            $child['begin_at'] = toSysDate($child['begin_at']);
            $child['end_at'] = toSysDate($child['end_at']);
            if (!is_empty($child['child_picture'])) {
                $child['child_picture_path'] = $child['child_picture'];
                $child['child_picture'] = $system['system_uploads'] . '/' . $child['child_picture'];
            }
        }

        return $child;
    }

    /**
     * Lấy ra thông tin của trẻ  từ màn hình phụ huynh
     *
     * @param $childId
     * @return array|null
     * @throws Exception
     */
    public function getChildByParent($childParentId) {
        global $db, $system;

        $strSql = sprintf("SELECT * FROM ci_child_parent WHERE child_parent_id = %s", secure($childParentId, 'int'));
        $child = null;
        $get_child = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_child->num_rows > 0) {
            $child = $get_child->fetch_assoc();
            $child['parent_name'] = convertText4Web($child['parent_name']);
            $child['first_name'] = convertText4Web($child['first_name']);
            $child['last_name'] = convertText4Web($child['last_name']);
            $child['child_name'] = convertText4Web($child['child_name']);
            if(!is_null($child['birthday'])) {
                $child['birthday'] = toSysDate($child['birthday']);
            }
            if(!is_null($child['due_date_of_childbearing'])) {
                $child['due_date_of_childbearing'] = toSysDate($child['due_date_of_childbearing']);
            }
            if($child['begin_at'] != null) {
                $child['begin_at'] = toSysDate($child['begin_at']);
            }
            if (!is_empty($child['child_picture'])) {
                $child['child_picture_path'] = $child['child_picture'];
                $child['child_picture'] = $system['system_uploads'] . '/' . $child['child_picture'];
            }
            $date_now = date("Y-m-d");
            if($child['foetus_begin_date'] != null && $child['foetus_begin_date'] != '0000-00-00 00:00:00') {
                $child['pregnant_week'] = (strtotime($date_now) - strtotime($child['foetus_begin_date'])) / (60 * 60 * 24 * 7);
                $child['pregnant_week'] = (int)$child['pregnant_week'];
            } else {
                $child['pregnant_week'] = null;
            }
            //Lấy ra thông tin trường và child_id của trường
            $strSql = sprintf("SELECT school_id, child_id FROM ci_parent_manage
                               WHERE child_parent_id = %s", secure($childParentId, 'int'));

            $get_school = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            if($get_school->num_rows > 0) {
                $school_id = $get_school->fetch_assoc();
                $child['school_id'] = $school_id['school_id'];
                $child['child_id'] = $school_id['child_id'];
            } else {
                $child['school_id'] = 0;
                $child['child_id'] = 0;
            }
        }
        return $child;
    }

    /**
     * Kiểm tra trẻ theo có đang học ở trường nào hay không? (status = ACTIVE)
     *
     * @param $childCode
     * @param $schoolId
     * @return array|null
     * @throws Exception
     */
    public function getStatusChildInSchool($childCode, $schoolId) {
        global $db;

        $strSql = sprintf("SELECT CP.child_parent_id, SC.school_id FROM ci_child_parent CP 
                           LEFT JOIN ci_school_child SC ON SC.child_parent_id = CP.child_parent_id AND SC.status = %s
                           WHERE CP.child_code = %s", secure(STATUS_ACTIVE, 'int'), secure($childCode));

        $child = null;
        $get_child = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_child->num_rows > 0) {
            $child = $get_child->fetch_assoc();
            $child['status'] = 1;
            if ($child['school_id'] == $schoolId) {
                $child['status'] = 2;
            } elseif ($child['school_id'] > 0) {
                $child['status'] = 3;
            }
        } else {
            $child['status'] = 0;
        }
        return $child;
    }
    /**
     * Lấy ra thông tin nghỉ có phép hoặc không phép của trẻ
     *
     * @param $childId
     * @param $classId
     * @return array|null
     * @throws Exception
     */
    public function getAbsentsChild($childId, $classId) {
        global $db;
        $strSql = sprintf("SELECT COALESCE(sum(CASE WHEN status = 0 THEN 1 ELSE 0 END),0) as absent_true,COALESCE(sum(CASE WHEN status = 4 THEN 1 ELSE 0 END),0) as absent_false
                                FROM (SELECT cad.* FROM `ci_attendance` ca JOIN `ci_attendance_detail` cad 
                                    ON ca.attendance_id = cad.attendance_id AND ca.class_id = %s AND cad.child_id=%s 
                                ORDER BY 1) x WHERE x.child_id=%s",secure($classId),secure($childId),secure($childId));
        $results = array();
        $get_child_Absents = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_child_Absents->num_rows > 0) {
            while ($detail = $get_child_Absents->fetch_assoc()) {
                $results = $detail;
            }
        }
        return $results;
    }
    /**
     * Lấy ra thông tin của trẻ  để thêm mới bằng chid code
     *
     * @param $childCode
     * @return array|null
     * @throws Exception
     */
    public function getChildByCode($childCode) {
        global $db, $system;

        $strSql = sprintf("SELECT child_parent_id, child_code, first_name, last_name, child_name, child_nickname, birthday, 
                           gender, child_picture, child_admin, parent_name, parent_phone, parent_email, address, description, 
                           created_user_id, name_for_sort FROM ci_child_parent
                           WHERE child_code = %s", secure($childCode));


        $child = null;
        $get_child = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_child->num_rows > 0) {
            $child = $get_child->fetch_assoc();
            $child['parent_name'] = convertText4Web($child['parent_name']);
            if(!is_null($child['birthday'])) {
                $child['birthday'] = toSysDate($child['birthday']);
            }

            if (!is_empty($child['child_picture'])) {
                $child['child_picture'] = $system['system_uploads'] . '/' . $child['child_picture'];
            }
        }
        return $child;
    }

    /**
     * Lấy ra thông tin của trẻ  để thêm mới bằng chid code
     *
     * @param $childId
     * @return array|null
     * @throws Exception
     */
    public function getParentManageChild($childParentId) {
        global $db;

        $results = array();
        $strSql = sprintf("SELECT U.user_id, U.user_name, U.user_fullname, U.user_gender, U.user_picture, U.user_phone, U.user_email FROM users U
                           INNER JOIN ci_parent_manage PM ON U.user_id = PM.user_id AND PM.child_parent_id = %s", secure($childParentId, 'int'));
        $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_users->num_rows > 0) {
            while ($user = $get_users->fetch_assoc()) {
                $user['user_picture'] = get_picture($user['user_picture'], $user['user_gender']);
                $results[] = $user;
            }
        }
        return $results;
    }

    /**
     * Lấy ra thông tin child_name, phục vụ tạo notification
     *
     * @param $childId
     * @return array|null
     * @throws Exception
     */
    public function getChildName($childId) {
        global $db;

        $strSql = "SELECT child_name FROM ci_child WHERE child_id = %s";
        $get_child = $db->query(sprintf($strSql, secure($childId, 'int'))) or _error(SQL_ERROR_THROWEN);
        if($get_child->num_rows > 0) {
            return $get_child->fetch_assoc()['child_name'];
        }
        return null;
    }

    /**
     * Lấy ra danh sách children của một trường
     *
     * @param $school_id
     * @return array
     * @throws Exception
     */
    public function getChildrenOfSchool($school_id) {
        global $db;

        $strSql = "SELECT SC.begin_at, SC.status, C.child_id, C.child_name, C.birthday, C.gender, C.parent_phone, C.address FROM ci_school_child SC INNER JOIN ci_child C ON SC.child_id = C.child_id
                    WHERE SC.school_id = %s ORDER BY SC.status DESC, C.name_for_sort ASC";

        $children = array();
        $get_children = $db->query(sprintf($strSql, secure($school_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if($get_children->num_rows > 0) {
            while($child = $get_children->fetch_assoc()) {
                $child['birthday'] = toSysDate($child['birthday']);
                $children[] = $child;
            }
        }
        return $children;
    }

    /**
     * Lấy ra số lượng học sinh nam và nữ của trường
     *
     * @param $school_id
     * @return array
     */
    public function getGenderCount($school_id) {
        global $db;

        $strSql = "SELECT C.gender, count(SC.child_id) AS cnt FROM ci_school_child SC INNER JOIN ci_child C ON SC.child_id = C.child_id
                    WHERE SC.school_id = %s GROUP BY C.gender";

        $get_cnts = $db->query(sprintf($strSql, secure($school_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        $cnts = array();
        if($get_cnts->num_rows > 0) {
            while($cnt = $get_cnts->fetch_assoc()) {
                $cnts[] = $cnt;
            }
        }
        return $cnts;
    }

    /**
     * Tìm danh sách child trong hệ thống theo điều kiện đầu vào.
     *
     * @param $keyword
     * @param $school_id
     * @param int|string $class_id
     * @param int $status
     * @return array
     * @throws Exception
     */
    public function search($keyword, $school_id, $class_id = '', $status = STATUS_ACTIVE)
    {
        global $db, $system;

        $whereClause = null;
        if (empty($keyword)) {
            $whereClause = " ";
        } else {
            $whereClause = sprintf(' AND (C.child_name LIKE %1$s OR C.description LIKE %1$s) ', secure($keyword, 'search'));
        }

        $result = array();
        $children = array();
        $strSql = null;
        if (is_empty($class_id)) {
            $strSql = sprintf('SELECT C.*, SC.begin_at, SC.end_at, SC.class_id, G.group_title, G.group_name FROM ci_school_child SC
                            INNER JOIN ci_child C ON SC.school_id = %1$s AND SC.child_id = C.child_id AND SC.status = %2$s %3$s
                            LEFT JOIN groups G ON SC.class_id = G.group_id
                            ORDER BY SC.class_id ASC, C.name_for_sort ASC',
                secure($school_id, 'int'), secure($status, 'int'), $whereClause);
        } else if ($class_id == 0) {
            $strSql = sprintf('SELECT C.*, SC.begin_at, SC.end_at, SC.class_id FROM ci_school_child SC
                            INNER JOIN ci_child C ON SC.school_id = %1$s AND SC.class_id = 0 AND SC.child_id = C.child_id AND SC.status = %2$s %3$s
                            ORDER BY SC.begin_at ASC',
                secure($school_id, 'int'), secure($status, 'int'), $whereClause);
        } else {
            $strSql = sprintf('SELECT C.*, SC.begin_at, SC.end_at, SC.class_id, G.group_title, G.group_name FROM ci_school_child SC
                            INNER JOIN ci_child C ON SC.class_id = %1$s AND SC.child_id = C.child_id AND SC.status = %2$s %3$s
                            LEFT JOIN groups G ON SC.class_id = G.group_id
                            ORDER BY C.name_for_sort ASC',
                secure($class_id, 'int'), secure($status, 'int'), $whereClause);
        }
        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $maleCount = 0;
        if ($get_children->num_rows > 0) {
            while ($child = $get_children->fetch_assoc()) {
                $child['parent'] = $this->getParent($child['child_id']);
                $child['birthday'] = toSysDate($child['birthday']);
                $children[] = $child;
                if ($child['gender'] == MALE) $maleCount++;
            }
        }
        $result['children'] = $children;
        $result['male_count'] = $maleCount;
        $result['female_count'] = count($children) - $maleCount;

        return $result;
    }

    /**
     * Tìm danh sách child trong hệ thống theo điều kiện đầu vào có tháng tuổi
     *
     * @param $keyword
     * @param $school_id
     * @param int|string $class_id
     * @param int|string $child_month
     * @param int $status
     * @return array
     * @throws Exception
     */
    public function searchHaveMonth($keyword, $school_id, $class_id = '', $child_month, $status = STATUS_ACTIVE)
    {
        global $db, $system;

        $whereClause = null;
        if (empty($keyword)) {
            $whereClause = " ";
        } else {
            $whereClause = sprintf(' AND (C.child_name LIKE %1$s OR C.description LIKE %1$s) ', secure($keyword, 'search'));
        }

        // Điều kiện search theo tháng tuổi
        if($child_month == 0) {
            $whereClauseAge = " ";
        } else {
            $whereClauseAge = sprintf(' AND (TIMESTAMPDIFF(month, C.birthday, CURRENT_DATE())) = %s ', secure($child_month, 'int'));
        }

        $result = array();
        $children = array();
        $strSql = null;
        if (is_empty($class_id)) {
            $strSql = sprintf('SELECT C.*, CP.child_admin, SC.begin_at, SC.end_at, SC.class_id, G.group_title, G.group_name FROM ci_school_child SC
                            INNER JOIN ci_child C ON SC.school_id = %1$s AND SC.child_id = C.child_id AND SC.status = %2$s %3$s %4$s
                            INNER JOIN ci_child_parent CP ON CP.child_parent_id = C.child_parent_id
                            LEFT JOIN groups G ON SC.class_id = G.group_id
                            ORDER BY SC.class_id ASC, C.name_for_sort ASC',
                secure($school_id, 'int'), secure($status, 'int'), $whereClause, $whereClauseAge);
        } else if ($class_id == 0) {
            $strSql = sprintf('SELECT C.*, CP.child_admin, SC.begin_at, SC.end_at, SC.class_id FROM ci_school_child SC
                            INNER JOIN ci_child C ON SC.school_id = %1$s AND SC.class_id = 0 AND SC.child_id = C.child_id AND SC.status = %2$s %3$s %4$s
                            INNER JOIN ci_child_parent CP ON CP.child_parent_id = C.child_parent_id
                            ORDER BY SC.begin_at ASC',
                secure($school_id, 'int'), secure($status, 'int'), $whereClause, $whereClauseAge);
        } else {
            $strSql = sprintf('SELECT C.*, CP.child_admin, SC.begin_at, SC.end_at, SC.class_id, G.group_title, G.group_name FROM ci_school_child SC
                            INNER JOIN ci_child C ON SC.class_id = %1$s AND SC.child_id = C.child_id AND SC.status = %2$s %3$s %4$s
                            INNER JOIN ci_child_parent CP ON CP.child_parent_id = C.child_parent_id
                            LEFT JOIN groups G ON SC.class_id = G.group_id
                            ORDER BY C.name_for_sort ASC',
                secure($class_id, 'int'), secure($status, 'int'), $whereClause, $whereClauseAge);
        }
        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $maleCount = 0;
        if ($get_children->num_rows > 0) {
            while ($child = $get_children->fetch_assoc()) {
                $child['parent'] = $this->getParent($child['child_id']);
                $child['birthday'] = toSysDate($child['birthday']);
                $children[] = $child;
                if ($child['gender'] == MALE) $maleCount++;
            }
        }
        $result['children'] = $children;
        $result['male_count'] = $maleCount;
        $result['female_count'] = count($children) - $maleCount;

        return $result;
    }

    /**
     * Đếm số lượng trẻ tìm được theo điều kiện nhập vào
     *
     * @param $keyword
     * @param $school_id
     * @param string $class_id
     * @return array
     * @throws Exception
     */
    public function searchCount($keyword, $school_id, $class_id = '', $status = STATUS_ACTIVE)
    {
        global $db, $system;

        $whereClause = null;
        if (empty($keyword)) {
            $whereClause = " ";
        } else {
            $whereClause = sprintf(' AND (C.child_name LIKE %1$s OR C.description LIKE %1$s) ', secure($keyword, 'search'));
        }

        $strSql = null;
        if (is_empty($class_id)) {
            //Trường hợp tìm toàn trường
            $strSql = sprintf('SELECT count(C.child_id) AS cnt FROM ci_school_child SC
                            INNER JOIN ci_child C ON SC.school_id = %1$s AND SC.child_id = C.child_id %2$s AND SC.status = %3$s
                            LEFT JOIN groups G ON SC.class_id = G.group_id WHERE SC.is_delete = 0
                            ORDER BY SC.class_id ASC, C.name_for_sort ASC',
                secure($school_id, 'int'), $whereClause, secure($status, 'int'));
        } else if ($class_id == 0) {
            //Trường hợp tìm những trẻ chưa phân lớp
            $strSql = sprintf('SELECT count(C.child_id) AS cnt FROM ci_school_child SC
                            INNER JOIN ci_child C ON SC.school_id = %1$s AND SC.class_id = 0 AND SC.child_id = C.child_id %2$s AND SC.status = %3$s
                            ORDER BY SC.begin_at ASC',
                secure($school_id, 'int'), $whereClause, secure($status, 'int'));
        } else {
            //Trường hợp tìm trẻ trong một lớp
            $strSql = sprintf('SELECT count(C.child_id) AS cnt FROM ci_school_child SC
                            INNER JOIN ci_child C ON SC.class_id = %1$s AND SC.child_id = C.child_id %2$s AND SC.status = %3$s
                            LEFT JOIN groups G ON SC.class_id = G.group_id
                            ORDER BY C.name_for_sort ASC',
                secure($class_id, 'int'), $whereClause, secure($status, 'int'));
            if($status == STATUS_INACTIVE) {
                $strSql = sprintf('SELECT count(C.child_id) AS cnt FROM ci_school_child SC
                            INNER JOIN ci_child C ON SC.class_id = %1$s AND SC.child_id = C.child_id %2$s AND SC.status = %3$s
                            LEFT JOIN groups G ON SC.class_id = G.group_id
                            WHERE SC.is_delete = 0
                            ORDER BY C.name_for_sort ASC',
                    secure($class_id, 'int'), $whereClause, secure($status, 'int'));
            }
        }
        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        return $get_children->fetch_assoc()['cnt'];
    }

    /**
     * Đếm số lượng trẻ tìm được theo điều kiện nhập vào (thêm search theo tuổi - Taila)
     *
     * @param $keyword
     * @param $school_id
     * @param string $class_id
     * @param $child_age
     * @return array
     * @throws Exception
     */
    public function searchCountHaveMonth($keyword, $school_id, $class_id = '', $child_month, $status = STATUS_ACTIVE)
    {
        global $db;

        $whereClause = null;
        if (empty($keyword)) {
            $whereClause = " ";
        } else {
            $whereClause = sprintf(' AND (C.child_name LIKE %1$s OR C.description LIKE %1$s) ', secure($keyword, 'search'));
        }
        // Điều kiện search theo tháng tuổi
        if($child_month == 0) {
            $whereClauseAge = " ";
        } else {
            $whereClauseAge = sprintf(' AND (TIMESTAMPDIFF(month, C.birthday, CURRENT_DATE())) = %s ', secure($child_month, 'int'));
        }

        $strSql = null;
        if (is_empty($class_id)) {
            //Trường hợp tìm toàn trường
            $strSql = sprintf('SELECT count(C.child_id) AS cnt FROM ci_school_child SC
                            INNER JOIN ci_child C ON SC.school_id = %1$s AND SC.child_id = C.child_id %2$s %3$s AND SC.status = %4$s
                            LEFT JOIN groups G ON SC.class_id = G.group_id
                            ORDER BY SC.class_id ASC, C.name_for_sort ASC',
                secure($school_id, 'int'), $whereClause, $whereClauseAge, secure($status, 'int'));
        } else if ($class_id == 0) {
            //Trường hợp tìm những trẻ chưa phân lớp
            $strSql = sprintf('SELECT count(C.child_id) AS cnt FROM ci_school_child SC
                            INNER JOIN ci_child C ON SC.school_id = %1$s AND SC.class_id = 0 AND SC.child_id = C.child_id %2$s %3$s AND SC.status = %4$s
                            ORDER BY SC.begin_at ASC',
                secure($school_id, 'int'), $whereClause, $whereClauseAge, secure($status, 'int'));
        } else {
            //Trường hợp tìm trẻ trong một lớp
            $strSql = sprintf('SELECT count(C.child_id) AS cnt FROM ci_school_child SC
                            INNER JOIN ci_child C ON SC.class_id = %1$s AND SC.child_id = C.child_id %2$s %3$s AND SC.status = %4$s
                            LEFT JOIN groups G ON SC.class_id = G.group_id
                            ORDER BY C.name_for_sort ASC',
                secure($class_id, 'int'), $whereClause, $whereClauseAge, secure($status, 'int'));
        }
        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        return $get_children->fetch_assoc()['cnt'];
    }

    /**
     * Tìm kiếm danh sách trẻ theo điều kiện nhập vào, hỗ trợ paging
     *
     * @param $keyword
     * @param $school_id
     * @param string $class_id
     * @param int $page
     * @param int $limit
     * @param int $status
     * @return array
     * @throws Exception
     */
    public function searchPaging($keyword, $school_id, $class_id = '', $page = 1, $limit = 30, $status = STATUS_ACTIVE, $is_noga = 0)
    {
        global $db, $system;

        $whereClause = null;
        if (empty($keyword)) {
            $whereClause = " ";
        } else {
            $whereClause = sprintf(' AND (C.child_name LIKE %1$s OR C.description LIKE %1$s) ', secure($keyword, 'search'));
        }

        //$result = array();
        $children = array();
        $strSql = null;
        if (is_empty($class_id)) {
            //Trường hợp tìm toàn trường
            $strSql = sprintf('SELECT C.*, SC.begin_at, SC.end_at, SC.class_id, G.group_title, G.group_name FROM ci_school_child SC
                            INNER JOIN ci_child C ON SC.school_id = %1$s AND SC.child_id = C.child_id %2$s AND SC.status = %3$s
                            LEFT JOIN groups G ON SC.class_id = G.group_id
                            ORDER BY SC.class_id ASC, C.name_for_sort ASC
                            LIMIT %4$s, %5$s',
                secure($school_id, 'int'), $whereClause, secure($status, 'int'), ($page - 1)*$limit, $limit);
            if($status == STATUS_INACTIVE) {
                $strSql = sprintf('SELECT C.*, SC.begin_at, SC.end_at, SC.class_id, G.group_title, G.group_name FROM ci_school_child SC
                            INNER JOIN ci_child C ON SC.school_id = %1$s AND SC.child_id = C.child_id %2$s AND SC.status = %3$s
                            LEFT JOIN groups G ON SC.class_id = G.group_id
                            WHERE SC.is_delete = 0
                            ORDER BY SC.class_id ASC, C.name_for_sort ASC
                            LIMIT %4$s, %5$s',
                    secure($school_id, 'int'), $whereClause, secure($status, 'int'), ($page - 1)*$limit, $limit);
            }
        } else if ($class_id == 0) {
            //Trường hợp tìm những trẻ chưa phân lớp
            $strSql = sprintf('SELECT C.*, SC.begin_at, SC.end_at, SC.class_id FROM ci_school_child SC
                            INNER JOIN ci_child C ON SC.school_id = %1$s AND SC.class_id = 0 AND SC.child_id = C.child_id %2$s AND SC.status = %3$s
                            ORDER BY SC.begin_at ASC
                            LIMIT %4$s, %5$s',
                secure($school_id, 'int'), $whereClause, secure($status, 'int'), ($page - 1)*$limit, $limit);
        } else {
            //Trường hợp tìm trẻ trong một lớp
            $strSql = sprintf('SELECT C.*, SC.begin_at, SC.end_at, SC.class_id, G.group_title, G.group_name FROM ci_school_child SC
                            INNER JOIN ci_child C ON SC.class_id = %1$s AND SC.child_id = C.child_id %2$s AND SC.status = %3$s
                            LEFT JOIN groups G ON SC.class_id = G.group_id
                            ORDER BY C.name_for_sort ASC
                            LIMIT %4$s, %5$s',
                secure($class_id, 'int'), $whereClause, secure($status, 'int'), ($page - 1)*$limit, $limit);
            if($status == STATUS_INACTIVE) {
                $strSql = sprintf('SELECT C.*, SC.begin_at, SC.end_at, SC.class_id, G.group_title, G.group_name FROM ci_school_child SC
                            INNER JOIN ci_child C ON SC.class_id = %1$s AND SC.child_id = C.child_id %2$s AND SC.status = %3$s
                            LEFT JOIN groups G ON SC.class_id = G.group_id
                            WHERE SC.is_delete = 0
                            ORDER BY C.name_for_sort ASC
                          
                            LIMIT %4$s, %5$s',
                    secure($class_id, 'int'), $whereClause, secure($status, 'int'), ($page - 1)*$limit, $limit);
            }
        }
        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        //$maleCount = 0;
        if ($get_children->num_rows > 0) {
            $index = ($page - 1)*$limit + 1;
            while ($child = $get_children->fetch_assoc()) {
                $child['index'] = $index++;
                $child['parent'] = $this->getParent($child['child_id'], $is_noga);
                $child['birthday'] = toSysDate($child['birthday']);
                $child['begin_at'] = toSysDate($child['begin_at']);
                $child['end_at'] = toSysDate($child['end_at']);
                $children[] = $child;
                //if ($child['gender'] == MALE) $maleCount++;
            }
        }
        //$result['children'] = $children;
        //$result['male_count'] = $maleCount;
        //$result['female_count'] = count($children) - $maleCount;

        return $children;
    }

    /**
     * Tìm kiếm danh sách trẻ theo điều kiện nhập vào, hỗ trợ paging
     *
     * @param $keyword
     * @param $school_id
     * @param string $class_id
     * @param int $page
     * @param int $limit
     * @param int $status
     * @return array
     * @throws Exception
     */
    public function searchPagingHaveMonth($keyword, $school_id, $class_id = '', $child_month, $page = 1, $limit = 30, $status = STATUS_ACTIVE)
    {
        global $db, $system;

        $whereClause = null;
        if (empty($keyword)) {
            $whereClause = " ";
        } else {
            $whereClause = sprintf(' AND (C.child_name LIKE %1$s OR C.description LIKE %1$s) ', secure($keyword, 'search'));
        }
        // Điều kiện search theo tháng tuổi
        if($child_month == 0) {
            $whereClauseAge = " ";
        } else {
            $whereClauseAge = sprintf(' AND (TIMESTAMPDIFF(month, C.birthday, CURRENT_DATE())) = %s ', secure($child_month, 'int'));
        }
        //$result = array();
        $children = array();
        $strSql = null;
        if (is_empty($class_id)) {
            //Trường hợp tìm toàn trường
            $strSql = sprintf('SELECT C.*, SC.begin_at, SC.end_at, SC.class_id, G.group_title, G.group_name FROM ci_school_child SC
                            INNER JOIN ci_child C ON SC.school_id = %1$s AND SC.child_id = C.child_id %2$s %3$s AND SC.status = %4$s
                            LEFT JOIN groups G ON SC.class_id = G.group_id
                            ORDER BY SC.class_id ASC, C.name_for_sort ASC
                            LIMIT %5$s, %6$s',
                secure($school_id, 'int'), $whereClause, $whereClauseAge, secure($status, 'int'), ($page - 1)*$limit, $limit);
        } else if ($class_id == 0) {
            //Trường hợp tìm những trẻ chưa phân lớp
            $strSql = sprintf('SELECT C.*, SC.begin_at, SC.end_at, SC.class_id FROM ci_school_child SC
                            INNER JOIN ci_child C ON SC.school_id = %1$s AND SC.class_id = 0 AND SC.child_id = C.child_id %2$s %3$s AND SC.status = %4$s
                            ORDER BY SC.begin_at ASC
                            LIMIT %5$s, %6$s',
                secure($school_id, 'int'), $whereClause, $whereClauseAge, secure($status, 'int'), ($page - 1)*$limit, $limit);
        } else {
            //Trường hợp tìm trẻ trong một lớp
            $strSql = sprintf('SELECT C.*, SC.begin_at, SC.end_at, SC.class_id, G.group_title, G.group_name FROM ci_school_child SC
                            INNER JOIN ci_child C ON SC.class_id = %1$s AND SC.child_id = C.child_id %2$s %3$s AND SC.status = %4$s
                            LEFT JOIN groups G ON SC.class_id = G.group_id
                            ORDER BY C.name_for_sort ASC
                            LIMIT %5$s, %6$s',
                secure($class_id, 'int'), $whereClause, $whereClauseAge, secure($status, 'int'), ($page - 1)*$limit, $limit);
        }

        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        //$maleCount = 0;
        if ($get_children->num_rows > 0) {
            $index = ($page - 1)*$limit + 1;
            while ($child = $get_children->fetch_assoc()) {
                $child['index'] = $index++;
                $child['parent'] = $this->getParent($child['child_id']);
                $child['birthday'] = toSysDate($child['birthday']);
                $child['begin_at'] = toSysDate($child['begin_at']);
                $child['end_at'] = toSysDate($child['end_at']);
                $children[] = $child;
                //if ($child['gender'] == MALE) $maleCount++;
            }
        }
        //$result['children'] = $children;
        //$result['male_count'] = $maleCount;
        //$result['female_count'] = count($children) - $maleCount;

        return $children;
    }

    /**
     * Lấy ra danh sách trẻ và phụ huynh của một lớp (danh sách phụ huynh được đưa vào từng trẻ tương ứng).
     *
     * @param $class_id
     * @return array
     * @throws Exception
     */
    function getChildNParentOfClass($class_id) {
        global $db;

        $results = array();
        $strSql = sprintf("SELECT CC.* , C.child_name, C.birthday, C.gender, C.parent_phone FROM ci_class_child CC INNER JOIN ci_child C ON CC.child_id = C.child_id
            WHERE CC.class_id = %s ORDER BY C.name_for_sort ASC", secure($class_id, 'int'));

        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_children->num_rows > 0) {
            while ($child = $get_children->fetch_assoc()) {
                $child['parent'] = $this->getParent($child['child_id']);
                $child['birthday'] = toSysDate($child['birthday']);
                $results[] = $child;
            }
        }

        return $results;
    }

    /** Lấy ra danh sách phụ huynh và trẻ từ danh sách ID trẻ nhập vào.
     * Hàm này phục vụ chức năng NOTIFICATION
     *
     * @param $childIds
     * @return array
     * @throws Exception
     */
    function getListChildNParentId($childIds) {
        global $db;

        $results = array();
        $strCon = implode(',', $childIds);
        $strSql = sprintf("SELECT C.child_id, C.child_name, UM.user_id AS parent_id FROM ci_user_manage UM
                            INNER JOIN ci_child C ON UM.object_id = C.child_id AND C.child_id IN (%s)
                            INNER JOIN ci_school_child SC ON C.child_id = SC.child_id AND SC.status = %s
                            WHERE UM.object_type = %s", $strCon, secure(STATUS_ACTIVE, 'int'), MANAGE_CHILD);


        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_children->num_rows > 0) {
            while ($child = $get_children->fetch_assoc()) {
                $results[] = $child;
            }
        }

        return $results;
    }

    /**
     * Lấy ra danh sách trẻ của lớp
     *
     * @param $class_id
     * @return array
     */
    function getChildrenOfClass($class_id, $date = null) {
        global $db,$system;

        $results = array();
        if (is_null($date)) {
            $strSql = sprintf("SELECT CC.* , CC.status AS child_status,C.child_code,C.child_picture, C.child_name, C.first_name, C.last_name, C.birthday, C.gender, C.parent_phone, C.parent_email,
                    C.parent_img1, C.parent_img2, C.parent_img3, C.parent_img4, C.address, C.description, C.name_for_sort
            FROM ci_class_child CC INNER JOIN ci_child C ON CC.child_id = C.child_id
            WHERE CC.class_id = %s AND CC.status= 1 ORDER BY C.name_for_sort ASC", secure($class_id, 'int'));
        } else {
            $date = toDBDate($date);
            $strSql = sprintf('SELECT CC.* , CC.status AS child_status,C.child_code,C.child_picture, C.child_name, C.first_name, C.last_name, C.birthday, C.gender, C.parent_phone, C.parent_email,
                    C.parent_img1, C.parent_img2, C.parent_img3, C.parent_img4, C.address, C.description, C.name_for_sort
            FROM ci_class_child CC INNER JOIN ci_child C ON CC.child_id = C.child_id AND CC.begin_at <= %1$s AND (CC.end_at IS NULL OR CC.end_at >= %1$s)
            WHERE CC.class_id = %2$s AND CC.status= 1 ORDER BY C.name_for_sort ASC', secure($date), secure($class_id, 'int'));
        }


        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_children->num_rows > 0) {
            while ($child = $get_children->fetch_assoc()) {
                $child['birthday'] = toSysDate($child['birthday']);
                if (!is_empty($child['child_picture'])) {
                    $child['child_picture'] = $system['system_uploads'] . '/' . $child['child_picture'];
                }
                $results[] = $child;
            }
        }

        return $results;
    }

    /**
     * Lấy ra danh sách trẻ một lớp (bao gồm trẻ đã nghỉ học), để điền dữ liệu vào combobox
     *
     * @param $classId
     * @return array
     * @throws Exception
     */
    function getChildrenOfClass4ComboBox($classId) {
        global $db;

        $results = array();
        $strSql = sprintf("SELECT C.child_id , CC.status AS child_status, C.child_name, C.birthday, C.name_for_sort
            FROM ci_class_child CC INNER JOIN ci_child C ON CC.child_id = C.child_id WHERE CC.class_id = %s", secure($classId, 'int'));

        $get_children = $db->query($strSql) or _error('error', $strSql);
        while ($child = $get_children->fetch_assoc()) {
            $child['birthday'] = toSysDate($child['birthday']);
            $results[] = $child;
        }

        return $results;
    }

    /**
     * Lấy ra danh sách trẻ một lớp (KHÔNG bao gồm trẻ đã nghỉ học), để điền dữ liệu vào combobox
     *
     * @param $classId
     * @return array
     * @throws Exception
     */
    function getChildrenOfClass4ComboBoxNoLeave($classId) {
        global $db;

        $results = array();
        $strSql = sprintf("SELECT C.child_id , CC.status AS child_status, C.child_name, C.birthday, C.name_for_sort
            FROM ci_class_child CC INNER JOIN ci_child C ON CC.child_id = C.child_id WHERE CC.class_id = %s AND CC.status = '1' ORDER BY C.name_for_sort ASC", secure($classId, 'int'));

        $get_children = $db->query($strSql) or _error('error', $strSql);
        while ($child = $get_children->fetch_assoc()) {
            $child['birthday'] = toSysDate($child['birthday']);
            $results[] = $child;
        }

        return $results;
    }

    /**
     * Lấy ra thông tin chi tiết của trẻ.
     * Hàm này chỉ sử dụng trong màn hình của trẻ.
     *
     * @param $childId
     * @return array
     */
    public function getFullInfoOfChild($childId) {
        $key = "full_child_info_".$childId;
        $child = $_SESSION[$key];
        if (is_null($child)) {
            $child = $this->_getFullInfoOfChild($childId);
            $_SESSION[$key] = $child;
        }

        return $child;
    }

    /**
     * Lấy ra thông tin chi tiết của trẻ màn hình phụ huynh.
     * Hàm này chỉ sử dụng trong màn hình của trẻ.
     *
     * @param $childId
     * @return array
     */
    public function getFullInfoOfChildByParent($childParentId, $childId) {
//        $key = "full_child_info_by_parent_".$childParentId;
//        $child = $_SESSION[$key];
//        if (is_null($child)) {
            $child = $this->_getFullInfoOfChildByParent($childParentId, $childId);
//            $_SESSION[$key] = $child;
//        }

        return $child;
    }

    /**
     * Lấy ra đầy đủ thông tin của trẻ
     *
     * @param $childId
     * @return array
     * @throws Exception
     */
    private function _getFullInfoOfChild($childId) {
        global $db;

        $childInfor = array();
        //Lấy ra thông tin trẻ
        $child = $this->getChildByParent($childId);
        $childInfor['child'] = $child;

        //Lấy ra thông tin lớp
        $strSql = sprintf("SELECT G.*, SC.school_id FROM groups G INNER JOIN ci_school_child SC
                                ON G.group_id = SC.class_id AND SC.status = 1 AND SC.child_id = %s", secure($childId, 'int'));

        $get_class = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_class->num_rows > 0) {
            $class = $get_class->fetch_assoc();
            $class['group_picture'] = get_picture($class['group_picture'], 'class');

            $childInfor['class'] = $class;

            //Lấy ra danh sách giáo viên của lớp (cũng chính là trẻ đó).
            //UPDATE START MANHDD 04/06/2021
//            $strSql = sprintf("SELECT U.user_id, U.user_name, U.user_phone, U.created_id, U.user_email, U.user_fullname, U.user_gender, U.user_picture FROM users U
//                INNER JOIN ci_user_manage UM ON U.user_id = UM.user_id AND UM.object_type = %s AND UM.object_id = %s AND UM.role_id = %s",
//                MANAGE_CLASS, secure($class['group_id'], 'int'), PERMISSION_MANAGE);
            $strSql = sprintf("SELECT U.user_id, U.user_name, U.user_phone, U.created_id, U.user_email, U.user_fullname, U.user_gender, U.user_picture FROM users U
                INNER JOIN ci_user_manage UM ON U.user_id = UM.user_id AND UM.object_type = %s AND UM.object_id = %s AND UM.role_id IN (%s,%s)",
                MANAGE_CLASS, secure($class['group_id'], 'int'), PERMISSION_MANAGE,PERMISSION_ALL);
            //UPDATE END MANHDD 04/06/2021
            $get_teachers = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            if($get_teachers->num_rows > 0) {
                $teachers = array();
                while($teacher = $get_teachers->fetch_assoc()) {
                    $teacher['user_picture'] = get_picture($teacher['user_picture'], $teacher['user_gender']);
                    $teachers[] = $teacher;
                }
                $childInfor['teacher'] = $teachers;
            }
        }

        //Lấy ra thông tin trường
        $strSql = sprintf("SELECT P.* FROM pages P INNER JOIN ci_school_child SC
                                ON P.page_id = SC.school_id AND SC.status=1 AND SC.child_id = %s", secure($childId, 'int'));

        $get_school = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_school->num_rows > 0) {
            $school = $get_school->fetch_assoc();

            //Lấy ra thông tin cấu hình của trường
            $strSql = sprintf("SELECT * FROM ci_school_configuration WHERE school_id = %s", secure($school['page_id'], 'int'));
            $get_config = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            if($get_config->num_rows > 0) {
                $school['config'] = $get_config->fetch_assoc();
            }

            $childInfor['school'] = $school;
        }
        //Lấy ra danh sách phụ huynh của trẻ
        $strSql = sprintf('SELECT U.* FROM users U
                            INNER JOIN ci_user_manage UM ON U.user_id = UM.user_id AND UM.object_type = %s AND UM.object_id = %s',
                            MANAGE_CHILD, secure($childId, 'int'));
        $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_users->num_rows > 0) {
            $parent = array();
            while ($user = $get_users->fetch_assoc()) {
                $user['user_picture'] = get_picture($user['user_picture'], $user['user_gender']);
                $parent[] = $user;
            }

            $childInfor['parent'] = $parent;
        }

        return $childInfor;
    }

    /**
     * Lấy ra đầy đủ thông tin của trẻ từ màn hình của phụ huynh
     *
     * @param $childId
     * @return array
     * @throws Exception
     */
    private function _getFullInfoOfChildByParent($childParentId, $childId) {
        global $db;

        $childInfor = array();
        //Lấy ra thông tin trẻ
        $child = $this->getChildByParent($childParentId);
        $childInfor['child'] = $child;

        //Lấy ra thông tin lớp
        $strSql = sprintf("SELECT G.*, SC.school_id FROM groups G INNER JOIN ci_school_child SC
                                ON G.group_id = SC.class_id AND SC.status=1 AND SC.child_id = %s", secure($childId, 'int'));

        $get_class = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_class->num_rows > 0) {
            $class = $get_class->fetch_assoc();
            $class['group_picture'] = get_picture($class['group_picture'], 'class');

            $childInfor['class'] = $class;

            //Lấy ra danh sách giáo viên của lớp (cũng chính là trẻ đó).
            //UPDATE START MANHDD 04/06/2021
//            $strSql = sprintf("SELECT U.user_id, U.user_name, U.user_phone, U.created_id, U.user_email, U.user_fullname, U.user_gender, U.user_picture FROM users U
//                INNER JOIN ci_user_manage UM ON U.user_id = UM.user_id AND UM.object_type = %s AND UM.object_id = %s AND UM.role_id = %s",
//                MANAGE_CLASS, secure($class['group_id'], 'int'), PERMISSION_MANAGE);
            $strSql = sprintf("SELECT U.user_id, U.user_name, U.user_phone, U.created_id, U.user_email, U.user_fullname, U.user_gender, U.user_picture FROM users U
                INNER JOIN ci_user_manage UM ON U.user_id = UM.user_id AND UM.object_type = %s AND UM.object_id = %s AND UM.role_id IN (%s,%s)",
                MANAGE_CLASS, secure($class['group_id'], 'int'), PERMISSION_MANAGE);
            //UPDATE END MANHDD 04/06/2021
            $get_teachers = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            if($get_teachers->num_rows > 0) {
                $teachers = array();
                while($teacher = $get_teachers->fetch_assoc()) {
                    $teacher['user_picture'] = get_picture($teacher['user_picture'], $teacher['user_gender']);
                    $teachers[] = $teacher;
                }
                $childInfor['teacher'] = $teachers;
            }
        }

        //Lấy ra thông tin trường
        $strSql = sprintf("SELECT P.* FROM pages P INNER JOIN ci_school_child SC
                                ON P.page_id = SC.school_id AND SC.status=1 AND SC.child_id = %s", secure($childId, 'int'));

        $get_school = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_school->num_rows > 0) {
            $school = $get_school->fetch_assoc();

            //Lấy ra thông tin cấu hình của trường
            $strSql = sprintf("SELECT * FROM ci_school_configuration WHERE school_id = %s", secure($school['page_id'], 'int'));
            $get_config = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            if($get_config->num_rows > 0) {
                $school['config'] = $get_config->fetch_assoc();
            }

            $childInfor['school'] = $school;
        }

        //Lấy ra danh sách phụ huynh của trẻ
        $strSql = sprintf('SELECT U.* FROM users U
                            INNER JOIN ci_parent_manage PM ON U.user_id = PM.user_id AND PM.child_parent_id = %s', secure($childParentId, 'int'));
        $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_users->num_rows > 0) {
            $parent = array();
            while ($user = $get_users->fetch_assoc()) {
                $user['user_picture'] = get_picture($user['user_picture'], $user['user_gender']);
                $parent[] = $user;
            }

            $childInfor['parent'] = $parent;
        }

        return $childInfor;
    }

    /**
     * @param $childId
     * @return array
     */
    public function getChildBasicInfo($childParentId) {
        global $db;

        $childInfor = array();
        //Lấy ra thông tin trẻ
        $child = $this->getChildByParent($childParentId);
        $childInfor['child'] = $child;

        //Lấy ra danh sách phụ huynh của trẻ
        $strSql = sprintf('SELECT U.user_id, U.user_name, U.user_email, U.user_fullname, U.user_picture, U.user_phone FROM users U
                            INNER JOIN ci_parent_manage PM ON U.user_id = PM.user_id AND PM.child_parent_id = %s', secure($childParentId, 'int'));
        $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_users->num_rows > 0) {
            $parent = array();
            while ($user = $get_users->fetch_assoc()) {
                $user['user_picture'] = get_picture($user['user_picture'], $user['user_gender']);
                $parent[] = $user;
            }

            $childInfor['parent'] = $parent;
        }
        return $childInfor;
    }

    /**
     * Lấy ra danh sách trẻ của lớp
     *
     * @param $class_id
     * @return array
     */
    function getChildrenOfClassForReport($class_id) {
        global $db, $system;

        $results = array();
        $strSql = sprintf("SELECT CC.* , C.child_name, C.birthday, C.child_picture
            FROM ci_class_child CC INNER JOIN ci_child C ON CC.child_id = C.child_id
            WHERE CC.class_id = %s AND CC.status = %s ORDER BY C.name_for_sort ASC", secure($class_id, 'int'), secure(STATUS_ACTIVE, 'int'));

        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_children->num_rows > 0) {
            while ($child = $get_children->fetch_assoc()) {
                $child['birthday'] = toSysDate($child['birthday']);
                $reports = $this->_getChildReportNew($child['child_id']);
                $child['report_new'] = ($system['is_mobile']) ? toSysDatetime($reports[0]['created_at']) : $reports[0]['created_at'];
                $child['child_picture'] = $system['system_uploads'] . '/' . $child['child_picture'];
                $results[] = $child;
            }
        }

        return $results;
    }

    /**
     * Lấy ra danh sách báo cáo cho 1 trẻ
     *
     * @param $child_id
     * @throws Exception
     */
    private function _getChildReportNew($child_id)
    {
        global $db, $system;
        $strSql = sprintf("SELECT * FROM ci_report WHERE child_id = %s AND is_notified = %s
                            ORDER BY created_at DESC, date DESC", secure($child_id, 'int'), 1);

        $get_report = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $result = array();
        if ($get_report->num_rows > 0) {
            while ($report = $get_report->fetch_assoc()) {
                if (!is_empty($report['source_file'])) {
                    $report['source_file'] = $system['system_uploads'] . '/' . $report['source_file'];
                }
                $result[] = $report;
            }
        }
        return $result;
    }

    /**
     * Kiểm tra thông tin của trẻ đã được tạo trong lớp chưa.
     *
     * @param $class_id
     * @param $name4sorting
     * @param $parent_email
     * @return bool
     * @throws Exception
     */
    function isCreatedInClass($class_id, $name4sorting, $parent_email, $birthday, $parent_phone) {
        global $db;

        $birthday = toDBDate($birthday);
        $parent_email = trim(strtolower($parent_email));

        $strSql = sprintf("SELECT C.child_id FROM ci_child C INNER JOIN ci_class_child CC ON CC.child_id = C.child_id
            AND CC.class_id = %s AND C.name_for_sort = %s AND C.birthday = %s AND C.parent_phone = %s AND CC.status = 1", secure($class_id, 'int'),
            secure($name4sorting), secure($birthday), secure($parent_phone));

        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_children->num_rows > 0) {
            return true;
        }

        return false;
    }

    /**
     * Lấy ra danh sách trẻ của lớp
     *
     * @param $class_id
     * @return array
     */
    function getChildrenDetailOfClass($class_id, $includeParent = false) {
        global $db;

        $results = array();
        $strSql = sprintf("SELECT CC.child_id , C.* FROM ci_class_child CC INNER JOIN ci_child C ON CC.child_id = C.child_id
            WHERE CC.class_id = %s ORDER BY C.name_for_sort ASC", secure($class_id, 'int'));

        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_children->num_rows > 0) {
            while ($child = $get_children->fetch_assoc()) {
                $child['birthday'] = toSysDate($child['birthday']);

                if ($includeParent) {
                    $child['parent'] = $this->getParent($child['child_id']);
                }

                $results[] = $child;
            }
        }

        return $results;
    }

    /**
     * Lấy ra thông tin cha mẹ của trẻ
     *
     * @param $childId
     * @return array
     * @throws Exception
     */
    public function getParent($childId, $is_noga = 0)
    {
        global $db;
        $results = array();
        /* CI-Mobile lấy ra thêm user_phone, user_email của phụ huynh */
        $strSql = sprintf("SELECT U.user_id, U.user_name, U.user_fullname, U.user_gender, U.user_picture, U.user_phone, U.user_email, U.user_last_login, U.user_last_active FROM users U
                           INNER JOIN ci_user_manage UM ON U.user_id = UM.user_id AND UM.object_type = %s AND UM.object_id = %s",
                            MANAGE_CHILD, secure($childId, 'int'));
        $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_users->num_rows > 0) {
            while ($user = $get_users->fetch_assoc()) {
                $user['user_picture'] = get_picture($user['user_picture'], $user['user_gender']);
                $user['user_last_login'] = toSysDate($user['user_last_login']);
                $user['user_last_active'] = toSysDatetime($user['user_last_active']);
                $results[] = $user;
            }
        } else {
            if($is_noga == 0) {
                $strSql = sprintf("SELECT U.user_id, U.user_name, U.user_fullname, U.user_gender, U.user_picture, U.user_phone, U.user_email, U.user_last_login, U.user_last_active FROM users U
                            INNER JOIN ci_child C ON C.child_id = %s AND ((BINARY U.user_phone = C.parent_phone AND C.parent_phone != '')
                            OR (BINARY U.user_email = C.parent_email AND C.parent_email != ''))", secure($childId, 'int'));
                $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                while ($user = $get_users->fetch_assoc()) {
                    $user['user_picture'] = get_picture($user['user_picture'], $user['user_gender']);
                    $user['user_last_login'] = toSysDate($user['user_last_login']);
                    $user['user_last_active'] = toSysDatetime($user['user_last_active']);
                    $user['suggest'] = '1';
                    $results[] = $user;
                }
            }
        }

        return $results;
    }

    /**
     * Xóa trẻ khỏi hệ thống
     * Điều kiện xóa được:
     *  - Trường: chỉ xóa được khi trẻ không được gán cho cha mẹ nào.
     *
     * @param $child_id
     * @throws Exception
     */
    public function deleteChild($child_id)
    {
        global $db, $user;
        $strSql = sprintf('DELETE FROM ci_child WHERE child_id = %1$s AND NOT EXISTS
                          (SELECT user_id FROM ci_user_manage WHERE object_id = %1$s AND object_type = %2$s)',
            secure($child_id, 'int'), MANAGE_CHILD);
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Xóa trẻ khỏi hệ thống
     * Điều kiện xóa được:
     *  - Phụ huynh: chỉ xóa được khi trẻ không được học ở trường nào
     *
     * @param $child_parent_id
     * @throws Exception
     */
    public function deleteChildForParent($child_parent_id)
    {
        global $db;
        $strSql = sprintf('DELETE FROM ci_child_parent WHERE child_parent_id = %s',
            secure($child_parent_id, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Xóa trẻ khỏi lớp
     *
     * @param $child_id
     */
    public function deleteClassChild($child_id) {
        global $db;
        $strSql = sprintf('DELETE FROM ci_class_child WHERE child_id = %s',
            secure($child_id, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Xóa trẻ khỏi trường
     *
     * @param $child_id
     */
    public function deleteSchoolChild($child_id) {
        global $db;
        $strSql = sprintf('DELETE FROM ci_school_child WHERE child_id = %s',
            secure($child_id, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    // Begin Birthday
    /**
     * Lấy ra danh sách trẻ có sinh nhật trong tháng
     *
     * @param
     * @return array
     */
    function getChildrenBirthdayOfMonth() {
        global $db;

        $results = array();
        /* CI-Mobile lấy ra thêm address và description*/
//        $strSql = sprintf("SELECT G.group_name, G.group_title, P.page_title, P.page_name, SC.school_id, SC.class_id , C.child_id, C.child_name, C.birthday, C.gender, C.parent_phone, C.parent_email FROM ci_school_child SC
//                    INNER JOIN ci_child C ON SC.child_id = C.child_id
//                    INNER JOIN pages P ON SC.school_id = P.page_id
//                    INNER JOIN groups G ON SC.class_id = G.group_id
//                    ORDER BY C.name_for_sort ASC");

        $strSql = sprintf("SELECT G.group_name, G.group_title, P.page_title, P.page_name, SC.school_id, SC.class_id , C.child_id, C.child_name, C.birthday, C.gender, C.parent_phone, C.parent_email FROM ci_school_child SC
                    INNER JOIN ci_child C ON SC.child_id = C.child_id 
                    INNER JOIN pages P ON SC.school_id = P.page_id
                    INNER JOIN groups G ON SC.class_id = G.group_id
                    WHERE 1 = (FLOOR(DATEDIFF(DATE_ADD(DATE(NOW()),INTERVAL 30 DAY),birthday) / 365.25)) - (FLOOR(DATEDIFF(DATE(NOW()),birthday) / 365.25))
ORDER BY MONTH(birthday),DAY(birthday)");
        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_children->num_rows > 0) {
            while ($child = $get_children->fetch_assoc()) {
                convertText4Web($child);
                $this->insertChildBirthday($child);
                $this->insertClassOfChildBirthday($child);
                // $child['birthday'] = toSysDate($child['birthday']);
                // Lấy thông tin phụ huynh
                $child['parent'] = $this->getParent($child['child_id']);
                convertText4Web($child['parent']);
                foreach ($child['parent'] as $rows) {
                    $this->insertParentOfChildBirthday($child['child_id'], $rows);
                }
                $results[] = $child;
            }
        }
        return $results;
    }

    /**
     * Insert trẻ có sinh nhật trong tháng vào ci_user_birthday
     *
     * @param array
     * @return
     */
    private function insertChildBirthday($results = array()) {
        global $db;

        $strSql = sprintf("INSERT INTO ci_user_birthday (user_id, name, fullname, gender, birthday, phone, email, type) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)", secure($results['child_id'], 'int'), secure($results['child_name']), secure($results['child_name']), secure($results['gender']), secure($results['birthday']), secure($results['parent_phone']), secure($results['parent_email']), 1);

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Insert phụ huynh trẻ có sinh nhật trong tháng vào ci_parent_of_child_birthday
     * @param $childId
     * @param array
     * @return
     */
    private function insertParentOfChildBirthday($childId, $args = array()) {
        global $db;
        $strSql = sprintf("INSERT INTO ci_parent_of_child_birthday (child_id, parent_id, parent_phone, parent_email, parent_name, parent_fullname, 
parent_gender) VALUES (%s, %s, %s, %s, %s, %s, %s)", secure($childId, 'int'), secure($args['user_id'], 'int'), secure($args['user_phone']), secure($args['user_email']), secure($args['user_name']), secure($args['user_fullname']), secure($args['user_gender']));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Insert thông tin lớp của trẻ có sinh nhật trong tháng vào ci_class_birthday
     * @param $childId
     * @param array
     * @return
     */
    private function insertClassOfChildBirthday($args = array()) {
        global $db;
        $strSql = sprintf("INSERT INTO ci_class_birthday (user_id, class_id, class_name, class_title, school_id, school_name, school_title) VALUES (%s, 
%s, %s, %s, %s, %s, %s)", secure($args['child_id'], 'int'), secure($args['class_id'], 'int'), secure($args['group_name']), secure($args['group_title']), secure($args['school_id']), secure($args['page_name']), secure($args['page_title']));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy thông tin người đón trẻ
     *
     * @param $childId
     * @return array
     * @throws Exception
     */
    public function getChildInformation($childId){
        global $db, $system;

        $strSql = sprintf("SELECT CI.*, U.user_fullname FROM ci_child_info CI
                        INNER JOIN users U ON U.user_id = CI.created_user_id
                        WHERE child_id = %s", secure($childId, 'int'));
        $get_child_info = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $childInfo = array();
        if($get_child_info->num_rows > 0) {
            while ($child_info = $get_child_info->fetch_assoc()) {
                if (!is_empty($child_info['picker_source_file'])) {
                    $child_info['source_file'] = $child_info['picker_source_file'];
                    $child_info['picker_source_file'] = $system['system_uploads'] . '/' . $child_info['picker_source_file'];
                } else {
                    $child_info['source_file'] = '';
                }
                $childInfo[] = $child_info;
            }
        }
        return $childInfo;
    }

    /**
     * Lấy thông tin chi tiết người đón trẻ
     *
     * @param $childInfoId
     * @return null
     * @throws Exception
     */
    public function getChildInformationDetail($childInfoId){
        global $db, $system;

        $strSql = sprintf("SELECT * FROM ci_child_info WHERE child_info_id = %s", secure($childInfoId, 'int'));
        $get_child_info = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $childInfo = null;
        if($get_child_info->num_rows > 0) {
            $childInfo = $get_child_info->fetch_assoc();
            if (!is_empty($childInfo['picker_source_file'])) {
                $childInfo['source_file'] = $childInfo['picker_source_file'];
                $childInfo['picker_source_file'] = $system['system_uploads'] . '/' . $childInfo['picker_source_file'];
            } else {
                $childInfo['source_file'] = '';
            }
        }
        return $childInfo;
    }

    /**
     * Thêm mới thông tin người đón trẻ
     *
     * @param $args
     * @return mixed
     * @throws Exception
     */
    public function insertChildInfomation($args) {
        global $db, $date, $user;

        // Kiểm tra số lượng người đón trẻ (nếu đã có 4 người thì không cho tạo thêm nữa
        $strSql = sprintf("SELECT count(child_info_id) AS cnt FROM ci_child_info WHERE child_id = %s", secure($args['child_id'], 'int'));
        $get_count = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_count->num_rows > 0) {
            $countPicker = $get_count->fetch_assoc()['cnt'];
        }
        if($countPicker >= 4) {
            throw new Exception(__("You can only add up to 4 people"));
        }

        $strSql = sprintf("INSERT INTO ci_child_info (child_id, picker_name, picker_relation, picker_phone, picker_address, picker_source_file, picker_file_name, created_at, created_user_id, status) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", secure($args['child_id'], 'int'), secure($args['picker_name']), secure($args['picker_relation']), secure($args['picker_phone']), secure($args['picker_address']), secure($args['source_file']), secure($args['file_name']), secure($date), secure($user->_data['user_id'], 'int'), PICKER_NOT_CONFIRMED);

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        //Trả về ID vừa mới được insert vào DB
        return $db->insert_id;
    }

    /**
     * Cập nhật thông tin người đón trẻ
     *
     * @param $args
     * @throws Exception
     */
    public function updateChildInformation($args) {
        global $db, $date, $user;

        $strSql = sprintf("UPDATE ci_child_info SET picker_name = %s, picker_relation = %s, picker_phone = %s, picker_address = %s, picker_source_file = %s, picker_file_name = %s, updated_at = %s, status = %s, created_user_id = %s WHERE child_info_id = %s", secure($args['picker_name']), secure($args['picker_relation']), secure($args['picker_phone']), secure($args['picker_address']), secure($args['source_file']), secure($args['file_name']), secure($date), PICKER_NOT_CONFIRMED, secure($user->_data['user_id'], 'int'), secure($args['child_info_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }
    /*
     * Update trạng thái đã trả trẻ
     */
    public function updateChildInformationConfirm($childId, $status) {
        global $db;

        $strSql = sprintf("UPDATE ci_child_info SET status = %s WHERE child_id = %s", PICKER_CONFIRMED, secure($childId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy thông tin chi tiết phụ huynh
     *
     * @param $user_id
     * @return null
     */
    private function _getUser($user_id) {
        global $db;

        $strSql = sprintf("SELECT user_id, user_name, user_fullname user_birthdate, user_email, user_picture, user_gender FROM users WHERE user_id = %s", secure($user_id, 'int'));

        $get_user = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $user = null;
        if ($get_user->num_rows > 0) {
            $user = $get_user->fetch_assoc();
            $user['user_picture'] = get_picture($user['user_picture'], $user['user_gender']);
        }
        return $user;
    }

    /**
     * Lấy toàn bộ danh sách ID trẻ trong trường được giáo viên edit
     *
     * @param $school_id
     * @return array
     */
    public function getAllChildIdEditOfTeacher($school_id) {
        global $db;

        $strSql = sprintf("SELECT child_id FROM ci_child_for_teacher WHERE school_id = %s", secure($school_id, 'int'));

        $get_child_edit = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $childrenIdEdit = array();
        if ($get_child_edit->num_rows > 0) {
            while ($childId = $get_child_edit->fetch_assoc()['child_id']) {
                $childrenIdEdit[] = $childId;
            }
        }
        return $childrenIdEdit;
    }

    /**
     * Lấy chi tiết trẻ được edit trước đó
     *
     * @param $child_id
     * @param $school_id
     * @return null
     */
    public function getChildEditOfTeacher($child_id, $school_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_child_for_teacher WHERE child_id = %s AND school_id = %s", secure($child_id, 'int'), secure($school_id, 'int'));

        $get_child = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $child = null;
        if ($get_child->num_rows > 0) {
            $child = $get_child->fetch_assoc();
            $child['birthday'] = toSysDate($child['birthday']);
            $child['begin_at'] = toSysDate($child['begin_at']);
            if($child['parent_id1'] != 0) {
                $child['parent'][] = $this->_getUser($child['parent_id1']);
            }
            if($child['parent_id2'] != 0) {
                $child['parent'][] = $this->_getUser($child['parent_id2']);
            }
            if($child['parent_id3'] != 0) {
                $child['parent'][] = $this->_getUser($child['parent_id3']);
            }
            if($child['parent_id4'] != 0) {
                $child['parent'][] = $this->_getUser($child['parent_id4']);
            }
        }
        return $child;
    }

    /**
     * Kiểm tra email đã tồn tại trong hệ thống chưa
     *
     * @param $email
     * @return bool
     * @throws Exception
     */
    private function check_email($email) {
        global $db;
        $query = $db->query(sprintf("SELECT user_id FROM users WHERE user_email = %s", secure($email) )) or _error(SQL_ERROR_THROWEN);
        if($query->num_rows > 0) {
            return true;
        }
        return false;
    }

    /**
     * Lấy thông tin sức khỏe của trẻ
     *
     * @param $child_parent_id
     * @return null
     */
    public function getChildHealth($child_parent_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_child_health WHERE child_parent_id = %s", secure($child_parent_id, 'int'));

        $get_health = $db->query($strSql) or _error('error', $strSql);
        if($get_health->num_rows > 0) {
            $health = $get_health->fetch_assoc();
            return $health;
        }
        return null;
    }

    /**
     * Lấy thông tin chiều cao cân nặng của trẻ
     *
     * @param $child_parent_id
     * @return array
     */
    public function getChildGrowth($child_parent_id) {
        global $db, $system;

        $strSql = sprintf("SELECT * FROM ci_child_growth WHERE child_parent_id = %s ORDER  BY recorded_at DESC, child_growth_id DESC", secure($child_parent_id, 'int'));

        $get_growth = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $growths = array();
        if($get_growth->num_rows > 0) {
            while($temp = $get_growth->fetch_assoc()) {
               $temp['recorded_at'] = toSysDate($temp['recorded_at']);
                if (!is_empty($temp['source_file'])) {
                    $temp['source_file_path'] = $temp['source_file'];
                    $temp['source_file'] = $system['system_uploads'] . '/' . $temp['source_file'];
                }
                $growths[] = $temp;
            }
        }
        return $growths;
    }

    /**
     * Lấy thông tin chiều cao cân nặng của trẻ có limit
     *
     * @param $child_parent_id
     * @return array
     */
    public function getChildGrowthLimit($child_parent_id) {
        global $db, $system;

        $strSql = sprintf("SELECT * FROM ci_child_growth WHERE child_parent_id = %s ORDER  BY recorded_at DESC, child_growth_id DESC LIMIT 15", secure($child_parent_id, 'int'));

        $get_growth = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $growths = array();
        if($get_growth->num_rows > 0) {
            while($temp = $get_growth->fetch_assoc()) {
                $temp['recorded_at'] = toSysDate($temp['recorded_at']);
                if (!is_empty($temp['source_file'])) {
                    $temp['source_file_path'] = $temp['source_file'];
                    $temp['source_file'] = $system['system_uploads'] . '/' . $temp['source_file'];
                }
                $growths[] = $temp;
            }
        }
        return $growths;
    }

    /**
     * Lấy thông tin sức khỏe của trẻ lần gần nhất
     *
     * @param $child_parent_id
     * @return array
     */
    public function getChildGrowthLastest($child_parent_id) {
        global $db, $system;

        $strSql = sprintf("SELECT * FROM ci_child_growth WHERE child_parent_id = %s ORDER  BY recorded_at DESC, child_growth_id DESC LIMIT %s", secure($child_parent_id, 'int'), 1);

        $get_growth = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $growth = null;
        if($get_growth->num_rows > 0) {
            $growth = $get_growth->fetch_assoc();
            $growth['recorded_at'] = toSysDate($growth['recorded_at']);
            if (!is_empty($growth['source_file'])) {
                $growth['source_file_path'] = $growth['source_file'];
                $growth['source_file'] = $system['system_uploads'] . '/' . $growth['source_file'];
            }
        }
        return $growth;
    }

    /**
     * Lấy thông tin chiều cao cân nặng của trẻ lần gần nhất
     *
     * @param $child_parent_id
     * @return array
     */
    public function getChildGrowthLastestWeightNHeight($child_parent_id) {
        global $db, $system;

        $strSql = sprintf("SELECT child_growth_id, weight, height, recorded_at FROM ci_child_growth WHERE child_parent_id = %s ORDER BY recorded_at DESC, child_growth_id DESC LIMIT %s", secure($child_parent_id, 'int'), 1);

        $get_growth = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $growth = null;
        if($get_growth->num_rows > 0) {
            $growth = $get_growth->fetch_assoc();
            $growth['recorded_at'] = toSysDate($growth['recorded_at']);
        }
        return $growth;
    }

    /**
     * Lấy thông tin chiều cao cân nặng của trẻ để vẽ biểu đồ
     *
     * @param $child_parent_id
     * @return array
     */
    public function getChildGrowthForChart($child_parent_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_child_growth WHERE child_parent_id = %s ORDER  BY recorded_at ASC", secure($child_parent_id, 'int'));

        $get_growth = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $growths = array();
        if($get_growth->num_rows > 0) {
            while($temp = $get_growth->fetch_assoc()) {
                $temp['recorded_at'] =  toSysDate($temp['recorded_at']);

                if($temp['height'] > 0) {
                    $temp['bmi'] = $temp['weight'] * 10000 / ($temp['height'] * $temp['height']);
                    $temp['bmi'] = number_format($temp['bmi'], 2, '.', '');
                }
                $growths[] = $temp;
            }
        }
        return $growths;
    }

    /**
     * Tạo thông tin sức khỏe của trẻ
     *
     * @param $args
     * @return mixed
     */
    public function insertChildHealth($args) {
        global $db, $user, $date;

        $strSql = sprintf("INSERT INTO ci_child_health (child_parent_id, blood_type, heart, ear, eye, allergy, swim, hobby, description, created_at, created_user_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", secure($args['child_parent_id'], 'int'), secure($args['blood_type']), secure($args['heart']), secure($args['ear']), secure($args['eye']), secure($args['allergy']), secure($args['swim']), secure($args['hobby']), secure($args['description']), secure($date), secure($user->_data['user_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        //Trả về ID vừa mới được insert vào DB
        return $db->insert_id;
    }

    /**
     * Validate dữ liệu đầu vào khi tạo thong tin phát triển của trẻ
     *
     * @param $args
     * @throws Exception
     */
    private function _validateInputGrowth($args) {
        if(!isset($args['recorded_at']) || !validateDate($args['recorded_at'])) {
            throw new Exception(__("You must enter correct recorded_at"));
        }

        if(!isset($args['child_parent_id']) || !is_numeric($args['child_parent_id'])) {
            _error(404);
        }
    }

    /**
     * Tạo thông tin chiều cao cân nặng của trẻ
     *
     * @param $args
     * @return mixed
     */
    public function insertChildGrowth($args) {
        global $db, $user;
        $this->_validateInputGrowth($args);
        $args['recorded_at'] = toDBDate($args['recorded_at']);
        $strSql = sprintf("INSERT INTO ci_child_growth (child_parent_id, recorded_at, weight, height, nutriture_status, heart, blood_pressure, ear, eye, nose, source_file, file_name, description, created_user_id, is_parent) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", secure($args['child_parent_id'], 'int'), secure($args['recorded_at']), secure($args['weight']), secure($args['height']), secure($args['nutriture_status']), secure($args['heart']), secure($args['blood_pressure']), secure($args['ear']), secure($args['eye']), secure($args['nose']), secure($args['source_file']), secure($args['file_name']), secure($args['description']), secure($user->_data['user_id'], 'int'), secure($args['is_parent'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        //Trả về ID vừa mới được insert vào DB
        return $db->insert_id;
    }

    /**
     * Cập nhật thông tin sức khỏe của trẻ
     *
     * @param $args
     */
    public function updateChildHealth($args) {
        global $db, $date;

        $strSql = sprintf("UPDATE ci_child_health SET blood_type = %s, heart = %s, ear = %s, eye = %s, allergy = %s, swim = %s, hobby = %s, description = %s, updated_at = %s WHERE child_parent_id = %s", secure($args['blood_type']), secure($args['heart']), secure($args['ear']), secure($args['eye']), secure($args['allergy']), secure($args['swim']), secure($args['hobby']), secure($args['description']), secure($date), secure($args['child_parent_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Update thông tin chiều cao, cân nặng của trẻ
     *
     * @param $args
     */
    public function updateChildGrowth($args) {
        global $db;

        $args['recorded_at'] = toDBDate($args['recorded_at']);
        $strSql = sprintf("UPDATE ci_child_growth SET height = %s, weight = %s, nutriture_status = %s, heart = %s, blood_pressure = %s, ear = %s, eye = %s, nose = %s, description = %s, recorded_at = %s, source_file = %s, file_name = %s WHERE child_growth_id = %s", secure($args['height']), secure($args['weight']), secure($args['nutriture_status']), secure($args['heart']), secure($args['blood_pressure']), secure($args['ear']), secure($args['eye']), secure($args['nose']), secure($args['description']), secure($args['recorded_at']), secure($args['source_file']), secure($args['file_name']), secure($args['child_growth_id']));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }


    /* ---------- CHILD - MEMCACHED ---------- */
    /**
     * Lấy ra danh sách trẻ của lớp
     *
     * @param $class_id
     * @return array
     */
    function getChildrenOfClass4Memcache($class_id) {
        global $db;

        $results = array(
            'ids' => [],
            'lists' => []
        );
        $strSql = sprintf("SELECT CC.* , C.child_name, C.first_name, C.last_name, C.birthday, C.gender, C.parent_phone, C.parent_email, C.parent_img1, C.parent_img2,
                            C.parent_img3, C.parent_img4, C.address, C.description, C.name_for_sort
            FROM ci_class_child CC INNER JOIN ci_child C ON CC.child_id = C.child_id AND CC.status = %s
            WHERE CC.class_id = %s ORDER BY C.name_for_sort ASC", secure(STATUS_ACTIVE, 'int'), secure($class_id, 'int'));

        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_children->num_rows > 0) {
            while ($child = $get_children->fetch_assoc()) {
                $child['birthday'] = toSysDate($child['birthday']);
                $child['parent'] = $this->getParent4Memcache($child['child_id']);

                $results['lists'][$child['child_id']] = $child;
                $results['ids'][] = $child['child_id'];
            }
        }

        return $results;
    }


    /**
     * Lấy ra thông tin cha mẹ của trẻ
     *
     * @param $childId
     * @return array
     * @throws Exception
     */
    public function getParent4Memcache($childId)
    {
        global $db;
        $results = array();
        $strSql = sprintf('SELECT U.user_id, U.user_name, U.user_fullname, U.user_gender, U.user_picture, U.user_phone, U.user_email, U.user_last_active FROM users U 
                           INNER JOIN ci_user_manage UM ON U.user_id = UM.user_id AND UM.object_type = %s AND UM.object_id = %s
                           INNER JOIN ci_school_child SC ON UM.object_id = SC.child_id AND SC.status = %s',
            MANAGE_CHILD, secure($childId, 'int'), secure(STATUS_ACTIVE, 'int'));
        $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_users->num_rows > 0) {
            while ($user = $get_users->fetch_assoc()) {
                $user['user_picture'] = get_picture($user['user_picture'], $user['user_gender']);
                $results[$user['user_id']] = $user;
            }
        }

        return $results;
    }

    /**
     * Lấy ra danh sách children của một trường
     *
     * @param $school_id
     * @return array
     * @throws Exception
     */
    public function getChildrenOfSchool4Memcache($school_id) {
        global $db;

        $strSql = sprintf("SELECT C.*, SC.school_id, SC.class_id, SC.begin_at, SC.end_at FROM ci_school_child SC 
                   INNER JOIN ci_child C ON SC.child_id = C.child_id
                   WHERE SC.school_id = %s AND SC.status = '1' ORDER BY C.name_for_sort ASC", secure($school_id, 'int'));

        $children = array(
            'ids' => [],
            'lists' => []
        );
        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_children->num_rows > 0) {
            while($child = $get_children->fetch_assoc()) {
                $child['birthday'] = toSysDate($child['birthday']);
                $children['ids'][] = $child['child_id'];
                $children['lists'][$child['child_id']] = $child;
            }
        }
        return $children;
    }

    /** Lấy ra danh sách phụ huynh và trẻ từ danh sách ID trẻ nhập vào.
     * Hàm này phục vụ chức năng NOTIFICATION
     *
     * @param $childIds
     * @return array
     * @throws Exception
     */
    function getChildNParentIdForMemcache($childIds) {
        global $db;

        $results = array();
        $strCon = implode(',', $childIds);
        $strSql = sprintf("SELECT C.child_id, C.child_name, UM.user_id AS parent_id FROM ci_user_manage UM
                            INNER JOIN ci_child C ON UM.object_id = C.child_id AND C.child_id IN (%s)
                            INNER JOIN ci_school_child SC ON C.child_id = SC.child_id AND SC.status = %s
                            WHERE UM.object_type = %s", $strCon, STATUS_ACTIVE, MANAGE_CHILD);


        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_children->num_rows > 0) {
            while ($child = $get_children->fetch_assoc()) {
                $results[] = $child;
            }
        }

        return $results;
    }


    /* END - MEMCACHE */


    /**
     * Update thông tin chiều cao, cân nặng của trẻ ngày hôm nay
     *
     * @param $args
     */
    public function updateChildGrowthDateNow($args) {
        global $db;

        $args['recorded_at'] = toDBDate($args['recorded_at']);
        $strSql = sprintf("UPDATE ci_child_growth SET height = %s, weight = %s, nutriture_status = %s, heart = %s, blood_pressure = %s, ear = %s, eye = %s, nose = %s, description = %s, source_file = %s, file_name = %s WHERE recorded_at = %s AND child_parent_id = %s", secure($args['height']), secure($args['weight']), secure($args['nutriture_status']), secure($args['heart']), secure($args['blood_pressure']), secure($args['ear']), secure($args['eye']), secure($args['nose']), secure($args['description']), secure($args['source_file']), secure($args['file_name']), secure($args['recorded_at']), secure($args['child_parent_id']));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Validate thông tin thai nhi
     *
     * @param $args
     * @throws Exception
     */

    private function _validateInputFoetus($args) {
        // Kiểm tra ngày đã tạo có trong hệ thống chưa, nếu đã tạo rồi thì không cho tao nữa
        global $db;

        $strSql = sprintf("SELECT recorded_at FROM ci_child_foetus_growth WHERE recorded_at = %s AND child_parent_id = %s", secure($args['recorded_at']), secure($args['child_parent_id'], 'int'));

        $get_recorded_at = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_recorded_at->num_rows > 0) {
            throw new Exception(__("Day was created, please choose another day"));
        }

    }

    /**
     * Thêm thông tin sức khỏe thai nhi
     *
     * @param $args
     * @return mixed
     */
    public function createChildFoetusGrowth($args) {
        global $db, $user;
        $this->_validateInputFoetus($args);
        $args['recorded_at'] = toDBDate($args['recorded_at']);
        if(!is_null($args['due_date_of_childbearing'])) {
            $args['due_date_of_childbearing'] = toDBDate($args['due_date_of_childbearing']);
        } else {
            $args['due_date_of_childbearing'] = 'null';
        }

        $strSql = sprintf("INSERT INTO ci_child_foetus_growth (child_parent_id, recorded_at, pregnant_week, due_date_of_childbearing, femur_length, from_head_to_hips_length, weight, fetal_heart, hospital, hospital_address, doctor_name, doctor_phone, description, created_user_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", secure($args['child_parent_id'], 'int'), secure($args['recorded_at']), secure($args['pregnant_week'], 'int'), secure($args['due_date_of_childbearing']), secure($args['femus_length']), secure($args['from_head_to_hips_length']), secure($args['weight']), secure($args['fetal_heart'], 'int'), secure($args['hospital']), secure($args['hospital_address']), secure($args['doctor_name']), secure($args['doctor_phone']), secure($args['description']), secure($user->_data['user_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        //Trả về ID vừa mới được insert vào DB
        return $db->insert_id;
    }

    /**
     * Lấy thông tin phát triển của thai nhi
     *
     * @param $child_parent_id
     * @return array
     */
    public function getChildFoetusHealth($child_parent_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_child_foetus_growth WHERE child_parent_id = %s ORDER BY recorded_at DESC", secure($child_parent_id, 'int'));

        $get_child_foetus_growth = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $child_foetus_growth = array();
        if($get_child_foetus_growth->num_rows > 0) {
            while ($child_foetus = $get_child_foetus_growth->fetch_assoc()) {
                $child_foetus['recorded_at'] = toSysDate($child_foetus['recorded_at']);
                if($child_foetus['due_date_of_childbearing'] != null) {
                    $child_foetus['due_date_of_childbearing'] = toSysDate($child_foetus['due_date_of_childbearing']);
                }
                $child_foetus_growth[] = $child_foetus;
            }
        }

        return $child_foetus_growth;
    }

    /**
     * Lấy thông tin phát triển của thai nhi lần gần nhất
     *
     * @param $child_parent_id
     * @return array
     */
    public function getChildFoetusHealthLastest($child_parent_id) {
        global $db;

        $strSql = sprintf("SELECT from_head_to_hips_length, weight FROM ci_child_foetus_growth WHERE child_parent_id = %s ORDER BY recorded_at DESC, child_foetus_growth_id DESC LIMIT 1", secure($child_parent_id, 'int'));

        $get_child_foetus_growth = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $child_foetus_growth_lastest = null;
        if($get_child_foetus_growth->num_rows > 0) {
            $child_foetus = $get_child_foetus_growth->fetch_assoc();
            $child_foetus_growth_lastest = $child_foetus;
        }

        return $child_foetus_growth_lastest;
    }

    /**
     * Lấy thông tin phát triển của thai nhi để vẽ biểu đồ
     *
     * @param $child_parent_id
     * @return array
     */
    public function getChildFoetusHealthForChart($child_parent_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_child_foetus_growth WHERE child_parent_id = %s ORDER BY recorded_at ASC", secure($child_parent_id. 'int'));

        $get_child_foetus_growth = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $child_foetus_growth = array();
        if($get_child_foetus_growth->num_rows > 0) {
            while ($child_foetus = $get_child_foetus_growth->fetch_assoc()) {
                $child_foetus['recorded_at'] = toSysDate($child_foetus['recorded_at']);
                if($child_foetus['due_date_of_childbearing'] != null) {
                    $child_foetus['due_date_of_childbearing'] = toSysDate($child_foetus['due_date_of_childbearing']);
                }
                $child_foetus_growth[] = $child_foetus;
            }
        }

        return $child_foetus_growth;
    }

    /**
     * Lấy chi tiết thông tin sức khỏe của trẻ
     *
     * @param $child_foetus_growth_id
     * @return null
     */
    public function getChildFoetusHealthById($child_foetus_growth_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_child_foetus_growth WHERE child_foetus_growth_id = %s", secure($child_foetus_growth_id));

        $get_child_foetus_growth = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $foetus_growth = null;
        if($get_child_foetus_growth->num_rows > 0) {
            $foetus_growth = $get_child_foetus_growth->fetch_assoc();
            $foetus_growth['recorded_at'] = toSysDate($foetus_growth['recorded_at']);
            if(!is_null($foetus_growth['due_date_of_childbearing'])) {
                $foetus_growth['due_date_of_childbearing'] = toSysDate($foetus_growth['due_date_of_childbearing']);
            }
        }

        return $foetus_growth;
    }

    /**
     * Thay đổi thông tin sức khỏe của trẻ
     *
     * @param $args
     */
    public function updateChildFoetusGrowth($args) {
        global $db, $date;

        $args['recorded_at'] = toDBDate($args['recorded_at']);
        if(!is_null($args['due_date_of_childbearing'])) {
            $args['due_date_of_childbearing'] = toDBDate($args['due_date_of_childbearing']);
        } else {
            $args['due_date_of_childbearing'] = 'null';
        }

        $strSql = sprintf("UPDATE ci_child_foetus_growth SET recorded_at = %s, pregnant_week = %s, due_date_of_childbearing = %s, femur_length = %s, from_head_to_hips_length = %s, weight = %s, fetal_heart = %s, hospital = %s, hospital_address = %s, doctor_name = %s, doctor_phone = %s, description = %s, updated_at = %s WHERE child_foetus_growth_id = %s", secure($args['recorded_at']), secure($args['pregnant_week'], 'int'), secure($args['due_date_of_childbearing']), secure($args['femus_length']), secure($args['from_head_to_hips_length']), secure($args['weight']), secure($args['fetal_heart'], 'int'), secure($args['hospital']), secure($args['hospital_address']), secure($args['doctor_name']), secure($args['doctor_phone']), secure($args['description']), secure($date), secure($args['child_foetus_growth_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy ra danh sách CHILD được quản lý bởi một user
     *
     * @param $user_id
     * @param $objectType
     * @return array
     * @throws Exception
     */
    public function getChildren4Memcache($user_id,$objectType = null) {
        global $db;

        $strSql = sprintf("SELECT DISTINCT C.child_id, C.*, SC.school_id, SC.class_id FROM ci_child C
                   INNER JOIN ci_user_manage UM ON C.child_id = UM.object_id
                    AND UM.object_type = %s AND UM.user_id = %s 
                    LEFT JOIN ci_school_child SC ON SC.child_id = C.child_id ORDER BY C.name_for_sort ASC", isset($objectType) ? MANAGE_ITSELF : MANAGE_CHILD, secure($user_id, 'int'));

        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        /*$strSql = sprintf("SELECT PM.user_id, CP.*, C.child_id FROM ci_parent_manage PM
                   INNER JOIN ci_child_parent CP ON CP.child_parent_id = PM.child_parent_id AND PM.user_id = %s
                   LEFT JOIN ci_child C ON C.child_id = PM.child_id
                   ORDER BY CP.name_for_sort ASC", secure($user_id, 'int'));
        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);*/

        $children = array(
            'ids' => [],
            'lists' => []
        );
        if($get_children->num_rows > 0) {
            while($child = $get_children->fetch_assoc()) {
                $child['birthday'] = toSysDate($child['birthday']);
                $child['due_date_of_childbearing'] = toSysDate($child['due_date_of_childbearing']);
                $children['ids'][] = $child['child_id'];
                $children['lists'][$child['child_id']] = $child;
            }
        }

        return $children;
    }

    /**
     * Lấy ra thông tin trường HIỆN TẠI của trẻ
     *
     * @param $childId
     * @return array|null
     * @throws Exception
     */
    public function getCurrentSchoolOfChild4Memcache($childId) {
        global $db;

        $strSql = sprintf("SELECT P.page_admin, P.page_id, P.page_name, P.page_title, P.page_picture, SC.class_id FROM pages P 
                           INNER JOIN ci_school_child SC ON P.page_id = SC.school_id AND SC.child_id = %s AND SC.status = 1", secure($childId, 'int'));

        $result = array(
            'id' => null,
            'info' => null
        );
        $get_school = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_school->num_rows > 0) {
            $school =  $get_school->fetch_assoc();
            $school['page_picture'] = get_picture($school['page_picture'], 'school');
            $result['id'] = $school['page_id'];
            $result['info'] = $school;
        }
        return $result;
    }


    /**
     * Xóa toàn bộ phụ huynh của trẻ
     *
     * @param $child_id
     */
    public function deleteAllParentOfChild($child_id) {
        global $db;

        $strSql = sprintf("DELETE FROM ci_user_manage WHERE object_id = %s AND object_type = %s", secure($child_id, 'int'), MANAGE_CHILD);

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Xóa toàn bộ phụ huynh của trẻ trong bảng ci_parent_manage
     *
     * @param $child_parent_id
     */
    public function deleteAllParentOfChildForParent($child_parent_id) {
        global $db;

        $strSql = sprintf("DELETE FROM ci_parent_manage WHERE child_parent_id = %s", secure($child_parent_id, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy thông tin chi tiết chiều cao, cân nặng của trẻ
     *
     * @param $child_growth_id
     * @return null
     */
    public function getChildGrowthById($child_growth_id) {
        global $db, $system;

        $strSql = sprintf("SELECT * FROM ci_child_growth WHERE child_growth_id = %s", secure($child_growth_id));

        $get_growth = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $growth = null;
        if($get_growth->num_rows > 0) {
            $growth = $get_growth->fetch_assoc();
            $growth['recorded_at'] = toSysDate($growth['recorded_at']);
            $growth['source_file_path'] = $growth['source_file'];
            $growth['source_file'] = $system['system_uploads'] . '/' . $growth['source_file'];
        }

        return $growth;
    }

    /**
     * Lấy thông tin sức khỏe của trẻ theo recorded_at
     *
     * @param $recorded_at
     * @param $child_parent_id
     * @return null
     */
    public function getChildGrowthByDate($recorded_at, $child_parent_id) {
        global $db, $system;

        $recorded_at = toDBDate($recorded_at);
        $strSql = sprintf("SELECT * FROM ci_child_growth WHERE recorded_at = %s AND child_parent_id = %s", secure($recorded_at), secure($child_parent_id, 'int'));

        $get_growth = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $growth = null;
        if($get_growth->num_rows > 0) {
            $growth = $get_growth->fetch_assoc();
            $growth['recorded_at'] = toSysDate($growth['recorded_at']);
            $growth['source_file_path'] = $growth['source_file'];
            $growth['source_file'] = $system['system_uploads'] . '/' . $growth['source_file'];
        }

        return $growth;
    }

    /**
     * Delete thông tin sức khỏe của trẻ
     *
     * @param $child_parent_id
     * @param $child_growth_id
     */
    public function deleteChildGrowth($child_parent_id, $child_growth_id) {
        global $db;

        $strSql = sprintf("DELETE FROM ci_child_growth WHERE child_parent_id = %s AND child_growth_id = %s", secure($child_parent_id, 'int'), secure($child_growth_id,'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Delete thông tin sức khỏe của thai nhi
     *
     * @param $child_parent_id
     * @param $child_foerus_growth_id
     */
    public function deleteChildFoetusGrowth($child_parent_id, $child_foerus_growth_id) {
        global $db;

        $strSql = sprintf("DELETE FROM ci_child_foetus_growth WHERE child_parent_id = %s AND child_foetus_growth_id = %s", secure($child_parent_id, 'int'), secure($child_foerus_growth_id));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Tìm kiếm thông tin chiều cao, cân nặng theo năm
     *
     * @param $child_parent_id
     * @param $year
     * @return array
     */
    public function getChildGrowthSearch($child_parent_id, $year) {
        global $db, $system;

        $begin = "01/01/" . $year;
        $end = "31/12/" . $year;

        $begin = toDBDate($begin);
        $end = toDBDate($end);

        $strSql = sprintf("SELECT * FROM ci_child_growth WHERE child_parent_id = %s AND recorded_at > %s AND recorded_at < %s ORDER BY recorded_at DESC, child_growth_id DESC", secure($child_parent_id, 'int'), secure($begin), secure($end));

        $get_growth = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $growths = array();
        if($get_growth->num_rows > 0) {
            while($growth = $get_growth->fetch_assoc()) {
                $growth['recorded_at'] = toSysDate($growth['recorded_at']);
                if(!is_empty($growth['source_file'])) {
                    $growth['source_file_path'] = $growth['source_file'];
                    $growth['source_file'] = $system['system_uploads'] . '/' . $growth['source_file'];
                }
                $growths[] = $growth;
            }
        }

        return $growths;
    }

    /**
     * Tìm kiếm thông tin chiều cao, cân nặng 12 tháng gần nhất
     *
     * @param $child_parent_id
     * @return array
     */
    public function getChildGrowth12MonthLastest($child_parent_id) {
        global $db, $system;

        $dateNow = date('Y-m-d');
        $dateBegin = date('Y-m-d', strtotime("-1 year", strtotime($dateNow)));

        $strSql = sprintf("SELECT * FROM ci_child_growth WHERE child_parent_id = %s AND recorded_at > %s ORDER BY recorded_at DESC, child_growth_id DESC", secure($child_parent_id, 'int'), secure($dateBegin));

        $get_growth = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $growths = array();
        if($get_growth->num_rows > 0) {
            while($growth = $get_growth->fetch_assoc()) {
                $growth['recorded_at'] = toSysDate($growth['recorded_at']);
                if(!is_empty($growth['source_file'])) {
                    $growth['source_file_path'] = $growth['source_file'];
                    $growth['source_file'] = $system['system_uploads'] . '/' . $growth['source_file'];
                }
                $growths[] = $growth;
            }
        }

        return $growths;
    }

    /**
     * Tìm kiếm thông tin chiều cao cân nặng theo năm vẽ biểu đồ
     *
     * @param $child_parent_id
     * @param $year
     * @return array
     */
    public function getChildGrowthSearchForChart($child_parent_id, $year) {
        global $db;

        $begin = "01/01/" . $year;
        $end = "31/12/" . $year;

        $begin = toDBDate($begin);
        $end = toDBDate($end);

        $strSql = sprintf("SELECT recorded_at FROM ci_child_growth WHERE child_parent_id = %s AND recorded_at > %s AND recorded_at < %s ORDER BY recorded_at ASC, created_at ASC", secure($child_parent_id, 'int'), secure($begin), secure($end));

        $get_growth = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $growths = array();
        if($get_growth->num_rows > 0) {
            while($growth = $growths->fetch_assoc()['recorded_at']) {
                $growth = toSysDate($growth);
                $growth = substr($growth, 3, 2);
                $growths[] = $growth;
            }
        }

        return $growths;
    }

    /* ---------- CHILD - THÔI HỌC ---------- */

    /**
     * Hàm thực hiện khi cho trẻ thôi học.
     *
     * @param $childId
     * @param $classId
     * @param $endAt
     * @throws Exception
     */
    public function leaveChildInSchool($childId, $classId, $endAt) {
        global $db;

        if (!validateDate($endAt)) {
            throw new Exception(__("Thời gian gửi lên không đúng, vui lòng kiểm tra lại"));
        }
        $endAt = toDBDate($endAt);

        //Cập nhật trạng thái trẻ trong lớp
        $sqlStr = sprintf("UPDATE ci_class_child SET status = %s, end_at = %s WHERE class_id = %s AND child_id = %s",
            secure(STATUS_INACTIVE, 'int'), secure($endAt), secure($classId, 'int'), secure($childId, 'int'));
        $db->query($sqlStr) or _error(SQL_ERROR_THROWEN);

        //Cập nhật trạng thái trẻ trong trường
        $sqlStr = sprintf("UPDATE ci_school_child SET status = %s, end_at = %s WHERE class_id = %s AND child_id = %s",
            secure(STATUS_INACTIVE, 'int'), secure($endAt), secure($classId, 'int'), secure($childId, 'int'));
        $db->query($sqlStr) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Hàm thực hiện khi cho NHIỀU trẻ thôi học.
     *
     * @param $childIds
     * @param $classId
     * @param $endAt
     * @throws Exception
     */
    public function leaveChildrenInSchool($childIds, $classId, $endAt) {
        global $db;

        if (!validateDate($endAt)) {
            throw new Exception(__("Thời gian gửi lên không đúng, vui lòng kiểm tra lại"));
        }
        $endAt = toDBDate($endAt);

        if(count($childIds) > 0) {
            $strCon = implode(',', $childIds);
            //Cập nhật trạng thái trẻ trong lớp
            $sqlStr = sprintf("UPDATE ci_class_child SET status = %s, end_at = %s WHERE class_id = %s AND child_id IN (%s)",
                secure(STATUS_INACTIVE, 'int'), secure($endAt), secure($classId, 'int'), $strCon);
            $db->query($sqlStr) or _error(SQL_ERROR_THROWEN);

            //Cập nhật trạng thái trẻ trong trường
            $sqlStr = sprintf("UPDATE ci_school_child SET status = %s, end_at = %s WHERE class_id = %s AND child_id IN (%s)",
                secure(STATUS_INACTIVE, 'int'), secure($endAt), secure($classId, 'int'), $strCon);
            $db->query($sqlStr) or _error(SQL_ERROR_THROWEN);
        }
    }


    /* ---------- CHILD - THÔI HỌC ---------- */

    /* ---------- CHILD - API ---------- */

    /**
     * Lấy ra danh sách trẻ của lớp
     *
     * @param $class_id
     * @return array
     */
    function getChildrenOfClassForAPI($class_id, $includeParent = false) {
        global $db, $system;

        $results = array();

        $strSql = sprintf("SELECT C.child_id, C.child_name, C.child_nickname, C.birthday, C.gender, C.child_picture, C.parent_name, C.parent_phone, C.parent_email, C.address, C.description, C.created_user_id, 
            SC.school_id FROM ci_class_child CC INNER JOIN ci_child C ON CC.child_id = C.child_id
            INNER JOIN ci_school_child SC ON C.child_id = SC.child_id
            WHERE CC.class_id = %s ORDER BY C.name_for_sort ASC", secure($class_id, 'int'));

        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_children->num_rows > 0) {
            while ($child = $get_children->fetch_assoc()) {
                $child['birthday'] = toSysDate($child['birthday']);
                $child['begin_at'] = toSysDate($child['begin_at']);
                if (!is_empty($child['child_picture'])) {
                    $child['child_picture'] = $system['system_uploads'] . '/' . $child['child_picture'];
                }
                if ($includeParent) {
                    $child['parent'] = $this->getParent($child['child_id']);
                }

                $results[] = $child;
            }
        }

        return $results;
    }

    /**
     * Lấy ra danh sách trẻ của lớp
     *
     * @param $class_id
     * @return array
     */
    function getChildrenShortClassForAPI($class_id) {
        global $db;

        $results = array();
        $strSql = sprintf("SELECT C.child_id , C.child_name, C.birthday, C.gender, C.parent_name, C.parent_phone, CC.status AS child_status
            FROM ci_class_child CC INNER JOIN ci_child C ON CC.child_id = C.child_id
            WHERE CC.class_id = %s ORDER BY C.name_for_sort ASC", secure($class_id, 'int'));

        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_children->num_rows > 0) {
            while ($child = $get_children->fetch_assoc()) {
                $child['birthday'] = toSysDate($child['birthday']);

                $results[] = $child;
            }
        }

        return $results;
    }

    /**
     * Lấy ra danh sách trẻ của lớp
     *
     * @param $class_id
     * @return array
     */
    function getChildDetailOfClassForAPI($class_id, $child_id) {
        global $db, $system;

        $results = null;

        $strSql = sprintf("SELECT C.child_id, C.child_code, C.first_name, C.last_name, C.child_name, C.child_nickname, C.birthday, C.gender, C.child_picture, C.parent_name, C.parent_phone, C.parent_email, C.address, C.description, C.created_user_id, 
            SC.school_id, SC.status, SC.begin_at
            FROM ci_class_child CC INNER JOIN ci_child C ON CC.child_id = C.child_id
            INNER JOIN ci_school_child SC ON C.child_id = SC.child_id
            WHERE CC.class_id = %s AND CC.child_id = %s ORDER BY C.name_for_sort ASC", secure($class_id, 'int'), secure($child_id, 'int'));

        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_children->num_rows > 0) {
            $child = $get_children->fetch_assoc();
            $child['birthday'] = toSysDate($child['birthday']);
            $child['begin_at'] = toSysDate($child['begin_at']);
            if (!is_empty($child['child_picture'])) {
                $child['child_picture'] = $system['system_uploads'] . '/' . $child['child_picture'];
            }
            $child['parent'] = $this->getParent($child['child_id']);
            $results = $child;
        }

        return $results;
    }

    /**
     * Lấy ra thông tin ID trường, lớp, giới tính của trẻ.
     *
     * @param $childId
     * @param $schoolId
     * @return array|null
     * @throws Exception
     */
    public function getSchoolChildForApi($childId) {
        global $db;

        $strSql = sprintf("SELECT P.* FROM ci_school_child SC 
                           INNER JOIN ci_child C ON SC.child_id = C.child_id AND SC.child_id = %s
                           INNER JOIN pages P ON SC.school_id = P.page_id", secure($childId, 'int'));

        $get_school = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $result = array(
            'ids' => [],
            'lists' => []
        );
        if($get_school->num_rows > 0) {
            while ($school = $get_school->fetch_assoc()) {
                $school['page_picture'] = get_picture($school['page_picture'], 'school');
                $school['page_cover'] = get_picture($school['page_cover'], 'school');
                $result['ids'][] = $school['page_id'];
                $result['lists'][$school['page_id']] = $school;
            }
        }
        return $result;
    }
    /**
     * Lấy ra danh sách CHILD được quản lý bởi một phụ huynh
     *
     * @param $user_id
     * @return array
     * @throws Exception
     */
    public function getChildrenForParent($user_id) {
        global $db, $system;

        $strSql = "SELECT ci_child_parent.*, ci_parent_manage.school_id, ci_parent_manage.child_id FROM ci_child_parent INNER JOIN ci_parent_manage ON ci_child_parent.child_parent_id = ci_parent_manage.child_parent_id
                    AND ci_parent_manage.user_id = %s
                    ORDER BY name_for_sort ASC";

        $children = array();
        $get_children = $db->query(sprintf($strSql, secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if($get_children->num_rows > 0) {
            while($child = $get_children->fetch_assoc()) {
                if(!is_null($child['birthday'])) {
                    $child['birthday'] = toSysDate($child['birthday']);
                }
                if(!is_null($child['due_date_of_childbearing'])) {
                    $child['due_date_of_childbearing'] = toSysDate($child['due_date_of_childbearing']);
                }
                if (!is_empty($child['child_picture'])) {
                    $child['child_picture_path'] = $child['child_picture'];
                    $child['child_picture'] = $system['system_uploads'] . '/' . $child['child_picture'];
                }
                $date_now = date("Y-m-d");

                if($child['foetus_begin_date'] != null && $child['foetus_begin_date'] != '0000-00-00 00:00:00') {
                    $child['pregnant_week'] = (strtotime($date_now) - strtotime($child['foetus_begin_date'])) / (60 * 60 * 24 * 7);
                    $child['pregnant_week'] = (int)$child['pregnant_week'];
                } else {
                    $child['pregnant_week'] = null;
                }
                // Lấy thông tin ngày nhập học
                if($child['school_id'] > 0) {
                    $strSql = sprintf("SELECT begin_at FROM ci_school_child WHERE child_parent_id = %s AND status = 1", secure($child['child_parent_id'], 'int'));

                    $get_begin_at = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                    if($get_begin_at->num_rows > 0) {
                        $child['begin_at'] = $get_begin_at->fetch_assoc()['begin_at'];
                        $child['begin_at'] = toSysDate($child['begin_at']);
                    }
                }
                $children[] = $child;
            }
        }
        return $children;
    }

    /**
     * Lấy child_parent_id từ child_id hiện tại
     *
     * @param $child_id
     * @return null
     */
    public function getChildParentIdFromChildId ($child_id) {
        global $db;

        $strSql = sprintf("SELECT child_parent_id FROM ci_child WHERE child_id = %s", secure($child_id));

        $get_child_parent_id = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $child_parent_id = null;
        if($get_child_parent_id->num_rows > 0) {
            $child_parent_id = $get_child_parent_id->fetch_assoc()['child_parent_id'];
        }

        return $child_parent_id;
    }

    /**
     * Set child_admin cho trẻ
     *
     * @param $child_parent_id
     * @param $parent_id
     */
    public function setChildAdmin ($child_parent_id, $parent_id) {
        global $db;

        $strSql = sprintf("UPDATE ci_child_parent SET child_admin = %s WHERE child_parent_id = %s", secure($parent_id, 'int'), secure($child_parent_id, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Get child_admin từ child_id
     *
     * @param $childId
     * @return null
     */
    public function getChildAdminFromChildId($childId) {
        global $db;

        $strSql = sprintf("SELECT child_admin FROM ci_child_parent CP INNER JOIN ci_child C ON C.child_parent_id = CP.child_parent_id WHERE C.child_id = %s", secure($childId, 'int'));

        $get_child_admin = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $childAdmin = null;
        if($get_child_admin->num_rows > 0) {
            $childAdmin = $get_child_admin->fetch_assoc()['child_admin'];
        }
        return $childAdmin;
    }


    /* ------------------------------- */
    /* API RATING SCHOOL, TEACHER */
    /* ------------------------------- */

    /**
     * Kiểm tra user đã đánh đánh giá trường, giáo viên trong tháng hay chưa
     *
     * @param string $type
     * @param integer $object_id
     * @param integer $object_id
     * @return boolean
     */
    //public function checkExistUserRating($type, $object_id, $time) {
    public function checkReviewedSchool($schoolId, $time) {
        global $db, $user;

        $strSql = sprintf("SELECT user_id FROM ci_user_review WHERE user_id = %s AND type = 'school' AND school_id = %s AND time = %s", secure($user->_data['user_id'], 'int'), secure($schoolId, 'int'), secure($time));
        $get_user_rating = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_user_rating->num_rows > 0) {
            return true;
        }
        return false;
    }


    public function checkReviewedTeacher($teacherId, $classId, $time) {
        global $db, $user;

        $strSql = sprintf("SELECT user_id FROM ci_user_review WHERE user_id = %s AND type = 'teacher' AND teacher_id = %s AND class_id = %s AND time = %s", secure($user->_data['user_id'], 'int'), secure($teacherId, 'int'), secure($classId, 'int'), secure($time));
        $get_user_rating = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_user_rating->num_rows > 0) {
            return true;
        }
        return false;
    }

    /**
     * Tạo mới đánh giá cho trường
     *
     * @param string $type
     * @param integer $object_id
     * @param integer $object_id
     */
    public function createSchoolReview($schoolIds, $ratings, $comments, $time) {
        global $db, $user, $date;

        $strValues = "";
        for ($idx = 0; $idx < count($schoolIds); $idx++) {
            if (!in_array($ratings[$idx], range(1,5)) || !is_numeric($schoolIds[$idx])  || !isset($comments[$idx])) {
                _api_error(400);
            }

            $checkRating = $this->checkReviewedSchool($schoolIds[$idx], $time);
            if ($checkRating) {
                //throw new Exception(__("Bạn không thể gửi thêm đánh giá"));
                throw new Exception(__("You can not send more reviews"));
            }

            $strSql = sprintf("SELECT school_id FROM ci_review WHERE type = 'school' AND school_id = %s", secure($schoolIds[$idx], 'int'));
            $get_rating = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            if($get_rating->num_rows > 0) {
                $strSql = sprintf("UPDATE ci_review SET total_review = total_review + 1, average_review = (average_review*total_review + %s)/(total_review + 1) WHERE type = 'school' AND school_id = %s", secure($ratings[$idx], 'int', false), secure($schoolIds[$idx], 'int'));
            } else {
                $strSql = sprintf("INSERT INTO ci_review (type, school_id, total_review, average_review) VALUES ('school', %s, '1', %s)", secure($schoolIds[$idx], 'int'), secure($ratings[$idx]));
            }
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);

            $strValues .= "(" . secure($user->_data['user_id'], 'int') . ",'school'," . secure($schoolIds[$idx], 'int') . "," . secure($time) . "," . secure($ratings[$idx], 'int') . "," . secure($comments[$idx])  . "," . secure($date) . "),";
        }

        if (!is_empty($strValues)) {
            $strValues = trim($strValues, ",");
            $strSql = "INSERT INTO ci_user_review (user_id, type, school_id, time, rating, comment, created_at) VALUES " . $strValues;
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * Tạo mới đánh giá cho giáo viên
     *
     * @param string $type
     * @param integer $object_id
     * @param integer $object_id
     */
    public function createTeacherReview($teacherIds, $schoolIds, $classIds, $ratings, $comments, $time) {
        global $db, $user, $date;

        $strValues = "";
        for ($idx = 0; $idx < count($teacherIds); $idx++) {
            if (!in_array($ratings[$idx], range(1,5)) || !is_numeric($teacherIds[$idx])|| !is_numeric($schoolIds[$idx]) || !is_numeric($classIds[$idx])  || !isset($comments[$idx])) {
                _api_error(400);
            }

            $checkRating = $this->checkReviewedTeacher($teacherIds[$idx], $classIds[$idx], $time);
            if ($checkRating) {
                //throw new Exception(__("Không thể gửi thêm đánh giá"));
                throw new Exception(__("You can not send more reviews"));
            }

            $strSql = sprintf("SELECT teacher_id FROM ci_review WHERE type = 'teacher' AND teacher_id = %s AND school_id = %s", secure($teacherIds[$idx], 'int'), secure($schoolIds[$idx], 'int'));
            $get_rating = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            if($get_rating->num_rows > 0) {
                $strSql = sprintf("UPDATE ci_review SET total_review = total_review + 1, average_review = (average_review*total_review + %s)/(total_review + 1) WHERE type = 'teacher' AND teacher_id = %s AND school_id = %s",
                          secure($ratings[$idx], 'int', false), secure($teacherIds[$idx], 'int') , secure($schoolIds[$idx], 'int'));
            } else {
                $strSql = sprintf("INSERT INTO ci_review (type, school_id, teacher_id, total_review, average_review) VALUES ('teacher', %s, %s, '1', %s)", secure($schoolIds[$idx], 'int'), secure($teacherIds[$idx], 'int'), secure($ratings[$idx]));
            }
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);

            $strValues .= "(" . secure($user->_data['user_id'], 'int') . ",'teacher'," . secure($schoolIds[$idx], 'int') . "," . secure($classIds[$idx], 'int') . ",". secure($teacherIds[$idx], 'int') . "," . secure($time) . "," . secure($ratings[$idx], 'int') . "," . secure($comments[$idx])  . "," . secure($date) . "),";
        }

        if (!is_empty($strValues)) {
            $strValues = trim($strValues, ",");
            $strSql = "INSERT INTO ci_user_review (user_id, type, school_id, class_id, teacher_id, time, rating, comment, created_at) VALUES " . $strValues;
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }


    /**
     * Xóa thông tin người đón trẻ
     *
     * @param $childInfoId
     * @throws Exception
     */
    public function deleteChildInformation($childInfoId) {
        global $db;

        $strSql = sprintf("DELETE FROM ci_child_info WHERE child_info_id = %s", secure($childInfoId, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy thông tin chiều cao cân nặng của trẻ do trường hoặc giáo viên tạo ra
     *
     * @param $child_parent_id
     * @return array
     */
    public function getChildGrowthBySchoolOrClass($child_parent_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_child_growth WHERE child_parent_id = %s AND is_parent = 0 ORDER BY recorded_at DESC", secure($child_parent_id, 'int'));

        $get_growth = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $growths = array();
        if($get_growth->num_rows > 0) {
            while($temp = $get_growth->fetch_assoc()) {
                $temp['recorded_at'] = toSysDate($temp['recorded_at']);
                $growths[] = $temp;
            }
        }
        return $growths;
    }

    /**
     * Lấy lần cuối lưu thông tin sức khỏe của trẻ trong tháng do trường hoặc lớp tạo ra
     *
     * @param $child_parent_id
     * @param $month
     * @return array|null
     * @throws Exception
     */
    public function getChildGrowthLastestMonthBySchoolOrClass($child_parent_id, $month) {
        global $db;

        $firstDateOfMonth = toDBDate("01/" . $month);
        $firstDateOfNextMonth = addMonthToDBDate($firstDateOfMonth);

        $strSql = sprintf("SELECT * FROM ci_child_growth WHERE child_parent_id = %s
                  AND recorded_at < %s AND recorded_at >= %s AND is_parent = 0 ORDER BY recorded_at DESC LIMIT 1", secure($child_parent_id, 'int'), secure($firstDateOfNextMonth), secure($firstDateOfMonth));

        $child_growth_lastest_month = null;
        $get_child_lastest = $db->query($strSql) or _error('error', $strSql);
        if($get_child_lastest->num_rows > 0) {
            $child_growth_lastest_month = $get_child_lastest->fetch_assoc();
            $child_growth_lastest_month['recorded_at'] = toSysDate($child_growth_lastest_month['recorded_at']);
        }

        return $child_growth_lastest_month;
    }

    /**
     * Lấy lần gần lần cuối trong tháng do trường hoặc lớp tạo
     *
     * @param $child_parent_id
     * @param $recorded_lastest
     * @return array|null
     * @throws Exception
     */
    public function getChildGrowthNearLastestMonthBySchoolOrClass($child_parent_id, $recorded_lastest) {
        global $db;

        $recorded_lastest = toDBDate($recorded_lastest);

        $strSql = sprintf("SELECT * FROM ci_child_growth WHERE child_parent_id = %s
                  AND recorded_at < %s AND is_parent = 0 ORDER BY recorded_at DESC LIMIT 1", secure($child_parent_id, 'int'), secure($recorded_lastest));

        $child_growth_near_lastest_month = null;
        $get_child_near_lastest = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_child_near_lastest->num_rows > 0) {
            $child_growth_near_lastest_month = $get_child_near_lastest->fetch_assoc();
            $child_growth_near_lastest_month['recorded_at'] = toSysDate($child_growth_near_lastest_month['recorded_at']);
        }

        return $child_growth_near_lastest_month;
    }

    /**
     * Lấy danh sách chỉ số sức khỏe của trẻ trong 1 khoảng thời gian
     *
     * @param $childParentId
     * @param $begin
     * @param $end
     * @return array
     * @throws Exception
     */
    public function getChildGrowthSearchForChartBeginEnd($childParentId, $begin, $end) {
        global $db, $system;

        $begin = toDBDate($begin);
        $end = toDBDate($end);
        $strSql = sprintf("SELECT * FROM ci_child_growth WHERE child_parent_id = %s AND recorded_at >= %s AND recorded_at <= %s ORDER BY recorded_at DESC", secure($childParentId, 'int'), secure($begin), secure($end));

        $get_growth = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $growths = array();
        if($get_growth->num_rows > 0) {
            while($temp = $get_growth->fetch_assoc()) {
                $temp['recorded_at'] = toSysDate($temp['recorded_at']);
                if (!is_empty($temp['source_file'])) {
                    $temp['source_file_path'] = $temp['source_file'];
                    $temp['source_file'] = $system['system_uploads'] . '/' . $temp['source_file'];
                }
                if($temp['height'] > 0) {
                    $temp['bmi'] = $temp['weight'] * 10000 / ($temp['height'] * $temp['height']);
                    $temp['bmi'] = number_format($temp['bmi'], 2, '.', '');
                }
                $growths[] = $temp;
            }
        }

        return $growths;
    }

    /**
     * Thêm trẻ được chỉnh sửa vào bảng ci_child_edit_history
     *
     * @param $args
     * @throws Exception
     */
    public function insertChildEditHistory($args) {
        global $db, $date, $user;

        $strSql = sprintf("INSERT INTO ci_child_edit_history (school_id, child_id, edit_by, last_update, updated_user_id) VALUES (%s, %s, %s, %s, %s)", secure($args['school_id'], 'int'), secure($args['child_id'], 'int'), secure($args['edit_by'], 'int'), secure($date), secure($user->_data['user_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Kiểm tra xem trẻ đã được sửa trước đó chưa
     *
     * @param $childId
     * @return bool
     * @throws Exception
     */
    public function checkEditHistory($childId, $schoolId) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_child_edit_history WHERE child_id = %s AND school_id = %s", secure($childId, 'int'), secure($schoolId, 'int'));

        $get_child_edit = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_child_edit->num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Update child_edit_history
     *
     * @param $args
     * @throws Exception
     */
    public function updateChildEditHistory($args) {
        global $db, $date, $user;

        $strSql = sprintf("UPDATE ci_child_edit_history SET last_update = %s, updated_user_id = %s, edit_by = %s WHERE child_id = %s AND school_id = %s", secure($date), secure($user->_data['user_id'], 'int'), secure($args['edit_by'], 'int'), secure($args['child_id'], 'int'), secure($args['school_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Delete student edit trước đó 1 tháng (dùng để chạy ngầm)
     *
     * @throws Exception
     */
    public function deleteChildEditAfter1Month() {
        global $db;

        $datenow = date('Y-m-d');
        $dateBefore1Month = date('Y-m-d', strtotime("-1 months", strtotime($datenow)));
        $strSql = sprintf("DELETE FROM ci_child_edit_history WHERE last_update <= %s", secure($dateBefore1Month));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy danh sách trẻ được sửa theo điều kiện
     *
     * @param $schoolId
     * @param $datePre
     * @return array
     * @throws Exception
     */
    public function getChildEditHistory($schoolId, $datePre = "01/01/2000") {
        global $db;

        $datepre = toDBDate($datePre);
        $strSql = sprintf("SELECT CEH.*, C.child_name, U.user_fullname, SC.class_id FROM ci_child_edit_history CEH
                INNER JOIN ci_child C ON CEH.child_id = C.child_id
                INNER JOIN users U ON U.user_id = CEH.updated_user_id
                INNER JOIN ci_school_child SC ON SC.child_id = CEH.child_id
                WHERE CEH.school_id = %s AND CEH.last_update >= %s ORDER BY class_id ASC, last_update DESC", secure($schoolId, 'int'), secure($datepre));

        $get_child_edit_history = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $childEdits = array();
        if($get_child_edit_history->num_rows > 0) {
            while($child_edit = $get_child_edit_history->fetch_assoc()) {
                $child_edit['last_update'] = toSysDate($child_edit['last_update']);
                $childEdits[] = $child_edit;
            }
        }

        return $childEdits;
    }

    /**
     * Lấy danh sách trẻ được thêm mới trong tháng
     *
     * @param $schoolId
     * @param string $datePre
     * @param $today
     * @return array
     * @throws Exception
     */
    public function getChildAddNewInTheMonth($schoolId, $datePre = "01/01/2000", $today) {
        global $db;

        $datepre = toDBDate($datePre);
        $today = toDBDate($today);
        $strSql = sprintf("SELECT C.child_id, C.child_name, U.user_fullname, SC.class_id, SC.begin_at FROM ci_child C
                INNER JOIN users U ON U.user_id = C.created_user_id
                INNER JOIN ci_school_child SC ON SC.child_id = C.child_id
                WHERE SC.school_id = %s AND SC.begin_at >= %s AND SC.begin_at <= %s ORDER BY SC.class_id ASC, SC.begin_at DESC", secure($schoolId, 'int'), secure($datepre), secure($today));

        $get_child_edit_history = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $childEdits = array();
        if($get_child_edit_history->num_rows > 0) {
            while($child_edit = $get_child_edit_history->fetch_assoc()) {
                $child_edit['begin_at'] = toSysDate($child_edit['begin_at']);
                $childEdits[] = $child_edit;
            }
        }

        return $childEdits;
    }

    /**
     * Lấy danh sách tất cả trẻ có trong bảng ci_child_edit_history (phục vụ chạy ngầm)
     *
     * @return array
     * @throws Exception
     */
    public function getAllChildEditHistory() {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_child_edit_history");

        $get_child_edit_history = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $childEdits = array();
        if($get_child_edit_history->num_rows > 0) {
            while($child_edit = $get_child_edit_history->fetch_assoc()) {
                $child_edit['last_update'] = toSysDate($child_edit['last_update']);
                $childEdits[] = $child_edit;
            }
        }

        return $childEdits;
    }

    /**
     * Lấy ngày bắt đầu đi học trong lớp của trẻ
     *
     * @param $childId
     * @param $classId
     * @return null
     * @throws Exception
     */
    public function getChildBeginAtInClass($childId, $classId) {
        global $db;

        $strSql = sprintf("SELECT begin_at FROM ci_class_child WHERE child_id = %s AND class_id = %s AND status = 1", secure($childId, 'int'), secure($classId, 'int'));

        $get_begin_at = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $begin_at = null;
        if($get_begin_at->num_rows > 0) {
            $begin_at = $get_begin_at->fetch_assoc()['begin_at'];
        }

        return $begin_at;
    }

    /**
     * Lấy danh sách trẻ chưa phân lớp của trường
     *
     * @param $schoolId
     * @return array
     * @throws Exception
     */
    public function getChildNoClass($schoolId) {
        global $db;

        $strSql = sprintf('SELECT C.*, SC.begin_at, SC.end_at, SC.class_id FROM ci_school_child SC
                            INNER JOIN ci_child C ON SC.school_id = %s AND SC.class_id = 0 AND SC.child_id = C.child_id AND SC.status = %s
                            ORDER BY C.name_for_sort ASC', secure($schoolId, 'int'), STATUS_ACTIVE);

        $get_child = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $children = array();
        if($get_child->num_rows > 0) {
            while ($child = $get_child->fetch_assoc()) {
                $children[] = $child;
            }
        }

        return $children;
    }

    /**
     * Ẩn trẻ khỏi danh sách trẻ thôi học
     *
     * @param $schoolId
     * @param $childId
     * @throws Exception
     */
    public function deleteChildLeave ($schoolId, $childId) {
        global $db;

        $strSql = sprintf("UPDATE ci_school_child SET is_delete = %s WHERE school_id = %s AND child_id = %s AND status = %s", STATUS_ACTIVE, secure($schoolId, 'int'), secure($childId, 'int'), STATUS_INACTIVE);

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Cập nhật child_admin cho trẻ
     *
     * @param $child_parent_id
     * @param $child_admin
     * @throws Exception
     */
    public function updateChildAdmin($child_parent_id, $child_admin) {
        global $db;

        $strSql = sprintf("UPDATE ci_child_parent SET child_admin = %s WHERE child_parent_id = %s", secure($child_admin, 'int'), secure($child_parent_id, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Xóa trẻ trong bảng ci_school_child
     *
     * @param $schoolId
     * @throws Exception
     */
    public function deleteChildOfSchool($schoolId) {
        global $db;

        $strSql = sprintf("DELETE FROM ci_school_child WHERE school_id = %s", secure($schoolId, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    public function deleteChildOfClass ($classIds) {
        global $db;

        $strCon = "";
        if(count($classIds) > 0) {
            $strCon = implode(",", $classIds);
            $strSql = sprintf("DELETE FROM ci_class_child WHERE class_id IN (%s)", $strCon);

            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * Kiểm tra xem mã trẻ có trong lớp không
     *
     * @param $childCode
     * @param $classId
     * @return bool
     * @throws Exception
     */
    public function checkExistChildByCodeAndClassId($childCode, $classId) {
        global $db, $system;

        $strSql = sprintf("SELECT * FROM ci_child C INNER JOIN ci_class_child CC ON C.child_id = CC.child_id
                           WHERE C.child_code = %s AND CC.class_id = %s", secure($childCode), secure($classId, 'int'));

        $get_child = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_child->num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }
    /**
     * Lấy child_id by child code
     *
     * @param $childCode
     * @return int
     * @throws Exception
     */
    public function getChildIdByCode($childCode) {
        global $db, $system;

        $strSql = sprintf("SELECT child_id FROM ci_child
                           WHERE child_code = %s GROUP BY child_id DESC", secure($childCode));


        $childId = null;
        $get_child = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_child->num_rows > 0) {
            $childId = $get_child->fetch_assoc()['child_id'];
        }
        return $childId;
    }
    /**
     * Lấy thông tin user của học sinh by childId
     *
     * @param $childId
     * @return array
     * @throws Exception
     */
    public function getUserOfChildByChildId($childId) {
        global $db, $system;

        $strSql = sprintf("SELECT * FROM `ci_user_manage`
                           WHERE object_id = %s AND object_type= '0'", secure($childId));


        $userInfo = null;
        $get_child = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_child->num_rows > 0) {
            $userInfo = $get_child->fetch_assoc();
        }
        return $userInfo;
    }
}
?>