<?php
/**
 * Created by PhpStorm.
 * User: TaiLA
 * Date: 20/02/2017
 * Time: 09:54 PM
 */
/* Class PermissionDao: Thao tác với bảng ci_role của hệ thống*/

class RoleDao {
    /**
     * __construct
     *
     */
    public function __construct() {
    }

    /**
     * Kiểm tra đầu vào cho hàm insert và update
     *
     * @param array $args
     * @throws Exception
     */
    private function _validateInput(array $args = array())
    {
        if (is_empty($args['role_name'])) {
            throw new Exception(__("You must enter role name"));
        }
        if (strlen($args['role_name']) > 255) {
            throw new Exception(__("Role name length must be less than 255 characters long"));
        }
    }


    /**
     * Xoá một vai trò
     *
     * @param $schoolId
     * @param $roleId
     * @throws Exception
     */
    public function deleteRole($schoolId, $roleId) {
        global $db;

        /* delete the ci_role  */
        $strSql = sprintf("DELETE FROM ci_role WHERE role_id = %s AND school_id = %s", secure($roleId, 'int'), secure($schoolId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        /* delete the ci_role_permission */
        $db->query(sprintf("DELETE FROM ci_role_permission WHERE role_id = %s", secure($roleId, 'int'))) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Xoá tất cả role của user trong trường.
     *
     * @param $schoolId
     * @param $userId
     * @throws Exception
     */
    public function deleteUserRole($schoolId, $userId) {
        global $db;
        $strSql = sprintf("DELETE FROM ci_user_role WHERE school_id = %s AND user_id = %s", secure($schoolId, 'int'), secure($userId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy ra danh sách vai trò của một trường.
     *
     * @param $schoolId
     * @return array
     * @throws Exception
     */
    public function getRoleOfSchool($schoolId) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_role WHERE school_id = %s ORDER BY role_name ASC", secure($schoolId, 'int'));
        $roles = array();
        $get_roles = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        while ($role = $get_roles->fetch_assoc()) {
            $role['users'] = $this->_getUserOfRole($role['role_id']);
            $roles[] = $role;
        }
        return $roles;
    }

    /**
     * Lấy ra danh sách user của một role
     *
     * @param $roleId
     * @return array
     * @throws Exception
     */
    private function _getUserOfRole($roleId) {
        global $db;

        $strSql = sprintf("SELECT UR.*, U.user_fullname FROM ci_user_role UR
                    INNER JOIN users U ON UR.user_id = U.user_id
                    INNER JOIN ci_teacher C ON C.user_id = UR.user_id AND C.school_id = UR.school_id
                    WHERE UR.role_id = %s
                    ORDER BY U.user_firstname, U.user_lastname ASC", secure($roleId, 'int'));
        $userRoles = array();
        $get_roles = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        while ($userRole = $get_roles->fetch_assoc()) {
            $userRoles[] = $userRole;
        }
        return $userRoles;
    }

    /**
     * Insert một vai trò vào DB
     *
     * @param $args
     * @param $permissions
     * @throws Exception
     */
    public function insertRole($args, $permissions) {
        global $db;
        $this->_validateInput($args);

        //1. Insert role vào ci_role
        $strSql = sprintf("INSERT INTO ci_role (role_name, school_id, description) VALUES (%s, %s, %s)",
            secure($args['role_name']), secure($args['school_id'], 'int'), secure($args['description']));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        //2. Insert permission của role vào DB
        $roleId = $db->insert_id;
        if (count($permissions) > 0) {
            //Câu sql sẽ có dạng: INSERT INTO tbl_name (col_A,col_B,col_C) VALUES (1,2,3), (4,5,6), (7,8,9)
            //Build lên phần giá trị truyền vào.
            $strValues = "";
            foreach ($permissions as $permission) {
                //role_id, module, value
                $strValues .= "(" . secure($roleId, 'int') . "," . secure($permission['module']) . "," . secure($permission['value'], 'int') . "),";
            }
            $strValues = trim($strValues, ",");

            $strSql = "INSERT INTO ci_role_permission (role_id, module, value) VALUES " . $strValues;
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * Set danh sách user vào một role.
     *
     * @param $schoolId
     * @param $roleId
     * @param $userIds
     * @throws Exception
     */
    public function insertUser2Role($schoolId, $roleId, $userIds) {
        global $db;

        //1. Xoá tất cả user có role ABC trong trường.
        $strSql = sprintf("DELETE FROM ci_user_role WHERE school_id = %s AND role_id = %s", secure($schoolId, 'int'), secure($roleId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        if (count($userIds) > 0) {
            //Câu sql sẽ có dạng: INSERT INTO tbl_name (col_A,col_B,col_C) VALUES (1,2,3), (4,5,6), (7,8,9)
            //Build lên phần giá trị truyền vào.
            $strValues = "";
            foreach ($userIds as $userId) {
                //school_id, role_id, user_id
                $strValues .= "(" . secure($schoolId, 'int') . "," . secure($roleId, 'int') . "," . secure($userId, 'int') . "),";
            }
            $strValues = trim($strValues, ",");
            $strSql = "INSERT INTO ci_user_role (school_id, role_id, user_id) VALUES " . $strValues;
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * Set danh sách các role vào user.
     *
     * @param $schoolId
     * @param $roleIds
     * @param $userId
     * @throws Exception
     */
    public function insertRole2User($schoolId, $roleIds, $userId) {
        global $db;

        //1. Xoá tất cả role của user trong trường.
        $strSql = sprintf("DELETE FROM ci_user_role WHERE school_id = %s AND user_id = %s", secure($schoolId, 'int'), secure($userId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        if (count($roleIds) > 0) {
            //Câu sql sẽ có dạng: INSERT INTO tbl_name (col_A,col_B,col_C) VALUES (1,2,3), (4,5,6), (7,8,9)
            //Build lên phần giá trị truyền vào.
            $strValues = "";
            foreach ($roleIds as $roleId) {
                //school_id, role_id, user_id
                $strValues .= "(" . secure($schoolId, 'int') . "," . secure($roleId, 'int') . "," . secure($userId, 'int') . "),";
            }
            $strValues = trim($strValues, ",");
            $strSql = "INSERT INTO ci_user_role (school_id, role_id, user_id) VALUES " . $strValues;
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * Tạo một số vai trò mặc định trong trường.
     *
     * @param $schoolId
     * @throws Exception
     */
    public function insertDefaultRoles($schoolId) {
        global $db;

        //1. KẾ TOÁN
        $strSql = sprintf("INSERT INTO ci_role (role_name, school_id, description) VALUES (%s, %s, %s)", secure('Kế toán'), secure($schoolId, 'int'),
            secure("Vai trò cho kế toàn nhà trường, xem thông tin điểm danh, dịch vụ...để tính học phí."));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $roleId = $db->insert_id;

        //Câu sql sẽ có dạng: INSERT INTO tbl_name (col_A,col_B,col_C) VALUES (1,2,3), (4,5,6), (7,8,9)
        //role_id, module, value
        $strSql = "INSERT INTO ci_role_permission (role_id, module, value) VALUES ";
        $strSql .= "(".secure($roleId, 'int').",'events',1),";
        $strSql .= "(".secure($roleId, 'int').",'attendance',1),";
        $strSql .= "(".secure($roleId, 'int').",'birthdays',1),";
        $strSql .= "(".secure($roleId, 'int').",'classes',1),";
        $strSql .= "(".secure($roleId, 'int').",'classlevels',1),";
        $strSql .= "(".secure($roleId, 'int').",'pickup',1),";
        $strSql .= "(".secure($roleId, 'int').",'children',1),";
        $strSql .= "(".secure($roleId, 'int').",'teachers',1),";
        $strSql .= "(".secure($roleId, 'int').",'services',2),";
        $strSql .= "(".secure($roleId, 'int').",'tuitions',2),";
        $strSql .= "(".secure($roleId, 'int').",'loginstatistics',2)";
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        //2. ĐẦU BẾP
        $strSql = sprintf("INSERT INTO ci_role (role_name, school_id, description) VALUES (%s, %s, %s)", secure('Đầu bếp'), secure($schoolId, 'int'),
            secure("Vai trò dành cho nhà bếp, xem thông tin trẻ đi/nghỉ và dịch vụ ăn uống."));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $roleId = $db->insert_id;

        //Câu sql sẽ có dạng: INSERT INTO tbl_name (col_A,col_B,col_C) VALUES (1,2,3), (4,5,6), (7,8,9)
        //role_id, module, value
        $strSql = "INSERT INTO ci_role_permission (role_id, module, value) VALUES ";
        $strSql .= "(".secure($roleId, 'int').",'events',1),";
        $strSql .= "(".secure($roleId, 'int').",'attendance',1),";
        $strSql .= "(".secure($roleId, 'int').",'classes',1),";
        $strSql .= "(".secure($roleId, 'int').",'pickup',1),";
        $strSql .= "(".secure($roleId, 'int').",'teachers',1),";
        $strSql .= "(".secure($roleId, 'int').",'services',1),";
        $strSql .= "(".secure($roleId, 'int').",'menus',1)";
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        //3. CHỈ XEM TẤT CẢ
        $strSql = sprintf("INSERT INTO ci_role (role_name, school_id, description) VALUES (%s, %s, %s)", secure('Xem tất cả'), secure($schoolId, 'int'),
            secure("Vai trò cho người được xem tất cả thông tin trường, nhưng KHÔNG tương tác."));
        $db->query($strSql) or _error('chixem', $strSql);
        $roleId = $db->insert_id;

        //Câu sql sẽ có dạng: INSERT INTO tbl_name (col_A,col_B,col_C) VALUES (1,2,3), (4,5,6), (7,8,9)
        //role_id, module, value
        $strSql = "INSERT INTO ci_role_permission (role_id, module, value) VALUES ";
        $strSql .= "(".secure($roleId, 'int').",'events',1),";
        $strSql .= "(".secure($roleId, 'int').",'attendance',1),";
        $strSql .= "(".secure($roleId, 'int').",'birthdays',1),";
        $strSql .= "(".secure($roleId, 'int').",'classes',1),";
        $strSql .= "(".secure($roleId, 'int').",'feedback',1),";
        $strSql .= "(".secure($roleId, 'int').",'tuitions',1),";
        $strSql .= "(".secure($roleId, 'int').",'medicines',1),";
        $strSql .= "(".secure($roleId, 'int').",'pickup',1),";
        $strSql .= "(".secure($roleId, 'int').",'reports',1),";
        $strSql .= "(".secure($roleId, 'int').",'schedules',1),";
        $strSql .= "(".secure($roleId, 'int').",'services',1),";
        $strSql .= "(".secure($roleId, 'int').",'teachers',1),";
        $strSql .= "(".secure($roleId, 'int').",'children',1),";
        $strSql .= "(".secure($roleId, 'int').",'roles',1),";
        $strSql .= "(".secure($roleId, 'int').",'settings',1),";
        $strSql .= "(".secure($roleId, 'int').",'classlevels',1),";
        $strSql .= "(".secure($roleId, 'int').",'menus',1),";
        $strSql .= "(".secure($roleId, 'int').",'diarys',1),";
        $strSql .= "(".secure($roleId, 'int').",'healths',1),";
        $strSql .= "(".secure($roleId, 'int').",'managegroups',1),";
        $strSql .= "(".secure($roleId, 'int').",'loginstatistics',1)";

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        //4. TOÀN QUYỀN
        $strSql = sprintf("INSERT INTO ci_role (role_name, school_id, description) VALUES (%s, %s, %s)", secure('Toàn quyền'), secure($schoolId, 'int'),
            secure("Người có vai trò này có thể thực hiện mọi việc trong quản lý."));
        $db->query($strSql) or _error('full', $strSql);
        $roleId = $db->insert_id;

        //Câu sql sẽ có dạng: INSERT INTO tbl_name (col_A,col_B,col_C) VALUES (1,2,3), (4,5,6), (7,8,9)
        //role_id, module, value
        $strSql = "INSERT INTO ci_role_permission (role_id, module, value) VALUES ";
        $strSql .= "(".secure($roleId, 'int').",'events',2),";
        $strSql .= "(".secure($roleId, 'int').",'attendance',2),";
        $strSql .= "(".secure($roleId, 'int').",'birthdays',2),";
        $strSql .= "(".secure($roleId, 'int').",'classes',2),";
        $strSql .= "(".secure($roleId, 'int').",'feedback',2),";
        $strSql .= "(".secure($roleId, 'int').",'tuitions',2),";
        $strSql .= "(".secure($roleId, 'int').",'medicines',2),";
        $strSql .= "(".secure($roleId, 'int').",'pickup',2),";
        $strSql .= "(".secure($roleId, 'int').",'reports',2),";
        $strSql .= "(".secure($roleId, 'int').",'schedules',2),";
        $strSql .= "(".secure($roleId, 'int').",'services',2),";
        $strSql .= "(".secure($roleId, 'int').",'teachers',2),";
        $strSql .= "(".secure($roleId, 'int').",'children',2),";
        $strSql .= "(".secure($roleId, 'int').",'roles',2),";
        $strSql .= "(".secure($roleId, 'int').",'settings',2),";
        $strSql .= "(".secure($roleId, 'int').",'classlevels',2),";
        $strSql .= "(".secure($roleId, 'int').",'menus',2),";
        $strSql .= "(".secure($roleId, 'int').",'diarys',2),";
        $strSql .= "(".secure($roleId, 'int').",'healths',2),";
        $strSql .= "(".secure($roleId, 'int').",'managegroups',2),";
        $strSql .= "(".secure($roleId, 'int').",'loginstatistics',2)";

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Cập nhật thông tin một role.
     *
     * @param $args
     * @param $permissions
     * @throws Exception
     */
    public function updateRole($args, $permissions) {
        global $db;
        $this->_validateInput($args);

        //1. Cập nhât role vào ci_role
        $strSql = sprintf("UPDATE ci_role SET role_name = %s, description = %s WHERE role_id = %s",
            secure($args['role_name']), secure($args['description']), secure($args['role_id'], 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        //2. Xoá tất cả permission của role hiện tại.
        $db->query(sprintf("DELETE FROM ci_role_permission WHERE role_id = %s", secure($args['role_id'], 'int'))) or _error(SQL_ERROR_THROWEN);

        if (count($permissions) > 0) {
            //3. Insert permission của role vào DB
            //Câu sql sẽ có dạng: INSERT INTO tbl_name (col_A,col_B,col_C) VALUES (1,2,3), (4,5,6), (7,8,9)
            //Build lên phần giá trị truyền vào.
            $strValues = "";
            foreach ($permissions as $permission) {
                //role_id, module, value
                $strValues .= "(" . $args['role_id'] . ",'" . $permission['module'] . "'," . $permission['value'] . "),";
            }
            $strValues = trim($strValues, ",");
            $strSql = "INSERT INTO ci_role_permission (role_id, module, value) VALUES " . $strValues;
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * Lấy ra thông tin một vai trò, bao gồm cả permission của nó.
     *
     * @param $roleId
     * @return array|null
     * @throws Exception
     */
    public function getRoleWithPermissions($roleId) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_role WHERE role_id = %s", secure($roleId, 'int'));
        $get_roles = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_roles->num_rows > 0) {
            $role = $get_roles->fetch_assoc();
            $role['permissions'] = $this->getRolePermissions($roleId);

            return $role;
        }
        return null;
    }

    /**
     * Lấy ra thông tin một vai trò bao gồm danh sách đầy đủ nhân viên của trường.
     * Danh sách nhân viên: nếu người nào có vai trò này thì trong thông tin của người đó: role_id > 0
     *
     * @param $roleId
     * @return array|null
     * @throws Exception
     */
    public function getRoleWithUser($roleId) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_role WHERE role_id = %s", secure($roleId, 'int'));
        $get_roles = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_roles->num_rows > 0) {
            $role = $get_roles->fetch_assoc();
            $role['users'] = $this->getUserHaveRole($role['school_id'], $roleId);

            return $role;
        }
        return null;
    }

    /**
     * Lấy ra danh sách người dùng có vai trò ABC.
     * LẤY TẤT CẢ GIÁO VIÊN CỦA TRƯỜNG RA LUÔN
     *
     * @param $schoolId
     * @param $roleId
     * @return array
     * @throws Exception
     */
    public function getUserHaveRole ($schoolId, $roleId) {
        global $db;

        $userRoles = array();
        $strSql = sprintf('SELECT T.user_id, U.user_fullname, UR.role_id FROM ci_teacher T
                            INNER JOIN users U ON T.user_id = U.user_id
                            LEFT JOIN ci_user_role UR ON T.user_id = UR.user_id AND UR.role_id = %s AND UR.school_id = %s
                            WHERE T.school_id = %s', secure($roleId, 'int'), secure($schoolId, 'int'), secure($schoolId, 'int'));
        $get_userRoles = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        while ($userRole = $get_userRoles->fetch_assoc()) {
            $userRoles[] = $userRole;
        }
        return $userRoles;
    }

    /**
     * Lấy ra danh sách id người dùng có vai trò ABC.
     * Không lấy những giáo viên không có vai trò
     *
     * @param $schoolId
     * @param $roleId
     * @return array
     * @throws Exception
     */
    public function getUserIdHaveRole ($schoolId, $roleId) {
        global $db;

        $userRoles = array();
        $strSql = sprintf('SELECT user_id FROM ci_user_role WHERE role_id = %s AND school_id = %s', secure($roleId, 'int'), secure($schoolId, 'int'));
        $get_userRoles = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        while ($userRole = $get_userRoles->fetch_assoc()['user_id']) {
            $userRoles[] = $userRole;
        }
        return $userRoles;
    }

    /**
     * Lấy ra danh sách vai trò của một user.
     *
     * @param $schoolId
     * @param $userId
     * @return array
     * @throws Exception
     */
    public function getRoleOfUser ($schoolId, $userId) {
        global $db;

        $userRoles = array();
        $strSql = sprintf('SELECT R.*, UR.user_id FROM ci_role R LEFT JOIN ci_user_role UR ON R.role_id = UR.role_id AND UR.user_id = %s
                            WHERE R.school_id = %s ORDER BY R.role_name ASC', secure($userId, 'int'), secure($schoolId, 'int'));
        $get_userRoles = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        while ($userRole = $get_userRoles->fetch_assoc()) {
            $userRoles[] = $userRole;
        }
        return $userRoles;
    }

    /**
     * Lấy ra tất cả permissions của một vai trò.
     *
     * @param $roleId
     * @return null
     * @throws Exception
     */
    public function getRolePermissions ($roleId) {
        global $db;

        $permissions = array();
        $strSql = sprintf("SELECT * FROM ci_role_permission WHERE role_id = %s", secure($roleId, 'int'));
        $get_permissions = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        while ($per = $get_permissions->fetch_assoc()) {
            $permissions[] = $per;
        }
        return $permissions;
    }

    /**
     * Lấy ra quyền của người dùng đối với một trường.
     * Nếu người dùng có nhiều role khác nhau thì hợp lại, lấy quyền cao nhất đối với từng module.
     *
     * @param $schoolId
     * @param $userId
     * @return array
     * @throws Exception
     */
    public function getPermissionOfSchool($schoolId, $userId) {
        global $db, $systemModules;
        //Lấy ra danh sách các quyền mà người dùng có
        $strSql = sprintf("SELECT RP.* FROM ci_role_permission RP
                            INNER JOIN ci_user_role UR ON RP.role_id = UR.role_id AND UR.user_id = %s AND UR.school_id = %s",
                            secure($userId, 'int'), secure($schoolId, 'int'));
        $ownPermissions = array();
        $get_permission = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        while ($permission = $get_permission->fetch_assoc()) {
            $ownPermissions[] = $permission;
        }

        $newPermissions = array();
        foreach ($systemModules as $module) {
            $newPer = $module;
            $newPer['value'] = PERM_NONE;
            foreach ($ownPermissions as $per) {
                if (($newPer['module'] == $per['module']) && ($newPer['value'] < $per['value'])) {
                    $newPer['value'] = $per['value'];
                }
            }
            $newPermissions[] = $newPer;
        }
        return $newPermissions;
    }

    /**
     * Lấy danh sách user có quyền đối với một view
     *
     * @param $school_id
     * @param $view
     * @return array
     * @throws Exception
     */
    public function getUserIdsOfModule($school_id, $view) {
        global $db;

        $strSql = sprintf("SELECT UR.user_id FROM ci_user_role UR 
                  INNER JOIN ci_role_permission RP ON RP.role_id = UR.role_id
                  WHERE UR.school_id = %s AND RP.value != %s AND RP.module = %s", secure($school_id, 'int'), PERM_NONE, secure($view));
        $get_user_ids = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $userIds = array();
        if($get_user_ids->num_rows > 0) {
            while($userId = $get_user_ids->fetch_assoc()['user_id']) {
                $userIds[] = $userId;
            }
        }

        return $userIds;
    }

    /**
     * Lấy danh sách user_id được phân quyền của trường
     *
     * @param $schoolId
     * @return array
     * @throws Exception
     */
    public function getAllUserManageSchool($schoolId) {
        global $db;

        $strSql = sprintf("SELECT user_id FROM ci_user_role WHERE school_id = %s", secure($schoolId, 'int'));

        $get_user_ids = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $userIds = array();
        if($get_user_ids->num_rows > 0) {
            while($userId = $get_user_ids->fetch_assoc()['user_id']) {
                $userIds[] = $userId;
            }
        }

        return $userIds;
    }
}
?>