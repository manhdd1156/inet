<?php
/**
 * DAO -> Statistic
 * Thao tác với bảng ci_review
 * 
 * @package Coniu
 * @author Coniu
 */

class StatisticDAO {
    /**
     * __construct
     *
     */
    public function __construct() {
    }

    /**
     * Lấy ra thống kê tương tác trong 30 ngày gần nhất của trường
     *
     * @return array
     */
    public function getSchoolStatisticInLast30Days($school_id) {
        global $db;

        $result = array(
            'page' => array(
                'count_views' => 0
            ),
            'post' => array(
                'count_created' => 0,
                'count_views' => 0
            ),
            'event' => array(
                'count_created' => 0,
                'count_views' => 0
            ),
            'schedule' => array(
                'count_created' => 0,
                'count_views' => 0
            ),
            'menu' => array(
                'count_created' => 0,
                'count_views' => 0
            ),
            'report' => array(
                'count_created' => 0,
                'count_views' => 0
            ),
            'tuition' => array(
                'count_created' => 0,
                'count_views' => 0
            ),
        );
        $strSql = sprintf("SELECT view_type, created_in_thirty_days, views_in_thirty_days FROM ci_views_count WHERE school_id = %s", secure($school_id, 'int'));
        $get_statistics = $db->query($strSql) or secure('error', $strSql);
        if($get_statistics->num_rows > 0) {
            while($statistic = $get_statistics->fetch_assoc()) {
                if ($statistic['view_type'] == 'page') {
                    $result['page']['count_views'] = $statistic['views_in_thirty_days'];
                }
                if ($statistic['view_type'] == 'post') {
                    $result['post']['count_created'] = $statistic['created_in_thirty_days'];
                    $result['post']['count_views'] = $statistic['views_in_thirty_days'];
                }
                if ($statistic['view_type'] == 'event') {
                    $result['event']['count_created'] = $statistic['created_in_thirty_days'];
                    $result['event']['count_views'] = $statistic['views_in_thirty_days'];
                }
                if ($statistic['view_type'] == 'schedule') {
                    $result['schedule']['count_created'] = $statistic['created_in_thirty_days'];
                    $result['schedule']['count_views'] = $statistic['views_in_thirty_days'];
                }
                if ($statistic['view_type'] == 'menu') {
                    $result['menu']['count_created'] = $statistic['created_in_thirty_days'];
                    $result['menu']['count_views'] = $statistic['views_in_thirty_days'];
                }
                if ($statistic['view_type'] == 'report') {
                    $result['report']['count_created'] = $statistic['created_in_thirty_days'];
                    $result['report']['count_views'] = $statistic['views_in_thirty_days'];
                }
                if ($statistic['view_type'] == 'tuition') {
                    $result['tuition']['count_created'] = $statistic['created_in_thirty_days'];
                    $result['tuition']['count_views'] = $statistic['views_in_thirty_days'];
                }
            }
        }

        return $result;
    }


    /**
     * Lấy ra thống kê tương tác toàn trường
     *
     * @return array
     */
    public function getSchoolStatistic($school_id) {
        global $db;

        $result = array(
            'page' => array(),
            'post' => array(),
            'event' => array(),
            'schedule' => array(),
            'menu' => array(),
            'report' => array(),
            'tuition' => array(),

            'post_created' => array(),
            'event_created' => array(),
            'schedule_created' => array(),
            'menu_created' => array(),
            'report_created' => array(),
            'tuition_created' => array()
        );

        $strSql = sprintf("SELECT * FROM ci_views_count_detail WHERE school_id = %s", secure($school_id, 'int'));
        $get_statistics = $db->query($strSql) or secure('error', $strSql);
        if($get_statistics->num_rows > 0) {
            while($statistic = $get_statistics->fetch_assoc()) {
                if ($statistic['view_type'] == 'page') {
                    $result['page'][$statistic['date']] = $statistic['views_count_in_date'];
                } elseif ($statistic['view_type'] == 'post') {
                    if ($statistic['is_add_new']) {
                        $result['post_created'][$statistic['date']][] = $statistic['object_id'];
                    }
                    if ($statistic['views_count_in_date'] > 0) {
                        $result['post'][$statistic['date']][$statistic['object_id']] = $statistic['views_count_in_date'];
                    }
                } elseif ($statistic['view_type'] == 'event') {
                    if ($statistic['is_add_new']) {
                        $result['event_created'][$statistic['date']][] = $statistic['object_id'];
                    }
                    if ($statistic['views_count_in_date'] > 0) {
                        $result['event'][$statistic['date']][$statistic['object_id']] = $statistic['views_count_in_date'];
                    }
                } elseif ($statistic['view_type'] == 'schedule') {
                    if ($statistic['is_add_new']) {
                        $result['schedule_created'][$statistic['date']][] = $statistic['object_id'];
                    }
                    if ($statistic['views_count_in_date'] > 0) {
                        $result['schedule'][$statistic['date']][$statistic['object_id']] = $statistic['views_count_in_date'];
                    }
                } elseif ($statistic['view_type'] == 'menu') {
                    if ($statistic['is_add_new']) {
                        $result['menu_created'][$statistic['date']][] = $statistic['object_id'];
                    }
                    if ($statistic['views_count_in_date'] > 0) {
                        $result['menu'][$statistic['date']][$statistic['object_id']] = $statistic['views_count_in_date'];
                    }
                } elseif ($statistic['view_type'] == 'report') {
                    if ($statistic['is_add_new']) {
                        $result['report_created'][$statistic['date']][] = $statistic['object_id'];
                    }
                    if ($statistic['views_count_in_date'] > 0) {
                        $result['report'][$statistic['date']][$statistic['object_id']] = $statistic['views_count_in_date'];
                    }
                } elseif ($statistic['view_type'] == 'tuition') {
                    if ($statistic['is_add_new']) {
                        $result['tuition_created'][$statistic['date']][] = $statistic['object_id'];
                    }
                    if ($statistic['views_count_in_date'] > 0) {
                        $result['tuition'][$statistic['date']][$statistic['object_id']] = $statistic['views_count_in_date'];
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Cập nhật số lượt xem trang trường
     *
     */
    public function insertViewsCountOfPage($args = array()) {
        global $db;
        if (!is_empty($args['view_type']) && !is_empty($args['date']) && is_numeric($args['school_id']) && is_numeric($args['views_count_in_date']) ) {
            $strSql = sprintf('INSERT INTO ci_views_count_detail (view_type, date, object_id, school_id, views_count_in_date) VALUES (%1$s, %2$s, %3$s, %3$s, %4$s) 
                  ON DUPLICATE KEY UPDATE views_count_in_date = %4$s',
                secure($args['view_type']), secure($args['date']), secure($args['school_id'], 'int'), secure($args['views_count_in_date'], 'int'));
            $db->query($strSql) or secure('error', $strSql);
        }
    }

    /**
     * Lấy ra số lượt xem và tạo trong 30 ngày gần nhất
     *
     */
    public function updateViewsCountInLast30Days($school_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_views_count_detail WHERE school_id = %s AND date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)", secure($school_id, 'int'));
        $get_statistics = $db->query($strSql) or secure('error', $strSql);

        $cntPage = 0; $cntPost = 0; $cntNewPost = 0; $cntEvent = 0; $cntNewEvent = 0;
        $cntSchedule = 0; $cntNewSchedule = 0; $cntMenu = 0; $cntNewMenu = 0;
        $cntReport = 0; $cntNewReport = 0; $cntTuition = 0; $cntNewTuition = 0;
        if ($get_statistics->num_rows > 0) {
            while ($statistic = $get_statistics->fetch_assoc()) {
                if ($statistic['view_type'] == 'page') {
                    $cntPage = $cntPage + $statistic['views_count_in_date'];
                } elseif ($statistic['view_type'] == 'post') {
                    if ($statistic['is_add_new']) {
                        $cntNewPost++;
                    }
                    if ($statistic['views_count_in_date'] > 0) {
                        $cntPost = $cntPost + $statistic['views_count_in_date'];
                    }
                } elseif ($statistic['view_type'] == 'event') {
                    if ($statistic['is_add_new']) {
                        $cntNewEvent++;
                    }
                    if ($statistic['views_count_in_date'] > 0) {
                        $cntEvent = $cntEvent + $statistic['views_count_in_date'];
                    }
                } elseif ($statistic['view_type'] == 'schedule') {
                    if ($statistic['is_add_new']) {
                        $cntNewSchedule++;
                    }
                    if ($statistic['views_count_in_date'] > 0) {
                        $cntSchedule = $cntSchedule + $statistic['views_count_in_date'];
                    }
                } elseif ($statistic['view_type'] == 'menu') {
                    if ($statistic['is_add_new']) {
                        $cntNewMenu++;
                    }
                    if ($statistic['views_count_in_date'] > 0) {
                        $cntMenu = $cntMenu + $statistic['views_count_in_date'];
                    }
                } elseif ($statistic['view_type'] == 'report') {
                    if ($statistic['is_add_new']) {
                        $cntNewReport++;
                    }
                    if ($statistic['views_count_in_date'] > 0) {
                        $cntReport = $cntReport + $statistic['views_count_in_date'];
                    }
                } elseif ($statistic['view_type'] == 'tuition') {
                    if ($statistic['is_add_new']) {
                        $cntNewTuition++;
                    }
                    if ($statistic['views_count_in_date'] > 0) {
                        $cntTuition = $cntTuition + $statistic['views_count_in_date'];
                    }
                }

            }
        }


        $strSql = sprintf('INSERT INTO ci_views_count (school_id, view_type, created_in_thirty_days, views_in_thirty_days) VALUES (%1$s, %2$s, %3$s, %4$s) 
                  ON DUPLICATE KEY UPDATE created_in_thirty_days = %3$s, views_in_thirty_days = %4$s',
            secure($school_id, 'int'), secure('page'), secure(0, 'int'), secure($cntPage, 'int'));
        $db->query($strSql) or secure('error', $strSql);

        $strSql = sprintf('INSERT INTO ci_views_count (school_id, view_type, created_in_thirty_days, views_in_thirty_days) VALUES (%1$s, %2$s, %3$s, %4$s) 
                  ON DUPLICATE KEY UPDATE created_in_thirty_days = %3$s, views_in_thirty_days = %4$s',
            secure($school_id, 'int'), secure('post'), secure($cntNewPost, 'int'), secure($cntPost, 'int'));
        $db->query($strSql) or secure('error', $strSql);

        $strSql = sprintf('INSERT INTO ci_views_count (school_id, view_type, created_in_thirty_days, views_in_thirty_days) VALUES (%1$s, %2$s, %3$s, %4$s) 
                  ON DUPLICATE KEY UPDATE created_in_thirty_days = %3$s, views_in_thirty_days = %4$s',
            secure($school_id, 'int'), secure('event'), secure($cntNewEvent, 'int'), secure($cntEvent, 'int'));
        $db->query($strSql) or secure('error', $strSql);

        $strSql = sprintf('INSERT INTO ci_views_count (school_id, view_type, created_in_thirty_days, views_in_thirty_days) VALUES (%1$s, %2$s, %3$s, %4$s) 
                  ON DUPLICATE KEY UPDATE created_in_thirty_days = %3$s, views_in_thirty_days = %4$s',
            secure($school_id, 'int'), secure('schedule'), secure($cntNewSchedule, 'int'), secure($cntSchedule, 'int'));
        $db->query($strSql) or secure('error', $strSql);

        $strSql = sprintf('INSERT INTO ci_views_count (school_id, view_type, created_in_thirty_days, views_in_thirty_days) VALUES (%1$s, %2$s, %3$s, %4$s) 
                  ON DUPLICATE KEY UPDATE created_in_thirty_days = %3$s, views_in_thirty_days = %4$s',
            secure($school_id, 'int'), secure('menu'), secure($cntNewMenu, 'int'), secure($cntMenu, 'int'));
        $db->query($strSql) or secure('error', $strSql);

        $strSql = sprintf('INSERT INTO ci_views_count (school_id, view_type, created_in_thirty_days, views_in_thirty_days) VALUES (%1$s, %2$s, %3$s, %4$s) 
                  ON DUPLICATE KEY UPDATE created_in_thirty_days = %3$s, views_in_thirty_days = %4$s',
            secure($school_id, 'int'), secure('report'), secure($cntNewReport, 'int'), secure($cntReport, 'int'));
        $db->query($strSql) or secure('error', $strSql);

        $strSql = sprintf('INSERT INTO ci_views_count (school_id, view_type, created_in_thirty_days, views_in_thirty_days) VALUES (%1$s, %2$s, %3$s, %4$s) 
                  ON DUPLICATE KEY UPDATE created_in_thirty_days = %3$s, views_in_thirty_days = %4$s',
            secure($school_id, 'int'), secure('tuition'), secure($cntNewTuition, 'int'), secure($cntTuition, 'int'));
        $db->query($strSql) or secure('error', $strSql);
    }

    /**
     * Cập nhật số lượt xem trang trường trong 30 ngày gần nhất
     *
     */
    public function updateViewsCountOfPageInLast30Days($school_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_views_count_detail WHERE school_id = %s AND view_type = 'page' AND date >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)", secure($school_id, 'int'));
        $get_statistics = $db->query($strSql) or secure('error', $strSql);

        //1.Lấy ra số lượt xem
        $cntPage = 0;
        if ($get_statistics->num_rows > 0) {
            while ($statistic = $get_statistics->fetch_assoc()) {
                $cntPage = $cntPage + $statistic['views_count_in_date'];
            }
        }

        //2.Cập nhật
        $strSql = sprintf("UPDATE pages SET views_in_thirty_days = %s WHERE page_id = %s", secure($cntPage, 'int'), secure($school_id, 'int'));
        $db->query($strSql) or secure('error', $strSql);
    }

    /**
     * Cập nhật số lượt xem bài viết trong trường
     *
     */
    public function updateViewsCountOfPost($school_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_views_count_detail WHERE school_id = %s AND view_type = 'post'", secure($school_id, 'int'));
        $get_views = $db->query($strSql) or secure('error', $strSql);

        //1.Lấy ra số lượt xem
        $data = array();
        if ($get_views->num_rows > 0) {
            while ($views = $get_views->fetch_assoc()) {
                $data[$views['object_id']][] = $views['views_count_in_date'];
            }
        }

        //2.Cập nhật
        foreach ($data as $postId => $value) {
            if (is_array($value)) {
                $strSql = sprintf("UPDATE posts SET views_count = %s WHERE post_id = %s", secure(array_sum($value), 'int'), secure($postId, 'int'));
                $db->query($strSql) or secure('error', $strSql);
            }
        }
    }

    /**
     * Cập nhật số lượt xem sự kiện trong trường
     *
     */
    public function updateViewsCountOfEvent($school_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_views_count_detail WHERE school_id = %s AND view_type = 'event'", secure($school_id, 'int'));
        $get_views = $db->query($strSql) or secure('error', $strSql);

        //1.Lấy ra số lượt xem
        $data = array();
        if ($get_views->num_rows > 0) {
            while ($views = $get_views->fetch_assoc()) {
                $data[$views['object_id']][] = $views['views_count_in_date'];
            }
        }

        //2.Cập nhật
        foreach ($data as $eventId => $value) {
            if (is_array($value)) {
                $strSql = sprintf("UPDATE ci_event SET views_count = %s WHERE event_id = %s", secure(array_sum($value), 'int'), secure($eventId, 'int'));
                $db->query($strSql) or secure('error', $strSql);
            }
        }
    }

    /**
     * Cập nhật số lượt xem lịch học trong trường
     *
     */
    public function updateViewsCountOfSchedule($school_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_views_count_detail WHERE school_id = %s AND view_type = 'schedule'", secure($school_id, 'int'));
        $get_views = $db->query($strSql) or secure('error', $strSql);

        //1.Lấy ra số lượt xem
        $data = array();
        if ($get_views->num_rows > 0) {
            while ($views = $get_views->fetch_assoc()) {
                $data[$views['object_id']][] = $views['views_count_in_date'];
            }
        }

        //2.Cập nhật
        foreach ($data as $scheduleId => $value) {
            if (is_array($value)) {
                $strSql = sprintf("UPDATE ci_schedule SET views_count = %s WHERE schedule_id = %s", secure(array_sum($value), 'int'), secure($scheduleId, 'int'));
                $db->query($strSql) or secure('error', $strSql);
            }
        }
    }

    /**
     * Cập nhật số lượt xem thực đơn trong trường
     *
     */
    public function updateViewsCountOfMenu($school_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_views_count_detail WHERE school_id = %s AND view_type = 'menu'", secure($school_id, 'int'));
        $get_views = $db->query($strSql) or secure('error', $strSql);

        //1.Lấy ra số lượt xem
        $data = array();
        if ($get_views->num_rows > 0) {
            while ($views = $get_views->fetch_assoc()) {
                $data[$views['object_id']][] = $views['views_count_in_date'];
            }
        }

        //2.Cập nhật
        foreach ($data as $menuId => $value) {
            if (is_array($value)) {
                $strSql = sprintf("UPDATE ci_menu SET views_count = %s WHERE menu_id = %s", secure(array_sum($value), 'int'), secure($menuId, 'int'));
                $db->query($strSql) or secure('error', $strSql);
            }
        }
    }


    /**
     * Cập nhật số lượt xem SLL trong trường
     *
     */
    public function updateViewsCountOfReport($school_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_views_count_detail WHERE school_id = %s AND view_type = 'report'", secure($school_id, 'int'));
        $get_views = $db->query($strSql) or secure('error', $strSql);

        //1.Lấy ra số lượt xem
        $data = array();
        if ($get_views->num_rows > 0) {
            while ($views = $get_views->fetch_assoc()) {
                $data[$views['object_id']][] = $views['views_count_in_date'];
            }
        }

        //2.Cập nhật
        foreach ($data as $reportId => $value) {
            if (is_array($value)) {
                $strSql = sprintf("UPDATE ci_report SET views_count = %s WHERE report_id = %s", secure(array_sum($value), 'int'), secure($reportId, 'int'));
                $db->query($strSql) or secure('error', $strSql);
            }
        }
    }

    /**
     * Cập nhật số lượt xem SLL trong trường
     *
     */
    public function updateViewsCountOfTuition($school_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_views_count_detail WHERE school_id = %s AND view_type = 'tuition'", secure($school_id, 'int'));
        $get_views = $db->query($strSql) or secure('error', $strSql);

        //1.Lấy ra số lượt xem
        $data = array();
        if ($get_views->num_rows > 0) {
            while ($views = $get_views->fetch_assoc()) {
                $data[$views['object_id']][] = $views['views_count_in_date'];
            }
        }

        //2.Cập nhật
        foreach ($data as $tuitionChildId => $value) {
            if (is_array($value)) {
                $strSql = sprintf("UPDATE ci_tuition_child SET views_count = %s WHERE tuition_child_id = %s", secure(array_sum($value), 'int'), secure($tuitionChildId, 'int'));
                $db->query($strSql) or secure('error', $strSql);
            }
        }
    }


    /**
     * Cập nhật số lượt xem
     *
     */
    public function insertViewsCount($args = array()) {
        global $db;

        foreach ($args['data'] as $object_id => $views_count_in_date) {
            if (!is_empty($args['view_type']) && !is_empty($args['date']) && is_numeric($args['school_id']) && is_numeric($views_count_in_date) && is_numeric($object_id)) {
                //$strValues .= "(" . secure($args['view_type']) . "," . secure($args['date']) . "," . secure($object_id, 'int') . "," . secure($args['school_id'], 'int') . "," . secure($views_count_in_date, 'int') . "),";

                $strSql = sprintf('INSERT INTO ci_views_count_detail (view_type, date, object_id, school_id, views_count_in_date) VALUES (%1$s, %2$s, %3$s, %4$s, %5$s) 
                  ON DUPLICATE KEY UPDATE views_count_in_date = %5$s',
                    secure($args['view_type']), secure($args['date']), secure($object_id, 'int'), secure($args['school_id'], 'int'), secure($views_count_in_date, 'int'));
                $db->query($strSql) or secure('error', $strSql);
            }
        }
    }

    /**
     * Cập nhật số lượt tạo mới
     *
     */
    public function insertCreated($args = array()) {
        global $db;

        $strValues = "";
        foreach ($args['data'] as $object_id) {
            if (!is_empty($args['view_type']) && !is_empty($args['date']) && is_numeric($args['school_id']) && is_numeric($object_id)) {
                $strValues .= "(" . secure($args['view_type']) . "," . secure($args['date']) . "," . secure($object_id, 'int') . "," . secure($args['school_id'], 'int') . ",'1'" . "),";
            }
        }
        $strValues = trim($strValues, ",");

        if (!is_empty($strValues)) {
            $strSql = "INSERT INTO ci_views_count_detail (view_type, date, object_id, school_id, is_add_new) VALUES " . $strValues;
            $db->query($strSql) or secure('error', $strSql);
        }
    }

    /* ---------- NOGA - Thống kê tương tác của trường ----------*/
    /**
     * Lấy ra thống kê tương tác toàn trường
     *
     * @return array
     */
    public function getSchoolStatisticsDetailForNoga($school_id) {
        global $db;

        $result = array(
            'dashboard' => [],
            'attendance' => [],
            'service' => [],
            'pickup' => [],
            'tuition' => [],
            'event' => [],
            'schedule' => [],
            'menu' => [],
            'report' => [],
            'medicine' => [],
            'feedback' => [],
            'birthday' => []
        );

        $strSql = sprintf("SELECT * FROM ci_interactive_school WHERE school_id = %s", secure($school_id, 'int'));
        $get_statistics = $db->query($strSql) or secure('error', $strSql);
        if($get_statistics->num_rows > 0) {
            while($statistic = $get_statistics->fetch_assoc()) {

                while ($statistic = $get_statistics->fetch_assoc()) {
                    if ($statistic['view_type'] == 'dashboard') {
                        $result['dashboard'][$statistic['date']] = $statistic['interactive_count_in_date'];
                    } elseif ($statistic['view_type'] == 'attendance') {
                        $result['attendance'][$statistic['date']] = $statistic['interactive_count_in_date'];
                    } elseif ($statistic['view_type'] == 'service') {
                        $result['service'][$statistic['date']] = $statistic['interactive_count_in_date'];
                    } elseif ($statistic['view_type'] == 'pickup') {
                        $result['pickup'][$statistic['date']] = $statistic['interactive_count_in_date'];
                    } elseif ($statistic['view_type'] == 'tuition') {
                        $result['tuition'][$statistic['date']] = $statistic['interactive_count_in_date'];
                    } elseif ($statistic['view_type'] == 'event') {
                        $result['event'][$statistic['date']] = $statistic['interactive_count_in_date'];
                    } elseif ($statistic['view_type'] == 'schedule') {
                        $result['schedule'][$statistic['date']] = $statistic['interactive_count_in_date'];
                    } elseif ($statistic['view_type'] == 'menu') {
                        $result['menu'][$statistic['date']] = $statistic['interactive_count_in_date'];
                    } elseif ($statistic['view_type'] == 'report') {
                        $result['report'][$statistic['date']] = $statistic['interactive_count_in_date'];
                    } elseif ($statistic['view_type'] == 'medicine') {
                        $result['medicine'][$statistic['date']] = $statistic['interactive_count_in_date'];
                    } elseif ($statistic['view_type'] == 'feedback') {
                        $result['feedback'][$statistic['date']] = $statistic['interactive_count_in_date'];
                    } elseif ($statistic['view_type'] == 'birthday') {
                        $result['birthday'][$statistic['date']] = $statistic['interactive_count_in_date'];
                    }

                }

            }
        }

        return $result;
    }


    /**
     * Lấy ra danh sách tất cả các trường trong hệ thống. Phục vụ cho chức năng quản lý của NOGA
     *
     * @param int $from
     * @param int $limit
     * @return array
     * @throws Exception
     */
    public function getAllSchoolStatisticsForNoga($from = 0, $limit = 0) {
        global $db;

        $toDate =  date('t/m/Y');
        $fromDate = date('01/m/Y');

        if ($from > 0) {
            $strSql = sprintf("SELECT P.page_id, P.page_name,  P.page_title, P.telephone, P.website, P.email, P.address FROM pages P
                      INNER JOIN ci_school_configuration SC ON SC.school_id = P.page_id AND SC.school_status = '1'
                      WHERE page_category = %s ORDER BY page_title ASC LIMIT %s, %s", SCHOOL_CATEGORY_ID, secure($from, 'int'), secure($limit, 'int'));
        } else {
            $strSql = sprintf("SELECT P.page_id, P.page_name,  P.page_title, P.telephone, P.website, P.email, P.address FROM pages P
                      INNER JOIN ci_school_configuration SC ON SC.school_id = P.page_id AND SC.school_status = '1'
                      WHERE page_category = %s ORDER BY page_title ASC", SCHOOL_CATEGORY_ID);
        }

        $schools = array();
        $get_schools = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_schools->num_rows > 0) {
            while($school = $get_schools->fetch_assoc()) {
                $total_interactive = 0;
                //$statistics = $this->getSchoolInteractive($school['page_id'], $fromDate, $toDate);
                $statistics = getStatisticsInteractive($school['page_id'], ALL, $fromDate, $toDate);
                foreach ($statistics as $statistic) {
                    $total_interactive = $total_interactive + array_sum($statistic);
                }
                $school['total_interactive'] = $total_interactive;
                $schools[] = $school;
            }
        }
        return $schools;
    }


    /**
     * Lấy ra danh sách tất cả các trường trong hệ thống. Phục vụ cho chức năng quản lý của NOGA
     *
     * @param int $from
     * @param int $limit
     * @return array
     * @throws Exception
     */
    public function getSchoolStatisticInThisMonth($school_id) {
        if (!is_numeric($school_id) || $school_id == 0) {
            _error(404);
        }

        $strToDate =  strtotime(date('Y-m-t'));
        $strFromDate = strtotime(date('Y-m-01'));
        $result = array();
        $statistics = getSchoolStatisticsForNoga($school_id);
        foreach ($statistics as $view => $statistic) {

            $cntInteractive = 0;
            foreach ($statistic as $date => $cnt) {
                $strDate = strtotime($date);
                if ($strDate >= $strFromDate && $strDate <= $strToDate) {
                    $cntInteractive = $cntInteractive + $cnt;
                }
            }
            $result[$view] = $cntInteractive;
        }

        return $result;
    }

    /**
     * Lấy ra tương tác của 1 trường theo thời gian
     *
     * @param int $from
     * @param int $limit
     * @return array
     * @throws Exception
     */
    public function getSchoolInteractive($school_id,  $fromDate, $toDate) {
        if (!is_numeric($school_id) || $school_id == 0) {
            _error(404);
        }
        $fromDbDate = toDBDate($fromDate);
        $toDbDate = toDBDate($toDate);
        $strFromDate = strtotime($fromDbDate);
        $strToDate = strtotime($toDbDate);

        $result = array(
            'dashboard' => 0,
            'attendance' => 0,
            'service' => 0,
            'pickup' => 0,
            'tuition' => 0,
            'event' => 0,
            'schedule' => 0,
            'menu' => 0,
            'report' => 0,
            'medicine' => 0,
            'feedback' => 0,
            'birthday' => 0
        );

        $statistics = getSchoolStatisticsForNoga($school_id);
        foreach ($statistics as $view => $statistic) {
                $cntInteractive = 0;
                foreach ($statistic as $date => $cnt) {
                    $strDate = strtotime($date);
                    if ($strDate >= $strFromDate && $strDate <= $strToDate) {
                        $cntInteractive = $cntInteractive + $cnt;
                    }
                }
                $result[$view] = $cntInteractive;
        }


        return $result;
    }

    /* ---------- END - Thống kê tương tác của trường ----------*/

    /* ---------- NOGA - Thống kê tương tác với các nhóm, chủ đề ----------*/

    /**
     * Lấy ra thống kê tương tác toàn trường
     *
     * @return array
     */
    public function getTopicStatisticsDetailForNoga() {
        global $db;

        $result = array(
            GROUP_NAME_CONIU => [],
            GROUP_NAME_ANDAM => [],
            GROUP_NAME_DINHDUONG => [],
            GROUP_NAME_DAYCON => [],
            GROUP_NAME_THAIGIAO => [],
            GROUP_NAME_HIEMMUON => [],
            GROUP_NAME_HONNHAN => [],
            GROUP_NAME_THUCPHAMORGANIC => []
        );

        $strSql = sprintf("SELECT * FROM ci_interactive_topic");
        $get_statistics = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_statistics->num_rows > 0) {
            while($statistic = $get_statistics->fetch_assoc()) {

                while ($statistic = $get_statistics->fetch_assoc()) {
                    if ($statistic['group_name'] == GROUP_NAME_CONIU) {
                        $result[GROUP_NAME_CONIU][$statistic['date']] = $statistic['interactive_count_in_date'];
                    } elseif ($statistic['group_name'] == GROUP_NAME_ANDAM) {
                        $result[GROUP_NAME_ANDAM][$statistic['date']] = $statistic['interactive_count_in_date'];
                    } elseif ($statistic['group_name'] == GROUP_NAME_DINHDUONG) {
                        $result[GROUP_NAME_DINHDUONG][$statistic['date']] = $statistic['interactive_count_in_date'];
                    } elseif ($statistic['group_name'] == GROUP_NAME_DAYCON) {
                        $result[GROUP_NAME_DAYCON][$statistic['date']] = $statistic['interactive_count_in_date'];
                    } elseif ($statistic['group_name'] == GROUP_NAME_THAIGIAO) {
                        $result[GROUP_NAME_THAIGIAO][$statistic['date']] = $statistic['interactive_count_in_date'];
                    } elseif ($statistic['group_name'] == GROUP_NAME_HIEMMUON) {
                        $result[GROUP_NAME_HIEMMUON][$statistic['date']] = $statistic['interactive_count_in_date'];
                    } elseif ($statistic['group_name'] == GROUP_NAME_HONNHAN) {
                        $result[GROUP_NAME_HONNHAN][$statistic['date']] = $statistic['interactive_count_in_date'];
                    } elseif ($statistic['group_name'] == GROUP_NAME_THUCPHAMORGANIC) {
                        $result[GROUP_NAME_THUCPHAMORGANIC][$statistic['date']] = $statistic['interactive_count_in_date'];
                    }

                }

            }
        }

        return $result;
    }

    /**
     * Lấy ra tương tác của 1 trường theo thời gian
     *
     * @param int $from
     * @param int $limit
     * @return array
     * @throws Exception
     */
    public function getTopicInteractive($fromDate, $toDate) {

        $strFromDate = strtotime(toDBDate($fromDate));
        $strToDate = strtotime(toDBDate($toDate));

        $result = array(
            GROUP_NAME_CONIU => 0,
            GROUP_NAME_ANDAM => 0,
            GROUP_NAME_DINHDUONG => 0,
            GROUP_NAME_DAYCON => 0,
            GROUP_NAME_THAIGIAO => 0,
            GROUP_NAME_HIEMMUON => 0,
            GROUP_NAME_HONNHAN => 0,
            GROUP_NAME_THUCPHAMORGANIC => 0
        );

        $statistics = getTopicStatisticsForNoga();
        foreach ($statistics as $view => $statistic) {
            $cntInteractive = 0;
            foreach ($statistic as $date => $cnt) {
                $strDate = strtotime($date);
                if ($strDate >= $strFromDate && $strDate <= $strToDate) {
                    $cntInteractive = $cntInteractive + $cnt;
                }
            }
            $result[$view] = $cntInteractive;
        }

        return $result;
    }

    /**
     * Cập nhật tương tác của người dùng với group, topic theo ngày
     *
     * @param int $from
     * @param int $limit
     * @throws Exception
     */
    public function updateInteractiveTopic($data, $curDate) {
        global $db;

        $strValues = "";
        foreach ($data as $group_name => $value) {
            $get_group = $db->query(sprintf("SELECT group_id FROM groups WHERE group_name = %s", secure($group_name))) or _error(SQL_ERROR_THROWEN);
            if ($get_group->num_rows == 0) continue;
            $group_id = $get_group->fetch_assoc()['group_id'];

            foreach ($value as $date => $interactiveInDate) {
                if ($date == $curDate) {
                    $strValues .= "(" . secure($group_name) . "," . secure($date) . "," . secure($group_id, 'int') . "," .secure($interactiveInDate, 'int') . "),";
                }
            }
        }
        $strValues = trim($strValues, ",");
        if (!is_empty($strValues)) {
            $strSql = "INSERT INTO ci_interactive_topic (group_name, date, group_id, interactive_count_in_date) VALUES " . $strValues;
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }

    /* ---------- END - Thống kê tương tác của trường ----------*/

    /* -------------- BEGIN -  Taila - thống kê tương tác làm lại --------*/

    /**
     * Lấy ra toàn bộ thống kê tương tác của phụ huynh toàn trường, lấy từ db để cập nhật vào memcache
     *
     * @return array
     */
    public function getStatisticInteractive($school_id) {
        global $db;

        $result = array(
            'page' => array(),
            'post' => array(),
            'school_dashboard' => array(),
            'attendance' => array(),
            'service' => array(),
            'pickup' => array(),
            'tuition' => array(),
            'event' => array(),
            'schedule' => array(),
            'menu' => array(),
            'report' => array(),
            'medicine' => array(),
            'feedback' => array(),
            'diary' => array(),
            'health' => array(),
            'birthday' => array(),
            'picker' => array()
        );
        // $result = array();

        $strSql = sprintf("SELECT * FROM ci_interactive WHERE school_id = %s", secure($school_id, 'int'));
        $get_statistics = $db->query($strSql) or secure('error', $strSql);
        if($get_statistics->num_rows > 0) {
            while($statistic = $get_statistics->fetch_assoc()) {
                if ($statistic['school_view'] > 0 || $statistic['parent_view'] > 0 || $statistic['is_created'] > 0) {
                    if($statistic['parent_view'] > 0) {
                        $result[$statistic['module']][$statistic['date']][$statistic['object_id']]['parent_view'] = $statistic['parent_view'];
                    }
                    if($statistic['school_view'] > 0) {
                        $result[$statistic['module']][$statistic['date']][$statistic['object_id']]['school_view'] = $statistic['school_view'];
                    }
                    if($statistic['is_created'] > 0) {
                        $result[$statistic['module']][$statistic['date']][$statistic['object_id']]['is_created'] = $statistic['is_created'];
                    }
                }

//                if ($statistic['module'] == 'page') {
//                    if ($statistic['school_view'] > 0 || $statistic['parent_view'] > 0) {
//                        $result['page'][$statistic['date']]['school_view'] = $statistic['school_view'];
//                        $result['page'][$statistic['date']]['parent_view'] = $statistic['parent_view'];
//                    }
//                } elseif ($statistic['module'] == 'post') {
//                    if ($statistic['school_view'] > 0 || $statistic['parent_view'] > 0 || $statistic['is_created'] > 0) {
//                        $result['post'][$statistic['date']][$statistic['object_id']]['is_created'] = $statistic['object_id'];
//                        $result['post'][$statistic['date']][$statistic['object_id']]['school_view'] = $statistic['school_view'];
//                        $result['post'][$statistic['date']][$statistic['object_id']]['parent_view'] = $statistic['parent_view'];
//                    }
//                } elseif ($statistic['module'] == 'school_dashboard') {
//                     if ($statistic['school_view'] > 0) {
//                         $result['school_dashboard'][$statistic['date']][$statistic['object_id']]['school_view'] = $statistic['school_view'];
//                     }
//                } elseif ($statistic['module'] == 'attendance') {
//                    if ($statistic['school_view'] > 0 || $statistic['parent_view'] > 0 || $statistic['is_created'] > 0) {
//                        $result['attendance'][$statistic['date']][$statistic['object_id']]['school_view'] = $statistic['school_view'];
//                        $result['attendance'][$statistic['date']][$statistic['object_id']]['parent_view'] = $statistic['parent_view'];
//                        $result['attendance'][$statistic['date']][$statistic['object_id']]['is_created'] = $statistic['is_created'];
//                    }
//                } elseif ($statistic['module'] == 'service') {
//                    if ($statistic['school_view'] > 0 || $statistic['parent_view'] > 0 || $statistic['is_created'] > 0) {
//                        $result['service'][$statistic['date']][$statistic['object_id']]['school_view'] = $statistic['school_view'];
//                        $result['service'][$statistic['date']][$statistic['object_id']]['parent_view'] = $statistic['parent_view'];
//                        $result['service'][$statistic['date']][$statistic['object_id']]['is_created'] = $statistic['is_created'];
//                    }
//                } elseif ($statistic['module'] == 'pickup') {
//                    if ($statistic['school_view'] > 0 || $statistic['parent_view'] > 0 || $statistic['is_created'] > 0) {
//                        $result['pickup'][$statistic['date']][$statistic['object_id']]['school_view'] = $statistic['school_view'];
//                        $result['pickup'][$statistic['date']][$statistic['object_id']]['parent_view'] = $statistic['parent_view'];
//                        $result['pickup'][$statistic['date']][$statistic['object_id']]['is_created'] = $statistic['is_created'];
//                    }
//                } elseif ($statistic['module'] == 'tuition') {
//                    if ($statistic['school_view'] > 0 || $statistic['parent_view'] > 0 || $statistic['is_created'] > 0) {
//                        $result['tuition'][$statistic['date']][$statistic['object_id']]['school_view'] = $statistic['school_view'];
//                        $result['tuition'][$statistic['date']][$statistic['object_id']]['parent_view'] = $statistic['parent_view'];
//                        $result['tuition'][$statistic['date']][$statistic['object_id']]['is_created'] = $statistic['is_created'];
//                    }
//                }
//                elseif ($statistic['module'] == 'event') {
//                    if ($statistic['school_view'] > 0 || $statistic['parent_view'] > 0 || $statistic['is_created'] > 0) {
//                        $result['event'][$statistic['date']][$statistic['object_id']]['school_view'] = $statistic['school_view'];
//                        $result['event'][$statistic['date']][$statistic['object_id']]['parent_view'] = $statistic['parent_view'];
//                        $result['event'][$statistic['date']][$statistic['object_id']]['is_created'] = $statistic['is_created'];
//                    }
//                }elseif ($statistic['module'] == 'schedule') {
//                    if ($statistic['school_view'] > 0 || $statistic['parent_view'] > 0 || $statistic['is_created'] > 0) {
//                        $result['schedule'][$statistic['date']][$statistic['object_id']]['school_view'] = $statistic['school_view'];
//                        $result['schedule'][$statistic['date']][$statistic['object_id']]['parent_view'] = $statistic['parent_view'];
//                        $result['schedule'][$statistic['date']][$statistic['object_id']]['is_created'] = $statistic['is_created'];
//                    }
//                } elseif ($statistic['module'] == 'menu') {
//                    if ($statistic['school_view'] > 0 || $statistic['parent_view'] > 0 || $statistic['is_created'] > 0) {
//                        $result['menu'][$statistic['date']][$statistic['object_id']]['school_view'] = $statistic['school_view'];
//                        $result['menu'][$statistic['date']][$statistic['object_id']]['parent_view'] = $statistic['parent_view'];
//                        $result['menu'][$statistic['date']][$statistic['object_id']]['is_created'] = $statistic['is_created'];
//                    }
//                } elseif ($statistic['module'] == 'report') {
//                    if ($statistic['school_view'] > 0 || $statistic['parent_view'] > 0 || $statistic['is_created'] > 0) {
//                        $result['report'][$statistic['date']][$statistic['object_id']]['school_view'] = $statistic['school_view'];
//                        $result['report'][$statistic['date']][$statistic['object_id']]['parent_view'] = $statistic['parent_view'];
//                        $result['report'][$statistic['date']][$statistic['object_id']]['is_created'] = $statistic['is_created'];
//                    }
//                } elseif ($statistic['module'] == 'medicine') {
//                    if ($statistic['school_view'] > 0 || $statistic['parent_view'] > 0 || $statistic['is_created'] > 0) {
//                        $result['medicine'][$statistic['date']][$statistic['object_id']]['school_view'] = $statistic['school_view'];
//                        $result['medicine'][$statistic['date']][$statistic['object_id']]['parent_view'] = $statistic['parent_view'];
//                        $result['medicine'][$statistic['date']][$statistic['object_id']]['is_created'] = $statistic['is_created'];
//                    }
//                } elseif ($statistic['module'] == 'feedback') {
//                    if ($statistic['school_view'] > 0 || $statistic['parent_view'] > 0 || $statistic['is_created'] > 0) {
//                        $result['schedule'][$statistic['date']][$statistic['object_id']]['school_view'] = $statistic['school_view'];
//                        $result['schedule'][$statistic['date']][$statistic['object_id']]['parent_view'] = $statistic['parent_view'];
//                        $result['schedule'][$statistic['date']][$statistic['object_id']]['is_created'] = $statistic['is_created'];
//                    }
//                } elseif ($statistic['module'] == 'diary') {
//                    if ($statistic['school_view'] > 0 || $statistic['parent_view'] > 0 || $statistic['is_created'] > 0) {
//                        $result['diary'][$statistic['date']][$statistic['object_id']]['school_view'] = $statistic['school_view'];
//                        $result['diary'][$statistic['date']][$statistic['object_id']]['parent_view'] = $statistic['parent_view'];
//                        $result['diary'][$statistic['date']][$statistic['object_id']]['is_created'] = $statistic['is_created'];
//                    }
//                } elseif ($statistic['module'] == 'health') {
//                    if ($statistic['school_view'] > 0 || $statistic['parent_view'] > 0 || $statistic['is_created'] > 0) {
//                        $result['health'][$statistic['date']][$statistic['object_id']]['school_view'] = $statistic['school_view'];
//                        $result['health'][$statistic['date']][$statistic['object_id']]['parent_view'] = $statistic['parent_view'];
//                        $result['health'][$statistic['date']][$statistic['object_id']]['is_created'] = $statistic['is_created'];
//                    }
//                } elseif ($statistic['module'] == 'birthday') {
//                    if ($statistic['school_view'] > 0) {
//                        $result['birthday'][$statistic['date']][$statistic['object_id']]['school_view'] = $statistic['school_view'];
//                    }
//                } elseif ($statistic['module'] == 'picker') {
//                    if ($statistic['school_view'] > 0 || $statistic['parent_view'] > 0) {
//                        $result['picker'][$statistic['date']][$statistic['object_id']]['school_view'] = $statistic['school_view'];
//                        $result['schedule'][$statistic['date']][$statistic['object_id']]['parent_view'] = $statistic['parent_view'];
//                    }
//                }
            }
        }

        return $result;
    }

    /**
     * Lấy ra thống kê tương tác của trường theo thời gian truyền vào (Lấy trong DB)
     *
     * @return array
     */
    public function getInteractiveByDate($school_id, $module = ALL, $fromDate = null, $toDate = null) {
        global $db, $statistic_keys;

        if(!is_null($fromDate)) {
            $fromDate = toDBDate($fromDate);
        } else {
            $fromDate = date('Y-m-01');
        }
        if(!is_null($toDate)) {
            $toDate = toDBDate($toDate);
        } else {
            $toDate = date('Y-m-d');
        }
        $result = array();
        if($module == ALL) {
            $strSql = sprintf("SELECT * FROM ci_interactive WHERE school_id = %s AND date <= %s AND date >= %s", secure($school_id, 'int'), secure($toDate), secure($fromDate));
            $get_statistics = $db->query($strSql) or secure('error', $strSql);
            if($get_statistics->num_rows > 0) {
                foreach ($statistic_keys as $key) {
                    if($key == 'post') {
                        $cntSchoolCreated = 0;
                        $cntParentCreated = 0;
                        $cntSchoolView = 0;
                        while($statistic = $get_statistics->fetch_assoc()) {
                            if($statistic['is_created'] == 1) {
                                $cntSchoolCreated = $cntSchoolCreated + 1;
                            } else if($statistic['is_created'] == 2) {
                                $cntParentCreated = $cntParentCreated + 1;
                            }
                            $cntSchoolView = $cntSchoolView + $statistic['school_view'];
                        }
                        $result[$key]['school_createds'] = $cntSchoolCreated;
                        $result[$key]['parent_createds'] = $cntParentCreated;
                    } else {
                        $cntSchoolCreated = 0;
                        $cntParentCreated = 0;
                        while($statistic = $get_statistics->fetch_assoc()) {
                            if ($statistic['module'] == $key) {
                                if($statistic['object_id'] == 0) {
                                    $result[$key]['school_views'] = $statistic['school_view'];
                                    $result[$key]['parent_views'] = $statistic['parent_view'];
                                }
                                if($statistic['is_created'] == 1) {
                                    $cntSchoolCreated = $cntSchoolCreated + 1;
                                } else if($statistic['is_created'] == 2) {
                                    $cntParentCreated = $cntParentCreated + 1;
                                }
                            }
                        }
                        $result[$key]['school_createds'] = $cntSchoolCreated;
                        $result[$key]['parent_createds'] = $cntParentCreated;
                    }
                }
            }
        } else {
            $strSql = sprintf("SELECT * FROM ci_interactive WHERE school_id = %s AND date <= %s AND date >= %s AND module = %s", secure($school_id, 'int'), secure($toDate), secure($fromDate), secure($module));
            $get_statistics = $db->query($strSql) or secure('error', $strSql);
            if($get_statistics->num_rows > 0) {
                if($module == 'post') {
                    $cntSchoolCreated = 0;
                    $cntParentCreated = 0;
                    $cntSchoolView = 0;
                    while($statistic = $get_statistics->fetch_assoc()) {
                        if($statistic['is_created'] == 1) {
                            $cntSchoolCreated = $cntSchoolCreated + 1;
                        } else if($statistic['is_created'] == 2) {
                            $cntParentCreated = $cntParentCreated + 1;
                        }
                        $cntSchoolView = $cntSchoolView + $statistic['school_view'];
                    }
                    $result[$module]['school_createds'] = $cntSchoolCreated;
                    $result[$module]['parent_createds'] = $cntParentCreated;
                    $result[$module]['school_views'] = $cntSchoolView;
                } else {
                    $cntSchoolCreated = 0;
                    $cntParentCreated = 0;
                    while($statistic = $get_statistics->fetch_assoc()) {
                        if($statistic['object_id'] == 0) {
                            $result[$module]['school_views'] = $statistic['school_view'];
                            $result[$module]['parent_views'] = $statistic['parent_view'];
                        }
                        if($statistic['is_created'] == 1) {
                            $cntSchoolCreated = $cntSchoolCreated + 1;
                        } else if($statistic['is_created'] == 2) {
                            $cntParentCreated = $cntParentCreated + 1;
                        }
                    }
                    $result[$module]['school_createds'] = $cntSchoolCreated;
                    $result[$module]['parent_createds'] = $cntParentCreated;
                }
            }
        }

        return $result;
    }

    /**
     * Cập nhật thống kê tương tác từ memcache vào DB
     *
     * @param $args
     */
    public function insertInteractive($args) {
        global $db;

        // Kiểm tra ngày đó đã update chưa
        $strSql = sprintf("SELECT * FROM ci_interactive WHERE school_id = %s AND date = %s AND module = %s AND object_id = %s", secure($args['school_id'], 'int'), secure($args['date']), secure($args['module']), secure($args['object_id'], 'int'));

        $get_inter = $db->query($strSql) or secure('error', $strSql);
        if($get_inter->num_rows > 0) {
            $strSql = sprintf("UPDATE ci_interactive SET school_view = %s, parent_view = %s, is_created = %s WHERE school_id = %s AND module = %s AND date = %s AND object_id = %s", secure($args['school_view'], 'int'), secure($args['parent_view'], 'int'), secure($args['is_created'], 'int'), secure($args['school_id'], 'int'), secure($args['module']), secure($args['date']), secure($args['object_id'], 'int'));
        } else {
            $strSql = sprintf('INSERT INTO ci_interactive (school_id, date, object_id, module, school_view, parent_view, is_created) VALUES (%s, %s, %s, %s, %s, %s, %s)',
                secure($args['school_id'], 'int'), secure($args['date']), secure($args['object_id'], 'int'), secure($args['module']), secure($args['school_view'], 'int'), secure($args['parent_view'], 'int'), secure($args['is_created'], 'int'));
        }
        $db->query($strSql) or secure('error', $strSql);
    }

    /**
     * Cập nhật số lượng xem lịch học trong DB
     *
     * @param $schoolId
     */
    public function updateViewCountSchedule($schoolId) {
        global $db;

        // Lấy tất cả lượt xem lịch học được xem trong db
        $strSql = sprintf("SELECT * FROM ci_interactive WHERE school_id = %s AND module = %s AND object_id > 0 AND parent_view > 0", secure($schoolId, 'int'), secure('schedule'));

        $get_schedule = $db->query($strSql) or secure('error', $strSql);
        $data = array();
        if($get_schedule->num_rows > 0) {
            while ($views = $get_schedule->fetch_assoc()) {
                $data[$views['object_id']][] = $views['school_view'] + $views['parent_view'];
            }
        }

        //2.Cập nhật
        foreach ($data as $scheduleId => $value) {
            if (is_array($value)) {
                $strSql = sprintf("UPDATE ci_schedule SET views_count = %s WHERE schedule_id = %s", secure(array_sum($value), 'int'), secure($scheduleId, 'int'));
                $db->query($strSql) or secure('error', $strSql);
            }
        }
    }

    /**
     * Cập nhật số lượng xem thực đơn trong DB
     *
     * @param $schoolId
     */
    public function updateViewCountMenu($schoolId) {
        global $db;

        // Lấy tất cả lượt xem lịch học được xem trong db
        $strSql = sprintf("SELECT * FROM ci_interactive WHERE school_id = %s AND module = %s AND object_id > 0 AND parent_view > 0", secure($schoolId, 'int'), secure('menu'));

        $get_menu = $db->query($strSql) or secure('error', $strSql);
        $data = array();
        if($get_menu->num_rows > 0) {
            while ($views = $get_menu->fetch_assoc()) {
                $data[$views['object_id']][] = $views['school_view'] + $views['parent_view'];
            }
        }

        //2.Cập nhật
        foreach ($data as $menuId => $value) {
            if (is_array($value)) {
                $strSql = sprintf("UPDATE ci_menu SET views_count = %s WHERE menu_id = %s", secure(array_sum($value), 'int'), secure($menuId, 'int'));
                $db->query($strSql) or secure('error', $strSql);
            }
        }
    }

    /**
     * Cập nhật số lượng xem bài viết trong DB
     *
     * @param $schoolId
     */
    public function updateViewCountPost($schoolId) {
        global $db;

        // Lấy tất cả lượt xem bài viết được xem trong db
        $strSql = sprintf("SELECT * FROM ci_interactive WHERE school_id = %s AND module = %s AND object_id > 0 AND school_view > 0", secure($schoolId, 'int'), secure('post'));

        $get_post = $db->query($strSql) or secure('error', $strSql);
        $data = array();
        if($get_post->num_rows > 0) {
            while ($views = $get_post->fetch_assoc()) {
                $data[$views['object_id']][] = $views['school_view'];
            }
        }

        //2.Cập nhật
        foreach ($data as $postId => $value) {
            if (is_array($value)) {
                $strSql = sprintf("UPDATE posts SET views_count = %s WHERE post_id = %s", secure(array_sum($value), 'int'), secure($postId, 'int'));
                $db->query($strSql) or secure('error', $strSql);
            }
        }
    }

    /**
     * Cập nhật số lượt xem SLL trong trường
     *
     */
    public function updateViewCountReport($schoolId) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_interactive WHERE school_id = %s AND module = %s AND object_id > 0 AND parent_view > 0", secure($schoolId, 'int'), secure('report'));

        $get_views = $db->query($strSql) or secure('error', $strSql);
        //1.Lấy ra số lượt xem
        $data = array();
        if ($get_views->num_rows > 0) {
            while ($views = $get_views->fetch_assoc()) {
                $data[$views['object_id']][] = $views['school_view'] + $views['parent_view'];
            }
        }

        //2.Cập nhật
        foreach ($data as $reportId => $value) {
            if (is_array($value)) {
                $strSql = sprintf("UPDATE ci_report SET views_count = %s WHERE report_id = %s", secure(array_sum($value), 'int'), secure($reportId, 'int'));
                $db->query($strSql) or secure('error', $strSql);
            }
        }
    }

    /**
     * Cập nhật số lượt xem học phí trong trường
     *
     */
    public function updateViewCountTuition($schoolId) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_interactive WHERE school_id = %s AND module = %s AND object_id > 0 AND parent_view > 0", secure($schoolId, 'int'), secure('tuition'));

        $get_views = $db->query($strSql) or secure('error', $strSql);
        //1.Lấy ra số lượt xem
        $data = array();
        if ($get_views->num_rows > 0) {
            while ($views = $get_views->fetch_assoc()) {
                $data[$views['object_id']][] = $views['school_view'] + $views['parent_view'];
            }
        }

        //2.Cập nhật
        foreach ($data as $tuitionId => $value) {
            if (is_array($value)) {
                $strSql = sprintf("UPDATE ci_tuition SET views_count = %s WHERE tuition_id = %s", secure(array_sum($value), 'int'), secure($tuitionId, 'int'));
                $db->query($strSql) or secure('error', $strSql);
            }
        }
    }
    /**
     * Cập nhật số lượt xem học phí trong trường
     *
     */
    public function updateViewCountEvent($schoolId) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_interactive WHERE school_id = %s AND module = %s AND object_id > 0 AND (parent_view > 0 OR school_view > 0)", secure($schoolId, 'int'), secure('event'));

        $get_views = $db->query($strSql) or secure('error', $strSql);
        //1.Lấy ra số lượt xem
        $data = array();
        if ($get_views->num_rows > 0) {
            while ($views = $get_views->fetch_assoc()) {
                $data[$views['object_id']][] = $views['school_view'] + $views['parent_view'];
            }
        }

        //2.Cập nhật
        foreach ($data as $eventId => $value) {
            if (is_array($value)) {
                $strSql = sprintf("UPDATE ci_event SET views_count = %s WHERE event_id = %s", secure(array_sum($value), 'int'), secure($eventId, 'int'));
                $db->query($strSql) or secure('error', $strSql);
            }
        }
    }

    /**
     * Lấy danh sách user_online cập nhật vào memcache
     *
     * @return array
     */
    public function getUserOnline() {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_users_online");
        $get_users = $db->query($strSql) or secure('error', $strSql);
        $result = array();
        if($get_users->num_rows > 0) {
            while($user = $get_users->fetch_assoc()) {
                $result[$user['date']][] = $user['user_id'];
            }
        }

        return $result;
    }

    /**
     * Update user online vào db
     *
     * @param array $args
     * @throws Exception
     */
    public function updateUserOnlineForDB($args = array()) {
        global $db;

        $today = date("Y-m-d");
        // Lấy danh sách user đã online trong ngày
        $strSql = sprintf("SELECT user_id FROM ci_users_online WHERE date = %s", secure($today));

        $get_user = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $userIds = array();
        if($get_user->num_rows > 0) {
            while($user = $get_user->fetch_assoc()) {
                $userIds[] = $user['user_id'];
            }
        }

        $userNew = array_diff($args, $userIds);
        if(count($userNew) > 0) {

            $strValues = "";
            foreach ($userNew as $userId) {
                //date, userId
                $strValues .= "(" . secure($today) . "," . secure($userId, 'int') . "),";
            }
            $strValues = trim($strValues, ",");

            $strSql = "INSERT INTO ci_users_online (date, user_id) VALUES " . $strValues;
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }
}
?>

