<?php
/**
 * DAO -> Services
 * Thao tác với các bảng ci_menu
 *
 * @package ConIu
 * @author TaiLa
 */

class MenuDAO
{
    /**
     * __construct
     *
     */
    public function __construct()
    {
    }
    //////////////////////////////// MENU ////////////////////////

    /**
     * Lấy danh sách menu của trường
     * @param $school_id
     * @return array
     */
    public function getMenuOfSchool($school_id) {
        global $db;

        // Lấy danh sách lịch học của toàn trường
        $strSql = sprintf("SELECT * FROM ci_menu 
                      WHERE school_id = %s ORDER BY begin DESC", secure($school_id, 'int'));

        $get_menu = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $menus = array();

        if ($get_menu->num_rows > 0) {
            while ($menu = $get_menu->fetch_assoc()){
                $menu['begin'] = toSysDate($menu['begin']);
                $menu['end'] = toSysDate($menu['end']);
                $menus[] = $menu;
            }
        }
        return $menus;
    }

    /**
     * Lấy danh sách menu của trường API
     * @param $school_id
     * @return array
     */
    public function getMenuOfSchoolForAPI($school_id, $offset = 0) {
        global $db, $system;

        $offset *= $system['max_results'];

        // Lấy danh sách lịch học của toàn trường
        $strSql = sprintf("SELECT menu_id, menu_name, begin, applied_for, school_id, class_level_id, class_id, is_notified FROM ci_menu 
                      WHERE school_id = %s ORDER BY begin DESC LIMIT %s, %s", secure($school_id, 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false));

        $get_menu = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $menus = array();

        if ($get_menu->num_rows > 0) {
            while ($menu = $get_menu->fetch_assoc()){
                $menu['begin'] = toSysDate($menu['begin']);
                $menus[] = $menu;
            }
        }
        return $menus;
    }

    /**
     * Lấy danh sách lịch học theo school_id, class_id, class_level_id
     *
     * @param $school_id
     * @param $class_level_id
     * @param $class_id
     * @return array
     */
    public function getMenuOfSchoolById($school_id, $class_level_id, $class_id) {
        global $db;

        // Lấy danh sách lịch học của toàn trường
        $strSql = sprintf("SELECT * FROM ci_menu
                      WHERE school_id = %s AND is_notified = 1 AND((class_level_id = 0 AND class_id = %s) OR (class_id = 0 AND class_level_id = %s) OR (class_id = 0 AND class_level_id = 0)) ORDER BY begin DESC", secure($school_id, 'int'), secure($class_id, 'int'), secure($class_level_id, 'int'));

        $get_menu = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $menus = array();
        if ($get_menu->num_rows > 0) {

            while ($menu = $get_menu->fetch_assoc()){
                $menu['begin'] = toSysDate($menu['begin']);
                $menus[] = $menu;
            }
        }
        return $menus;
    }

    /**
     * Thêm mới MENU
     *
     * @param array $args
     * @return mixed
     */
    public function insertMenu(array $args = array()) {
        global $db, $date, $user;
        $this->_validateMenuInput($args);
        $begin = toDBDate($args['begin']);


        $end = toDBDate($args['end']);
        $strSql = sprintf("INSERT INTO ci_menu (menu_name, applied_for, school_id, class_level_id, class_id, begin, description, is_meal, is_saturday, is_notified, created_at, created_user_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", secure($args['menu_name']), secure($args['applied_for'], 'int'), secure($args['school_id'], 'int'), secure($args['class_level_id'], 'int'), secure($args['class_id'], 'int'), secure($begin), secure($args['description']), secure($args['is_meal'], 'int'), secure($args['is_saturday'], 'int'), secure($args['is_notified'], 'int'), secure($date), secure($user->_data['user_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        return $db->insert_id;

    }

    /*
     * validate insert lịch học add
     */
    private function _validateMenuInput($args) {
        if(!isset($args['menu_name'])) {
            throw new Exception(__("You must enter menu name"));
        }
        if (strlen($args['menu_name']) < 10 && strlen($args['menu_name']) > 100) {
            throw new Exception(__("Menu name must be greater than 10 and less than 100 characters"));
        }

        if (isset($args['description']) && (strlen($args['description'])) > 300) {
            throw new Exception(__("Description length must be less than 300 characters long"));
        }
        $begin = toDBDate($args['begin']);
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $dateBegin = strtotime($begin);
        $weekday =  strtolower(date('D', $dateBegin));
        if(!($weekday === "mon")) {
            throw new Exception(__("Start date must be Monday"));
        }
        global $db;
        $begin = toDBDate($args['begin']);
        $strSql = sprintf("SELECT * FROM ci_menu WHERE school_id = %s AND class_level_id = %s AND class_id = %s AND begin = %s", secure($args['school_id'], 'int'), secure($args['class_level_id'], 'int'), secure($args['class_id'], 'int'), secure($begin));

        $getMenu = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($getMenu->num_rows > 0) {
            throw new Exception(__("There are menu for this week, please check again!"));
        }
    }

    /**
     * validate
     *
     * @param $args
     * @param $data
     * @throws Exception
     */
    private function _validateMenuInputEdit($args, $data) {
        if(!isset($args['menu_name'])) {
            throw new Exception(__("You must enter menu name"));
        }
        if (strlen($args['menu_name']) < 10 && strlen($args['menu_name']) > 100) {
            throw new Exception(__("Menu name must be greater than 10 and less than 100 characters"));
        }

        if (isset($args['description']) && (strlen($args['description'])) > 300) {
            throw new Exception(__("Description length must be less than 300 characters long"));
        }
        $begin = toDBDate($args['begin']);
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $dateBegin = strtotime($begin);
        $weekday =  strtolower(date('D', $dateBegin));
        if(!($weekday === "mon")) {
            throw new Exception(__("Start date must be Monday"));
        }
        if(!(($data['school_id'] == $args['school_id']) && ($data['class_level_id'] == $args['class_level_id']) && ($data['class_id'] == $args['class_id']) && ($args['begin'] == $data['begin']))) {
            global $db;
            $begin = toDBDate($args['begin']);
            $strSql = sprintf("SELECT * FROM ci_menu WHERE school_id = %s AND class_level_id = %s AND class_id = %s AND begin = %s", secure($args['school_id'], 'int'), secure($args['class_level_id'], 'int'), secure($args['class_id'], 'int'), secure($begin));

            $getMenu = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            if($getMenu->num_rows > 0) {
                throw new Exception(__("There are menu for this week, please check again!"));
            }
        }
    }

    /**
     * Thêm chi tiết THỰC ĐƠN
     *
     * @param $menu_id
     * @param $starts
     * @param $meals
     * @param $meal_detail_mon
     * @param $meal_detail_tue
     * @param $meal_detail_wed
     * @param $meal_detail_thu
     * @param $meal_detail_fri
     * @param $meal_detail_sat
     * @throws Exception
     */
    public function insertMenuDetail ($menu_id, $starts, $meals, $meal_detail_mon, $meal_detail_tue, $meal_detail_wed,$meal_detail_thu, $meal_detail_fri, $meal_detail_sat) {
        global $db;
        if(count($starts) == 0 || ((count($meals) > 0)) && (count($starts) != count($meals))) {
            throw new Exception(__('Please enter the full information'));
        }
        $strValues = "";
        // subject
        for ($idx = 0; $idx < count($starts); $idx++) {
            $strValues .= ",(" . $menu_id . "," . "'" . $starts[$idx]. "'" . "," . "'" . $meals[$idx]. "'" . "," . "'" . $meal_detail_mon[$idx] . "'" . "," . "'" . $meal_detail_tue[$idx] . "'" . "," . "'" . $meal_detail_wed[$idx] . "'" . "," . "'" . $meal_detail_thu[$idx] . "'" . "," . "'" . $meal_detail_fri[$idx] . "'" . "," . "'" . $meal_detail_sat[$idx] . "'" . "),";
            $strValues = trim($strValues, ",");
            $strSql = "INSERT INTO ci_menu_detail (menu_id, meal_time, meal_name, monday, tuesday, wednesday, thursday, friday, saturday) VALUES " . $strValues;
        }
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /*
     * Insert vào bảng ci_menu_detail (Hàm này chưa dùng tới, lưu lại cho tiện sửa, phòng lỗi)! Hì!
     */
    public function insertMenuDetail2 ($menu_id, $starts, $meals, $meal_detail_mon, $meal_detail_tue, $meal_detail_wed,$meal_detail_thu, $meal_detail_fri, $meal_detail_sat) {
        global $db;
        if(count($starts) == 0 || ((count($meals) > 0)) && (count($starts) != count($meals))) {
            throw new Exception(__('Please enter the full information'));
        }
        $strValues = "";
        // subject
        for ($idx = 0; $idx < count($starts); $idx++) {
            if(empty($meals) && empty($meal_detail_sat)) {
                $strValues .= ",(" . $menu_id . "," . "'" . $starts[$idx]. "'" . "," . "'" . $meal_detail_mon[$idx] . "'" . "," . "'" . $meal_detail_tue[$idx] . "'" . "," . "'" . $meal_detail_wed[$idx] . "'" . "," . "'" . $meal_detail_thu[$idx] . "'" . "," . "'" . $meal_detail_fri[$idx] . "'" . "),";

                $strValues = trim($strValues, ",");
                $strSql = "INSERT INTO ci_menu_detail (menu_id, meal_time, monday, tuesday, wednesday, thursday, friday) VALUES " . $strValues;
            } elseif (!empty($meals) && empty($meal_detail_sat)) {
                $strValues .= ",(" . $menu_id . "," . "'" . $starts[$idx]. "'" . "," . "'" . $meals[$idx]. "'" . "," . "'" . $meal_detail_mon[$idx] . "'" . "," . "'" . $meal_detail_tue[$idx] . "'" . "," . "'" . $meal_detail_wed[$idx] . "'" . "," . "'" . $meal_detail_thu[$idx] . "'" . "," . "'" . $meal_detail_fri[$idx] . "'" . "),";
                $strValues = trim($strValues, ",");
                $strSql = "INSERT INTO ci_menu_detail (menu_id, meal_time, meal_name, monday, tuesday, wednesday, thursday, friday) VALUES " . $strValues;
            } elseif (empty($meals) && !empty($meal_detail_sat)) {
                $strValues .= ",(" . $menu_id . "," . "'" . $starts[$idx]. "'" . "," . "'" . $meal_detail_mon[$idx] . "'" . "," . "'" . $meal_detail_tue[$idx] . "'" . "," . "'" . $meal_detail_wed[$idx] . "'" . "," . "'" . $meal_detail_thu[$idx] . "'" . "," . "'" . $meal_detail_fri[$idx] . "'" . "," . "'" . $meal_detail_sat[$idx] . "'" . "),";
                $strValues = trim($strValues, ",");
                $strSql = "INSERT INTO ci_menu_detail (menu_id, meal_time, monday, tuesday, wednesday, thursday, friday, saturday) VALUES " . $strValues;
            } else {
                $strValues .= ",(" . $menu_id . "," . "'" . $starts[$idx]. "'" . "," . "'" . $meals[$idx]. "'" . "," . "'" . $meal_detail_mon[$idx] . "'" . "," . "'" . $meal_detail_tue[$idx] . "'" . "," . "'" . $meal_detail_wed[$idx] . "'" . "," . "'" . $meal_detail_thu[$idx] . "'" . "," . "'" . $meal_detail_fri[$idx] . "'" . "," . "'" . $meal_detail_sat[$idx] . "'" . "),";
                $strValues = trim($strValues, ",");
                $strSql = "INSERT INTO ci_menu_detail (menu_id, meal_time, meal_name, monday, tuesday, wednesday, thursday, friday, saturday) VALUES " . $strValues;
            }
        }
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy Thực đơn theo lựa chọn
     *
     * @param $school_id
     * @param $class_level_id
     * @param $class_id
     * @return array
     */
    public function getMenuById($school_id, $class_level_id, $class_id) {
        global $db;
        if(!isset($school_id) || $school_id == 0 || !isset($class_level_id) || !isset($class_id)) {
            _error(400);
        }
        $strSql = sprintf("SELECT * FROM ci_menu
            WHERE school_id = %s AND class_level_id = %s AND class_id = %s ORDER BY begin DESC", secure($school_id, 'int'), secure($class_level_id, 'int'), secure($class_id, 'int'));

        $get_menu = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $menus = array();

        if ($get_menu->num_rows > 0) {
            while($sche = $get_menu->fetch_assoc()) {
                $sche['begin'] = toSysDate($sche['begin']);
                $menus[] = $sche;
            }
        }
        return $menus;
    }

    /**
     * Lấy chi tiết Thực đơn theo ID
     *
     * @param $menuId
     * @return array
     */
    public function getMenuDetailById($menuId) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_menu 
                    WHERE menu_id = %s", secure($menuId, 'int'));
        $get_menu = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $menu = array();
        if($get_menu->num_rows > 0) {
            $sche = $get_menu->fetch_assoc();
            $sche['begin'] = toSysDate($sche['begin']);
            $details = $this->_getMenuDetail($sche['menu_id']);
            $sche['details'] = $details;
            $menu = $sche;
        }

        return $menu;
    }

    /**
     * Lấy chi tiết các món ăn của THỰC ĐƠN
     *
     * @param $menuId
     * @return array
     */
    private function _getMenuDetail($menuId) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_menu_detail
                      WHERE menu_id = %s", secure($menuId, 'int'));

        $get_menu_detail = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $menu_detail =  array();
        if($get_menu_detail->num_rows > 0) {
            while ($sub = $get_menu_detail->fetch_assoc()) {
                $sub['monday'] = trim($sub['monday'], ' ');
                $sub['tuesday'] = trim($sub['tuesday'], ' ');
                $sub['wednesday'] = trim($sub['wednesday'], ' ');
                $sub['thursday'] = trim($sub['thursday'], ' ');
                $sub['friday'] = trim($sub['friday'], ' ');
                $sub['saturday'] = trim($sub['saturday'], ' ');
                $menu_detail[] = $sub;
            }
        }

        return $menu_detail;
    }

    /**
     * UPDATE Thực đơn
     *
     * @param array $args
     * @param $data
     */
    public function updateMenu(array $args = array(), $data) {
        global $db, $date;
        $this->_validateMenuInputEdit($args, $data);

        $begin = toDBDate($args['begin']);
        $strSql = sprintf("UPDATE ci_menu SET menu_name = %s, begin = %s, description = %s, applied_for = %s, class_level_id = %s, class_id = %s, is_meal = %s, is_saturday = %s, is_notified = %s, updated_at = %s WHERE menu_id = %s", secure($args['menu_name']), secure($begin), secure($args['description']), secure($args['applied_for'], 'int'), secure($args['class_level_id'], 'int'), secure($args['class_id'], 'int'), secure($args['is_meal'], 'int'), secure($args['is_saturday'], 'int'), secure($args['is_notified'], 'int'), secure($date), secure($args['menu_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Delete menu
     *
     * @param $id
     */
    public function deleteMenu($id) {
        global $db;

        $strSql = sprintf("DELETE FROM ci_menu WHERE menu_id = %s", secure($id, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $strSql = sprintf("DELETE FROM ci_menu_detail WHERE menu_id = %s", secure($id, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Delete menu detail
     */
    public function deleteMenuDetail($menuId) {
        global $db;

        $strSql = sprintf("DELETE FROM ci_menu_detail WHERE menu_id = %s", secure($menuId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Update trạng thái đã gửi thông báo
     *
     * @param $menu_id
     */
    public function updateStatusToNotified($menu_id) {
        global $db, $date;

        $strSql = sprintf("UPDATE ci_menu SET is_notified = %s, updated_at = %s WHERE menu_id = %s", 1, secure($date), secure($menu_id, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Thêm chi tiết thực đơn bằng excel
     *
     * @param $args
     * @return mixed
     */
    public function insertMenuDetailImport($args) {
        global $db;

        $strSql = sprintf("INSERT INTO ci_menu_detail (menu_id, meal_name, meal_time, monday, tuesday, wednesday, thursday, friday, saturday) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)", secure($args['menu_id'], 'int'), secure($args['meal']), secure($args['start']), secure($args['mon']), secure($args['tue']), secure($args['wed']), secure($args['thu']), secure($args['fri']), secure($args['sat']));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        return $db->insert_id;
    }

    /**
     * Lấy danh sách lịch học theo level
     *
     * @param $schoolId
     * @param $level
     * @return array
     * @throws Exception
     */
    public function getAllMenuByLevel($schoolId, $level) {
        global $db;

        // Lấy danh sách lịch học theo level
        $strSql = sprintf("SELECT * FROM ci_menu 
                      WHERE school_id = %s AND applied_for = %s ORDER BY begin DESC", secure($schoolId, 'int'), secure($level, 'int'));

        $get_menu = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $menus = array();

        if ($get_menu->num_rows > 0) {
            while ($menu = $get_menu->fetch_assoc()){
                $menu['begin'] = toSysDate($menu['begin']);
                $menu['end'] = toSysDate($menu['end']);
                $menus[] = $menu;
            }
        }
        return $menus;
    }

    /**
     * Lấy thực đơn trong tuần của lớp
     *
     * @param $school_id
     * @param $class_level_id
     * @param $class_id
     * @param $monDate
     * @return array
     * @throws Exception
     */
    public function getMenuOfSchoolByIdOnDate($school_id, $class_level_id, $class_id, $monDate) {
        global $db;

        $monDate = toDBDate($monDate);

        // Lấy danh sách lịch học của toàn trường
        $strSql = sprintf("SELECT * FROM ci_menu
                      WHERE school_id = %s AND begin = %s AND is_notified = 1 AND ((class_level_id = 0 AND class_id = %s) OR (class_id = 0 AND class_level_id = %s) OR (class_id = 0 AND class_level_id = 0)) ORDER BY begin DESC", secure($school_id, 'int'), secure($monDate), secure($class_id, 'int'), secure($class_level_id, 'int'));

        $get_menu = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $menus = array();
        if ($get_menu->num_rows > 0) {
            while ($menu = $get_menu->fetch_assoc()){
                $menu['begin'] = toSysDate($menu['begin']);
                $menus[] = $menu;
            }
        }
        return $menus;
    }

    /* ---------- MENU - API ---------- */

    /**
     * Lấy danh sách lịch học theo school_id, class_id, class_level_id
     *
     * @param $school_id
     * @param $class_level_id
     * @param $class_id
     * @return array
     */
    public function getMenuSchoolForApi($school_id, $class_level_id, $class_id, $offset = 0) {
        global $db, $system;

        $offset *= $system['max_results'];

        // Lấy danh sách lịch học của toàn trường
        $strSql = sprintf("SELECT menu_id, menu_name, applied_for, class_level_id, class_id, begin, status, description FROM ci_menu
                      WHERE school_id = %s AND is_notified = 1 AND((class_level_id = 0 AND class_id = %s) OR (class_id = 0 AND class_level_id = %s) OR (class_id = 0 AND class_level_id = 0)) ORDER BY begin DESC LIMIT %s, %s", secure($school_id, 'int'), secure($class_id, 'int'), secure($class_level_id, 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false));

        $get_menu = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $menus = array();
        if ($get_menu->num_rows > 0) {

            while ($menu = $get_menu->fetch_assoc()){
                $menu['begin'] = toSysDate($menu['begin']);
                $menus[] = $menu;
            }
        }
        return $menus;
    }

    /**
     * Lấy danh sách lịch học theo school_id, class_id, class_level_id
     *
     * @param $school_id
     * @param $class_level_id
     * @param $class_id
     * @return array
     */
    public function getMenuOfSchoolInWeekForApi($school_id, $class_level_id, $class_id, $begin) {
        global $db;

        // Lấy danh sách lịch học của toàn trường
        $strSql = sprintf("SELECT menu_id, applied_for, begin FROM ci_menu
                      WHERE school_id = %s AND begin = %s AND is_notified = 1 AND((class_level_id = 0 AND class_id = %s) OR (class_id = 0 AND class_level_id = %s) OR (class_id = 0 AND class_level_id = 0)) ORDER BY begin DESC", secure($school_id, 'int'), secure($begin), secure($class_id, 'int'), secure($class_level_id, 'int'));

        $get_menu = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $menus = array();
        if ($get_menu->num_rows > 0) {

            while ($menu = $get_menu->fetch_assoc()){
                $menu['begin'] = toSysDate($menu['begin']);
                $menus[] = $menu;
            }
        }
        return $menus;
    }

    /**
     * Lấy chi tiết Thực đơn theo ID
     *
     * @param $menuId
     * @return array
     */
    public function getMenuDetailForApi($menuId) {
        global $db;

        $strSql = sprintf("SELECT menu_id, menu_name, applied_for, begin, status, is_meal, is_saturday, is_notified, description FROM ci_menu 
                    WHERE menu_id = %s", secure($menuId, 'int'));
        $get_menu = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $menu = null;
        if($get_menu->num_rows > 0) {
            $menu = $get_menu->fetch_assoc();
            $menu['begin'] = toSysDate($menu['begin']);
            $details = $this->_getMenuDetailForApi($menu['menu_id']);
            $menu['details'] = $details;
        }

        return $menu;
    }


    /**
     * Lấy chi tiết các món ăn của THỰC ĐƠN
     *
     * @param $menuId
     * @return array
     */
    private function _getMenuDetailForApi($menuId) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_menu_detail
                      WHERE menu_id = %s", secure($menuId, 'int'));

        $get_menu_detail = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $menu_detail =  array();
        if($get_menu_detail->num_rows > 0) {
            while ($sub = $get_menu_detail->fetch_assoc()) {
                $sub['monday'] = trim($sub['monday'], ' ');
                $sub['tuesday'] = trim($sub['tuesday'], ' ');
                $sub['wednesday'] = trim($sub['wednesday'], ' ');
                $sub['thursday'] = trim($sub['thursday'], ' ');
                $sub['friday'] = trim($sub['friday'], ' ');
                $sub['saturday'] = trim($sub['saturday'], ' ');
                $menu_detail[] = $sub;
            }
        }

        return $menu_detail;
    }

    /* ---------- END - API ---------- */

}
?>