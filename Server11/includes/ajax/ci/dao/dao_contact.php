<?php
/**
 * Created by PhpStorm.
 * DAO -> CONTACT
 * User: Taila
 * Date: 12/19/2016
 * Time: 9:56 PM
 */
class ContactDAO {
    public function __construct() {

    }
    /**
     * Lưu thông tin Liên hệ vào database
     */
    public function saveContact(array $args = array()) {
        global $db;
        $this->validateInput($args);

        $strSql = sprintf("INSERT INTO contacts (name_contact, email_contact, phone_contact, content_contact, ip_contact) VALUES (%s, %s, %s, %s, %s)",
                        secure($args['name']), secure($args['email']), secure($args['phone']),
                        secure($args['content']), secure($args['contact_ip']));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    public function validateInput(array $args = array()) {
        if (is_empty($args['name'])) {
            throw new Exception("You must enter name");
        }
        if(!valid_email($args['email'])) {
            throw new Exception(__("Please enter a valid email address"));
        }
        /* if (is_empty($args['phone'])) {
            throw new Exception("You must enter phone");
        } */
        if (is_empty($args['content'])) {
            throw new Exception("You must enter content");
        }
    }
}

?>