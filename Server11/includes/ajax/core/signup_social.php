<?php
/**
 * ajax -> users -> social signup
 * 
 * @package CINet v1.1
 * @author NOGA Co., Ltd
 */

// fetch bootstrap
require('../../../bootstrap.php');

// check AJAX Request
is_ajax();

// check user logged in
if($user->_logged_in) {
    return_json( array('callback' => 'window.location.reload();') );
}

// check if registration is open
if(!$system['registration_enabled']) {
	return_json( array('error' => true, 'message' => __('Registration is closed right now')) );
}

// signup
try {
    $db->autocommit(false);
    $db->begin_transaction();
    $user_id = $user->socail_register($_POST['full_name'], $_POST['first_name'], $_POST['last_name'], $_POST['username'], $_POST['email'], $_POST['password'], $_POST['gender'], $_POST['avatar'], $_POST['provider']);

    include_once(DAO_PATH.'dao_user.php');
    $userDao = new UserDAO();
    /* CI - Cho user like các page và join các group của hệ thống */
    $userDao->addUsersLikeSomePages($system_page_ids, [$user_id]);
    $userDao->addUserToSomeGroups($system_group_ids, [$user_id]);

    $db->commit();
    return_json( array('callback' => 'window.location = site_path;') );
} catch (Exception $e) {
    $db->rollback();
	return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>