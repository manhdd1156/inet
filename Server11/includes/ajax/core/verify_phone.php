<?php

// set override_shutdown
$override_shutdown = true;

// fetch bootstrap
require('../../../bootstrap.php');

// check AJAX Request
is_ajax();

// check user logged in
if ($user->_logged_in && ($_POST['do'] == 'signin') ) {
    return_json(array('callback' => 'window.location.reload();'));
}

try {

    $ch = curl_init();
    // Set url elements
    $fb_app_id = '1638502962897148';
    $ak_secret = '15b19ddb50d26f41ee3e7aadb01d5e75';
    $token = 'AA|' . $fb_app_id . '|' . $ak_secret;

    // Get access token
    $url = 'https://graph.accountkit.com/v1.0/access_token?grant_type=authorization_code&code=' . $_POST["code"] . '&access_token=' . $token;
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    $result = curl_exec($ch);
    $info = json_decode($result);

    // Get account information
    $url = 'https://graph.accountkit.com/v1.0/me/?access_token=' . $info->access_token;
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    $result = curl_exec($ch);
    curl_close($ch);
    $final = json_decode($result);

    $db->autocommit(false);
    $db->begin_transaction();

    switch ($_POST['do']) {
        case 'signin' :
            if (!isset($final->phone->number) || $_POST['csrf'] != 'coniuahihi') {
                _error(404);
            }

            $phone = standardizePhone($final->phone->number);
            $_SESSION["phone"] = $phone;
            $_SESSION["csrf"] = $_POST["csrf"];

            $info = null;
            if (valid_phone($phone)) {
                $info = $user->check_phone($phone, true);
                if (is_numeric($info['user_id'])) {
                    /* set cookies */
                    $user->set_cookies_via_sms($info['user_id']);

                    $db->commit();
                    return_json(array('callback' => 'window.location = "' . $system['system_url'] . '";'));
                }
            } else {
                throw new Exception(__("Invalid mobile number"));
            }

            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/started_sms' . '";'));
            break;

        case 'edit_phone' :
            if (!isset($final->phone->number) || $_POST['csrf'] != 'coniuahihi') {
                _error(404);
            }

            include_once(DAO_PATH . 'dao_teacher.php');
            $teacherDao = new TeacherDAO();

            $phone = standardizePhone($final->phone->number);
            if (valid_phone($phone)) {
                $args['edit'] = 'phone';
                $args['phone'] = $phone;
                $user->settings($args);

                $db->commit();


                /* ---------- UPDATE - MEMCACHE ---------- */
                // Lấy thông tin trường
                $schoolIds = $teacherDao->getSchoolIdOfUserId($user->_data['user_id']);
                if (count($schoolIds) > 0) {
                    updateTeacherData($user->_data['user_id'], TEACHER_INFO);

                }
                /* ---------- END - MEMCACHE ---------- */
                return_json( array('success' => true, 'message' => __("Your phone has been updated")) );
            } else {
                throw new Exception(__("Invalid mobile number"));
            }
            break;

    }
} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>