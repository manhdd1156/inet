<?php
/**
 * ajax -> core -> signin
 * 
 * @package CINet v1.1
 * @author NOGA Co., Ltd
 */

// set override_shutdown
$override_shutdown = true;

// fetch bootstrap
require('../../../bootstrap.php');

// check AJAX Request
is_ajax();

// check user logged in
if($user->_logged_in) {
    return_json( array('callback' => 'window.location.reload();') );
}

// signin
try {
    $remember = (isset($_POST['remember'])) ? true : false;
    $user_id = $user->sign_in($_POST['username_email'], $_POST['password'], $remember);

    // Lấy danh sách đối tượng quản lý của user
    // ConIu - Lấy ra danh sách schools, classes, children mà user quản lý
    include_once('../../../includes/ajax/ci/dao/dao_region_manage.php');
    $regionDao = new RegionManageDAO();

    include_once('../../../includes/ajax/ci/dao/dao_school.php');
    $schoolDao = new SchoolDAO();

    // Lấy thông tin quản lý vùng của user
    $region_manages = $regionDao->getRegionManageByUserId($user_id);
    if(count($region_manages) > 0) {
        $_SESSION['isGovManager'] = true;

        // Lấy danh sách các trường user quản lý
        $schools = array();
        $schoolIds = array();
        // Lấy danh sách tỉnh và huyện user quản lý
        $manages = array();
        foreach ($region_manages as $region) {
            // Lấy danh sách trường thuộc region truyền vào
            $school_ids = $schoolDao->getSchoolIdsByRegion($region['city_id'], $region['district_slug']);

            $schoolIds = array_merge($schoolIds, $school_ids);
        }

        $schoolIds = array_unique($schoolIds);

        foreach ($schoolIds as $schoolId) {
            $school = getSchoolData($schoolId, SCHOOL_INFO);
            $schools[] = $school['page_name'];
        }

        $_SESSION['schools'] = $schools;
    } else {
        $_SESSION['isGovManager'] = false;
    }

    $objects = getRelatedObjectsOnUserId($user_id);
    // Lấy những trường đang sử dụng inet

    $schoolUsing = array();
    $classUsing = array();
    if(count($objects['schools']) > 0) {
        $schoolUsing = array();
        foreach ($objects['schools'] as $school) {
            if($school['school_status'] == SCHOOL_USING_CONIU && !$school['is_teacher']) {
                $schoolUsing[] = $school;
            }
        }

    }
    if(count($objects['classes']) > 0) {
        foreach ($objects['classes'] as $class) {
            if ($class['school_status'] == SCHOOL_USING_CONIU) {
                $classUsing[] = $class;
            }
        }
    }
    if(count($schoolUsing) > 0) {
        return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $schoolUsing[0]['page_name'] . '";'));
    }
    if(count($classUsing) > 0) {
        return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/class/' . $classUsing[0]['group_name'] . '";'));
    }
    //$children = $childDao->getChildrenOfParent($user->_data['user_id']);

    return_json( array('callback' => 'setTimeout(function(){ window.location.reload(); });') );
} catch (Exception $e) {
    return_json( array('error' => true, 'message' => $e->getMessage()) );
}

?>