<?php
/**
 * ajax -> data -> report
 *
 * @package Coniu
 * @author Coniu
 */

// fetch bootstrap
require('../../../bootstrap.php');

// check AJAX Request
is_ajax();

// user access
user_access(true);

// report
try {

    $session_user_chat = $_POST['session_user_chat'];
    switch ($_POST['func']) {
        case 'change_session':
            if($session_user_chat) {
                $_SESSION[SESSION_USER_CHAT][$user->_data['user_id']] = 0;
            } else {
                $_SESSION[SESSION_USER_CHAT][$user->_data['user_id']] = 1;
            }

            return_json( array('success' => true, 'message' => __("Success")));
            break;
        default:
            _error(404);
            break;

    }

} catch (Exception $e) {
    modal(ERROR, __("Error"), $e->getMessage());
}

?>