<?php
/**
 * ajax -> users -> signup
 * 
 * @package CINet v1.1
 * @author NOGA Co., Ltd
 */

// fetch bootstrap
require('../../../bootstrap.php');

// check AJAX Request
is_ajax();

// check user logged in
if($user->_logged_in) {
    return_json( array('callback' => 'window.location.reload();') );
}

// check if registration is open
if(!$system['registration_enabled'] && !$system['invitation_enabled']) {
    return_json( array('error' => true, 'message' => __('Registration is closed right now')) );
}
/* -- CI --*/
unset($_SESSION['menu_status']);
/* END - CI */

// signup
try {

    $db->autocommit(false);
    $db->begin_transaction();

    //$user_id = $user->sign_up($_POST['full_name'], $_POST['username'], $_POST['email'], $_POST['password'], $_POST['gender'], $_POST['g-recaptcha-response']);
    $user_id = $user->sign_up($_POST);

    include_once (DAO_PATH."dao_user.php");
    $userDao = new UserDAO();
    //Cho user like các page của hệ thống, join các group của hệ thống
    $userDao->addUsersLikeSomePages($system_page_ids, [$user_id]);
    $userDao->addUserToSomeGroups($system_group_ids, [$user_id]);

    $db->commit();
    return_json( array('callback' => 'window.location.reload();') );

} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}
?>