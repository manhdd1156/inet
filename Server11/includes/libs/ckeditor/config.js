/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
    config.language = 'vi';
    config.skin = 'office2013';
    config.enterMode = CKEDITOR.ENTER_BR;
    // config.uiColor = '#AADC6E';

    config.height = 350;
    config.toolbarCanCollapse = true;

    config.disableObjectResizing = true;
    config.removePlugins = 'elementspath';
    config.dialog_noConfirmCancel = true;
    config.title = false;

    // config.removeButtons = 'Source, Preview, Print, Templates, Document, Iframe, Anchor, CreateDiv, Blockquote, Save';
    // config.removeButtons = 'Source, Save';
    config.toolbar = [
        ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
        ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
        ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
        ['BidiLtr', 'BidiRtl' ],
        ['Link','Unlink'],
        ['Image','Table','HorizontalRule','Smiley','SpecialChar','PageBreak', '-', 'Undo','Redo', '-', 'TextColor','BGColor','Font','FontSize'],
        ['Syntaxhighlight']
    ];

    config.filebrowserBrowseUrl = 'http://localhost:8888/server11/includes/libs/ckfinder/ckfinder.html';

    config.filebrowserImageBrowseUrl = 'http://localhost:8888/server11/includes/libs/ckfinder/ckfinder.html?type=Images';

    config.filebrowserFlashBrowseUrl = 'http://localhost:8888/server11/includes/libs/ckfinder/ckfinder.html?type=Flash';

    config.filebrowserUploadUrl = 'http://localhost:8888/includes/server11/libs/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';

    config.filebrowserImageUploadUrl = 'http://localhost:8888/server11/includes/libs/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';

    config.filebrowserFlashUploadUrl = 'http://localhost:8888/server11/includes/libs/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';

};
// CKEDITOR.on( 'dialogDefinition', function( ev )
// {
//     var dialogName = ev.data.name;
//     var dialogDefinition = ev.data.definition;
//
//
//     if (dialogName == 'table') {
//
//         // Get the advanced tab reference
//         var infoTab2 = dialogDefinition.getContents('advanced');
//
//         //Set the default
//
//         // Remove the 'Advanced' tab completely
//         dialogDefinition.removeContents('advanced');
//
//
//         // Get the properties tab reference
//         var infoTab = dialogDefinition.getContents('info');
//
//         // Remove unnecessary bits from this tab
//         infoTab.remove('txtBorder');
//         infoTab.remove('cmbAlign');
//         infoTab.remove('txtWidth');
//         infoTab.remove('txtHeight');
//         infoTab.remove('txtCellSpace');
//         infoTab.remove('txtCellPad');
//         infoTab.remove('txtCaption');
//         infoTab.remove('txtSummary');
//     }
// });