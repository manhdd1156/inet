<?php
/**
 * ajax -> data -> upload
 *
 * @package CINet v1.1
 * @author NOGA Co., Ltd
 */

require('class-image.php');

// check user activated
if($system['activation_enabled'] && !$user->_data['user_activated']) {
    _api_error(0, __("Before you can interact with other users, you need to confirm your email address"));
}

// check type
if(!isset($type)) {
    _api_error(403);
}

// check handle
if(!isset($handle)) {
    _api_error(403);
}

// check multiple
if(!isset($multiple)) {
    _api_error(403);
}

$_files = array();
// upload
try {

    switch ($type) {
        case 'photos':
            // check photo upload enabled
            if($handle == 'publisher' && !$system['photos_enabled']) {
                _api_error(0, __("This feature has been disabled"));
            }

            // get allowed file size
            if($handle == 'picture-user' || $handle == 'picture-page' || $handle == 'picture-group') {
                $max_allowed_size = $system['max_avatar_size'] * 1024;
            } elseif ($handle == 'cover-user' || $handle == 'cover-page' || $handle == 'cover-group') {
                $max_allowed_size = $system['max_cover_size'] * 1024;
            } else {
                $max_allowed_size = $system['max_photo_size'] * 1024;
            }

            /* check & create uploads dir */
            $depth = '';
            $folder = 'photos';
            if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder)) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder, 0777, true);
            }
            if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'))) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'), 0777, true);
            }
            if(!file_exists($system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
            }

            if($multiple) {

                $files = array();
                foreach($_FILES['photo'] as $key => $val) {
                    for($i=0; $i < count($val); $i++) {
                        $files[$i][$key] = $val[$i];
                    }
                }

                $return_files = array();
                $files_num = count($files);
                foreach ($files as $file) {

                    // valid inputs
                    if(!isset($file) || $file["error"] != UPLOAD_ERR_OK) {
                        if($files_num > 1) {
                            continue;
                        } else {
                            _api_error(0, __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                        }
                    }

                    // check file size
                    if($file["size"] > $max_allowed_size) {
                        if($files_num > 1) {
                            continue;
                        } else {
                            _api_error(0, __("The file size is so big"));
                        }
                    }

                    /* prepare new file name */
                    $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                    $prefix = $system['uploads_prefix'].'_'.md5(time()*rand(1, 9999));
                    $image = new Image($file["tmp_name"]);
                    $image_tmp_name = $directory.$prefix.'_tmp'.$image->_img_ext;
                    $image_new_name = $directory.$prefix.$image->_img_ext;
                    $path_tmp = $depth.$system['system_uploads_directory'].'/'.$image_tmp_name;
                    $path_new = $depth.$system['system_uploads_directory'].'/'.$image_new_name;

                    /* check if the file uploaded successfully */
                    if(!@move_uploaded_file($file['tmp_name'], $path_tmp)) {
                        if($files_num > 1) {
                            continue;
                        } else {
                            _api_error(0, __("Sorry, can not upload the file"));
                        }
                    }

                    /* save the new image */
                    $image->save($path_new, $path_tmp);

                    /* delete the tmp image */
                    unlink($path_tmp);

                    /* return */
                    $return_files[] = $image_new_name;
                }

                // return the return_files & exit
                $_photos = $return_files;

            } else {
                // valid inputs
                if(!isset($_FILES["photo"]) || $_FILES["photo"]["error"] != UPLOAD_ERR_OK) {
                    _api_error(0, __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                }

                // check file size
                if($_FILES["photo"]["size"] > $max_allowed_size) {
                    _api_error(0, __("The file size is so big"));
                }

                /* prepare new file name */
                $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'].'_'.md5(time()*rand(1, 9999));
                $image = new Image($_FILES["photo"]["tmp_name"]);
                $image_tmp_name = $directory.$prefix.'_tmp'.$image->_img_ext;
                $image_new_name = $directory.$prefix.$image->_img_ext;
                $path_tmp = $depth.$system['system_uploads_directory'].'/'.$image_tmp_name;
                $path_new = $depth.$system['system_uploads_directory'].'/'.$image_new_name;

                /* check if the file uploaded successfully */
                if(!@move_uploaded_file($_FILES['photo']['tmp_name'], $path_tmp)) {
                    _api_error(0, __("Sorry, can not upload the file"));
                }

                /* save the new image */
                $image->save($path_new, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);

                // check the handle
                switch ($handle) {

                    case 'cover-user':
                        /* check for cover album */
                        if(!$user->_data['user_album_covers']) {
                            /* create new cover album */
                            $db->query(sprintf("INSERT INTO posts_photos_albums (user_id, user_type, title, privacy) VALUES (%s, 'user', 'Cover Photos', 'public')", secure($user->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                            $user->_data['user_album_covers'] = $db->insert_id;
                            /* update user cover album id */
                            $db->query(sprintf("UPDATE users SET user_album_covers = %s WHERE user_id = %s", secure($user->_data['user_album_covers'], 'int'), secure($user->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                        }

                        /* insert updated cover photo post */
                        $db->query(sprintf("INSERT INTO posts (user_id, user_type, post_type, time, privacy) VALUES (%s, 'user', 'profile_cover', %s, 'public')", secure($user->_data['user_id'], 'int'), secure($date) )) or _error(SQL_ERROR_THROWEN);
                        $post_id = $db->insert_id;
                        /* insert new cover photo to album */
                        $db->query(sprintf("INSERT INTO posts_photos (post_id, album_id, source) VALUES (%s, %s, %s)", secure($post_id, 'int'), secure($user->_data['user_album_covers'], 'int'), secure($image_new_name) )) or _error(SQL_ERROR_THROWEN);
                        $photo_id = $db->insert_id;
                        /* update user cover */
                        $db->query(sprintf("UPDATE users SET user_cover = %s, user_cover_id = %s WHERE user_id = %s", secure($image_new_name), secure($photo_id, 'int'), secure($user->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                        break;

                    case 'picture-user':
                        try {
                            $db->autocommit(false);
                            $db->begin_transaction();

                            /* check for profile pictures album */
                            if(!$user->_data['user_album_pictures']) {
                                /* create new profile pictures album */
                                $db->query(sprintf("INSERT INTO posts_photos_albums (user_id, user_type, title, privacy) VALUES (%s, 'user', 'Profile Pictures', 'public')", secure($user->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                                $user->_data['user_album_pictures'] = $db->insert_id;
                                /* update user profile picture album id */
                                $db->query(sprintf("UPDATE users SET user_album_pictures = %s WHERE user_id = %s", secure($user->_data['user_album_pictures'], 'int'), secure($user->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                            }
                            /* insert updated profile picture post */
                            $db->query(sprintf("INSERT INTO posts (user_id, user_type, post_type, time, privacy) VALUES (%s, 'user', 'profile_picture', %s, 'public')", secure($user->_data['user_id'], 'int'), secure($date) )) or _error(SQL_ERROR_THROWEN);
                            $post_id = $db->insert_id;
                            /* insert new profile picture to album */
                            $db->query(sprintf("INSERT INTO posts_photos (post_id, album_id, source) VALUES (%s, %s, %s)", secure($post_id, 'int'), secure($user->_data['user_album_pictures'], 'int'), secure($image_new_name) )) or _error(SQL_ERROR_THROWEN);
                            $photo_id = $db->insert_id;
                            /* update user profile picture */
                            $db->query(sprintf("UPDATE users SET user_picture = %s, user_picture_id = %s WHERE user_id = %s", secure($image_new_name), secure($photo_id, 'int'), secure($user->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);

                            /* CI - Firebase */
                            $args = array(
                                'user_id' => $user->_data['user_id'],
                                'action' => UPDATE_USER,
                                'user_picture' => get_picture($image_new_name, $user->_data['user_gender'])
                            );
                            insertBackgroundFirebase($args);
                            /* END - Firebase */

                            $db->commit();
                        } catch (Exception $e) {
                            $db->rollback();
                            throw new Exception();
                        } finally {
                            $db->autocommit(true);
                        }

                        break;

                    case 'cover-page':
                        /* check if page id is set */
                        if(!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                            /* delete the uploaded image & return error 403 */
                            unlink($path);
                            _api_error(403);
                        }
                        /* check if the user is the page admin */
                        $get_page = $db->query(sprintf("SELECT * FROM pages WHERE page_id = %s AND page_admin = %s", secure($_POST['id'], 'int'), secure($user->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                        if($get_page->num_rows == 0) {
                            /* delete the uploaded image & return error 403 */
                            unlink($path);
                            _api_error(403);
                        }
                        $page = $get_page->fetch_assoc();
                        /* check for cover album */
                        if(!$page['page_album_covers']) {
                            /* create new cover album */
                            $db->query(sprintf("INSERT INTO posts_photos_albums (user_id, user_type, title, privacy) VALUES (%s, 'page', 'Cover Photos', 'public')", secure($page['page_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                            $page['page_album_covers'] = $db->insert_id;
                            /* update page cover album id */
                            $db->query(sprintf("UPDATE pages SET page_album_covers = %s WHERE page_id = %s", secure($page['page_album_covers'], 'int'), secure($page['page_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                        }

                        /* CI - kiểm tra nếu là trường thì thêm vào school_id */
                        $schoolId = 0;
                        if ($page['page_category'] == SCHOOL_CATEGORY_ID) {
                            $schoolId = $page['page_id'];
                        }
                        /* insert updated cover photo post */
                        $db->query(sprintf("INSERT INTO posts (user_id, user_type, post_type, time, privacy, school_id) VALUES (%s, 'page', 'page_cover', %s, 'public', %s)", secure($page['page_id'], 'int'), secure($date), secure($schoolId, 'int') )) or _error(SQL_ERROR_THROWEN);
                        $post_id = $db->insert_id;
                        /* END - CI */

                        /* insert new cover photo to album */
                        $db->query(sprintf("INSERT INTO posts_photos (post_id, album_id, source) VALUES (%s, %s, %s)", secure($post_id, 'int'), secure($page['page_album_covers'], 'int'), secure($image_new_name) )) or _error(SQL_ERROR_THROWEN);
                        $photo_id = $db->insert_id;
                        /* update page cover */
                        $db->query(sprintf("UPDATE pages SET page_cover = %s, page_cover_id = %s WHERE page_id = %s", secure($image_new_name), secure($photo_id, 'int'), secure($page['page_id'], 'int') )) or _error(SQL_ERROR_THROWEN);

                        /* ---------- UPDATE - MEMCACHE ---------- */
                        if($page['page_category'] == SCHOOL_CATEGORY_ID) {
                            updateSchoolData($page['page_id'], SCHOOL_INFO);
                        }
                        /* ---------- END - MEMCACHE ---------- */


                        break;

                    case 'picture-page':
                        /* check if page id is set */
                        if(!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                            /* delete the uploaded image & return error 403 */
                            unlink($path);
                            _api_error(403);
                        }
                        /* check if the user is the page admin */
                        $get_page = $db->query(sprintf("SELECT * FROM pages WHERE page_id = %s AND page_admin = %s", secure($_POST['id'], 'int'), secure($user->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                        if($get_page->num_rows == 0) {
                            /* delete the uploaded image & return error 403 */
                            unlink($path);
                            _api_error(403);
                        }
                        $page = $get_page->fetch_assoc();
                        /* check for page pictures album */
                        if(!$page['page_album_pictures']) {
                            /* create new page pictures album */
                            $db->query(sprintf("INSERT INTO posts_photos_albums (user_id, user_type, title, privacy) VALUES (%s, 'page', 'Profile Pictures', 'public')", secure($page['page_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                            $page['page_album_pictures'] = $db->insert_id;
                            /* update page profile picture album id */
                            $db->query(sprintf("UPDATE pages SET page_album_pictures = %s WHERE page_id = %s", secure($page['page_album_pictures'], 'int'), secure($page['page_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                        }

                        /* CI - kiểm tra nếu là trường thì thêm vào school_id */
                        $schoolId = 0;
                        if ($page['page_category'] == SCHOOL_CATEGORY_ID) {
                            $schoolId = $page['page_id'];
                        }
                        /* insert updated page picture post */
                        $db->query(sprintf("INSERT INTO posts (user_id, user_type, post_type, time, privacy, school_id) VALUES (%s, 'page', 'page_picture', %s, 'public', %s)", secure($page['page_id'], 'int'), secure($date), secure($schoolId, 'int') )) or _error(SQL_ERROR_THROWEN);
                        $post_id = $db->insert_id;
                        /* END - CI */

                        /* insert new page picture to album */
                        $db->query(sprintf("INSERT INTO posts_photos (post_id, album_id, source) VALUES (%s, %s, %s)", secure($post_id, 'int'), secure($page['page_album_pictures'], 'int'), secure($image_new_name) )) or _error(SQL_ERROR_THROWEN);
                        $photo_id = $db->insert_id;
                        /* update page picture */
                        $db->query(sprintf("UPDATE pages SET page_picture = %s, page_picture_id = %s WHERE page_id = %s", secure($image_new_name), secure($photo_id, 'int'), secure($page['page_id'], 'int') )) or _error(SQL_ERROR_THROWEN);

                        /* ---------- UPDATE - MEMCACHE ---------- */
                        if($page['page_category'] == SCHOOL_CATEGORY_ID) {
                            updateSchoolData($page['page_id'], SCHOOL_INFO);
                        }
                        /* ---------- END - MEMCACHE ---------- */

                        break;

                    case 'cover-group':
                        /* check if group id is set */
                        if(!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                            /* delete the uploaded image & return error 403 */
                            unlink($path);
                            _error(403);
                        }
                        /* check if the user is the group admin */
                        //CONIU-Cho phép giáo viên cập nhật ảnh cover group
                        //$get_group = $db->query(sprintf("SELECT * FROM groups WHERE group_id = %s AND group_admin = %s", secure($_POST['id'], 'int'), secure($user->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                        $get_group = $db->query(sprintf("SELECT * FROM groups WHERE group_id = %s", secure($_POST['id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                        if($get_group->num_rows == 0) {
                            /* delete the uploaded image & return error 403 */
                            unlink($path);
                            _api_error(403);
                        }
                        $group = $get_group->fetch_assoc();
                        /* check for group covers album */
                        if(!$group['group_album_covers']) {
                            /* create new group covers album */
                            $db->query(sprintf("INSERT INTO posts_photos_albums (user_id, user_type, in_group, group_id, title, privacy) VALUES (%s, 'user', '1', %s, 'Cover Photos', 'public')", secure($user->_data['user_id'], 'int'), secure($group['group_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                            $group['group_album_covers'] = $db->insert_id;
                            /* update group cover album id */
                            $db->query(sprintf("UPDATE groups SET group_album_covers = %s WHERE group_id = %s", secure($group['group_album_covers'], 'int'), secure($group['group_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                        }
                        /* insert updated group cover post */
                        $db->query(sprintf("INSERT INTO posts (user_id, user_type, post_type, in_group, group_id, time, privacy) VALUES (%s, 'user', 'group_cover', '1', %s, %s, 'public')", secure($user->_data['user_id'], 'int'), secure($group['group_id'], 'int'), secure($date) )) or _error(SQL_ERROR_THROWEN);
                        $post_id = $db->insert_id;
                        /* insert new group cover to album */
                        $db->query(sprintf("INSERT INTO posts_photos (post_id, album_id, source) VALUES (%s, %s, %s)", secure($post_id, 'int'), secure($group['group_album_covers'], 'int'), secure($image_new_name) )) or _error(SQL_ERROR_THROWEN);
                        $photo_id = $db->insert_id;
                        /* update group cover */
                        $db->query(sprintf("UPDATE groups SET group_cover = %s, group_cover_id = %s WHERE group_id = %s", secure($image_new_name), secure($photo_id, 'int'), secure($group['group_id'], 'int') )) or _error(SQL_ERROR_THROWEN);

                        /* ---------- UPDATE - MEMCACHE ---------- */
                        if($group['class_level_id']  > 0) {
                            updateClassData($group['group_id'], CLASS_INFO);
                        }
                        /* ---------- END - MEMCACHE ---------- */
                        break;

                    case 'picture-group':
                        /* check if group id is set */
                        if(!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                            /* delete the uploaded image & return error 403 */
                            unlink($path);
                            _api_error(403);
                        }
                        /* check if the user is the group admin */
                        //CONIU - Cho phep giao vien cung co the upload avatar
                        //$get_group = $db->query(sprintf("SELECT * FROM groups WHERE group_id = %s AND group_admin = %s", secure($_POST['id'], 'int'), secure($user->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                        $get_group = $db->query(sprintf("SELECT * FROM groups WHERE group_id = %s", secure($_POST['id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                        if($get_group->num_rows == 0) {
                            /* delete the uploaded image & return error 403 */
                            unlink($path);
                            _api_error(403);
                        }
                        $group = $get_group->fetch_assoc();
                        /* check for group pictures album */
                        if(!$group['group_album_pictures']) {
                            /* create new group pictures album */
                            $db->query(sprintf("INSERT INTO posts_photos_albums (user_id, user_type, in_group, group_id, title, privacy) VALUES (%s, 'user', '1', %s, 'Profile Pictures', 'public')", secure($user->_data['user_id'], 'int'), secure($group['group_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                            $group['group_album_pictures'] = $db->insert_id;
                            /* update group profile picture album id */
                            $db->query(sprintf("UPDATE groups SET group_album_pictures = %s WHERE group_id = %s", secure($group['group_album_pictures'], 'int'), secure($group['group_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                        }
                        /* insert updated group picture post */
                        $db->query(sprintf("INSERT INTO posts (user_id, user_type, post_type, in_group, group_id, time, privacy) VALUES (%s, 'user', 'group_picture', '1', %s, %s, 'public')", secure($user->_data['user_id'], 'int'), secure($group['group_id'], 'int'), secure($date) )) or _error(SQL_ERROR_THROWEN);
                        $post_id = $db->insert_id;
                        /* insert new group picture to album */
                        $db->query(sprintf("INSERT INTO posts_photos (post_id, album_id, source) VALUES (%s, %s, %s)", secure($post_id, 'int'), secure($group['group_album_pictures'], 'int'), secure($image_new_name) )) or _error(SQL_ERROR_THROWEN);
                        $photo_id = $db->insert_id;
                        /* update group picture */
                        $db->query(sprintf("UPDATE groups SET group_picture = %s, group_picture_id = %s WHERE group_id = %s", secure($image_new_name), secure($photo_id, 'int'), secure($group['group_id'], 'int') )) or _error(SQL_ERROR_THROWEN);

                        /* ---------- UPDATE - MEMCACHE ---------- */
                        if($group['class_level_id']  > 0) {
                            updateClassData($group['group_id'], CLASS_INFO);
                        }
                        /* ---------- END - MEMCACHE ---------- */
                        break;

                }

                // return the file new name & exit
                // return_json(array("file" => $image_new_name));
                 $_photos[] = $image_new_name;
            }

            break;

        case 'video':
            // check video upload enabled
            if(!$system['videos_enabled']) {
                _api_error(0, __("This feature has been disabled"));
            }

            // valid inputs
            if(!isset($_FILES["video"]) || $_FILES["video"]["error"] != UPLOAD_ERR_OK) {
                _api_error(0, __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
            }

            // check file size
            $max_allowed_size = $system['max_video_size'] * 1024;
            if($_FILES["video"]["size"] > $max_allowed_size) {
                _api_error(0, __("The file size is so big"));
            }

            // check file extesnion
            $extension = get_extension($_FILES['video']['name']);
            if(!valid_extension($extension, $system['video_extensions'])) {
                _api_error(0, __("The file type is not valid or not supported"));
            }

            /* check & create uploads dir */
            $depth = '';
            $folder = 'videos';
            if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder)) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder, 0777, true);
            }
            if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'))) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'), 0777, true);
            }
            if(!file_exists($system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
            }

            /* prepare new file name */
            $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
            $prefix = $system['uploads_prefix'].'_'.md5(time()*rand(1, 9999));
            $file_name = $directory.$prefix.'.'.$extension;
            $path = $depth.$system['system_uploads_directory'].'/'.$file_name;

            /* check if the file uploaded successfully */
            if(!@move_uploaded_file($_FILES['video']['tmp_name'], $path)) {
                _api_error(0, __("Sorry, can not upload the file"));
            }

            // return the file new name & exit
            //return_json(array("file" => $file_name));
            $_video = $file_name;
            break;

        case 'audio':
            // check audio upload enabled
            if(!$system['audio_enabled']) {
                _api_error(0, __("This feature has been disabled"));
            }

            // valid inputs
            if(!isset($_FILES["audio"]) || $_FILES["audio"]["error"] != UPLOAD_ERR_OK) {
                _api_error(0, __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
            }

            // check file size
            $max_allowed_size = $system['max_audio_size'] * 1024;
            if($_FILES["audio"]["size"] > $max_allowed_size) {
                _api_error(0, __("The file size is so big"));
            }

            // check file extesnion
            $extension = get_extension($_FILES['audio']['name']);
            if(!valid_extension($extension, $system['audio_extensions'])) {
                _api_error(0, __("The file type is not valid or not supported"));
            }

            /* check & create uploads dir */
            $depth = '';
            $folder = 'sounds';
            if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder)) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder, 0777, true);
            }
            if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'))) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'), 0777, true);
            }
            if(!file_exists($system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
            }

            /* prepare new file name */
            $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
            $prefix = $system['uploads_prefix'].'_'.md5(time()*rand(1, 9999));
            $file_name = $directory.$prefix.'.'.$extension;
            $path = $depth.$system['system_uploads_directory'].'/'.$file_name;

            /* check if the file uploaded successfully */
            if(!@move_uploaded_file($_FILES['audio']['tmp_name'], $path)) {
                _api_error(0, __("Upload Error"), __("Sorry, can not upload the file"));
            }

            // return the file new name & exit
            //return_json(array("file" => $file_name));
            $_audio = $file_name;
            break;

        case 'file':
            // check file upload enabled
            if(!$system['file_enabled']) {
                _api_error(0, __("This feature has been disabled"));
            }

            // valid inputs
            if(!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                _api_error(0, __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
            }

            // check file size
            $max_allowed_size = $system['max_file_size'] * 1024;
            if($_FILES["file"]["size"] > $max_allowed_size) {
                _api_error(0, __("Upload Error"), __("The file size is so big"));
            }

            // check file extesnion
            $extension = get_extension($_FILES['file']['name']);
            if(!valid_extension($extension, $system['file_extensions'])) {
                _api_error(0, __("The file type is not valid or not supported"));
            }

            /* check & create uploads dir */
            $depth = '';
            $folder = 'files';
            if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder)) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder, 0777, true);
            }
            if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'))) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'), 0777, true);
            }
            if(!file_exists($system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
            }

            /* prepare new file name */
            $directory = '/'.$folder.'/'. date('Y') . '/' . date('m') . '/';
            $prefix = $system['uploads_prefix'].'_'.md5(time()*rand(1, 9999));
            $file_name = $directory.$prefix.'.'.$extension;
            $path = $depth.$system['system_uploads_directory'].'/'.$file_name;

            /* check if the file uploaded successfully */
            if(!@move_uploaded_file($_FILES['file']['tmp_name'], $path)) {
                _api_error(0, __("Sorry, can not upload the file"));
            }

            // return the file new name & exit
            // return_json(array("file" => $file_name));
            $_file = $file_name;
            break;

        case 'uploadImage':
            // check photo upload enabled
            if($handle == 'upload' && !$system['photos_enabled']) {
                _api_error(0, __("This feature has been disabled"));
            }

            // get allowed file size
            $max_allowed_size = $system['max_photo_size'] * 1024;


            /* check & create uploads dir */
            $depth = '';
            $folder = 'chat';
            if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder)) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder, 0777, true);
            }
            if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'))) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'), 0777, true);
            }
            if(!file_exists($system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
            }

            if($multiple) {

                $files = array();
                foreach($_FILES['photo'] as $key => $val) {
                    for($i=0; $i < count($val); $i++) {
                        $files[$i][$key] = $val[$i];
                    }
                }

                $return_files = array();
                $files_num = count($files);
                foreach ($files as $file) {

                    // valid inputs
                    if(!isset($file) || $file["error"] != UPLOAD_ERR_OK) {
                        if($files_num > 1) {
                            continue;
                        } else {
                            _api_error(0, __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                        }
                    }

                    // check file size
                    if($file["size"] > $max_allowed_size) {
                        if($files_num > 1) {
                            continue;
                        } else {
                            _api_error(0, __("The file size is so big"));
                        }
                    }

                    /* prepare new file name */
                    $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                    $prefix = $system['uploads_prefix'].'_'.md5(time()*rand(1, 9999));
                    $image = new Image($file["tmp_name"]);
                    $image_tmp_name = $directory.$prefix.'_tmp'.$image->_img_ext;
                    $image_new_name = $directory.$prefix.$image->_img_ext;
                    $path_tmp = $depth.$system['system_uploads_directory'].'/'.$image_tmp_name;
                    $path_new = $depth.$system['system_uploads_directory'].'/'.$image_new_name;

                    /* check if the file uploaded successfully */
                    if(!@move_uploaded_file($file['tmp_name'], $path_tmp)) {
                        if($files_num > 1) {
                            continue;
                        } else {
                            _api_error(0, __("Sorry, can not upload the file"));
                        }
                    }

                    /* save the new image */
                    $image->save($path_new, $path_tmp);

                    /* delete the tmp image */
                    unlink($path_tmp);

                    /* return */
                    $return_files[] = $system['system_uploads'].'/'.$image_new_name;
                }

                // return the return_files & exit
                $_photos = $return_files;

            } else {
                // valid inputs
                if(!isset($_FILES["photo"]) || $_FILES["photo"]["error"] != UPLOAD_ERR_OK) {
                    _api_error(0, __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                }

                // check file size
                if($_FILES["photo"]["size"] > $max_allowed_size) {
                    _api_error(0, __("The file size is so big"));
                }

                /* prepare new file name */
                $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'].'_'.md5(time()*rand(1, 9999));
                $image = new Image($_FILES["photo"]["tmp_name"]);
                $image_tmp_name = $directory.$prefix.'_tmp'.$image->_img_ext;
                $image_new_name = $directory.$prefix.$image->_img_ext;
                $path_tmp = $depth.$system['system_uploads_directory'].'/'.$image_tmp_name;
                $path_new = $depth.$system['system_uploads_directory'].'/'.$image_new_name;

                /* check if the file uploaded successfully */
                if(!@move_uploaded_file($_FILES['photo']['tmp_name'], $path_tmp)) {
                    _api_error(0, __("Sorry, can not upload the file"));
                }

                /* save the new image */
                $image->save($path_new, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);

                // return the file new name & exit
                $_photos = $system['system_uploads'].'/'.$image_new_name;
            }

            break;
    }

} catch (Exception $e) {
    _api_error(0, $e->getMessage());
}

?>