<?php
/**
 * version
 * 
 * @package CINet v1.1
 * @author NOGA Co., Ltd
 */

/**
 * Holds the Coniu version
 *
 * @global string $sngine_version
 */
$inet_version = '2.3';


/**
 * Holds the Coniu kernel version - Just for APIs
 *
 * @global string $sngine_kernal
 */
$sngine_api_version = '1.0';


/**
 * Holds the Coniu kernel version - Just for APIs
 *
 * @global string $sngine_kernal
 */
$sngine_kernel = '36.74.5758';


/**
 * Holds the required PHP version
 *
 * @global string $required_php_version
 */
$required_php_version = '5.4';

/**
 * Holds the required MySQL version
 *
 * @global string $required_mysql_version
 */
$required_mysql_version = '5.0';

?>