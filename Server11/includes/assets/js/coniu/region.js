/**
 * REGION js
 *
 * @package ConIu
 * @author TaiLA
 */

// initialize API URLs
api['region/userregion'] = ajax_path + "ci/bo/region/bo_region_userregion.php";

$(function() {
    // biến lưu thời gian xuất hiện modal
    var timeModal = 1500;

    // run DataTable
    // run DataTable
    $('.js_dataTable').DataTable({
        "aoColumnDefs": [{'aDataSort': false, 'bSortable': false, 'aTargets': [-1]}],
        "language": {
            "decimal":        "",
            "emptyTable":     __["No data available in table"],
            "info":           __["Showing _START_ to _END_ of _TOTAL_ results"],
            "infoEmpty":      __["Showing 0 to 0 of 0 results"],
            "infoFiltered":   "(filtered from _MAX_ total entries)",
            "infoPostFix":    "",
            "thousands":      ",",
            "lengthMenu":     __["Show"] + " " + "_MENU_" + " " + __["results"],
            "loadingRecords": __["Loading..."],
            "processing":     __["Processing..."],
            "search":         __["Search"],
            "zeroRecords":    __["No matching records found"],
            "paginate": {
                "first":      __["First"],
                "last":       __["Last"],
                "next":       __["Next"],
                "previous":   __["Previous"]
            },
            "aria": {
                "sortAscending":  ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            }
        }
    });

    // run metisMenu
    $(".js_metisMenu").metisMenu();

    // run open window
    $('body').on('click', '.js_open_window', function () {
        window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;
    });

    //BEGIN - Màn hình: thêm mới, sửa thông tin trường ---------------------------------
    /**
     * Tìm kiếm danh sách người dùng khi gõ vào ô text để search quản lý trường
     */
    $('body').on('keyup', '#search-manager', search_delay(function() {
        var query = $(this).val();

        if (query.length < 4) return;

        var user_ids = new Array();
        $("input[name*='user_ids']").each(function() {
            user_ids.push($(this).val());
        });
        if ((!is_empty(query)) && (user_ids.length == 0)) {
            $('#search-manager-results').show();
            $.post(api['region/userregion'], {'query': query, 'user_ids': user_ids, 'do': 'search_manager'}, function(response) {
                if(response.callback) {
                    eval(response.callback);
                } else if(response.results) {
                    $('#search-manager-results .dropdown-widget-header').show();
                    $('#search-manager-results .dropdown-widget-body').html(response.results);
                } else {
                    $('#search-manager-results .dropdown-widget-header').hide();
                    $('#search-manager-results .dropdown-widget-body').html(render_template('#search-for', {'query': query}));
                }
            }, 'json');
        }
    }, 1000));
    /**
     * Hiện danh sách xổ xuống khi click vào ô tìm user
     */
    $('body').on('click', '#search-manager', function() {
        var user_ids = new Array();
        $("input[name*='user_ids']").each(function() {
            user_ids.push($(this).val());
        });

        if (($(this).val() != '') && (user_ids.length == 0)) {
            $('#search-manager-results').show();
        }
    });

    /**
     * Hàm xử lý khi click vào nút Select trong danh sách người dùng search ra.
     */
    $('body').on('click', '.js_manager-select', function () {
        var user_id = $(this).attr('data-uid');
        /*
        var user_ids = new Array();
        $("input[name*='user_id']").each(function() {
            user_ids.push($(this).val());
        });
        */
        $.post(api['region/userregion'], {'user_id': user_id, 'do': 'add_manager'}, function(response) {
            if(response.results) {
                $('#manager_list').html(response.results);
                if($('#telephone').val() == '') {
                    $('#telephone').val(response.phone);
                }
                if($('#email').val() == '') {
                    $('#email').val(response.email);
                }
            }
        }, 'json');
    });

    /**
     * Hàm được gọi khi xóa cán bộ quản lý của trường.
     */
    $('body').on('click', '.js_manager-remove', function () {
        //var user_id = $(this).attr('data-uid');
        $('#manager_list').html('');
    });
    //END - Màn hình: thêm mới, sửa thông tin trường ---------------------------------

    /* ---------- BEGIN - PUSH NOTIFICATION ---------- */
    $('#push_time').datetimepicker({
        format: DATETIME_FORMAT,
        sideBySide: true
    });

    $('body').on('change', '#cb_type_user', function() {
        var div_type_user = $('#div_type_user');
        if(this.checked) {
            div_type_user.removeClass('x-hidden');
        } else {
            div_type_user.addClass('x-hidden');
        }
    });

    $('body').on('change', '#cb_area', function() {
        var div_area_city = $('#div_area_city');
        if(this.checked) {
            div_area_city.removeClass('x-hidden');
        } else {
            div_area_city.addClass('x-hidden');
        }
    });

    //  BEGIN - Màn hình tạo trường mới
    $('#area_city').change(function () {
        var city_id = this.value;
        if (city_id == '') {
            $('#district_slug').html('');
            $('#div_area_district').addClass('x-hidden');
        } else {
            $.post(api['noga/school'], {
                'do': 'district_list',
                'city_id': city_id
            }, function (response) {
                $('#district_slug').html(response.results);
                $('#div_area_district').removeClass('x-hidden');
            }, 'json')
                .fail(function () {
                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        }
    });

    $('body').on('change', '#cb_gender', function() {
        var div_gender = $('#div_gender');
        if(this.checked) {
            div_gender.removeClass('x-hidden');
        } else {
            div_gender.addClass('x-hidden');
        }
    });
    $('body').on('change', '#cb_age', function() {
        var div_from_age = $('#div_from_age');
        var div_to_age = $('#div_to_age');
        if(this.checked) {
            div_from_age.removeClass('x-hidden');
            div_to_age.removeClass('x-hidden');
        } else {
            div_from_age.addClass('x-hidden');
            div_to_age.addClass('x-hidden');
        }
    });

    $('#is_repeat').on('change', function () {
        var is_repeat = $('#is_repeat');
        var cyclic = $('#cyclic');
        if (is_repeat.is(':checked')) {
            cyclic.removeClass('x-hidden');
        } else {
            cyclic.addClass('x-hidden');
        }
    });

    /**
     * Khi xóa 1 thông báo
     */
    $('body').on('click', '.js_noga-notification-delete', function () {
        var notification_id = $(this).attr('data-id');

        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            $.post(api['noga/notification'], {
                'do': 'delete',
                'notification_id': notification_id
            }, function (response) {
                if (response.callback) {
                    eval(response.callback);
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });


    /* ---------- END - PUSH NOTIFICATION ---------- */


    /* ---------- BEGIN - FOETUS DEVELOPMENT  ---------- */

    // bắt sự kiện khi tạo thông tin thai nhi
    $(document).on('submit', '#create_foetus_development', function (e) {
        var _this = $(this);
        var submit = _this.find('button[type="submit"]');

        /* show loading */
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading']);

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        e.preventDefault();
        $.ajax({
            url: api['noga/foetusdevelopment'],
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false
        }).done(function (data) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            /* hide loading */
            submit.prop('disabled', true);
            submit.html(submit.data('text'));
            if (data.success) {
                modal('#modal-success', {title: __['Success'], message: data.message});
                modal_hidden(timeModal);
            } else if (data.error) {
                modal('#modal-error', {title: __['Error'], message: data.message});
            } else if (data.callback) {
                eval(data.callback);
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                /* hide loading */
                submit.prop('disabled', false);
                submit.html(submit.data('text'));
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // bắt sự kiện khi tạo thông tin phát triển trẻ
    $(document).on('submit', '#create_child_development', function (e) {
        var _this = $(this);
        var submit = _this.find('button[type="submit"]');

        /* show loading */
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading']);

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        e.preventDefault();
        $.ajax({
            url: api['noga/childdevelopment'],
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false
        }).done(function (data) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            /* hide loading */
            submit.prop('disabled', true);
            submit.html(submit.data('text'));
            if (data.success) {
                modal('#modal-success', {title: __['Success'], message: data.message});
                modal_hidden(timeModal);
            } else if (data.error) {
                modal('#modal-error', {title: __['Error'], message: data.message});
            } else if (data.callback) {
                eval(data.callback);
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                /* hide loading */
                submit.prop('disabled', false);
                submit.html(submit.data('text'));
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Xử lý xóa ảnh đính kèm
    $(".delete_image").click(function () {
        $("input[name*='file']").val("");
        $("#file_old").remove();
        $("#is_file").val("0");
    });

    // Xử lý khi thêm hàng ở màn hình thêm mới thông tin phát triển thai nhi
    $('body').on('click', '.js_add-foetus', function () {
        var index = $('#myTableFoetus tbody tr').length + 1;
        $('#myTableFoetus').append('<tr><td class = "index col_no" align = "center">' + index + '</td> <td align = "center"> <a class = "btn btn-default js_cate-delete_null"> ' + __["Delete"] + ' </a></td> <td><input style = "min-width:100px; box-shadow:0 4px 6px rgba(0,0,0,0.1);" type="text" name="title[]" class="form-control"> </td> <td><textarea class = "note" type="text" name = "shortdes[]"> </textarea></td> <td><textarea class = "note" type="text" name = "content[]"></textarea></td> <td> <input type = "text" name = "hour[]" placeholder="" style = "padding-left: 10px; height: 35px; min-width: 150px;"> </td> <td><select name="type[]" class="form-control" style="width: 150px"><option value="1">'+ __['Pregnancy check'] +'</option><option value="2">'+ __['Information'] +'</option></select></td> <td><select name="is_development[]" class="form-control" style="width: 150px"><option value="1">'+ __['Yes'] +'</option><option value="0">'+ __['No'] +'</option></select></td></tr>');
        $('.note').css('overflow', 'hidden').autogrow();
    });
    // Xử lý xóa dòng
    $('body').on('click', '.js_cate-delete', function () {
        var id = $(this).attr('data-id');
        $('.subject'+id).remove();
    });
    $('body').on("click", 'a.js_cate-delete_null', function(){
        var $this = $(this);
        var idx = 1;
        $this.parents('tr').remove();
        $(".col_no").each(function() {
            $(this).html(idx);
            idx = idx + 1;
        });
        // return false;
    });

    /**
     * Push notify
     */
    $('body').on('click', '.js_push-notify', function () {
        // var page_id = $('#page_id').val();
        // var class_id = $('#class_id option:selected').val();

        $.post(api['noga/foetusdevelopment'], {
            'do': 'push_notify'
        }, function (response) {
            //$('#parent_list').html(response.results);
            //eval($('#child_list').innerHTML);
        }, 'json')
            .fail(function () {
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Xử lý khi xóa: thông tin phát triển của thai nhi
    $('body').on('click', '.js_noga-foetus-info-delete', function () {
        var id = $(this).attr('data-id');

        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            $.post(api['noga/foetusdevelopment'], {
                'do': 'delete',
                'id': id
            }, function (response) {
                if (response.callback) {
                    eval(response.callback);
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    // Xử lý khi thêm hàng ở màn hình thêm mới thông tin kiến thức thai nhi
    $('body').on('click', '.js_add-foetus-know', function () {
        var index = $('#myTableFoetusKnow tbody tr').length + 1;
        $('#myTableFoetusKnow').append('<tr><td class = "index col_no" align = "center">' + index + '</td> <td align = "center"> <a class = "btn btn-default js_cate-delete_null"> ' + __["Delete"] + ' </a></td> <td><input style = "min-width:100px; box-shadow:0 4px 6px rgba(0,0,0,0.1);" type="text" name="title[]" class="form-control"> </td> <td><textarea class = "note" type="text" name = "links[]" style="width: 100%"></textarea></td></tr>');
        $('.note').css('overflow', 'hidden').autogrow();
    });

    //Xử lý khi xóa: thông tin phát triển của trẻ
    $('body').on('click', '.js_noga-child-development-delete', function () {
        var id = $(this).attr('data-id');

        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            $.post(api['noga/childdevelopment'], {
                'do': 'delete',
                'id': id
            }, function (response) {
                if (response.callback) {
                    eval(response.callback);
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    // Xử lý khi thêm hàng ở màn hình thêm mới thông tin phát triển của trẻ
    $('body').on('click', '.js_add-foetus', function () {
        var index = $('#myTableChild tbody tr').length + 1;
        $('#myTableChild').append('<tr><td class = "index col_no" align = "center">' + index + '</td> <td align = "center"> <a class = "btn btn-default js_cate-delete_null"> ' + __["Delete"] + ' </a></td> <td><input style = "min-width:100px; box-shadow:0 4px 6px rgba(0,0,0,0.1);" type="text" name="title[]" class="form-control"> </td> <td><textarea class = "note" type="text" name = "shortdes[]"> </textarea></td> <td><textarea class = "note" type="text" name = "content[]"></textarea></td> <td> <input type = "number" name = "days[]" placeholder="" style = "padding-left: 10px; height: 35px; width: 70px;"> </td>  <td> <input type = "text" name = "times[]" placeholder="" style = "padding-left: 10px; height: 35px; min-width: 150px;"></td> <td><select name="type[]" class="form-control" style="width: 150px"><option value="1">'+ __['Vaccination'] +'</option><option value="2">'+ __['Information'] +'</option></select></td> <td><select name="is_development[]" class="form-control" style="width: 150px"><option value="1">'+ __['Yes'] +'</option><option value="0">'+ __['No'] +'</option></select></td></tr>');
        $('.note').css('overflow', 'hidden').autogrow();
    });

    /**
     * Push notify quá trình phát triển trẻ
     */
    $('body').on('click', '.js_push-notify-child', function () {
        // var page_id = $('#page_id').val();
        // var class_id = $('#class_id option:selected').val();

        $.post(api['noga/childdevelopment'], {
            'do': 'push_notify'
        }, function (response) {
            //$('#parent_list').html(response.results);
            //eval($('#child_list').innerHTML);
        }, 'json')
            .fail(function () {
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    /* ---------- END - FOETUS DEVELOPMENT ---------- */

    /* hide the control when clicked outside control */
    $('body').on('click', function(e) {
        if(!$(e.target).is("#search-manager")) {
            $('#search-manager-results').hide();
        }
    });

    $('#medicine_beginpicker_new').datetimepicker({
        format: DATE_FORMAT,
        defaultDate: new Date()
    });
    $('#medicine_endpicker_new').datetimepicker({
        format: DATE_FORMAT,
        defaultDate: new Date()
    });
    $("#medicine_beginpicker_new").on("dp.change", function (e) {
        $('#medicine_endpicker_new').data("DateTimePicker").minDate(e.date);
    });
    $('#medicine_beginpicker').datetimepicker({
        format: DATE_FORMAT
    });
    $('#medicine_endpicker').datetimepicker({
        format: DATE_FORMAT
    });
    $("#medicine_beginpicker").on("dp.change", function (e) {
        $('#medicine_endpicker').data("DateTimePicker").minDate(e.date);
    });
    //END - CHILD - Màn hình gửi thuốc cho trẻ-----------------------------

    //  BEGIN - Màn hình tạo trường mới
    $('#city_id').change(function () {
        var city_id = this.value;
        if (city_id == '') {
            $('#district_slug').html('');
        } else {
            $.post(api['noga/school'], {
                'do': 'district_list',
                'city_id': city_id
            }, function (response) {
                $('#district_slug').html(response.results);
            }, 'json')
                .fail(function () {
                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        }
    });

    /**
     * Tìm kiếm danh sách PHỤ HUYNH theo điều kiện nhập vào
     */
    $('body').on('click', '.js_noga-view-parent', function () {
        var page_id = $('#page_id').val();
        var class_id = $('#class_id option:selected').val();

        $.post(api['noga/school'], {
            'class_id': class_id,
            'page_id': page_id,
            'do': 'view_user'
        }, function (response) {
            $('#parent_list').html(response.results);
            //eval($('#child_list').innerHTML);
        }, 'json')
            .fail(function () {
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });


    /**
     * Tìm kiếm trẻ theo điều kiện nhập vào
     */
    $('body').on('click', '.js_child-search', function () {
        var school_username = $(this).attr('data-username');
        var school_id = $(this).attr('data-id');
        var is_new = $(this).attr('data-isnew');
        var page = $(this).attr('data-page');
        var class_id = $('#class_id option:selected').val();
        var keyword = $('#keyword').val();

        $('#child_list').html('');
        $('#search').addClass('x-hidden');
        $('#loading').removeClass('x-hidden');

        $.post(api['noga/search'], {
            'keyword': keyword,
            'school_id': school_id,
            'class_id': class_id,
            'page': page,
            'is_new': is_new,
            'school_username': school_username,
            'func': 'searchchildnoga'
        }, function (response) {
            $('#search').removeClass('x-hidden');
            $('#loading').addClass('x-hidden');

            $('#child_list').html(response.results);
        }, 'json')
            .fail(function () {
                $('#search').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    /**
     * Xử lý khi bấm vào export danh sách trẻ
     */
    $('body').on('click', '.js_noga-export-children', function () {
        var school_username = $(this).attr('data-username');
        var school_id = $(this).attr('data-id');

        $('#search').addClass('x-hidden');
        $('#export2excel').addClass('x-hidden');
        $('#export_processing').removeClass('x-hidden');
        $.post(api['noga/school'], {
                'school_username': school_username,
                'school_id': school_id,
                'do': 'export'
            },
            function (response) {
                $('#search').removeClass('x-hidden');
                $('#export2excel').removeClass('x-hidden');
                $('#export_processing').addClass('x-hidden');

                var $a = $("<a>");
                $a.attr("href", response.file);
                $("body").append($a);
                $a.attr("download",response.file_name);
                $a[0].click();
                $a.remove();
            }, 'json')
            .fail(function () {
                $('#search').removeClass('x-hidden');
                $('#export2excel').removeClass('x-hidden');
                $('#export_processing').addClass('x-hidden');
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Xử lý khi xóa: thông tin tháng tuổi của trẻ
    $('body').on('click', '.js_noga-child-month-delete', function () {
        var id = $(this).attr('data-id');

        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            $.post(api['noga/childmonth'], {
                'do': 'delete',
                'id': id
            }, function (response) {
                if (response.callback) {
                    eval(response.callback);
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    // Xử lý khi thêm hàng ở màn hình thêm mới thông tin tháng tuổi của trẻ
    $('body').on('click', '.js_add-child-month', function () {
        var index = $('#myTableChildMonth tbody tr').length + 1;
        $('#myTableChildMonth').append('<tr><td class = "index col_no" align = "center">' + index + '</td> <td align = "center"> <a class = "btn btn-default js_cate-delete_null"> ' + __["Delete"] + ' </a></td> <td><input style = "min-width:100px; box-shadow:0 4px 6px rgba(0,0,0,0.1);" type="number" name="months[]" class="form-control"></td><td><textarea class = "note js_autosize" type="text" name = "titles[]" style="width:100%"></textarea></td><td><textarea class = "note js_autosize" type="text" name = "links[]" style="width: 100%"></textarea></td></tr>');
        $('.note').css('overflow', 'hidden').autogrow();
    });

    //Xử lý khi xóa: thông tin tuần thai
    $('body').on('click', '.js_noga-pregnancy-delete', function () {
        var id = $(this).attr('data-id');

        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            $.post(api['noga/pregnancy'], {
                'do': 'delete',
                'id': id
            }, function (response) {
                if (response.callback) {
                    eval(response.callback);
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    // Xử lý khi thêm hàng ở màn hình thêm mới thông tin tháng tuổi của trẻ
    $('body').on('click', '.js_add-pregnancy', function () {
        var index = $('#myTablePregnancy tbody tr').length + 1;
        $('#myTablePregnancy').append('<tr><td class = "index col_no" align = "center">' + index + '</td> <td align = "center"> <a class = "btn btn-default js_cate-delete_null"> ' + __["Delete"] + ' </a></td> <td><input style = "min-width:100px; box-shadow:0 4px 6px rgba(0,0,0,0.1);" type="number" name="weeks[]" class="form-control"> </td><td><textarea class = "note" type="text" name = "titles[]" style="width:100%"></textarea></td> <td><textarea class = "note js_autosize" type="text" name = "links[]" style="width: 100%"></textarea></td></tr>');
        $('.note').css('overflow', 'hidden').autogrow();
    });

    // Tự động giãn chiều cao textarea có class = "note"
    function h(e) {
        $(e).css({'height':'35px','overflow':'hidden', 'resize':'vertical'}).height(e.scrollHeight);
    }
    $('.note').each(function () {
        h(this);
    }).on('input', function () {
        h(this);
    });

    // //Xử lý khi chọn TRẠNG THÁI ở màn hình noga danh sách trường
    // $("#school_status").change(function () {
    //     var nogaRole = $("#nogaRole").val();
    //     var school_list = $('#school_list');
    //     var school_status = $('#school_status').val();
    //     school_list.html('');
    //
    //     $.post(api['noga/school'], {
    //         'do': 'school_search',
    //         'nogaRole': nogaRole,
    //         'school_status': school_status
    //     }, function (response) {
    //         school_list.html(response.results);
    //         // $('#all_month').addClass('x-hidden');
    //     }, 'json')
    //         .fail(function () {
    //             modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
    //         });
    // });

    //Xử lý khi tìm kiếm theo điều kiện trong danh sách trường (noga)
    $("body").on('click', '#noga_search_school', function () {
        var nogaRole = $("#nogaRole").val();
        var school_list = $('#school_list');
        var school_status = $('#school_status').val();
        var city_id = $('#city_id').val();
        var district_slug = $('#district_slug').val();
        school_list.html('');

        $.post(api['noga/school'], {
            'do': 'school_search',
            'nogaRole': nogaRole,
            'school_status': school_status,
            'city_id': city_id,
            'district_slug': district_slug,
        }, function (response) {
            school_list.html(response.results);
            // $('#all_month').addClass('x-hidden');
        }, 'json')
            .fail(function () {
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Xử lý khi chọn TRẠNG THÁI ở màn hình noga danh sách trường
    $('body').on('change', '.school_status_change', function() {
        var id = $(this).attr('data-id');
        var school_status = $(this).val();
        $.post(api['noga/school'], {
            'do': 'change_status',
            'id': id,
            'school_status': school_status
        }, function (response) {
            // school_list.html(response.results);
            // $('#all_month').addClass('x-hidden');
        }, 'json')
            .fail(function () {
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Xử lý khi chọn kiểu nội dung
    $('#content_type_select').on('change', function () {
        var content_text = $('.content_text');
        var content_link = $('.content_link');

        switch (this.value) {
            case '1': //chọn text
                if (!content_link.hasClass('x-hidden')) {
                    content_link.addClass('x-hidden');
                }
                if (content_text.hasClass('x-hidden')) {
                    content_text.removeClass('x-hidden');
                }
                break;
            case '2': //Chọn link
                if (!content_text.hasClass('x-hidden')) {
                    content_text.addClass('x-hidden');
                }
                if (content_link.hasClass('x-hidden')) {
                    content_link.removeClass('x-hidden');
                }
                break;
            case '3': //Chọn cả 2
                if (content_text.hasClass('x-hidden')) {
                    content_text.removeClass('x-hidden');
                }
                if (content_link.hasClass('x-hidden')) {
                    content_link.removeClass('x-hidden');
                }
                break;
            default:
        }
    });

    $('#fromuserregpicker').datetimepicker({
        format: DATE_FORMAT,
        defaultDate: new Date()
    });

    $('#touserregpicker').datetimepicker({
        format: DATE_FORMAT,
        defaultDate: new Date()
    });
    $("#fromuserregpicker").on("dp.change", function (e) {
        $('#touserregpicker').data("DateTimePicker").minDate(e.date);
    });

    //Xử lý khi tìm kiếm user mới đăng ký (từ ngày đến ngày)
    $('body').on('click', '.js_user-registed-new', function() {
        var fromDate = $('#fromDate').val();
        var toDate = $('#toDate').val();
        var user_new_list = $('#user_new_list');
        $.post(api['noga/user'], {
            'do': 'search_user_new',
            'from_date': fromDate,
            'to_date': toDate
        }, function (response) {
            if (response.results) {
                user_new_list.html(response.results);
            } else if (response.error){
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else {
                window.location.reload();
            }

        }, 'json')
            .fail(function () {
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Xử lý khi tìm kiếm quản lý và giáo viên
    $('body').on('click', '.js_noga-role-search', function() {
        var school_role = $('#school_role').val();
        var school_status = $('#school_status_no_js').val();
        var role_list = $('#role_list');
        $.post(api['noga/user'], {
            'do': 'search_role_user',
            'school_role': school_role,
            'school_status': school_status
        }, function (response) {
            if (response.results) {
                role_list.html(response.results);
            } else if (response.error){
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else {
                window.location.reload();
            }

        }, 'json')
            .fail(function () {
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    /**
     * Xử lý khi bấm vào export danh sách quản lý hoặc giáo viên
     */
    $('body').on('click', '.js_noga-export-user-role', function () {
        var school_role = $('#school_role').val();
        var school_status = $('#school_status_no_js').val();

        $('#search').addClass('x-hidden');
        $('#export2excel').addClass('x-hidden');
        $('#export_processing').removeClass('x-hidden');
        $.post(api['noga/user'], {
                'do': 'export',
                'school_role': school_role,
                'school_status': school_status
            },
            function (response) {
                $('#search').removeClass('x-hidden');
                $('#export2excel').removeClass('x-hidden');
                $('#export_processing').addClass('x-hidden');

                var $a = $("<a>");
                $a.attr("href", response.file);
                $("body").append($a);
                $a.attr("download",response.file_name);
                $a[0].click();
                $a.remove();
            }, 'json')
            .fail(function () {
                $('#search').removeClass('x-hidden');
                $('#export2excel').removeClass('x-hidden');
                $('#export_processing').addClass('x-hidden');
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    /**
     * Xử lý khi bấm vào export danh sách quản lý hoặc giáo viên
     */
    $('body').on('click', '.js_noga-export-user-new', function () {
        var fromDate = $('#fromDate').val();
        var toDate = $('#toDate').val();
        var user_new_list = $('#user_new_list');
        $.post(api['noga/user'], {
                'do': 'export_user_new',
                'from_date': fromDate,
                'to_date': toDate
            },
            function (response) {
                $('#search').removeClass('x-hidden');
                $('#export2excel').removeClass('x-hidden');
                $('#export_processing').addClass('x-hidden');

                var $a = $("<a>");
                $a.attr("href", response.file);
                $("body").append($a);
                $a.attr("download",response.file_name);
                $a[0].click();
                $a.remove();
            }, 'json')
            .fail(function () {
                $('#search').removeClass('x-hidden');
                $('#export2excel').removeClass('x-hidden');
                $('#export_processing').addClass('x-hidden');
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    /**
     * Xử lý khi chọn loại trong quá trình phát triển của trẻ
     */
    $('body').on('change', '.js_noga-child-development-search', function () {
        var type = $('#child_type').val();
        var noga_development_list = $('#noga_development_list');
        $.post(api['noga/childdevelopment'], {
                'do': 'search',
                'type': type
            },
            function (response) {
                if (response.results) {
                    noga_development_list.html(response.results);
                } else if (response.error){
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
            .fail(function () {
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    /**
     * Xử lý khi chọn loại trong quá trình phát triển của thai nhi
     */
    $('body').on('change', '.js_noga-foetus-development-search', function () {
        var type = $('#child_type').val();
        var noga_development_list = $('#noga_development_list');
        $.post(api['noga/foetusdevelopment'], {
                'do': 'search',
                'type': type
            },
            function (response) {
                if (response.results) {
                    noga_development_list.html(response.results);
                } else if (response.error){
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
            .fail(function () {
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Xử lý khi tìm kiếm page có bài đăng mới
    $('body').on('click', '.js_noga-page-search', function() {
        var fromDate = $('#fromDate').val();
        var toDate = $('#toDate').val();
        var page_list = $('#page_list');
        $.post(api['noga/page'], {
            'do': 'search',
            'from_date': fromDate,
            'to_date': toDate
        }, function (response) {
            if (response.results) {
                page_list.html(response.results);
            } else if (response.error){
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else {
                window.location.reload();
            }

        }, 'json')
            .fail(function () {
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    $('#fromDate').datetimepicker({
        format: DATE_FORMAT,
        defaultDate: new Date()
    });

    $('#toDate').datetimepicker({
        format: DATE_FORMAT,
        defaultDate: new Date()
    });
    $("#fromDate").on("dp.change", function (e) {
        $('#toDate').data("DateTimePicker").minDate(e.date);
    });

    // Search tổng hợp tương tác của từng trường
    $('body').on('click', '.js_statistic_school-search', function () {
        var school_id = $(this).attr('data-id');
        var fromDate = $('#begin').val();
        var toDate = $('#end').val();

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['noga/statistic'], {
            'do': 'search_interactive_school',
            'school_id': school_id,
            'fromDate': fromDate,
            'toDate': toDate
        }, function (response) {
            if (response.callback) {
                eval(response.callback);
            } else if (response.result_noga && response.result_school) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                $('#statistic_school_detail').html(response.result_noga);
                $('#statistic_parent_detail').html(response.result_school);
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Search tổng hợp tương tác của cả hệ thống
    $('body').on('click', '.js_statistic-search', function () {
        var fromDate = $('#begin').val();
        var toDate = $('#end').val();

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['noga/statistic'], {
            'do': 'search_interactive',
            'fromDate': fromDate,
            'toDate': toDate
        }, function (response) {
            if (response.callback) {
                eval(response.callback);
            } else if (response.result_noga && response.result_school) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                $('#statistic_school_detail').html(response.result_noga);
                $('#statistic_parent_detail').html(response.result_school);
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Search tổng hợp user online của cả hệ thống
    $('body').on('click', '.js_statistic_useronline-search', function () {
        var fromDate = $('#begin').val();
        var toDate = $('#end').val();

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['noga/statistic'], {
            'do': 'search_useronline',
            'fromDate': fromDate,
            'toDate': toDate
        }, function (response) {
            if (response.callback) {
                eval(response.callback);
            } else if (response.result) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                $('#statistic_useronline').html(response.result);
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    $('body').on('click', '.js_statistic_topic-search', function () {
        var fromDate = $('#begin').val();
        var toDate = $('#end').val();

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['noga/statistic'], {
            'do': 'search_interactive_topic',
            'fromDate': fromDate,
            'toDate': toDate
        }, function (response) {
            if (response.callback) {
                eval(response.callback);
            } else if (response.results) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                $('#statistic_topic_detail').html(response.results);
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });


    $('body').on('click', '.js_user-search', function () {
        var is_new = $(this).attr('data-isnew');
        var page = $(this).attr('data-page');
        var keyword = $('#keyword').val();

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $('#user_list').html('');
        $('#search').addClass('x-hidden');

        $.post(api['noga/search'], {
            'keyword': keyword,
            'page': page,
            'is_new': is_new,
            'func': 'searchuser'
        }, function (response) {
            // Show loading
            $("#loading_full_screen").addClass('hidden');

            if (response.results) {
                $('#search').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');

                $('#user_list').html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                $('#search').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    /**
     * Xóa user khỏi danh sách quản lý vùng
     */
    $('body').on('click', '.js_region_manage-delete-region', function () {
        var id = $(this).attr('data-id');

        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            $.post(api['region/userregion'], {
                'do': 'delete',
                'id': id
            }, function (response) {
                if (response.callback) {
                    eval(response.callback);
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });

    });
});