/**
 * notify js
 *
 * @package ConIu
 * @author QuanND
 */

// initialize API URLs
api['notify/notify'] = ajax_path + "ci/bo/bonotify_all.php";

$(function() {
    $('body').on('click', '.js_notification', function () {
        var action = $(this).attr('data-action');
        var node_type = $(this).attr('data-type');
        var node_url = $(this).attr('data-url');
        var id = $(this).attr('data-id');
        var extra1 = $(this).attr('data-extra1');
        var extra2 = $(this).attr('data-extra2');
        var extra3 = $(this).attr('data-extra3');

        $.post(api['notify/notify'], {'id': id, 'action': action,
                'extra1': extra1, 'extra2': extra2, 'extra3': extra3,
                'node_type': node_type, 'node_url': node_url}, function(response) {
                if(response.callback) {
                    eval(response.callback);
                } else {
                    window.location.reload();
                }
            }, 'json')
            .fail(function() {
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });
});