/**
 * chat js
 *
 * @package Coniu v1.1
 * @author NOGA Co., Ltd
 */

// initialize API URLs
api['chat/initialize'] = ajax_path + "chat/initialize.php";
api['chat/live'] = ajax_path + "chat/live.php";
api['chat/settings'] = ajax_path + "users/settings.php?edit=chat";
api['messages/post'] = ajax_path + "chat/post.php";
api['messages/get'] = ajax_path + "chat/get.messages.php";
api['conversation/get'] = ajax_path + "chat/get.conversation.php";
api['conversation/reaction'] = ajax_path + "chat/reaction.php";
api['messages/notification'] = ajax_path + "chat/send_notification.php";
var chat_participants = {};


// reconstruct chat-widgets
function reconstruct_chat_widgets() {
    if($(window).width() < 970) {
        return;
    }
    $('.chat-widget').each(function(index) {
        $(this).attr('style', '');
        var offset = (index*210) + (index*10);
        if($(this).prevAll('.chat-box').length > 0) {
            offset += $(this).prevAll('.chat-box').length*50;
        }
        if($('html').attr('dir') == 'RTL') {
            $(this).css('left', offset);
        } else {
            $(this).css('right', offset);
        }
    });
}


// chat box
function chat_box(ids, name, picture, conversation_id, multiple, name_list) {
    /* open the #chat_key */
    var chat_key_value = '';
    if (ids != 0) {
        chat_key_value = 'chat_' + ids;
    } else {
        chat_key_value = 'chat_' + conversation_id;
    }

    var chat_key = '#' + chat_key_value;
    /* check if this #chat_key already exists */
    if ($(chat_key).length == 0) {
        /* construct a new one */
        $('body').append(render_template('#chat-box', {
            'chat_key_value': chat_key_value,
            'ids': ids,
            'name': name,
            'picture': picture,
            'conversation_id': conversation_id,
            'multiple': multiple,
            'name_list': name_list
        }));
        $(chat_key).find('.chat-widget-content').toggle();
        $(chat_key).find('textarea').focus();
        /* initialize the plugins */
        initialize();
        /* reconstruct chat-widgets */
        reconstruct_chat_widgets();

        /* Xét trạng thái user online, offline ở chat_box */
        var item = document.getElementById('item_' + ids);
        if (item) {
            var hasItem = item.getElementsByClassName('chat-sidebar-online');
            if (hasItem.length > 0) {
                $(chat_key).find(".js_chat-box-status").removeClass("fa-user-secret").addClass("fa-circle");
            }
        }

        /* get conversation messages */
        if (conversation_id == false) {
            /* ids in this case will be just a single number */
            var data = {'ids': ids};
        } else {
            var data = {'ids': ids, 'conversation_id': conversation_id};
        }

        /* get conversation messages */
        if (conversation_id == false) {
            if (!isNaN(ids)) {
                /* ids in this case will be just a single number */
                var to_conversation_id = null;
                var args_chat = {};

                friendlyChat.database.ref('Users/' + data_user_id + '/friends/_' + ids).once('value').then(function (snapshot) {
                    to_conversation_id = snapshot.val();
                    if (to_conversation_id == null) {
                        friendlyChat.database.ref('Users/' + data_user_id + '/conversations').once('value').then(function (snaps) {
                            var list_conversations = snaps.val();
                            for (var key in list_conversations) {
                                if (list_conversations.hasOwnProperty(key)) {
                                    if (list_conversations[key].user_id == '_' + ids) {
                                        to_conversation_id = key;
                                        break;
                                    }
                                }
                            }
                            if (to_conversation_id != null) {
                                $(chat_key).attr("data-cid", to_conversation_id);
                                args_chat = {
                                    'chat_key_value': chat_key_value,
                                    'ids': ids,
                                    'name': name,
                                    'picture': picture,
                                    'conversation_id': to_conversation_id,
                                    'multiple': multiple,
                                    'name_list': name_list
                                };

                                // Process lấy ra nội dung chat
                                get_messages(args_chat);
                            }
                        });
                    }
                });
            }
        } else {
            // var data = {'ids': ids, 'conversation_id': conversation_id};
            args_chat = {
                'chat_key_value': chat_key_value,
                'ids': ids,
                'name': name,
                'picture': picture,
                'conversation_id': conversation_id,
                'multiple': multiple,
                'name_list': name_list
            };
            // Process lấy ra nội dung chat
            get_messages(args_chat);
        }

    } else {
        /* open chat-box with that chat_key that already exists if not opened */
        if (!$(chat_key).hasClass('opened')) {
            $(chat_key).addClass('opened').find('.chat-widget-content').toggle();
        }
        $(chat_key).find('textarea').focus();
        /* reconstruct chat-widgets */
        reconstruct_chat_widgets();
    }
}


function chat_panel(ids, name, picture, conversation_id, uname) {
    var container = document.getElementsByClassName("js_conversation-container");
    var panel_chat = container[0].querySelector(".panel-messages");
    panel_chat.setAttribute('data-cid', conversation_id);
    panel_chat.setAttribute('data-uid', ids);
    panel_chat.querySelector(".mt5").innerHTML =
        '<span class="js_user-popover" data-uid="' + ids + '">' +
        '<a href="' + site_path + '/' + uname + '">' +
        name +
        '</a>' +
        '</span>';
    var display = panel_chat.querySelector(".panel-body .chat-conversations ul");
    display.innerHTML = '';

    var conversation_data = friendlyChat.database.ref().child('Conversations/' + conversation_id);
    var conversation_info = conversation_data.child('conversationInfo');
    /* Lấy danh sách messages */
    var list_messages = conversation_data.child('messages');

    // Make sure we remove all previous listeners.
    list_messages.off();

    // Lấy ra danh sách messages và lắng nghe thay đổi
    var getMessages = function (data) {
        var messages = data.val();

        /* Hiển thị nội dung chat lên view */
        if (panel_chat.getAttribute('data-cid') == conversation_id) {
            if (display) {
                var container = document.createElement('li');
                container.innerHTML = FriendlyChat.MESSAGE_TEMPLATE;

                var conv = container.querySelector('.conversation');
                var conv_user = container.querySelector('.conversation-user');
                var conv_body = container.querySelector('.conversation-body');
                var textElement = conv_body.querySelector('.text');
                var timeElement = conv_body.querySelector('.time');
                if (data_user_id == messages.userId) {
                    conv.className += ' right';
                    conv_user.innerHTML = '<a href="' + site_path + '/' + data_user_name + '">' +
                        '<img src="' + data_user_picture + '" title="' + data_user_fullname + '" alt="' + data_user_fullname + '">' +
                        '</a>';
                } else {
                    conv_user.innerHTML = '<a href="' + site_path + '/' + uname + '">' +
                        '<img src="' + picture + '" title="' + name + '" alt="' + name + '">' +
                        '</a>';
                }
                conv.setAttribute('id', data.key);
                timeElement.setAttribute('data-time', messages.time);
                timeElement.textContent = messages.time;
                textElement.textContent = messages.content;

                display.appendChild(container);
            }

            /* scroll chat box xuống vị trí cuối cùng */
            $('.panel-messages').find(".js_scroller").slimScroll({scrollTo: $('.panel-messages').find(".js_scroller").prop("scrollHeight") + "px"});

        }

    }.bind(this);

    /* Lắng nghe khi có thay đổi */
    list_messages.on('child_added', getMessages);
    list_messages.on('child_changed', getMessages);
}

// Lấy ra messages và hiển thị lên chat_box
function get_messages(args_chat) {
    var conversation_data = friendlyChat.database.ref().child('Conversations/' + args_chat.conversation_id);
    /* Lấy ra thông tin cuộc trò chuyện */
    var conversation_info = conversation_data.child('conversationInfo');
    /* Lấy ra danh sách người tham gia */
    var conversation_participants = conversation_info.child('participants');
    /* Lấy danh sách messages */
    var list_messages = conversation_data.child('messages');
    // Make sure we remove all previous listeners.
    list_messages.off();
    conversation_info.off();

    var participants_info = {};
    // Lấy ra thông tin người tham gia và lắng nghe thay đổi
    var participants_added = function (data) {
        var friends_data = friendlyChat.database.ref().child('Users/' + data.key);
        friends_data.on('value', function (snap) {
            var friend_data = snap.val();
            if (!participants_info.hasOwnProperty(data.key) ||
                participants_info[data.key].avatar != friend_data.avatar ||
                participants_info[data.key].username != friend_data.username ||
                participants_info[data.key].fullName != friend_data.fullName) {

                participants_info[data.key] = snap.val();

                var div = document.getElementById(args_chat.chat_key_value);
                if (div) {
                    var user_conv = div.getElementsByClassName('uid' + data.key);
                    for (var i = 0; i < user_conv.length; i++) {
                        user_conv[i].innerHTML = '<a href="' + site_path + '/' + friend_data.username + '">' +
                            '<img src="' + friend_data.avatar + '" title="' + friend_data.fullName + '" alt="' + friend_data.fullName + '">' +
                            '</a>';
                    }
                }
            }

        });

    }.bind(this);
    /* Lắng nghe khi có thay đổi */
    conversation_participants.on('child_added', participants_added);

    // Lấy ra danh sách messages và lắng nghe thay đổi
    var getMessages = function (data) {
        var messages = data.val();
        /* Hiển thị nội dung chat lên view */
        var div = document.getElementById(args_chat.chat_key_value);
        if (div) {
            var display = div.querySelector('.chat-conversations ul');
            if (display) {
                var container = document.createElement('li');
                container.innerHTML = FriendlyChat.MESSAGE_TEMPLATE;

                var conv = container.querySelector('.conversation');
                var conv_user = container.querySelector('.conversation-user');
                var conv_body = container.querySelector('.conversation-body');
                var textElement = conv_body.querySelector('.text');
                var timeElement = conv_body.querySelector('.time');
                if (data_user_id == messages.userId) {
                    conv.className += ' right';
                }

                var isPass = false;
                for (var key in participants_info) {
                    if (participants_info.hasOwnProperty(key)) {
                        if (key == messages.userId) {
                            conv_user.className += ' uid' + messages.userId;
                            conv_user.innerHTML = '<a href="' + site_path + '/' + participants_info[key].username + '">' +
                                '<img src="' + participants_info[key].avatar + '" title="' + participants_info[key].fullName + '" alt="' + participants_info[key].fullName + '">' +
                                '</a>';
                            isPass = true;
                            break;
                        }
                    }
                }

                if (!isPass) {
                    var friend = friendlyChat.database.ref('Users/' + messages.userId);
                    friend.once('value').then(function (f_snap) {
                        var friend_info = f_snap.val();
                        var f_avatar = (friend_info.avatar !== '') ? friend_info.avatar : avatar_default_path;
                        conv_user.className += ' uid' + messages.userId;
                        conv_user.innerHTML = '<a href="' + site_path + '/' + friend_info.username + '">' +
                            '<img src="' + f_avatar + '" title="' + friend_info.fullName + '" alt="' + friend_info.fullName + '">' +
                            '</a>';
                    });
                }

                // conv.setAttribute('id', data.key);
                timeElement.setAttribute('data-time', messages.time);
                timeElement.textContent = messages.time;
                textElement.textContent = messages.content;

                display.appendChild(container);
            }

            /* scroll chat box xuống vị trí cuối cùng */
            // $(chat_key).find(".js_scroller").html(response.messages).slimScroll({scrollTo : $(chat_key).prop("scrollHeight") + "px"});
            $("#" + args_chat.chat_key_value).find(".js_scroller").slimScroll({scrollTo: $("#" + args_chat.chat_key_value).find(".js_scroller").prop("scrollHeight") + "px"});
        }
    }.bind(this);

    /* Lắng nghe khi có thay đổi */
    list_messages.on('child_added', getMessages);
    // list_messages.on('child_changed', getMessages);


    // Xử lý chat-box khi opened
    var last_message = list_messages.limitToLast(1);
    last_message.on('value', function (snap) {

        if (window.location.pathname.indexOf("messages") == -1 || $('.panel-messages[data-cid="' + args_chat.conversation_id + '"]').length == 0) {
            var chat_box_widget = $("#" + args_chat.chat_key_value);
            chat_box_widget.find(".js_scroller").slimScroll({scrollTo: chat_box_widget.find(".js_scroller").prop("scrollHeight") + "px"});

            // Hiện count chat ở chat box
            var box = document.getElementById(args_chat.chat_key_value);
            if (box) {
                var last_mess = snap.val();
                for (var key in last_mess) {
                    if (last_mess.hasOwnProperty(key)) {
                        // Kiểm tra tin nhắn mới không phải của mình
                        if (last_mess[key].userId != data_user_id) {
                            if (!chat_box_widget.hasClass("opened")) {
                                chat_box_widget.addClass("new").find(".js_chat-box-label").text("1");
                                $("#chat_sound")[0].play();
                            } else {
                                setIsSeen(args_chat);
                            }
                        }
                        break;
                    }
                }

            }
        }
    });

}

function setIsSeen(args) {
    friendlyChat.database.ref('Users/' + data_user_id + '/countChat').once('value').then(function (snaps) {
        var updates = {};
        updates['Conversations/' + args.conversation_id + '/conversationInfo/participants/' + data_user_id + '/isSeen'] = 1;

        var countChat = snaps.val();
        if (countChat > 0)
            updates['Users/' + data_user_id + '/countChat'] = countChat - 1;

        friendlyChat.database.ref().update(updates);
    });
}

function send_messages(args) {
    var participants = args['recipients'];
    // A post entry.
    var messageData = {
        content: args.message,
        time: args.curTime,
        type: 'text',
        userId: data_user_id
    };

    var messageInfo = {
        lastMessage: args.message,
        lastTimeUpdated: args.curTime
    };

    // Get a key for a new Messages.
    var newMessageKey = friendlyChat.database.ref().child('Conversations/' + args.conversation_id + '/messages').push().key;
    var updates = {};
    updates['Conversations/' + args.conversation_id + '/messages/' + newMessageKey] = messageData;
    updates['Conversations/' + args.conversation_id + '/conversationInfo/lastMessage'] = messageInfo.lastMessage;
    updates['Conversations/' + args.conversation_id + '/conversationInfo/lastTimeUpdated'] = messageInfo.lastTimeUpdated;

    updates['Conversations/' + args.conversation_id + '/conversationInfo/participants/' + data_user_id + '/isSeen'] = 1;

    if (Array.isArray(participants)) {
        var part_length = participants.length;
        for (var i = 0; i < part_length; i++) {
            if ( participants[i] != data_user_id) {

                $.post(api['messages/notification'], {
                    'friend_id': participants[i],
                    'conversation_id': args.conversation_id,
                    'user_fullname': data_user_fullname,
                    'message': args.message,
                    'count_chat': 1
                },function(response) {

                }, 'json');

                updates['Conversations/' + args.conversation_id + '/conversationInfo/participants/' + participants[i] + '/isSeen'] = 0;

                friendlyChat.database.ref('Conversations/' + args.conversation_id + '/conversationInfo/participants/' + participants[i] ).once('value').then(function (snapshot) {
                    var part_info = snapshot.val();
                    var fId = snapshot.key;

                    if (part_info.isLeave == 0 && part_info.isSeen == 1) {
                        friendlyChat.database.ref('Users/' + fId).once('value').then(function (snaps) {
                            var info = snaps.val();
                            updates['Users/' + snaps.key + '/countChat'] = info.countChat + 1;

                            if (fId == participants[part_length - 1])
                                friendlyChat.database.ref().update(updates);
                        });
                    } else {
                        if (fId == participants[part_length - 1])
                            friendlyChat.database.ref().update(updates);
                    }
                });
            }
        }
    } else {
        friendlyChat.database.ref('Conversations/' + args.conversation_id + '/conversationInfo/participants').once('value').then(function (snap) {

            var part = snap.val();
            var part_length = Object.keys(part).length;

            var i = 1;

            for (var key in part) {
                if (part.hasOwnProperty(key) && key != data_user_id) {

                    $.post(api['messages/notification'], {
                        'friend_id': key.replace('_', ''),
                        'conversation_id': args.conversation_id,
                        'user_fullname': data_user_fullname,
                        'message': args.message,
                        'count_chat': 1
                    },function(response) {

                    }, 'json');

                    if (part[key].isLeave == 0 && part[key].isSeen == 1) {
                        friendlyChat.database.ref('Users/' + key).once('value').then(function (snaps) {
                            var info = snaps.val();
                            updates['Users/' + snaps.key + '/countChat'] = info.countChat + 1;

                            if (i == part_length - 1) {
                                friendlyChat.database.ref().update(updates);
                            }
                            i++;
                        });
                    } else {
                        if (i == part_length - 1) {
                            friendlyChat.database.ref().update(updates);
                        }
                        i++;
                    }

                }

            }
        });
    }

}

function compareArrays(arr1, arr2) {
    return $(arr1).not(arr2).length == 0 && $(arr2).not(arr1).length == 0;
}

function createConversation(args) {
    var participants = args['recipients'];
    var part_length = participants.length;
    var nameList = args['nameList'];

    var update_conversation = {};
    update_conversation['Conversations/' + args.conversation_id + '/conversationInfo/avatarGroup'] = '';
    update_conversation['Conversations/' + args.conversation_id + '/conversationInfo/isGroup'] = (part_length > 1) ? 1 : 0;
    update_conversation['Conversations/' + args.conversation_id + '/conversationInfo/nameGroup'] = (part_length > 1) ? args.nameConversation : '';
    update_conversation['Conversations/' + args.conversation_id + '/conversationInfo/lastMessage'] = args.message;
    update_conversation['Conversations/' + args.conversation_id + '/conversationInfo/lastTimeUpdated'] = args.curTime;
    update_conversation['Conversations/' + args.conversation_id + '/conversationInfo/participants/' + data_user_id + '/fromMessageKey'] = '';
    update_conversation['Conversations/' + args.conversation_id + '/conversationInfo/participants/' + data_user_id + '/isAdmin'] = 0;
    update_conversation['Conversations/' + args.conversation_id + '/conversationInfo/participants/' + data_user_id + '/isLeave'] = 0;
    update_conversation['Conversations/' + args.conversation_id + '/conversationInfo/participants/' + data_user_id + '/isSeen'] = 1;
    update_conversation['Conversations/' + args.conversation_id + '/conversationInfo/participants/' + data_user_id + '/isTyping'] = 0;
    update_conversation['Conversations/' + args.conversation_id + '/conversationInfo/participants/' + data_user_id + '/name'] = data_user_fullname;

    update_conversation['Users/' + data_user_id + '/conversations/' + args.conversation_id + '/isDelete'] = 0;
    update_conversation['Users/' + data_user_id + '/conversations/' + args.conversation_id + '/user_id'] = (part_length > 1) ? '' : participants[0];

    for (var i = 0; i < part_length; i++) {
        update_conversation['Conversations/' + args.conversation_id + '/conversationInfo/participants/' + participants[i] + '/fromMessageKey'] = '';
        update_conversation['Conversations/' + args.conversation_id + '/conversationInfo/participants/' + participants[i] + '/isAdmin'] = 0;
        update_conversation['Conversations/' + args.conversation_id + '/conversationInfo/participants/' + participants[i] + '/isSeen'] = 0;
        update_conversation['Conversations/' + args.conversation_id + '/conversationInfo/participants/' + participants[i] + '/isTyping'] = 0;
        update_conversation['Conversations/' + args.conversation_id + '/conversationInfo/participants/' + participants[i] + '/name'] = (nameList != undefined) ? nameList[participants[i]] : '';

        update_conversation['Users/' + participants[i] + '/conversations/' + args.conversation_id + '/isDelete'] = 0;
        update_conversation['Users/' + participants[i] + '/conversations/' + args.conversation_id + '/user_id'] = (part_length > 1) ? '' : data_user_id;
    }

    friendlyChat.database.ref().update(update_conversation);
}

$(function () {

    // start new chat-box
    $('body').on('click', '.js_chat-start', function (e) {
        if(!chat_enabled) return;
        e.preventDefault();
        /* get data from (header conversation feeds || master online widget) */
        /* mandatory  */
        var uid = $(this).attr('data-uid');
        var name = $(this).attr('data-name');
        var picture = $(this).attr('data-picture');
        /* optional (case: conversation feeds) */
        var conversation_id = $(this).attr('data-cid') || false;
        var multiple = ($(this).attr('data-multiple') == "true")? true: false;
        var name_list = $(this).attr('data-name-list') || name;
        /* check if (create a new conversation || load previous conversation) */
        if (uid != undefined) {
            /* load chat-box */
            chat_box(uid, name, picture, conversation_id, multiple, name_list);
        } else if (conversation_id) {
            /* load previous conversation */
            /* check if the viewer in the messages page & open already conversation */
            if(window.location.pathname.indexOf("messages") != -1) {
                $(".js_conversation-container").html('<div class="loader loader_medium pr10"></div>');

                var container = document.getElementsByClassName("js_conversation-container");
                var panel_chat = container[0].querySelector(".panel-messages");
                var data_cid = panel_chat.getAttribute('data-cid');
                if (conversation_id != data_cid)
                    chat_panel(0, name, picture, conversation_id, multiple, name_list);
            } else {
                /* load chat-box */
                chat_box(0, name, picture, conversation_id, multiple, name_list);
            }
        } else {
            /* open fresh chat-box */
            /* check if there is any fresh chat-box already exists */
            if($('.chat-box.fresh').length == 0) {
                /* construct a new one */
                $('body').append(render_template('#chat-box-new'));
                $('.chat-box.fresh').find('.chat-widget-content').toggle();
                $('.chat-box.fresh').find('.chat-to input').focus();
                /* initialize the main plugins */
                initialize();
                /* reconstruct chat-widgets */
                reconstruct_chat_widgets();
            } else {
                /* open fresh chat-box that already exists if not opened */
                if(!$('.chat-box.fresh').hasClass('opened')) {
                    $('.chat-box.fresh').addClass('opened');
                    $('.chat-box.fresh').find('.chat-widget-content').toggle();
                }
                $('.chat-box.fresh').find('.chat-to input').focus();
            }
        }

    });


    // post message
    $('body').on('keydown', '.js_post-message', function (event) {
        if (event.keyCode == 13 && event.shiftKey == 0) {
            event.preventDefault();
            var _this = $(this);
            var message = _this.val();
            var widget = _this.parents('.chat-widget, .panel-messages');
            var conversation_id = widget.attr('data-cid');
            var attachments = widget.find('.chat-attachments');
            // current time
            var curTime = moment.utc().local().format("YYYY-MM-DD H:mm:ss");
            /* check if there is current (sending) process */
            if(widget.data('sending')) {
                return false;
            }
            /* get photo from widget data */
            var photo = widget.data('photo');
            /* check if message is empty */
            if(is_empty(message) && !photo) {
                return;
            }
            /* check if posting the message to (new || existed) conversation */
            if (widget.hasClass('fresh')) {
                /* post the message to -> a new conversation */
                /* check recipients */
                if(widget.find('.tags li').length == 0) {
                    return;
                }
                /* get recipients */
                var recipients = [];
                var nameG = '';
                var nameList = {};
                $.each(widget.find('.tags li'), function (i, tag) {
                    nameList['_' + $(tag).attr('data-uid')] = $(tag).text().replace("×","");
                    nameG = nameG + $(tag).text() + ', ';
                    recipients.push('_' + $(tag).attr('data-uid'));
                });
                nameG = nameG.slice(0, -3);
                nameG = nameG.replace(/×/g, "");

                var data = {'message': message, 'photo': photo, 'recipients': recipients, 'nameConversation': nameG, 'nameList': nameList};
            } else {
                if (conversation_id === undefined) {
                    /* post the message to -> a new conversation */
                    /* get recipients */
                    var recipients = [];
                    recipients.push('_' + widget.attr('data-uid'));
                    var data = {'message': message, 'photo': JSON.stringify(photo), 'recipients': recipients};
                } else {
                    /* post the message to -> already existed conversation */
                    var data = {'message': message, 'photo': JSON.stringify(photo), 'conversation_id': conversation_id};
                }
            }
            data.curTime = curTime;
            /* add currenet sending process to widget data */
            widget.data('sending', true);

            if (data.conversation_id === undefined) {
                var isExist = false;
                // kiển tra xem đã tồn tại cuộc trò chuyện chưa
                for (var k in chat_participants) {
                    if (chat_participants.hasOwnProperty(k)) {
                        if (compareArrays(data.recipients, chat_participants[k])) {
                            isExist = true;
                            data.conversation_id = k;
                            break;
                        }
                    }
                }
                // Nếu chưa tồn tại cuộc trò chuyện thì tạo mới
                if (!isExist) {
                    // Get a key for a new Conversations.
                    data.conversation_id = friendlyChat.database.ref().child('Conversations').push().key;
                    createConversation(data);
                }
                widget.attr("data-cid", data.conversation_id);
            }

            if (widget.hasClass('fresh')) {
                if (window.location.pathname.indexOf("messages") != -1) {
                    window.location.replace(site_path + '/messages/' + data.conversation_id);
                } else {
                    widget.remove();
                    chat_box(0, data.nameConversation, '', data.conversation_id, true, data.nameList);
                    widget = $('#chat_' + data.conversation_id);
                    send_messages(data);
                }
            } else {
                if (conversation_id === undefined) {
                    var args_chat = {
                        'chat_key_value': widget.attr('id'),
                        'conversation_id': data.conversation_id
                    };
                    get_messages(args_chat);
                }

                send_messages(data);
                if(window.location.pathname.indexOf("messages") != -1) {
                    /* messages page */
                    _this.focus().val('').height(parseInt(_this.css('line-height'))*3);
                } else {
                    /* in messages page */
                    _this.focus().val('').height(_this.css('line-height'));
                }
            }

            widget.find(".js_scroller").slimScroll({scrollTo: widget.find(".js_scroller").prop("scrollHeight") + "px"});
            attachments.hide();
            attachments.find('li.item').remove();
            widget.removeData('photo');
            widget.find('.x-form-tools-attach').show();
            /* remove widget sending data */
            widget.removeData('sending');

        }
    });


    // post message
    $('body').on('keydown', '.js_post-message1', function (event) {
        if (event.keyCode == 13 && event.shiftKey == 0) {
            event.preventDefault();
            var _this = $(this);
            var message = _this.val();
            var widget = _this.parents('.chat-widget, .panel-messages');
            var conversation_id = widget.attr('data-cid');
            var to_user_id = '_' + widget.attr('data-uid');
            var attachments = widget.find('.chat-attachments');
            // current time
            var curTime = moment.utc().local().format("YYYY-MM-DD H:mm:ss");

            var args_chat = {};
            /* check if there is current (sending) process */
            if (widget.data('sending')) {
                return false;
            }
            /* get photo from widget data */
            var photo = widget.data('photo');
            /* check if message is empty */
            if (is_empty(message) && !photo) {
                return;
            }
            /* check if posting the message to (new || existed) conversation */
            // if (widget.hasClass('fresh')) {
            //     /* post the message to -> a new conversation */
            //     /* check recipients */
            //     if (widget.find('.tags li').length == 0) {
            //         return;
            //     }
            //     /* get recipients */
            //     // var recipients = [];
            //     $.each(widget.find('.tags li:first'), function (i, tag) {
            //         to_user_id = '_' + $(tag).attr('data-uid');
            //     });
            //     // var data = {
            //     //     'message': message,
            //     //     'photo': JSON.stringify(photo),
            //     //     'recipients': JSON.stringify(recipients)
            //     // };
            // } else {
            //     if (conversation_id === undefined) {
            //         to_user_id = '_' + widget.attr('data-uid');
            //         /* post the message to -> a new conversation */
            //         /* get recipients */
            //         // var recipients = [];
            //
            //         // var data = {
            //         //     'message': message,
            //         //     'photo': JSON.stringify(photo),
            //         //     'recipients': JSON.stringify(recipients)
            //         // };
            //     } else {
            //         /* post the message to -> already existed conversation */
            //         var data = {'message': message, 'photo': JSON.stringify(photo), 'conversation_id': conversation_id};
            //     }
            // }
            /* add currenet sending process to widget data */
            widget.data('sending', true);

            if (widget.hasClass('fresh')) {
                if (window.location.pathname.indexOf("messages") != -1) {
                    window.location.replace(site_path + '/messages/' + conversation_id);
                } else {
                    if (!isNaN(to_user_id)) {
                        var to_uid = '_' + to_user_id;
                        friendlyChat.database.ref('Users/' + to_uid).once('value').then(function (snapshot) {
                            var f_info = snapshot.val();
                            var cid = 0;
                            if ('friends' in f_info) {
                                var lst_friends = f_info['friends'];
                                if (data_user_id in lst_friends) {
                                    cid = lst_friends[data_user_id];
                                }
                            }
                            if (cid == 0) {
                                if ('conversations' in f_info) {
                                    var list_conv = f_info['conversations'];
                                    for (var key in list_conv) {
                                        if (list_conv.hasOwnProperty(key)) {
                                            if (list_conv[key].user_id == data_user_id) {
                                                cid = key;
                                            }
                                        }
                                    }
                                }
                            }

                            if (cid == 0) {
                                friendlyChat.database.ref('Users/' + to_user_id).once('value').then(function (snapshot) {
                                    var data = snapshot.val();

                                    if (data != null) {
                                        // Get a key for a new Conversations.
                                        conversation_id = friendlyChat.database.ref().child('Users/' + data_user_id + '/conversations').push().key;

                                        // $('#chat' + to_user_id).attr("data-cid", conversation_id);

                                        var update_conversation = {};
                                        update_conversation['Conversations/' + conversation_id + '/conversationInfo/avatarGroup'] = '';
                                        update_conversation['Conversations/' + conversation_id + '/conversationInfo/isGroup'] = 0;
                                        update_conversation['Conversations/' + conversation_id + '/conversationInfo/nameGroup'] = '';
                                        update_conversation['Conversations/' + conversation_id + '/conversationInfo/lastMessage'] = message;
                                        update_conversation['Conversations/' + conversation_id + '/conversationInfo/lastTimeUpdated'] = curTime;
                                        update_conversation['Conversations/' + conversation_id + '/conversationInfo/participants/' + data_user_id + '/fromMessageKey'] = '';
                                        update_conversation['Conversations/' + conversation_id + '/conversationInfo/participants/' + data_user_id + '/isAdmin'] = 0;
                                        update_conversation['Conversations/' + conversation_id + '/conversationInfo/participants/' + data_user_id + '/isSeen'] = 1;
                                        update_conversation['Conversations/' + conversation_id + '/conversationInfo/participants/' + data_user_id + '/isTyping'] = 0;
                                        update_conversation['Conversations/' + conversation_id + '/conversationInfo/participants/' + data_user_id + '/name'] = data_user_fullname;

                                        update_conversation['Conversations/' + conversation_id + '/conversationInfo/participants/' + to_user_id + '/fromMessageKey'] = '';
                                        update_conversation['Conversations/' + conversation_id + '/conversationInfo/participants/' + to_user_id + '/isAdmin'] = 0;
                                        update_conversation['Conversations/' + conversation_id + '/conversationInfo/participants/' + to_user_id + '/isSeen'] = 0;
                                        update_conversation['Conversations/' + conversation_id + '/conversationInfo/participants/' + to_user_id + '/isTyping'] = 0;
                                        update_conversation['Conversations/' + conversation_id + '/conversationInfo/participants/' + to_user_id + '/name'] = data.fullName;
                                        update_conversation['Users/' + data_user_id + '/conversations/' + conversation_id + '/isDelete'] = 0;
                                        update_conversation['Users/' + data_user_id + '/conversations/' + conversation_id + '/user_id'] = to_user_id;
                                        update_conversation['Users/' + to_user_id + '/conversations/' + conversation_id + '/isDelete'] = 0;
                                        update_conversation['Users/' + to_user_id + '/conversations/' + conversation_id + '/user_id'] = data_user_id;

                                        friendlyChat.database.ref().update(update_conversation);

                                        args_chat = {
                                            'chat_key_value': 'chat' + to_user_id,
                                            'ids': to_user_id,
                                            'name': data.fullName,
                                            'picture': data.avatar,
                                            'conversation_id': conversation_id,
                                            'uname': data.username
                                        };
                                        get_messages(args_chat, conversation_id);

                                        var args_send = {
                                            'message': message,
                                            'curTime': curTime,
                                            'conversation_id': conversation_id,
                                            'to_user_id': to_user_id
                                        };
                                        send_messages(args_send);
                                    }
                                });
                            }
                        });

                    }
                    // widget.remove();
                    // chat_box(response.ids, response.name, response.picture, response.conversation_id, response.multiple_recipients, response.name_list);
                    // widget = $('#chat_' + response.ids);
                }
            } else {

                if (conversation_id === undefined) {
                    // widget.attr("data-cid", response.conversation_id);
                    if (to_user_id != null) {
                        friendlyChat.database.ref('Users/' + to_user_id).once('value').then(function (snapshot) {
                            var data = snapshot.val();

                            if (data != null) {
                                // Get a key for a new Conversations.
                                conversation_id = friendlyChat.database.ref().child('Users/' + data_user_id + '/conversations/').push().key;

                                $('#chat' + to_user_id).attr("data-cid", conversation_id);

                                var update_conversation = {};

                                update_conversation['Conversations/' + conversation_id + '/conversationInfo/avatarGroup'] = '';
                                update_conversation['Conversations/' + conversation_id + '/conversationInfo/isGroup'] = 0;
                                update_conversation['Conversations/' + conversation_id + '/conversationInfo/nameGroup'] = '';
                                update_conversation['Conversations/' + conversation_id + '/conversationInfo/lastMessage'] = message;
                                update_conversation['Conversations/' + conversation_id + '/conversationInfo/lastTimeUpdated'] = curTime;
                                update_conversation['Conversations/' + conversation_id + '/conversationInfo/participants/' + data_user_id + '/fromMessageKey'] = '';
                                update_conversation['Conversations/' + conversation_id + '/conversationInfo/participants/' + data_user_id + '/isAdmin'] = 0;
                                update_conversation['Conversations/' + conversation_id + '/conversationInfo/participants/' + data_user_id + '/isSeen'] = 1;
                                update_conversation['Conversations/' + conversation_id + '/conversationInfo/participants/' + data_user_id + '/isTyping'] = 0;
                                update_conversation['Conversations/' + conversation_id + '/conversationInfo/participants/' + data_user_id + '/name'] = data_user_fullname;

                                update_conversation['Conversations/' + conversation_id + '/conversationInfo/participants/' + to_user_id + '/fromMessageKey'] = '';
                                update_conversation['Conversations/' + conversation_id + '/conversationInfo/participants/' + to_user_id + '/isAdmin'] = 0;
                                update_conversation['Conversations/' + conversation_id + '/conversationInfo/participants/' + to_user_id + '/isSeen'] = 0;
                                update_conversation['Conversations/' + conversation_id + '/conversationInfo/participants/' + to_user_id + '/isTyping'] = 0;
                                update_conversation['Conversations/' + conversation_id + '/conversationInfo/participants/' + to_user_id + '/name'] = data.fullName;

                                update_conversation['Users/' + data_user_id + '/conversations/' + conversation_id + '/isDelete'] = 0;
                                update_conversation['Users/' + data_user_id + '/conversations/' + conversation_id + '/user_id'] = to_user_id;
                                update_conversation['Users/' + to_user_id + '/conversations/' + conversation_id + '/isDelete'] = 0;
                                update_conversation['Users/' + to_user_id + '/conversations/' + conversation_id + '/user_id'] = data_user_id;

                                friendlyChat.database.ref().update(update_conversation);

                                args_chat = {
                                    'chat_key_value': 'chat' + to_user_id,
                                    'ids': to_user_id,
                                    'name': data.fullName,
                                    'picture': data.avatar,
                                    'conversation_id': conversation_id,
                                    'uname': data.username
                                };
                                get_messages(args_chat, conversation_id);

                                var args_send = {
                                    'message': message,
                                    'curTime': curTime,
                                    'conversation_id': conversation_id,
                                    'to_user_id': to_user_id
                                };
                                send_messages(args_send);
                            } else {
                                modal('#modal-message', {
                                    title: __['Error'],
                                    message: __['There is something that went wrong!']
                                });
                            }
                        });
                    } else {
                        modal('#modal-message', {
                            title: __['Error'],
                            message: __['There is something that went wrong!']
                        });
                    }
                } else {
                    // function post messages
                    var args_send = {
                        'message': message,
                        'curTime': curTime,
                        'conversation_id': conversation_id,
                        'to_user_id': to_user_id
                    };

                    send_messages(args_send);
                }
                if (window.location.pathname.indexOf("messages") != -1) {
                    /* messages page */
                    _this.focus().val('').height(parseInt(_this.css('line-height')) * 3);
                } else {
                    /* in messages page */
                    _this.focus().val('').height(_this.css('line-height'));
                }
            }


            // widget.find(".js_scroller ul").append(render_template('#chat-message', {'message': response.message, 'image': response.image, 'id': response.last_message_id, 'time': moment.utc().format("YYYY-MM-DD H:mm:ss")}));
            widget.find(".js_scroller").slimScroll({scrollTo: widget.find(".js_scroller").prop("scrollHeight") + "px"});
            attachments.hide();
            attachments.find('li.item').remove();
            widget.removeData('photo');
            widget.find('.x-form-tools-attach').show();
            /* remove widget sending data */
            widget.removeData('sending');
        }
    });
    /* chat attachment remover */
    $('body').on('click', '.js_chat-attachment-remover', function () {
        var widget = $(this).parents('.chat-widget, .panel-messages');
        var attachments = widget.find('.chat-attachments');
        var item = $(this).parents('li.item');
        /* remove the attachment from widget data */
        widget.removeData('photo');
        /* remove the attachment item */
        item.remove();
        /* hide attachments */
        attachments.hide();
        /* show widget form tools */
        widget.find('.x-form-tools-attach').show();
    });

    // Reset số lượng conut_chat ở header
    $('body').on('click', '.js_live-messages', function () {
        var _this = $(this);
        var counter = parseInt(_this.find("span.label").text()) || 0;
        if (!$(this).hasClass('open') && counter > 0) {
            /* reset the client counter & hide it */
            _this.find("span.label").addClass('hidden').text('0');
            /* get the reset target */
            if (_this.hasClass('js_live-messages')) {
                /* reset the server counter */
                var updates = {};
                updates['Users/' + data_user_id + '/countChat'] = 0;
                friendlyChat.database.ref().update(updates);
            }
        }
    });

    // toggle chat-widget
    $('body').on('click', '.chat-widget-head', function () {
        var widget = $(this).parents('.chat-widget');
        /* check if widget is master & chat disabled */
        if (widget.hasClass('js_chat-widget-master') && $('body').attr('data-chat-enabled') != 1) {
            return;
        }
        /* check if close || open */
        if (widget.hasClass('opened')) {
            /* close */
            widget.removeClass('opened');
            /* rearrange chat-widgets if widget is chat-box && screen width >= 768px */
            if ($(window).width() >= 751 && widget.hasClass('chat-box')) {
                widget.nextAll('.chat-box').each(function (index) {
                    var right = parseInt($(this).css('right')) - 100;
                    /* 100px is the increased value in width of opened chat-widget */
                    $(this).css('right', right);
                });
            }
        } else {
            /* open */
            if ($(window).width() >= 751) {
                /* screen width >= 768px */
                /* rearrange chat-widgets if widget is chat-box */
                if (widget.hasClass('chat-box')) {
                    widget.nextAll('.chat-box').each(function (index) {
                        var right = parseInt($(this).css('right')) + 100;
                        $(this).css('right', right);
                    });
                }
            } else {
                /* screen width < 768px */
                /* close all opened chat-boxes */
                $('.chat-widget-content').hide();
                $('.chat-widget').removeClass('opened');
            }
            widget.addClass('opened');
        }
        /* toggle widget content */
        widget.find('.chat-widget-content').toggle();
        /* scroll to latest message if has class new */
        if (widget.hasClass('new')) {
            widget.find(".js_scroller").slimScroll({scrollTo: widget.find(".js_scroller").prop("scrollHeight") + "px"});
        }
        /* remove class "new" if any */
        if (widget.hasClass('new')) {
            widget.removeClass('new');
        }
    });

    // close chat-widgets when click chat-close
    $('body').on('click', '.js_chat-box-close', function () {
        var widget = $(this).parents('.chat-widget');
        widget.remove();
        /* reconstruct chat-widgets */
        reconstruct_chat_widgets();
        /* unset from session */
        if (widget.attr('data-cid') !== undefined) {
            $.post(api['conversation/reaction'], {
                'do': 'close',
                'conversation_id': widget.attr('data-cid')
            }, function (response) {
                if (response.callback) {
                    eval(response.callback);
                }
            }, 'json')
                .fail(function () {
                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        }
    });

    // delete conversation
    $('body').on('click', '.js_delete-conversation', function () {
        confirm(__['Delete Conversation'], __['Are you sure you want to delete this conversation?'], function () {
            $.post(api['conversation/reaction'], {
                'do': 'delete',
                'conversation_id': $('.panel-messages').attr('data-cid')
            }, function (response) {
                if (response.callback) {
                    eval(response.callback);
                }
            }, 'json')
                .fail(function () {
                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    // adjust x-form-tools position for chat-form
    $(document).on('keyup', '.chat-form textarea', function () {
        var scrollHeight = $(this).parent()[0].scrollHeight;
        var clientHeight = $(this).parent()[0].clientHeight;
        var direction = ($('html').attr('dir') === undefined || $('html').attr('dir') == 'LTR') ? 'right' : 'left';
        if (scrollHeight > clientHeight) {
            $(this).parents('.chat-form').find('.x-form-tools').css(direction, '21px');
        } else {
            $(this).parents('.chat-form').find('.x-form-tools').css(direction, '7px');
        }
    });

    // reconstruct chat widgets when window load & resize
    $(window).bind("resize", function () {
        reconstruct_chat_widgets();
    });

    $('body').on('keyup', '.chat-sidebar-filter', function (event) {
        var search = $(this).val().toLowerCase();

        $('.chat-sidebar-content ul > li').each(function () {
            var item = $(this).text().toLowerCase();
            (item.indexOf(search) != -1) ? $(this).show() : $(this).hide();
        });
    });

    $('.dropdown-menu.dropdown-menu-checkbox > li > a')
        .each(function (index) {
            if (!$('> .fa', this).length) {
                $(this).prepend('<i class="fa fa-check dropdown-item-check"></i>');
            } else {
                if ($(this).parent().hasClass('dropdown-item-checkbox')) {
                    $('> .fa', this).addClass('dropdown-item-check');
                }
            }
        })
        .click(function (e) {
            var item = $(this).parent();
            if (item.hasClass('dropdown-item-checkbox')) {
                item.toggleClass('checked');
                var isChecked = item.hasClass('checked') ? 1 : 0;

                if (item.data('param')) {
                    var data = [];
                    data[item.data('param')] = isChecked;

                    $.post(api['chat/settings'], $.extend({}, data), function (response) {
                        if (response.success) {
                            if (item.hasClass('checked')) {
                                $('.chat-sidebar-content').removeClass('chat-sidebar-disabled');
                            } else {
                                $('.chat-sidebar-content').addClass('chat-sidebar-disabled');
                            }

                            $('body').attr('data-chat-enabled', isChecked);

                        } else {
                            modal('#modal-message', {
                                title: __['Error'],
                                message: __['There is something that went wrong!']
                            });
                        }
                    }, 'json')
                        .fail(function () {
                            modal('#modal-message', {
                                title: __['Error'],
                                message: __['There is something that went wrong!']
                            });
                        });
                }
            }
        });

});


/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

// Initializes FriendlyChat.
function FriendlyChat() {
    this.checkSetup();
    this.initFirebase();

    this.loadChatList();
    this.loadHeaderMessages();
    this.loadCountChat();
    this.setUserStatus();
}

// Sets up shortcuts to Firebase features and initiate firebase auth.
FriendlyChat.prototype.initFirebase = function () {
    // TODO(DEVELOPER): Initialize Firebase.
    // Shortcuts to Firebase SDK features.
    this.database = firebase.database();
    this.storage = firebase.storage();
    // Initiates Firebase auth and listen to auth state changes.
};

// Template for messages.
FriendlyChat.ITEM_TEMPLATE =
    '<li class="feeds-item">' +
    '<div class="data-container clickable small js_chat-start">' +
    '<img class="data-avatar">' +
    '<div class="data-content">' +

    '</div>' +
    '</div>' +
    '</li>';

// Template for messages.
FriendlyChat.ITEM_HEADER_TEMPLATE =
    '<li class="feeds-item">' +
    '<a class="data-container js_chat-start">' +
    '<img class="data-avatar" alt="">' +
    '<div class="data-content">' +

    '</div>' +
    '</a>' +
    '</li>';

// Template for messages.
FriendlyChat.MESSAGE_TEMPLATE =
    '<div class="conversation clearfix">' +
    '<div class="conversation-user">' +

    '</div>' +
    '<div class="conversation-body">' +
    '<div class="text">' +

    '</div>' +
    '<div class="time js_moment">' +

    '</div>' +
    '</div>' +
    '</div>';

// Loads chat list and listens for upcoming ones.
FriendlyChat.prototype.loadChatList = function () {
    var users = this.database.ref().child('Users');
    this.user_friends = users.child(data_user_id + '/friends');

    // Reference to the /messages/ database path.
    // Make sure we remove all previous listeners.
    this.user_friends.off();

    // Lấy ra danh sách bạn bè và lắng nghe thay đổi
    var getFriends = function (data) {
        /* friend_id = data.key và conversation_id = data.val() */
        /* Lấy thông tin của bạn bè */
        var list_friends = users.child(data.key);
        console.log('list_friends = ',list_friends);
        /* Hiển thị thông tin bạn bè lên view */
        list_friends.on('value', function (snapshot) {
            var friend_info = snapshot.val();
            console.log('friend_info = ',friend_info);
            if (friend_info.fullName) {
                var uid = snapshot.key.replace('_', '');
                var div = document.getElementById('item_' + uid);
                // If an element for that message does not exists yet we create it.
                if (div === null) {
                    var container = document.createElement('item_' + uid);
                    container.innerHTML = FriendlyChat.ITEM_TEMPLATE;
                    div = container.firstChild;
                    div.setAttribute('id', 'item_' + uid);

                    var itemChat = document.getElementById('item_chat');
                    itemChat.appendChild(div);
                }

                var containerElement = div.querySelector('.data-container');
                var avatarElement = div.querySelector('.data-avatar');
                var contentElement = div.querySelector('.data-content');

                containerElement.setAttribute('data-uid', uid);
                containerElement.setAttribute('data-name', friend_info.fullName);
                containerElement.setAttribute('data-cid', data.val());

                containerElement.setAttribute('data-picture', friend_info.avatar);
                avatarElement.setAttribute('src', friend_info.avatar);
                avatarElement.setAttribute('onerror', "this.src=avatar_default_path;");
                avatarElement.setAttribute('alt', friend_info.fullName);

                var item = document.getElementById('chat_' + uid);
                console.log('item = ',item);
                if (friend_info.isOnline) {
                    if (item) {
                        $('#chat_' + uid).find(".js_chat-box-status").removeClass("fa-user-secret").addClass("fa-circle");
                    }
                    contentElement.innerHTML =
                        '<div class="pull-right flip">' +
                        '<i class="fa fa-circle chat-sidebar-online"></i>' +
                        '</div> ' +
                        '<div>' +
                        '<strong>' + friend_info.fullName + '</strong>' +
                        '</div>';
                } else {
                    if (item) {
                        $('#chat_' + uid).find(".js_chat-box-status").removeClass("fa-circle").addClass("fa-user-secret");
                    }
                    contentElement.innerHTML =
                        '<div class="pull-right flip">' +
                        '<i class="fa fa-circle chat-sidebar-offline"></i>' +
                        '</div> ' +
                        '<div>' +
                        '<strong>' + friend_info.fullName + '</strong><br>' +
                        '<small>' +
                        'Online <span class="js_moment" data-time="' + friend_info.lastLogin + '" title="' + friend_info.lastLogin + '"></span> ' +
                        '</small>' +
                        '</div>';
                }
            }
        });
    }.bind(this);
    /* Lắng nghe khi có thay đổi */
    this.user_friends.on('child_added', getFriends);
    this.user_friends.on('child_changed', getFriends);
    this.user_friends.on('child_removed', getFriends);
};

FriendlyChat.prototype.setUserStatus = function () {
    // since I can connect from multiple devices or browser tabs, we store each connection instance separately
    // any time that connectionsRef's value is null (i.e. has no children) I am offline
    var myConnectionsRef = this.database.ref('Users/' + data_user_id + '/isOnline');
    // stores the timestamp of my last disconnect (the last time I was seen online)
    var lastOnlineRef = this.database.ref('Users/' + data_user_id + '/lastLogin');

    var connectedRef = this.database.ref('.info/connected');
    connectedRef.on('value', function (snap) {
        if (snap.val() === true) {
            // We're connected (or reconnected)! Do anything here that should happen only if online (or on reconnect)
            myConnectionsRef.set(1);
            // when I disconnect, remove this device
            myConnectionsRef.onDisconnect().set(0);
            // when I disconnect, update the last time I was seen online
            lastOnlineRef.onDisconnect().set(moment.utc().local().format("YYYY-MM-DD H:mm:ss"));
        }
    });
};

FriendlyChat.prototype.loadCountChat = function () {
    var user_count_chat = this.database.ref('Users/' + data_user_id + '/countChat');
    user_count_chat.on("value", function (snapshot) {
        var count_chat = snapshot.val();
        var nav = document.getElementsByClassName("navbar-container");
        var value_display = nav[0].querySelector(".js_live-messages .dropdown-toggle .label");

        if (count_chat != 0) {
            value_display.textContent = count_chat;
            value_display.className = "label";
        } else {
            value_display.className = "label hidden";
        }
    });

};


// Lấy ra danh sách các conversation (friends + unfriends)
FriendlyChat.prototype.loadHeaderMessages = function () {
    var conversations = this.database.ref('Users/' + data_user_id + '/conversations');

    conversations.on('value', function (fsnap) {
        var all_conversations = [];
        var list_conversations = [];
        fsnap.forEach(function (childFSnap) {
            var conv = childFSnap.val();
            all_conversations.push(childFSnap.key);
            if (conv.isDelete == 0) {
                list_conversations.push(childFSnap.key);
            }
        });
        getConversationInfo(list_conversations);
        getAllParticipant(all_conversations);
    });
};

function getAllParticipant(list_conversations) {
    var length = list_conversations.length;

    for (var i = 0; i < length; i++) {
        /* Lấy thông tin của conversation và lắng nghe*/
        var conversations = friendlyChat.database.ref('Conversations/' + list_conversations[i]);

        /* Hiển thị lịch sử chat lên view */
        conversations.on('value', function (snapshot) {
            var conv_id = snapshot.key;
            var conv_info = snapshot.val().conversationInfo;
            var participants = conv_info.participants;
            var part = [];
            for (var k in participants) {
                if (participants.hasOwnProperty(k)) {
                    if (k != data_user_id) {
                        part.push(k);
                    }
                }
            }
            chat_participants[conv_id] = part;
        });
    }
}

function getConversationInfo(list_conversations) {
    var itemChat = document.getElementById('item_chat_header');
    if (itemChat) itemChat.innerHTML = '';
    var panel_chat = document.getElementById("item-chat-panel");
    if (panel_chat) panel_chat.innerHTML = '';

    var length = list_conversations.length;

    for (var i = 0; i < length; i++) {
        /* Lấy thông tin của conversation và lắng nghe*/
        var conversations = friendlyChat.database.ref('Conversations/' + list_conversations[i]);
        var isUpdate = false;

        /* Hiển thị lịch sử chat lên view */
        conversations.on('value', function (snapshot) {
            var conv_id = snapshot.key;
            var conv_info = snapshot.val().conversationInfo;
            var participants = conv_info.participants;
            var friend_id = 0;
            var arr = [];

            for (var k in participants) {
                if (participants.hasOwnProperty(k)) {
                    if (k != data_user_id) {
                        friend_id = k;
                        arr.push(k);
                    }
                }
            }

            if (conv_info.isGroup != 0) friend_id = 0;

            // Hiển thị thông tin người tham gia
            if (friend_id != 0) {
                var friend = friendlyChat.database.ref('Users/' + friend_id);

                friend.once('value').then(function (f_snap) {
                    var friend_info = f_snap.val();
                    // Open chat_box nếu có tin nhắn mới.
                    if (window.location.pathname.indexOf("messages") == -1) {
                        var chat_box_widget = document.getElementById('chat' + friend_id);
                        if (chat_box_widget === null) {
                            var me = friendlyChat.database.ref('Users/' + data_user_id + '/lastLogin');
                            me.once('value').then(function (snaps) {
                                var lastUpdate = new Date(conv_info.lastTimeUpdated).getTime();
                                var lastLgn = new Date(snaps.val()).getTime();

                                if (lastUpdate >= lastLgn) {
                                    chat_box(friend_id.replace('_', ''), friend_info.fullName, friend_info.avatar, conv_id, friend_info.username);
                                    $("#chat_sound")[0].play();
                                }
                            });
                        }
                    }

                    if (!is_empty(conv_info.lastTimeUpdated)) {
                        if (friend_info.fullName) {
                            var uid = f_snap.key.replace('_', '');

                            var div = document.getElementById('item_header_' + conv_id);
                            // If an element for that message does not exists yet we create it.
                            if (div === null) {
                                var container = document.createElement('item_header_' + conv_id);
                                container.innerHTML = FriendlyChat.ITEM_HEADER_TEMPLATE;
                                div = container.firstChild;
                                div.setAttribute('id', 'item_header_' + conv_id);

                                itemChat.appendChild(div);
                            }
                            div.setAttribute('data-time', conv_info.lastTimeUpdated);

                            if (participants[data_user_id].isSeen == 0) {
                                div.className = 'feeds-item unread';
                            }


                            var containerElement = div.querySelector('.data-container');
                            var avatarElement = div.querySelector('.data-avatar');
                            var contentElement = div.querySelector('.data-content');

                            containerElement.setAttribute('data-uid', uid);
                            containerElement.setAttribute('data-name', friend_info.fullName);
                            containerElement.setAttribute('data-uname', friend_info.username);
                            containerElement.setAttribute('data-cid', conv_id);

                            containerElement.setAttribute('data-picture', friend_info.avatar);
                            avatarElement.setAttribute('src', friend_info.avatar);
                            avatarElement.setAttribute('onerror', "this.src=avatar_default_path;");
                            avatarElement.setAttribute('alt', friend_info.fullName);

                            contentElement.innerHTML =
                                '<div>' +
                                '<span class="name">' + friend_info.fullName + '</span>' +
                                '</div>' +
                                '<div class="text">' +
                                conv_info.lastMessage +
                                '</div>' +
                                '<div class="time js_moment" data-time="' + conv_info.lastTimeUpdated + '">' +
                                conv_info.lastTimeUpdated +
                                '</div>';

                            if (window.location.pathname.indexOf("messages") != -1) {
                                // Hiển thị ở panel
                                if (panel_chat) {
                                    var p_div = document.getElementById('item_panel_' + conv_id);
                                    // If an element for that message does not exists yet we create it.
                                    if (p_div === null) {
                                        var p_container = document.createElement('item_panel_' + conv_id);
                                        p_container.innerHTML = FriendlyChat.ITEM_HEADER_TEMPLATE;
                                        p_div = p_container.firstChild;
                                        p_div.setAttribute('id', 'item_panel_' + conv_id);

                                        panel_chat.appendChild(p_div);
                                    }
                                    p_div.setAttribute('data-time', conv_info.lastTimeUpdated);

                                    if (participants[data_user_id].isSeen == 0) {
                                        p_div.className = 'feeds-item unread';
                                    }

                                    var p_containerElement = p_div.querySelector('.data-container');
                                    var p_avatarElement = p_div.querySelector('.data-avatar');
                                    var p_contentElement = p_div.querySelector('.data-content');

                                    p_containerElement.setAttribute('data-uid', uid);
                                    p_containerElement.setAttribute('data-name', friend_info.fullName);
                                    p_containerElement.setAttribute('data-uname', friend_info.username);
                                    p_containerElement.setAttribute('data-cid', conv_id);

                                    p_containerElement.setAttribute('data-picture', friend_info.avatar);
                                    p_avatarElement.setAttribute('src', friend_info.avatar);
                                    p_avatarElement.setAttribute('alt', friend_info.fullName);

                                    p_contentElement.innerHTML =
                                        '<div>' +
                                        '<span class="name">' + friend_info.fullName + '</span>' +
                                        '</div>' +
                                        '<div class="text">' +
                                        conv_info.lastMessage +
                                        '</div>' +
                                        '<div class="time js_moment" data-time="' + conv_info.lastTimeUpdated + '">' +
                                        conv_info.lastTimeUpdated +
                                        '</div>';
                                }
                            }
                        }

                    }
                    // Khởi tạo xong hoặc update thì sắp xếp.
                    if (conv_id == list_conversations[length - 1] || isUpdate) {
                        sortMyData('#item_chat_header', '.feeds-item', false);
                        sortMyData('#item-chat-panel', '.feeds-item', true);
                        isUpdate = true;
                    }
                });
            } else {
                if (!is_empty(conv_info.lastTimeUpdated)) {

                    // Open chat_box nếu có tin nhắn mới.
                    if (window.location.pathname.indexOf("messages") == -1) {
                        var chat_box_widget = document.getElementById('chat_' + conv_id);
                        if (chat_box_widget === null) {

                            var me = friendlyChat.database.ref('Users/' + data_user_id + '/lastLogin');
                            me.once('value').then(function (snaps) {
                                var lastUpdate = new Date(conv_info.lastTimeUpdated).getTime();
                                var lastLgn = new Date(snaps.val()).getTime();

                                if ((participants[data_user_id].isSeen == 0) && (lastUpdate >= lastLgn)) {
                                    chat_box(0, conv_info.nameGroup, conv_info.avatarGroup, conv_id, true, conv_info.nameGroup);
                                    $("#chat_sound")[0].play();
                                }
                            });
                        }
                    }

                    if (conv_info.nameGroup) {
                        var div = document.getElementById('item_header_' + conv_id);
                        // If an element for that message does not exists yet we create it.
                        if (div === null) {
                            var container = document.createElement('item_header_' + conv_id);
                            container.innerHTML = FriendlyChat.ITEM_HEADER_TEMPLATE;
                            div = container.firstChild;
                            div.setAttribute('id', 'item_header_' + conv_id);

                            itemChat.appendChild(div);
                            //nav_chat.appendChild(div);
                        }
                        div.setAttribute('data-time', conv_info.lastTimeUpdated);

                        if (participants[data_user_id].isSeen == 0) {
                            div.className = 'feeds-item unread';
                        }

                        var containerElement = div.querySelector('.data-container');
                        var avatarElement = div.querySelector('.data-avatar');
                        var contentElement = div.querySelector('.data-content');

                        // containerElement.setAttribute('data-uid', uid);
                        containerElement.setAttribute('data-name', conv_info.nameGroup);
                        // containerElement.setAttribute('data-uname', friend_info.username);
                        containerElement.setAttribute('data-cid', conv_id);

                        if (!is_empty(conv_info.avatarGroup)) {
                            containerElement.setAttribute('data-picture', conv_info.avatarGroup);
                            avatarElement.setAttribute('src', conv_info.avatarGroup);
                        } else {
                            containerElement.setAttribute('data-picture', avatar_group_default_path);
                            avatarElement.setAttribute('src', avatar_group_default_path);
                        }

                        avatarElement.setAttribute('alt', conv_info.nameGroup);

                        contentElement.innerHTML =
                            '<div>' +
                            '<span class="name">' + conv_info.nameGroup + '</span>' +
                            '</div>' +
                            '<div class="text">' +
                            conv_info.lastMessage +
                            '</div>' +
                            '<div class="time js_moment" data-time="' + conv_info.lastTimeUpdated + '">' +
                            conv_info.lastTimeUpdated +
                            '</div>';

                        if (window.location.pathname.indexOf("messages") != -1) {
                            // Hiển thị tạm thời ở panel
                            if (panel_chat) {
                                var p_div = document.getElementById('item_panel_' + conv_id);
                                // If an element for that message does not exists yet we create it.
                                if (p_div === null) {
                                    var p_container = document.createElement('item_panel_' + conv_id);
                                    p_container.innerHTML = FriendlyChat.ITEM_HEADER_TEMPLATE;
                                    p_div = p_container.firstChild;
                                    p_div.setAttribute('id', 'item_panel_' + conv_id);

                                    panel_chat.appendChild(p_div);
                                }
                                p_div.setAttribute('data-time', conv_info.lastTimeUpdated);

                                if (participants[data_user_id].isSeen == 0) {
                                    p_div.className = 'feeds-item unread';
                                }

                                var p_containerElement = p_div.querySelector('.data-container');
                                var p_avatarElement = p_div.querySelector('.data-avatar');
                                var p_contentElement = p_div.querySelector('.data-content');

                                p_containerElement.setAttribute('data-cid', conv_id);

                                p_containerElement.setAttribute('data-picture', conv_info.avatarGroup);
                                p_avatarElement.setAttribute('src', conv_info.avatarGroup);
                                p_avatarElement.setAttribute('alt', conv_info.nameGroup);

                                p_contentElement.innerHTML =
                                    '<div>' +
                                    '<span class="name">' + conv_info.nameGroup + '</span>' +
                                    '</div>' +
                                    '<div class="text">' +
                                    conv_info.lastMessage +
                                    '</div>' +
                                    '<div class="time js_moment" data-time="' + conv_info.lastTimeUpdated + '">' +
                                    conv_info.lastTimeUpdated +
                                    '</div>';
                            }
                        }
                    }

                }
                // Khởi tạo xong hoặc update thì sắp xếp.
                if (conv_id == list_conversations[length - 1] || isUpdate) {
                    sortMyData('#item_chat_header', '.feeds-item', false);
                    sortMyData('#item-chat-panel', '.feeds-item', true);
                    isUpdate = true;
                }
            }

        });

    }
}

function sortMyData(container_attr, items_attr, isClick) {
    var container = $(container_attr);
    var live_messages_alt = $('.js_live-messages-alt .loader');
    var live_messages = $('.js_live-messages .loader');
    var items = $(container_attr + ' ' + items_attr);

    items.each(function () {
        // Convert the string in 'data-event-date' attribute to a more
        // standardized date format
        var standardDate = $(this).attr("data-time");
        if (isNaN(standardDate)) {
            standardDate = new Date(standardDate).getTime();
            $(this).attr("data-time", standardDate);
        }
    });
    live_messages_alt.remove();
    live_messages.remove();
    container.removeClass('hidden');
    items.sort(function (a, b) {
        a = parseFloat($(a).attr("data-time"));
        b = parseFloat($(b).attr("data-time"));
        return a < b ? -1 : a > b ? 1 : 0;
    }).each(function () {
        container.prepend(this);
    });

    if (isClick) {
        var panel_messages = document.getElementsByClassName("panel-messages");
        if (panel_messages[0] != null) {
            if (!panel_messages[0].hasAttribute('data-cid')) {
                if ($('ul#item-chat-panel' + ' li').length > 1) {
                    var id = $('ul#item-chat-panel' + ' li:first').attr('id');
                    $("#" + id + ' .js_chat-start').click();
                }
            }
        }
    }

}

// Checks that the Firebase SDK has been correctly setup and configured.
FriendlyChat.prototype.checkSetup = function () {
    if (!window.firebase || !(firebase.app instanceof Function) || !window.config) {
        window.alert('You have not configured and imported the Firebase SDK. ' +
            'Make sure you go through the codelab setup instructions.');
    } else if (config.storageBucket === '') {
        window.alert('Your Firebase Storage bucket has not been enabled. Sorry about that. This is ' +
            'actually a Firebase bug that occurs rarely. ' +
            'Please go and re-generate the Firebase initialisation snippet (step 4 of the codelab) ' +
            'and make sure the storageBucket attribute is not empty. ' +
            'You may also need to visit the Storage tab and paste the name of your bucket which is ' +
            'displayed there.');
    }
};

window.onload = function () {
    window.friendlyChat = new FriendlyChat();
};