/**
 * child js
 *
 * @package ConIu
 * @author QuanND
 */

// initialize API URLs
api['child/child'] = ajax_path + "ci/bo/child/bochild_all.php";
api['child/medicine'] = ajax_path + "ci/bo/child/bochild_medicine.php";
api['child/childinfo'] = ajax_path + "ci/bo/child/bochild_child.php";
api['child/childhealth'] = ajax_path + "ci/bo/child/bochild_health.php";
api['child/childjournal'] = ajax_path + "ci/bo/child/bochild_journal.php";
api['child/childmedical'] = ajax_path + "ci/bo/child/bochild_medical.php";
api['child/childdevelopment'] = ajax_path + "ci/bo/child/bochild_development.php";
api['child/point'] = ajax_path + "ci/bo/child/bochild_point.php";

$(function () {
    var timeModal = 1500;

    // run DataTable
    $('.js_dataTable').DataTable({
        "aoColumnDefs": [{'bSortable': false, 'aTargets': [-1]}],
        "language": {
            "decimal": "",
            "emptyTable": __["No data available in table"],
            "info": __["Showing _START_ to _END_ of _TOTAL_ results"],
            "infoEmpty": __["Showing 0 to 0 of 0 results"],
            "infoFiltered": "(filtered from _MAX_ total entries)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": __["Show"] + " " + "_MENU_" + " " + __["results"],
            "loadingRecords": __["Loading..."],
            "processing": __["Processing..."],
            "search": __["Search"],
            "zeroRecords": __["No matching records found"],
            "paginate": {
                "first": __["First"],
                "last": __["Last"],
                "next": __["Next"],
                "previous": __["Previous"]
            },
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            }
        }
    });

    // run metisMenu
    $(".js_metisMenu").metisMenu();

    // run open window
    $('body').on('click', '.js_open_window', function () {
        window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0');
        return false;
    });

    $('#select_child_id').on('change', function () {
        var child_id = this.value;
        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['child/child'], {'do': 'change_child', 'child_id': child_id}, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.callback) {
                eval(response.callback);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //BEGIN - Xử lý trong màn hình event
    $("#child_event_list :checkbox").change(function (e) {

        var action_id = $(this).attr('name');
        var child_id = $(this).attr('value');
        var event_id = $(this).attr('event_id');
        var register = this.checked;

        //if ($(this).is(":checked")) {
        confirm(__['Actions'], __['Are you sure you want to do this?'], function () {
            if (action_id == 'child_id') {
                $.post(api['child/child'], {
                    'do': 'event',
                    'child_id': child_id,
                    'type': '0',
                    'event_id': event_id,
                    'register': register
                }, function (response) {

                }, 'json')
                    .fail(function () {
                        modal('#modal-message', {
                            title: __['Error'],
                            message: __['There is something that went wrong!']
                        });
                    });
            } else if (action_id == 'parent_id') {
                $.post(api['child/child'], {
                    'do': 'event',
                    'child_id': child_id,
                    'type': '1',
                    'event_id': event_id,
                    'register': register
                }, function (response) {
                    /*if(response.callback) {
                       eval(response.callback);
                    } else {
                       window.location.reload();
                    }*/
                }, 'json')
                    .fail(function () {
                        modal('#modal-message', {
                            title: __['Error'],
                            message: __['There is something that went wrong!']
                        });
                    });
            }
            window.location.reload();
        });


    });

    /*$('#event_child_id').click(function () {
        console.log('dasdas--');
        var child_id = $(this).attr('value');
        var event_id = $(this).attr('event_id');
        var register = this.checked;

        $.post(api['child/child'], {'do': 'event','child_id': child_id, 'type': '0', 'event_id': event_id, 'register': register}, function(response) {
            //if(response.callback) {
            //    eval(response.callback);
            //} else {
            //    window.location.reload();
            //}
        }, 'json')
        .fail(function() {
            modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
        });
    });*/

    //END - Xử lý trong màn hình event

    $('body').on('click', '.js_child-tuition', function () {
        var child_id = $(this).attr('data-child_id');
        var child_tuition_id = $(this).attr('data-id');

        confirm(__['Actions'], __['Are you sure you want to do this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['child/child'], {
                'do': 'tuition',
                'child_id': child_id,
                'id': child_tuition_id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    //BEGIN- SCHOOL - Màn hình gửi thuốc cho trẻ-----------------------------
    //Xóa một lần gửi thuốc
    $('body').on('click', '.js_child-medicine-delete', function () {
        var child_id = $(this).attr('data-child');
        var id = $(this).attr('data-id');

        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['child/medicine'], {'do': 'delete', 'child_id': child_id, 'id': id}, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    //Hủy một lần gửi thuốc
    $('body').on('click', '.js_child-medicine-cancel', function () {
        var id = $(this).attr('data-id');
        var child_id = $(this).attr('data-child');

        confirm(__['Cancel'], __['Are you sure you want to cancel this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['child/medicine'], {'do': 'cancel', 'child_id': child_id, 'id': id}, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    //Bắt sự kiện khi click vào Detail
    $('body').on('click', '.js_child-medicine-detail', function () {
        var child_id = $(this).attr('data-child');
        var id = $(this).attr('data-id');
        var detail = $('#medicine-detail_' + id);
        var btnDetail = $('#button-detail_' + id);

        detail.removeClass('x-hidden');
        btnDetail.addClass('x-hidden');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['child/medicine'], {'do': 'detail', 'child_id': child_id, 'id': id}, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            if (response.results) {
                detail.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    $('#medicine_beginpicker_new').datetimepicker({
        format: DATE_FORMAT,
        minDate: new Date(),
        defaultDate: new Date()
    });
    $('#medicine_endpicker_new').datetimepicker({
        format: DATE_FORMAT,
        defaultDate: new Date()
    });
    $("#medicine_beginpicker_new").on("dp.change", function (e) {
        $('#medicine_endpicker_new').data("DateTimePicker").minDate(e.date);
    });
    $('#medicine_beginpicker').datetimepicker({
        format: DATE_FORMAT
    });
    $('#medicine_endpicker').datetimepicker({
        format: DATE_FORMAT
    });
    $("#medicine_beginpicker").on("dp.change", function (e) {
        $('#medicine_endpicker').data("DateTimePicker").minDate(e.date);
    });
    $('#medical_beginpicker').datetimepicker({
        format: DATE_FORMAT,
        defaultDate: new Date()
    });
    $('#medical_endpicker').datetimepicker({
        format: DATE_FORMAT,
        defaultDate: new Date()
    });
    $("#medical_beginpicker").on("dp.change", function (e) {
        $('#medical_endpicker').data("DateTimePicker").minDate(e.date);
    });
    //END - CHILD - Màn hình gửi thuốc cho trẻ-----------------------------

    //BEGIN - Màn hình điểm danh  ----------------------------------------------------
    $('#attendance_pk').datetimepicker({
        format: DATE_FORMAT,
        defaultDate: new Date()
    });

    $('#fromdate_picker').datetimepicker({
        format: DATE_FORMAT
    });

    $('#todate_picker').datetimepicker({
        format: DATE_FORMAT
    });

    $("#fromdate_picker").on("dp.change", function (e) {
        $('#todate_picker').data("DateTimePicker").minDate(e.date);
    });

    $('body').on('click', '.js_child-attendance', function () {
        var child_id = $(this).attr('data-id');
        var fromDate = $('#fromDate').val();
        var toDate = $('#toDate').val();
        var attendance_list = $('#attendance_list');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['child/child'], {
            'do': 'attendance',
            'child_id': child_id,
            'fromDate': fromDate,
            'toDate': toDate
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.results) {
                attendance_list.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });
    //END - Màn hình điểm danh ----------------------------------------------------

    //BEGIN - Màn hình dịch vụ ----------------------------------------------------
    $('#history_begin_picker').datetimepicker({
        format: DATE_FORMAT
    });

    $('#history_end_picker').datetimepicker({
        format: DATE_FORMAT,
        defaultDate: new Date()
    });

    //Tìm kiếm lịch sử sử dụng dịch vụ của 1 trẻ.
    $('body').on('click', '.js_service-search-history', function () {
        var school_id = $(this).attr('data-sid');
        var child_id = $(this).attr('data-cid');
        var begin = $('#begin').val();
        var end = $('#end').val();
        var service_id = $('#service_id option:selected').val();

        $('#usage_history').html('');
        $('#search').addClass('x-hidden');
        $('#loading').removeClass('x-hidden');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['child/child'], {
            'service_id': service_id,
            'child_id': child_id,
            'begin': begin,
            'end': end,
            'school_id': school_id,
            'do': 'service_history'
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            $('#search').removeClass('x-hidden');
            $('#loading').addClass('x-hidden');
            if (response.results) {
                $('#usage_history').html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $('#search').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });
    //END - Màn hình dịch vụ ----------------------------------------------------

    // Show detail trong màn hình báo cáo
    //Bắt sự kiện khi click vào Detail
    // $('body').on('click', '.js_child-report-detail', function () {
    //     var child_id = $(this).attr('data-child');
    //     var id = $(this).attr('data-id');
    //     var detail = $('#report-detail_' + id);
    //     var btnDetail = $('#button-detail_' + id);
    //
    //     // Show loading
    //     $("#loading_full_screen").removeClass('hidden');
    //
    //     $.post(api['child/child'], { 'do': 'report_detail', 'child_id': child_id, 'id': id}, function (response) {
    //         console.log('21312', response);
    //
    //         detail.removeClass('x-hidden');
    //         detail.html(response.results);
    //         btnDetail.addClass('x-hidden');
    //     }, 'json')
    //         .fail(function () {
    //             modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
    //         });
    // });
    //END - Màn hình báo cáo
    $('#using_time_picker').datetimepicker({
        format: DATE_FORMAT,
        // minDate: new Date(),
        defaultDate: new Date()
    });

    // khi chọn năm học thì gọi lại select học kỳ trong màn hình lists by subject
    $("#school_year").change(function () {

        // var class_level_id = $('#point_class_level_id').val();
        // var class_id = $('#point_class_id_import').val();
        var school_year = $('#school_year').val();
        var school_id = $('#schoolId').val();
        var class_id = $('#classId').val();
        var child_id = $('#childId').val();
        var search_with = $("input[name='do']").val();
        if (search_with == "detail") {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');
            $('#child_list_point').html('');
            $.post(api['child/point'], {
                'do': 'list',
                'school_id': school_id,
                'class_id': class_id,
                'child_id': child_id,
                'school_year': school_year,
            }, function (data) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $('#child_list_point').html(data.results);
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');
                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        } else if (search_with == "comment") {
            $("#semester").trigger("change");
        }
    }).trigger("change");

    // Khi chọn kỳ học ở màn hình comment
    $("#semester").change(function () {
        var child_id = $('#childId').val();
        var school_id = $('#schoolId').val();
        var class_id = $('#classId').val();
        var semester = $('#semester').val();
        var school_year = $('#school_year').val();
        $('#child_comment').html('');
// Show loading
        $("#loading_full_screen").removeClass('hidden');
        $.post(api['child/point'], {
            'do': 'show_comment_child', // Sử dụng ở màn hình thêm nhận xét
            // 'username': username,
            'school_id': school_id,
            'class_id': class_id,
            'school_year': school_year,
            // 'is_last_semester': is_last_semester,
            'child_id': child_id,
            'semester': semester
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            if (response.results) {
                $('#child_comment').html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    }).trigger("change");
    //Liệt kê danh sách dịch vụ để điểm danh sử dụng dịch vụ
    $('#using_time_picker').on("dp.change", function (e) {
        var school_id = $('#school_id').val();
        var using_at = $('#using_at').val();
        var child_id = $('#child_id').val();
        var error = $('.alert.alert-danger');
        var success = $('.alert.alert-success');
        var ncb_cnt = $('#ncb_cnt').val();

        $('#service_list').html('');
        $('button[type="submit"]').prop('disabled', true);
        if (success.is(":visible")) success.hide();
        if (error.is(":visible")) error.hide();

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['child/child'], {
            'child_id': child_id,
            'using_at': using_at,
            'school_id': school_id,
            'do': 'service_list4record'
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.results) {
                $('#service_list').html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
            $('button[type="submit"]').prop('disabled', (response.disableSave && (ncb_cnt == 0)));
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });
    // bắt sự kiện khi tạo đơn thuốc
    $(document).on('submit', '#create_medicine', function (e) {
        var _this = $(this);
        var submit = _this.find('button[type="submit"]');

        /* show loading */
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading']);

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        e.preventDefault();
        $.ajax({
            url: api['child/medicine'],
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false
        }).done(function (data) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            /* hide loading */
            submit.prop('disabled', false);
            submit.html(submit.data('text'));
            if (data.success) {
                modal('#modal-success', {title: __['Success'], message: data.message});
                modal_hidden(timeModal);
            } else if (data.error) {
                modal('#modal-error', {title: __['Error'], message: data.message});
            } else if (data.callback) {
                eval(data.callback);
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                /* hide loading */
                submit.prop('disabled', false);
                submit.html(submit.data('text'));
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // bắt sự kiện khi sửa thông tin người đón trẻ thay
    $(document).on('submit', '#edit_information', function (e) {
        var _this = $(this);
        var submit = _this.find('button[type="submit"]');

        /* show loading */
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading']);

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        e.preventDefault();
        $.ajax({
            url: api['child/child'],
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false
        }).done(function (data) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            /* hide loading */
            submit.prop('disabled', false);
            submit.html(submit.data('text'));
            if (data.success) {
                modal('#modal-success', {title: __['Success'], message: data.message});
                modal_hidden(timeModal);
            } else if (data.error) {
                modal('#modal-error', {title: __['Error'], message: data.message});
            } else {
                eval(data.callback);
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                /* hide loading */
                submit.prop('disabled', false);
                submit.html(submit.data('text'));
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    /* BEGIN - Đón muộn */

    $('#fromDate_pickup').datetimepicker({
        format: DATE_FORMAT,
        sideBySide: true
    });

    $('#toDate_pickup').datetimepicker({
        format: DATE_FORMAT,
        sideBySide: true
    });

    $('body').on('click', '.js_pickup-search', function () {
        var child_id = $(this).attr('data-child');
        var fromDate = $('#fromDate').val();
        var toDate = $('#toDate').val();

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['child/child'], {
            'do': 'pickup_history',
            'child_id': child_id,
            'from_date': fromDate,
            'to_date': toDate
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.callback) {
                eval(response.callback);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else {
                if (response.results) {
                    $('#pickup_list_child').html(response.results);
                }
            }

        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });
    /* END - Đón muộn */

    // Phụ huynh xóa trẻ
    $('body').on('click', '.js_child-delete', function () {
        var child_parent_id = $(this).attr('data-id');

        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['child/childinfo'], {'do': 'delete', 'child_parent_id': child_parent_id}, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    // bắt sự kiện khi sửa thông tin sức khỏe của trẻ
    $(document).on('submit', '#edit_child_health', function (e) {
        var _this = $(this);
        var submit = _this.find('button[type="submit"]');

        /* show loading */
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading']);

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        e.preventDefault();
        $.ajax({
            url: api['child/childhealth'],
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false
        }).done(function (data) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            /* hide loading */
            submit.prop('disabled', false);
            submit.html(submit.data('text'));
            if (data.success) {
                modal('#modal-success', {title: __['Success'], message: data.message});
                modal_hidden(timeModal);
            } else if (data.error) {
                modal('#modal-error', {title: __['Error'], message: data.message});
            }
            if (data.callback) {
                eval(data.callback);
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                /* hide loading */
                submit.prop('disabled', false);
                submit.html(submit.data('text'));
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // bắt sự kiện khi sửa thông tin chiều cao cân nặng của trẻ
    $(document).on('submit', '#edit_child_growth', function (e) {
        var _this = $(this);
        var error = $('.alert.alert-danger');
        var success = $('.alert.alert-success');
        var submit = _this.find('button[type="submit"]');

        /* show loading */
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading']);

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        e.preventDefault();
        $.ajax({
            url: api['child/childhealth'],
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false
        }).done(function (data) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            /* hide loading */
            submit.prop('disabled', false);
            submit.html(submit.data('text'));
            if (data.success) {
                modal('#modal-success', {title: __['Success'], message: data.message});
                modal_hidden(timeModal);
            } else if (data.error) {
                modal('#modal-error', {title: __['Error'], message: data.message});
            } else if (data.callback) {
                eval(data.callback);
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                /* hide loading */
                submit.prop('disabled', false);
                submit.html(submit.data('text'));
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Xóa thông tin y bạ
    $('body').on('click', '.js_child-medical-delete', function () {
        var child_parent_id = $(this).attr('data-child');
        var id = $(this).attr('data-id');

        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['child/childmedical'], {
                'do': 'delete',
                'child_parent_id': child_parent_id,
                'child_medical_id': id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    // Xử lý khi tìm kiếm theo năm thông tin bệnh
    $('body').on('click', '.js_child-medical-search', function () {
        var child_parent_id = $(this).attr('data-id');
        var year = $('#year').val();
        var medical_list = $('#medical_list');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['child/childmedical'], {
            'do': 'search',
            'child_parent_id': child_parent_id,
            'year': year
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            if (response.results) {
                medical_list.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Xử lý khi tìm kiếm theo năm góc nhật ký
    $('body').on('click', '.js_child-journal-search', function () {
        var child_parent_id = $(this).attr('data-id');
        var year = $('#year').val();
        var journal_list = $('#journal_list');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['child/childjournal'], {
            'do': 'search',
            'child_parent_id': child_parent_id,
            'year': year
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            if (response.results) {
                journal_list.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Xử lý khi xoa thông tin nhật ký của trẻ
    $('body').on('click', '.js_child-journal-delete', function () {
        var id = $(this).attr('data-id');
        var child_parent_id = $(this).attr('data-child');
        var handle = $(this).attr('data-handle');

        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['child/childjournal'], {
                'do': handle,
                'child_parent_id': child_parent_id,
                'child_journal_id': id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    // Xử lý khi xoa thông tin sức khỏe của trẻ
    $('body').on('click', '.js_child-health-delete', function () {
        var id = $(this).attr('data-id');
        var child_parent_id = $(this).attr('data-child');
        var handle = $(this).attr('data-handle');

        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['child/childhealth'], {
                'do': handle,
                'child_parent_id': child_parent_id,
                'id': id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    // Xử lý xóa ảnh đính kèm
    $(".delete_image").click(function () {
        $("input[name*='file']").val("");
        $("#file_old").remove();
        $("#is_file").val("0");
    });

    //Bắt sự kiện khi edit trẻ từ phụ huynh
    $(document).on('submit', '#edit_child_parent', function (e) {
        var _this = $(this);
        var submit = _this.find('button[type="submit"]');

        /* show loading */
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading']);

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        e.preventDefault();
        $.ajax({
            url: api['child/childinfo'],
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false
        }).done(function (data) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            /* hide loading */
            submit.prop('disabled', false);
            submit.html(submit.data('text'));
            if (data.success) {
                modal('#modal-success', {title: __['Success'], message: data.message});
                modal_hidden(timeModal);
            } else if (data.error) {
                modal('#modal-error', {title: __['Error'], message: data.message});
            } else if (data.callback) {
                eval(data.callback);
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                /* hide loading */
                submit.prop('disabled', false);
                submit.html(submit.data('text'));
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // bắt sự kiện khi tạo góc nhật ký cho trẻ
    $(document).on('submit', '#add_child_journal', function (e) {
        var _this = $(this);
        var submit = _this.find('button[type="submit"]');

        /* show loading */
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading']);

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        e.preventDefault();
        $.ajax({
            url: api['child/childjournal'],
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false
        }).done(function (data) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            /* hide loading */
            submit.prop('disabled', false);
            submit.html(submit.data('text'));
            if (data.success) {
                modal('#modal-success', {title: __['Success'], message: data.message});
                modal_hidden(timeModal);
            } else if (data.error) {
                modal('#modal-error', {title: __['Error'], message: data.message});
            } else if (data.callback) {
                eval(data.callback);
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                /* hide loading */
                submit.prop('disabled', false);
                submit.html(submit.data('text'));
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Xử lý khi cho trẻ tiêm phòng
    $('body').on('click', '.js_child-vaccinationed', function () {
        var child_development_id = $(this).attr('data-id');
        var child_parent_id = $(this).attr('data-child');

        confirm(__['Actions'], __['Are you sure you want to do this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['child/childdevelopment'], {
                'do': 'vaccinationed',
                'child_parent_id': child_parent_id,
                'child_development_id': child_development_id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    // Xử lý khi hủy tiêm phòng
    $('body').on('click', '.js_child-cancel-vaccination', function () {
        var child_development_id = $(this).attr('data-id');
        var child_parent_id = $(this).attr('data-child');

        confirm(__['Actions'], __['Are you sure you want to do this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['child/childdevelopment'], {
                'do': 'cancel_vaccination',
                'child_parent_id': child_parent_id,
                'child_development_id': child_development_id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    // Xử lý khi search thông tin sức khỏe
    $('body').on('click', '.js_child-chart-search', function () {
        var child_parent_id = $(this).attr('data-child');
        var begin = $('#begin').val();
        var end = $('#end').val();
        var chart_list = $('#chart_list');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['child/childhealth'], {
            'do': 'search_chart',
            'child_parent_id': child_parent_id,
            'begin': begin,
            'end': end
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            if (response.results) {
                chart_list.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Xử lý khi cho trẻ tiêm phòng
    $('body').on('click', '.js_child-picker-delete', function () {
        var child_info_id = $(this).attr('data-id');
        var child_id = $(this).attr('data-child');

        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['child/child'], {
                'do': 'delete_info',
                'child_id': child_id,
                'child_info_id': child_info_id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    // Xử lý khi xóa album ảnh
    $('body').on('click', '.js_child-journal-delete-album', function () {
        var journal_id = $(this).attr('data-id');
        var child_parent_id = $(this).attr('data-child');
        var handle = $(this).attr('data-handle');

        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['child/childjournal'], {
                'do': handle,
                'child_journal_album_id': journal_id,
                'child_parent_id': child_parent_id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    $('body').on('click', '.edit_caption', function () {
        var _this = $(this);
        var journal_album = _this.parents('.journal_album');
        var caption_show = journal_album.find('.caption_show');
        var caption_edit = journal_album.find('.caption_edit');
        caption_show.hide();
        caption_edit.removeClass('x-hidden');
    });

    // Xử lý khi sửa caption album ảnh
    $('body').on('click', '.js_child-journal-edit-caption', function () {
        var journal_id = $(this).attr('data-id');
        var child_parent_id = $(this).attr('data-child');
        var caption = $('.caption_' + journal_id).val();

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['child/childjournal'], {
            'do': 'edit_caption',
            'child_journal_album_id': journal_id,
            'child_parent_id': child_parent_id,
            'caption': caption
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.callback) {
                eval(response.callback);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    $('body').on('change', '.js_child-journal-edit-caption_change', function () {
        var journal_id = $(this).attr('data-id');
        var child_parent_id = $(this).attr('data-child');
        var caption = $(this).val();

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['child/childjournal'], {
            'do': 'edit_caption',
            'child_journal_album_id': journal_id,
            'child_parent_id': child_parent_id,
            'caption': caption
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.callback) {
                eval(response.callback);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Xử lý ẩn hiện nhật ký
    $('body').on('click', '.show_or_hidden_diary', function () {
        var id = $(this).attr('data-id');
        $('.album_images_' + id).toggle();
    });
});