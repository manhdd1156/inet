/**
 * school js
 *
 * @package ConIu
 * @author QuanND
 */

// initialize API URLs
api['school/class_level'] = ajax_path + "ci/bo/school/bo_class_level.php";
api['school/class'] = ajax_path + "ci/bo/school/bo_class.php";
api['school/child'] = ajax_path + "ci/bo/school/bo_child.php";
api['school/search'] = ajax_path + "ci/bo/bo_search.php";
api['school/attendance'] = ajax_path + "ci/bo/school/bo_attendance.php";
api['school/event'] = ajax_path + "ci/bo/school/bo_event.php";
api['school/teacher'] = ajax_path + "ci/bo/school/bo_teacher.php";
api['school/tuition'] = ajax_path + "ci/bo/school/bo_tuition.php";
api['school/medicine'] = ajax_path + "ci/bo/school/bo_medicine.php";
api['school/teacherlist'] = ajax_path + "ci/bo/school/bo_teacherlist.php";
api['school/classlevellist'] = ajax_path + "ci/bo/school/bo_classlevellist.php";
api['school/parentlist'] = ajax_path + "ci/bo/bo_parentlist.php";
api['school/service'] = ajax_path + "ci/bo/school/bo_service.php";
api['school/permission'] = ajax_path + "ci/bo/school/bo_permission.php";
api['school/role'] = ajax_path + "ci/bo/school/bo_role.php";
api['school/foodlist'] = ajax_path + "ci/bo/bo_foodlist.php";
api['school/report'] = ajax_path + "ci/bo/school/bo_report.php";
api['school/feedback'] = ajax_path + "ci/bo/school/bo_feedback.php";
api['school/birthday'] = ajax_path + "ci/bo/school/bo_birthday.php";
api['school/schedule'] = ajax_path + "ci/bo/school/bo_schedule.php";
api['school/setting'] = ajax_path + "ci/bo/school/bo_setting.php";
api['school/pickup'] = ajax_path + "ci/bo/school/bo_pickup.php";
api['school/teacherpickup'] = ajax_path + "ci/bo/school/bo_teacherpickup.php";
api['school/menu'] = ajax_path + "ci/bo/school/bo_menu.php";
api['school/cookie'] = ajax_path + "ci/bo/school/bo_cookie.php";
api['school/point'] = ajax_path + "ci/bo/school/bo_point.php";
api['school/conduct'] = ajax_path + "ci/bo/school/bo_conduct.php";
api['school/course'] = ajax_path + "ci/bo/school/bo_course.php";

var arrPickupSchoolDP = [];
var arrPickupClassDP = [];

$(function () {
    // biến lưu thời gian xuất hiện modal
    var timeModal = 1500;

    // run DataTable
    $('.js_dataTable').DataTable({
        "aoColumnDefs": [{'bSortable': false, 'aTargets': [-1]}],
        "language": {
            "decimal": "",
            "emptyTable": __["No data available in table"],
            "info": __["Showing _START_ to _END_ of _TOTAL_ results"],
            "infoEmpty": __["Showing 0 to 0 of 0 results"],
            "infoFiltered": "(filtered from _MAX_ total entries)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": __["Show"] + " " + "_MENU_" + " " + __["results"],
            "loadingRecords": __["Loading..."],
            "processing": __["Processing..."],
            "search": __["Search"],
            "zeroRecords": __["No matching records found"],
            "paginate": {
                "first": __["First"],
                "last": __["Last"],
                "next": __["Next"],
                "previous": __["Previous"]
            },
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            }
        }
    });
    // run DataTable
    /*$('.js_dataTable1').DataTable({
        "aoColumnDefs": [{'aDataSort': false, 'aTargets': [-1]}]
    });*/

    // run metisMenu
    $(".js_metisMenu").metisMenu();

    // run open window
    $('body').on('click', '.js_open_window', function () {
        window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0');
        return false;
    });

    // run open window
    // $('body').on('click', '.js_show-loading', function () {
    //     $('#loading').removeClass('hidden');
    // });

    //Xử lý khi xóa: thông báo, gửi thuốc, sự kiện, child, class, class_level
    $('body').on('click', '.js_school-delete', function () {
        var _this = $(this);
        var handle = $(this).attr('data-handle');
        var username = $(this).attr('data-username');
        var id = $(this).attr('data-id');

        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['school/' + handle], {
                'do': 'delete',
                'school_username': username,
                'id': id
            }, function (response) {

                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else if (response.success) {
                    modal('#modal-success', {title: __['Success'], message: response.message});
                    modal_hidden(timeModal);
                    if (handle === "event") {
                        var count_event = $('.count_event').text();
                        count_event = parseInt(count_event);
                        count_event = count_event - 1;
                        $('.count_event').text(count_event);
                        _this.closest("tr").hide();
                    }
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    //Xử lý khi loại giáo viên khỏi trường
    $('body').on('click', '.js_school-unassign', function () {
        var handle = $(this).attr('data-handle');
        var username = $(this).attr('data-username');
        var id = $(this).attr('data-id');

        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['school/' + handle], {
                'do': 'unassign',
                'school_username': username,
                'id': id
            }, function (response) {
                // Show loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else if (response.success) {
                    modal('#modal-success', {title: __['Success'], message: response.message});
                    modal_hidden(timeModal);
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    //Xử lý khi xóa: loại phí
    $('body').on('click', '.js_school-fee-delete', function () {
        var username = $(this).attr('data-username');
        var id = $(this).attr('data-id');

        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['school/tuition'], {
                'do': 'delete_fee',
                'school_username': username,
                'id': id
            }, function (response) {
                // Show loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else if (response.success) {
                    modal('#modal-success', {title: __['Success'], message: response.message});
                    modal_hidden(timeModal);
                    var count_fee = $('.count_fee').text();
                    count_fee = parseInt(count_fee);
                    count_fee = count_fee - 1;
                    $('.count_fee').text(count_fee);
                    //_this.closest("tr").hide();
                    $('.list_fee_' + id).each(function () {
                        $(this).hide();
                    });
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    //Xử lý khi không áp dụng loại phí
    $('body').on('click', '.js_school-fee-cancel', function () {
        var _this = $(this);
        var username = $(this).attr('data-username');
        var id = $(this).attr('data-id');

        confirm(__['Actions'], __['Are you sure you want to do this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['school/tuition'], {
                'do': 'cancel_fee',
                'school_username': username,
                'id': id
            }, function (response) {
                // Show loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else if (response.success) {
                    modal('#modal-success', {title: __['Success'], message: response.message});
                    modal_hidden(timeModal);
                    var count_fee = $('.count_fee').text();
                    count_fee = parseInt(count_fee);
                    count_fee = count_fee - 1;
                    $('.count_fee').text(count_fee);
                    //_this.closest("tr").hide();
                    $('.list_fee_' + id).each(function () {
                        $(this).hide();
                    });
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    /**
     * Tìm kiếm trẻ theo điều kiện nhập vào
     */
    $('body').on('click', '.js_child-search', function () {
        var school_username = $(this).attr('data-username');
        var school_id = $(this).attr('data-id');
        var is_new = $(this).attr('data-isnew');
        var page = $(this).attr('data-page');
        var class_id = $('#class_id option:selected').val();
        var child_month = $('#child_month option:selected').val();
        var keyword = $('#keyword').val();

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $('#child_list').html('');
        $('#search').addClass('x-hidden');

        $.post(api['school/search'], {
            'keyword': keyword,
            'school_id': school_id,
            'class_id': class_id,
            'child_month': child_month,
            'page': page,
            'is_new': is_new,
            'school_username': school_username,
            'func': 'searchchild'
        }, function (response) {
            // Show loading
            $("#loading_full_screen").addClass('hidden');

            if (response.results) {
                $('#search').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');

                $('#child_list').html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                $('#search').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    /**
     * Tìm kiếm trẻ theo điều kiện nhập vào
     */
    $('body').on('click', '.js_child-statistics-search', function () {
        var school_username = $(this).attr('data-username');
        var school_id = $(this).attr('data-id');
        var is_new = $(this).attr('data-isnew');
        var page = $(this).attr('data-page');
        var class_id = $('#class_id option:selected').val();

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $('#child_list').html('');
        $('#search').addClass('x-hidden');

        $.post(api['school/search'], {
            'school_id': school_id,
            'class_id': class_id,
            'page': page,
            'is_new': is_new,
            'school_username': school_username,
            'func': 'searchchildstatistics'
        }, function (response) {
            // Show loading
            $("#loading_full_screen").addClass('hidden');

            if (response.results) {
                $('#search').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');

                $('#child_list').html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                $('#search').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    /**
     * Tìm kiếm trẻ theo điều kiện nhập vào
     */
    $('body').on('click', '.js_child-search-paging', function () {
        var school_username = $(this).attr('data-username');
        var school_id = $(this).attr('data-id');
        var page = $(this).attr('data-page');
        var is_new = $(this).attr('data-isNew');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $('#child_list').html('');
        $('#search').addClass('x-hidden');

        $.post(api['school/search'], {
            'school_id': school_id,
            'page': page,
            'school_username': school_username,
            'func': 'searchchildleave'
        }, function (response) {
            // Show loading
            $("#loading_full_screen").addClass('hidden');

            if (response.results) {
                $('#search').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');

                $('#child_list').html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                $('#search').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    /**
     * Xử lý khi bấm vào export danh sách trẻ
     */
    $('body').on('click', '.js_school-export-children', function () {
        var school_username = $(this).attr('data-username');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $('#search').addClass('x-hidden');
        $('#export2excel').addClass('x-hidden');
        $('#export_processing').removeClass('x-hidden');
        $.post(api['school/child'], {
                'school_username': school_username,
                'do': 'export'
            },
            function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }

                $('#search').removeClass('x-hidden');
                $('#export2excel').removeClass('x-hidden');
                $('#export_processing').addClass('x-hidden');

                var $a = $("<a>");
                $a.attr("href", response.file);
                $("body").append($a);
                $a.attr("download", response.file_name);
                $a[0].click();
                $a.remove();
            }, 'json')
            .fail(function () {
                $('#search').removeClass('x-hidden');
                $('#export2excel').removeClass('x-hidden');
                $('#export_processing').addClass('x-hidden');
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Xác nhận user là phụ huynh của trẻ
    $('body').on('click', '.js_school-approve-parent', function () {
        var parent_id = $(this).attr('data-parent');
        var child_id = $(this).attr('data-id');
        var school_username = $(this).attr('data-username');
        var button = $('#button_' + child_id + '_' + parent_id);

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        button.addClass('x-hidden');
        $.post(api['school/child'], {
            'do': 'approve',
            'child_id': child_id,
            'parent_id': parent_id,
            'school_username': school_username
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else if (response.success) {
                modal('#modal-success', {title: __['Success'], message: response.message});
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });


    // Xử lý khi bấm thêm mới trẻ vào trường
    $('body').on('submit', '.js_ajax-add-child-form', function (e) {
        e.preventDefault();
        var _this = $(this);
        var url = _this.attr('data-url');
        var submit = _this.find('button[type="submit"]');
        // var error = _this.find('.alert.alert-danger');
        // var success = _this.find('.alert.alert-success');
        /* show any collapsed section if any */
        console.log('this :', _this);
        if (_this.find('.js_hidden-section').length > 0 && !_this.find('.js_hidden-section').is(':visible')) {
            _this.find('.js_hidden-section').slideDown();
            return false;
        }

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        /* show loading */
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading']);
        /* get ajax response */
        $.post(ajax_path + url, $(this).serialize(), function (response) {
            /* hide loading */
            submit.prop('disabled', false);
            submit.html(submit.data('text'));

            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            /* handle response */
            if (response.error) {
                // if (success.is(":visible")) success.hide(); // hide previous alert
                // error.html(response.message).slideDown();
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else if (response.success) {
                $('.js_ajax-add-child-form').trigger("reset");
                $('#create_parent_account').prop('disabled', false);
                $('#code_auto').prop('disabled', false);
                $('#child_code').prop('disabled', true);
                $('#parent_list').html("");
                $("#for_service").prop('checked', false);
                $("#service_box").addClass('x-hidden');
                $(".service_ids").each(function (e) {
                    $(this).prop('checked', false);
                });
                // if (error.is(":visible")) error.hide(); // hide previous alert
                // success.html(response.message).slideDown();
                modal('#modal-success', {title: __['Success'], message: response.message});
                modal_hidden(timeModal);
            } else {
                eval(response.callback);
            }
        }, "json")
            .fail(function () {
                /* hide loading */
                submit.prop('disabled', false);
                submit.html(submit.data('text'));
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                /* handle error */
                // if (success.is(":visible")) success.hide(); // hide previous alert
                // error.html(__['There is something that went wrong!']).slideDown();
                modal('#modal-error', {title: __['Error'], message: response.message});
            });
    });

    /**
     * Tìm kiếm trẻ đã có sẵn trong hệ thống bằng child code
     */
    $('body').on('click', '.js_child-addexisting', function () {
        var school_username = $(this).attr('data-username');
        var school_id = $(this).attr('data-id');
        var childcode = $('#childcode').val();
        var table_child_addexisting = $('#table_child_addexisting');

        $('#search').addClass('x-hidden');

        // Show loading
        $('#loading').removeClass('x-hidden');
        $('#loading_full_screen').removeClass('hidden');

        table_child_addexisting.html('');

        $.post(api['school/search'], {
            'school_username': school_username,
            'school_id': school_id,
            'func': 'searchchildexist',
            'childcode': childcode
        }, function (response) {
            if (response.results) {
                $('#search').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                table_child_addexisting.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                $('#search').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // ---- BEGIN - Màn hình: Thêm user có sẳn làm giáo viên của trường --------------------------------------------------
    /**
     * Tìm kiếm user khi nhập vào ô text
     */
    $('body').on('keyup', '#search-username', search_delay(function () {
        var query = $(this).val();

        //Khi nào gõ 3 ký tự trẻ lên mới search
        if (query.length < 4) return;

        var user_ids = new Array();
        $("input[name*='user_id']").each(function () {
            user_ids.push($(this).val());
        });

        if (!is_empty(query)) {
            $('#search-username-results').show();
            $.post(api['school/search'], {'query': query, 'user_ids': user_ids, 'func': 'user'}, function (response) {
                if (response.callback) {
                    eval(response.callback);
                } else if (response.results) {
                    $('#search-username-results .dropdown-widget-header').show();
                    $('#search-username-results .dropdown-widget-body').html(response.results);
                } else {
                    $('#search-username-results .dropdown-widget-header').hide();
                    $('#search-username-results .dropdown-widget-body').html(render_template('#search-for', {'query': query}));
                }
            }, 'json');
        }
    }, 1000));

    /* show previous search-username-results when the search-username is clicked */
    $('body').on('click', '#search-username', function () {
        if ($(this).val() != '') {
            $('#search-username-results').show();
        }
    });

    /**
     * Hàm xử lý khi chọn user từ danh sách xổ ra
     */
    $('body').on('click', '.js_user-select', function () {
        var user_id = $(this).attr('data-uid');
        var user_ids = new Array();
        $("input[name*='user_id']").each(function () {
            user_ids.push($(this).val());
        });

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/teacherlist'], {
            'user_id': user_id,
            'user_ids': user_ids,
            'func': 'add'
        }, function (response) {
            $("#loading_full_screen").addClass('hidden');

            if (response.results) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                $('#teacher_list').html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: respons.message});
            }
            $('button[type="submit"]').prop('disabled', response.no_data);
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });
    //END - Màn hình: Thêm user có sẳn làm giáo viên của trường ----------------------------------------------------------

    // ---- BEGIN - Màn hình: Thêm user có sẳn làm cha mẹ của một trẻ --------------------------------------------------
    /**
     * Tìm kiếm user khi nhập vào ô text
     */
    $('body').on('keyup', '#search-parent', search_delay(function () {
        var query = $(this).val();
        if (query.length < 4) return;

        var user_ids = new Array();
        $("input[name*='user_id']").each(function () {
            user_ids.push($(this).val());
        });

        if (!is_empty(query)) {
            $('#search-parent-results').show();
            $.post(api['school/search'], {'query': query, 'user_ids': user_ids, 'func': 'parent'}, function (response) {
                if (response.callback) {
                    eval(response.callback);
                } else if (response.results) {
                    $('#search-parent-results .dropdown-widget-header').show();
                    $('#search-parent-results .dropdown-widget-body').html(response.results);
                } else {
                    $('#search-parent-results .dropdown-widget-header').hide();
                    $('#search-parent-results .dropdown-widget-body').html(render_template('#search-for', {'query': query}));
                }
            }, 'json');
        }
    }, 1000));

    /* show previous search-parent-results when the search-parent is clicked */
    $('body').on('click', '#search-parent', function () {
        if ($(this).val() != '') {
            $('#search-parent-results').show();
        }
    });

    /**
     * Hàm xử lý khi chọn user từ danh sách xổ ra
     */
    $('body').on('click', '.js_parent-select', function () {
        var child_id = $('#child_id').val();
        var child_admin = $('#child_admin').val();
        var user_id = $(this).attr('data-uid');
        var user_ids = new Array();
        $("input[name*='user_id']").each(function () {
            user_ids.push($(this).val());
        });

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/parentlist'], {
            'user_id': user_id,
            'child_id': child_id,
            'child_admin': child_admin,
            'user_ids': user_ids,
            'func': 'add'
        }, function (response) {
            if (response.results) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $('#parent_list').html(response.results);
                if ($('#parent_phone').val() == '') {
                    $('#parent_phone').val(response.phone);
                }
                if ($('#parent_email').val() == '') {
                    $('#parent_email').val(response.email);
                }
                $('#create_parent_account').prop('checked', response.no_parent);
                $('#create_parent_account').prop('disabled', !response.no_parent);
                $('#parent_name').prop('disabled', !response.no_parent);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Xóa user từ danh sách xổ ra
    $('body').on('click', '.js_parent-remove', function () {
        var child_id = $('#child_id').val();
        var child_admin = $('#child_admin').val();
        var user_id = $(this).attr('data-uid');
        var user_ids = new Array();
        $("input[name*='user_id']").each(function () {
            user_ids.push($(this).val());
        });

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/parentlist'], {
            'user_id': user_id,
            'child_admin': child_admin,
            'child_id': child_id,
            'user_ids': user_ids,
            'func': 'remove'
        }, function (response) {
            if (response.results) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $('#parent_list').html(response.results);
                if ($('#parent_phone').val() == '') {
                    $('#parent_phone').val(response.phone);
                }
                if ($('#parent_email').val() == '') {
                    $('#parent_email').val(response.email);
                }
                $('#create_parent_account').prop('checked', response.no_parent);
                $('#create_parent_account').prop('disabled', !response.no_parent);
                $('#parent_name').prop('disabled', !response.no_parent);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Set admin cho phụ huynh
    $('body').on('click', '.js_parent-set-admin', function () {
        var user_id = $(this).attr('data-uid');
        $('#child_admin').val(user_id);
        $('.js_parent-set-admin').removeClass('x-hidden');
        $('.js_parent-remove').removeClass('x-hidden');
        $(this).addClass('x-hidden');
        $(this).parent().find('.js_parent-remove').addClass('x-hidden');
    });
    //END - Màn hình: Thêm user có sẳn làm cha mẹ của một trẻ ----------------------------------------------------------

    //Xóa dữ liệu trên màn hình tạo trẻ
    $('body').on('click', '.js_add_child_clear', function () {
        $("input[name*='child_code']").val("");
        $("input[name*='last_name']").val("");
        $("input[name*='first_name']").val("");
        $("input[name*='birthday']").val("");
        $("input[name*='search-parent']").val("");
        $('#parent_list').html("");
        $("input[name*='parent_phone']").val("");
        $("input[name*='parent_phone_dad']").val("");
        $("input[name*='parent_job']").val("");
        $("input[name*='parent_job_dad']").val("");
        $("input[name*='parent_name']").val("");
        $("input[name*='parent_name_dad']").val("");
        $("input[name*='parent_email']").val("");
        $('#create_parent_account').prop('checked', true);
        $('#create_parent_account').prop('disabled', false);
        $("input[name*='address']").val("");
        $("input[name*='begin_at']").val("");
        $("#description").val("");
        $("#for_service").prop('checked', false);
        $("#service_box").addClass('x-hidden');
        $(".service_ids").each(function (e) {
            $(this).prop('checked', false);
        });
    });

    //Bắt sự kiện khi upload một file Excel danh sách trẻ lên
    $(document).on('submit', '#import_excel_form', function (e) {
        e.preventDefault();

        var submit = $('#submit_id');
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading...']);
        $('#result_info').html('');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.ajax({
            url: api['school/child'],
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false
        }).done(function (data) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            submit.prop('disabled', false);
            submit.html(submit.data('text'));

            $('#result_info').html(data.results);
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                submit.prop('disabled', false);
                submit.html(submit.data('text'));

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });
    //Bắt sự kiện khi upload một file Excel chi tiết lịch học lên
    $('body').on('click', '.js_school-schedule-preview-excel', function (e) {
        $('#do').val('preview_excel');
    });
    $('body').on('click', '.schedule_submit', function (e) {
        if ($('#import_excel').is(':checked')) {
            $('#do').val('import');
        } else {
            $('#do').val('add');
        }
    });

    //Bắt sự kiện khi upload một file Excel chi tiết lịch học lên
    $(document).on('submit', '#import_schedule_excel_form', function (e) {
        e.preventDefault();

        var submit = $('#submit');
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading...']);
        var error = $(this).find('.alert.alert-danger');
        var success = $(this).find('.alert.alert-success');
        $('#result_info').html('');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.ajax({
            url: api['school/schedule'],
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false
        }).done(function (data) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            submit.prop('disabled', false);
            submit.html(submit.data('text'));

            if (data.error) {
                modal('#modal-error', {title: __['Error'], message: data.message});
            } else if (data.results) {
                if ($('#do').val() == 'preview_excel') {
                    $('#open_dialog_schedule').html(data.results);
                    $("#open_dialog_schedule").dialog({
                        modal: true,
                        height: 600,
                        width: 1040,
                        closeOnEscape: true,
                        resizable: true,
                        autoOpen: true,
                        buttons: [{
                            text: __['Close'],
                            click: function () {
                                $(this).dialog("close");
                            }
                        }],
                        closeText: __['Close']
                    });
                } else {
                    $('#result_info').html(data.results);
                }
                // submit.addClass('hidden');
                // if (error.is(":visible")) error.hide(); // hide previous alert
            } else {
                eval(data.callback);
            }

        }, 'json').fail(function () {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            submit.prop('disabled', false);
            submit.html(submit.data('text'));

            modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
        });
    });

    //Bắt sự kiện khi upload một file Excel chi tiết lịch học lên
    $(document).on('submit', '#import_menu_excel_form', function (e) {
        e.preventDefault();

        var submit = $('#submit');
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading...']);
        $('#result_info').html('');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.ajax({
            url: api['school/menu'],
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false
        }).done(function (data) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            submit.prop('disabled', false);
            submit.html(submit.data('text'));

            if (data.error) {
                modal('#modal-error', {title: __['Error'], message: data.message});
            } else if (data.results) {
                if ($('#do').val() == 'preview_excel') {
                    $('#open_dialog_menu').html(data.results);
                    $("#open_dialog_menu").dialog({
                        modal: true,
                        height: 600,
                        width: 1040,
                        closeOnEscape: true,
                        resizable: true,
                        autoOpen: true,
                        buttons: [{
                            text: __['Close'],
                            click: function () {
                                $(this).dialog("close");
                            }
                        }],
                        closeText: __['Close']
                    });
                } else {
                    $('#result_info').html(data.results);
                }
                // submit.addClass('hidden');
            } else {
                eval(data.callback);
            }

        }, 'json').fail(function () {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            submit.prop('disabled', false);
            submit.html(submit.data('text'));

            modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
        });
    });

    //Xử lý khi chọn ngày sinh của trẻ
    $('#birthdate_picker').datetimepicker({
        format: DATE_FORMAT
    });
    $('#beginat_datepicker').datetimepicker({
        format: DATE_FORMAT
    });

    $('body').on('click', '#birthdate_dp', (function () {
        $('#birthday').focus();
        $(this).datetimepicker({
            format: DATE_FORMAT
        });
    }));

    $('body').on('focus', '#birthday', (function () {
        $(this).datetimepicker({
            format: DATE_FORMAT
        });
    }));

    $('body').on('click', '#beginat_dp', (function () {
        $('#start_date').focus();
        $(this).datetimepicker({
            format: DATE_FORMAT,
            defaultDate: new Date()
        });
    }));

    $('body').on('focus', '#start_date', (function () {
        $(this).datetimepicker({
            format: DATE_FORMAT
        });
    }));

    //Xử lý khi chọn tự động sinh mã của trẻ
    $('#code_auto').click(function () {
        var child_code = $('#child_code');
        child_code.prop('disabled', this.checked);
    });

    //BEGIN - Màn hình: thêm mới, sửa thông tin lớp ----------------------------------------------------------------------
    /**
     * Tìm kiếm danh sách giáo viên khi gõ vào ô text
     */
    $('body').on('keyup', '#search-teacher', search_delay(function () {
        var query = $(this).val();
        var school_username = $('#school_username').val();

        var user_ids = new Array();
        $("input[name='user_id[]']").each(function () {
            user_ids.push($(this).val());
        });
        if (!is_empty(query)) {
            $('#search-teacher-results').show();
            $.post(api['school/search'], {
                'query': query,
                'school_username': school_username,
                'user_ids': user_ids,
                'func': 'teacher'
            }, function (response) {
                if (response.callback) {
                    eval(response.callback);
                } else if (response.results) {
                    $('#search-teacher-results .dropdown-widget-header').show();
                    $('#search-teacher-results .dropdown-widget-body').html(response.results);
                } else {
                    $('#search-teacher-results .dropdown-widget-header').hide();
                    $('#search-teacher-results .dropdown-widget-body').html(render_template('#search-for', {'query': query}));
                }
            }, 'json');
        }
    }, 1000));

    /**
     * Hiện danh sách xổ xuống khi click vào ô tìm giáo viên
     */
    $('body').on('click', '#search-teacher', function () {
        if ($(this).val() != '') {
            $('#search-teacher-results').show();
        }
    });

    /**
     * Hàm xử lý khi click vào nút Assign trong danh sách giáo viên search ra.
     * Màn hình: thêm mới, sửa thông tin lớp
     */
    $('body').on('click', '.js_teacher-select', function () {
        var user_id = $(this).attr('data-uid');
        var user_ids = new Array();
        $("input[name*='user_id']").each(function () {
            user_ids.push($(this).val());
        });

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/teacherlist'], {
            'user_id': user_id,
            'user_ids': user_ids,
            'func': 'add'
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.results) {
                $('#teacher_list').html(response.results);
                if ($('#telephone').val() == '') {
                    $('#telephone').val(response.phone);
                }
                if ($('#email').val() == '') {
                    $('#email').val(response.email);
                }
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    /**
     * Hàm xử lý khi click vào nút Add trong danh sách giáo viên search ra.
     * Màn hình: phân quyền
     */
    $('body').on('click', '.js_employee-select', function () {
        var user_id = $(this).attr('data-uid');
        var user_ids = new Array();
        $("input[name*='user_id']").each(function () {
            user_ids.push($(this).val());
        });

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/teacherlist'], {
            'user_id': user_id,
            'user_ids': user_ids,
            'func': 'add'
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.results) {
                $('#teacher_list').html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    /**
     * Hàm được gọi khi xóa giáo viên khỏi danh sách phân công lớp.
     * Màn hình: thêm mới, sửa thông tin lớp
     */
    $('body').on('click', '.js_teacher-remove', function () {
        var user_id = $(this).attr('data-uid');
        var user_ids = new Array();
        $("input[name*='user_id']").each(function () {
            user_ids.push($(this).val());
        });

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/teacherlist'], {
            'user_id': user_id,
            'user_ids': user_ids,
            'func': 'remove'
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.results) {
                $('#teacher_list').html(response.results);
                if ($('#telephone').val() == '') {
                    $('#telephone').val(response.phone);
                }
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
            // $('button[type="submit"]').prop('disabled', response.no_data);
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });
    //END- Màn hình: thêm mới, sửa thông tin lớp ------------------------------------------------------------------------

    //BEGIN - Màn hình: xem, sửa thông tin môn học cho khối ----------------------------------------------------------------------
    /**
     * Tìm kiếm danh sách khối khi gõ vào ô text
     */
    $('body').on('keyup', '#search-classlevel', search_delay(function () {
        var query = $(this).val();
        var school_id = $('#school_id').val();

        var classlevel_ids = new Array();
        $("input[name='classlevel_id[]']").each(function () {
            classlevel_ids.push($(this).val());
        });
        if (!is_empty(query)) {
            $('#search-classlevel-results').show();
            $.post(api['school/search'], {
                'query': query,
                'school_id': school_id,
                'classlevel_ids': classlevel_ids,
                'func': 'classlevel'
            }, function (response) {
                if (response.callback) {
                    eval(response.callback);
                } else if (response.results) {
                    $('#search-classlevel-results .dropdown-widget-header').show();
                    $('#search-classlevel-results .dropdown-widget-body').html(response.results);
                } else {
                    $('#search-classlevel-results .dropdown-widget-header').hide();
                    $('#search-classlevel-results .dropdown-widget-body').html(render_template('#search-for', {'query': query}));
                }
            }, 'json');
        }
    }, 1000));

    /**
     * Hiện danh sách xổ xuống khi click vào ô tìm khối
     */
    $('body').on('click', '#search-classlevel', function () {
        if ($(this).val() != '') {
            $('#search-classlevel-results').show();
        }
    });

    /**
     * Hàm xử lý khi click vào nút add trong danh sách khối search ra.
     * Màn hình: sửa thông tin gán môn học vào khối
     */
    $('body').on('click', '.js_classlevel-select', function () {
        var classlevel_id = $(this).attr('data-uid');
        var classlevel_ids = new Array();
        $("input[name*='classlevel_id']").each(function () {
            classlevel_ids.push($(this).val());
        });

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/classlevellist'], {
            'classlevel_id': classlevel_id,
            'classlevel_ids': classlevel_ids,
            'func': 'add'
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.results) {
                $('#class_level_list').html(response.results);
                if ($('#telephone').val() == '') {
                    $('#telephone').val(response.phone);
                }
                if ($('#email').val() == '') {
                    $('#email').val(response.email);
                }
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else {
                window.location.reload();
            }
            // $('button[type="submit"]').prop('disabled', response.no_data);
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // /**
    //  * Hàm xử lý khi click vào nút Add trong danh sách giáo viên search ra.
    //  * Màn hình: phân quyền
    //  */
    // $('body').on('click', '.js_employee-select', function () {
    //     var user_id = $(this).attr('data-uid');
    //     var user_ids = new Array();
    //     $("input[name*='user_id']").each(function () {
    //         user_ids.push($(this).val());
    //     });
    //
    //     // Show loading
    //     $("#loading_full_screen").removeClass('hidden');
    //
    //     $.post(api['school/teacherlist'], {
    //         'user_id': user_id,
    //         'user_ids': user_ids,
    //         'func': 'add'
    //     }, function (response) {
    //         // Hidden loading
    //         $("#loading_full_screen").addClass('hidden');
    //
    //         if (response.results) {
    //             $('#teacher_list').html(response.results);
    //         } else if (response.error) {
    //             modal('#modal-error', {title: __['Error'], message: response.message});
    //         } else {
    //             window.location.reload();
    //         }
    //     }, 'json')
    //         .fail(function () {
    //             // Hidden loading
    //             $("#loading_full_screen").addClass('hidden');
    //
    //             modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
    //         });
    // });

    /**
     * Hàm được gọi khi xóa khối khỏi danh sách add môn vào khối.
     * Màn hình: sửa thông tin khối
     */
    $('body').on('click', '.js_classlevel-remove', function () {
        var classlevel_id = $(this).attr('data-uid');
        var classlevel_ids = new Array();
        $("input[name*='classlevel_id']").each(function () {
            classlevel_ids.push($(this).val());
        });

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/classlevellist'], {
            'classlevel_id': classlevel_id,
            'classlevel_ids': classlevel_ids,
            'func': 'remove'
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.results) {
                $('#class_level_list').html(response.results);
                // if ($('#telephone').val() == '') {
                //     $('#telephone').val(response.phone);
                // }
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
            // $('button[type="submit"]').prop('disabled', response.no_data);
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });
    //END- Màn hình: thêm mới, sửa thông tin môn học cho khối ------------------------------------------------------------------------

    /* hide the contro when clicked outside control */
    $('body').on('click', function (e) {
        /* hide the search-username-results when clicked outside search-username */
        if (!$(e.target).is("#search-username")) {
            $('#search-username-results').hide();
        }
        /* hide the search-teacher-results when clicked outside search-teacher */
        if (!$(e.target).is("#search-teacher")) {
            $('#search-teacher-results').hide();
        }
        if (!$(e.target).is("#search-classlevel")) {
            $('#search-classlevel-results').hide();
        }
        if (!$(e.target).is("#search-parent")) {
            $('#search-parent-results').hide();
        }
    });

    //BEGIN - Phần xử lý trong màn hình tạo mới/sửa sự kiện ------------------------------------------------------------------------
    $('#beginpicker').datetimepicker({
        format: DATETIME_FORMAT,
        sideBySide: true
    });

    $('#beginpickersche').datetimepicker({
        format: DATETIME_FORMAT,
        sideBySide: true
    });

    $('#endpicker').datetimepicker({
        format: DATETIME_FORMAT,
        sideBySide: true
    });
    $('#registration_deadlinepicker').datetimepicker({
        format: DATETIME_FORMAT,
        sideBySide: true
    });
    $("#beginpicker").on("dp.change", function (e) {
        $('#endpicker').data("DateTimePicker").minDate(e.date);
    });
    /*
     $("#endpicker").on("dp.change", function (e) {
     $('#beginpicker').data("DateTimePicker").maxDate(e.date);
     });
     */
    //END - Phần xử lý trong màn hình tạo mới/sửa sự kiện ------------------------------------------------------------------------

    //BEGIN - Phần xử lý trong màn hình tạo mới/sửa menu ------------------------------------------------------------------------
    $('#beginpicker').datetimepicker({
        format: DATETIME_FORMAT,
        sideBySide: true
    });

    $('#endpicker').datetimepicker({
        format: DATETIME_FORMAT,
        sideBySide: true
    });
    $("#beginpicker").on("dp.change", function (e) {
        $('#endpicker').data("DateTimePicker").minDate(e.date);
    });
    /*
     $("#endpicker").on("dp.change", function (e) {
     $('#beginpicker').data("DateTimePicker").maxDate(e.date);
     });
     */
    //END - Phần xử lý trong màn hình tạo mới/sửa menu ------------------------------------------------------------------------

    //BEGIN- Phần xử lý trong màn hình điểm danh ------------------------------------------------------------------------
    $('#fromdatepicker').datetimepicker({
        format: DATE_FORMAT,
        defaultDate: new Date()
    });

    $('#todatepicker').datetimepicker({
        format: DATE_FORMAT,
        defaultDate: new Date()
    });
    $("#fromdatepicker").on("dp.change", function (e) {
        $('#todatepicker').data("DateTimePicker").minDate(e.date);
    });


    //END - Phần xử lý trong màn hình điểm danh ------------------------------------------------------------------------

    //BEGIN - Màn hình: thêm mới, sửa thông tin event/school ------------------------------------------------------------------------
    $('#event_level').on('change', function () {
        var class_level = $('div[name=event_class_level]');
        var _class = $('div[name=event_class]');

        var must_register = $('#must_register');
        var for_teacher = $('#for_teacher');
        var for_child = $('#event_for_child');
        var for_parent = $('#event_for_parent');
        var event_registration_deadline = $('#event_registration_deadline');
        if (must_register.is(':checked')) {
            event_registration_deadline.removeClass('x-hidden');
            $('input[name=registration_deadline]').prop('required', true);
            if (for_teacher.is(':checked')) { //dành cho giáo viên
                for_child.addClass('x-hidden');
                for_parent.addClass('x-hidden');
            } else {
                for_child.removeClass('x-hidden');
                for_parent.removeClass('x-hidden');
            }
        } else {
            event_registration_deadline.addClass('x-hidden');
            for_child.addClass('x-hidden');
            for_parent.addClass('x-hidden');
        }
        // if (for_teacher.is(':checked')) {
        //     if (this.value != 6) { //6 là phạm vi giáo viên
        //         for_child.removeClass('x-hidden');
        //         for_parent.removeClass('x-hidden');
        //     } else {
        //         for_child.addClass('x-hidden');
        //         for_parent.addClass('x-hidden');
        //     }
        // } else {
        //     event_registration_deadline.addClass('x-hidden');
        //     for_child.addClass('x-hidden');
        //     for_parent.addClass('x-hidden');
        // }

        switch (this.value) {
            case '1': //Chọn trường
                if (!class_level.hasClass('x-hidden')) {
                    class_level.addClass('x-hidden');
                }
                if (!_class.hasClass('x-hidden')) {
                    _class.addClass('x-hidden');
                }
                break;
            case '2': //Chọn khối
                if (class_level.hasClass('x-hidden')) {
                    class_level.removeClass('x-hidden');
                }
                if (!_class.hasClass('x-hidden')) {
                    _class.addClass('x-hidden');
                }
                break;
            case '3': //Chọn lớp
                if (!class_level.hasClass('x-hidden')) {
                    class_level.addClass('x-hidden');
                }
                if (_class.hasClass('x-hidden')) {
                    _class.removeClass('x-hidden');
                }
                break;
            case '6': //6 là phạm vi giáo viên
                if (!class_level.hasClass('x-hidden')) {
                    class_level.addClass('x-hidden');
                }
                if (!_class.hasClass('x-hidden')) {
                    _class.addClass('x-hidden');
                }
                break;
            default:
        }
    });
    $('#for_teacher').on('change', function () {
        var level = $('#level');
        var must_register = $('#must_register');
        var for_teacher = $('#for_teacher');
        var for_child = $('#event_for_child');
        var for_parent = $('#event_for_parent');
        var event_registration_deadline = $('#event_registration_deadline');
        if (for_teacher.is(':checked')) {
            level.addClass('x-hidden');
        } else {
            level.removeClass('x-hidden');
        }
        if (must_register.is(':checked')) {
            event_registration_deadline.removeClass('x-hidden');
            if (for_teacher.is(':checked')) { //dành cho giáo viên
                for_child.addClass('x-hidden');
                for_parent.addClass('x-hidden');
            } else {
                for_child.removeClass('x-hidden');
                for_parent.removeClass('x-hidden');
            }
        } else {
            event_registration_deadline.addClass('x-hidden');
            for_child.addClass('x-hidden');
            for_parent.addClass('x-hidden');
        }
        // if (for_teacher.is(':checked')) {
        //     if (this.value != 6) { //6 là phạm vi giáo viên
        //         for_child.removeClass('x-hidden');
        //         for_parent.removeClass('x-hidden');
        //     } else {
        //         for_child.addClass('x-hidden');
        //         for_parent.addClass('x-hidden');
        //     }
        // } else {
        //     event_registration_deadline.addClass('x-hidden');
        //     for_child.addClass('x-hidden');
        //     for_parent.addClass('x-hidden');
        // }
    });

    //END - Màn hình: thêm mới, sửa thông tin event/school ------------------------------------------------------------------------

    //BEGIN - Màn hình: danh sách sự kiện ------------------------------------------------------------------------
    $('body').on('click', '.js_school-event-notify', function () {
        var username = $(this).attr('data-username');
        var id = $(this).attr('data-id');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/event'], {'do': 'notify', 'school_username': username, 'id': id}, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            /* check the response */
            if (response.callback) {
                eval(response.callback);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else if (response.success) {
                modal('#modal-success', {title: __['Success'], message: response.message});
                modal_hidden(timeModal);
                $('.js_school-event-notify').hide();
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    $('body').on('click', '.js_event-more-information', function () {
        var moreInfo = $('#event_more_information');
        var buttonDiv = $('#event_more_information_button');
        moreInfo.removeClass('x-hidden');
        buttonDiv.addClass('x-hidden');
    });

    //END - Màn hình: danh sách sự kiện ------------------------------------------------------------------------

    //BEGIN - Màn hình danh sách người tham gia sự kiện -----------------------------
    $('#eventCheckAll').click(function () {
        if (this.checked) {
            $('.child').prop('checked', this.checked);
            $('.parent').prop('checked', this.checked);
        } else {
            $('.child').removeAttr('checked');
            $('.parent').removeAttr('checked');
        }
    });
    $('#eventCheckAllChildren').click(function () {
        if (this.checked) {
            $('.child').prop('checked', this.checked);
        } else {
            $('.child').removeAttr('checked');
        }
    });
    $('#eventCheckAllParent').click(function () {
        if (this.checked) {
            $('.parent').prop('checked', this.checked);
        } else {
            $('.parent').removeAttr('checked');
        }
    });

    $('#eventCheckAllTeacher').click(function () {
        if (this.checked) {
            $('.teacher').prop('checked', this.checked);
        } else {
            $('.teacher').removeAttr('checked');
        }
    });

    ////Khi chọn level sự kiện
    //$("#event_level").change(function () {
    //    var level = this.value;
    //    var must_register = $('#must_register');
    //    var for_child = $('#event_for_child');
    //    var for_parent = $('#event_for_parent');
    //    var event_registration_deadline = $('#event_registration_deadline');
    //
    //    if (must_register.is(':checked')) {
    //        event_registration_deadline.removeClass('x-hidden');
    //        if (level != 6) { //6 là phạm vi giáo viên
    //            for_child.removeClass('x-hidden');
    //            for_parent.removeClass('x-hidden');
    //        }
    //    } else {
    //        event_registration_deadline.addClass('x-hidden');
    //        for_child.addClass('x-hidden');
    //        for_parent.addClass('x-hidden');
    //    }
    //});

    //Xử lý khi click vào checkbox Phải đăng ký
    $('#must_register').click(function () {
        var for_child = $('#event_for_child');
        var for_parent = $('#event_for_parent');
        var for_teacher = $('#for_teacher');
        var event_registration_deadline = $('#event_registration_deadline');

        if (this.checked) {
            event_registration_deadline.removeClass('x-hidden');
            if (!(for_teacher.is(':checked'))) { //sự kiện dành cho giáo viên
                for_child.removeClass('x-hidden');
                for_parent.removeClass('x-hidden');
            }
        } else {
            event_registration_deadline.addClass('x-hidden');
            if (!for_child.hasClass('x-hidden')) {
                for_child.addClass('x-hidden');
                for_parent.addClass('x-hidden');
            }
        }
    });

    $('body').on('click', '.js_school-event-remove-participant', function () {
        var username = $('#school_username').val();
        var pp_id = $(this).attr('data-uid');
        var event_id = $('#event_id').val();
        var event_name = $('#event_name').val();
        var type = $(this).attr('data-type');
        var name = $(this).attr('data-name');
        var child_id = $(this).attr('data-child');
        var class_id = $('#class_id').val();
        if (!class_id) {
            class_id = $('#participants_class_id option:selected').val();
        }

        confirm(__['Cancel'], __['Are you sure you want to do this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['school/event'], {
                'do': 'reject',
                'school_username': username,
                'event_id': event_id,
                'event_name': event_name,
                'type': type,
                'name': name,
                'class_id': class_id,
                'child_id': child_id,
                'pp_id': pp_id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    //Xử lý khi bấm hủy sự kiện
    $('body').on('click', '.js_school-event-cancel', function () {
        var _this = $(this);
        var username = $(this).attr('data-username');
        var event_id = $(this).attr('data-id');

        confirm(__['Cancel'], __['Are you sure you want to do this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['school/event'], {
                'do': 'cancel',
                'school_username': username,
                'event_id': event_id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else if (response.success) {
                    modal('#modal-success', {title: __['Success'], message: response.message});
                    modal_hidden(timeModal);
                    $('.cancel_hidden_' + event_id).each(function () {
                        _this.hide();
                    });
                    $('.cancel_img_' + event_id).removeClass('x-hidden');
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    $("#participants_class_id").change(function () {
        var username = $(this).attr('data-username');
        var event_id = $(this).attr('data-uid');
        var class_id = this.value;
        var error = $('.alert.alert-danger');
        var success = $('.alert.alert-success');
        var selectPanel = $('#event_select_panel');

        if (success.is(":visible")) success.hide();
        if (error.is(":visible")) error.hide();
        $('#participant_list').html('');
        if (!selectPanel.hasClass("x-hidden")) {
            selectPanel.addClass("x-hidden");
        }
        $('button[type="submit"]').prop('disabled', true);

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/event'], {
            'do': 'search',
            'school_username': username,
            'class_id': class_id,
            'event_id': event_id
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.results) {
                $('#participant_list').html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }

            $('button[type="submit"]').prop('disabled', response.no_edit);
            if (!response.no_edit) {
                selectPanel.removeClass("x-hidden");
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //END - Màn hình danh sách người tham gia sự kiện -----------------------------

    //BEGIN - SCHOOL REPORT - Màn hình gửi mail báo cáo cho phụ huynh trẻ -----------------------------
    //Khi chọn class hiển thị danh sách trẻ tương ứng
    $("#report_class_id").change(function () {
        var username = $(this).attr('data-username');
        var view = $(this).attr('data-view');
        var class_id = this.value;

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        if (class_id == '') {
            $("#loading_full_screen").addClass('hidden');
            $('#list_child').html('');
        } else {
            $.post(api['school/report'], {
                'do': 'list_child',
                'school_username': username,
                'class_id': class_id,
                'view': view
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.results) {
                    $('#list_child').html(response.results);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        }
    });

    $("#re_class_id").change(function () {
        var username = $(this).attr('data-username');
        var view = $(this).attr('data-view');
        var class_id = this.value;

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        if (class_id == '') {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            $('#re_child_id').html('');
        } else {
            $.post(api['school/report'], {
                'do': 'list_child',
                'school_username': username,
                'class_id': class_id,
                'view': view
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.results) {
                    $('#re_child_id').html(response.results);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        }
    });


    // Hiển thị các phạm vi tương ứng
    $('#report_level').on('change', function () {
        var _class = $('div[name=report_class]');
        var child = $('div[name=report_child]');
        switch (this.value) {
            case '1': //Chọn trường
                if (!_class.hasClass('x-hidden')) {
                    _class.addClass('x-hidden');
                }
                if (!child.hasClass('x-hidden')) {
                    child.addClass('x-hidden');
                }
                break;

            case '3': //Chọn lớp
                if (_class.hasClass('x-hidden')) {
                    _class.removeClass('x-hidden');
                }
                if (!child.hasClass('x-hidden')) {
                    child.addClass('x-hidden');
                }
                break;

            case '4': //Chọn trẻ
                if (_class.hasClass('x-hidden')) {
                    _class.removeClass('x-hidden');
                }
                if (child.hasClass('x-hidden')) {
                    child.removeClass('x-hidden');
                }
                break;
            default:
        }
    });


    //BEGIN - Hàm xử lý trong màn hình report ------------------------------------------------------------------------
    $('body').on('click', '.js_school-report-notify', function () {
        var _this = $(this);
        var handle = $(this).attr('data-handle');
        var username = $(this).attr('data-username');
        var id = $(this).attr('data-id');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/report'], {'do': handle, 'school_username': username, 'id': id}, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            /* check the response */
            if (response.callback) {
                eval(response.callback);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else if (response.success) {
                modal('#modal-success', {title: __['Success'], message: response.message});
                modal_hidden(timeModal);
                var count_not_notify = $('.count_not_notify').text();
                count_not_notify = parseInt(count_not_notify);
                count_not_notify = count_not_notify - 1;
                $('.count_not_notify').text(count_not_notify);
                _this.parent('td').html('');
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });
    //END - Hàm xử lý khi bấm Thông báo một notification ------------------------------------------------------------------------

    //BEGIN - Hàm xử lý trong màn hình notification LỊCH HỌC ------------------------------------------------------------------------
    $('body').on('click', '.js_school-schedule-notify', function () {
        var _this = $(this);
        var handle = $(this).attr('data-handle');
        var username = $(this).attr('data-username');
        var id = $(this).attr('data-id');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/schedule'], {'do': handle, 'school_username': username, 'id': id}, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            /* check the response */
            if (response.callback) {
                eval(response.callback);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else if (response.success) {
                modal('#modal-success', {title: __['Success'], message: response.message});
                modal_hidden(timeModal);
                _this.hide();
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });
    //END - Hàm xử lý khi bấm Thông báo một notification ------------------------------------------------------------------------

    //BEGIN - Hàm xử lý trong màn hình notification thực đơn ------------------------------------------------------------------------
    $('body').on('click', '.js_school-menu-notify', function () {
        var _this = $(this);
        var handle = $(this).attr('data-handle');
        var username = $(this).attr('data-username');
        var id = $(this).attr('data-id');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/menu'], {'do': handle, 'school_username': username, 'id': id}, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            /* check the response */
            if (response.callback) {
                eval(response.callback);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else if (response.success) {
                modal('#modal-success', {title: __['Success'], message: response.message});
                modal_hidden(timeModal);
                _this.hide();
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });
    //END - Hàm xử lý khi bấm Thông báo một notification ------------------------------------------------------------------------

    //END - SCHOOL REPORT -------------------------------------------

    /**
     * Tìm kiếm danh sách báo cáo theo điều kiện nhập vào
     */
    $('body').on('click', '.js_report-search', function () {
        var school_username = $('#school_username').val();
        var child_id = $('#re_child_id option:selected').val();
        var class_id = $('#re_class_id option:selected').val();
        var level = $('#report_level option:selected').val();
        var category = $('#category option:selected').val();

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/report'], {
            'child_id': child_id,
            'class_id': class_id,
            'school_username': school_username,
            'level': level,
            'category': category,
            'do': 'search_report'
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            if (response.results) {
                $('#report_list').html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
            //eval($('#child_list').innerHTML);
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Bắt sự kiện khi tạo báo cáo
    $(document).on('submit', '#create_report', function (e) {
        var _this = $(this);
        var submit = _this.find('button[type="submit"]');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        /* show loading */
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading']);

        e.preventDefault();
        $.ajax({
            url: api['school/report'],
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false
        }).done(function (data) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            /* hide loading */
            submit.prop('disabled', false);
            submit.html(submit.data('text'));
            if (data.success) {
                modal('#modal-success', {title: __['Success'], message: data.message});
                $('#template_detail').html('');
                $("#report_template_id option[value='']").prop('selected', true);
                $('.checkbox_child').each(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).prop('disabled', true);
                    }
                });
                modal_hidden(timeModal);
            } else if (data.error) {
                modal('#modal-error', {title: __['Error'], message: data.message});
            } else if (data.callback) {
                eval(data.callback);
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                /* hide loading */
                submit.prop('disabled', false);
                submit.html(submit.data('text'));
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });
    //END - SCHOOL REPORT - Màn hình gửi mail báo cáo cho phụ huynh trẻ ---------------------
    //ADD START MANHDD 05/06/2021
    //START - SCHOOL CONDUCT - Màn hình đánh giá hạnh kiểm học sinh
    /**
     * Tìm kiếm danh sách báo cáo theo điều kiện nhập vào
     */
    $('body').on('click', '.js_conduct-search', function () {
        var school_username = $('#school_username').val();
        // var child_id = $('#re_child_id option:selected').val();
        var class_id = $('#co_class_id option:selected').val();
        var school_year = $('#school_year option:selected').val();
        // var category = $('#category option:selected').val();

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/conduct'], {
            // 'child_id': child_id,
            'class_id': class_id,
            'school_username': school_username,
            'school_year': school_year,
            'do': 'search_conduct'
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            if (response.results) {
                $('#conduct_list').html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
            //eval($('#child_list').innerHTML);
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });
    // bắt sự kiện khi sửa thông tin hạnh kiểm của trẻ
    $(document).on('submit', '#school_edit_conduct', function (e) {
        var _this = $(this);
        var submit = _this.find('button[type="submit"]');

        /* show loading */
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading']);

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        e.preventDefault();
        $.ajax({
            url: api['school/conduct'],
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false
        }).done(function (data) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            /* hide loading */
            submit.prop('disabled', false);
            submit.html(submit.data('text'));
            if (data.success) {
                modal('#modal-success', {title: __['Success'], message: data.message});
                modal_hidden(timeModal);
            } else if (data.error) {
                modal('#modal-error', {title: __['Error'], message: data.message});
            } else if (data.callback) {
                eval(data.callback);
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                /* hide loading */
                submit.prop('disabled', false);
                submit.html(submit.data('text'));
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });
    // ADD END MANHDD 03/06/2021
    //END - SCHOOL CONDUCT - Màn hình đánh giá hạnh kiểm học sinh
    //ADD END MANHDD 05/06/2021
    //BEGIN- SCHOOL - Màn hình gửi thuốc cho trẻ-----------------------------
    //Bắt sự kiện khi ghi nhận cho trẻ uống thuốc hoặc xác nhận giáo viên nhận được tin
    $('body').on('click', '.js_school-medicine', function () {
        var _this = $(this);
        var screen = $(this).attr('data-screen');
        var handle = $(this).attr('data-handle');
        var username = $(this).attr('data-username');
        var id = $(this).attr('data-id');
        var child_id = $(this).attr('data-child');
        var max = $(this).attr('data-max');

        confirm(__['Actions'], __['Are you sure you want to do this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['school/medicine'], {
                'do': handle,
                'screen': screen,
                'school_username': username,
                'child_id': child_id,
                'id': id,
                'max': max
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else if (response.success) {
                    modal('#modal-success', {title: __['Success'], message: response.message});
                    modal_hidden(timeModal);
                    if (handle === "confirm") {
                        _this.hide();
                    }
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    //Hủy một lần gửi thuốc
    $('body').on('click', '.js_school-medicine-cancel', function () {
        var _this = $(this);
        var username = $(this).attr('data-username');
        var id = $(this).attr('data-id');
        var child_id = $(this).attr('data-child');
        var screen = $(this).attr('data-screen');

        confirm(__['Cancel'], __['Are you sure you want to cancel this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['school/medicine'], {
                'do': 'cancel',
                'child_id': child_id,
                'screen': screen,
                'school_username': username,
                'id': id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else if (response.success) {
                    modal('#modal-success', {title: __['Success'], message: response.message});
                    modal_hidden(timeModal);
                    var count_medicine = $('.count_medicine').text();
                    count_medicine = parseInt(count_medicine);
                    count_medicine = count_medicine - 1;
                    $('.count_medicine').text(count_medicine);
                    $('list_medicine_' + id).each(function () {
                        $(this).hide();
                    })
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    //Bắt sự kiện khi click vào Detail
    $('body').on('click', '.js_school-medicine-detail', function () {
        var username = $(this).attr('data-username');
        var id = $(this).attr('data-id');
        var detail = $('#medicine-detail_' + id);
        var btnDetail = $('#button-detail_' + id);

        detail.removeClass('x-hidden');
        btnDetail.addClass('x-hidden');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/medicine'], {'do': 'detail', 'school_username': username, 'id': id}, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            if (response.results) {
                detail.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Khi chọn class hiển thị danh sách trẻ tương ứng
    $("#medicine_class_id").change(function () {
        var username = $(this).attr('data-username');
        var class_id = this.value;

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        if (class_id == '') {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            $('#medicine_child_id').html('');
        } else {
            $.post(api['school/medicine'], {
                'do': 'list_child',
                'school_username': username,
                'class_id': class_id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                if (response.results) {
                    $('#medicine_child_id').html(response.results);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        }
    });

    //Khi chọn một trẻ ở màn hình tạo đơn thuốc
    $("#medicine_child_id").change(function () {
        var child_id = this.value;

        if (child_id > 0) {
            $('button[type="submit"]').prop('disabled', false);
        }
    });
    $('#schedule_beginpicker_new').datetimepicker({
        format: DATE_FORMAT,
        defaultDate: new Date()
    });

    $('#medicine_beginpicker_new').datetimepicker({
        format: DATE_FORMAT,
        defaultDate: new Date()
    });
    $('#medicine_endpicker_new').datetimepicker({
        format: DATE_FORMAT,
        defaultDate: new Date()
    });
    $("#medicine_beginpicker_new").on("dp.change", function (e) {
        $('#medicine_endpicker_new').data("DateTimePicker").minDate(e.date);
    });
    /*$("#medicine_endpicker_new").on("dp.change", function (e) {
        var endDate = new Date(e.date);
        $('#medicine_beginpicker_new').data("DateTimePicker").date(endDate);
    });*/

    $('#medicine_beginpicker').datetimepicker({
        format: DATE_FORMAT
    });
    $('#medicine_endpicker').datetimepicker({
        format: DATE_FORMAT
    });
    $("#medicine_beginpicker").on("dp.change", function (e) {
        $('#medicine_endpicker').data("DateTimePicker").minDate(e.date);
    });
    //END - SCHOOL - Màn hình gửi thuốc cho trẻ-----------------------------

    //BEGIN - Màn hình dashboard -----------------------------------------------------------------------
    $('#medicine_on_dashboard').slimScroll({
        height: '250px'
    });
    //END - Màn hình dashboard -----------------------------------------------------------------------

    //BEGIN - Màn hình: các loại phí của trường--------------------------------------------------------

    $('#fee_level').on('change', function () {
        var class_level = $('div[name=fee_class_level]');
        var _class = $('div[name=fee_class]');
        var _child = $('div[name=fee_child]');
        var _fee_change = $('div[name=fee_change]');
        switch (this.value) {
            case '1': //Chọn trường
                if (!class_level.hasClass('x-hidden')) {
                    class_level.addClass('x-hidden');
                }
                if (!_class.hasClass('x-hidden')) {
                    _class.addClass('x-hidden');
                }
                if (!_child.hasClass('x-hidden')) {
                    _child.addClass('x-hidden');
                }
                if (!_fee_change.hasClass('x-hidden')) {
                    _fee_change.addClass('x-hidden');
                }
                break;
            case '2': //Chọn khối
                if (class_level.hasClass('x-hidden')) {
                    class_level.removeClass('x-hidden');
                }
                if (!_class.hasClass('x-hidden')) {
                    _class.addClass('x-hidden');
                }
                if (!_child.hasClass('x-hidden')) {
                    _child.addClass('x-hidden');
                }
                if (!_fee_change.hasClass('x-hidden')) {
                    _fee_change.addClass('x-hidden');
                }
                break;
            case '3': //Chọn lớp
                if (!class_level.hasClass('x-hidden')) {
                    class_level.addClass('x-hidden');
                }
                if (_class.hasClass('x-hidden')) {
                    _class.removeClass('x-hidden');
                }
                if (!_child.hasClass('x-hidden')) {
                    _child.addClass('x-hidden');
                }
                if (!_fee_change.hasClass('x-hidden')) {
                    _fee_change.addClass('x-hidden');
                }
                break;
            case '5': //Chọn trẻ
                if (!class_level.hasClass('x-hidden')) {
                    class_level.addClass('x-hidden');
                }
                if (_class.hasClass('x-hidden')) {
                    _class.removeClass('x-hidden');
                }
                if (_child.hasClass('x-hidden')) {
                    _child.removeClass('x-hidden');
                }
                if (_fee_change.hasClass('x-hidden')) {
                    _fee_change.removeClass('x-hidden');
                }
                //var prev = $(this).data('val');
                var level = 5;
                var username = $('#school_username').val();
                var remanding_child_ids = $('#remanding_child_ids').val();
                var class_id = $('#fee_class_id').val();
                var checkedValues = $('input:checkbox:checked').map(function () {
                    if (this.value > 0) return this.value;
                }).get();

                // Show loading
                $("#loading_full_screen").removeClass('hidden');

                $('#fee_child_id').html('');
                if (class_id != '') {
                    $.post(api['school/tuition'], {
                        'do': 'fee_list_child',
                        'school_username': username,
                        'class_id': class_id,
                        'remanding_child_ids': remanding_child_ids,
                        'child_ids': checkedValues
                    }, function (response) {
                        // Hidden loading
                        $("#loading_full_screen").addClass('hidden');

                        if (response.results) {
                            $('#fee_child_id').html(response.results);
                        } else if (response.error) {
                            modal('#modal-error', {title: __['Error'], message: response.message});
                        }
                    }, 'json')
                        .fail(function () {
                            // Hidden loading
                            $("#loading_full_screen").addClass('hidden');

                            modal('#modal-message', {
                                title: __['Error'],
                                message: __['There is something that went wrong!']
                            });
                        });
                } else {
                    $("#loading_full_screen").addClass('hidden');
                }
                break;
            default:
        }
    });

    $('body').on('click', '.js_sh_children', function () {
        var fee_id = $(this).attr('data-id');
        var children = $('.children_list_' + fee_id);

        if (!children.hasClass('x-hidden')) {
            children.addClass('x-hidden');
        } else {
            children.removeClass('x-hidden');
        }
        $("html, body").animate({
            scrollTop: children.offset().top - ($(window).height() - children.outerHeight() + 10)
        }, 500);
    });

    $("#fee_class_id").on('focus', function () {
        $(this).data('val', this.value);
    });

    //Khi chọn class hiển thị danh sách trẻ tương ứng
    $("#fee_class_id").change(function () {
        //var prev = $(this).data('val');
        var level = $('#fee_level option:selected').val();
        if (level != 5) return; //CHILD_LEVEL = 5

        var remanding_child_ids = $('#remanding_child_ids').val();
        var username = $(this).attr('data-username');
        var class_id = this.value;
        var checkedValues = $('input:checkbox:checked').map(function () {
            if (this.value > 0) return this.value;
        }).get();

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $('#fee_child_id').html('');
        if (class_id != '') {
            $.post(api['school/tuition'], {
                'do': 'fee_list_child',
                'school_username': username,
                'class_id': class_id,
                'remanding_child_ids': remanding_child_ids,
                'child_ids': checkedValues
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.results) {
                    $('#fee_child_id').html(response.results);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        } else {
            $("#loading_full_screen").addClass('hidden');
        }
        //$(this).data('val', this.value);
    });
    //END - Màn hình: các loại phí của trường--------------------------------------------------------

    //BEGIN - Màn hình: các loại menu của trường--------------------------------------------------------
    $('#menu_level').on('change', function () {
        var class_level = $('div[name=menu_class_level]');
        var _class = $('div[name=menu_class]');
        switch (this.value) {
            case '1': //Chọn trường
                if (!class_level.hasClass('x-hidden')) {
                    class_level.addClass('x-hidden');
                }
                if (!_class.hasClass('x-hidden')) {
                    _class.addClass('x-hidden');
                }
                break;
            case '2': //Chọn khối
                if (class_level.hasClass('x-hidden')) {
                    class_level.removeClass('x-hidden');
                }
                if (!_class.hasClass('x-hidden')) {
                    _class.addClass('x-hidden');
                }
                break;
            case '3': //Chọn lớp
                if (!class_level.hasClass('x-hidden')) {
                    class_level.addClass('x-hidden');
                }
                if (_class.hasClass('x-hidden')) {
                    _class.removeClass('x-hidden');
                }
                break;
            default:
        }
    });
    //END - Màn hình: các loại menu của trường--------------------------------------------------------

    //Hàm học phí một trẻ khỏi học phí lớp
    $('body').on('click', '.js_school-delete-tuition-child', function () {
        var username = $(this).attr('data-username');
        var tuition_child_id = $(this).attr('data-id');

        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['school/tuition'], {
                'do': 'delchild',
                'school_username': username,
                'tuition_child_id': tuition_child_id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    //BEGIN - Màn hình: Học phí--------------------------------------------------------
    $('#month_picker').datetimepicker({
        format: 'MM/YYYY',
        // maxDate: new Date(),
        defaultDate: new Date()
    });

    //Xử lý khi chọn class ở màn hình tạo mới học phí
    $("#tuition_class_id").change(function () {
        var username = $(this).attr('data-username');
        var class_id = this.value;
        var month = $('#month').val();
        var day_of_month = $('#day_of_month').val();
        var error = $('.alert.alert-danger');
        var success = $('.alert.alert-success');

        if (success.is(":visible")) success.hide();
        if (error.is(":visible")) error.hide();
        $('#children_list').html('');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        if (class_id != '' && month.length == 7) {
            $('button[type="submit"]').addClass("x-hidden");
            $('.js_tuition_service_usage').addClass('x-hidden');
            $('#loading').removeClass('x-hidden');

            $.post(api['school/tuition'], {
                'do': 'list_child',
                'month': month,
                'day_of_month': day_of_month,
                'school_username': username,
                'class_id': class_id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $('button[type="submit"]').removeClass("x-hidden");
                $('.js_tuition_service_usage').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');
                $('button[type="submit"]').prop('disabled', response.no_data);

                if (response.results) {
                    $('#children_list').html(response.results);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    $('button[type="submit"]').removeClass("x-hidden");
                    $('.js_tuition_service_usage').removeClass('x-hidden');
                    $('#loading').addClass('x-hidden');
                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        } else {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            $('button[type="submit"]').prop('disabled', true);
        }
    });

    //Xử lý khi reload lại học phí ước tính của một trẻ.
    //Trên màn hình tạo học phí.
    $('body').on('click', '.js_reload_child_tuition', function () {
        var username = $('#school_username').val();
        var month = $('#month').val();
        var day_of_month = $('#day_of_month').val();
        var class_id = $('#tuition_class_id option:selected').val();
        var child_id = $(this).attr('child_id');
        var child_idx = $(this).attr('child_idx');
        var error = $('.alert.alert-danger');
        var success = $('.alert.alert-success');

        if (success.is(":visible")) success.hide();
        if (error.is(":visible")) error.hide();
        $('#child_tuition_' + child_id).html(__['Loading...']);

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        if (class_id != '' && month.length == 7) {
            $('button[type="submit"]').addClass("x-hidden");
            $('.js_tuition_service_usage').addClass('x-hidden');
            $('#loading').removeClass('x-hidden');

            $.post(api['school/tuition'], {
                'do': 'reload_child_tuition',
                'month': month,
                'day_of_month': day_of_month,
                'school_username': username,
                'child_id': child_id,
                'child_idx': child_idx,
                'class_id': class_id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.results) {
                    $('button[type="submit"]').removeClass("x-hidden");
                    $('.js_tuition_service_usage').removeClass('x-hidden');
                    $('#loading').addClass('x-hidden');

                    //Tính toán loại học phí cả lớp (trên màn hình tạo, sửa học phí).
                    $('#child_tuition_' + child_id).html(response.results);

                    function stateChange() {
                        setTimeout(function () {
                            calculateClassTuition();
                        }, 1100);
                    }

                    stateChange();
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    $('button[type="submit"]').removeClass("x-hidden");
                    $('.js_tuition_service_usage').removeClass('x-hidden');
                    $('#loading').addClass('x-hidden');
                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        } else {
            $("#loading_full_screen").addClass('hidden');
        }
    });

    //Tính toán loại học phí cả lớp (trên màn hình tạo, sửa học phí).
    function calculateClassTuition() {
        //Tính tổng học phí của cả lớp
        var total = parseInt(0);
        var num = 0;
        $('input[name="child_total[]"]').each(function () {
            num = !$.isNumeric(parseInt($(this).cleanVal())) ? 0 : parseInt($(this).cleanVal());
            total = total + parseInt($(this).cleanVal());
        });
        total = total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"); // chuyển định dạng
        $('#class_total').val(total);
        $('#class_total_2').val(total);
    }

    //Đóng màn hình popup thêm học phí (trong màn hình tạo học phí)
    $('body').on('click', '.js_tuition_popup_close', function () {
        $("#open_dialog").dialog('close');
    });
    $('body').on('click', '.js_tuition_us_popup_close', function () {
        $("#service_usage_open_dialog").dialog('close');
    });

    //Xóa một trẻ trên màn hình tạo học phí.
    $('body').on('click', '.js_new_tuition_del_child', function () {
        var child_id = $(this).attr('child_id');
        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            $("#child_row_" + child_id).remove();

            //Tính tổng lại học phí của cả lớp
            calculateClassTuition();
            $('#modal').modal('hide');
        });
    });

    //Hiển thị popup để thêm loại phí mới, trong màn hình tạo học phí.
    $('body').on('click', '.js_display_fee_2_add', function () {
        var child_id = $(this).attr('child_id');
        var username = $('#school_username').val();

        //Lấy ra danh sách FEE IDs có trên màn hình
        var feeIdList = new Array();
        $('.fee_id_' + child_id).each(function () {
            feeIdList.push($(this).val());
        });

        //Danh sách ID dịch vụ tính phí tháng hiện tại
        var serviceIdList = new Array();
        $('.service_id_' + child_id).each(function () {
            serviceIdList.push($(this).val());
        });

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $('#open_dialog').html("");
        $.post(api['school/tuition'], {
            'do': 'display_fee_2_add',
            'school_username': username,
            'fee_id_list': feeIdList,
            'service_id_list': serviceIdList,
            'child_id': child_id
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.results) {
                $('#open_dialog').html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }

            $("#open_dialog").dialog({
                modal: true,
                height: 590,
                width: 800,
                closeOnEscape: true,
                resizable: true,
                autoOpen: true
            });
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    $('body').on('click', '.js_tuition_service_usage', function () {
        var class_id = $('#tuition_class_id option:selected').val();
        var username = $('#school_username').val();
        var month = $('#month').val();

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $('#service_usage_open_dialog').html("");
        $.post(api['school/tuition'], {
            'do': 'service_usage',
            'school_username': username,
            'month': month,
            'class_id': class_id
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.results) {
                $('#service_usage_open_dialog').html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }

            $("#service_usage_open_dialog").dialog({
                modal: true,
                height: screen.height - 200,
                width: screen.width - 200,
                closeOnEscape: true,
                resizable: true,
                scrollbars: true,
                autoOpen: true
            });
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Khi click ADD trên màn hình popup khi thêm phí vào học phí
    $('body').on('click', '.js_add_fee_to_tuition', function () {
        var child_id = $(this).attr('child_id');
        var username = $(this).attr('data-username');
        var fee_id_list = $('#fee_id_list').val();
        var service_id_list = $('#service_id_list').val();
        var pickup = $('#pickup').is(":checked");

        var feeIds = new Array();
        $('.fee_id:checked').each(function () {
            feeIds.push($(this).val());
        });

        var serviceIds = new Array();
        $('.service_id:checked').each(function () {
            serviceIds.push($(this).val());
        });

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $("#open_dialog").dialog('close');
        $.post(api['school/tuition'], {
            'do': 'add_fee_to_tuition',
            'school_username': username,
            'fee_id_list': fee_id_list,
            'service_id_list': service_id_list,
            'fee_ids': feeIds,
            'service_ids': serviceIds,
            'pickup': pickup,
            'child_id': child_id
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.str_fee) {
                $('#fee_checkpoint_' + child_id).before(response.str_fee);
            }
            if (response.str_service) {
                $('#service_checkpoint_' + child_id).before(response.str_service);
            }
            if (response.change_amount != 0) {
                //Tính lại học phí của trẻ
                var total = !$.isNumeric(parseInt($('#total_' + child_id).cleanVal())) ? 0 : parseInt($('#total_' + child_id).cleanVal());
                total = total + parseInt(response.change_amount);
                total = total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                $('#total_' + child_id).val(total);

                //Tính lại học phí cả lớp
                total = !$.isNumeric(parseInt($('#class_total').cleanVal())) ? 0 : parseInt($('#class_total').cleanVal());
                total = total + parseInt(response.change_amount);
                total = total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"); // chuyển định dạng
                $('#class_total').val(total);
                $('#class_total_2').val(total);
            }
            if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Bắt sự kiện khi nhập vào số ngày học trong tháng
    $('body').on('change', '#day_of_month', function () {
        var day_of_month = this.value;
        var username = $('#school_username').val();
        var month = $('#month').val();
        var class_id = $('#tuition_class_id option:selected').val();
        var error = $('.alert.alert-danger');
        var success = $('.alert.alert-success');

        if (success.is(":visible")) success.hide();
        if (error.is(":visible")) error.hide();
        $('#children_list').html('');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        if (class_id != "" && month.length == 7 && day_of_month > 0) {
            $('button[type="submit"]').addClass("x-hidden");
            $('.js_tuition_service_usage').addClass('x-hidden');
            $('#loading').removeClass('x-hidden');

            $.post(api['school/tuition'], {
                'do': 'list_child',
                'month': month,
                'school_username': username,
                'class_id': class_id,
                'day_of_month': day_of_month
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.results) {
                    $('#children_list').html(response.results);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
                $('button[type="submit"]').removeClass("x-hidden");
                $('.js_tuition_service_usage').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');
                $('button[type="submit"]').prop('disabled', response.no_data);

            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    $('button[type="submit"]').removeClass("x-hidden");
                    $('.js_tuition_service_usage').removeClass('x-hidden');
                    $('#loading').addClass('x-hidden');
                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        } else {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            $('button[type="submit"]').prop('disabled', true);
        }
    });

    //Bắt sự kiện khi thông tin tháng thay đổi
    $('#month_picker').on("dp.change", function (e) {
        var username = $('#school_username').val();
        var month = $('#month').val();
        var class_id = $('#tuition_class_id option:selected').val();
        var error = $('.alert.alert-danger');
        var success = $('.alert.alert-success');

        if (success.is(":visible")) success.hide();
        if (error.is(":visible")) error.hide();
        $('#children_list').html('');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        if (class_id != '' && month.length == 7) {
            $('button[type="submit"]').addClass("x-hidden");
            $('.js_tuition_service_usage').addClass('x-hidden');
            $('#loading').removeClass('x-hidden');

            $.post(api['school/tuition'], {
                'do': 'list_child',
                'month': month,
                'school_username': username,
                'class_id': class_id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.results) {
                    $('#children_list').html(response.results);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
                $('button[type="submit"]').removeClass("x-hidden");
                $('.js_tuition_service_usage').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');
                $('button[type="submit"]').prop('disabled', response.no_data);
                $('#day_of_month').val(response.attCount);
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    $('button[type="submit"]').removeClass("x-hidden");
                    $('.js_tuition_service_usage').removeClass('x-hidden');
                    $('#loading').addClass('x-hidden');
                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        } else {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            $('button[type="submit"]').prop('disabled', true);
        }
    });

    //Tính học phí tổng cộng của một trẻ, trên màn hình tạo mới, sửa học phí.
    function calculateChildTuition(child_id) {
        var total = parseInt(0);
        var num = 0;
        //Tổng các khoản học phí
        $('input[name=' + 'fee_amount_' + child_id + '\\[\\]' + ']').each(function () {
            num = !$.isNumeric(parseInt($(this).cleanVal())) ? 0 : parseInt($(this).cleanVal());
            total = total + num;
        });
        //Tổng các khoản dịch vụ
        $('input[name=' + 'service_amount_' + child_id + '\\[\\]' + ']').each(function () {
            num = !$.isNumeric(parseInt($(this).cleanVal())) ? 0 : parseInt($(this).cleanVal());
            total = total + num;
        });
        //Cộng thêm khoản nợ học phí tháng trước.
        num = !$.isNumeric(parseInt($('#debt_amount_' + child_id).cleanVal())) ? 0 : parseInt($('#debt_amount_' + child_id).cleanVal());
        total = total + num;
        total = total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        $('#total_' + child_id).val(total);
    }

    //Bắt sự kiện thay đổi số lần sử dụng dịch vụ để tính lại học phí của trẻ trên màn hình
    $('body').on('keyup change mousewheel mouseout', '.service_quantity', function () {
        var child_id = $(this).attr('child_id');
        var service_id = $(this).attr('service_id');

        var quantity = this.value;
        quantity = !$.isNumeric(quantity) ? 0 : parseInt(quantity);

        var deductionQty = $('#service_quantity_deduction_' + child_id + '_' + service_id).val();
        deductionQty = !$.isNumeric(deductionQty) ? 0 : parseInt(deductionQty);

        var servicePrice = $('#service_unit_price_' + child_id + '_' + service_id).cleanVal();
        servicePrice = !$.isNumeric(servicePrice) ? 0 : parseInt(servicePrice);

        var servicePriceDeduction = $('#service_unit_price_deduction_' + child_id + '_' + service_id).val();
        servicePriceDeduction = !$.isNumeric(servicePriceDeduction) ? 0 : parseInt(servicePriceDeduction);

        //Tính thành tiền của mỗi dịch vụ
        var service_amount = quantity * servicePrice - deductionQty * servicePriceDeduction;
        service_amount = service_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

        $('#service_amount_' + child_id + '_' + service_id).val(service_amount);

        //Tính tổng học phí của trẻ
        calculateChildTuition(child_id);

        //Tính tổng học phí của cả lớp
        calculateClassTuition();
    });

    //Xử lý khi thay đổi nợ học phí tháng trước. Trên màn hình tạo, sửa học phí.
    $('body').on('keyup', '.tuition_debt_amount', function () {
        var child_id = $(this).attr('child_id');

        //Tính tổng học phí của trẻ
        calculateChildTuition(child_id);

        //Tính tổng học phí của cả lớp
        calculateClassTuition();
    });

    //Xử lý khi thay đổi số tiền đón muộn trên màn hình học phí.
    $('body').on('keyup', '.tuition_pick_up', function () {
        var child_id = $(this).attr('child_id');
        var pickup = this.value;
        pickup = pickup.replace(/,/g, '');
        pickup = !$.isNumeric(pickup) ? 0 : parseInt(pickup);

        $('#service_unit_price_' + child_id + '_0').val(pickup);

        //Tính tổng học phí của trẻ
        calculateChildTuition(child_id);

        //Tính tổng học phí của cả lớp
        calculateClassTuition();
    });

    //Bắt sự kiện thay đổi số lần sử dụng dịch vụ để tính lại học phí của trẻ phần giảm trừ trên màn hình
    $('body').on('keyup change mousewheel mouseout', '.service_quantity_deduction', function () {
        var child_id = $(this).attr('child_id');
        var service_id = $(this).attr('service_id');

        var quantity = $('#service_quantity_' + child_id + '_' + service_id).val();
        quantity = !$.isNumeric(quantity) ? 0 : parseInt(quantity);

        var deductionQty = this.value;
        deductionQty = !$.isNumeric(deductionQty) ? 0 : parseInt(deductionQty);

        var servicePrice = $('#service_unit_price_' + child_id + '_' + service_id).cleanVal();
        servicePrice = !$.isNumeric(servicePrice) ? 0 : parseInt(servicePrice);

        var servicePriceDeduction = $('#service_unit_price_deduction_' + child_id + '_' + service_id).val();
        servicePriceDeduction = !$.isNumeric(servicePriceDeduction) ? 0 : parseInt(servicePriceDeduction);

        //Tính thành tiền của mỗi dịch vụ
        var service_amount = quantity * servicePrice - deductionQty * servicePriceDeduction;
        service_amount = service_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

        $('#service_amount_' + child_id + '_' + service_id).val(service_amount);

        //Tính tổng học phí của trẻ
        calculateChildTuition(child_id);

        //Tính tổng học phí của cả lớp
        calculateClassTuition();
    });

    //Bắt sự kiện thay đổi phí dịch vụ để tính lại học phí của trẻ trên màn hình
    $('body').on('keyup', '.service_unit_price', function () {
        var child_id = $(this).attr('child_id');
        var service_id = $(this).attr('service_id');
        var serviceType = $('#service_type_' + child_id + '_' + service_id).val();

        var servicePrice = this.value;
        servicePrice = servicePrice.replace(/,/g, ''); // Bỏ dấu ","

        servicePrice = !$.isNumeric(servicePrice) ? 0 : parseInt(servicePrice);

        // Hỏi
        //Nếu serviceType = Count-based
        if (serviceType == 3) {//define("SERVICE_TYPE_COUNT_BASED", 3);
            $('#service_unit_price_deduction_' + child_id + '_' + service_id).val(servicePrice);
        }

        var quantity = $('#service_quantity_' + child_id + '_' + service_id).val();
        quantity = !$.isNumeric(quantity) ? 0 : parseInt(quantity);

        var deductionQty = $('#service_quantity_deduction_' + child_id + '_' + service_id).val();
        deductionQty = !$.isNumeric(deductionQty) ? 0 : parseInt(deductionQty);

        var servicePriceDeduction = $('#service_unit_price_deduction_' + child_id + '_' + service_id).val();
        servicePriceDeduction = !$.isNumeric(servicePriceDeduction) ? 0 : parseInt(servicePriceDeduction);

        //Tính thành tiền của mỗi dịch vụ
        var service_amount = quantity * servicePrice - deductionQty * servicePriceDeduction;
        service_amount = service_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

        $('#service_amount_' + child_id + '_' + service_id).val(service_amount);

        //Tính tổng học phí của trẻ
        calculateChildTuition(child_id);

        //Tính tổng học phí của cả lớp
        calculateClassTuition();
    });

    //Bắt sự kiện thay đổi giảm trừ phí dịch vụ theo tháng
    $('body').on('keyup', '.service_unit_price_deduction', function () {
        var child_id = $(this).attr('child_id');
        var service_id = $(this).attr('service_id');

        var servicePriceDeduction = this.value;
        servicePriceDeduction = servicePriceDeduction.replace(/,/g, '');

        servicePriceDeduction = !$.isNumeric(servicePriceDeduction) ? 0 : parseInt(servicePriceDeduction);

        var servicePrice = $('#service_unit_price_' + child_id + '_' + service_id).cleanVal();
        servicePrice = !$.isNumeric(servicePrice) ? 0 : parseInt(servicePrice);

        //Tính thành tiền của mỗi dịch vụ. Trường hợp này chỉ xảy ra với kiểu THÁNG nên quantity/quantity_deduction luôn = 1
        var service_amount = servicePrice - servicePriceDeduction;
        service_amount = service_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

        $('#service_amount_' + child_id + '_' + service_id).val(service_amount);

        //Tính tổng học phí của trẻ
        calculateChildTuition(child_id);

        //Tính tổng học phí của cả lớp
        calculateClassTuition();
    });

    //Bắt sự kiện thay đổi số lượng để tính lại học phí của trẻ trên màn hình
    $('body').on('keyup change mousewheel mouseout', '.fee_quantity', function () {
        var child_id = $(this).attr('child_id');
        var fee_id = $(this).attr('fee_id');

        var quantity = this.value;
        quantity = !$.isNumeric(quantity) ? 0 : parseInt(quantity);

        var deductionQty = $('#fee_quantity_deduction_' + child_id + '_' + fee_id).val();
        deductionQty = !$.isNumeric(deductionQty) ? 0 : parseInt(deductionQty);

        var price = $('#fee_unit_price_' + child_id + '_' + fee_id).cleanVal();
        price = !$.isNumeric(price) ? 0 : parseInt(price);

        var deductionPrice = $('#fee_unit_price_deduction_' + child_id + '_' + fee_id).val();
        deductionPrice = !$.isNumeric(deductionPrice) ? 0 : parseInt(deductionPrice);

        //Tính thành tiền của mỗi khoản phí
        var fee_amount = quantity * price - deductionQty * deductionPrice;
        // chuyển định dạng
        fee_amount = fee_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

        $('#fee_amount_' + child_id + '_' + fee_id).val(fee_amount);

        //Tính tổng học phí của trẻ
        calculateChildTuition(child_id);

        //Tính tổng học phí của cả lớp
        calculateClassTuition();
    });

    //Bắt sự kiện thay đổi số lượng để tính lại học phí của trẻ trên màn hình phần phí giảm trừ
    $('body').on('keyup change mousewheel mouseout', '.fee_quantity_deduction', function () {
        var child_id = $(this).attr('child_id');
        var fee_id = $(this).attr('fee_id');

        var deductionQty = this.value;
        deductionQty = !$.isNumeric(deductionQty) ? 0 : parseInt(deductionQty);

        var quantity = $('#fee_quantity_' + child_id + '_' + fee_id).val();
        quantity = !$.isNumeric(quantity) ? 0 : parseInt(quantity);

        var price = $('#fee_unit_price_' + child_id + '_' + fee_id).cleanVal();
        price = !$.isNumeric(price) ? 0 : parseInt(price);

        var deductionPrice = $('#fee_unit_price_deduction_' + child_id + '_' + fee_id).val();
        deductionPrice = !$.isNumeric(deductionPrice) ? 0 : parseInt(deductionPrice);

        //Tính thành tiền của mỗi khoản phí
        var fee_amount = quantity * price - deductionQty * deductionPrice;
        // chuyển định dạng
        fee_amount = fee_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

        $('#fee_amount_' + child_id + '_' + fee_id).val(fee_amount);

        //Tính tổng học phí của trẻ
        calculateChildTuition(child_id);

        //Tính tổng học phí của cả lớp
        calculateClassTuition();
    });

    //Bắt sự kiện thay đổi đơn giá để tính lại học phí của trẻ trên màn hình
    $('body').on('keyup', '.fee_unit_price', function () {
        var child_id = $(this).attr('child_id');
        var fee_id = $(this).attr('fee_id');

        var price = this.value;
        price = price.replace(/,/g, ''); // BỎ dâu phẩy trong giá trị lấy được
        price = !$.isNumeric(price) ? 0 : parseInt(price);

        var quantity = $('#fee_quantity_' + child_id + '_' + fee_id).val();
        quantity = !$.isNumeric(quantity) ? 0 : parseInt(quantity);

        var deductionQty = $('#fee_quantity_deduction_' + child_id + '_' + fee_id).val();
        deductionQty = !$.isNumeric(deductionQty) ? 0 : parseInt(deductionQty);

        // Hỏi
        var deductionPrice = $('#fee_unit_price_deduction_' + child_id + '_' + fee_id).val();
        deductionPrice = deductionPrice.replace(/,/g, ''); // BỎ dâu phẩy trong giá trị lấy được
        deductionPrice = !$.isNumeric(deductionPrice) ? 0 : parseInt(deductionPrice);

        //Tính thành tiền của mỗi khoản phí
        var fee_amount = quantity * price - deductionQty * deductionPrice;
        // chuyển định dạng
        fee_amount = fee_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        console.log('fee_amount', quantity);
        console.log('fee_amount', price);
        console.log('fee_amount', deductionQty);
        console.log('fee_amount', deductionPrice);
        console.log('fee_amount', fee_amount);

        $('#fee_amount_' + child_id + '_' + fee_id).val(fee_amount);

        //Tính tổng học phí của trẻ
        calculateChildTuition(child_id);

        //Tính tổng học phí của cả lớp
        calculateClassTuition();
    });

    //Bắt sự kiện thay đổi giảm trừ phí theo tháng
    $('body').on('keyup', '.fee_unit_price_deduction', function () {
        var child_id = $(this).attr('child_id');
        var fee_id = $(this).attr('fee_id');

        var deductionPrice = this.value;
        deductionPrice = deductionPrice.replace(/,/g, ''); // bỏ dấu ","
        deductionPrice = !$.isNumeric(deductionPrice) ? 0 : parseInt(deductionPrice);

        var price = $('#fee_unit_price_' + child_id + '_' + fee_id).cleanVal();
        price = !$.isNumeric(price) ? 0 : parseInt(price);

        //Tính thành tiền của mỗi khoản phí
        //Trường hợp này chỉ xảy ra với kiểu THÁNG nên quantity/quantity_deduction luôn = 1
        var fee_amount = price - deductionPrice;
        // chuyển định dạng
        fee_amount = fee_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

        $('#fee_amount_' + child_id + '_' + fee_id).val(fee_amount);

        //Tính tổng học phí của trẻ
        calculateChildTuition(child_id);

        //Tính tổng học phí của cả lớp
        calculateClassTuition();
    });

    //Hàm xử lý khi xóa trên màn hình tạo học phí
    $('body').on('click', '.js_del_tuition_item', function () {
        var child_id = $(this).attr('child_id');
        $(this).parent().parent().remove();
        //Tính tổng học phí của trẻ
        calculateChildTuition(child_id);

        //Tính tổng học phí của cả lớp
        calculateClassTuition();
    });

    //Ẩn/hiện bảng học phí một trẻ
    $('body').on('click', '.js_show_hide_tuition_tbody', function () {
        var child_id = $(this).attr('child_id');
        var tblAttendance = $('#tbl_attendance_' + child_id);
        var tblFee = $('#tbl_fee_' + child_id);
        tblAttendance.toggle();
        tblFee.toggle();
    });

    //Hàm xử lý khi click vào PAY, CONFIRM, UNCONFIRM, DELETE học phí
    $('body').on('click', '.js_school-tuition', function () {
        var _this = $(this);
        var _parent = $(this).parent('.tuition_paid');
        var handle = $(this).attr('data-handle');
        var username = $(this).attr('data-username');
        var tuition_id = $(this).attr('data-tuition-id');
        var child_id = $(this).attr('data-child-id');
        var id = $(this).attr('data-id');

        var child_amount = $(this).attr('data-amount');
        // var pay_amount = $('#pay_amount_' + child_id).val();
        var pay_amount = _parent.find('#pay_amount_' + child_id).val();

        confirm(__['Actions'], __['Are you sure you want to do this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['school/tuition'], {
                'do': handle,
                'school_username': username,
                'tuition_id': tuition_id,
                'child_id': child_id,
                'child_amount': child_amount,
                'pay_amount': pay_amount,
                'id': id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else if (response.success) {
                    modal('#modal-success', {title: __['Success'], message: response.message});
                    modal_hidden(timeModal);
                    // Xử lý thay đổi trên màn hình
                    if (handle === "confirm") {
                        $('.paid_box_' + id).removeClass('hidden');
                        $('.notpaid_box_' + id).addClass('hidden');
                        $('.parentpaid_box_' + id).addClass('hidden');

                        // Thêm dấu ","
                        child_amount = child_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                        // console.log('child_amount', child_amount);
                        // console.log('pay_amount', pay_amount);
                        //pay_amount = pay_amount.replace(/,/g, ''); // bỏ dấu ","
                        if (child_amount === pay_amount) {
                            $('.paid_amount_' + id).html('');
                        } else {
                            $('.paid_amount_' + id).html('(' + pay_amount + '/' + child_amount + ')');
                        }

                        pay_amount = pay_amount.replace(/,/g, ''); // bỏ dấu ","
                        pay_amount = parseInt(pay_amount);

                        _parent.parent().find('.amount_' + child_id).val(pay_amount);
                        // Lấy giá trị đã thanh toán cả cả lớp
                        var class_paid_amount = $('.class_paid_amount').text();
                        class_paid_amount = class_paid_amount.replace(/,/g, ''); // bỏ dấu ","
                        class_paid_amount = parseInt(class_paid_amount);
                        class_paid_amount = class_paid_amount + pay_amount;
                        class_paid_amount = class_paid_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                        // Set lại giá trị đã thanh toán của cả lớp
                        $('.class_paid_amount').html(class_paid_amount);

                        // Lấy giá trị count của cả lớp
                        var class_paid_count = $('.class_paid_count').text();
                        class_paid_count = parseInt(class_paid_count);
                        class_paid_count = class_paid_count + 1;

                        // set lại giá chị paid count
                        $('.class_paid_count').html(class_paid_count);

                        // Hiển thị hàng đã thanh toán và còn nợ
                        $('.tuition_paid_amount_' + child_id).removeClass('x-hidden');
                        $('.tuition_debt_amount_' + child_id).removeClass('x-hidden');

                        // Set giá trị đã thanh toán và còn nợ
                        child_amount = child_amount.replace(/,/g, ''); // bỏ dấu ","
                        child_amount = parseInt(child_amount);
                        var debt_amount = child_amount - pay_amount;
                        pay_amount = pay_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                        debt_amount = debt_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                        $('.paid_amount_td_' + child_id).text(pay_amount);
                        $('.debt_amount_td_' + child_id).text(debt_amount);
                    } else if (handle === "unconfirm") {
                        $('.paid_box_' + id).addClass('hidden');
                        $('.notpaid_box_' + id).removeClass('hidden');
                        $('.parentpaid_box_' + id).addClass('hidden');
                        var is_parent = _this.attr('data-parent');
                        if (is_parent === "0") {
                            // Lấy giá trị đã thanh toán cả cả lớp
                            var class_paid_amount = $('.class_paid_amount').text();
                            class_paid_amount = class_paid_amount.replace(/,/g, ''); // bỏ dấu ","
                            class_paid_amount = parseInt(class_paid_amount);
                            class_paid_amount = class_paid_amount - pay_amount;
                            class_paid_amount = class_paid_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                            // Set lại giá trị đã thanh toán của cả lớp
                            $('.class_paid_amount').html(class_paid_amount);

                            // Lấy giá trị count của cả lớp
                            var class_paid_count = $('.class_paid_count').text();
                            class_paid_count = parseInt(class_paid_count);
                            class_paid_count = class_paid_count - 1;

                            // set lại giá chị paid count
                            $('.class_paid_count').html(class_paid_count);
                        }
                        child_amount = child_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                        // Set lại giá trị ô input của input confirm
                        _parent.parent().find('.child_total_amount_' + child_id).val(child_amount);
                        // ẩn hàng đã thanh toán và còn nợ
                        $('.tuition_paid_amount_' + child_id).addClass('x-hidden');
                        $('.tuition_debt_amount_' + child_id).addClass('x-hidden');

                    } else if (handle === "remove") {
                        var month = _this.attr('data-month');
                        month = month.replace('/', '_');

                        // Lấy các giá trị thanh toán, tổng và count
                        var paid_month = $('.tuition_paid_month_' + month).text();
                        paid_month = paid_month.replace(/,/g, ''); // bỏ dấu ","
                        paid_month = parseInt(paid_month);
                        var total_month = $('.tuition_total_month_' + month).text();
                        total_month = total_month.replace(/,/g, ''); // bỏ dấu ","
                        total_month = parseInt(total_month);
                        var count_month = $('.tuition_count_month_' + month).text();
                        count_month = count_month.replace(/,/g, ''); // bỏ dấu ","
                        count_month = parseInt(count_month);
                        var paid_list = $('.tuition_paid_list_' + id).html();
                        paid_list = paid_list.replace(/,/g, ''); // bỏ dấu ","
                        paid_list = parseInt(paid_list);
                        var total_list = $('.tuition_total_list_' + id).html();
                        total_list = total_list.replace(/,/g, ''); // bỏ dấu ","
                        total_list = parseInt(total_list);
                        var count_list = $('.tuition_count_list_' + id).html();
                        count_list = count_list.replace(/,/g, ''); // bỏ dấu ","
                        count_list = parseInt(count_list);

                        // Lấy tổng mới
                        paid_month = paid_month - paid_list;
                        total_month = total_month - total_list;
                        count_month = count_month - count_list;

                        // Thêm dấu ","
                        paid_month = paid_month.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                        total_month = total_month.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                        count_month = count_month.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

                        // Trả lại giá trị vào hàng tổng
                        $('.tuition_paid_month_' + month).html(paid_month);
                        $('.tuition_total_month_' + month).html(total_month);
                        $('.tuition_count_month_' + month).html(count_month);
                        _this.closest("tr").hide();
                    } else if (handle === "notify") {
                        _this.hide();
                    }
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    //Bắt sự kiện thay đổi số lượng để tính lại phí sử dụng. Màn hình Quyết toán thôi học
    $('body').on('change', '.tuition4leave_quantity', function () {
        var fee_id = $(this).attr('fee_id');
        var quantity = this.value;
        var price = $('#unit_price_' + fee_id).cleanVal();
        var moneyFee = quantity * price;
        // chuyển định dạng
        moneyFee = moneyFee.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        $('#money_' + fee_id).val(moneyFee);

        //Tính tổng sử dụng của trẻ
        calculateUsageHistory();
    });

    //Bắt sự kiện thay đổi số lượng để tính lại phí sử dụng. Màn hình Quyết toán thôi học
    $('body').on('change', '.class_tuition4leave_quantity', function () {
        var fee_id = $(this).attr('fee_id');
        var child_id = $(this).attr('child-id');
        var quantity = this.value;
        var price = $('#unit_price_' + fee_id + '_' + child_id).cleanVal();
        var moneyFee = quantity * price;
        // chuyển định dạng
        moneyFee = moneyFee.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        $('#money_' + fee_id + '_' + child_id).val(moneyFee);

        //Tính tổng sử dụng của trẻ
        // calculateUsageHistory();
        calculateClassUsageHistory(child_id);
    });

    //Bắt sự kiện thay đổi số lượng để tính lại phí sử dụng. Màn hình Quyết toán thôi học
    $('body').on('change', '.tuition4leave_unit_price', function () {
        var fee_id = $(this).attr('fee_id');
        var price = this.value;
        price = price.replace(/,/g, ''); // bỏ dấu ","
        var quantity = $('#quantity_' + fee_id).val();
        var moneyFee = quantity * price;
        // chuyển định dạng
        moneyFee = moneyFee.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        $('#money_' + fee_id).val(moneyFee);

        //Tính tổng sử dụng của trẻ
        calculateUsageHistory();
    });

    //Bắt sự kiện thay đổi số lượng để tính lại phí sử dụng. Màn hình Tốt nghiệp cho lớp
    $('body').on('change', '.class_tuition4leave_unit_price', function () {
        var fee_id = $(this).attr('fee_id');
        var child_id = $(this).attr('child-id');
        var price = this.value;
        price = price.replace(/,/g, ''); // bỏ dấu ","
        var quantity = $('#quantity_' + fee_id + '_' + child_id).val();
        var moneyFee = quantity * price;
        // chuyển định dạng
        moneyFee = moneyFee.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        $('#money_' + fee_id + '_' + child_id).val(moneyFee);

        //Tính tổng sử dụng của trẻ
        calculateClassUsageHistory(child_id);
    });

    //Bắt sự kiện thay đổi số lượng để tính lại phí sử dụng DỊCH VỤ. Màn hình Quyết toán thôi học
    $('body').on('change', '.tuition4leave_service_quantity', function () {
        var service_id = $(this).attr('service_id');
        var quantity = this.value;
        var price = $('#service_fee_' + service_id).cleanVal();

        var moneyFee = quantity * price;
        // chuyển định dạng
        moneyFee = moneyFee.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        $('#service_amount_' + service_id).val(moneyFee);

        //Tính tổng sử dụng của trẻ
        calculateUsageHistory();
    });

    //Bắt sự kiện thay đổi số lượng để tính lại phí sử dụng DỊCH VỤ. Màn hình Tốt nghiệp của lớp
    $('body').on('change', '.class_tuition4leave_service_quantity', function () {
        var service_id = $(this).attr('service_id');
        var child_id = $(this).attr('child-id');
        var quantity = this.value;
        var price = $('#service_fee_' + service_id + '_' + child_id).cleanVal();

        var moneyFee = quantity * price;
        // chuyển định dạng
        moneyFee = moneyFee.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        $('#service_amount_' + service_id + '_' + child_id).val(moneyFee);

        //Tính tổng sử dụng của trẻ
        calculateClassUsageHistory(child_id);
    });

    //Bắt sự kiện thay đổi đơn giá để tính lại phí sử dụng DỊCH VỤ. Màn hình Quyết toán thôi học
    $('body').on('change', '.tuition4leave_service_fee', function () {
        var service_id = $(this).attr('service_id');
        var price = this.value;
        price = price.replace(/,/g, ''); // bỏ dấu ","
        var quantity = $('#service_quantity_' + service_id).val();

        var moneyFee = quantity * price;
        // chuyển định dạng
        moneyFee = moneyFee.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        $('#service_amount_' + service_id).val(moneyFee);

        //Tính tổng sử dụng của trẻ
        calculateUsageHistory();
    });

    //Bắt sự kiện thay đổi đơn giá để tính lại phí sử dụng DỊCH VỤ. Màn hình Tốt nghiệp lớp
    $('body').on('change', '.class_tuition4leave_service_fee', function () {
        var service_id = $(this).attr('service_id');
        var child_id = $(this).attr('child-id');
        var price = this.value;
        price = price.replace(/,/g, ''); // bỏ dấu ","
        var quantity = $('#service_quantity_' + service_id + '_' + child_id).val();

        var moneyFee = quantity * price;
        // chuyển định dạng
        moneyFee = moneyFee.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        $('#service_amount_' + service_id + '_' + child_id).val(moneyFee);

        //Tính tổng sử dụng của trẻ
        calculateClassUsageHistory(child_id);
    });

    //Bắt sự kiện thay đổi phí đón muộn. Màn hình quyết toán thôi học.
    $('body').on('change', '.tuition4leave_pick_up', function () {
        var amount = this.value;
        amount = amount.replace(/,/g, ''); // bỏ dấu ","
        if (Number.isInteger(amount)) {
            $('#service_fee_0').val(amount);

            //Tính tổng sử dụng của trẻ
            calculateUsageHistory();
        }
    });

    //Bắt sự kiện thay đổi phí đón muộn. Màn hình Tốt nghiệp lớp
    $('body').on('change', '.class_tuition4leave_pick_up', function () {
        var amount = this.value;
        var child_id = $(this).attr('child-id');
        amount = amount.replace(/,/g, ''); // bỏ dấu ","

        if (Number.isInteger(amount)) {
            $('#service_fee_0' + '_' + child_id).val(amount);
            //Tính tổng sử dụng của trẻ
            calculateClassUsageHistory(child_id);
        }
    });

    //Tính tổng phí sử dụng của trẻ trên màn hình Quyết toán thôi học.
    function calculateUsageHistory() {
        var total = parseInt(0);

        //Tổng các khoản học phí
        $('input[name=' + 'money\\[\\]' + ']').each(function () {
            total = total + parseInt($(this).cleanVal());
        });
        //Tổng các khoản dịch vụ
        $('input[name=' + 'service_amount\\[\\]' + ']').each(function () {
            total = total + parseInt($(this).cleanVal());
        });

        //var status = $('#status').val();
        //if (status > 0) {
        //Trừ khi khoản đã trả đầu tháng.
        total = total + parseInt($('#debt_amount').cleanVal()) - parseInt($('#total_deduction').cleanVal()) - parseInt($('#paid_amount').cleanVal());
        //}
        // chuyển định dạng
        total = total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        $('#final_amount').val(total);
    }

    //Tính tổng phí sử dụng của trẻ trên màn hình Quyết toán thôi học.
    function calculateClassUsageHistory(child_id) {
        var total = parseInt(0);

        //Tổng các khoản học phí
        $('input[name=' + 'money_' + child_id + '_' + '\\[\\]' + ']').each(function () {
            total = total + parseInt($(this).cleanVal());
        });
        //Tổng các khoản dịch vụ
        $('input[name=' + 'service_amount_' + child_id + '_' + '\\[\\]' + ']').each(function () {
            total = total + parseInt($(this).cleanVal());
        });

        //var status = $('#status').val();
        //if (status > 0) {
        //Trừ khi khoản đã trả đầu tháng.
        total = total + parseInt($('#debt_amount_' + child_id).cleanVal()) - parseInt($('#total_deduction_' + child_id).cleanVal()) - parseInt($('#paid_amount_' + child_id).cleanVal());
        //}
        // chuyển định dạng
        total = total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        $('#final_amount_' + child_id).val(total);
    }

    //Hàm xử lý khi click vào Export học phí
    $('body').on('click', '.js_school-tuition-export', function () {
        var handle = $(this).attr('data-handle');
        var username = $(this).attr('data-username');
        var child_amount = $(this).attr('data-amount');
        var tuition_id = $(this).attr('data-tuition-id');
        var child_id = $(this).attr('data-child-id');
        var id = $(this).attr('data-id');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $(".js_school-tuition-export").addClass("x-hidden");
        $(".js_school-delete-tuition-child").addClass("x-hidden");
        $(".js_school-tuition").addClass("x-hidden");
        $(".processing_label").removeClass("x-hidden");

        $.post(api['school/tuition'], {
            'do': handle,
            'school_username': username,
            'tuition_id': tuition_id,
            'child_id': child_id,
            'child_amount': child_amount,
            'id': id
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            $(".js_school-tuition-export").removeClass("x-hidden");
            $(".js_school-delete-tuition-child").removeClass("x-hidden");
            $(".js_school-tuition").removeClass("x-hidden");
            $(".processing_label").addClass("x-hidden");
            var $a = $("<a>");
            $a.attr("href", response.file);
            $("body").append($a);
            $a.attr("download", response.file_name);
            $a[0].click();
            $a.remove();
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $(".js_school-tuition-export").removeClass("x-hidden");
                $(".js_school-delete-tuition-child").removeClass("x-hidden");
                $(".js_school-tuition").removeClass("x-hidden");
                $(".processing_label").addClass("x-hidden");
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //END - Màn hình: Học phí--------------------------------------------------------

    //Xử lý khi export thông tin quyết toán thôi học ra Excel
    $('body').on('click', '.js_school-export-account-4leave', function () {
        var username = $(this).attr('data-username');
        var child_id = $(this).attr('data-id');

        $("#export_to_excel_" + child_id).addClass("x-hidden");
        $("#processing_label_" + child_id).removeClass("x-hidden");

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/child'], {
            'do': 'export_4leaving',
            'school_username': username,
            'child_id': child_id
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            $("#export_to_excel_" + child_id).removeClass("x-hidden");
            $("#processing_label_" + child_id).addClass("x-hidden");
            var $a = $("<a>");
            $a.attr("href", response.file);
            $("body").append($a);
            $a.attr("download", response.file_name);
            $a[0].click();
            $a.remove();
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $("#export_to_excel_" + child_id).removeClass("x-hidden");
                $("#processing_label_" + child_id).addClass("x-hidden");
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //BEGIN - Màn hình Điểm danh ----------------------------------------
    $('body').on('click', '.js_attendance-search', function () {
        var username = $(this).attr('data-username');
        var fromDate = $('#fromDate').val();
        var toDate = $('#toDate').val();
        var class_id = $('#class_id option:selected').val();
        var attendance_list = $('#attendance_list');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/attendance'], {
            'do': 'search',
            'school_username': username,
            'class_id': class_id,
            'fromDate': fromDate,
            'toDate': toDate
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.results) {
                attendance_list.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });


    $('#att_month_picker').datetimepicker({
        format: 'MM/YYYY',
        maxDate: new Date(),
        defaultDate: new Date()
    });

    //Xử lý khi chọn lớp trong màn hình điểm danh cả lớp
    $("#classatt_class_id").change(function () {
        $('#attendance_list').html('');
        var class_id = this.value;
        var btnSearch = $('#search');
        var btnExportExcel = $('#export_excel');

        if (class_id == '') {
            btnSearch.attr('disabled', true);
            btnExportExcel.attr('disabled', true);
        } else {
            btnSearch.attr('disabled', false);
            btnExportExcel.attr('disabled', false);
        }
    });

    //Hiện điểm danh cả lớp trong một khoảng ngày
    $('body').on('click', '.js_attendance-search-class', function () {
        var username = $(this).attr('data-username');
        var fromDate = $('#fromDate').val();
        var toDate = $('#toDate').val();
        var class_id = $('#classatt_class_id option:selected').val();
        var attendance_list = $('#attendance_list');

        attendance_list.html('');
        $('#search').addClass('x-hidden');
        $('#export_excel').addClass('x-hidden');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/attendance'], {
            'do': 'classatt',
            'school_username': username,
            'class_id': class_id,
            'fromDate': fromDate,
            'toDate': toDate
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            $('#search').removeClass('x-hidden');
            $('#export_excel').removeClass('x-hidden');
            $('#loading').addClass('x-hidden');
            if (response.results) {
                attendance_list.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $('#search').removeClass('x-hidden');
                $('#export_excel').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Setting cho picker chọn ngày điểm danh trên màn hình ĐIỂM DANH NGÀY
    $('#attendance_picker').datetimepicker({
        format: DATE_FORMAT,
        defaultDate: new Date(),
        maxDate: new Date()
    });

    //Xử lý khi chọn ngày điểm danh trên màn hình ĐIỂM DANH NGÀY
    $("#attendance_picker").on("dp.change", function (e) {
        var class_id = $('#attendance_rollup_class_id option:selected').val();
        var btnSearch = $('#search');
        var btnExportExcel = $('#export_excel');

        $('#attendance_list').html('');
        $('button[type="submit"]').prop('disabled', true);
        if (class_id == '') {
            btnSearch.attr('disabled', true);
            btnExportExcel.attr('disabled', true);
        } else {
            btnSearch.attr('disabled', false);
            btnExportExcel.attr('disabled', false);
        }
    });

    //Xử lý khi chọn lớp trên màn hình 'điểm danh ngày'
    $("#attendance_rollup_class_id").change(function () {
        var class_id = this.value;
        var btnSearch = $('#search');
        var btnExportExcel = $('#export_excel');

        $('#attendance_list').html('');
        $('button[type="submit"]').prop('disabled', true);
        if (class_id == '') {
            btnSearch.attr('disabled', true);
            btnExportExcel.attr('disabled', true);
        } else {
            btnSearch.attr('disabled', false);
            btnExportExcel.attr('disabled', false);
        }
    });

    //Hiện danh sách lớp để thực hiện điểm danh khi bấm nút 'Search'
    $('body').on('click', '.js_attendance-search-4rollup', function () {
        var username = $(this).attr('data-username');
        var attendanceDate = $('#attendance_date').val();
        var class_id = $('#attendance_rollup_class_id option:selected').val();
        var attendance_list = $('#attendance_list');
        var error = $('.alert.alert-danger');
        var success = $('.alert.alert-success');

        if (class_id != '') {
            if (success.is(":visible")) success.hide();
            if (error.is(":visible")) error.hide();
            attendance_list.html('');
            $('button[type="submit"]').prop('disabled', true);
            $('#search').addClass('x-hidden');
            $('#export_excel').addClass('x-hidden');

            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['school/attendance'], {
                'do': 'list_4rollup',
                'school_username': username,
                'class_id': class_id,
                'attendance_date': attendanceDate
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $('#search').removeClass('x-hidden');
                $('#export_excel').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');
                if (response.results) {
                    attendance_list.html(response.results);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
                $('button[type="submit"]').prop('disabled', response.disableSave);
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    $('#search').removeClass('x-hidden');
                    $('#export_excel').removeClass('x-hidden');
                    $('#loading').addClass('x-hidden');
                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        }

    });

    //Hàm xóa một ngày điểm danh của lớp
    $('body').on('click', '.js_school-delete-attendance', function () {
        var username = $(this).attr('data-username');
        var attendance_id = $(this).attr('data-id');
        var fromDate = $('#fromDate').val();
        var toDate = $('#toDate').val();
        var class_id = $('#classatt_class_id option:selected').val();
        var attendance_list = $('#attendance_list');

        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            attendance_list.html('');
            $('#search').addClass('x-hidden');
            $('#export_excel').addClass('x-hidden');
            $('#loading').removeClass('x-hidden');

            $.post(api['school/attendance'], {
                'do': 'delete',
                'school_username': username,
                'attendance_id': attendance_id,
                'class_id': class_id,
                'fromDate': fromDate,
                'toDate': toDate
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $('#search').removeClass('x-hidden');
                $('#export_excel').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');
                if (response.results) {
                    attendance_list.html(response.results);
                    $('#modal').modal('hide');
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    $('#search').removeClass('x-hidden');
                    $('#export_excel').removeClass('x-hidden');
                    $('#loading').addClass('x-hidden');
                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    $('#att_aday_picker').datetimepicker({
        format: DATE_FORMAT,
        maxDate: new Date()
    });

    //Hiển thị thông tin điểm danh cả trường trong màn hình tổng hợp.
    //Hàm được gọi khi chọn ngày tương ứng.
    $("#att_aday_picker").on("dp.change", function (e) {
        var username = $('#username').val();
        var attendanceDate = $('#attendanceDate').val();
        var attendance_list = $('#attendance_list');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        attendance_list.html('');
        $('#loading').removeClass('x-hidden');

        $.post(api['school/attendance'], {
            'do': 'search_aday',
            'school_username': username,
            'attendanceDate': attendanceDate
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            $('#loading').addClass('x-hidden');
            if (response.results) {
                attendance_list.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $('#loading').addClass('x-hidden');
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Xử lý khi chọn lớp ở màn hình điểm danh trẻ
    $("#attendance_class_id").change(function () {
        var username = $(this).attr('data-username');
        var class_id = this.value;

        $('#search').attr('disabled', true);

        $('#attendance_list').html('');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        if (class_id == '') {
            $('#loading_full_screen').addClass('hidden');
            $('#attendance_child_id').html('');
        } else {
            $.post(api['school/pickup'], {
                'do': 'list_child',
                'school_username': username,
                'class_id': class_id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $('#search').attr('disabled', response.disableSearch);
                if (response.results) {
                    $('#attendance_child_id').html(response.results);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        }
    });

    $("#attendance_child_id").change(function () {
        $('#attendance_list').html('');
    });

    //Khi tìm kiếm điểm danh trẻ trong một khoảng thời gian.
    $('body').on('click', '.js_school-attendance_child', function () {
        var username = $(this).attr('data-username');
        var child_id = $('#attendance_child_id option:selected').val();
        var fromDate = $('#fromDate').val();
        var toDate = $('#toDate').val();
        var attendance_list = $('#attendance_list');
        var error = $('.alert.alert-danger');
        var success = $('.alert.alert-success');

        attendance_list.html('');
        $('button[type="submit"]').prop('disabled', true);
        $('#search').addClass('x-hidden');
        $('#loading').removeClass('x-hidden');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        if (success.is(":visible")) success.hide();
        if (error.is(":visible")) error.hide();

        $.post(api['school/attendance'], {
            'do': 'child',
            'school_username': username,
            'child_id': child_id,
            'fromDate': fromDate,
            'toDate': toDate
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            $('#search').removeClass('x-hidden');
            $('#loading').addClass('x-hidden');
            if (response.results) {
                attendance_list.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
            $('button[type="submit"]').prop('disabled', response.disableSave);
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $('#search').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Xử lý khi tìm trẻ nghỉ liên tục
    $('body').on('click', '.js_attendance-search-conabs', function () {
        var username = $(this).attr('data-username');
        var daynum = $('#daynum').val();
        var attendance_list = $('#attendance_list');

        attendance_list.html('');
        $('#search').addClass('x-hidden');
        $('#loading').removeClass('x-hidden');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/attendance'], {
            'do': 'conabs',
            'daynum': daynum,
            'school_username': username
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            $('#search').removeClass('x-hidden');
            $('#loading').addClass('x-hidden');
            if (response.results) {
                attendance_list.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $('#search').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });
    //END - Màn hình Điểm danh ----------------------------------------

    //BEGIN - Màn hình dịch vụ
    $('body').on('click', '.js_school_delete-mdservice-registration', function () {
        var school_username = $('#school_username').val();
        var child_id = $(this).attr('data-id');
        var service_id = $('#reg_service_id option:selected').val();
        var class_id = $('#class_id option:selected').val();

        confirm(__['Delete'], __['This function is only used to delete fault service registration.'] + " " + __['Are you sure you want to delete this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['school/service'], {
                'do': 'del_mdservice_registration',
                'school_username': school_username,
                'child_id': child_id,
                'class_id': class_id,
                'service_id': service_id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                if (response.results) {
                    $('#child_list').html(response.results);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
            $('#modal').modal('hide');
        });
    });

    $('body').on('click', '.js_school-service-delete', function () {
        var school_username = $(this).attr('data-username');
        var service_id = $(this).attr('data-id');

        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['school/service'], {
                'do': 'del_service',
                'school_username': school_username,
                'service_id': service_id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else if (response.success) {
                    modal('#modal-success', {title: __['Success'], message: response.message});
                    modal_hidden(timeModal);
                    var count_service = $('.count_service').text();
                    count_service = parseInt(count_service);
                    count_service = count_service - 1;
                    $('.count_service').text(count_service);
                    //_this.closest("tr").hide();
                    $('.list_service_' + service_id).each(function () {
                        $(this).hide();
                    });
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    // Xử lý inactive dịch vụ
    $('body').on('click', '.js_school-service-cancel', function () {
        var school_username = $(this).attr('data-username');
        var service_id = $(this).attr('data-id');

        confirm(__['Actions'], __['Are you sure you want to do this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['school/service'], {
                'do': 'cancel_service',
                'school_username': school_username,
                'service_id': service_id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else if (response.success) {
                    modal('#modal-success', {title: __['Success'], message: response.message});
                    modal_hidden(timeModal);
                    var count_service = $('.count_service').text();
                    count_service = parseInt(count_service);
                    count_service = count_service - 1;
                    $('.count_service').text(count_service);
                    //_this.closest("tr").hide();
                    $('.list_service_' + service_id).each(function () {
                        $(this).hide();
                    });
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    //Trên màn hình Đăng ký dịch vụ, khi người chọn một loại dịch vụ
    $("#reg_service_id").change(function () {
        var service_type = $('#reg_service_id option:selected').attr('data-type');
        var service_id = this.value;
        var error = $('.alert.alert-danger');
        var success = $('.alert.alert-success');

        if (success.is(":visible")) success.hide();
        if (error.is(":visible")) error.hide();

        $('#child_list').html('');
        $('button[type="submit"]').prop('disabled', true);
        if (service_id == '') {
            $('#search').attr('disabled', true);
            $('#using_at_div').addClass('x-hidden');
        } else {
            $('#search').attr('disabled', false);
            if (service_type != 3) {
                $('#using_at_div').addClass('x-hidden');
            } else {
                $('#using_at_div').removeClass('x-hidden');
            }
        }
    });

    //Khi chọn ô check Tất cả trên màn hình đăng ký dịch vụ
    $('#serviceCheckAll').click(function () {
        if (this.checked) {
            $('.child').prop('checked', this.checked);
        } else {
            $('.child').removeAttr('checked');
        }
    });

    //Liệt kê danh sách trẻ để đăng ký dịch vụ
    $('body').on('click', '.js_service-search-reg', function () {
        var school_username = $(this).attr('data-username');
        var using_at = '';
        var class_id = $('#class_id option:selected').val();
        var service_id = $('#reg_service_id option:selected').val();
        var service_type = $('#reg_service_id option:selected').attr('data-type');
        // var error = $('.alert.alert-danger');
        // var success = $('.alert.alert-success');

        $('#child_list').html('');
        $('button[type="submit"]').prop('disabled', true);
        $('#search').addClass('x-hidden');
        $('#loading').removeClass('x-hidden');
        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        var action = '';
        if (service_type == 3) {
            action = 'list_child_4record';
            using_at = $('#using_at').val();
            $('#do').val('record');
        } else {
            action = 'list_child_4regmdservice';
            $('#do').val('register');
        }

        // if (success.is(":visible")) success.hide();
        // if (error.is(":visible")) error.hide();
        $.post(api['school/service'], {
            'service_id': service_id,
            'service_type': service_type,
            'using_at': using_at,
            'class_id': class_id,
            'school_username': school_username,
            'do': action
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            $('#search').removeClass('x-hidden');
            $('#loading').addClass('x-hidden');
            if (response.results) {
                $('#child_list').html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
            $('button[type="submit"]').prop('disabled', response.disableSave);
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $('#search').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Xử lý khi search danh sách trẻ để ghi chú giảm trừ.
    $('body').on('click', '.js_service-search-deduction', function () {
        var school_username = $(this).attr('data-username');
        var deduction_date = $('#deduction_date').val();
        var class_id = $('#class_id option:selected').val();
        var service_id = $('#service_id option:selected').val();
        var error = $('.alert.alert-danger');
        var success = $('.alert.alert-success');

        $('#child_list').html('');
        $('button[type="submit"]').prop('disabled', true);
        $('#search').addClass('x-hidden');
        $('#loading').removeClass('x-hidden');
        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        if (success.is(":visible")) success.hide();
        if (error.is(":visible")) error.hide();
        $.post(api['school/service'], {
            'service_id': service_id,
            'deduction_date': deduction_date,
            'class_id': class_id,
            'school_username': school_username,
            'do': 'list_child_4deduction'
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            $('#search').removeClass('x-hidden');
            $('#loading').addClass('x-hidden');
            if (response.results) {
                $('#child_list').html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
            $('button[type="submit"]').prop('disabled', response.disableSave);
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $('#search').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    $('#using_time_picker').datetimepicker({
        format: DATE_FORMAT,
        defaultDate: new Date()
    });

    $('.reg_service_begin').mask('00/00/0000');
    $('.reg_service_end').mask('00/00/0000');

    $('#history_begin_picker').datetimepicker({
        format: DATE_FORMAT,
        defaultDate: new Date()
    });
    $('#history_begin_picker_food').datetimepicker({
        format: DATE_FORMAT,
        defaultDate: new Date()
    });

    $('#history_end_picker').datetimepicker({
        format: DATE_FORMAT,
        defaultDate: new Date()
    });

    //Khi chọn lớp thì hiển thị danh sách trẻ trong màn hình lịch sử sử dụng dịch vụ
    $("#service_class_id").change(function () {
        var username = $(this).attr('data-username');
        var class_id = this.value;

        $('#usage_history').html('');
        $('#service_child_id').html('');

        if (class_id > 0) {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');
            $('#search').addClass('x-hidden');
            $('#loading').removeClass('x-hidden');

            $.post(api['school/service'], {
                'do': 'list_child',
                'school_username': username,
                'class_id': class_id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $('#search').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');
                if (response.results) {
                    $('#service_child_id').html(response.results);
                    $('#service_child_id').removeClass('x-hidden');
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    $('#search').removeClass('x-hidden');
                    $('#loading').addClass('x-hidden');
                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        }
    });

    //Tìm kiếm lịch sử sử dụng 1 dịch vụ của một trẻ
    $('body').on('click', '.js_service-search-history', function () {
        var school_username = $(this).attr('data-username');
        var begin = $('#begin').val();
        var end = $('#end').val();
        var class_id = $('#service_class_id option:selected').val();
        var child_id = $('#service_child_id option:selected').val();
        var service_id = $('#service_id option:selected').val();
        var service_type = $('#service_id option:selected').attr('data-type');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $('#usage_history').html('');
        $('#search').addClass('x-hidden');
        $('#loading').removeClass('x-hidden');

        $.post(api['school/service'], {
            'service_id': service_id,
            'service_type': service_type,
            'class_id': class_id,
            'child_id': child_id,
            'begin': begin,
            'end': end,
            'school_username': school_username,
            'do': 'history'
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            $('#search').removeClass('x-hidden');
            $('#loading').addClass('x-hidden');
            $('#usage_history').removeClass('hidden');
            if (response.results) {
                $('#usage_history').html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $('#search').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Tìm kiếm lịch sử sử dụng theo số lần sử dụng của một lớp
    $('body').on('click', '.js_service-search-class', function () {
        var school_username = $(this).attr('data-username');
        var date = $('#date').val();
        var class_id = $('#class_id option:selected').val();

        $('#usage_class').html('');
        $('#search').addClass('x-hidden');
        $('#loading').removeClass('x-hidden');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/service'], {
            'class_id': class_id,
            'date': date,
            'school_username': school_username,
            'do': 'class'
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            $('#search').removeClass('x-hidden');
            $('#loading').addClass('x-hidden');
            if (response.results) {
                $('#usage_class').html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $('#search').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Thông kê ăn uống theo ngày
    // $('body').on('change', '#date', function () {
    $('#history_begin_picker_food').on("dp.change", function (e) {
        //alert('test');
        var username = $('#school_username').val();
        var date = $('#date').val();
        $('#foodsvr').html('');
        $('#search').addClass('x-hidden');
        $('#loading').removeClass('x-hidden');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/service'], {
            'do': 'foodsvr',
            'school_username': username,
            'date': date
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            $('#search').removeClass('x-hidden');
            $('#loading').addClass('x-hidden');
            if (response.results) {
                $('#foodsvr').html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $('#search').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //END - Màn hình dịch vụ

    //Tìm kiếm sinh nhật của một trẻ từ ngày đến ngày
    $('body').on('click', '.js_birthday-search', function () {
        var begin = $('#begin').val();
        var end = $('#end').val();
        var school_username = $(this).attr('data-username');
        var handle = $(this).attr('data-handle');
        var birthday_list = $('#birthday');
        birthday_list.html('');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/birthday'], {
            'begin': begin,
            'end': end,
            'school_username': school_username,
            'do': handle
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            if (response.results) {
                birthday_list.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
            // $('#all_month').addClass('x-hidden');
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Xử lý khi chọn thời gian ở màn hình xem sinh nhật
    $("#birthday_time_id").change(function () {
        var time = this.value;
        var school_username = $(this).attr('data-username');
        var handle = $(this).attr('data-handle');
        var class_id = $("#birthday_class_id").val();
        var birthday_list = $('#birthday');
        birthday_list.html('');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/birthday'], {
            'do': handle,
            'school_username': school_username,
            'time': time,
            'class_id': class_id
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.results) {
                birthday_list.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Xử lý khi bấm tạo vai trò mặc định.
    $('body').on('click', '.js_school-default-role', function () {
        var username = $(this).attr('data-username');

        $('#default').addClass('x-hidden');
        $('#processing').removeClass('x-hidden');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/role'], {
            'do': 'default',
            'school_username': username
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.callback) {
                eval(response.callback);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $('#default').removeClass('x-hidden');
                $('#procession').addClass('x-hidden');
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // bắt sự kiện khi tạo đơn thuốc
    $(document).on('submit', '#create_medicine', function (e) {
        var _this = $(this);
        var submit = _this.find('button[type="submit"]');

        /* show loading */
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading']);

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        e.preventDefault();
        $.ajax({
            url: api['school/medicine'],
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false
        }).done(function (data) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            /* hide loading */
            submit.prop('disabled', false);
            submit.html(submit.data('text'));
            if (data.success) {
                modal('#modal-success', {title: __['Success'], message: data.message});
                modal_hidden(timeModal);
                $("#medicine_list_clear").val("");
                $("#guide_clear").val("");
            } else if (data.error) {
                modal('#modal-error', {title: __['Error'], message: data.message});
            } else if (data.callback) {
                eval(data.callback);
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                /* hide loading */
                submit.prop('disabled', false);
                submit.html(submit.data('text'));
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //BEGIN - Màn hình Đóng góp ý kiến
    $('body').on('click', '.js_school-feedback', function () {
        var _this = $(this);
        var handle = $(this).attr('data-handle');
        var school_username = $(this).attr('data-username');
        var feedback_id = $(this).attr('data-id');

        confirm(__['Actions'], __['Are you sure you want to do this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['school/feedback'], {
                'do': handle,
                'school_username': school_username,
                'feedback_id': feedback_id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else if (response.success) {
                    modal('#modal-success', {title: __['Success'], message: response.message});
                    modal_hidden(timeModal);
                    if (handle === "confirm") {
                        _this.hide();
                        $('.feddback_confirmed_' + feedback_id).removeClass('x-hidden');
                    } else if (handle === "delete") {
                        var count_feedback = $('.count_feedback').text();
                        count_feedback = parseInt(count_feedback);
                        count_feedback = count_feedback - 1;
                        $('.count_feedback').text(count_feedback);
                        //_this.closest("tr").hide();
                        $('.list_feedback_' + feedback_id).each(function () {
                            $(this).hide();
                        });
                    }
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    //END - Màn hình Đóng góp ý kiến

    // Màn hình thêm mới chương trình học
    $('#cate_level').on('change', function () {
        var username = $(this).attr('data-username');
        var school_id = $(this).attr('data-id');
        var class_level = $('div[name=cate_class_level]');
        var _class = $('div[name=cate_class]');
        switch (this.value) {
            case '1': //Chọn trường
                if (!class_level.hasClass('x-hidden')) {
                    class_level.addClass('x-hidden');
                }
                if (!_class.hasClass('x-hidden')) {
                    _class.addClass('x-hidden');
                }
                $('#class_level_id').val(0);
                $('#class_id').val(0);
                break;
            case '2': //Chọn khối
                if (class_level.hasClass('x-hidden')) {
                    class_level.removeClass('x-hidden');
                }
                if (!_class.hasClass('x-hidden')) {
                    _class.addClass('x-hidden');
                }
                $('#class_id').val(0);
                break;
            case '3': //Chọn lớp
                if (!class_level.hasClass('x-hidden')) {
                    class_level.addClass('x-hidden');
                }
                if (_class.hasClass('x-hidden')) {
                    _class.removeClass('x-hidden');
                }
                $('#class_level_id').val(0);

                break;
            default:
        }
    });

    // Màn hình thêm mới lịch học
    $('#schedule_level').on('change', function () {
        var username = $(this).attr('data-username');
        var class_level = $('div[name=schedule_class_level]');
        var _class = $('div[name=schedule_class]');
        var school_id = $(this).attr('data-id');
        switch (this.value) {
            case '0':
                if (!class_level.hasClass('x-hidden')) {
                    class_level.addClass('x-hidden');
                }
                if (!_class.hasClass('x-hidden')) {
                    _class.addClass('x-hidden');
                }
                $('#class_level_id').val(0);
                $('#class_id').val(0);
                break;
            case '1': //Chọn trường
                if (!class_level.hasClass('x-hidden')) {
                    class_level.addClass('x-hidden');
                }
                if (!_class.hasClass('x-hidden')) {
                    _class.addClass('x-hidden');
                }
                $('#class_level_id').val(0);
                $('#class_id').val(0);
                break;
            case '2': //Chọn khối
                if (class_level.hasClass('x-hidden')) {
                    class_level.removeClass('x-hidden');
                }
                if (!_class.hasClass('x-hidden')) {
                    _class.addClass('x-hidden');
                }
                $('#class_id').val(0);
                break;
            case '3': //Chọn lớp
                if (!class_level.hasClass('x-hidden')) {
                    class_level.addClass('x-hidden');
                }
                if (_class.hasClass('x-hidden')) {
                    _class.removeClass('x-hidden');
                }
                $('#class_level_id').val(0);
                break;
            default:
        }
    });

    // Xử lý khi thêm hàng ở màn hình thêm mới lịch học
    $('body').on('click', '.js_cate-add-schedule', function () {
        var index = $('#myTableSchedule tbody tr').length + 1;
        $('#myTableSchedule').append('<tr><td class = "index col_no" align = "center">' + index + '</td> <td> <a align = "center" class = "btn btn-default js_cate-delete_null"> ' + __["Delete"] + ' </a></td> <td><input style = "min-width:100px; box-shadow:0 4px 6px rgba(0,0,0,0.1);" type="text" name="start[]" class="form-control"> </td> <td><input style = "min-width:200px; box-shadow:0 4px 6px rgba(0,0,0,0.1);"type = "text" class = "form-control" name = "category[]"></td> <td><textarea class = "note" type="text" name = "subject_detail_mon[]"></textarea></td> <td><textarea class = "note" type="text" name = "subject_detail_tue[]"></textarea></td> <td><textarea class = "note" type="text" name = "subject_detail_wed[]"></textarea></td> <td><textarea class = "note" type="text" name = "subject_detail_thu[]"></textarea></td> <td><textarea class = "note" type="text" name = "subject_detail_fri[]"></textarea></td> <td><textarea class = "note" type="text" name = "subject_detail_sat[]"></textarea></td></tr>');
        $('.note').css('overflow', 'hidden').autogrow();
        if (!($('#use_category').is(':checked'))) {
            $('td:nth-child(4),th:nth-child(4)').hide();
        }
        if (!($('#study_saturday').is(':checked'))) {
            $('td:nth-child(10),th:nth-child(10)').hide();
        }
    });

    //Thêm mới shedule
    $('body').on('click', '.js_school_schedule_add', function () {
        var school_username = $(this).attr('data-username');
        var class_level_id = $('#class_level_id').val();
        var class_id = $('#class_id').val();
        var handle = $(this).attr('data-handle');
        $('div[name=schedule_all]').addClass('x-hidden');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/schedule'], {
            'do': handle,
            'school_username': school_username,
            'class_level_id': class_level_id,
            'class_id': class_id
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.results) {
                $('#cate_list').html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
            // $('button[type="submit"]').prop('disabled', response.no_data);
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Tìm kiếm schedule theo điều kiện search
    $('body').on('click', '.js_school_schedule_search', function () {
        var school_username = $(this).attr('data-username');
        var class_level_id = $('#class_level_id').val();
        var class_id = $('#class_id').val();
        var level = $('#schedule_level').val();
        var handle = $(this).attr('data-handle');
        $('div[name=schedule_all]').remove();

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/schedule'], {
            'do': 'search',
            'school_username': school_username,
            'class_level_id': class_level_id,
            'class_id': class_id,
            'level': level
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.results) {
                $('#cate_list').html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
            // $('button[type="submit"]').prop('disabled', response.no_data);
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Thêm mới thực đơn
    $('body').on('click', '.js_school_menu_add', function () {
        var school_username = $(this).attr('data-username');
        var class_level_id = $('#class_level_id').val();
        var class_id = $('#class_id').val();
        var handle = $(this).attr('data-handle');
        $('div[name=menu_all]').addClass('x-hidden');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/menu'], {
            'do': handle,
            'school_username': school_username,
            'class_level_id': class_level_id,
            'class_id': class_id
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.results) {
                $('#cate_list').html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
            // $('button[type="submit"]').prop('disabled', response.no_data);
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Tìm kiếm menu theo điều kiện search
    $('body').on('click', '.js_school_menu_search', function () {
        var school_username = $(this).attr('data-username');
        var class_level_id = $('#class_level_id').val();
        var level = $('#schedule_level').val();
        var class_id = $('#class_id').val();
        var handle = $(this).attr('data-handle');
        $('div[name=menu_all]').remove();

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/menu'], {
            'do': 'search',
            'school_username': school_username,
            'class_level_id': class_level_id,
            'class_id': class_id,
            'level': level
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.results) {
                $('#cate_list').html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
            // $('button[type="submit"]').prop('disabled', response.no_data);
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Xử lý khi xóa: lịch học
    $('body').on('click', '.js_school-schedule-delete', function () {
        var _this = $(this);
        var username = $(this).attr('data-username');
        var id = $(this).attr('data-id');
        var handle = $(this).attr('data-handle');

        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['school/schedule'], {
                'do': handle,
                'school_username': username,
                'id': id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else if (response.success) {
                    modal('#modal-success', {title: __['Success'], message: response.message});
                    modal_hidden(timeModal);
                    var count_schedule = $('.count_schedule').text();
                    count_schedule = parseInt(count_schedule);
                    console.log(count_schedule);
                    count_schedule = count_schedule - 1;
                    console.log(count_schedule);
                    $('.count_schedule').text(count_schedule);
                    _this.closest("tr").hide();
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    //Xử lý khi xóa: thực đơn
    $('body').on('click', '.js_school-menu-delete', function () {
        var _this = $(this);
        var username = $(this).attr('data-username');
        var id = $(this).attr('data-id');
        var handle = $(this).attr('data-handle');

        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['school/menu'], {
                'do': handle,
                'school_username': username,
                'id': id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else if (response.success) {
                    modal('#modal-success', {title: __['Success'], message: response.message});
                    modal_hidden(timeModal);
                    var count_menu = $('.count_menu').text();
                    count_menu = parseInt(count_menu);
                    count_menu = count_menu - 1;
                    $('.count_menu').text(count_menu);
                    _this.closest("tr").hide();
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    // Xử lý xóa dòng
    $('body').on('click', '.js_cate-delete', function () {
        var id = $(this).attr('data-id');
        $('.subject' + id).remove();
    });
    $('body').on("click", 'a.js_cate-delete_null', function () {
        var $this = $(this);
        var idx = 1;
        $this.parents('tr').remove();
        $(".col_no").each(function () {
            $(this).html(idx);
            idx = idx + 1;
        });
        // return false;
    });


    // Xử lý phân quyền
    $('#permission').ready(function () {
        if (this.checked) {
            $('#view').attr('checked', 'checked');
        } else {
            $('#view').attr('disabled');
            $('#all').attr('disabled');
        }
    });

    // Xử lý check all phân quyền
    $('#select_checkbox').on('change', function () {
        if ($(this).is(':checked')) {
            $(".no_permission").attr("checked", true);
        } else {
            $(".no_permission").attr("checked", false);
        }
    });

    $('body').on('click', '.js_school-check-view', function () {
        $('.view_permission').attr('checked', true);
    });

    $('body').on('click', '.js_school-check-all', function () {
        $('.all_permission').attr('checked', true);
    });
    $("#checkAllNo").change(function () {
        $(".no_permission").prop('checked', $(this).prop("checked"));
        $('#checkAllView').removeAttr('checked');
        $('#checkAllAll').removeAttr('checked');
    });
    $("#checkAllView").change(function () {
        $(".view_permission").prop('checked', $(this).prop("checked"));
        $('#checkAllNo').removeAttr('checked');
        $('#checkAllAll').removeAttr('checked');
    });
    $("#checkAllAll").change(function () {
        $(".all_permission").prop('checked', $(this).prop("checked"));
        $('#checkAllNo').removeAttr('checked');
        $('#checkAllView').removeAttr('checked');
    });

    // Xử lý check all cấu hình thông báo
    $("#checkAllTeacherNotify").change(function () {
        $(".hidden").val("0");
        $(".teacher").prop('checked', $(this).prop("checked"));
    });
    $("#checkAllParentNotify").change(function () {
        $(".hidden").val("0");
        $(".parent").prop('checked', $(this).prop("checked"));
    });

    // Thêm mới lịch học cho đối tượng chưa có chương trình học
    // $('body').on('click', '.js_school-add-schedule-no-category', function () {
    //     $('input[name=do]').val('add_no_cate');
    // });

    // Delete hàng khi tạo mới report template
    $('body').on("click", 'a.js_report_template-delete', function () {
        var $this = $(this);
        var idx = 1;
        $this.parents('tr').remove();
        $(".col_no").each(function () {
            $(this).html(idx);
            idx = idx + 1;
        });
        //return false;
    });

    // Xử lý khi thêm hàng ở màn hình thêm mới report template
    $('body').on('click', '.js_report_template-add', function () {
        var index = $('#addTempTable tbody tr').length + 1;
        $('#addTempTable').append('<tr><td align="center" class="col_no align-middle">' + index + '</td><td class="align-middle"><input type="text" name = "cate[]" required class = "form-control"></td><td class="align-middle"><textarea type="text" name = "content[]" required class = "form-control" style="height: 35px;overflow: hidden;resize: vertical;width: 100%;"></textarea></td><td class="align-middle" align="center"><a class="btn btn-xs btn-danger js_report_template-delete"> ' + __["Delete"] + '</a></td></tr>');
    });

    // Xử lý khi chợn mẫu khi tạo sổ liên lạc
    $("#report_template_id").change(function () {
        var username = $(this).attr('data-username');
        // var view = $(this).attr('data-view');
        var template_id = this.value;

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        if (template_id == '') {
            $('#template_detail').html('');
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            if ($('#template_detail').hasClass('hidden')) {
                $('#template_detail').removeClass('hidden');
            }
            if ($('#notemplate').hasClass('hidden')) {
                $('#notemplate').removeClass('hidden');
            }
        } else {
            if (!($('#notemplate').hasClass('hidden'))) {
                $('#notemplate').addClass('hidden');
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
            }
            $.post(api['school/report'], {
                'do': 'template_detail',
                'school_username': username,
                'template_id': template_id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.results) {
                    $('#template_detail').html(response.results);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        }
    });

    //Xử lý khi xóa: report, template, hạng mục
    $('body').on('click', '.js_school-delete-report', function () {
        var _this = $(this);
        var handle = $(this).attr('data-handle');
        var username = $(this).attr('data-username');
        var id = $(this).attr('data-id');
        var is_notified = $(this).attr('data-notify');

        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['school/report'], {
                'do': handle,
                'school_username': username,
                'id': id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else if (response.success) {
                    modal('#modal-success', {title: __['Success'], message: response.message});
                    modal_hidden(timeModal);
                    var count_report = $('.count_report').text();
                    count_report = parseInt(count_report);
                    count_report = count_report - 1;
                    $('.count_report').text(count_report);
                    if (is_notified == 0) {
                        var count_not_notify = $('.count_not_notify').text();
                        count_not_notify = parseInt(count_not_notify);
                        count_not_notify = count_not_notify - 1;
                        $('.count_not_notify').text(count_not_notify);
                    }
                    _this.closest("tr").hide();
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    //Xử lý khi xóa những sổ liên lạc được chọn
    $('body').on('click', '#js_school-report-delete-all-select', function () {
        var handle = $(this).attr('data-handle');
        var username = $(this).attr('data-username');
        var ids = new Array();
        $('input[name="report_ids"]:checked').each(function () {
            ids.push(this.value);
        });

        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['school/report'], {
                'do': handle,
                'school_username': username,
                'ids': ids
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });
    // Xử lý khi assig lớp cho giáo viên trong màn assign teacher
    $('input[name="class_ids[]"]').on('click', function () {
        var class_id = $(this).val();
        var homeroom_id = "#homeroom_" + class_id;
        var div_homeroom = $(homeroom_id);
        if (this.checked) {
            div_homeroom.css('display', 'inline')
        } else {
            div_homeroom.css('display', 'none')
        }

    });
    //Xử lý khi thông báo những sổ liên lạc được chọn
    $('body').on('click', '#js_school-report-notify-all-select', function () {
        var handle = $(this).attr('data-handle');
        var username = $(this).attr('data-username');
        var ids = new Array();
        $('input[name="notify_report_ids"]:checked').each(function () {
            ids.push(this.value);
        });

        confirm(__['Actions'], __['Are you sure you want to do this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['school/report'], {
                'do': handle,
                'school_username': username,
                'ids': ids
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    //BEGIN - Màn hình đón muộn

    $('#pickup_beginpicker').datetimepicker({
        format: TIME_FORMAT
        //defaultDate: new Date(1483264800000)
    });

    $('.ending_time').datetimepicker({
        format: TIME_FORMAT,
        defaultDate: new Date()
    });

    // Xử lý khi thêm hàng ở màn hình thêm mới menu
    $('body').on('click', '.js_cate-add-menu', function () {
        var index = $('#myTableMenu tbody tr').length + 1;
        $('#myTableMenu').append('<tr><td class = "index col_no" align = "center">' + index + '</td> <td> <a align = "center" class = "btn btn-default js_cate-delete_null"> ' + __["Delete"] + ' </a></td> <td><input style = "min-width:100px; box-shadow:0 4px 6px rgba(0,0,0,0.1);" type="text" name="start[]" class="form-control" required> </td> <td><input style = "min-width:200px; box-shadow:0 4px 6px rgba(0,0,0,0.1);"type = "text" class = "form-control" name = "meal[]"></td> <td><textarea class = "note" type="text" name = "meal_detail_mon[]"></textarea></td> <td><textarea class = "note" type="text" name = "meal_detail_tue[]"></textarea></td> <td><textarea class = "note" type="text" name = "meal_detail_wed[]"></textarea></td> <td><textarea class = "note" type="text" name = "meal_detail_thu[]"></textarea></td> <td><textarea class = "note" type="text" name = "meal_detail_fri[]"></textarea></td> <td><textarea class = "note" type="text" name = "meal_detail_sat[]"></textarea></td></tr>');
        $('.note').css('overflow', 'hidden').autogrow();
        if (!($('#use_meal').is(':checked'))) {
            $('td:nth-child(4),th:nth-child(4)').hide();
        }
        if (!($('#study_saturday').is(':checked'))) {
            $('td:nth-child(10),th:nth-child(10)').hide();
        }
    });

    // Xử lý xóa dòng
    $('body').on("click", 'a.js_pickup_price-delete', function () {
        var $this = $(this);
        $this.parents('tr').remove();
        // return false;
    });

    $('body').on("click", 'a.js_pickup_service-delete', function () {
        var $this = $(this);
        var id = $this.attr('data-id');
        if (id > 0) {
            $('#pickup_service').append(
                '<input type="hidden" name="service_delete_id[]" value="' + id + '" required>'
            );
        }
        $this.parents('tr').remove();
        // return false;
    });

    $('body').on("click", 'a.js_pickup_class-delete', function () {
        var $this = $(this);
        var id = $this.attr('data-id');
        if (id > 0) {
            $('#pickup_classes').append(
                '<input type="hidden" name="class_delete_id[]" value="' + id + '" required>'
            );
        }
        $this.parents('tr').remove();
        // return false;
    });

    // Xử lý khi ấn vào nút thêm mới giá đón muộn
    $('body').on('click', '.js_pickup_price-add', function () {
        var index = $('#pickup_price_list tbody tr').length + 1;
        $('#pickup_price_list').append('' +
            '<tr>' +
            '<td><input type="text" name="ending_time[]" class="form-control ending_time text-center" required> </td>' +
            '<td><input type="text" name="unit_price[]" class="form-control text-center money_tui" required></td>' +
            '<td class="align-middle" align="center"><a class="btn btn-xs btn-danger mt5 js_pickup_price-delete" data-handle="delete">' + __["Delete"] + '</a></td>' +
            '</tr>');

        $('.ending_time').datetimepicker({
            format: TIME_FORMAT,
            defaultDate: new Date()
        });

    });

    // Xử lý khi ấn vào nút thêm mới dịch vụ đón muộn
    $('body').on('click', '.js_pickup_service-add', function () {

        $('#pickup_service').append('' +
            '<tr>' +
            '<td><input type="text" name="service_name[]" class="form-control" required></td>' +
            '<td><input type="text" name="service_price[]" class="form-control text-center money_tui" required> </td>' +
            '<td><input type="text" name="service_description[]" class="form-control"></td>' +
            '<td class="align-middle" align="center"><a class="btn btn-xs btn-danger mt5 js_pickup_service-delete" data-handle="delete">' + __["Delete"] + '</a></td>' +
            '</tr>');
    });

    // Xử lý khi ấn vào nút thêm mới lớp đón muộn
    $('body').on('click', '.js_pickup_class-add', function () {

        $('#pickup_classes').append('' +
            '<tr>' +
            '<td><input type="text" name="class_name[]" class="form-control" required></td>' +
            '<td><input type="text" name="class_description[]" class="form-control"></td>' +
            '<td class="align-middle" align="center"><a class="btn btn-xs btn-danger mt5 js_pickup_class-delete">' + __["Delete"] + '</a></td>' +
            '</tr>');
    });

    $('#pickup_month_picker').datetimepicker({
        format: 'MM/YYYY',
        // maxDate: new Date(),
        defaultDate: new Date()
    });

    $('#pickup_date_picker').datetimepicker({
        format: DATE_FORMAT
    });

    $('body').on('click', '.js_pickup_assign-search', function () {
        var username = $(this).attr('data-username');
        var fromDate = $('#fromDate').val();
        var toDate = $('#toDate').val();
        var pickup_assign_history = $('#pickup_assign_history');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/pickup'], {
            'do': 'search_assign',
            'school_username': username,
            'fromDate': fromDate,
            'toDate': toDate
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            if (response.results) {
                pickup_assign_history.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });


    $('body').on('change', 'input[type=radio][name=assign_type]', function () {
        var type = $(this).attr('value');

        var username = $('#school_username').val();
        var month = $('#month').val();
        var date = $('#date').val();
        var class_id = $('#pickup_class_assign option:selected').val();

        var error = $('.alert.alert-danger');
        var success = $('.alert.alert-success');
        if (success.is(":visible")) success.hide();
        if (error.is(":visible")) error.hide();

        if (type == 'week') {
            $('#div_assign_month').addClass('x-hidden');
            $('#div_assign_date').removeClass('x-hidden');
            $('#div_repeat').removeClass('x-hidden');
        } else {
            $('#div_assign_date').addClass('x-hidden');
            $('#div_assign_month').removeClass('x-hidden');
            $('#div_repeat').addClass('x-hidden');
        }

        $('#table_assign').html('');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        if (class_id != '' && date.length == 10 && month.length == 7) {
            $('button[type="submit"]').addClass("x-hidden");

            $.post(api['school/pickup'], {
                'do': type == 'week' ? 'get_assign_by_date' : 'get_assign_by_month',
                'school_username': username,
                'pickup_class_assign': class_id,
                'date': date,
                'month': month
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.results) {
                    $('#table_assign').html(response.results);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
                $('button[type="submit"]').removeClass("x-hidden");
                $('button[type="submit"]').prop('disabled', response.disableSave);
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    $('button[type="submit"]').removeClass("x-hidden");
                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        } else {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            $('button[type="submit"]').prop('disabled', true);
        }

    });


    $('body').on('change', '#pickup_class_assign', function () {
        var type = $('input[type=radio][name=assign_type]:checked').val();

        var username = $('#school_username').val();
        var month = $('#month').val();
        var date = $('#date').val();
        var class_id = $('#pickup_class_assign option:selected').val();

        var error = $('.alert.alert-danger');
        var success = $('.alert.alert-success');

        if (success.is(":visible")) success.hide();
        if (error.is(":visible")) error.hide();
        $('#table_assign').html('');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        if (class_id != '' && date.length == 10 && month.length == 7) {
            $('button[type="submit"]').addClass("x-hidden");

            $.post(api['school/pickup'], {
                'do': type == 'week' ? 'get_assign_by_date' : 'get_assign_by_month',
                'school_username': username,
                'pickup_class_assign': class_id,
                'month': month,
                'date': date
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.results) {
                    $('#table_assign').html(response.results);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
                $('button[type="submit"]').removeClass("x-hidden");
                $('button[type="submit"]').prop('disabled', response.disableSave);
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    $('button[type="submit"]').removeClass("x-hidden");
                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        } else {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            $('button[type="submit"]').prop('disabled', true);
        }
    });


    //Bắt sự kiện khi thông tin ngày thay đổi
    $('#pickup_date_picker').on("dp.change", function (e) {
        var username = $('#school_username').val();
        var date = $('#date').val();
        var class_id = $('#pickup_class_assign option:selected').val();
        var error = $('.alert.alert-danger');
        var success = $('.alert.alert-success');

        if (success.is(":visible")) success.hide();
        if (error.is(":visible")) error.hide();
        $('#table_assign').html('');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        if (class_id != '' && date.length == 10) {
            $('button[type="submit"]').addClass("x-hidden");

            $.post(api['school/pickup'], {
                'do': 'get_assign_by_date',
                'school_username': username,
                'pickup_class_assign': class_id,
                'date': date
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.results) {
                    $('#table_assign').html(response.results);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
                $('button[type="submit"]').removeClass("x-hidden");
                $('button[type="submit"]').prop('disabled', response.disableSave);
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    $('button[type="submit"]').removeClass("x-hidden");
                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        } else {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            $('button[type="submit"]').prop('disabled', true);
        }
    });

    //Bắt sự kiện khi thông tin tháng thay đổi
    $('#pickup_month_picker').on("dp.change", function (e) {
        var username = $('#school_username').val();
        var month = $('#month').val();
        var class_id = $('#pickup_class_assign option:selected').val();
        var error = $('.alert.alert-danger');
        var success = $('.alert.alert-success');

        if (success.is(":visible")) success.hide();
        if (error.is(":visible")) error.hide();
        $('#table_assign').html('');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        if (class_id != '' && month.length == 7) {
            $('button[type="submit"]').addClass("x-hidden");

            $.post(api['school/pickup'], {
                'do': 'get_assign_by_month',
                'school_username': username,
                'pickup_class_assign': class_id,
                'month': month
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.results) {
                    $('#table_assign').html(response.results);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
                $('button[type="submit"]').removeClass("x-hidden");
                $('button[type="submit"]').prop('disabled', response.disableSave);
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    $('button[type="submit"]').removeClass("x-hidden");
                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        } else {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            $('button[type="submit"]').prop('disabled', true);
        }
    });

    $('body').on('click', '.js_display_teacher_assign', function () {

        var username = $('#school_username').val();

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/pickup'], {
            'do': 'display_teacher_assign',
            'school_username': username
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.callback) {
                eval(response.callback);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    $('body').on('click', '.js_school-pickup', function () {
        var handle = $(this).attr('data-handle');
        var username = $(this).attr('data-username');
        var child_id = $(this).attr('data-child');
        var pickup_id = $(this).attr('data-pickup');

        confirm(__['Actions'], __['Are you sure you want to do this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['school/pickup'], {
                'do': handle,
                'school_username': username,
                'child_id': child_id,
                'pickup_id': pickup_id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    // Xử lý khi nhấn nút trả trẻ ở màn hình quản lý trường
    $('body').on('click', '.js_school-pickup-one-child', function () {
        var _this = $(this);

        var username = _this.attr('data-username');
        var child_id = _this.attr('data-child');
        var pickup_id = _this.attr('data-pickup');

        // Lấy danh sách các dịch vụ được đăng ký
        var serviceIdList = new Array();
        var serviceFeeList = new Array();
        $('input[name="service_' + child_id + '[]"]:checked').each(function () {
            serviceIdList.push($(this).attr("data-service"));
            serviceFeeList.push($(this).attr("data-fee"));
        });

        // Lấy ghi chú
        var note = $('#pickup_note_' + child_id).val();

        // Lấy thời gian
        var time = $('#pickup_time_' + child_id).val();

        // Lấy tiền trông muộn, chưa tính phí dịch vụ
        var pickup_fee = $('#total_pickup_' + child_id).val();

        // Lấy tiền dịch vụ
        var services_fee = $('#total_service_' + child_id).val();

        // Lấy tổng tiền
        var total_fee = $('#total_' + child_id).val();

        confirm(__['Actions'], __['Are you sure you want to do this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['school/pickup'], {
                'do': 'pickup',
                'school_username': username,
                'child_id': child_id,
                'pickup_id': pickup_id,
                'serviceIdList': serviceIdList,
                'serviceFeeList': serviceFeeList,
                'note': note,
                'time': time,
                'pickup_fee': pickup_fee,
                'services_fee': services_fee,
                'total_fee': total_fee
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    $('body').on('click', '.js_school-pickup_child', function () {
        var username = $(this).attr('data-username');
        var child_id = $('#attendance_child_id option:selected').val();
        var fromDate = $('#fromDate').val();
        var toDate = $('#toDate').val();
        var pickup_list_child = $('#pickup_list_child');
        var error = $('.alert.alert-danger');
        var success = $('.alert.alert-success');

        pickup_list_child.html('');
        //$('button[type="submit"]').prop('disabled', true);
        $('#search').addClass('x-hidden');
        $('#loading').removeClass('x-hidden');

        if (success.is(":visible")) success.hide();
        if (error.is(":visible")) error.hide();

        $.post(api['school/pickup'], {
            'do': 'get_pickup_child',
            'school_username': username,
            'child_id': child_id,
            'fromDate': fromDate,
            'toDate': toDate
        }, function (response) {
            $('#search').removeClass('x-hidden');
            $('#loading').addClass('x-hidden');
            pickup_list_child.html(response.results);
            //$('button[type="submit"]').prop('disabled', response.disableSave);
        }, 'json')
            .fail(function () {
                $('#search').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    $('body').on('click', '.js_school-pickup_edit', function () {
        var username = $(this).attr('data-username');
        var pickup_time = $(this).attr('data-time');
        var pickup_class_id = $(this).attr('data-class');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/pickup'], {
            'do': 'edit',
            'school_username': username,
            'pickup_time': pickup_time,
            'pickup_class_id': pickup_class_id
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.callback) {
                eval(response.callback);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Xử lý khi chọn class ở màn hình thêm trẻ
    $('body').on('change', '#pickup_class', function () {
        var username = $('#school_username').val();
        // var pickup_class_id = $('#pickup_class_id').val();
        var pickup_class_id = $('input[name=pickup_class_id]:checked').val();
        var class_id = this.value;
        var add_to_date = $('#add_to_date').val();

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        if (class_id != '') {
            $.post(api['school/pickup'], {
                'do': 'list_child_edit',
                'school_username': username,
                'pickup_class_id': pickup_class_id,
                'class_id': class_id,
                'add_to_date': add_to_date
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.results) {
                    $('#pickup_school_list').html(response.results);
                    $('button[type="submit"]').prop('disabled', response.disableSave);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        }
    });

    $('body').on('click', '.js_pickup-search', function () {
        var username = $(this).attr('data-username');
        var fromDate = $('#fromDate').val();
        var toDate = $('#toDate').val();
        var pickup_list = $('#pickup_list');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/pickup'], {
            'do': 'search_pickup',
            'school_username': username,
            'fromDate': fromDate,
            'toDate': toDate
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.callback) {
                eval(response.callback);
            } else if (response.results) {
                pickup_list.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    $('body').on('click', '#childCheckAll', function () {
        //$("#childCheckAll").click(function () {
        if (this.checked) {
            $('.child').prop('checked', this.checked);
        } else {
            $('.child').removeAttr('checked');
        }
    });

    //Bắt sự kiện thay đổi số lần sử dụng dịch vụ để tính lại học phí của trẻ trên màn hình
    $('body').on('click', '.pickup_service_fee', function () {
        var child_id = $(this).attr('data-child');
        var service_id = $(this).attr('data-service');
        var total_pickup = $('#total_pickup_' + child_id);
        var total_service = $('#total_service_' + child_id);
        var total_child = $('#total_' + child_id);
        //var total = $('#total');

        var service_fee = parseInt($('#pickup_service_fee_' + child_id + '_' + service_id).attr('data-fee'));
        var service_amount_prev = total_service.val();
        service_amount_prev = service_amount_prev.replace(/,/g, ''); // BỎ dâu phẩy trong giá trị lấy được
        service_amount_prev = !$.isNumeric(service_amount_prev) ? 0 : parseInt(service_amount_prev);

        var new_service_value = 0;
        if (this.checked) {
            //Tính thành tiền của mỗi dịch vụ
            new_service_value = service_amount_prev + service_fee;
            var new_service_str = new_service_value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            total_service.val(new_service_str);
        } else {
            //Tính thành tiền của mỗi dịch vụ
            new_service_value = service_amount_prev - service_fee;
            if (new_service_value > 0) {
                var new_service_str = new_service_value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                total_service.val(new_service_str);
            } else {
                total_service.val(0);
            }
        }

        var pickup_fee = total_pickup.val();
        pickup_fee = pickup_fee.replace(/,/g, ''); // BỎ dâu phẩy trong giá trị lấy được
        pickup_fee = !$.isNumeric(pickup_fee) ? 0 : parseInt(pickup_fee);

        //Tổng phí của 1 trẻ
        var total_child_value = (pickup_fee + new_service_value).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        total_child.val(total_child_value);

        //Tổng tiền của cả lớp
        var total = 0;
        $('input[name*=total_child]').each(function () {
            var total_child_value = $(this).val();
            total_child_value = total_child_value.replace(/,/g, ''); // BỎ dâu phẩy trong giá trị lấy được
            total_child_value = !$.isNumeric(total_child_value) ? 0 : parseInt(total_child_value);

            total = total + total_child_value;
        });
        total = total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        $('#total').val(total);
    });

    $('body').on('change', '[name="pickup_fee[]"]', function () {
        var child_id = $(this).attr('data-child');
        //Tiền phí trông muộn
        var pickup_fee = this.value;
        pickup_fee = pickup_fee.replace(/,/g, ''); // BỎ dâu phẩy trong giá trị lấy được
        pickup_fee = !$.isNumeric(pickup_fee) ? 0 : parseInt(pickup_fee);

        //Tiền sử dụng dịch vụ
        var total_service = $('#total_service_' + child_id);
        var total_service_value = total_service.val();
        total_service_value = total_service_value.replace(/,/g, ''); // BỎ dâu phẩy trong giá trị lấy được
        total_service_value = !$.isNumeric(total_service_value) ? 0 : parseInt(total_service_value);

        //Tổng phí của 1 trẻ
        var total_child = $('#total_' + child_id);
        var total_child_value = (pickup_fee + total_service_value).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        total_child.val(total_child_value);

        //Tổng tiền của cả lớp
        var total = 0;
        $('input[name*=total_child]').each(function () {
            var total_child_value = $(this).val();
            total_child_value = total_child_value.replace(/,/g, ''); // BỎ dâu phẩy trong giá trị lấy được
            total_child_value = !$.isNumeric(total_child_value) ? 0 : parseInt(total_child_value);

            total = total + total_child_value;
        });
        total = total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        $('#total').val(total);
    });

    $('body').on('change', '[name="service_fee[]"]', function () {
        var child_id = $(this).attr('data-child');

        //Tiền sử dụng dịch vụ
        var service_fee = this.value;
        service_fee = service_fee.replace(/,/g, ''); // BỎ dâu phẩy trong giá trị lấy được
        service_fee = !$.isNumeric(service_fee) ? 0 : parseInt(service_fee);

        //Tiền phí trông muộn
        var pickup_fee = $('#total_pickup_' + child_id);
        var total_pickup_value = pickup_fee.val();
        total_pickup_value = total_pickup_value.replace(/,/g, ''); // BỎ dâu phẩy trong giá trị lấy được
        total_pickup_value = !$.isNumeric(total_pickup_value) ? 0 : parseInt(total_pickup_value);

        //Tổng phí của 1 trẻ
        var total_child = $('#total_' + child_id);
        var total_child_value = (service_fee + total_pickup_value).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        total_child.val(total_child_value);

        //Tính tổng học phí của cả lớp
        var total = 0;
        $('input[name*=total_child]').each(function () {
            var total_child_value = $(this).val();
            total_child_value = total_child_value.replace(/,/g, ''); // BỎ dâu phẩy trong giá trị lấy được
            total_child_value = !$.isNumeric(total_child_value) ? 0 : parseInt(total_child_value);

            total = total + total_child_value;
        });
        total = total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        $('#total').val(total);
    });

    $('.pickup_time').datetimepicker({
        format: TIME_FORMAT,
        sideBySide: true
    });

    $('body').on('focus', '.pickup_time', function () {
        var timeOption = $('#time_option').attr('data-value');
        var currentTIme = $(this).val();
        if ($(this).val() == '') {
            $(this).val(timeOption);
        }
        var pid = $(this).attr('id');
        if ($.inArray(pid, arrPickupSchoolDP) == -1) {
            $('#' + pid).on("dp.change", function (e) {
                var pickup_time = $(this).val();
                var pickup_id = $('#pickup_id').val();
                var child_id = pid.replace("pickup_time_", "");
                var username = $('#school_username').val();
                var pickup_date = $('#pickup_date').val();

                if (pickup_time == '') {
                    $('#total_pickup_' + child_id).val(0);

                    var total_child = $('#total_' + child_id);

                    //Tổng tiền trông muộn của trẻ = tiền sử dụng dịch vụ
                    var total_service = $('#total_service_' + child_id);
                    var total_service_value = total_service.val();
                    total_child.val(total_service_value);

                    //Tính tiền đón muộn của cả lớp
                    var total = 0;
                    $('input[name*=total_child]').each(function () {
                        var total_child_value = $(this).val();
                        total_child_value = total_child_value.replace(/,/g, ''); // BỎ dâu phẩy trong giá trị lấy được
                        total_child_value = !$.isNumeric(total_child_value) ? 0 : parseInt(total_child_value);

                        total = total + total_child_value;
                    });
                    total = total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                    $('#total').val(total);
                } else {
                    // Show loading
                    $("#loading_full_screen").removeClass('hidden');

                    $.post(api['school/pickup'], {
                        'do': 'get_pickup_fee',
                        'school_username': username,
                        'child_id': child_id,
                        'pickup_id': pickup_id,
                        'pickup_date': pickup_date,
                        'pickup_time': pickup_time
                    }, function (response) {
                        // Hidden loading
                        $("#loading_full_screen").addClass('hidden');

                        if (response.callback) {
                            eval(response.callback);
                            $('#' + pid).val(currentTIme);
                        } else if (response.error) {
                            modal('#modal-error', {title: __['Error'], message: response.message});
                        } else {
                            // Hiển thị tổng tiền đón muộn khi thay đổi thời gian
                            var pickup_fee = parseInt(response.results);
                            var total_pickup_child = pickup_fee.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                            $('#total_pickup_' + child_id).val(total_pickup_child);

                            //Tiền sử dụng dịch vụ
                            var total_service = $('#total_service_' + child_id);
                            var total_service_value = total_service.val();
                            total_service_value = total_service_value.replace(/,/g, ''); // BỎ dâu phẩy trong giá trị lấy được
                            total_service_value = !$.isNumeric(total_service_value) ? 0 : parseInt(total_service_value);

                            //Tổng phí của 1 trẻ
                            var total_child = $('#total_' + child_id);
                            var total_child_value = (pickup_fee + total_service_value).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                            total_child.val(total_child_value);

                            //Tổng tiền của cả lớp
                            var total = 0;
                            $('input[name*=total_child]').each(function () {
                                var total_child_value = $(this).val();
                                total_child_value = total_child_value.replace(/,/g, ''); // BỎ dâu phẩy trong giá trị lấy được
                                total_child_value = !$.isNumeric(total_child_value) ? 0 : parseInt(total_child_value);

                                total = total + total_child_value;
                            });
                            total = total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                            $('#total').val(total);
                        }
                    }, 'json')
                        .fail(function () {
                            // Hidden loading
                            $("#loading_full_screen").addClass('hidden');

                            modal('#modal-message', {
                                title: __['Error'],
                                message: __['There is something that went wrong!']
                            });
                        });
                }

            });
            arrPickupSchoolDP.push(pid);
        }

    });

    $('body').on('change', '.pickup_class_id', function () {
        var username = $('#school_username').val();
        var pickup_class_id = $(this).val();
        var class_id = $('#pickup_class').val();
        var add_to_date = $('#add_to_date').val();

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        if (class_id != '') {
            $.post(api['school/pickup'], {
                'do': 'list_child_edit',
                'school_username': username,
                'pickup_class_id': pickup_class_id,
                'class_id': class_id,
                'add_to_date': add_to_date
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.results) {
                    $('#pickup_school_list').html(response.results);
                    $('button[type="submit"]').prop('disabled', response.disableSave);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        }

    });

    //Xử lý khi chọn class ở màn hình tạo mới học phí
    $("#pickup_teacher_class").change(function () {
        var username = $('#school_username').val();
        var class_id = this.value;
        var pickup_class_id = $('#pickup_class_id').val();
        var pickup_id = $('#pickup_id').val();
        var add_to_date = $('#add_to_date').val();

        if (class_id != '') {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['school/teacherpickup'], {
                'do': 'list_child',
                'school_username': username,
                'class_id': class_id,
                'pickup_id': pickup_id,
                'pickup_class_id': pickup_class_id,
                'add_to_date': add_to_date
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.results) {
                    $('#pickup_class_list').html(response.results);
                    $('button[type="submit"]').prop('disabled', response.disableSave);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        }
    });

    $('.pickup_teacher_time').datetimepicker({
        format: TIME_FORMAT,
        sideBySide: true
    });

    $('body').on('focus', '.pickup_teacher_time', function () {
        var timeOption = $('#time_option').attr('data-value');
        var currentTIme = $(this).val();
        if ($(this).val() == '') {
            $(this).val(timeOption);
        }
        /*var pid = $(this).attr('id');
        if ($.inArray(pid, arrPickupClassDP) == -1) {
            $('#' + pid).on("dp.change", function (e) {
                var pickup_time = $(this).val();
                var pickup_id = $('#pickup_id').val();
                var child_id = pid.replace("pickup_time_", "");
                var school_username = $('#school_username').val();
                var pickup_date = $('#pickup_date').val();

                if (pickup_time == '') {
                    $('#total_pickup_' + child_id).val(0);

                    var total_child = $('#total_' + child_id);

                    //Tổng tiền trông muộn của trẻ = tiền sử dụng dịch vụ
                    var total_service = $('#total_service_' + child_id);
                    var total_service_value = total_service.val();
                    total_child.val(total_service_value);

                    //Tính tiền đón muộn của cả lớp
                    var total = 0;
                    $('input[name*=total_child]').each(function () {
                        var total_child_value = $(this).val();
                        total_child_value = total_child_value.replace(/,/g, ''); // BỎ dâu phẩy trong giá trị lấy được
                        total_child_value = !$.isNumeric(total_child_value) ? 0 : parseInt(total_child_value);

                        total = total + total_child_value;
                    });
                    total = total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                    $('#total').val(total);
                } else {
                    // Show loading
                    $("#loading_full_screen").removeClass('hidden');

                    $.post(api['school/teacherpickup'], {
                        'do': 'get_pickup_fee',
                        'school_username': school_username,
                        'child_id': child_id,
                        'pickup_id': pickup_id,
                        'pickup_date': pickup_date,
                        'pickup_time': pickup_time
                    }, function (response) {
                        // Hidden loading
                        $("#loading_full_screen").addClass('hidden');

                        if (response.callback) {
                            eval(response.callback);
                            $('#' + pid).val(currentTIme);
                        } else if(response.error) {
                            modal('#modal-error', {title: __['Error'], message: response.message});
                        } else {
                            // Hiển thị tổng tiền đón muộn khi thay đổi thời gian
                            var pickup_fee = parseInt(response.results);
                            var total_pickup_child = pickup_fee.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                            $('#total_pickup_' + child_id).val(total_pickup_child);

                            //Tiền sử dụng dịch vụ
                            var total_service = $('#total_service_' + child_id);
                            var total_service_value = total_service.val();
                            total_service_value = total_service_value.replace(/,/g, ''); // BỎ dâu phẩy trong giá trị lấy được
                            total_service_value = !$.isNumeric(total_service_value) ? 0 : parseInt(total_service_value);

                            //Tổng phí của 1 trẻ
                            var total_child = $('#total_' + child_id);
                            var total_child_value = (pickup_fee + total_service_value).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                            total_child.val(total_child_value);

                            //Tổng tiền của cả lớp
                            var total = 0;
                            $('input[name*=total_child]').each(function () {
                                var total_child_value = $(this).val();
                                total_child_value = total_child_value.replace(/,/g, ''); // BỎ dâu phẩy trong giá trị lấy được
                                total_child_value = !$.isNumeric(total_child_value) ? 0 : parseInt(total_child_value);

                                total = total + total_child_value;
                            });
                            total = total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                            $('#total').val(total);
                        }
                    }, 'json')
                        .fail(function () {
                            // Hidden loading
                            $("#loading_full_screen").addClass('hidden');

                            modal('#modal-message', {
                                title: __['Error'],
                                message: __['There is something that went wrong!']
                            });
                        });
                }

            });
            arrPickupClassDP.push(pid);
        }*/

    });

    $('body').on('click', '.js_class-pickup', function () {
        var _this = $(this);
        var handle = _this.attr('data-handle');
        var username = _this.attr('data-username');
        var child_id = _this.attr('data-child');
        var pickup_id = _this.attr('data-pickup');
        var callback = $('#callback').val();

        // Lấy danh sách các dịch vụ được đăng ký
        /*var serviceIdList = new Array();
        var serviceFeeList = new Array();
        $('input[name="service_' + child_id + '[]"]:checked').each(function () {
            serviceIdList.push($(this).attr("data-service"));
            serviceFeeList.push($(this).attr("data-fee"));
        });

        // Lấy ghi chú
        var note = $('#pickup_note_' + child_id).val();*/

        confirm(__['Actions'], __['Are you sure you want to do this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['school/teacherpickup'], {
                'do': handle,
                'school_username': username,
                'child_id': child_id,
                'pickup_id': pickup_id,
                'callback': callback
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');
                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    $('body').on('click', '.js_class-pickup-one-child', function () {
        var _this = $(this);
        var username = _this.attr('data-username');
        var child_id = _this.attr('data-child');
        var pickup_id = _this.attr('data-pickup');
        var callback = $('#callback').val();

        // Lấy danh sách các dịch vụ được đăng ký
        var serviceIdList = new Array();
        var serviceFeeList = new Array();
        $('input[name="service_' + child_id + '[]"]:checked').each(function () {
            serviceIdList.push($(this).attr("data-service"));
            serviceFeeList.push($(this).attr("data-fee"));
        });

        // Lấy ghi chú
        var note = $('#pickup_note_' + child_id).val();

        // Lấy thời gian
        var time = $('#pickup_time_' + child_id).val();

        confirm(__['Actions'], __['Are you sure you want to do this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['school/teacherpickup'], {
                'do': "pickup",
                'school_username': username,
                'child_id': child_id,
                'pickup_id': pickup_id,
                'serviceIdList': serviceIdList,
                'serviceFeeList': serviceFeeList,
                'note': note,
                'time': time,
                'callback': callback
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');
                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    $('body').on('click', '.js_pickup_teacher-search', function () {
        var username = $(this).attr('data-username');
        var fromDate = $('#fromDate').val();
        var toDate = $('#toDate').val();
        var pickup_list = $('#pickup_list');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/teacherpickup'], {
            'do': 'search_pickup',
            'school_username': username,
            'fromDate': fromDate,
            'toDate': toDate
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.callback) {
                eval(response.callback);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else if (response.results) {
                pickup_list.html(response.results);
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    $('#pickup_teacher_begin_assign').datetimepicker({
        format: DATE_FORMAT,
        defaultDate: new Date()
    });

    //Bắt sự kiện khi thông tin tháng thay đổi
    $('#pickup_teacher_begin_assign').on("dp.change", function (e) {
        var username = $('#school_username').val();
        var begin = $('#begin').val();

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/teacherpickup'], {
            'do': 'get_class_assign',
            'begin': begin,
            'school_username': username
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.results) {
                $('#tbody-assign').html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    $('#leave_datepicker').datetimepicker({
        format: DATE_FORMAT,
        defaultDate: new Date()
    });

    //Bắt sự kiện khi ngày nghỉ học thay đổi. Trên màn hình Nghỉ học của trẻ.
    $('#leave_datepicker').on("dp.change", function (e) {
        var username = $('#school_username').val();
        var class_id = $('#class_id').val();
        var child_id = $('#child_id').val();
        var end_at = $('#end_at').val();

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $('#children_info').html('');
        $('button[type="submit"]').prop('disabled', true);
        if (end_at.length > 8) {
            $.post(api['school/tuition'], {
                'do': 'load_4leave',
                'school_username': username,
                'child_id': child_id,
                'class_id': class_id,
                'end_at': end_at
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.results) {
                    $('#children_info').html(response.results);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
                $('button[type="submit"]').prop('disabled', false);
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    $('button[type="submit"]').prop('disabled', false);
                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        }
    });

    $('#pickup_datepicker').datetimepicker({
        format: DATE_FORMAT,
        defaultDate: new Date()
    });

    //Bắt sự kiện khi thông tin tháng thay đổi
    $('#pickup_datepicker').on("dp.change", function (e) {
        var username = $('#school_username').val();
        var class_id = $('#pickup_class').val();
        var pickup_class_id = $('input[name=pickup_class_id]:checked').val();
        //var pickup_class_id = $('#pickup_class_id').val();
        var add_to_date = $('#add_to_date').val();
        var error = $('.alert.alert-danger');
        var success = $('.alert.alert-success');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        if (success.is(":visible")) success.hide();
        if (error.is(":visible")) error.hide();
        if (class_id != '') {
            $.post(api['school/pickup'], {
                'do': 'list_child_edit',
                'school_username': username,
                'pickup_class_id': pickup_class_id,
                'class_id': class_id,
                'add_to_date': add_to_date
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.results) {
                    $('#pickup_school_list').html(response.results);
                    $('button[type="submit"]').prop('disabled', response.disableSave);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        }
    });

    $('#is_multiple_classes').on('change', function () {
        var is_multiple_classes = $('#is_multiple_classes');
        var pickup_classes_table = $('#pickup_classes_table');
        if (is_multiple_classes.is(':checked')) {
            pickup_classes_table.removeClass('x-hidden');
        } else {
            pickup_classes_table.addClass('x-hidden');
        }
    });

    //END - Màn hình Đón muộn


    // Xử lý ẩn cột hoạt động khi không sử dụng category
    $('#use_category').on('change', function () {
        if ($(this).is(':checked')) {
            $('td:nth-child(4),th:nth-child(4)').show();
        } else {
            $('td:nth-child(4),th:nth-child(4)').hide();
        }
    });

    // Xử lý ẩn cột bữa ăn khi không sử dụng bữa ăn
    $('#use_meal').on('change', function () {
        if ($(this).is(':checked')) {
            $('td:nth-child(4),th:nth-child(4)').show();
        } else {
            $('td:nth-child(4),th:nth-child(4)').hide();
        }
    });

    // Xử lý ẩn cột thứ 7 khi không học thứ 7
    $('#study_saturday').on('change', function () {
        if ($(this).is(':checked')) {
            $('td:nth-child(10),th:nth-child(10)').show();
        } else {
            $('td:nth-child(10),th:nth-child(10)').hide();
        }
    });

    // Xử lý khi sửa lịch học
    $('#myTableSchedule', function () {
        if ($('#use_category').is(':not(:checked)')) {
            $('td:nth-child(4),th:nth-child(4)').hide();
        }
        if ($('#study_saturday').is(':not(:checked)')) {
            $('td:nth-child(10),th:nth-child(10)').hide();
        }
    });

    // Xử lý khi sửa menu
    $('#myTableMenu', function () {
        if ($('#use_meal').is(':not(:checked)')) {
            $('td:nth-child(4),th:nth-child(4)').hide();
        }
        if ($('#study_saturday').is(':not(:checked)')) {
            $('td:nth-child(10),th:nth-child(10)').hide();
        }
    });

    // Xử lý xóa ảnh đính kèm
    $(".delete_image").click(function () {
        $("input[name*='file']").val("");
        $("#file_old").remove();
        $("#is_file").val("0");
    });

    //  Xử lý khi ấn ẩn menu
    $('body').on('click', '.js_school_hidden', function () {
        // var status = 'on';
        var is_attendance = $('#full_screen').val();
        var username = $(this).attr('data-username');

        if ($('#menu_left').hasClass("hidden")) {
            if (is_attendance == 1) {
                $('#content_right').removeClass("col-md-12 col-sm-12");
                $('#content_right').addClass("col-md-10 col-sm-10");
            } else {
                $('#content_right').removeClass("col-md-12 col-sm-12");
                $('#content_right').addClass("col-md-9 col-sm-9");
            }
            $('#menu_left').removeClass("hidden");
            $('.menu_fixed').addClass("hidden");
            // $('.container').removeClass("width100");
            // set trang thái menu left = 1 (hiển thị)
            var status = 1;
        } else {
            $('#menu_left').addClass("hidden");
            if (is_attendance == 1) {
                $('#content_right').removeClass("col-md-10 col-sm-10");
                $('#content_right').addClass("col-md-12 col-sm-12");
            } else {
                $('#content_right').removeClass("col-md-9 col-sm-9");
                $('#content_right').addClass("col-md-12 col-sm-12");
            }
            // $('.container').addClass("width100");
            $('.menu_fixed').removeClass("hidden");
            // set trang thái menu left = 0 (ẩn)
            var status = 0;
        }
        $.post(api['school/setting'], {
            'do': 'menu_display',
            'school_username': username,
            'status': status
        });
    });

    function h(e) {
        $(e).css({'height': '35px', 'overflow': 'hidden', 'resize': 'vertical'}).height(e.scrollHeight);
    }

    $('.note').each(function () {
        h(this);
    }).on('input', function () {
        h(this);
    });

    // Xử lý khi xác nhận tất cả trẻ chờ được sửa
    $('body').on('click', '.js_school-confirm-all-child-edit', function () {
        var username = $(this).attr('data-username');
        var handle = $(this).attr('data-handle');

        confirm(__['Actions'], __['Are you sure you want to do this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['school/child'], {
                'do': handle,
                'school_username': username
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    // Xử lý khi hủy xác nhận trẻ chờ được sửa
    $('body').on('click', '.js_school-child-edit', function () {
        var username = $(this).attr('data-username');
        var handle = $(this).attr('data-handle');
        var child_id = $(this).attr('data-id');
        var class_id = $(this).attr('data-class_id');

        confirm(__['Actions'], __['Are you sure you want to do this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['school/child'], {
                'do': handle,
                'school_username': username,
                'child_id': child_id,
                'class_id': class_id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    //Hàm xử lý khi click vào Export lịch học
    $('body').on('click', '.js_school-schedule-export', function () {
        var handle = $(this).attr('data-handle');
        var username = $(this).attr('data-username');
        var id = $(this).attr('data-id');

        $(".js_school-schedule-export").addClass("x-hidden");
        $(".processing_label").removeClass("x-hidden");

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/schedule'], {
            'do': handle,
            'school_username': username,
            'id': id
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
            $(".js_school-schedule-export").removeClass("x-hidden");
            $(".processing_label").addClass("x-hidden");
            var $a = $("<a>");
            $a.attr("href", response.file);
            $("body").append($a);
            $a.attr("download", response.file_name);
            $a[0].click();
            $a.remove();
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $(".js_school-schedule-export").removeClass("x-hidden");
                $(".processing_label").addClass("x-hidden");
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });


    /* ---------- TRẺ THÔI HỌC ---------- */

    $('#leaveSchoolDate').datetimepicker({
        format: DATE_FORMAT,
        defaultDate: new Date()
    });

    /* ---------- END - TRẺ THÔI HỌC ---------- */

    // Xử lý khi thêm hàng ở màn hình thêm mới gợi ý cho hạng mục (popup)
    $('body').on('click', '.js_report_suggest-add', function () {
        var index = $('#addCategoryTable tbody tr').length + 1;
        $('#addCategoryTable').append('<tr><td align="center" class="col_no align-middle">' + index + '</td><td class="align-middle"><input type="text" name = "suggests_add" class = "form-control suggest"></td><td class="align-middle" align="center"><a class="btn btn-xs btn-danger js_report_template-delete"> ' + __["Delete"] + '</a></td></tr>');
    });

    // Xử lý khi thêm hàng ở màn hình thêm mới gợi ý cho hạng mục (popup)
    $('body').on('click', '.js_report_suggest-add-temp', function () {
        var index = $('#editCategoryTable tbody tr').length + 1;
        $('#editCategoryTable').append('<tr><td align="center" class="col_no align-middle">' + index + '</td><td class="align-middle"><input type="text" name = "suggests" class = "form-control suggest"></td><td class="align-middle" align="center"><a class="btn btn-xs btn-danger js_report_template-delete"> ' + __["Delete"] + '</a></td></tr>');
    });

    // Xử lý khi thêm hàng ở màn hình thêm mới gợi ý cho hạng mục (no Popup)
    $('body').on('click', '.js_report_suggest-add', function () {
        var index = $('#addCategoryTableNoPopup tbody tr').length + 1;
        $('#addCategoryTableNoPopup').append('<tr><td align="center" class="col_no align-middle">' + index + '</td><td class="align-middle"><input type="text" name = "suggests[]" class = "form-control suggest"></td><td class="align-middle" align="center"><a class="btn btn-xs btn-danger js_report_template-delete"> ' + __["Delete"] + '</a></td></tr>');
    });

    //Hiển thị popup để thêm hạng mục mới, trong màn hình thêm mới mẫu
    $('body').on('click', '.js_school-category-add', function () {
        $("#open_dialog").dialog({
            modal: true,
            height: 400,
            width: 800,
            closeOnEscape: true,
            resizable: true,
            autoOpen: true,
            buttons: [{
                text: __['Close'],
                click: function () {
                    $(this).dialog("close");
                }
            }],
            closeText: __['Close']
        });
        $('input[name=category_name_add]').val("");
        $('input[name=suggests_add]').val("");
        $(".table_suggest tr").each(function (index) {
            if (index > 0) {
                $(this).remove();
            }
        });
    });

    //Khi click ADD trên màn hình popup khi thêm mới hạng mục ở màn hình tạo mới mẫu sổ liên lạc
    $('body').on('click', '.js_school-category-add-done', function () {
        var username = $('#school_username').val();
        var category_name = $('input[name=category_name_add]').val();

        var suggests = new Array();
        // var value = $('input[name=suggests]').val();
        $('input[name=suggests_add]').each(function () {
            suggests.push($(this).val());
        });
        var id = $('input[name=report_template_id]').val();

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $("#open_dialog").dialog('close');
        $.post(api['school/report'], {
            'do': 'add_cate_temp',
            'school_username': username,
            'category_name': category_name,
            'suggests': suggests,
            'report_template_id': id
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.category_new_pm) {
                $('#category_new_pm').before(response.category_new_pm);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Xử lý chọn tất cả hạng mục ở màn hình thêm và sửa mẫu sổ liên lạc
    $('body').on('click', '#select_all', function () {
        var c = this.checked;
        $(':checkbox').prop('checked', c);
    });

    // Xử lý chọn tất cả hạng mục ở màn hình thêm và sửa sổ liên lạc
    $('body').on('click', '#select_all_child', function () {
        var c = this.checked;
        $('.checkbox_child').prop('checked', c);
    });

    //Hiển thị popup để xem chi tiết hạng mục trong màn hình thêm mới và edit mẫu bên tài khoản giáo viên
    $('body').on('click', '.js_school-category-detail', function () {
        var report_template_category_id = $(this).attr('data-id');
        var username = $('#school_username').val();

        $('#open_dialog_category_detail').html("");

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/report'], {
            'do': 'cate_detail',
            'school_username': username,
            'report_template_category_id': report_template_category_id
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.results) {
                $('#open_dialog_category_detail').html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }

            $("#open_dialog_category_detail").dialog({
                modal: true,
                height: 500,
                width: 600,
                closeOnEscape: true,
                resizable: true,
                autoOpen: true,
                buttons: [{
                    text: __['Close'],
                    click: function () {
                        $(this).dialog("close");
                    }
                }],
                closeText: __['Close']
            });
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Khi click Lưu thay đổi trên màn hình popup khi sửa hạng mục ở màn hình tạo mới mẫu sổ liên lạc
    $('body').on('click', '.js_report-category-edit', function () {
        var username = $('#school_username').val();
        var category_name = $('input[name=category_name]').val();

        var suggests = new Array();
        // var value = $('input[name=suggests]').val();
        $('input[name=suggests]').each(function () {
            suggests.push($(this).val());
        });
        var id = $('input[name=category_id]').val();

        $("#open_dialog_category_detail").dialog('close');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/report'], {
            'do': 'edit_cate_temp',
            'school_username': username,
            'category_name': category_name,
            'suggests': suggests,
            'category_id': id
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Xử lý khi xác nhận xin nghỉ của 1 trẻ
    $('body').on('click', '.js_class-confirm-attendance', function () {
        var username = $(this).attr('data-username');
        var detail_id = $(this).attr('data-detail-id');
        var child_id = $(this).attr('data-id');
        var button = $('#button_' + detail_id);

        // đổi value của feedback_childid = 1
        $('#feedback_' + child_id).val('1');

        button.addClass('x-hidden');
        $.post(api['school/attendance'], {
            'do': 'confirm',
            'attendance_detail_id': detail_id,
            'child_id': child_id,
            'school_username': username
        }, function (response) {
            if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    /**
     * Tìm kiếm trẻ theo điều kiện nhập vào màn hình thêm thông tin sức khỏe của trẻ
     */
    $('body').on('click', '.js_child-health-search', function () {
        var school_username = $(this).attr('data-username');
        var school_id = $(this).attr('data-id');
        var is_new = $(this).attr('data-isnew');
        var page = $(this).attr('data-page');
        var class_id = $('#class_id option:selected').val();
        var keyword = $('#keyword').val();

        $('#child_health_list').html('');
        $('#search').addClass('x-hidden');
        $('#loading').removeClass('x-hidden');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/search'], {
            'keyword': keyword,
            'school_id': school_id,
            'class_id': class_id,
            'page': page,
            'is_new': is_new,
            'school_username': school_username,
            'func': 'searchhealthchild'
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            $('#search').removeClass('x-hidden');
            $('#loading').addClass('x-hidden');
            if (response.results) {
                $('#child_health_list').html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $('#search').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Xử lý kích vào tên trẻ ở màn hình thêm chỉ số sức khỏe của trẻ
    $('body').on('click', '.js_school-child-health', function () {
        var child_id = $(this).attr('data-id');

        $('.hideAll').addClass('hidden');
        $('#health_' + child_id).removeClass('hidden');
        $('.js_school-child-health').css({'color': '#4083a9', 'font-weight': 'normal'});

        $('#child_' + child_id).css({'color': 'red', 'font-weight': 'bold'});
        $('.submit_hidden').removeClass('hidden');
        // $("[id[name*='user_id']").each(function() {
        //     user_ids.push($(this).val());
        // });
        // moreInfo.removeClass('x-hidden');
        // buttonDiv.addClass('x-hidden');
    });


    // bắt sự kiện khi thêm thông tin sức khỏe của trẻ
    $(document).on('submit', '#school_add_health_index', function (e) {
        var _this = $(this);
        var submit = _this.find('button[type="submit"]');

        /* show loading */
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading']);

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        e.preventDefault();
        $.ajax({
            url: api['school/child'],
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false
        }).done(function (data) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            /* hide loading */
            submit.prop('disabled', false);
            submit.html(submit.data('text'));
            if (data.success) {
                modal('#modal-success', {title: __['Succes'], message: data.message});
                modal_hidden(timeModal);
            } else if (data.error) {
                modal('#modal-error', {title: __['Error'], message: data.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                /* hide loading */
                submit.prop('disabled', false);
                submit.html(submit.data('text'));
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // bắt sự kiện khi sửa thông tin sức khỏe của trẻ
    $(document).on('submit', '#school_edit_child_health', function (e) {
        var _this = $(this);
        var submit = _this.find('button[type="submit"]');

        /* show loading */
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading']);

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        e.preventDefault();
        $.ajax({
            url: api['school/child'],
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false
        }).done(function (data) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            /* hide loading */
            submit.prop('disabled', false);
            submit.html(submit.data('text'));
            if (data.success) {
                modal('#modal-success', {title: __['Succes'], message: data.message});
                modal_hidden(timeModal);
            } else if (data.error) {
                modal('#modal-error', {title: __['Error'], message: data.message});
            } else if (data.callback) {
                eval(data.callback);
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                /* hide loading */
                submit.prop('disabled', false);
                submit.html(submit.data('text'));
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Xử lý khi xoa thông tin sức khỏe của trẻ
    $('body').on('click', '.js_school-health-delete', function () {
        var id = $(this).attr('data-id');
        var child_id = $(this).attr('data-child');
        var handle = $(this).attr('data-handle');
        var username = $(this).attr('data-username');
        var module = $(this).attr('data-module');

        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['school/child'], {
                'do': handle,
                'child_id': child_id,
                'id': id,
                'school_username': username,
                'module': module
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (data.error) {
                    modal('#modal-error', {title: __['Error'], message: data.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    // Xử lý ẩn bảng nhập lịch học và hiển thị ô import excel khi bật import = excel
    $('#import_excel').on('change', function () {
        if ($(this).is(':checked')) {
            $('#cate_list').addClass("hidden");
            if ($('#select_excel').hasClass('hidden')) {
                $('#select_excel').removeClass('hidden');
            }
            $('#do').val("import");
            if ($('#preview_excel').hasClass('x-hidden')) {
                $('#preview_excel').removeClass('x-hidden');
            }
            if (!$('#preview_normal').hasClass('x-hidden')) {
                $('#preview_normal').addClass('x-hidden');
            }
        } else {
            if ($('#cate_list').hasClass('hidden')) {
                $('#cate_list').removeClass("hidden");
            }
            if (!($('#select_excel').hasClass('hidden'))) {
                $('#select_excel').addClass("hidden");
            }
            $('#do').val("add");
            if ($('#preview_normal').hasClass('x-hidden')) {
                $('#preview_normal').removeClass("x-hidden");
            }
            if (!$('#preview_excel').hasClass('x-hidden')) {
                $('#preview_excel').addClass("x-hidden");
            }
        }
    });

    //Xử lý khi chọn TRẠNG THÁI điểm danh ở màn hình tổng hợp điểm danh lớp
    $('body').on('change', '.js_school-attendance-status', function () {
        var id = $(this).attr('data-id');
        var child_id = $(this).attr('data-child');
        var attendance_status = $(this).val();
        var attendance_date = $(this).attr('data-date');
        var school_username = $(this).attr('data-username');
        var old_status = $(this).attr('data-status');

        // Xử lý đổi màu background select khi thay đổi trạng thái điểm danh
        if (attendance_status == 1) {
            $(this).css("background", '#fff');
        } else if (attendance_status == 2) {
            $(this).css("background", '#6495ed');
        } else if (attendance_status == 3) {
            $(this).css("background", '#008b8b');
        } else if (attendance_status == 4) {
            $(this).css("background", '#ff1493');
        } else {
            $(this).css("background", '#deb887');
        }


        // Set data-status = giá trị select để thay đổi lần sau đúng
        $(this).attr("data-status", parseInt(attendance_status));

        // Xử lý ô tổng hợp đi học và nghỉ của trẻ
        var presentCnt = $('#present_' + child_id).text();
        var absenceCnt = $('#absence_' + child_id).text();
        presentCnt = parseInt(presentCnt);
        absenceCnt = parseInt(absenceCnt);

        // Tổng đi, nghỉ học của cả lớp
        var classPresentCount = $('#present_count_' + id).text();
        var classAbsenceCnt = $('#absence_count_' + id).text();
        classPresentCount = parseInt(classPresentCount);
        classAbsenceCnt = parseInt(classAbsenceCnt);

        // Tổng trong khoảng thời gian tìm kiếm
        var totalPresent = $('#total_present').text();
        var totalAbsence = $('#total_absence').text();
        totalPresent = parseInt(totalPresent);
        totalAbsence = parseInt(totalAbsence);

        var presentStatus = [1, 2, 3];
        var absenceStatus = [0, 4];

        if ((jQuery.inArray(parseInt(attendance_status), presentStatus) !== -1) && (jQuery.inArray(parseInt(old_status), absenceStatus) !== -1)) {
            // Trẻ
            var newPresentCnt = presentCnt + 1;
            var newAbsenceCnt = absenceCnt - 1;
            ;

            $('#present_' + child_id).html(newPresentCnt);
            $('#absence_' + child_id).text(newAbsenceCnt);

            // Cả lớp
            var newClassPresentCount = classPresentCount + 1;
            var newClassAbsenceCnt = classAbsenceCnt - 1;

            $('#present_count_' + id).html(newClassPresentCount);
            $('#absence_count_' + id).html(newClassAbsenceCnt);

            // Tổng trong khoảng thời gian
            var totalPresent = totalPresent + 1;
            var totalAbsence = totalAbsence - 1;

            $('#total_present').html(totalPresent);
            $('#total_absence').html(totalAbsence);
        }
        if ((jQuery.inArray(parseInt(attendance_status), absenceStatus) !== -1) && (jQuery.inArray(parseInt(old_status), presentStatus) !== -1)) {
            // Trẻ
            var newPresentCnt = presentCnt - 1;
            var newAbsenceCnt = absenceCnt + 1;
            $('#present_' + child_id).text(newPresentCnt);
            $('#absence_' + child_id).text(newAbsenceCnt);

            // Cả lớp
            var newClassPresentCount = classPresentCount - 1;
            var newClassAbsenceCnt = classAbsenceCnt + 1;

            $('#present_count_' + id).html(newClassPresentCount);
            $('#absence_count_' + id).html(newClassAbsenceCnt);

            // Tổng trong khoảng thời gian
            var totalPresent = totalPresent - 1;
            var totalAbsence = totalAbsence + 1;

            $('#total_present').html(totalPresent);
            $('#total_absence').html(totalAbsence);
        }

        // Xử lý ô tổng đi học và nghỉ học của cả lớp
        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/attendance'], {
            'do': 'change_attendance_status',
            'id': id,
            'attendance_date': attendance_date,
            'child_id': child_id,
            'attendance_status': attendance_status,
            'school_username': school_username,
            'old_status': old_status
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            if (response.error) {
                modal('#modal-error', {title: __['Error'], message: data.message});
            }
            // school_list.html(response.results);
            // $('#all_month').addClass('x-hidden');
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    /**
     * Begin - Tìm kiếm danh sách thuốc theo điều kiện nhập vào
     */
    $('body').on('click', '.js_medicine-search', function () {
        var school_username = $('#school_username').val();
        var begin = $('#begin').val();
        var end = $('#end').val();
        var child_id = $('#me_child_id option:selected').val();
        var class_id = $('#me_class_id option:selected').val();
        var level = $('#medicine_level option:selected').val();

        $("#medicine_confirm").addClass('hidden');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/medicine'], {
            'child_id': child_id,
            'class_id': class_id,
            'school_username': school_username,
            'level': level,
            'do': 'search_medicine',
            'begin': begin,
            'end': end
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            if (response.results) {
                $('#medicine_list').html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
            //eval($('#child_list').innerHTML);
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Hiển thị các phạm vi tương ứng (medicine)
    $('#medicine_level').on('change', function () {
        var _class = $('div[name=medicine_class]');
        var child = $('div[name=medicine_child]');
        switch (this.value) {
            case '3': //Chọn lớp
                if (_class.hasClass('x-hidden')) {
                    _class.removeClass('x-hidden');
                }
                if (!child.hasClass('x-hidden')) {
                    child.addClass('x-hidden');
                }
                break;

            case '4': //Chọn trẻ
                if (_class.hasClass('x-hidden')) {
                    _class.removeClass('x-hidden');
                }
                if (child.hasClass('x-hidden')) {
                    child.removeClass('x-hidden');
                }
                break;
            default:
        }
    });

    //Khi chọn class hiển thị danh sách trẻ tương ứng
    // $("#me_class_id").change(function () {
    //     var username = $(this).attr('data-username');
    //     var class_id = this.value;
    //
    //     if (class_id == '') {
    //         $('#list_child').html('');
    //     } else {
    //         $.post(api['school/medicine'], {
    //             'do': 'list_child',
    //             'school_username': username,
    //             'class_id': class_id
    //         }, function (response) {
    //             $('#list_child').html(response.results);
    //         }, 'json')
    //             .fail(function () {
    //                 modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
    //             });
    //     }
    // });

    $("#me_class_id").change(function () {
        var username = $(this).attr('data-username');
        // var view = $(this).attr('data-view');
        var class_id = this.value;

        if (class_id == '') {
            $('#me_child_id').html('');
        } else {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['school/medicine'], {
                'do': 'list_child_search',
                'school_username': username,
                'class_id': class_id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                if (response.results) {
                    $('#me_child_id').html(response.results);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: data.message});
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        }
    });
    // End medicine search

    //feedback list
    $('body').on('click', '.js_school-feedback-search', function () {
        var school_username = $('#school_username').val();
        var class_id = $('#class_id option:selected').val();

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/feedback'], {
            'class_id': class_id,
            'school_username': school_username,
            'do': 'search_feedback'
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.results) {
                $('#feedback_list').html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: data.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //xử lý check all xóa sổ liên lạc
    $('body').on('change', '#report_checkall', function () {
        $(".report_delete").prop('checked', $(this).prop("checked"));
    });

    //xử lý check all thông báo sổ liên lạc
    $('body').on('change', '#report_notify_checkall', function () {
        $(".report_notify").prop('checked', $(this).prop("checked"));
    });

    //Xử lý khi click vào checkbox Phải đăng ký
    $('#class_option').change(function () {
        var add_class = $('#add_class');
        var select_class = $('#select_class');

        if (this.checked) {
            if (add_class.hasClass('hidden')) {
                add_class.removeClass('hidden');
            }
            if (!(select_class.hasClass('hidden'))) {
                select_class.addClass('hidden');
            }
        } else {
            if (select_class.hasClass('hidden')) {
                select_class.removeClass('hidden');
            }
            if (!(add_class.hasClass('hidden'))) {
                add_class.addClass('hidden');
            }
        }
    });

    //Hiển thị popup khi kích vào thêm mới tài khoản giáo viên
    //Hiển thị popup để thêm hạng mục mới, trong màn hình thêm mới mẫu
    $('body').on('click', '.js_school-teacher-add', function () {
        $(".user_ids").remove();
        var user_ids = new Array();
        $("input[name*='user_id']").each(function () {
            user_ids.push($(this).val());
        });
        $(".user_ids").remove();
        $("#userIds").append("<input class='user_ids' type='hidden' name='new_user_ids' value='" + user_ids + "'>");

        $("#open_dialog_teacher_add").dialog({
            modal: true,
            height: 400,
            width: 800,
            closeOnEscape: true,
            resizable: true,
            autoOpen: true
        });
        $("#full_name").val("");
        $("#user_phone").val("");
        $("#email_teacher").val("");
        $("#password").val("");
    });

    //Khi click ADD trên màn hình popup khi thêm mới giáo viên
    $('body').on('click', '.js_school-teacher-add-done', function () {
        var username = $('#school_username').val();
        var full_name = $('#full_name').val();
        var user_phone = $('#user_phone').val();
        var email = $('#email_teacher').val();
        var password = $('#password').val();
        var gender = $('#gender').val();
        var user_ids = $('input[name=new_user_ids]').val();

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/teacher'], {
            'do': 'add_teacher_new_class',
            'school_username': username,
            'full_name': full_name,
            'user_phone': user_phone,
            'email': email,
            'password': password,
            'gender': gender,
            'user_ids': user_ids
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.results) {
                $("#open_dialog_teacher_add").dialog('close');
                $('#teacher_list').html(response.results);
                if ($('#telephone').val() == '') {
                    $('#telephone').val(response.phone);
                }
                if ($('#email').val() == '') {
                    $('#email').val(response.email);
                }
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else if (response.success) {
                modal('#modal-success', {title: __['Success'], message: response.message});
                modal_hidden(timeModal);
            } else {
                eval(response.callback);
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Xử lý thay đổi khi chọn lớp trong màn hình chỉ số tăng trưởng
    $('body').on('change', '.js_development-index-search', function () {
        var username = $(this).attr('data-username');
        var class_id = $('#class_id option:selected').val();

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        if (class_id != '') {
            $.post(api['school/child'], {
                'do': 'development_index_search',
                'school_username': username,
                'class_id': class_id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.results) {
                    $('#development_index').html(response.results);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        }

    });

    // Xử lý khi search thông tin sức khỏe
    $('body').on('click', '.js_school-chart-search', function () {
        var child_id = $(this).attr('data-child');
        var school_username = $(this).attr('data-username');
        var begin = $('#begin').val();
        var end = $('#end').val();
        var chart_list = $('#chart_list');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/child'], {
            'do': 'search_chart',
            'child_id': child_id,
            'begin': begin,
            'end': end,
            'school_username': school_username
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            if (response.results) {
                chart_list.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Xử lý cookie khi kích hiện/ẩn hướng dẫn ở màn hình dashboard
    $('body').on('click', '.js_school-hidden-guide', function () {
        var cookie_status = 0;
        if (!($('#guide_school').hasClass('hidden'))) {
            $('#guide_school').addClass('hidden');
        } else {
            $('#guide_school').removeClass('hidden');
            cookie_status = 1;
        }
        if (cookie_status) {
            $('#guide_hidden').addClass('hidden');
            $('#guide_show').removeClass('hidden');
        } else {
            $('#guide_show').addClass('hidden');
            $('#guide_hidden').removeClass('hidden');
        }

        var school_username = $(this).attr('data-username');
        var user_id = $(this).attr('data-id');

        var name = "guide_dashboard_" + school_username + "_" + user_id;

        createCookie(name, cookie_status);
    });

    // Xử lý cookie khi kích hiện/ẩn hướng dẫn ở màn hình học phí
    $('body').on('click', '.js_school-tuition-guide', function () {
        var cookie_status = 0;
        if (!($('#guide_tuition').hasClass('hidden'))) {
            $('#guide_tuition').addClass('hidden');
        } else {
            $('#guide_tuition').removeClass('hidden');
            cookie_status = 1;
        }
        if (cookie_status) {
            $('#guide_hidden').addClass('hidden');
            $('#guide_show').removeClass('hidden');
        } else {
            $('#guide_show').addClass('hidden');
            $('#guide_hidden').removeClass('hidden');
        }
        var school_username = $(this).attr('data-username');
        var user_id = $(this).attr('data-id');

        var name = "guide_tuition_" + school_username + "_" + user_id;

        createCookie(name, cookie_status);
    });

    // Set chiều cao trong màn hình học phí
    var height_auto = $("#left_height").height();
    $(".height_auto").height(height_auto);

    // Xử lý cookie khi kích hiện/ẩn hướng dẫn ở màn hình sổ liên lạc
    $('body').on('click', '.js_school-contact-book-guide', function () {
        var cookie_status = 0;
        if (!($('#guide_contact').hasClass('hidden'))) {
            $('#guide_contact').addClass('hidden');
        } else {
            $('#guide_contact').removeClass('hidden');
            cookie_status = 1;
        }
        if (cookie_status) {
            $('#guide_hidden').addClass('hidden');
            $('#guide_show').removeClass('hidden');
        } else {
            $('#guide_show').addClass('hidden');
            $('#guide_hidden').removeClass('hidden');
        }
        var school_username = $(this).attr('data-username');
        var user_id = $(this).attr('data-id');

        var name = "guide_contact_book_" + school_username + "_" + user_id;

        createCookie(name, cookie_status);
    });

    // Xử lý cookie khi kích hiện/ẩn hướng dẫn ở màn hình đón muộn
    $('body').on('click', '.js_school-pickup-guide', function () {
        var cookie_status = 0;
        if (!($('#guide_pickup').hasClass('hidden'))) {
            $('#guide_pickup').addClass('hidden');
        } else {
            $('#guide_pickup').removeClass('hidden');
            cookie_status = 1;
        }
        if (cookie_status) {
            $('#guide_hidden').addClass('hidden');
            $('#guide_show').removeClass('hidden');
        } else {
            $('#guide_show').addClass('hidden');
            $('#guide_hidden').removeClass('hidden');
        }
        var school_username = $(this).attr('data-username');
        var user_id = $(this).attr('data-id');

        var name = "guide_pickup_" + school_username + "_" + user_id;

        createCookie(name, cookie_status);
    });

    // Xử lý ẩn danh sách ở màn hình thông tin sử dụng dịch vụ khi thay đổi dịch vụ
    $('body').on('change', '.js_school-history-serviceid', function () {
        $('#usage_history').addClass('hidden');
    });

    // Hàm định dạng tiền
    $(document).ready(function () {
        //$('body .money_tui').mask('#,##0,000', {reverse: true});
        $('body .money_tui').mask('#,##0', {
            reverse: true,
            translation: {
                '#': {
                    pattern: /-|\d/,
                    recursive: true
                }
            },
            onChange: function (value, e) {
                e.target.value = value.replace(/(?!^)-/g, '').replace(/^,/, '').replace(/^-,/, '-');
            }
        });
    });

    // Hàm remove dấu "," sau dấu "-"
    $(document).ready(function () {
        $('body .money_tui').each(function () {
            var a = $(this).val();
            a = a.toString().replace(/^-,/, '-');
            $(this).val(a);
        });
    });

    // Tìm kiếm danh sách trẻ được giáo viên edit
    $('#child_edit_day_search').on('change', function () {
        var school_username = $(this).attr('data-username');
        var day_search = $('#child_edit_day_search option:selected').val();
        var child_list = $('#child_list');

        child_list.html('');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/child'], {
            'do': 'child_edit_search',
            'school_username': school_username,
            'day_search': day_search
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            if (response.results) {
                child_list.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Tìm kiếm danh sách trẻ được thêm mới
    $('#child_new_day_search').on('change', function () {
        var school_username = $(this).attr('data-username');
        var day_search = $('#child_new_day_search option:selected').val();
        var child_list = $('#child_list');

        child_list.html('');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/child'], {
            'do': 'child_new_search',
            'school_username': school_username,
            'day_search': day_search
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            if (response.results) {
                child_list.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Bắt sự kiện khi cập nhật link hướng dẫn sử dụng camera
    $(document).on('submit', '#update_camera_guide', function (e) {
        var _this = $(this);
        var submit = _this.find('button[type="submit"]');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        /* show loading */
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading']);

        e.preventDefault();
        $.ajax({
            url: api['school/setting'],
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false
        }).done(function (data) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            /* hide loading */
            submit.prop('disabled', false);
            submit.html(submit.data('text'));
            if (data.success) {
                modal('#modal-success', {title: __['Success'], message: data.message});
                modal_hidden(timeModal);
            } else if (data.error) {
                modal('#modal-error', {title: __['Error'], message: data.message});
            } else if (data.callback) {
                eval(data.callback);
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                /* hide loading */
                submit.prop('disabled', false);
                submit.html(submit.data('text'));
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Hiện sinh nhật của trẻ trong khoảng thời gian theo lớp
    $('body').on('click', '.js_birthday-search-child', function () {
        var username = $(this).attr('data-username');
        var fromDate = $('#fromDate').val();
        var toDate = $('#toDate').val();
        var class_id = $('#birthday_class_id option:selected').val();
        var birthday_list = $('#birthday');

        birthday_list.html('');
        $('#search').addClass('x-hidden');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/birthday'], {
            'do': 'child_search',
            'school_username': username,
            'class_id': class_id,
            'fromDate': fromDate,
            'toDate': toDate
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            $('#search').removeClass('x-hidden');
            $('#loading').addClass('x-hidden');
            if (response.results) {
                birthday_list.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $('#search').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Hiện sinh nhật của phụ huynh trong khoảng thời gian theo lớp
    $('body').on('click', '.js_birthday-search-parent', function () {
        var username = $(this).attr('data-username');
        var fromDate = $('#fromDate').val();
        var toDate = $('#toDate').val();
        var class_id = $('#birthday_class_id option:selected').val();
        var birthday_list = $('#birthday');

        birthday_list.html('');
        $('#search').addClass('x-hidden');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/birthday'], {
            'do': 'parent_search',
            'school_username': username,
            'class_id': class_id,
            'fromDate': fromDate,
            'toDate': toDate
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            $('#search').removeClass('x-hidden');
            $('#loading').addClass('x-hidden');
            if (response.results) {
                birthday_list.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $('#search').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Hiện sinh nhật của phụ huynh trong khoảng thời gian theo lớp
    $('body').on('click', '.js_birthday-search-teacher', function () {
        var username = $(this).attr('data-username');
        var fromDate = $('#fromDate').val();
        var toDate = $('#toDate').val();
        var birthday_list = $('#birthday');

        birthday_list.html('');
        $('#search').addClass('x-hidden');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/birthday'], {
            'do': 'teacher_search',
            'school_username': username,
            'fromDate': fromDate,
            'toDate': toDate
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            $('#search').removeClass('x-hidden');
            $('#loading').addClass('x-hidden');
            if (response.results) {
                birthday_list.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $('#search').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Xử lý khi thay đổi số ngày học trong tháng của 1 trẻ
    $('body').on('change', '.day_of_child', function () {
        var child_id = $(this).attr('child_id');
        var day_of_child = $(this).val();
        // $('.day_of_child_val_' + child_id).val(day_of_child);
        $('.day_of_child_val_' + child_id).each(function () {
            var type = $(this).attr('data-type');
            $(this).val(day_of_child);
            if (type == 1) {
                // Nếu là phí
                var child_id = $(this).attr('child_id');
                var fee_id = $(this).attr('fee_id');

                var quantity = this.value;
                quantity = !$.isNumeric(quantity) ? 0 : parseInt(quantity);

                var deductionQty = $('#fee_quantity_deduction_' + child_id + '_' + fee_id).val();
                deductionQty = !$.isNumeric(deductionQty) ? 0 : parseInt(deductionQty);

                var price = $('#fee_unit_price_' + child_id + '_' + fee_id).cleanVal();
                price = !$.isNumeric(price) ? 0 : parseInt(price);

                var deductionPrice = $('#fee_unit_price_deduction_' + child_id + '_' + fee_id).val();
                deductionPrice = !$.isNumeric(deductionPrice) ? 0 : parseInt(deductionPrice);

                //Tính thành tiền của mỗi khoản phí
                var fee_amount = quantity * price - deductionQty * deductionPrice;
                // chuyển định dạng
                fee_amount = fee_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

                $('#fee_amount_' + child_id + '_' + fee_id).val(fee_amount);
            } else {
                // Nếu là dịch vụ
                var child_id = $(this).attr('child_id');
                var service_id = $(this).attr('service_id');

                var quantity = this.value;
                quantity = !$.isNumeric(quantity) ? 0 : parseInt(quantity);

                var deductionQty = $('#service_quantity_deduction_' + child_id + '_' + service_id).val();
                deductionQty = !$.isNumeric(deductionQty) ? 0 : parseInt(deductionQty);

                var servicePrice = $('#service_unit_price_' + child_id + '_' + service_id).cleanVal();
                servicePrice = !$.isNumeric(servicePrice) ? 0 : parseInt(servicePrice);

                var servicePriceDeduction = $('#service_unit_price_deduction_' + child_id + '_' + service_id).val();
                servicePriceDeduction = !$.isNumeric(servicePriceDeduction) ? 0 : parseInt(servicePriceDeduction);

                //Tính thành tiền của mỗi dịch vụ
                var service_amount = quantity * servicePrice - deductionQty * servicePriceDeduction;
                service_amount = service_amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

                $('#service_amount_' + child_id + '_' + service_id).val(service_amount);
            }
            //Tính tổng học phí của trẻ
            calculateChildTuition(child_id);

            //Tính tổng học phí của cả lớp
            calculateClassTuition();
        });
    });

    //Hiển thị popup để xem trước lịch học
    $('body').on('click', '.js_school-schedule-preview', function () {
        var username = $('#school_username').val();
        var schedule_name = $('input[name=schedule_name]').val();
        var schedule_begin = $('input[name=begin]').val();
        var schedule_description = $('textarea[name=description]').val();
        var schedule_level = $('#schedule_level').val();
        if ($('#use_category').is(':checked')) {
            var schedule_category = 'on';
        } else {
            var schedule_category = 'off';
        }

        if ($('#study_saturday').is(':checked')) {
            var schedule_saturday = 'on';
        } else {
            var schedule_saturday = 'off';
        }

        var schedule_class_level_id = $('#class_level_id').val();
        var schedule_class_id = $('#class_id').val();
        // Lấy danh sách thời gian có trên màn hình
        var startList = new Array();
        $('input[name*=start]').each(function () {
            startList.push($(this).val());
        });

        // Lấy danh sách thứ 2 có trên màn hình
        var monList = new Array();
        $('textarea[name*=subject_detail_mon]').each(function () {
            monList.push($(this).val());
        });

        // Lấy danh sách thứ 3 có trên màn hình
        var tueList = new Array();
        $('textarea[name*=subject_detail_tue]').each(function () {
            tueList.push($(this).val());
        });

        // Lấy danh sách thứ 4 có trên màn hình
        var wedList = new Array();
        $('textarea[name*=subject_detail_wed]').each(function () {
            wedList.push($(this).val());
        });

        // Lấy danh sách thứ 5 có trên màn hình
        var thuList = new Array();
        $('textarea[name*=subject_detail_thu]').each(function () {
            thuList.push($(this).val());
        });

        // Lấy danh sách thứ 6 có trên màn hình
        var friList = new Array();
        $('textarea[name*=subject_detail_fri]').each(function () {
            friList.push($(this).val());
        });

        // Lấy danh sách chương trình học có trên màn hình (nếu chọn)
        var categoryList = new Array();
        if (schedule_category == 'on') {
            $('input[name*=category]').each(function () {
                categoryList.push($(this).val());
            });
        }

        // Lấy danh sách thứ 7 có trên màn hình (nếu chọn)
        var satList = new Array();
        if (schedule_saturday == 'on') {
            $('textarea[name*=subject_detail_sat]').each(function () {
                satList.push($(this).val());
            });
        }

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/schedule'], {
            'do': 'preview',
            'school_username': username,
            'schedule_name': schedule_name,
            'schedule_level': schedule_level,
            'schedule_begin': schedule_begin,
            'schedule_description': schedule_description,
            'schedule_category': schedule_category,
            'schedule_saturday': schedule_saturday,
            'class_level_id': schedule_class_level_id,
            'class_id': schedule_class_id,
            'startList': startList,
            'monList': monList,
            'tueList': tueList,
            'wedList': wedList,
            'thuList': thuList,
            'friList': friList,
            'categoryList': categoryList,
            'satList': satList
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.results) {
                $('#open_dialog_schedule').html(response.results);
                $("#open_dialog_schedule").dialog({
                    modal: true,
                    height: 600,
                    width: 1040,
                    closeOnEscape: true,
                    resizable: true,
                    autoOpen: true,
                    buttons: [{
                        text: __['Close'],
                        click: function () {
                            $(this).dialog("close");
                        }
                    }],
                    closeText: __['Close']
                });
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }

        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Bắt sự kiện khi tạo nhật ký cho trẻ bên nhà trường
    $(document).on('submit', '#add_child_journal_school', function (e) {
        var _this = $(this);
        var submit = _this.find('button[type="submit"]');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        /* show loading */
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading']);

        e.preventDefault();
        $.ajax({
            url: api['school/child'],
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false
        }).done(function (data) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            /* hide loading */
            submit.prop('disabled', false);
            submit.html(submit.data('text'));
            if (data.success) {
                modal('#modal-success', {title: __['Success'], message: data.message});
                modal_hidden(timeModal);
                $("input[name*='file']").val("");
            } else if (data.error) {
                modal('#modal-error', {title: __['Error'], message: data.message});
            } else if (data.callback) {
                modal('#modal-success', {title: __['Success'], message: __["Success"]});
                modal_hidden(timeModal);
                $("input[name*='file']").val("");
                //eval(data.callback);
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                /* hide loading */
                submit.prop('disabled', false);
                submit.html(submit.data('text'));
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Xử lý khi xóa album ảnh
    $('body').on('click', '.js_school-journal-delete-album', function () {
        var journal_id = $(this).attr('data-id');
        var child_id = $(this).attr('data-child');
        var handle = $(this).attr('data-handle');
        var username = $(this).attr('data-username');
        var _this = $(this);
        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['school/child'], {
                'do': handle,
                'child_journal_album_id': journal_id,
                'child_id': child_id,
                'school_username': username
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                var journal_album = _this.parents('.journal_album');
                journal_album.hide();
                $("#modal").modal('hide');
                $(".modal-backdrop").hide();
                // if (response.callback) {
                //     eval(response.callback);
                // } else if(response.error) {
                //     modal('#modal-error', {title: __['Error'], message: response.message});
                // } else {
                //     window.location.reload();
                // }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    $('body').on('click', '.edit_caption', function () {
        var _this = $(this);
        var journal_album = _this.parents('.journal_album');
        var caption_show = journal_album.find('.caption_show');
        var caption_edit = journal_album.find('.caption_edit');
        caption_show.hide();
        caption_edit.removeClass('x-hidden');
    });

    // Xử lý khi sửa caption album ảnh
    $('body').on('change', '.js_school-journal-edit-caption_change', function () {
        var journal_id = $(this).attr('data-id');
        var child_id = $(this).attr('data-child');
        var username = $(this).attr('data-username');
        var caption = $(this).val();
        var _this = $(this);
        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/child'], {
            'do': 'edit_caption',
            'child_journal_album_id': journal_id,
            'school_username': username,
            'child_id': child_id,
            'caption': caption
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            var journal_album = _this.parents('.journal_album');
            var caption_show = journal_album.find('.caption_show');
            var caption_edit = journal_album.find('.caption_edit');
            caption_show.text(caption);
            caption_show.show();
            caption_edit.hide();
            // if (response.callback) {
            //     eval(response.callback);
            // } else if(response.error) {
            //     modal('#modal-error', {title: __['Error'], message: response.message});
            // } else {
            //     window.location.reload();
            // }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Xử lý khi sửa caption album ảnh
    $('body').on('change', '.js_school-journal-edit-caption', function () {
        var journal_id = $(this).attr('data-id');
        var child_id = $(this).attr('data-child');
        var username = $(this).attr('data-username');
        var caption = $('.caption_' + journal_id).val();
        var _this = $(this);
        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/child'], {
            'do': 'edit_caption',
            'child_journal_album_id': journal_id,
            'school_username': username,
            'child_id': child_id,
            'caption': caption
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            var journal_album = _this.parents('.journal_album');
            var caption_show = journal_album.find('.caption_show');
            var caption_edit = journal_album.find('.caption_edit');
            caption_show.text(caption);
            caption_show.show();
            caption_edit.hide();
            // if (response.callback) {
            //     eval(response.callback);
            // } else if(response.error) {
            //     modal('#modal-error', {title: __['Error'], message: response.message});
            // } else {
            //     window.location.reload();
            // }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Xử lý khi xoa thông tin nhật ký của trẻ
    $('body').on('click', '.js_school-journal-delete', function () {
        var id = $(this).attr('data-id');
        var child_id = $(this).attr('data-child');
        var username = $(this).attr('data-username');
        var handle = $(this).attr('data-handle');
        var _this = $(this);

        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['school/child'], {
                'do': handle,
                'child_id': child_id,
                'school_username': username,
                'child_journal_id': id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                var fa_border = _this.parents('.col-sm-3');
                fa_border.hide();
                $("#modal").modal('hide');
                $(".modal-backdrop").hide();
                // if (response.callback) {
                //     eval(response.callback);
                // } else if(response.error) {
                //     modal('#modal-error', {title: __['Error'], message: response.message});
                // } else {
                //     window.location.reload();
                // }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    //Hiển thị popup để xem trước thực đơn
    $('body').on('click', '.js_school-menu-preview', function () {
        var username = $('#school_username').val();
        var menu_name = $('input[name=menu_name]').val();
        var menu_begin = $('input[name=begin]').val();
        var menu_description = $('textarea[name=description]').val();
        var menu_level = $('#menu_level').val();
        if ($('#use_meal').is(':checked')) {
            var menu_meal = 'on';
        } else {
            var menu_meal = 'off';
        }

        if ($('#study_saturday').is(':checked')) {
            var menu_saturday = 'on';
        } else {
            var menu_saturday = 'off';
        }

        var menu_class_level_id = $('#class_level_id').val();
        var menu_class_id = $('#class_id').val();
        // Lấy danh sách thời gian có trên màn hình
        var startList = new Array();
        $('input[name*=start]').each(function () {
            startList.push($(this).val());
        });

        // Lấy danh sách thứ 2 có trên màn hình
        var monList = new Array();
        $('textarea[name*=meal_detail_mon]').each(function () {
            monList.push($(this).val());
        });

        // Lấy danh sách thứ 3 có trên màn hình
        var tueList = new Array();
        $('textarea[name*=meal_detail_tue]').each(function () {
            tueList.push($(this).val());
        });

        // Lấy danh sách thứ 4 có trên màn hình
        var wedList = new Array();
        $('textarea[name*=meal_detail_wed]').each(function () {
            wedList.push($(this).val());
        });

        // Lấy danh sách thứ 5 có trên màn hình
        var thuList = new Array();
        $('textarea[name*=meal_detail_thu]').each(function () {
            thuList.push($(this).val());
        });

        // Lấy danh sách thứ 6 có trên màn hình
        var friList = new Array();
        $('textarea[name*=meal_detail_fri]').each(function () {
            friList.push($(this).val());
        });

        // Lấy danh sách chương trình học có trên màn hình (nếu chọn)
        var mealList = new Array();
        if (menu_meal == 'on') {
            $('input[name*=meal]').each(function () {
                mealList.push($(this).val());
            });
        }

        // Lấy danh sách thứ 7 có trên màn hình (nếu chọn)
        var satList = new Array();
        if (menu_saturday == 'on') {
            $('textarea[name*=meal_detail_sat]').each(function () {
                satList.push($(this).val());
            });
        }

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/menu'], {
            'do': 'preview',
            'school_username': username,
            'menu_name': menu_name,
            'menu_level': menu_level,
            'menu_begin': menu_begin,
            'menu_description': menu_description,
            'menu_meal': menu_meal,
            'menu_saturday': menu_saturday,
            'class_level_id': menu_class_level_id,
            'class_id': menu_class_id,
            'startList': startList,
            'monList': monList,
            'tueList': tueList,
            'wedList': wedList,
            'thuList': thuList,
            'friList': friList,
            'mealList': mealList,
            'satList': satList
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.results) {
                $('#open_dialog_menu').html(response.results);
                $("#open_dialog_menu").dialog({
                    modal: true,
                    height: 600,
                    width: 1040,
                    closeOnEscape: true,
                    resizable: true,
                    autoOpen: true,
                    buttons: [{
                        text: __['Close'],
                        click: function () {
                            $(this).dialog("close");
                        }
                    }],
                    closeText: __['Close']
                });
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }

        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Bắt sự kiện khi upload một file Excel chi tiết menu lên
    $('body').on('click', '.js_school-menu-preview-excel', function (e) {
        $('#do').val('preview_excel');
    });
    $('body').on('click', '.menu_submit', function (e) {
        if ($('#import_excel').is(':checked')) {
            $('#do').val('import');
        } else {
            $('#do').val('add');
        }
    });

    // Xử lý khi tìm kiếm theo năm góc nhật ký
    $('body').on('click', '.js_school-journal-search', function () {
        var child_id = $(this).attr('data-id');
        var school_username = $(this).attr('data-username');
        var year = $('#year').val();
        var journal_list = $('#journal_list');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/child'], {
            'do': 'search_diary',
            'child_id': child_id,
            'school_username': school_username,
            'year': year
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            if (response.results) {
                journal_list.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Xử lý khi tìm kiếm theo năm góc nhật ký ở màn hình chung
    $('body').on('click', '.js_school-diary-search', function () {
        var child_id = $('#attendance_child_id').val();
        var class_id = $('#attendance_class_id').val();
        var school_username = $(this).attr('data-username');
        var is_new = $(this).attr('data-isnew');
        var year = $('#year').val();
        var journal_list = $('#journal_list');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/child'], {
            'do': 'search_diary_class',
            'child_id': child_id,
            'class_id': class_id,
            'school_username': school_username,
            'year': year,
            'is_new': is_new
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            if (response.results) {
                journal_list.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Xử lý khi tìm kiếm theo năm góc nhật ký ở màn hình chung
    $('body').on('click', '.js_school-health-search', function () {
        var child_id = $('#attendance_child_id').val();
        var class_id = $('#attendance_class_id').val();
        var school_username = $(this).attr('data-username');
        var is_new = $(this).attr('data-isnew');
        var begin = $('#begin').val();
        var end = $('#end').val();
        var chart_list = $('#chart_list');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/child'], {
            'do': 'search_health_class',
            'child_id': child_id,
            'class_id': class_id,
            'school_username': school_username,
            'begin': begin,
            'end': end,
            'is_new': is_new
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            if (response.results) {
                chart_list.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Xử lý ẩn hiện nhật ký
    $('body').on('click', '.show_or_hidden_diary', function () {
        var id = $(this).attr('data-id');
        $('.album_images_' + id).toggle();
    });

    $('#class_leave_datepicker').datetimepicker({
        format: DATE_FORMAT,
        defaultDate: new Date()
    });

    //Bắt sự kiện khi ngày nghỉ học thay đổi. Trên màn hình Tốt nghiệp của lớp
    $('#class_leave_datepicker').on("dp.change", function (e) {
        var username = $('#school_username').val();
        var class_id = $('#class_id').val();
        var class_end_at = $('#class_end_at').val();

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $('#class_children_info').html('');
        $('button[type="submit"]').prop('disabled', true);
        if (class_end_at.length > 8) {
            $.post(api['school/tuition'], {
                'do': 'class_load_4leave',
                'school_username': username,
                'class_id': class_id,
                'class_end_at': class_end_at
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.results) {
                    $('#class_children_info').html(response.results);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
                $('button[type="submit"]').prop('disabled', false);
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    $('button[type="submit"]').prop('disabled', false);
                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        }
    });

    //Xuất excel danh sách điểm danh của trẻ
    $('body').on('click', '.js_attendance-export-excel', function () {
        var username = $(this).attr('data-username');
        var fromDate = $('#fromDate').val();
        var toDate = $('#toDate').val();
        var class_id = $('#classatt_class_id option:selected').val();

        $('#search').addClass('x-hidden');
        $('#export_excel').addClass('x-hidden');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/attendance'], {
            'do': 'export_attendance_class',
            'school_username': username,
            'class_id': class_id,
            'fromDate': fromDate,
            'toDate': toDate
        }, function (response) {
            if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else {
                var $a = $("<a>");
                $a.attr("href", response.file);
                $("body").append($a);
                $a.attr("download", response.file_name);
                $a[0].click();
                $a.remove();
            }
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            $('#search').removeClass('x-hidden');
            $('#export_excel').removeClass('x-hidden');
            $('#loading').addClass('x-hidden');
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $('#search').removeClass('x-hidden');
                $('#export_excel').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Hàm xử lý khi click vào Export thực đơn
    $('body').on('click', '.js_school-menu-export', function () {
        var handle = $(this).attr('data-handle');
        var username = $(this).attr('data-username');
        var id = $(this).attr('data-id');

        $(".js_school-menu-export").addClass("x-hidden");
        $(".processing_label").removeClass("x-hidden");

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/menu'], {
            'do': handle,
            'school_username': username,
            'id': id
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
            $(".js_school-menu-export").removeClass("x-hidden");
            $(".processing_label").addClass("x-hidden");
            var $a = $("<a>");
            $a.attr("href", response.file);
            $("body").append($a);
            $a.attr("download", response.file_name);
            $a[0].click();
            $a.remove();
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $(".js_school-menu-export").removeClass("x-hidden");
                $(".processing_label").addClass("x-hidden");
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Hiển thị danh sách trẻ khi chọn lớp ở màn hình chuyển lớp cho nhiều trẻ
    $('body').on('change', '.js_school-move-class-search', function () {
        var class_id = $('#class_id').val();
        var school_username = $(this).attr('data-username');
        var submit = $('#btnSave_disable').find('button[type="submit"]');
        // Show loading
        $("#loading_full_screen").removeClass('hidden');
        if (class_id == '') {
            $("#loading_full_screen").addClass('hidden');
            $('#list_child').html('');
            submit.prop('disabled', true);
        } else {
            $.post(api['school/child'], {
                'do': 'search_move_class_child',
                'class_id': class_id,
                'school_username': school_username
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                if (response.results) {
                    $('#list_child').html(response.results);
                    submit.prop('disabled', false);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                    submit.prop('disabled', true);
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        }
    });

    //Xử lý khi tìm trẻ nghỉ liên tục
    $('body').on('click', '.js_school_conabs_dashboard', function () {
        var username = $(this).attr('data-username');
        var conabs_list = $('#conabs_list');

        $('#loading').removeClass('x-hidden');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/attendance'], {
            'do': 'conabs_dashboard',
            'school_username': username
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            $('#loading').addClass('x-hidden');
            if (response.results) {
                conabs_list.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $('#loading').addClass('x-hidden');
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Cái này để kéo các hàng trong table - Sổ liên lạc
    $(document).ready(function () {
        var fixHelperModified = function (e, tr) {
                var $originals = tr.children();
                var $helper = tr.clone();
                $helper.children().each(function (index) {
                    $(this).width($originals.eq(index).width())
                });
                return $helper;
            },
            updateIndex = function (e, ui) {
                $('td.index', ui.item.parent()).each(function (i) {
                    $(this).html(i + 1);
                });
                $('.positions', ui.item.parent()).each(function (i) {
                    $(this).val(i + 1);
                });
            };

        $("#sort tbody").sortable({
            helper: fixHelperModified,
            stop: updateIndex
        }).disableSelection();
    });
    $('body').on('change', '.js_change_status_summary', function () {
        var status = $('#status option:selected').val();
        if (status == 2) {
            $('#cashier').removeClass('x-hidden');
            $('#fromDateClass').removeClass('x-hidden');
            $('#toDateClass').removeClass('x-hidden');
        } else {
            $('#cashier').addClass('x-hidden');
            $('#fromDateClass').addClass('x-hidden');
            $('#toDateClass').addClass('x-hidden');
        }
        if (status == '') {
            $('#search').attr('disabled', true);
            $('#export_excel').attr('disabled', true);
        } else {
            $('#search').attr('disabled', false);
            $('#export_excel').attr('disabled', false);
        }
    });
    //Hiển thị danh sách trẻ đã đóng học phí
    $('body').on('click', '.js_school-tuition-paid-search', function () {
        var username = $(this).attr('data-username');
        var fromDate = $('#fromDate').val();
        var toDate = $('#toDate').val();
        var cashier_id = $('#cashier_id option:selected').val();
        var status = $('#status option:selected').val();
        var class_id = $('#class_id option:selected').val();
        var summary_paid_list = $('#summary_paid_list');

        summary_paid_list.html('');
        $('#search').addClass('x-hidden');
        $('#export_excel').addClass('x-hidden');
        if (status == 2) {
            $('#cashier').removeClass('x-hidden');
        } else {
            $('#cashier').addClass('x-hidden');
        }
        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/tuition'], {
            'do': 'summary_paid_search',
            'school_username': username,
            'fromDate': fromDate,
            'toDate': toDate,
            'cashier_id': cashier_id,
            'status': status,
            'class_id': class_id
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            $('#search').removeClass('x-hidden');
            $('#export_excel').removeClass('x-hidden');
            $('#loading').addClass('x-hidden');
            if (response.results) {
                summary_paid_list.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $('#search').removeClass('x-hidden');
                $('#export_excel').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Hàm xử lý khi click vào Export học phí
    $('body').on('click', '.js_school-tuition-summary-export-excel', function () {
        var username = $(this).attr('data-username');
        var fromDate = $('#fromDate').val();
        var toDate = $('#toDate').val();
        var cashier_id = $('#cashier_id option:selected').val();
        var status = $('#status option:selected').val();
        var class_id = $('#class_id option:selected').val();

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $("#export_excel").addClass("x-hidden");
        $("#search").addClass("x-hidden");
        $("#loading").removeClass("x-hidden");

        $.post(api['school/tuition'], {
            'do': 'export_summary',
            'school_username': username,
            'fromDate': fromDate,
            'toDate': toDate,
            'cashier_id': cashier_id,
            'status': status,
            'class_id': class_id
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            $("#export_excel").removeClass("x-hidden");
            $("#search").removeClass("x-hidden");
            $("#loading").addClass("x-hidden");
            var $a = $("<a>");
            $a.attr("href", response.file);
            $("body").append($a);
            $a.attr("download", response.file_name);
            $a[0].click();
            $a.remove();
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $("#export_excel").removeClass("x-hidden");
                $("#search").removeClass("x-hidden");
                $("#loading").addClass("x-hidden");
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Xử lý khi click vào checkbox đăng ký dịch vụ (màn hình thêm trẻ)
    $('#for_service').click(function () {
        var service_box = $('#service_box');

        if (this.checked) {
            service_box.removeClass('x-hidden');
        } else {
            service_box.addClass('x-hidden');
        }
    });

    $('body').on('click', '.js_school-class-level-add', function () {
        $('.add_class_level_box').removeClass('x-hidden');
        $('.edit_box').addClass('x-hidden');
    });

    $('body').on('click', '.close_box_add', function () {
        $('.add_class_level_box').addClass('x-hidden');
    });

    $('body').on('click', '.close_box_edit', function () {
        $('.edit_box').addClass('x-hidden');
    });

    $('body').on('click', '.js_class-level-edit', function () {
        var id = $(this).attr('data-id');
        var name = $(this).attr('data-name');
        var description = $(this).attr('data-description');
        $('input[name=class_level_id]').val(id);
        $('.edit_box').find('input[name=class_level_name]').val(name);
        $('.edit_box').find('textarea[name=description]').val(description);

        $('.edit_box').removeClass('x-hidden');
        $('.add_class_level_box').addClass('x-hidden');
        $('.span_class_level_name').text(name);
    });

    // Cập nhật các bước khởi tạo trường
    $('body').on('click', '.js_school-step', function () {
        var step = $(this).attr('data-step');
        var username = $(this).attr('data-username');
        var view = $(this).attr('data-view');
        var subview = $(this).attr('data-subview');
        $.post(api['school/setting'], {
            'do': 'update_step',
            'step': step,
            'view': view,
            'subview': subview,
            'school_username': username
        }, function (response) {
            if (response.callback) {
                eval(response.callback);
            } else if (data.error) {
                modal('#modal-error', {title: __['Error'], message: data.message});
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $('#loading').addClass('x-hidden');
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Khởi tạo lại trường
    $('body').on('click', '.js_school-step-restart', function () {
        var step = $(this).attr('data-step');
        var username = $(this).attr('data-username');

        confirm(__['Actions'], __['Are you sure you want to do this?'], function () {
            $.post(api['school/setting'], {
                'do': 'update_step',
                'step': step,
                'school_username': username
            });
            window.location.reload();
        });
    });

    //Xử lý khi ẩn 1 trẻ khỏi danh sách trẻ thôi học
    $('body').on('click', '.js_school-delete-child-4leave', function () {
        var username = $(this).attr('data-username');
        var child_id = $(this).attr('data-id');

        $("#export_to_excel_" + child_id).addClass("x-hidden");
        $("#delete_child_" + child_id).addClass("x-hidden");
        $("#processing_label_" + child_id).removeClass("x-hidden");

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            $.post(api['school/child'], {
                'do': 'delete_child_leave',
                'school_username': username,
                'child_id': child_id
            }, function (response) {
                if (response.callback) {
                    eval(response.callback);
                } else if (data.error) {
                    modal('#modal-error', {title: __['Error'], message: data.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    $("#export_to_excel_" + child_id).removeClass("x-hidden");
                    $("#processing_label_" + child_id).addClass("x-hidden");
                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    // Xử lý show full birthday và less birthday_teacher ở dashboard
    $('body').on('click', '.full_birthday_teacher_but', function () {
        $("#full_birthday_teacher").removeClass('x-hidden');
        $("#hide_birthday_teacher").addClass('x-hidden');
    });

    $('body').on('click', '.hide_birthday_teacher_but', function () {
        $("#full_birthday_teacher").addClass('x-hidden');
        $("#hide_birthday_teacher").removeClass('x-hidden');
    });

    // Xử lý show full birthday và less birthday ở dashboard
    $('body').on('click', '.full_birthday_but', function () {
        $("#full_birthday").removeClass('x-hidden');
        $("#hide_birthday").addClass('x-hidden');
    });

    $('body').on('click', '.hide_birthday_but', function () {
        $("#full_birthday").addClass('x-hidden');
        $("#hide_birthday").removeClass('x-hidden');
    });

    // Xử lý show full trẻ nghỉ nhiều liên tục và less trẻ nghỉ nhiều liên tục ở dashboard
    $('body').on('click', '.full_absent_but', function () {
        $("#full_absent").removeClass('x-hidden');
        $("#hide_absent").addClass('x-hidden');
    });

    $('body').on('click', '.hide_absent_but', function () {
        $("#full_absent").addClass('x-hidden');
        $("#hide_absent").removeClass('x-hidden');
    });

    //xử lý check all chọn điểm danh về
    $('body').on('change', '#attendance_came_back_checkall', function () {
        $(".attendance_cameback_check").prop('checked', $(this).prop("checked"));
    });

    $('body').on('click', '#came-tab', function () {
        $("#attendance_do").val('save_rollup');
    });

    $('body').on('click', '#go-tab', function () {
        $("#attendance_do").val('save_rollup_back');
    });

    /**
     * Tìm kiếm trẻ thôi học theo điều kiện nhập vào
     */
    $('body').on('click', '.js_leave-school-child-search', function () {
        var school_username = $(this).attr('data-username');
        var school_id = $(this).attr('data-id');
        var is_new = $(this).attr('data-isnew');
        var page = $(this).attr('data-page');
        var class_id = $('#class_id option:selected').val();
        var keyword = $('#keyword').val();

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $('#child_list').html('');
        $('#search').addClass('x-hidden');

        $.post(api['school/search'], {
            'keyword': keyword,
            'school_id': school_id,
            'class_id': class_id,
            'page': page,
            'is_new': is_new,
            'school_username': school_username,
            'func': 'searchchildleave'
        }, function (response) {
            // Show loading
            $("#loading_full_screen").addClass('hidden');

            if (response.results) {
                $('#search').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');

                $('#child_list').html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                $('#search').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });
    $("#school_year").change(function () {
        var search_with = $("input[name='do']").val();
        if ($('#school_list_point')) {
            $("#point_class_id_import").trigger("change");
        }
        if (search_with == 'comment') {
            $("#point_class_level_id").trigger("change");
        }
    })
    //Khi chọn khối hiển thị danh sách lớp tương ứng ở màn hình xem - chấm điểm
    $("#point_class_level_id").change(function () {
        var username = $(this).attr('data-username');
        var class_level_id = this.value;
        var search_with = $("input[name='do']").val();
        // $("#point_subject_id").disabled = true;
        // Show loading

        $("#loading_full_screen").removeClass('hidden');
        if ($('#school_list_point')) {
            $('#school_list_point').html('');
        }
        if (class_level_id == '') {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            if (search_with == 'comment' || search_with == 'importManual') {//màn comment
                var submit = $('#submit_id');
                submit.prop('disabled', true);
                $('#child_comment').html('');
            }
            // trả lại text default của select box class id assgin
            if ($('#point_class_id').val() != undefined) {
                var temp_option_class_id = $("select#point_class_id option")[0].innerHTML;
                $('#point_class_id').html('');
                $("#point_class_id").append(new Option(temp_option_class_id, ""));
            }
            // trả lại text default của select box class id import
            if ($('#point_class_id_import').val() != undefined) {
                var temp_option_class_id_import = $("select#point_class_id_import option")[0].innerHTML;
                $('#point_class_id_import').html('');
                $("#point_class_id_import").append(new Option(temp_option_class_id_import, ""));
            }
            // trả lại text default của select box class id  comment
            if ($('#point_class_id_comment').val() != undefined) {
                var temp_option_class_id_comment = $("select#point_class_id_comment option")[0].innerHTML;
                $('#point_class_id_comment').html('');
                $("#point_class_id_comment").append(new Option(temp_option_class_id_comment, ""));
            }
            // trả lại text default của select box child id comment
            if ($('#point_child_id_comment').val() != undefined) {
                var temp_option_child_id = $("select#point_child_id_comment option")[0].innerHTML;
                $('#point_child_id_comment').html('');
                $("#point_child_id_comment").append(new Option(temp_option_child_id, ""));
            }
        } else {
            $.post(api['school/point'], {
                'do': 'list_class',
                'school_username': username,
                'class_level_id': class_level_id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                if (response.results) {
                    $('#point_class_id').html(response.results);
                    $('#point_class_id_import').html(response.results);
                    $('#point_class_id_comment').html(response.results);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
                if (search_with == 'comment' && $('#point_class_id_comment')) {
                    var submit = $('#submit_id');
                    submit.prop('disabled', true);
                    $('#child_comment').html('');
                    // $('#point_child_id_comment').html('');
                    $("#point_class_id_comment").trigger("change");
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        }

    });
    // ADD START MANHDD 17/06/2021
    //Khi chọn khối hiển thị danh sách lớp tương ứng ở màn hình khóa học
    $("#course_class_level_id").change(function () {
        var username = $(this).attr('data-username');
        var class_level_id = this.value;
        var search_with = $("input[name='do']").val();
        var school_year = $('#school_year').val();
        // $("#point_subject_id").disabled = true;
        // Show loading

        $("#loading_full_screen").removeClass('hidden');
        if ($('#course_list_point')) {
            $('#course_list_point').html('');
        }
        if (class_level_id == '') {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            // trả lại text default của select box class id
            if ($('#course_class_id').val() != undefined) {
                var temp_option_class_id = $("select#course_class_id option")[0].innerHTML;
                $('#course_class_id').html('');
                $("#course_class_id").append(new Option(temp_option_class_id, ""));
            }
        } else {
            $.post(api['school/course'], {
                'do': 'list_class',
                'school_username': username,
                'school_year': school_year,
                'class_level_id': class_level_id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                if (response.results) {
                    $('#course_class_id').html(response.results);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        }

    });
    // ADD END MANHDD 17/06/2021
    //Xử lý khi chọn class ở màn hình phân công giáo viên theo từng môn
    $("#point_class_id").change(function () {
        var username = $(this).attr('data-username');
        var class_id = this.value;
        var class_level_id = $('#point_class_level_id').val();
        var school_year = $('#school_year').val();
        var semester = $('#semester').val();
        var error = $('.alert.alert-danger');
        var success = $('.alert.alert-success');
        var submit = $('#submit_id');

        if (success.is(":visible")) success.hide();
        if (error.is(":visible")) error.hide();
        $('#subject_list').html('');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        if (class_id != '') {
            $('#loading').removeClass('x-hidden');

            $.post(api['school/point'], {
                'do': 'list_subject',
                'school_username': username,
                'class_id': class_id,
                'class_level_id': class_level_id,
                'semester': semester,
                'school_year': school_year
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $('#loading').addClass('x-hidden');

                if (response.results) {
                    $('#subject_list').html(response.results);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    $('#loading').addClass('x-hidden');
                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        } else {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

        }
    });

    //Xử lý khi chọn học kỳ ở màn hình phân công giáo viên theo từng môn
    $("#semester").change(function () {
        var semester = this.value;
        var search_with = $("input[name='do']").val();
        var point_child_id_comment = $('#point_child_id_comment').val();
        if (search_with == 'importManual') {
            $("#point_subject_id").trigger("change");
        } else if (search_with == 'comment' && point_child_id_comment) {
            $("#point_child_id_comment").trigger("change");
        } else if (search_with == 'assign') {
            $("#point_class_id").trigger("change");
        }

        // nếu chọn 'cả năm' thì sẽ disable phần chọn môn học, để tính điểm cả năm = sum ( all subject )

        // if (semester == '0') {
        //     document.getElementById("point_subject_id").style.display = "none";
        //     document.getElementById("point_subject_id").required = false;
        // } else {
        //     document.getElementById("point_subject_id").style.display = "block";
        //     document.getElementById("point_subject_id").required = true;
        // }
    });
    //ADD START MANHDD 24/06/2021
    // Sử dụng trong chức năng nhập điểm ( mỗi khi nhấn thêm cột mới sẽ resize lại table )
    function resize_table() {
        $table_front = $('.table-pinned').eq(0);
        $table_back = $('.table-pinned').eq(1);
        for(j = 0;j<$table_back.find('th').length;j++) {
            if($table_back.find('th').eq(j).hasClass("pinned")) {
                $table_front.find('th').eq(j).css("width",$table_back.find('th').eq(j)[0].getBoundingClientRect().width);
                $table_front.find('th').eq(j).css("height",$table_back.find('th').eq(j)[0].getBoundingClientRect().height);
            }
        }
        for(j = 0;j<$table_back.find('tr').length;j++) {
            if($table_back.find('tr').eq(j).find('td').length > 0) {
                for(k = 0;k<$table_back.find('tr').eq(j).find('td').length;k++) {
                    if($table_back.find('tr').eq(j).find('td').eq(k).hasClass("pinned")) {
                        $table_front.find('tr').eq(j).find('td').eq(k).css("width",$table_back.find('tr').eq(j).find('td').eq(k)[0].getBoundingClientRect().width);
                        $table_front.find('tr').eq(j).find('td').eq(k).css("height",$table_back.find('tr').eq(j).find('td').eq(k)[0].getBoundingClientRect().height);

                    }
                }
            }
        }
    }
    //ADD END MANHDD 24/06/2021
    //ADD START MANHDD 10/06/2021
    // Thêm cột tx khi nhấn Add Freq-point ở màn hình import điểm bẳng tay ( học kỳ 1 or 2 )
    $(document).on('click', '.js_point_tx-add', function (e) {
        e.preventDefault();
        var semester = $('#semester').val();
        var score_fomula = $("input[name='score_fomula']").val();
        var flag_added = false;
        var count_tx = 0;
        var index_insert_td = 0;
        var table = $("table tbody").eq(1); // màn hình bị chia thành 2 tbody, tbody 1 chứa những cột pined, tbody2 là cả table mình cần
        if (semester != '0') {

            table.find('tr').each(function (index, el) {
                var $tds = $(this).find('td');
                var th_length = $('tr:nth-child(2) th').length; // lấy số lượng cột điểm ( bị duplicate nên sẽ chia 2 để lấy tên cột cho chính xác )
                if (score_fomula == 'vn') {
                    for (i = Math.floor(th_length/2); i <= th_length; i++) {
                        if ($('tr:nth-child(2) th').eq(i).text() == 'tx') {
                            count_tx++;
                        } else if ($('tr:nth-child(2) th').eq(i).text() == 'gk') {

                            if (!flag_added) {
                                // vì table đang là 2 bảng chồng lên nhau nên phải thêm vào cả 2 để tăng length th_length cho đều
                                // thêm vào mặt trước
                                $('tr:nth-child(2) th').eq((i - 1)).after('<th>tx</th>');
                                //thêm vào mặt sau
                                $('tr:nth-child(2) th').eq((i - 1)-Math.floor(th_length/2)).after('<th style=\"display:none;\">tx</th>');
                                // tăng độ dài cột semester
                                var cols =  $('tr:nth-child(1) th').eq(7).attr("colspan");
                                $('tr:nth-child(1) th').eq(7).attr("colspan",parseInt(cols) + 1);
                                flag_added = true;
                                index_insert_td = i - Math.floor(th_length/2)- 1 + 3;
                                // count_tx=0;
                            }
                            if(count_tx == 6) {
                                count_tx =0;
                                $('.js_point_tx-add').prop('disabled', true);
                            }
                            $tds.eq(index_insert_td).after('<td><input name=\"point\" type=\"number\" min=\"0\" step=\"0.01\" style=\"max-width: 50px\"\n value=\"\"/></td>');
                            count_tx =0;
                            break;
                        }
                    }
                }
            });
            resize_table();
        }
    });
    // Thêm cột gk khi nhấn Add Mid-Point ở màn hình import điểm bẳng tay ( học kỳ 1 or 2 )
    $(document).on('click', '.js_point_gk-add', function (e) {
        e.preventDefault();
        var semester = $('#semester').val();
        var score_fomula = $("input[name='score_fomula']").val();
         var flag_added = false;
        var count_gk = 0;
        var index_insert_td = 0;
        var table = $("table tbody").eq(1); // màn hình bị chia thành 2 tbody, tbody 1 chứa những cột pined, tbody2 là cả table mình cần
        if (semester != '0') {
            table.find('tr').each(function (index, el) {
                var $tds = $(this).find('td');
                var th_length = $('tr:nth-child(2) th').length; // lấy số lượng cột điểm ( bị duplicate nên sẽ chia 2 để lấy tên cột cho chính xác )
                if (score_fomula == 'vn') {
                    for (i = Math.floor(th_length/2); i <= th_length; i++) {
                        if ($('tr:nth-child(2) th').eq(i).text() == 'gk') {
                            count_gk++;
                        } else if ($('tr:nth-child(2) th').eq(i).text() == 'ck') {
                            if (!flag_added) {
                                // vì table đang là 2 bảng chồng lên nhau nên phải thêm vào cả 2 để tăng length th_length cho đều
                                // thêm vào mặt trước
                                $('tr:nth-child(2) th').eq(i - 1).after('<th>gk</th>');
                                //thêm vào mặt sau
                                $('tr:nth-child(2) th').eq((i - 1)-Math.floor(th_length/2)).after('<th style=\"display:none;\">gk</th>');
                                // tăng độ dài cột semester
                                var cols =  $('tr:nth-child(1) th').eq(7).attr("colspan");
                                $('tr:nth-child(1) th').eq(7).attr("colspan",parseInt(cols) + 1);
                                flag_added = true;
                                index_insert_td = i - Math.floor(th_length/2)- 1 + 3;
                            }
                            if(count_gk == 2) {
                                count_gk =0;
                                $('.js_point_gk-add').prop('disabled', true);
                             }
                            $tds.eq(index_insert_td).after('<td><input name=\"point\" type=\"number\" min=\"0\" step=\"0.01\" style=\"max-width: 50px\"\n value=\"\"/></td>');
                            count_gk =0;
                            break;
                        }
                    }
                }
            });
            //ADD START MANHDD 19/06/2021 => fix lỗi khi add thêm col trong phần nhập điểm table (không pined ) thay đổi width mà table (pined) vẫn lấy giá trị width cũ
            resize_table();
            //ADD END MANHDD 19/06/2021
        }
    });
    // Thêm cột tx khi nhấn Add Freq-point1 ở màn hình import điểm bẳng tay ( cả năm )
    $(document).on('click', '.js_point_tx1-add', function (e) {
        e.preventDefault();
        var semester = $('#semester').val();
        var score_fomula = $("input[name='score_fomula']").val();
        var flag_added = false;
        var count_tx = 1;
        var index_insert_td = 0;
        var table = $("table tbody").eq(1); // màn hình bị chia thành 2 tbody, tbody 1 chứa những cột pined, tbody2 là cả table mình cần
        if (semester == '0') {
            table.find('tr').each(function (i, el) {
                var $tds = $(this).find('td');
                var th_length = $('tr:nth-child(2) th').length; // lấy số lượng cột điểm ( bị duplicate nên sẽ chia 2 để lấy tên cột cho chính xác )
                if (score_fomula == 'vn') {
                    for (i = Math.floor(th_length/2); i <= th_length; i++) {
                        if ($('tr:nth-child(2) th').eq(i).text() == 'tx') {
                            count_tx++;
                        } else if ($('tr:nth-child(2) th').eq(i).text() == 'gk') {
                            if (!flag_added) {
                                // vì table đang là 2 bảng chồng lên nhau nên phải thêm vào cả 2 để tăng length th_length cho đều
                                // thêm vào mặt trước
                                $('tr:nth-child(2) th').eq((i - 1)).after('<th>tx</th>');
                                //thêm vào mặt sau
                                $('tr:nth-child(2) th').eq((i - 1)-Math.floor(th_length/2)).after('<th style=\"display:none;\">tx</th>');
                                // tăng độ dài cột semester
                                var cols =  $('tr:nth-child(1) th').eq(9).attr("colspan");
                                $('tr:nth-child(1) th').eq(9).attr("colspan",parseInt(cols) + 1);
                                flag_added = true;
                                index_insert_td = i - Math.floor(th_length/2)- 1 + 3;
                                // count_tx=0;
                            }
                            if(count_tx == 6) {
                                count_tx =0;
                                $('.js_point_tx1-add').prop('disabled', true);
                            }
                            $tds.eq(index_insert_td).after('<td><input name=\"point\" type=\"number\" min=\"0\" step=\"0.01\" style=\"max-width: 50px\"\n value=\"\"/></td>');
                            count_tx =0;
                            break;
                        }
                    }
                }
            });
            resize_table();
        }
    });
    // Thêm cột gk khi nhấn Add Mid-Point ở màn hình import điểm bẳng tay ( cả năm )
    $(document).on('click', '.js_point_gk1-add', function (e) {
        e.preventDefault();
        var semester = $('#semester').val();
        var score_fomula = $("input[name='score_fomula']").val();
        var th_length = $('tr:nth-child(2) th').length; // lấy số lượng cột điểm ( bị duplicate nên sẽ chia 2 để lấy tên cột cho chính xác )
        var flag_added = false;
        var count_gk = 1;
        var index_insert_td = 0;
        var table = $("table tbody").eq(1); // màn hình bị chia thành 2 tbody, tbody 1 chứa những cột pined, tbody2 là cả table mình cần
        if (semester == '0') {
            table.find('tr').each(function (i, el) {
                var $tds = $(this).find('td');
                if (score_fomula == 'vn') {
                    for (i = Math.floor(th_length/2); i <= th_length; i++) {
                        if ($('tr:nth-child(2) th').eq(i).text() == 'gk') {
                            count_gk++;
                        } else if ($('tr:nth-child(2) th').eq(i).text() == 'ck') {
                            if (!flag_added) {
                                // vì table đang là 2 bảng chồng lên nhau nên phải thêm vào cả 2 để tăng length th_length cho đều
                                // thêm vào mặt trước
                                $('tr:nth-child(2) th').eq(i - 1).after('<th>gk</th>');
                                //thêm vào mặt sau
                                $('tr:nth-child(2) th').eq((i - 1)-Math.floor(th_length/2)).after('<th style=\"display:none;\">gk</th>');
                                // tăng độ dài cột semester
                                var cols =  $('tr:nth-child(1) th').eq(9).attr("colspan");
                                $('tr:nth-child(1) th').eq(9).attr("colspan",parseInt(cols) + 1);
                                flag_added = true;
                                index_insert_td = i - Math.floor(th_length/2)- 1 + 3;
                            }
                            if(count_gk == 2) {
                                count_gk =0;
                                $('.js_point_gk1-add').prop('disabled', true);
                            }
                            $tds.eq(index_insert_td).after($.trim('<td><input name=\"point\" type=\"number\" min=\"0\" step=\"0.01\" style=\"max-width: 50px\"\n value=\"\"/></td>'));
                            count_gk =0;
                            break;
                        }
                    }
                }
            });
            resize_table();
        }
    });
    // Thêm cột tx khi nhấn Add Freq-point2 ở màn hình import điểm bẳng tay ( cả năm )
    $(document).on('click', '.js_point_tx2-add', function (e) {
        e.preventDefault();
        var semester = $('#semester').val();
        var score_fomula = $("input[name='score_fomula']").val();
        // var th_length = $('tr:nth-child(2) th').length; // lấy số lượng cột điểm ( bị duplicate nên sẽ chia 2 để lấy tên cột cho chính xác )
        var flag_added = false;
        var count_tx = 1;
        var index_insert_td = 0;
        var table = $("table tbody").eq(1); // màn hình bị chia thành 2 tbody, tbody 1 chứa những cột pined, tbody2 là cả table mình cần
        if (semester == '0') {
            table.find('tr').each(function (i, el) {
                var $tds = $(this).find('td');
                var th_length = $('tr:nth-child(2) th').length; // lấy số lượng cột điểm ( bị duplicate nên sẽ chia 2 để lấy tên cột cho chính xác )
                // đặt 1 biến flag đánh dấu cột hiện tại đang ở kỳ nào
                var semester_flag = 1;
                if (score_fomula == 'vn') {
                    for (i = Math.floor(th_length/2); i <= th_length; i++) {
                        if (semester_flag == 1) {
                            if ($('tr:nth-child(2) th').eq(i).text() == 'ck') {
                                //chuyển sang học kỳ 2
                                semester_flag = 2;
                            }
                        } else if (semester_flag == 2) {

                            if ($('tr:nth-child(2) th').eq(i).text() == 'tx') {
                                    count_tx++;
                            } else if ($('tr:nth-child(2) th').eq(i).text() == 'gk') {
                                if (!flag_added) {
                                    // vì table đang là 2 bảng chồng lên nhau nên phải thêm vào cả 2 để tăng length th_length cho đều
                                    // thêm vào mặt trước
                                    $('tr:nth-child(2) th').eq((i - 1)).after('<th>tx</th>');
                                    //thêm vào mặt sau
                                    $('tr:nth-child(2) th').eq((i - 1)-Math.floor(th_length/2)).after('<th style=\"display:none;\">tx</th>');
                                    // tăng độ dài cột semester
                                    var cols =  $('tr:nth-child(1) th').eq(10).attr("colspan");
                                    $('tr:nth-child(1) th').eq(10).attr("colspan",parseInt(cols) + 1);
                                    flag_added = true;
                                    index_insert_td = i - Math.floor(th_length/2)- 1 + 3;
                                    // count_tx=0;
                                }
                                if(count_tx == 6) {
                                    count_tx =0;
                                    $('.js_point_tx2-add').prop('disabled', true);
                                }
                                $tds.eq(index_insert_td).after('<td><input name=\"point\" type=\"number\" min=\"0\" step=\"0.01\" style=\"max-width: 50px\"\n value=\"\"/></td>');
                                count_tx =0;
                                break;
                            }
                        }
                    }
                }
            });
            resize_table();
        }
    });
    // Thêm cột gk khi nhấn Add Mid-Point ở màn hình import điểm bẳng tay ( cả năm )
    $(document).on('click', '.js_point_gk2-add', function (e) {
        e.preventDefault();
        var semester = $('#semester').val();
        var score_fomula = $("input[name='score_fomula']").val();
        var th_length = $('tr:nth-child(2) th').length; // lấy số lượng cột điểm ( bị duplicate nên sẽ chia 2 để lấy tên cột cho chính xác )
        var flag_added = false;
        var count_gk = 1;
        var index_insert_td = 0;
        var table = $("table tbody").eq(1); // màn hình bị chia thành 2 tbody, tbody 1 chứa những cột pined, tbody2 là cả table mình cần
        if (semester == '0') {
            table.find('tr').each(function (i, el) {
                var $tds = $(this).find('td');
                // đặt 1 biến flag đánh dấu cột hiện tại đang ở kỳ nào
                var semester_flag = 1;
                if (score_fomula == 'vn') {
                    for (i = Math.floor(th_length/2); i <= th_length; i++) {
                        if (semester_flag == 1) {
                            if ($('tr:nth-child(2) th').eq(i).text() == 'ck') {
                                //chuyển sang học kỳ 2
                                semester_flag = 2;
                            }
                        } else if (semester_flag == 2) {
                            if ($('tr:nth-child(2) th').eq(i).text() == 'gk') {
                                count_gk++;
                            } else if ($('tr:nth-child(2) th').eq(i).text() == 'ck') {
                                if (!flag_added) {
                                    // vì table đang là 2 bảng chồng lên nhau nên phải thêm vào cả 2 để tăng length th_length cho đều
                                    // thêm vào mặt trước
                                    $('tr:nth-child(2) th').eq(i - 1).after('<th>gk</th>');
                                    //thêm vào mặt sau
                                    $('tr:nth-child(2) th').eq((i - 1)-Math.floor(th_length/2)).after('<th style=\"display:none;\">gk</th>');
                                    // tăng độ dài cột semester
                                    var cols =  $('tr:nth-child(1) th').eq(10).attr("colspan");
                                    $('tr:nth-child(1) th').eq(10).attr("colspan",parseInt(cols) + 1);
                                    flag_added = true;
                                    index_insert_td = i - Math.floor(th_length/2)- 1 + 3;
                                }
                                if(count_gk == 2) {
                                    count_gk =0;
                                    $('.js_point_gk2-add').prop('disabled', true);
                                }
                                $tds.eq(index_insert_td).after($.trim('<td><input name=\"point\" type=\"number\" min=\"0\" step=\"0.01\" style=\"max-width: 50px\"\n value=\"\"/></td>'));
                                count_gk =0;
                                break;
                            }
                        }
                    }
                }
            });
            resize_table();
        }
    });
    //ADD END MANHDD 10/06/2021
    $("#point_subject_id").change(function () {
        var submit = $('#submit_id');

        var username = $(this).attr('data-username');
        var class_level_id = $('#point_class_level_id').val();
        var class_id = $('#point_class_id_import').val();
        var school_year = $('#school_year').val();
        var subject_id = this.value;
        var search_with = $("input[name='do']").val();
        if (search_with != 'importManual') return;
        if (subject_id == '') {
            submit.prop('disabled', true);
            return;
        } else {
            submit.prop('disabled', false);
        }
         //Đếm số lượng column điểm xem có max hay không
        var count_tx1 = 0;
        var count_tx2 =0;
        var count_gk1 =0;
        var count_gk2= 0;
        var semester_flag = 1; // đánh dấu đang ở kỳ nào

        var is_last_semester = $('#is_last_semester').val();
        var semester = $('select[name="semester"]').val();
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading...']);
        $('.action-point').hide();
        $('#school_list_point').html('');
        $('#result_info').html('');
        // Show loading
        $("#loading_full_screen").removeClass('hidden');
        $.post(api['school/point'], {
            'do': 'showDataImport',
            'school_username': username,
            'class_level_id': class_level_id,
            'class_id': class_id,
            'school_year': school_year,
            'is_last_semester': is_last_semester,
            'subject_id': subject_id,
            'semester': semester
        }, function (data) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            submit.prop('disabled', false);
            submit.html(submit.data('text'));
            $('.action-point').show();
            $('#school_list_point').html(data.results);
            var th_length = $('tr:nth-child(2) th').length; // lấy số lượng cột điểm ( bị duplicate nên sẽ chia 2 để lấy tên cột cho chính xác )

            for (i = 0; i <= th_length / 2; i++) {
                if ($('tr:nth-child(2) th').eq(i).text() == 'tx') {
                    if(semester_flag==1) {
                        count_tx1++;
                    }else if ( semester_flag ==2) {
                        count_tx2++;
                    }
                } else if ($('tr:nth-child(2) th').eq(i).text() == 'gk') {
                    if(semester_flag==1) {
                        count_gk1++;
                    }else if (semester_flag ==2) {
                        count_gk2++;
                    }
                }else if($('tr:nth-child(2) th').eq(i).text() == 'ck') {
                    if(i==Math.floor(th_length/2)-1) {
                        break;
                    }  else {
                        semester_flag = 2;
                    }
                }
            }
            console.log('count_tx1 =' +count_tx1);
            console.log('count_tx2 =' +count_tx2);
            console.log('count_gk1 =' +count_gk1);
            console.log('count_gk2 =' +count_gk2);
            if (semester != 0) {
                //button
                $('.js_point_tx1-add').hide();
                $('.js_point_gk1-add').hide();
                $('.js_point_tx2-add').hide();
                $('.js_point_gk2-add').hide();
                //label
                $('.point_tx1-add').hide();
                $('.point_gk1-add').hide();
                $('.point_tx2-add').hide();
                $('.point_gk2-add').hide();
                //button
                $('.js_point_tx-add').show();
                $('.js_point_gk-add').show();
                //label
                $('.point_tx-add').show();
                $('.point_gk-add').show();
                $('.js_point_tx-add').prop('disabled', false);
                $('.js_point_gk-add').prop('disabled', false);
            } else if (semester == 0) {
                //button
                $('.js_point_tx-add').hide();
                $('.js_point_gk-add').hide();
                //label
                $('.point_tx-add').hide();
                $('.point_gk-add').hide();
                //button
                $('.js_point_tx1-add').show();
                $('.js_point_gk1-add').show();
                $('.js_point_tx2-add').show();
                $('.js_point_gk2-add').show();
                // label
                $('.point_tx1-add').show();
                $('.point_gk1-add').show();
                $('.point_tx2-add').show();
                $('.point_gk2-add').show();
                $('.js_point_tx1-add').prop('disabled', false);
                $('.js_point_gk1-add').prop('disabled', false);
                $('.js_point_tx2-add').prop('disabled', false);
                $('.js_point_gk2-add').prop('disabled', false);
            }
            if(count_tx1 == 6) {
                $('.js_point_tx-add').prop('disabled', true);
                $('.js_point_tx1-add').prop('disabled', true);
            }
            if(count_tx2 == 6) {
                $('.js_point_tx2-add').prop('disabled', true);
            }
            if(count_gk1 == 2) {
                $('.js_point_gk-add').prop('disabled', true);
                $('.js_point_gk1-add').prop('disabled', true);
            }
            if(count_gk2 == 2) {
                $('.js_point_gk2-add').prop('disabled', true);
            }

        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                submit.prop('disabled', false);
                submit.html(submit.data('text'));

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Xử lý khi chọn học kỳ ở màn hình phân công giáo viên theo từng môn
    $("#search_with").change(function () {
        var search_with = this.value;
        var point_class_id_import = $('#point_class_id_import').val();
        // nếu đã chọn lớp ( point_class_id_import ) thì sẽ reload lại phần chọn môn học hoặc học sinh (point_subject_id/point_student_id)
        if (point_class_id_import != '') {
            if (search_with == '0') { //subject
                document.getElementById("point_student_code").required = false;
                document.getElementById("point_student_code").style.display = "none";
                document.getElementById("point_subject_id").required = true;
                document.getElementById("point_subject_id").style.display = 'block';
                document.getElementById("semester").style.display = 'block';

            } else { //student
                document.getElementById("point_student_code").required = true;
                document.getElementById("point_student_code").style.display = 'block';
                document.getElementById("point_subject_id").required = false;
                document.getElementById("point_subject_id").style.display = 'none';
                document.getElementById("semester").style.display = 'none';
            }
            $("#point_class_id_import").trigger("change");
        }
    });
    //Khi chọn lớp hiển thị danh sách môn học tương ứng màn hình point
    $("#point_class_id_import").change(function () {
        var username = $(this).attr('data-username');
        var class_id = this.value;
        var class_level_id = $('#point_class_level_id').val();
        var school_year = $('#school_year').val();
        var search_with = $("input[name='do']").val();
        // Show loading
        $("#loading_full_screen").removeClass('hidden');
        if ($('#school_list_point')) {
            $('#school_list_point').html('');

        }
        if (class_id == '') {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            if (search_with == 'importManual') {
                var submit = $('#submit_id');
                submit.prop('disabled', true);
            }
            // $('#point_subject_id').html('');
            // trả lại text default của select box subject id lists by subject
            if ($('#point_subject_id').val() != undefined) {
                var temp_option_subject_id = $("select#point_subject_id option")[0].innerHTML;
                $('#point_subject_id').html('');
                $("#point_subject_id").append(new Option(temp_option_subject_id, ""));
            }
        } else {
            $.post(api['school/point'], {
                'do': 'list_subject_import', // Sử dụng ở màn hình import điểm
                'school_username': username,
                'class_level_id': class_level_id,
                'class_id': class_id,
                'school_year': school_year,
                'search_with': search_with
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                if (response.results) {
                    if (search_with == 'search_with_subject' || search_with == 'importExcel' || search_with == 'importManual') {//nếu xem theo môn học thì fill môn học
                        $('#point_subject_id').html(response.results);
                    } else {//nếu xem theo học sinh thì fill học sinh search_with_student
                        $('#point_student_code').html(response.results);
                    }

                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        }
    });
    // ADD START MANHDD 09/06/2021
    //Bắt sự kiện khi nhập điểm bằng tay
    $(document).on('submit', '#point_import_Manual_form', function (e) {
        e.preventDefault();
        var username = $('#school_username').val();
        var class_id = $('#point_class_id_import').val();
        var school_year = $('#school_year').val();
        // var search_with = $("input[name='do']").val();
        var semester = $('#semester').val();
        var score_fomula = $("input[name='score_fomula']").val();
        var subject_id = $('#point_subject_id').val();
        var is_last_semester = $('#is_last_semester').val();
        var childs_points = [];
        var row = [];
        var th_length = $('tr:nth-child(2) th').length; // lấy số lượng cột điểm ( bị duplicate nên sẽ chia 2 để lấy tên cột cho chính xác )
        console.log('th_length= ' + th_length);
        var table = $("table tbody").eq(1); // màn hình bị chia thành 2 tbody, tbody 1 chứa những cột pined, tbody2 là cả table mình cần
        if (semester == '0') {
            table.find('tr').each(function (i, el) {
                row = {};
                var $tds = $(this).find('td');
                var child_name = $tds.eq(1).text();
                var child_code = $tds.eq(2).text();
                row['child_code'] = child_code;
                row['child_name'] = child_name;
                // đặt 1 biến flag đánh dấu cột hiện tại đang ở kỳ nào
                var semester_flag = 1;
                if (score_fomula == 'vn') {
                    var count_hs1 = 1;
                    var count_gk = 1;
                    var index_to_start = 0;
                    // vì các cột được thêm vào nên số lượng th_length/2 sẽ k chính xác => phải đếm từng số lượng cột
                    for (i = 0; i < th_length; i++) {
                        if ($('tr:nth-child(2) th').eq(i).text() == 'ck') {
                            index_to_start++; // ở dưới sẽ bắt đầu từ index này
                            console.log('index_to_start = '+ index_to_start);
                            if(index_to_start==2) {
                                index_to_start = i+1;
                                break;
                            }
                        }
                    }
                    for (i = index_to_start; i < th_length; i++) {
                        if (semester_flag == 1) {
                            if ($('tr:nth-child(2) th').eq(i).text() == 'tx') {
                                // lấy bắt đầu từ cột thứ 3 ( các cột chứa thẻ input - điểm )
                                row['hs1' + count_hs1] = $tds.eq(i-index_to_start + 3).find('input').val();
                                count_hs1++;
                            } else if ($('tr:nth-child(2) th').eq(i).text() == 'gk') {
                                row['gk1' + count_gk] = $tds.eq(i-index_to_start + 3).find('input').val();
                                count_gk++;
                            } else if ($('tr:nth-child(2) th').eq(i).text() == 'ck') {
                                row['ck1'] = $tds.eq(i-index_to_start + 3).find('input').val();
                                // lấy xong học kỳ 1, chuyển sang học kỳ 2
                                count_hs1 = 1;
                                count_gk = 1;
                                semester_flag = 2;
                            }
                        } else if (semester_flag == 2) {
                            if ($('tr:nth-child(2) th').eq(i).text() == 'tx') {
                                // lấy bắt đầu từ cột thứ 3 ( các cột chứa thẻ input - điểm )
                                row['hs2' + count_hs1] = $tds.eq(i-index_to_start + 3).find('input').val();
                                count_hs1++;
                            } else if ($('tr:nth-child(2) th').eq(i).text() == 'gk') {
                                row['gk2' + count_gk] = $tds.eq(i-index_to_start + 3).find('input').val();
                                count_gk++;
                            } else if ($('tr:nth-child(2) th').eq(i).text() == 'ck') {
                                row['ck2'] = $tds.eq(i-index_to_start + 3).find('input').val();
                                // Lấy xong kỳ 2, chuyển sang lấy điểm thi lại
                                semester_flag = 3;
                            }
                        } else {
                            row['re_exam'] = $tds.eq(i-index_to_start + 4).find('input').val();
                        }
                    }
                } else {
                    hs11 = $tds.eq(3).find('input').val();
                    hs12 = $tds.eq(4).find('input').val();
                    hs13 = $tds.eq(5).find('input').val();
                    gk11 = $tds.eq(6).find('input').val();
                    hs21 = $tds.eq(7).find('input').val();
                    hs22 = $tds.eq(8).find('input').val();
                    hs23 = $tds.eq(9).find('input').val();
                    gk21 = $tds.eq(10).find('input').val();
                    re_exam = $tds.eq(11).find('input').val();


                    row['hs11'] = hs11;
                    row['hs12'] = hs12;
                    row['hs13'] = hs13;
                    row['gk11'] = gk11;
                    row['hs21'] = hs21;
                    row['hs22'] = hs22;
                    row['hs23'] = hs23;
                    row['gk21'] = gk21;
                    row['re_exam'] = re_exam;
                }
                childs_points.push(row);
                // do something with productId, product, Quantity
            });
        } else {
            table.find('tr').each(function (i, el) {
                row = {};
                var $tds = $(this).find('td'),
                    child_name = $tds.eq(1).text();
                child_code = $tds.eq(2).text();
                row['child_code'] = child_code;
                row['child_name'] = child_name;
                if (score_fomula == 'vn') {
                    var count_hs1 = 1;
                    var count_hk = 1;
                    var index_to_start = 0;
                    // vì các cột được thêm vào nên số lượng th_length/2 sẽ k chính xác => phải đếm từng số lượng cột
                    for (i = 0; i < th_length; i++) {
                        if ($('tr:nth-child(2) th').eq(i).text() == 'ck') {
                            index_to_start = i+1; // ở dưới sẽ bắt đầu từ index này
                            break;
                        }
                    }
                    for (i = index_to_start; i < th_length; i++) {
                        if ($('tr:nth-child(2) th').eq(i).text() == 'tx') {
                            // lấy bắt đầu từ cột thứ 3 ( các cột chứa thẻ input - điểm )
                            row['hs' + semester + count_hs1] = $tds.eq(i-index_to_start + 3).find('input').val();
                            count_hs1++;
                        } else if ($('tr:nth-child(2) th').eq(i).text() == 'gk') {
                            row['gk' + semester + count_hk] = $tds.eq(i-index_to_start + 3).find('input').val();
                            count_hk++;
                        } else if ($('tr:nth-child(2) th').eq(i).text() == 'ck') {
                            row['ck' + semester] = $tds.eq(i-index_to_start + 3).find('input').val();
                            // lấy xong học kỳ 1, chuyển sang học kỳ 2
                        }
                    }
                } else {
                    row['hs' + semester + '1'] = $tds.eq(3).find('input').val();
                    row['hs' + semester + '2'] = $tds.eq(4).find('input').val();
                    row['hs' + semester + '3'] = $tds.eq(5).find('input').val();
                    row['gk' + semester + '1'] = $tds.eq(6).find('input').val();
                }
                childs_points.push(row);
            });
        }
        var submit = $('#submit_id');
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading...']);
        $('#result_info').html('');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/point'], {
            'school_username': username,
            'class_id': class_id,
            'do': 'importManual',
            'school_year': school_year,
            'semester': semester,
            'subject_id': subject_id,
            'is_last_semester': is_last_semester,
            'childs_points': childs_points
        }, function (data) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            if( data['error']) {
                submit.prop('disabled', false);
                submit.html(submit.data('text'));
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                return;
            }
            submit.prop('disabled', false);
            submit.html(submit.data('text'));
            $('#result_info').html(data.results);
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                submit.prop('disabled', false);
                submit.html(submit.data('text'));
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });
    // ADD END MANHDD 09/06/2021
    //Bắt sự kiện khi upload một file Excel điểm lên
    $(document).on('submit', '#point_import_excel_form', function (e) {
        e.preventDefault();

        var submit = $('#submit_id');
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading...']);
        $('#result_info').html('');
        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.ajax({
            url: api['school/point'],
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false
        }).done(function (data) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            submit.prop('disabled', false);
            submit.html(submit.data('text'));

            $('#result_info').html(data.results);
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                submit.prop('disabled', false);
                submit.html(submit.data('text'));

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });
    /**
     * Bắt sự kiện khi nhấn nút tìm kiếm ở màn hình course
     */
    $(document).on('submit', '#course_search_form', function (e) {
        e.preventDefault();
        var submit = $('#submit_id');
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading...']);
        $('#course_list_point').html('');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.ajax({
            url: api['school/course'],
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false
        }).done(function (data) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            submit.prop('disabled', false);
            submit.html(submit.data('text'));
            $('#course_list_point').html(data.results);

        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                submit.prop('disabled', false);
                submit.html(submit.data('text'));

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });
    /**
     * Bắt sự kiện khi nhấn nút tìm kiếm ở bảng điểm
     */
    $(document).on('submit', '#point_search_form', function (e) {
        e.preventDefault();
        var submit = $('#submit_id');
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading...']);
        $('#school_list_point').html('');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.ajax({
            url: api['school/point'],
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false
        }).done(function (data) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            submit.prop('disabled', false);
            submit.html(submit.data('text'));
            $('#school_list_point').html(data.results);

        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                submit.prop('disabled', false);
                submit.html(submit.data('text'));

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    $('body').on('click', '.delete_attendance_child', function () {
        var school_username = $(this).attr('data-username');
        var attendance_detail_id = $(this).attr('data-id');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $('#search').addClass('x-hidden');

        $.post(api['school/attendance'], {
            'school_username': school_username,
            'attendance_detail_id': attendance_detail_id,
            'do': 'delete_attendance_child'
        }, function (response) {
            if (response.callback) {
                eval(response.callback);
            } else if (data.error) {
                modal('#modal-error', {title: __['Error'], message: data.message});
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                $('#search').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Khi chọn lớp hiển thị danh sách trẻ tương ứng
    $("#point_class_id_comment").change(function () {
        var username = $(this).attr('data-username');
        var class_id = this.value;
        var submit = $('#submit_id');
        var search_with = $("input[name='do']").val();
        if (search_with == 'comment') {
            submit.prop('disabled', true);
            $('#child_comment').html('');
            // $("#point_class_id_comment").trigger("change");
        }
        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        if (class_id == '') {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            var temp_option_child_id = $("select#point_child_id_comment option")[0].innerHTML;
            // trả lại text default của select box child id // trong màn comment
            $('#point_child_id_comment').html('');
            $("#point_child_id_comment").append(new Option(temp_option_child_id, ""));
        } else {
            $.post(api['school/medicine'], {
                'do': 'list_child',
                'school_username': username,
                'class_id': class_id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                if (response.results) {
                    $('#point_child_id_comment').html(response.results);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        }
    });
    // khi chọn trẻ thì show comment tương ứng
    $("#point_child_id_comment").change(function () {
        var username = $(this).attr('data-username');
        var child_id = $('#point_child_id_comment').val();
        var class_level_id = $('#point_class_level_id').val();
        var class_id = $('#point_class_id_comment').val();
        var school_year = $('#school_year').val();
        var is_last_semester = $('#is_last_semester').val();
        var semester = $('select[name="semester"]').val();

        var submit = $('#submit_id');
        submit.prop('disabled', true);

        // Show loading
        $("#loading_full_screen").removeClass('hidden');
        if (child_id == '') {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            $('#child_comment').html('');
        } else {
            submit.prop('disabled', false);
            $.post(api['school/point'], {
                'do': 'show_comment_child', // Sử dụng ở màn hình thêm nhận xét
                'school_username': username,
                'class_level_id': class_level_id,
                'class_id': class_id,
                'school_year': school_year,
                'is_last_semester': is_last_semester,
                'child_id': child_id,
                'semester': semester
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                if (response.results) {
                    $('#child_comment').html(response.results);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        }
    });

    // $(document).on('submit', '#point_comment_form', function (e) {
    //     e.preventDefault();
    //     var submit = $('#submit_id');
    //     submit.data('text', submit.html());
    //     submit.prop('disabled', true);
    //     submit.html(__['Loading...']);
    //
    //     // Show loading
    //     $("#loading_full_screen").removeClass('hidden');
    //
    //     $.ajax({
    //         url: api['school/point'],
    //         type: 'POST',
    //         data: new FormData(this),
    //         processData: false,
    //         contentType: false
    //     }).done(function (data) {
    //         // Hidden loading
    //         $("#loading_full_screen").addClass('hidden');
    //
    //         submit.prop('disabled', false);
    //         submit.html(submit.data('text'));
    //
    //         $('#school_list_point').html(data.results);
    //     }, 'json')
    //         .fail(function () {
    //             // Hidden loading
    //             $("#loading_full_screen").addClass('hidden');
    //             submit.prop('disabled', false);
    //             submit.html(submit.data('text'));
    //
    //             modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
    //         });
    // });
});

