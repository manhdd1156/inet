/**
 * class js
 *
 * @package ConIu
 * @author QuanND
 */

// initialize API URLs
api['school/delete'] = ajax_path + "ci/bo/bo_delete.php";
api['school/search'] = ajax_path + "ci/bo/bo_search.php";
api['class/event'] = ajax_path + "ci/bo/class/boclass_event.php";
api['class/attendance'] = ajax_path + "ci/bo/class/boclass_attendance.php";
api['class/tuition'] = ajax_path + "ci/bo/boclass_tuition.php";
api['class/medicine'] = ajax_path + "ci/bo/class/boclass_medicine.php";
api['class/child'] = ajax_path + "ci/bo/class/boclass_child.php";
api['school/parentlist'] = ajax_path + "ci/bo/bo_parentlist.php";
api['class/service'] = ajax_path + "ci/bo/class/boclass_service.php";
api['class/feedback'] = ajax_path + "ci/bo/class/boclass_feedback.php";
api['class/pickup'] = ajax_path + "ci/bo/class/boclass_pickup.php";
api['class/report'] = ajax_path + "ci/bo/class/boclass_report.php";
api['class/point'] = ajax_path + "ci/bo/class/boclass_point.php";
api['class/conduct'] = ajax_path + "ci/bo/class/boclass_conduct.php";

var arrPickupClassDP = [];

$(function () {
    var timeModal = 1500;

    // run DataTable
    $('.js_dataTable').DataTable({
        "aoColumnDefs": [{'bSortable': false, 'aTargets': [-1]}],
        "language": {
            "decimal": "",
            "emptyTable": __["No data available in table"],
            "info": __["Showing _START_ to _END_ of _TOTAL_ results"],
            "infoEmpty": __["Showing 0 to 0 of 0 results"],
            "infoFiltered": "(filtered from _MAX_ total entries)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": __["Show"] + " " + "_MENU_" + " " + __["results"],
            "loadingRecords": __["Loading..."],
            "processing": __["Processing..."],
            "search": __["Search"],
            "zeroRecords": __["No matching records found"],
            "paginate": {
                "first": __["First"],
                "last": __["Last"],
                "next": __["Next"],
                "previous": __["Previous"]
            },
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            }
        }
    });

    // run metisMenu
    $(".js_metisMenu").metisMenu();

    // run open window
    $('body').on('click', '.js_open_window', function () {
        window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0');
        return false;
    });

    // ---- BEGIN - Màn hình: Thêm user có sẳn làm cha mẹ của một trẻ --------------------------------------------------
    /**
     * Tìm kiếm user khi nhập vào ô text
     */
    $('body').on('keyup', '#search-parent', search_delay(function () {
        var query = $(this).val();
        var user_ids = new Array();
        $("input[name*='user_id']").each(function () {
            user_ids.push($(this).val());
        });

        if (!is_empty(query)) {
            $('#search-parent-results').show();
            $.post(api['school/search'], {'query': query, 'user_ids': user_ids, 'func': 'parent'}, function (response) {
                if (response.callback) {
                    eval(response.callback);
                } else if (response.results) {
                    $('#search-parent-results .dropdown-widget-header').show();
                    $('#search-parent-results .dropdown-widget-body').html(response.results);
                } else {
                    $('#search-parent-results .dropdown-widget-header').hide();
                    $('#search-parent-results .dropdown-widget-body').html(render_template('#search-for', {'query': query}));
                }
            }, 'json');
        }
    }, 1000));

    /* show previous search-parent-results when the search-parent is clicked */
    $('body').on('click', '#search-parent', function () {
        if ($(this).val() != '') {
            $('#search-parent-results').show();
        }
    });

    /**
     * Hàm xử lý khi chọn user từ danh sách xổ ra
     */
    $('body').on('click', '.js_parent-select', function () {
        var user_id = $(this).attr('data-uid');
        var user_ids = new Array();
        $("input[name*='user_id']").each(function () {
            user_ids.push($(this).val());
        });
        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/parentlist'], {
            'user_id': user_id,
            'user_ids': user_ids,
            'func': 'add'
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.results) {
                if (response.results) {
                    $('#parent_list').html(response.results);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
                if ($('#parent_phone').val() == '') {
                    $('#parent_phone').val(response.phone);
                }
                if ($('#parent_email').val() == '') {
                    $('#parent_email').val(response.email);
                }
                $('#create_parent_account').prop('checked', response.no_parent);
                $('#create_parent_account').prop('disabled', !response.no_parent);
                $('#parent_name').prop('disabled', !response.no_parent);
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    $('body').on('click', '.js_parent-remove', function () {
        var user_id = $(this).attr('data-uid');
        var user_ids = new Array();
        $("input[name*='user_id']").each(function () {
            user_ids.push($(this).val());
        });

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['school/parentlist'], {
            'user_id': user_id,
            'user_ids': user_ids,
            'func': 'remove'
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.results) {
                $('#parent_list').html(response.results);
                if ($('#parent_phone').val() == '') {
                    $('#parent_phone').val(response.phone);
                }
                if ($('#parent_email').val() == '') {
                    $('#parent_email').val(response.email);
                }
                $('#create_parent_account').prop('checked', response.no_parent);
                $('#create_parent_account').prop('disabled', !response.no_parent);
                $('#parent_name').prop('disabled', !response.no_parent);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Xác nhận user là phụ huynh của trẻ
    $('body').on('click', '.js_class-approve-parent', function () {
        var parent_id = $(this).attr('data-parent');
        var child_id = $(this).attr('data-id');
        var username = $(this).attr('data-username');
        var button = $('#button_' + child_id + '_' + parent_id);

        button.addClass('x-hidden');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['class/child'], {
            'do': 'approve',
            'child_id': child_id,
            'parent_id': parent_id,
            'username': username
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Xử lý khi chọn ngày sinh của trẻ
    $('#birthdate_picker').datetimepicker({
        format: DATE_FORMAT
    });
    $('#beginat_datepicker').datetimepicker({
        format: DATE_FORMAT
    });

    /* hide the contro when clicked outside control */
    $('body').on('click', function (e) {
        if (!$(e.target).is("#search-parent")) {
            $('#search-parent-results').hide();
        }
    });
    //END - Màn hình: Thêm user có sẳn làm cha mẹ của một trẻ ----------------------------------------------------------

    //BEGIN - Hàm xử lý trong màn hình notification ------------------------------------------------------------------------
    //$('body').on('click', '.js_class-notification-notify', function () {
    //    var handle = $(this).attr('data-handle');
    //    var username = $(this).attr('data-username');
    //    var id = $(this).attr('data-id');
    //
    //    $.post(api['class/notification'], {'do': handle, 'username': username, 'id': id}, function(response) {
    //        if(response.callback) {
    //            eval(response.callback);
    //        } else {
    //            window.location.reload();
    //        }
    //    }, 'json')
    //    .fail(function() {
    //        modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
    //    });
    //});
    //
    //$('body').on('click', '.js_class-notification-delete', function () {
    //    var handle = $(this).attr('data-handle');
    //    var username = $(this).attr('data-username');
    //    var id = $(this).attr('data-id');
    //
    //    confirm(__['Delete'], __['Are you sure you want to delete this?'], function() {
    //        $.post(api['class/notification'], {'do': handle, 'username': username, 'id': id}, function(response) {
    //            if(response.callback) {
    //                eval(response.callback);
    //            } else {
    //                window.location.reload();
    //            }
    //        }, 'json')
    //        .fail(function() {
    //            modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
    //        });
    //    });
    //});
    //END - Hàm xử lý khi bấm Thông báo một notification ------------------------------------------------------------------------

    //BEGIN - Phần xử lý trong màn hình tạo mới/sửa sự kiện ------------------------------------------------------------------------
    $('#beginpicker').datetimepicker({
        format: DATETIME_FORMAT,
        sideBySide: true
    });

    $('#endpicker').datetimepicker({
        format: DATETIME_FORMAT,
        sideBySide: true
    });
    $("#beginpicker").on("dp.change", function (e) {
        $('#endpicker').data("DateTimePicker").minDate(e.date);
    });

    $('body').on('click', '.js_event-more-information', function () {
        var moreInfo = $('#event_more_information');
        var buttonDiv = $('#event_more_information_button');
        moreInfo.removeClass('x-hidden');
        buttonDiv.addClass('x-hidden');
    });
    //END - Màn hình: thêm mới, sửa thông tin event/school ------------------------------------------------------------------------

    //BEGIN - Màn hình: danh sách sự kiện ------------------------------------------------------------------------
    $('body').on('click', '.js_class-event-notify', function () {
        var username = $(this).attr('data-username');
        var id = $(this).attr('data-id');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['class/event'], {'do': 'notify', 'username': username, 'id': id}, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.callback) {
                eval(response.callback);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    $('#registration_deadlinepicker').datetimepicker({
        format: DATETIME_FORMAT,
        sideBySide: true
    });

    $('body').on('click', '.js_class-event-delete', function () {
        var username = $(this).attr('data-username');
        var id = $(this).attr('data-id');

        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['class/event'], {'do': 'delete', 'username': username, 'id': id}, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    //Xử lý khi click vào checkbox Phải đăng ký
    $('#must_register').click(function () {
        var for_child = $('#event_for_child');
        var for_parent = $('#event_for_parent');
        var event_registration_deadline = $('#event_registration_deadline');

        if (this.checked) {
            event_registration_deadline.removeClass('x-hidden');
            for_child.removeClass('x-hidden');
            for_parent.removeClass('x-hidden');
        } else {
            event_registration_deadline.addClass('x-hidden');
            for_child.addClass('x-hidden');
            for_parent.addClass('x-hidden');
        }
    });
    //END - Màn hình: danh sách sự kiện ------------------------------------------------------------------------

    //BEGIN - Màn hình danh sách người tham gia sự kiện -----------------------------
    $('#eventCheckAll').click(function () {
        if (this.checked) {
            $('.child').prop('checked', this.checked);
            $('.parent').prop('checked', this.checked);
        } else {
            $('.child').removeAttr('checked');
            $('.parent').removeAttr('checked');
        }
    });
    $('#eventCheckAllChildren').click(function () {
        if (this.checked) {
            $('.child').prop('checked', this.checked);
        } else {
            $('.child').removeAttr('checked');
        }
    });
    $('#eventCheckAllParent').click(function () {
        if (this.checked) {
            $('.parent').prop('checked', this.checked);
        } else {
            $('.parent').removeAttr('checked');
        }
    });

    $('body').on('click', '.js_class-event-remove-participant', function () {
        var username = $('#username').val();
        var pp_id = $(this).attr('data-uid');
        var event_id = $('#event_id').val();
        var event_name = $('#event_name').val();
        var type = $(this).attr('data-type');
        var name = $(this).attr('data-name');
        var child_id = $(this).attr('data-child');

        confirm(__['Cancel'], __['Are you sure you want to do this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['class/event'], {
                'do': 'reject',
                'username': username,
                'event_id': event_id,
                'event_name': event_name,
                'type': type,
                'name': name,
                'child_id': child_id,
                'pp_id': pp_id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                /* check the response */
                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });
    //END - Màn hình danh sách người tham gia sự kiện -----------------------------

    //Xử lý khi bấm hủy sự kiện
    $('body').on('click', '.js_class-event-cancel', function () {
        var username = $(this).attr('data-username');
        var event_id = $(this).attr('data-id');

        confirm(__['Cancel'], __['Are you sure you want to do this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['class/event'], {
                'do': 'cancel',
                'username': username,
                'event_id': event_id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });


    //BEGIN- SCHOOL - Màn hình gửi thuốc cho trẻ-----------------------------
    //Bắt sự kiện khi ghi nhận cho trẻ uống thuốc hoặc xác nhận giáo viên nhận được tin
    $('body').on('click', '.js_class-medicine', function () {
        var screen = $(this).attr('data-screen');
        var handle = $(this).attr('data-handle');
        var username = $(this).attr('data-username');
        var id = $(this).attr('data-id');
        var child_id = $(this).attr('data-child');
        var max = $(this).attr('data-max');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['class/medicine'], {
            'do': handle,
            'screen': screen,
            'username': username,
            'child_id': child_id,
            'id': id,
            'max': max
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.callback) {
                eval(response.callback);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Xóa một lần gửi thuốc
    $('body').on('click', '.js_class-medicine-delete', function () {
        var username = $(this).attr('data-username');
        var id = $(this).attr('data-id');
        var screen = $(this).attr('data-screen');

        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['class/medicine'], {
                'do': 'delete',
                'screen': screen,
                'username': username,
                'id': id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    //Hủy một lần gửi thuốc
    $('body').on('click', '.js_class-medicine-cancel', function () {
        var username = $(this).attr('data-username');
        var id = $(this).attr('data-id');
        var child_id = $(this).attr('data-child');
        var screen = $(this).attr('data-screen');

        confirm(__['Cancel'], __['Are you sure you want to cancel this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['class/medicine'], {
                'do': 'cancel',
                'child_id': child_id,
                'screen': screen,
                'username': username,
                'id': id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });
    $("#point_child_id_comment").change(function () {
        var username = $(this).attr('data-username');
        var child_id = $('#point_child_id_comment').val();
        // var class_level_id = $('#point_class_level_id').val();
        // var class_id = $('#point_class_id_comment').val();
        var school_year = $('#school_year').val();
        var is_last_semester = $('#is_last_semester').val();
        var semester = $('select[name="semester"]').val();

        var submit = $('#submit_id');
        submit.prop('disabled', true);

        // Show loading
        $("#loading_full_screen").removeClass('hidden');
        if (child_id == '') {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            $('#child_comment').html('');
        } else {
            submit.prop('disabled', false);
            $.post(api['class/point'], {
                'do': 'show_comment_child', // Sử dụng ở màn hình thêm nhận xét
                'username': username,
                // 'class_level_id': class_level_id,
                // 'class_id': class_id,
                'school_year': school_year,
                'is_last_semester': is_last_semester,
                'child_id': child_id,
                'semester': semester
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                if (response.results) {
                    $('#child_comment').html(response.results);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        }
    });
    //Bắt sự kiện khi click vào Detail
    $('body').on('click', '.js_class-medicine-detail', function () {
        var username = $(this).attr('data-username');
        var id = $(this).attr('data-id');
        var detail = $('#medicine-detail_' + id);
        var btnDetail = $('#button-detail_' + id);

        detail.removeClass('x-hidden');
        btnDetail.addClass('x-hidden');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['class/medicine'], {'do': 'detail', 'username': username, 'id': id}, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            if (response.results) {
                detail.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    $('#medicine_beginpicker_new').datetimepicker({
        format: DATE_FORMAT,
        defaultDate: new Date()
    });
    $('#medicine_endpicker_new').datetimepicker({
        format: DATE_FORMAT,
        defaultDate: new Date()
    });
    $("#medicine_beginpicker_new").on("dp.change", function (e) {
        $('#medicine_endpicker_new').data("DateTimePicker").minDate(e.date);
    });
    $('#medicine_beginpicker').datetimepicker({
        format: DATE_FORMAT
    });
    $('#medicine_endpicker').datetimepicker({
        format: DATE_FORMAT
    });
    $("#medicine_beginpicker").on("dp.change", function (e) {
        $('#medicine_endpicker').data("DateTimePicker").minDate(e.date);
    });
    //END - SCHOOL - Màn hình gửi thuốc cho trẻ-----------------------------

    //BEGIN - Màn hình dashboard -----------------------------------------------------------------------
    /*
    $('#medicine_on_dashboard').slimScroll({
        height: '350px',
        alwaysVisible: true
    });
    */
    //$('#notification_on_dashboard').slimScroll({
    //    height: '350px',
    //    alwaysVisible: true
    //});
    //$('#event_on_dashboard').slimScroll({
    //    height: '250px',
    //    alwaysVisible: true
    //});
    //END - Màn hình dashboard -----------------------------------------------------------------------

    //
    //BEGIN - Màn hình điểm danh của lớp ----------------------------------------------------
    $('#attendance_picker').datetimepicker({
        //format: DATE_FORMAT
        format: DATE_FORMAT,
        maxDate: new Date()
    });

    $("#attendance_picker").on("dp.change", function (e) {
        var username = $('#username').val();
        var attendance_date = $('#attendance_date').val();
        var attendance_list = $('#attendance_list');

        attendance_list.html('');
        $('button[type="submit"]').prop('disabled', true);

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['class/attendance'], {
            'do': 'list',
            'username': username,
            'attendance_date': attendance_date
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            if (response.results) {
                attendance_list.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
            $('button[type="submit"]').prop('disabled', response.disableSave);
            $("#attendanceCheckAll").click(function () {
                if (this.checked) {
                    $('.child').prop('checked', this.checked);
                } else {
                    $('.child').removeAttr('checked');
                }
            });
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    $("#attendanceCheckAll").click(function () {
        if (this.checked) {
            $('.child').prop('checked', this.checked);
        } else {
            $('.child').removeAttr('checked');
        }
    });

    $('#fromdate_picker').datetimepicker({
        format: DATE_FORMAT
    });

    $('#todate_picker').datetimepicker({
        format: DATE_FORMAT
    });

    $("#fromdate_picker").on("dp.change", function (e) {
        $('#todate_picker').data("DateTimePicker").minDate(e.date);
    });

    $('body').on('click', '.js_class-attendancesearch', function () {
        var username = $(this).attr('data-username');
        var fromDate = $('#fromDate').val();
        var toDate = $('#toDate').val();
        var attendance_list = $('#attendance_list');

        $('#search').addClass('x-hidden');
        $('#loading').removeClass('x-hidden');

        attendance_list.html('');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['class/attendance'], {
            'do': 'search',
            'username': username,
            'fromDate': fromDate,
            'toDate': toDate
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            $('#search').removeClass('x-hidden');
            $('#loading').addClass('x-hidden');
            if (response.results) {
                attendance_list.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $('#search').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    $('body').on('click', '.js_class-attendancechild', function () {
        var username = $(this).attr('data-username');
        var child_id = $(this).attr('data-id');
        var fromDate = $('#fromDate').val();
        var toDate = $('#toDate').val();
        var attendance_list = $('#attendance_list');
        $(".js_class-attendancechild").addClass("x-hidden");
        $(".processing_label").removeClass("x-hidden");

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['class/attendance'], {
            'do': 'child',
            'username': username,
            'child_id': child_id,
            'fromDate': fromDate,
            'toDate': toDate
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            //UPDATE START MANHDD 11/06/2021
            // $('button[type="submit"]').prop('disabled', response.no_data);
            if(response.no_data) {
                $('button[type="submit"]').prop('disabled',true);
            }

            //UPDATE END MANHDD 11/06/2021
            $(".js_class-attendancechild").removeClass("x-hidden");
            $(".processing_label").addClass("x-hidden");
            if (response.results) {
                attendance_list.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $(".js_class-attendancechild").removeClass("x-hidden");
                $(".processing_label").addClass("x-hidden");
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    $('body').on('click', '.js_class-confirm-attendance', function () {
        var username = $(this).attr('data-username');
        var detail_id = $(this).attr('data-detail-id');
        var child_id = $(this).attr('data-id');
        var button = $('#button_' + detail_id);

        // đổi value của feedback_childid = 1
        $('#feedback_' + child_id).val('1');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        button.addClass('x-hidden');
        $.post(api['class/attendance'], {
            'do': 'confirm',
            'attendance_detail_id': detail_id,
            'child_id': child_id,
            'username': username
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });
    //END - Màn hình điểm danh của lớp ----------------------------------------------------

    //BEGIN - Màn hình dịch vụ ----------------------------------------------------
    $('#using_time_picker').datetimepicker({
        format: DATE_FORMAT,
        defaultDate: new Date()
    });

    $('#history_begin_picker').datetimepicker({
        format: DATE_FORMAT
    });

    $('#history_end_picker').datetimepicker({
        format: DATE_FORMAT,
        defaultDate: new Date()
    });

    //Liệt kê danh sách trẻ để điểm danh sử dụng dịch vụ
    $('body').on('click', '.js_service-search-child4record', function () {
        var username = $(this).attr('data-username');
        var using_at = $('#using_at').val();
        var service_id = $('#service_id option:selected').val();
        var error = $('.alert.alert-danger');
        var success = $('.alert.alert-success');

        $('#search').addClass('x-hidden');
        $('#loading').removeClass('x-hidden');
        $('#child_list').html('');
        $('button[type="submit"]').prop('disabled', true);

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        if (success.is(":visible")) success.hide();
        if (error.is(":visible")) error.hide();
        $.post(api['class/service'], {
            'service_id': service_id,
            'using_at': using_at,
            'username': username,
            'do': 'child_list4record'
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            if (response.results) {
                $('#child_list').html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
            $('#search').removeClass('x-hidden');
            $('#loading').addClass('x-hidden');
            $('button[type="submit"]').prop('disabled', response.disableSave);
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $('#search').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Tìm kiếm lịch sử sử dụng dịch vụ của 1 trẻ.
    $('body').on('click', '.js_service-search-history', function () {
        var username = $(this).attr('data-username');
        var begin = $('#begin').val();
        var end = $('#end').val();
        var child_id = $('#service_child_id option:selected').val();
        var service_id = $('#service_id option:selected').val();

        $('#search').addClass('x-hidden');
        $('#loading').removeClass('x-hidden');
        $('#usage_history').html('');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['class/service'], {
            'service_id': service_id,
            'child_id': child_id,
            'begin': begin,
            'end': end,
            'username': username,
            'do': 'history'
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            $('#search').removeClass('x-hidden');
            $('#loading').addClass('x-hidden');
            if (response.results) {
                $('#usage_history').html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                $('#search').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });
    //END - Màn hình dịch vụ ----------------------------------------------------

    // bắt sự kiện khi tạo đơn thuốc
    $(document).on('submit', '#create_medicine', function (e) {
        var _this = $(this);
        var submit = _this.find('button[type="submit"]');

        /* show loading */
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading']);

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        e.preventDefault();
        $.ajax({
            url: api['class/medicine'],
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false
        }).done(function (data) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            /* hide loading */
            submit.prop('disabled', false);
            submit.html(submit.data('text'));

            if (data.success) {
                modal('#modal-success', {title: __['Success'], message: data.message});
                modal_hidden(timeModal);
                $("#medicine_list_clear").val("");
                $("#guide_clear").val("");
            } else if (data.error) {
                modal('#modal-error', {title: __['Error'], message: data.message});
            } else if (data.callback) {
                eval(data.callback);
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                /* hide loading */
                submit.prop('disabled', false);
                submit.html(submit.data('text'));
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //BEGIN - Màn hình Đóng góp ý kiến
    $('body').on('click', '.js_class-feedback', function () {
        var handle = $(this).attr('data-handle');
        var username = $(this).attr('data-username');
        var feedback_id = $(this).attr('data-id');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['class/feedback'], {
            'do': 'confirm',
            'username': username,
            'feedback_id': feedback_id
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.callback) {
                eval(response.callback);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: data.message});
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });
    //END

    //BEGIN - Màn hình thông tin người đón thay
    $('body').on('click', '.js_paid_child', function () {
        var username = $(this).attr('data-username');
        var child_id = $(this).attr('data-id');

        confirm(__['Actions'], __['Are you sure you want to do this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['class/child'], {
                'do': 'confirm',
                'username': username,
                'child_id': child_id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: data.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    // Delete hàng khi tạo mới report template
    $('body').on("click", 'a.js_report_template-delete', function () {
        var $this = $(this);
        var idx = 1;
        $this.parents('tr').remove();
        $(".col_no").each(function () {
            $(this).html(idx);
            idx = idx + 1;
        });
        //return false;
    });

    // Xử lý khi thêm hàng ở màn hình thêm mới report template
    $('body').on('click', '.js_report_template-add', function () {
        var index = $('#addTempTable tbody tr').length + 1;
        $('#addTempTable').append('<tr> <td align="center" class = "col_no align-middle"> ' + index + ' </td> <td class="align-middle"> <input type="text" name = "cate[]" required class = "form-control"> </td> <td class="align-middle"> <textarea type="text" name = "content[]" required class = "form-control" style="height: 35px;overflow: hidden;resize: vertical;"></textarea> </td> <td class="align-middle" align = "center"> <a class="btn btn-xs btn-danger js_report_template-delete"> ' + __["Delete"] + ' </a></td> </tr>');
    });

    // Xử lý khi chợn mẫu khi tạo sổ liên lạc
    $("#report_template_id").change(function () {
        var username = $(this).attr('data-username');
        // var view = $(this).attr('data-view');
        var template_id = this.value;

        if (template_id == '') {
            $('#template_detail').html('');
            if ($('#template_detail').hasClass('hidden')) {
                $('#template_detail').removeClass('hidden');
            }
            if ($('#notemplate').hasClass('hidden')) {
                $('#notemplate').removeClass('hidden');
            }
        } else {
            if (!($('#notemplate').hasClass('hidden'))) {
                $('#notemplate').addClass('hidden');
            }
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['class/report'], {
                'do': 'template_detail',
                'username': username,
                'template_id': template_id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                if (response.results) {
                    $('#template_detail').html(response.results);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: data.message});
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        }
    });

    //Bắt sự kiện khi tạo báo cáo
    $(document).on('submit', '#class_create_report', function (e) {
        var _this = $(this);
        var error = $('.alert.alert-danger');
        var success = $('.alert.alert-success');
        var submit = _this.find('button[type="submit"]');

        /* show loading */
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading']);

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        e.preventDefault();
        $.ajax({
            url: api['class/report'],
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false
        }).done(function (data) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            /* hide loading */
            submit.prop('disabled', false);
            submit.html(submit.data('text'));
            if (data.success) {
                modal('#modal-success', {title: __['Success'], message: data.message});
                modal_hidden(timeModal);
                $('#template_detail').html('');
                $("#report_template_id option[value='']").prop('selected', true);
                $('.checkbox_child').each(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).prop('disabled', true);
                    }
                });
            } else if (data.error) {
                modal('#modal-error', {title: __['Error'], message: data.message});
            } else if (data.callback) {
                eval(data.callback);
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                /* hide loading */
                submit.prop('disabled', false);
                submit.html(submit.data('text'));
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });
    //END

    //BEGIN - Màn hình đón muộn
    $("#pickup_teacher_class").change(function () {
        var username = $('#username').val();
        var class_id = this.value;
        var pickup_class_id = $('#pickup_class_id').val();
        var pickup_id = $('#pickup_id').val();
        var add_to_date = $('#add_to_date').val();

        if (class_id != '') {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['class/pickup'], {
                'do': 'list_child',
                'username': username,
                'class_id': class_id,
                'pickup_id': pickup_id,
                'pickup_class_id': pickup_class_id,
                'add_to_date': add_to_date
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.results) {
                    $('#pickup_class_list').html(response.results);
                    $('button[type="submit"]').prop('disabled', response.disableSave);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        }
    });

    $('.pickup_time').datetimepicker({
        format: TIME_FORMAT,
        sideBySide: true
    });

    $('body').on('focus', '.pickup_time', function () {
        var timeOption = $('#time_option').attr('data-value');
        var currentTIme = $(this).val();
        if ($(this).val() == '') {
            $(this).val(timeOption);
        }
        /*var pid = $(this).attr('id');
        if ($.inArray(pid, arrPickupClassDP) == -1) {
            $('#' + pid).on("dp.change", function (e) {
                var pickup_time = $(this).val();
                var pickup_id = $('#pickup_id').val();
                var child_id = pid.replace("pickup_time_", "");
                var username = $('#username').val();
                var pickup_date = $('#pickup_date').val();

                if (pickup_time == '') {
                    $('#total_pickup_' + child_id).val(0);

                    var total_child = $('#total_' + child_id);

                    //Tổng tiền trông muộn của trẻ = tiền sử dụng dịch vụ
                    var total_service = $('#total_service_' + child_id);
                    var total_service_value = total_service.val();
                    total_child.val(total_service_value);

                    //Tính tiền đón muộn của cả lớp
                    var total = 0;
                    $('input[name*=total_child]').each(function () {
                        var total_child_value = $(this).val();
                        total_child_value = total_child_value.replace(/,/g, ''); // BỎ dâu phẩy trong giá trị lấy được
                        total_child_value = !$.isNumeric(total_child_value) ? 0 : parseInt(total_child_value);

                        total = total + total_child_value;
                    });
                    total = total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                    $('#total').val(total);
                } else {
                    // Show loading
                    $("#loading_full_screen").removeClass('hidden');

                    $.post(api['class/pickup'], {
                        'do': 'get_pickup_fee',
                        'username': username,
                        'child_id': child_id,
                        'pickup_id': pickup_id,
                        'pickup_date': pickup_date,
                        'pickup_time': pickup_time
                    }, function (response) {
                        // Hidden loading
                        $("#loading_full_screen").addClass('hidden');

                        if (response.callback) {
                            eval(response.callback);
                            $('#' + pid).val(currentTIme);
                        } else if (response.error) {
                            modal('#modal-error', {title: __['Error'], message: response.message});
                        } else {
                            // Hiển thị tổng tiền đón muộn khi thay đổi thời gian
                            var pickup_fee = parseInt(response.results);
                            var total_pickup_child = pickup_fee.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                            $('#total_pickup_' + child_id).val(total_pickup_child);

                            //Tiền sử dụng dịch vụ
                            var total_service = $('#total_service_' + child_id);
                            var total_service_value = total_service.val();
                            total_service_value = total_service_value.replace(/,/g, ''); // BỎ dâu phẩy trong giá trị lấy được
                            total_service_value = !$.isNumeric(total_service_value) ? 0 : parseInt(total_service_value);

                            //Tổng phí của 1 trẻ
                            var total_child = $('#total_' + child_id);
                            var total_child_value = (pickup_fee + total_service_value).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                            total_child.val(total_child_value);

                            //Tổng tiền của cả lớp
                            var total = 0;
                            $('input[name*=total_child]').each(function () {
                                var total_child_value = $(this).val();
                                total_child_value = total_child_value.replace(/,/g, ''); // BỎ dâu phẩy trong giá trị lấy được
                                total_child_value = !$.isNumeric(total_child_value) ? 0 : parseInt(total_child_value);

                                total = total + total_child_value;
                            });
                            total = total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                            $('#total').val(total);
                        }
                    }, 'json')
                        .fail(function () {
                            // Hidden loading
                            $("#loading_full_screen").addClass('hidden');

                            modal('#modal-message', {
                                title: __['Error'],
                                message: __['There is something that went wrong!']
                            });
                        });
                }

            });
            arrPickupSchoolDP.push(pid);
        }*/

    });

    //Xử lý khi chọn class ở màn hình thêm trẻ
    $('body').on('change', '#pickup_class', function () {
        var username = $('#username').val();
        var class_id = this.value;
        var pickup_class_id = $('#pickup_class_id').val();
        var pickup_id = $('#pickup_id').val();
        var add_to_date = $('#add_to_date').val();

        if (class_id != '') {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['class/pickup'], {
                'do': 'list_child',
                'username': username,
                'class_id': class_id,
                'pickup_id': pickup_id,
                'pickup_class_id': pickup_class_id,
                'add_to_date': add_to_date
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.results) {
                    $('#pickup_class_list').html(response.results);
                    $('button[type="submit"]').prop('disabled', response.disableSave);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        }
    });

    $(document).on('click', '#childCheckAll', function () {
        if (this.checked) {
            $('.child').prop('checked', this.checked);
        } else {
            $('.child').removeAttr('checked');
        }
    });

    //Bắt sự kiện thay sử dụng dịch vụ màn hình đón muộn
    /*$('body').on('click', '.pickup_service_fee', function () {
        var child_id = $(this).attr('data-child');
        var service_id = $(this).attr('data-service');
        //var total_pickup = $('#total_pickup_' + child_id);
        var total_service = $('#total_service_' + child_id);
        //var total_child = $('#total_' + child_id);
        //var total = $('#total');

        var service_fee = parseInt($('#pickup_service_fee_' + child_id + '_' + service_id).attr('data-fee'));
        var service_amount_prev = total_service.val();
        service_amount_prev = service_amount_prev.replace(/,/g, ''); // BỎ dâu phẩy trong giá trị lấy được
        service_amount_prev = !$.isNumeric(service_amount_prev) ? 0 : parseInt(service_amount_prev);

        var new_service_value = 0;
        if (this.checked) {
            //Tính thành tiền của mỗi dịch vụ
            new_service_value = service_amount_prev + service_fee;
            // var new_service_str = new_service_value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            total_service.val(new_service_value);
        } else {
            //Tính thành tiền của mỗi dịch vụ
            new_service_value = service_amount_prev - service_fee;
            if (new_service_value > 0) {
                /!*var new_service_str = new_service_value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");*!/
                total_service.val(new_service_value);
            } else {
                total_service.val(0);
            }
        }

        /!*var pickup_fee = total_pickup.val();
        pickup_fee = pickup_fee.replace(/,/g, ''); // BỎ dâu phẩy trong giá trị lấy được
        pickup_fee = !$.isNumeric(pickup_fee) ? 0 : parseInt(pickup_fee);

        //Tổng phí của 1 trẻ
        var total_child_value = (pickup_fee + new_service_value).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        total_child.val(total_child_value);

        //Tổng tiền của cả lớp
        var total = 0;
        $('input[name*=total_child]').each(function () {
            var total_child_value = $(this).val();
            total_child_value = total_child_value.replace(/,/g, ''); // BỎ dâu phẩy trong giá trị lấy được
            total_child_value = !$.isNumeric(total_child_value) ? 0 : parseInt(total_child_value);

            total = total + total_child_value;
        });
        total = total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        $('#total').val(total);*!/
    });*/


    $('#fromdatepicker').datetimepicker({
        format: DATE_FORMAT,
        defaultDate: new Date()
    });

    $('#todatepicker').datetimepicker({
        format: DATE_FORMAT,
        defaultDate: new Date()
    });

    $('body').on('click', '.js_pickup-search', function () {
        var username = $(this).attr('data-username');
        var fromDate = $('#fromDate').val();
        var toDate = $('#toDate').val();
        var pickup_list = $('#pickup_list');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['class/pickup'], {
            'do': 'search_pickup',
            'username': username,
            'fromDate': fromDate,
            'toDate': toDate
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.callback) {
                eval(response.callback);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else if (response.results) {
                pickup_list.html(response.results);
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });


    $('body').on('click', '.js_class-pickup', function () {
        var handle = $(this).attr('data-handle');
        var username = $(this).attr('data-username');
        var child_id = $(this).attr('data-child');
        var pickup_id = $(this).attr('data-pickup');
        var callback = $('#callback').val();

        confirm(__['Actions'], __['Are you sure you want to do this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['class/pickup'], {
                'do': handle,
                'username': username,
                'child_id': child_id,
                'pickup_id': pickup_id,
                'callback': callback
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });


    $('#begin_assign').datetimepicker({
        format: DATE_FORMAT,
        defaultDate: new Date()
    });

    //Bắt sự kiện khi thông tin tháng thay đổi
    $('#begin_assign').on("dp.change", function (e) {
        var username = $('#username').val();
        var begin = $('#begin').val();

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['class/pickup'], {
            'do': 'get_class_assign',
            'begin': begin,
            'username': username
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.results) {
                $('#tbody-assign').html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //END - Màn hình đón muộn


    //BEGIN - Hàm xử lý trong màn hình notification ------------------------------------------------------------------------
    $('body').on('click', '.js_class-report-notify', function () {
        var handle = $(this).attr('data-handle');
        var username = $(this).attr('data-username');
        var id = $(this).attr('data-id');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['class/report'], {'do': handle, 'username': username, 'id': id}, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            /* check the response */
            if (response.callback) {
                eval(response.callback);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });
    //END - Hàm xử lý khi bấm Thông báo một notification ------------------------------------------------------------------------

    //Xử lý khi xóa: report, template
    $('body').on('click', '.js_class-delete', function () {
        var handle = $(this).attr('data-handle');
        var username = $(this).attr('data-username');
        var id = $(this).attr('data-id');

        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['class/report'], {
                'do': handle,
                'username': username,
                'id': id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });
    //Xử lý khi chọn trẻ ở màn hình danh sách sổ liên lạc
    $("#report_child_search").change(function () {
        var child_id = this.value;
        var username = $(this).attr('data-username');
        var handle = $(this).attr('data-handle');
        var report_list_child = $('#report_list_child');
        report_list_child.html('');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        // if (child_id != '') {
        $.post(api['class/report'], {
            'do': handle,
            'username': username,
            'child_id': child_id
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.results) {
                report_list_child.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
            // $('#all_report_class').addClass('x-hidden');
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
        /*} else {
            $('#report_list_child').addClass('x-hidden');
            // $('#all_report_class').removeClass('x-hidden');
        }*/
    });

    $(".delete_image").click(function () {
        $("input[name*='file']").val("");
        $("#file_old").remove();
        $("#is_file").val("0");
    });

    //Xóa dữ liệu trên màn hình tạo trẻ
    $('body').on('click', '.js_add_child_clear', function () {
        $("input[name*='child_code']").val("");
        $("input[name*='last_name']").val("");
        $("input[name*='first_name']").val("");
        $("input[name*='birthday']").val("");
        $("input[name*='search-parent']").val("");
        $('#parent_list').html("");
        $("input[name*='parent_name']").val("");
        $("input[name*='parent_phone']").val("");
        $("input[name*='parent_job']").val("");
        $("input[name*='parent_name_dad']").val("");
        $("input[name*='parent_phone_dad']").val("");
        $("input[name*='parent_job_dad']").val("");
        $("input[name*='parent_email']").val("");
        $('#create_parent_account').prop('checked', true);
        $('#create_parent_account').prop('disabled', false);
        $("input[name*='address']").val("");
        $("input[name*='begin_at']").val("");
        $("#description").val("");
    });

    //Xử lý khi bấm thêm mới trẻ vào trường
    $('body').on('submit', '.js_ajax-add-child-form', function (e) {
        e.preventDefault();
        var _this = $(this);
        var url = _this.attr('data-url');
        var submit = _this.find('button[type="submit"]');
        /* show any collapsed section if any */
        if (_this.find('.js_hidden-section').length > 0 && !_this.find('.js_hidden-section').is(':visible')) {
            _this.find('.js_hidden-section').slideDown();
            return false;
        }
        /* show loading */
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading']);

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        /* get ajax response */
        $.post(ajax_path + url, $(this).serialize(), function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            /* hide loading */
            submit.prop('disabled', false);
            submit.html(submit.data('text'));
            /* handle response */
            if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else if (response.success) {
                $('.js_ajax-add-child-form').trigger("reset");
                $('#create_parent_account').prop('disabled', false);
                $('#code_auto').prop('disabled', false);
                $('#child_code').prop('disabled', true);
                $('#parent_list').html("");
                modal('#modal-success', {title: __['Success'], message: response.message});
                modal_hidden(timeModal);
            } else {
                eval(response.callback);
            }
        }, "json")
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                /* hide loading */
                submit.prop('disabled', false);
                submit.html(submit.data('text'));
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Bắt sự kiện khi upload một file Excel danh sách trẻ lên
    $(document).on('submit', '#import_excel_form', function (e) {
        e.preventDefault();

        var submit = $('#submit_id');
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading...']);
        $('#result_info').html('');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.ajax({
            url: api['class/child'],
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false
        }).done(function (data) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            submit.prop('disabled', false);
            submit.html(submit.data('text'));
            if (data.results) {
                $('#result_info').html(data.results);
            } else if (data.error) {
                modal('#modal-error', {title: __['Error'], message: data.message});
            }
        }, 'json').fail(function () {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            submit.prop('disabled', false);
            submit.html(submit.data('text'));
            modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
        });
    });

    // Xử lý khi xóa trẻ chờ được sửa
    $('body').on('click', '.js_class-child-edit', function () {
        var username = $(this).attr('data-username');
        var handle = $(this).attr('data-handle');
        var child_id = $(this).attr('data-id');

        confirm(__['Actions'], __['Are you sure you want to do this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['class/child'], {
                'do': handle,
                'username': username,
                'child_id': child_id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    // bắt sự kiện khi sửa thông tin sức khỏe của trẻ
    $(document).on('submit', '#class_edit_child_health', function (e) {
        var _this = $(this);
        var submit = _this.find('button[type="submit"]');

        /* show loading */
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading']);

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        e.preventDefault();
        $.ajax({
            url: api['class/child'],
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false
        }).done(function (data) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            /* hide loading */
            submit.prop('disabled', false);
            submit.html(submit.data('text'));
            if (data.success) {
                modal('#modal-success', {title: __['Success'], message: data.message});
                modal_hidden(timeModal);
            } else if (data.error) {
                modal('#modal-error', {title: __['Error'], message: data.message});
            } else if (data.callback) {
                eval(data.callback);
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                /* hide loading */
                submit.prop('disabled', false);
                submit.html(submit.data('text'));
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });
// ADD START MANHDD 03/06/2021
    // bắt sự kiện khi sửa thông tin hạnh kiểm của trẻ
    $(document).on('submit', '#class_edit_conduct', function (e) {
        var _this = $(this);
        var submit = _this.find('button[type="submit"]');

        /* show loading */
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading']);

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        e.preventDefault();
        $.ajax({
            url: api['class/conduct'],
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false
        }).done(function (data) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            /* hide loading */
            submit.prop('disabled', false);
            submit.html(submit.data('text'));
            if (data.success) {
                modal('#modal-success', {title: __['Success'], message: data.message});
                modal_hidden(timeModal);
            } else if (data.error) {
                modal('#modal-error', {title: __['Error'], message: data.message});
            } else if (data.callback) {
                eval(data.callback);
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                /* hide loading */
                submit.prop('disabled', false);
                submit.html(submit.data('text'));
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });
    // ADD END MANHDD 03/06/2021

    // Xử lý chọn tất cả hạng mục ở màn hình thêm và sửa mẫu sổ liên lạc
    $('body').on('click', '#select_all', function () {
        var c = this.checked;
        $(':checkbox').prop('checked', c);
    });

    // Xử lý chọn tất cả hạng mục ở màn hình thêm và sửa mẫu sổ liên lạc
    $('body').on('click', '#select_all_child', function () {
        var c = this.checked;
        $('.checkbox_child').prop('checked', c);
    });

    //Hiển thị popup để xem chi tiết hạng mục trong màn hình thêm mới và edit mẫu bên tài khoản giáo viên
    $('body').on('click', '.js_class-category-detail', function () {
        var report_template_category_id = $(this).attr('data-id');
        var username = $('#username').val();

        $('#open_dialog').html("");

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['class/report'], {
            'do': 'cate_detail',
            'username': username,
            'report_template_category_id': report_template_category_id
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.results) {
                $('#open_dialog').html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }

            $("#open_dialog").dialog({
                modal: false,
                height: 300,
                width: 400,
                closeOnEscape: true,
                resizable: true,
                autoOpen: true,
                // closeText: __['Close']
                closeText: ""
            });
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Xử lý khi nhấn thông báo tất cả
    $('body').on('click', '.js_class-report-notify-all', function () {
        var handle = $(this).attr('data-handle');
        var username = $(this).attr('data-username');

        confirm(__['Actions'], __['Are you sure you want to do this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['class/report'], {
                'do': handle,
                'username': username
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });
    $('.note').css('overflow', 'hidden').autogrow();

    function h(e) {
        $(e).css({'height': '35px', 'overflow-y': 'hidden', 'overflow-x': 'hidden'}).height(e.scrollHeight);
    }

    $('.note').each(function () {
        h(this);
    }).on('input', function () {
        h(this);
    });

    // Xử lý kích vào tên trẻ ở màn hình thêm chỉ số sức khỏe của trẻ
    $('body').on('click', '.js_class-child-health', function () {
        var child_id = $(this).attr('data-id');

        $('.hideAll').addClass('hidden');
        $('#health_' + child_id).removeClass('hidden');
        $('.js_class-child-health').css({'color': '#4083a9', 'font-weight': 'normal'});

        $('#child_' + child_id).css({'color': 'red', 'font-weight': 'bold'});
        $('.submit_hidden').removeClass('hidden');
        // $("[id[name*='user_id']").each(function() {
        //     user_ids.push($(this).val());
        // });
        // moreInfo.removeClass('x-hidden');
        // buttonDiv.addClass('x-hidden');
    });

    // bắt sự kiện khi thêm thông tin sức khỏe của trẻ (cả lớp)
    $(document).on('submit', '#class_add_health_index', function (e) {
        var _this = $(this);
        var error = $('.alert.alert-danger');
        var success = $('.alert.alert-success');
        var submit = _this.find('button[type="submit"]');

        /* show loading */
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading']);

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        e.preventDefault();
        $.ajax({
            url: api['class/child'],
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false
        }).done(function (data) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            /* hide loading */
            submit.prop('disabled', false);
            submit.html(submit.data('text'));
            if (data.success) {
                modal('#modal-success', {title: __['Success'], message: data.message});
                modal_hidden(timeModal);
            } else if (data.error) {
                modal('#modal-error', {title: __['Error'], message: data.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                /* hide loading */
                submit.prop('disabled', false);
                submit.html(submit.data('text'));
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Xử lý khi xoa thông tin sức khỏe của trẻ
    $('body').on('click', '.js_class-health-delete', function () {
        var id = $(this).attr('data-id');
        var child_id = $(this).attr('data-child');
        var handle = $(this).attr('data-handle');
        var username = $(this).attr('data-username');
        var module = $(this).attr('data-module');

        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['class/child'], {
                'do': handle,
                'child_id': child_id,
                'id': id,
                'username': username,
                'module': module
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    // khi chọn năm học thì gọi lại select học kỳ trong màn hình lists by subject
    $("#school_year").change(function () {
        // var semester = this.value;
        $('.action-point').hide();
        var search_with = $("input[name='do']").val();
        if (search_with == 'search_with_subject' || search_with == 'importManual') {
            $("#semester").trigger("change");
        }else if(search_with=='comment') {
            $("#point_child_id_comment").trigger("change");
        }
    });
    //ADD START MANHDD 24/06/2021
    // Sử dụng trong chức năng nhập điểm ( mỗi khi nhấn thêm cột mới sẽ resize lại table )
    function resize_table() {
        $table_front = $('.table-pinned').eq(0);
        $table_back = $('.table-pinned').eq(1);
        for(j = 0;j<$table_back.find('th').length;j++) {
            if($table_back.find('th').eq(j).hasClass("pinned")) {
                $table_front.find('th').eq(j).css("width",$table_back.find('th').eq(j)[0].getBoundingClientRect().width);
                $table_front.find('th').eq(j).css("height",$table_back.find('th').eq(j)[0].getBoundingClientRect().height);
            }
        }
        for(j = 0;j<$table_back.find('tr').length;j++) {
            if($table_back.find('tr').eq(j).find('td').length > 0) {
                for(k = 0;k<$table_back.find('tr').eq(j).find('td').length;k++) {
                    if($table_back.find('tr').eq(j).find('td').eq(k).hasClass("pinned")) {
                        $table_front.find('tr').eq(j).find('td').eq(k).css("width",$table_back.find('tr').eq(j).find('td').eq(k)[0].getBoundingClientRect().width);
                        $table_front.find('tr').eq(j).find('td').eq(k).css("height",$table_back.find('tr').eq(j).find('td').eq(k)[0].getBoundingClientRect().height);

                    }
                }
            }
        }
    }
    //ADD END MANHDD 24/06/2021
    //ADD START MANHDD 10/06/2021
    // Thêm cột tx khi nhấn Add Freq-point ở màn hình import điểm bẳng tay ( học kỳ 1 or 2 )
    $(document).on('click', '.js_point_tx-add', function (e) {
        e.preventDefault();
        var semester = $('#semester').val();
        var score_fomula = $("input[name='score_fomula']").val();
        var flag_added = false;
        var count_tx = 0;
        var index_insert_td = 0;
        var table = $("table tbody").eq(1); // màn hình bị chia thành 2 tbody, tbody 1 chứa những cột pined, tbody2 là cả table mình cần
        if (semester != '0') {

            table.find('tr').each(function (index, el) {
                var $tds = $(this).find('td');
                var th_length = $('tr:nth-child(2) th').length; // lấy số lượng cột điểm ( bị duplicate nên sẽ chia 2 để lấy tên cột cho chính xác )
                if (score_fomula == 'vn') {
                    for (i = Math.floor(th_length/2); i <= th_length; i++) {
                        if ($('tr:nth-child(2) th').eq(i).text() == 'tx') {
                            count_tx++;
                        } else if ($('tr:nth-child(2) th').eq(i).text() == 'gk') {

                            if (!flag_added) {
                                // vì table đang là 2 bảng chồng lên nhau nên phải thêm vào cả 2 để tăng length th_length cho đều
                                // thêm vào mặt trước
                                $('tr:nth-child(2) th').eq((i - 1)).after('<th>tx</th>');
                                //thêm vào mặt sau
                                $('tr:nth-child(2) th').eq((i - 1)-Math.floor(th_length/2)).after('<th style=\"display:none;\">tx</th>');
                                // tăng độ dài cột semester
                                var cols =  $('tr:nth-child(1) th').eq(7).attr("colspan");
                                $('tr:nth-child(1) th').eq(7).attr("colspan",parseInt(cols) + 1);
                                flag_added = true;
                                index_insert_td = i - Math.floor(th_length/2)- 1 + 3;
                                // count_tx=0;
                            }
                            if(count_tx == 6) {
                                count_tx =0;
                                $('.js_point_tx-add').prop('disabled', true);
                            }
                            $tds.eq(index_insert_td).after('<td><input name=\"point\" type=\"number\" min=\"0\" step=\"0.01\" style=\"max-width: 50px\"\n value=\"\"/></td>');
                            count_tx =0;
                            break;
                        }
                    }
                }
            });
            resize_table();
        }
    });
    // Thêm cột gk khi nhấn Add Mid-Point ở màn hình import điểm bẳng tay ( học kỳ 1 or 2 )
    $(document).on('click', '.js_point_gk-add', function (e) {
        e.preventDefault();
        var semester = $('#semester').val();
        var score_fomula = $("input[name='score_fomula']").val();
        var flag_added = false;
        var count_gk = 0;
        var index_insert_td = 0;
        var table = $("table tbody").eq(1); // màn hình bị chia thành 2 tbody, tbody 1 chứa những cột pined, tbody2 là cả table mình cần
        if (semester != '0') {
            table.find('tr').each(function (index, el) {
                var $tds = $(this).find('td');
                var th_length = $('tr:nth-child(2) th').length; // lấy số lượng cột điểm ( bị duplicate nên sẽ chia 2 để lấy tên cột cho chính xác )
                if (score_fomula == 'vn') {
                    for (i = Math.floor(th_length/2); i <= th_length; i++) {
                        if ($('tr:nth-child(2) th').eq(i).text() == 'gk') {
                            count_gk++;
                        } else if ($('tr:nth-child(2) th').eq(i).text() == 'ck') {
                            if (!flag_added) {
                                // vì table đang là 2 bảng chồng lên nhau nên phải thêm vào cả 2 để tăng length th_length cho đều
                                // thêm vào mặt trước
                                $('tr:nth-child(2) th').eq(i - 1).after('<th>gk</th>');
                                //thêm vào mặt sau
                                $('tr:nth-child(2) th').eq((i - 1)-Math.floor(th_length/2)).after('<th style=\"display:none;\">gk</th>');
                                // tăng độ dài cột semester
                                var cols =  $('tr:nth-child(1) th').eq(7).attr("colspan");
                                $('tr:nth-child(1) th').eq(7).attr("colspan",parseInt(cols) + 1);
                                flag_added = true;
                                index_insert_td = i - Math.floor(th_length/2)- 1 + 3;
                            }
                            if(count_gk == 2) {
                                count_gk =0;
                                $('.js_point_gk-add').prop('disabled', true);
                            }
                            $tds.eq(index_insert_td).after('<td><input name=\"point\" type=\"number\" min=\"0\" step=\"0.01\" style=\"max-width: 50px\"\n value=\"\"/></td>');
                            count_gk =0;
                            break;
                        }
                    }
                }
            });
            //ADD START MANHDD 19/06/2021 => fix lỗi khi add thêm col trong phần nhập điểm table (không pined ) thay đổi width mà table (pined) vẫn lấy giá trị width cũ
            resize_table();
            //ADD END MANHDD 19/06/2021
        }
    });
    // Thêm cột tx khi nhấn Add Freq-point1 ở màn hình import điểm bẳng tay ( cả năm )
    $(document).on('click', '.js_point_tx1-add', function (e) {
        e.preventDefault();
        var semester = $('#semester').val();
        var score_fomula = $("input[name='score_fomula']").val();
        var flag_added = false;
        var count_tx = 1;
        var index_insert_td = 0;
        var table = $("table tbody").eq(1); // màn hình bị chia thành 2 tbody, tbody 1 chứa những cột pined, tbody2 là cả table mình cần
        if (semester == '0') {
            table.find('tr').each(function (i, el) {
                var $tds = $(this).find('td');
                var th_length = $('tr:nth-child(2) th').length; // lấy số lượng cột điểm ( bị duplicate nên sẽ chia 2 để lấy tên cột cho chính xác )
                if (score_fomula == 'vn') {
                    for (i = Math.floor(th_length/2); i <= th_length; i++) {
                        if ($('tr:nth-child(2) th').eq(i).text() == 'tx') {
                            count_tx++;
                        } else if ($('tr:nth-child(2) th').eq(i).text() == 'gk') {
                            if (!flag_added) {
                                // vì table đang là 2 bảng chồng lên nhau nên phải thêm vào cả 2 để tăng length th_length cho đều
                                // thêm vào mặt trước
                                $('tr:nth-child(2) th').eq((i - 1)).after('<th>tx</th>');
                                //thêm vào mặt sau
                                $('tr:nth-child(2) th').eq((i - 1)-Math.floor(th_length/2)).after('<th style=\"display:none;\">tx</th>');
                                // tăng độ dài cột semester
                                var cols =  $('tr:nth-child(1) th').eq(9).attr("colspan");
                                $('tr:nth-child(1) th').eq(9).attr("colspan",parseInt(cols) + 1);
                                flag_added = true;
                                index_insert_td = i - Math.floor(th_length/2)- 1 + 3;
                                // count_tx=0;
                            }
                            if(count_tx == 6) {
                                count_tx =0;
                                $('.js_point_tx1-add').prop('disabled', true);
                            }
                            $tds.eq(index_insert_td).after('<td><input name=\"point\" type=\"number\" min=\"0\" step=\"0.01\" style=\"max-width: 50px\"\n value=\"\"/></td>');
                            count_tx =0;
                            break;
                        }
                    }
                }
            });
            resize_table();
        }
    });
    // Thêm cột gk khi nhấn Add Mid-Point ở màn hình import điểm bẳng tay ( cả năm )
    $(document).on('click', '.js_point_gk1-add', function (e) {
        e.preventDefault();
        var semester = $('#semester').val();
        var score_fomula = $("input[name='score_fomula']").val();
        var th_length = $('tr:nth-child(2) th').length; // lấy số lượng cột điểm ( bị duplicate nên sẽ chia 2 để lấy tên cột cho chính xác )
        var flag_added = false;
        var count_gk = 1;
        var index_insert_td = 0;
        var table = $("table tbody").eq(1); // màn hình bị chia thành 2 tbody, tbody 1 chứa những cột pined, tbody2 là cả table mình cần
        if (semester == '0') {
            table.find('tr').each(function (i, el) {
                var $tds = $(this).find('td');
                if (score_fomula == 'vn') {
                    for (i = Math.floor(th_length/2); i <= th_length; i++) {
                        if ($('tr:nth-child(2) th').eq(i).text() == 'gk') {
                            count_gk++;
                        } else if ($('tr:nth-child(2) th').eq(i).text() == 'ck') {
                            if (!flag_added) {
                                // vì table đang là 2 bảng chồng lên nhau nên phải thêm vào cả 2 để tăng length th_length cho đều
                                // thêm vào mặt trước
                                $('tr:nth-child(2) th').eq(i - 1).after('<th>gk</th>');
                                //thêm vào mặt sau
                                $('tr:nth-child(2) th').eq((i - 1)-Math.floor(th_length/2)).after('<th style=\"display:none;\">gk</th>');
                                // tăng độ dài cột semester
                                var cols =  $('tr:nth-child(1) th').eq(9).attr("colspan");
                                $('tr:nth-child(1) th').eq(9).attr("colspan",parseInt(cols) + 1);
                                flag_added = true;
                                index_insert_td = i - Math.floor(th_length/2)- 1 + 3;
                            }
                            if(count_gk == 2) {
                                count_gk =0;
                                $('.js_point_gk1-add').prop('disabled', true);
                            }
                            $tds.eq(index_insert_td).after($.trim('<td><input name=\"point\" type=\"number\" min=\"0\" step=\"0.01\" style=\"max-width: 50px\"\n value=\"\"/></td>'));
                            count_gk =0;
                            break;
                        }
                    }
                }
            });
            resize_table();
        }
    });
    // Thêm cột tx khi nhấn Add Freq-point2 ở màn hình import điểm bẳng tay ( cả năm )
    $(document).on('click', '.js_point_tx2-add', function (e) {
        e.preventDefault();
        var semester = $('#semester').val();
        var score_fomula = $("input[name='score_fomula']").val();
        var th_length = $('tr:nth-child(2) th').length; // lấy số lượng cột điểm ( bị duplicate nên sẽ chia 2 để lấy tên cột cho chính xác )
        var flag_added = false;
        var count_tx = 1;
        var index_insert_td = 0;
        var table = $("table tbody").eq(1); // màn hình bị chia thành 2 tbody, tbody 1 chứa những cột pined, tbody2 là cả table mình cần
        if (semester == '0') {
            table.find('tr').each(function (i, el) {
                var $tds = $(this).find('td');
                var th_length = $('tr:nth-child(2) th').length; // lấy số lượng cột điểm ( bị duplicate nên sẽ chia 2 để lấy tên cột cho chính xác )
                // đặt 1 biến flag đánh dấu cột hiện tại đang ở kỳ nào
                var semester_flag = 1;
                if (score_fomula == 'vn') {
                    for (i = Math.floor(th_length/2); i <= th_length; i++) {
                        if (semester_flag == 1) {
                            if ($('tr:nth-child(2) th').eq(i).text() == 'ck') {
                                //chuyển sang học kỳ 2
                                semester_flag = 2;
                            }
                        } else if (semester_flag == 2) {

                            if ($('tr:nth-child(2) th').eq(i).text() == 'tx') {
                                count_tx++;
                            } else if ($('tr:nth-child(2) th').eq(i).text() == 'gk') {
                                if (!flag_added) {
                                    // vì table đang là 2 bảng chồng lên nhau nên phải thêm vào cả 2 để tăng length th_length cho đều
                                    // thêm vào mặt trước
                                    $('tr:nth-child(2) th').eq((i - 1)).after('<th>tx</th>');
                                    //thêm vào mặt sau
                                    $('tr:nth-child(2) th').eq((i - 1)-Math.floor(th_length/2)).after('<th style=\"display:none;\">tx</th>');
                                    // tăng độ dài cột semester
                                    var cols =  $('tr:nth-child(1) th').eq(10).attr("colspan");
                                    $('tr:nth-child(1) th').eq(10).attr("colspan",parseInt(cols) + 1);
                                    flag_added = true;
                                    index_insert_td = i - Math.floor(th_length/2)- 1 + 3;
                                    // count_tx=0;
                                }
                                if(count_tx == 6) {
                                    count_tx =0;
                                    $('.js_point_tx2-add').prop('disabled', true);
                                }
                                $tds.eq(index_insert_td).after('<td><input name=\"point\" type=\"number\" min=\"0\" step=\"0.01\" style=\"max-width: 50px\"\n value=\"\"/></td>');
                                count_tx =0;
                                break;
                            }
                        }
                    }
                }
            });
            resize_table();
        }
    });
    // Thêm cột gk khi nhấn Add Mid-Point ở màn hình import điểm bẳng tay ( cả năm )
    $(document).on('click', '.js_point_gk2-add', function (e) {
        e.preventDefault();
        var semester = $('#semester').val();
        var score_fomula = $("input[name='score_fomula']").val();
        var th_length = $('tr:nth-child(2) th').length; // lấy số lượng cột điểm ( bị duplicate nên sẽ chia 2 để lấy tên cột cho chính xác )
        var flag_added = false;
        var count_gk = 1;
        var index_insert_td = 0;
        var table = $("table tbody").eq(1); // màn hình bị chia thành 2 tbody, tbody 1 chứa những cột pined, tbody2 là cả table mình cần
        if (semester == '0') {
            table.find('tr').each(function (i, el) {
                var $tds = $(this).find('td');
                // đặt 1 biến flag đánh dấu cột hiện tại đang ở kỳ nào
                var semester_flag = 1;
                if (score_fomula == 'vn') {
                    for (i = Math.floor(th_length/2); i <= th_length; i++) {
                        if (semester_flag == 1) {
                            if ($('tr:nth-child(2) th').eq(i).text() == 'ck') {
                                //chuyển sang học kỳ 2
                                semester_flag = 2;
                            }
                        } else if (semester_flag == 2) {
                            if ($('tr:nth-child(2) th').eq(i).text() == 'gk') {
                                count_gk++;
                            } else if ($('tr:nth-child(2) th').eq(i).text() == 'ck') {
                                if (!flag_added) {
                                    // vì table đang là 2 bảng chồng lên nhau nên phải thêm vào cả 2 để tăng length th_length cho đều
                                    // thêm vào mặt trước
                                    $('tr:nth-child(2) th').eq(i - 1).after('<th>gk</th>');
                                    //thêm vào mặt sau
                                    $('tr:nth-child(2) th').eq((i - 1)-Math.floor(th_length/2)).after('<th style=\"display:none;\">gk</th>');
                                    // tăng độ dài cột semester
                                    var cols =  $('tr:nth-child(1) th').eq(10).attr("colspan");
                                    $('tr:nth-child(1) th').eq(10).attr("colspan",parseInt(cols) + 1);
                                    flag_added = true;
                                    index_insert_td = i - Math.floor(th_length/2)- 1 + 3;
                                }
                                if(count_gk == 2) {
                                    count_gk =0;
                                    $('.js_point_gk2-add').prop('disabled', true);
                                }
                                $tds.eq(index_insert_td).after($.trim('<td><input name=\"point\" type=\"number\" min=\"0\" step=\"0.01\" style=\"max-width: 50px\"\n value=\"\"/></td>'));
                                count_gk =0;
                                break;
                            }
                        }
                    }
                }
            });
            resize_table();
        }
    });
    //ADD END MANHDD 10/06/2021
    $("#point_subject_id").change(function () {
        var submit = $('#submit_id');

        var username = $(this).attr('data-username');
        // var class_level_id = $('#point_class_level_id').val();
        // var class_id = $('#point_class_id_import').val();
        var school_year = $('#school_year').val();
        var subject_id = this.value;
        var search_with = $("input[name='do']").val();
        if (search_with != 'importManual') return;
        if (subject_id == '') {
            submit.prop('disabled', true);
            return;
        } else {
            submit.prop('disabled', false);
        }
        //Đếm số lượng column điểm xem có max hay không
        var count_tx1 = 0;
        var count_tx2 =0;
        var count_gk1 =0;
        var count_gk2= 0;
        var semester_flag = 1; // đánh dấu đang ở kỳ nào
        var is_last_semester = $('#is_last_semester').val();
        var semester = $('select[name="semester"]').val();
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading...']);
        $('.action-point').hide();
        $('#result_info').html('');
        $('#class_list_point').html('');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');
        $.post(api['class/point'], {
            'do': 'showDataImport',
            'username': username,
            // 'class_level_id': class_level_id,
            // 'class_id': class_id,
            'school_year': school_year,
            'is_last_semester': is_last_semester,
            'subject_id': subject_id,
            'semester': semester
        }, function (data) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            submit.prop('disabled', false);
            submit.html(submit.data('text'));
            $('.action-point').show();
            $('#class_list_point').html(data.results);
            var th_length = $('tr:nth-child(2) th').length; // lấy số lượng cột điểm ( bị duplicate nên sẽ chia 2 để lấy tên cột cho chính xác )

            for (i = 0; i <= th_length / 2; i++) {
                if ($('tr:nth-child(2) th').eq(i).text() == 'tx') {
                    if(semester_flag==1) {
                        count_tx1++;
                    }else if ( semester_flag ==2) {
                        count_tx2++;
                    }
                } else if ($('tr:nth-child(2) th').eq(i).text() == 'gk') {
                    if(semester_flag==1) {
                        count_gk1++;
                    }else if (semester_flag ==2) {
                        count_gk2++;
                    }
                }else if($('tr:nth-child(2) th').eq(i).text() == 'ck') {
                    if(i==Math.floor(th_length/2)-1) {
                        break;
                    }  else {
                        semester_flag = 2;
                    }
                }
            }
            console.log('count_tx1 =' +count_tx1);
            console.log('count_tx2 =' +count_tx2);
            console.log('count_gk1 =' +count_gk1);
            console.log('count_gk2 =' +count_gk2);
            if (semester != 0) {
                //button
                $('.js_point_tx1-add').hide();
                $('.js_point_gk1-add').hide();
                $('.js_point_tx2-add').hide();
                $('.js_point_gk2-add').hide();
                //label
                $('.point_tx1-add').hide();
                $('.point_gk1-add').hide();
                $('.point_tx2-add').hide();
                $('.point_gk2-add').hide();
                //button
                $('.js_point_tx-add').show();
                $('.js_point_gk-add').show();
                //label
                $('.point_tx-add').show();
                $('.point_gk-add').show();
                $('.js_point_tx-add').prop('disabled', false);
                $('.js_point_gk-add').prop('disabled', false);
            } else if (semester == 0) {
                //button
                $('.js_point_tx-add').hide();
                $('.js_point_gk-add').hide();
                //label
                $('.point_tx-add').hide();
                $('.point_gk-add').hide();
                //button
                $('.js_point_tx1-add').show();
                $('.js_point_gk1-add').show();
                $('.js_point_tx2-add').show();
                $('.js_point_gk2-add').show();
                // label
                $('.point_tx1-add').show();
                $('.point_gk1-add').show();
                $('.point_tx2-add').show();
                $('.point_gk2-add').show();
                $('.js_point_tx1-add').prop('disabled', false);
                $('.js_point_gk1-add').prop('disabled', false);
                $('.js_point_tx2-add').prop('disabled', false);
                $('.js_point_gk2-add').prop('disabled', false);
            }
            if(count_tx1 == 6) {
                $('.js_point_tx-add').prop('disabled', true);
                $('.js_point_tx1-add').prop('disabled', true);
            }
            if(count_tx2 == 6) {
                $('.js_point_tx2-add').prop('disabled', true);
            }
            if(count_gk1 == 2) {
                $('.js_point_gk-add').prop('disabled', true);
                $('.js_point_gk1-add').prop('disabled', true);
            }
            if(count_gk2 == 2) {
                $('.js_point_gk2-add').prop('disabled', true);
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                submit.prop('disabled', false);
                submit.html(submit.data('text'));

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Bắt sự kiện khi nhập điểm bằng tay
    $(document).on('submit', '#point_import_Manual_form', function (e) {
        e.preventDefault();
        var username = $('#username').val();
        // var class_id = $('#point_class_id_import').val();
        var school_year = $('#school_year').val();
        // var search_with = $("input[name='do']").val();
        var semester = $('#semester').val();
        var score_fomula = $("input[name='score_fomula']").val();
        var subject_id = $('#point_subject_id').val();
        var is_last_semester = $('#is_last_semester').val();
        var childs_points = [];
        var row = [];
        var th_length = $('tr:nth-child(2) th').length; // lấy số lượng cột điểm ( bị duplicate nên sẽ chia 2 để lấy tên cột cho chính xác )
        console.log('th_length= ' + th_length);
        var table = $("table tbody").eq(1); // màn hình bị chia thành 2 tbody, tbody 1 chứa những cột pined, tbody2 là cả table mình cần
        if (semester == '0') {
            table.find('tr').each(function (i, el) {
                row = {};
                var $tds = $(this).find('td');
                var child_name = $tds.eq(1).text();
                var child_code = $tds.eq(2).text();
                row['child_code'] = child_code;
                row['child_name'] = child_name;
                // đặt 1 biến flag đánh dấu cột hiện tại đang ở kỳ nào
                var semester_flag = 1;
                if (score_fomula == 'vn') {
                    var count_hs1 = 1;
                    var count_gk = 1;
                    var index_to_start = 0;
                    // vì các cột được thêm vào nên số lượng th_length/2 sẽ k chính xác => phải đếm từng số lượng cột
                    for (i = 0; i < th_length; i++) {
                        if ($('tr:nth-child(2) th').eq(i).text() == 'ck') {
                            index_to_start++; // ở dưới sẽ bắt đầu từ index này
                            console.log('index_to_start = '+ index_to_start);
                            if(index_to_start==2) {
                                index_to_start = i+1;
                                break;
                            }
                        }
                    }
                    for (i = index_to_start; i < th_length; i++) {
                        if (semester_flag == 1) {
                            if ($('tr:nth-child(2) th').eq(i).text() == 'tx') {
                                // lấy bắt đầu từ cột thứ 3 ( các cột chứa thẻ input - điểm )
                                row['hs1' + count_hs1] = $tds.eq(i-index_to_start + 3).find('input').val();
                                count_hs1++;
                            } else if ($('tr:nth-child(2) th').eq(i).text() == 'gk') {
                                row['gk1' + count_gk] = $tds.eq(i-index_to_start + 3).find('input').val();
                                count_gk++;
                            } else if ($('tr:nth-child(2) th').eq(i).text() == 'ck') {
                                row['ck1'] = $tds.eq(i-index_to_start + 3).find('input').val();
                                // lấy xong học kỳ 1, chuyển sang học kỳ 2
                                count_hs1 = 1;
                                count_gk = 1;
                                semester_flag = 2;
                            }
                        } else if (semester_flag == 2) {
                            if ($('tr:nth-child(2) th').eq(i).text() == 'tx') {
                                // lấy bắt đầu từ cột thứ 3 ( các cột chứa thẻ input - điểm )
                                row['hs2' + count_hs1] = $tds.eq(i-index_to_start + 3).find('input').val();
                                count_hs1++;
                            } else if ($('tr:nth-child(2) th').eq(i).text() == 'gk') {
                                row['gk2' + count_gk] = $tds.eq(i-index_to_start + 3).find('input').val();
                                count_gk++;
                            } else if ($('tr:nth-child(2) th').eq(i).text() == 'ck') {
                                row['ck2'] = $tds.eq(i-index_to_start + 3).find('input').val();
                                // Lấy xong kỳ 2, chuyển sang lấy điểm thi lại
                                semester_flag = 3;
                            }
                        } else {
                            row['re_exam'] = $tds.eq(i-index_to_start + 4).find('input').val();
                        }
                    }
                } else {
                    hs11 = $tds.eq(3).find('input').val();
                    hs12 = $tds.eq(4).find('input').val();
                    hs13 = $tds.eq(5).find('input').val();
                    gk11 = $tds.eq(6).find('input').val();
                    hs21 = $tds.eq(7).find('input').val();
                    hs22 = $tds.eq(8).find('input').val();
                    hs23 = $tds.eq(9).find('input').val();
                    gk21 = $tds.eq(10).find('input').val();
                    re_exam = $tds.eq(11).find('input').val();


                    row['hs11'] = hs11;
                    row['hs12'] = hs12;
                    row['hs13'] = hs13;
                    row['gk11'] = gk11;
                    row['hs21'] = hs21;
                    row['hs22'] = hs22;
                    row['hs23'] = hs23;
                    row['gk21'] = gk21;
                    row['re_exam'] = re_exam;
                }
                childs_points.push(row);
                // do something with productId, product, Quantity
            });
        } else {
            table.find('tr').each(function (i, el) {
                row = {};
                var $tds = $(this).find('td'),
                    child_name = $tds.eq(1).text();
                child_code = $tds.eq(2).text();
                row['child_code'] = child_code;
                row['child_name'] = child_name;
                if (score_fomula == 'vn') {
                    var count_hs1 = 1;
                    var count_hk = 1;
                    var index_to_start = 0;
                    // vì các cột được thêm vào nên số lượng th_length/2 sẽ k chính xác => phải đếm từng số lượng cột
                    for (i = 0; i < th_length; i++) {
                        if ($('tr:nth-child(2) th').eq(i).text() == 'ck') {
                            index_to_start = i+1; // ở dưới sẽ bắt đầu từ index này
                            break;
                        }
                    }
                    for (i = index_to_start; i < th_length; i++) {
                        if ($('tr:nth-child(2) th').eq(i).text() == 'tx') {
                            // lấy bắt đầu từ cột thứ 3 ( các cột chứa thẻ input - điểm )
                            row['hs' + semester + count_hs1] = $tds.eq(i-index_to_start + 3).find('input').val();
                            count_hs1++;
                        } else if ($('tr:nth-child(2) th').eq(i).text() == 'gk') {
                            row['gk' + semester + count_hk] = $tds.eq(i-index_to_start + 3).find('input').val();
                            count_hk++;
                        } else if ($('tr:nth-child(2) th').eq(i).text() == 'ck') {
                            row['ck' + semester] = $tds.eq(i-index_to_start + 3).find('input').val();
                            // lấy xong học kỳ 1, chuyển sang học kỳ 2
                        }
                    }
                } else {
                    row['hs' + semester + '1'] = $tds.eq(3).find('input').val();
                    row['hs' + semester + '2'] = $tds.eq(4).find('input').val();
                    row['hs' + semester + '3'] = $tds.eq(5).find('input').val();
                    row['gk' + semester + '1'] = $tds.eq(6).find('input').val();
                }
                childs_points.push(row);
            });
        }

        var submit = $('#submit_id');
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading...']);
        $('#result_info').html('');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        // $.ajax({
        //     url: api['school/point'],
        //     type: 'POST',
        //     data: {new FormData(this), 'school_username': school_username},
        //     processData: false,
        //     contentType: false
        // }).done

        $.post(api['class/point'], {
            'username': username,
            // 'class_id': class_id,
            'do': 'importManual',
            'school_year': school_year,
            'semester' : semester,
            'subject_id': subject_id,
            'is_last_semester': is_last_semester,
            'childs_points': childs_points
        }, function (data) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            submit.prop('disabled', false);
            submit.html(submit.data('text'));

            $('#result_info').html(data.results);
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                submit.prop('disabled', false);
                submit.html(submit.data('text'));

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });
    //Khi chọn học kỳ hiển thị danh sách môn học mà giáo viên được assign tương ứng
    $("#semester").change(function () {
        console.log('semester is called');
        var username = $(this).attr('data-username');
        var school_year = $('#school_year').val();
        var semester = this.value;
        var search_with = $("input[name='do']").val();
        if(search_with=='comment') { // nếu ở màn hình comment
            $("#point_child_id_comment").trigger("change");
            return;
        }
        $('.action-point').hide();
        // Show loading
        $("#loading_full_screen").removeClass('hidden');
        if ($('#class_list_point')) {
            $('#class_list_point').html('');
        }

        $.post(api['class/point'], {
            'do': 'list_subject_import', // Sử dụng ở màn hình import điểm
            'username': username,
            // 'class_level_id': class_level_id,
            'semester': semester,
            'school_year': school_year,
            'search_with': search_with
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            if (response.results) {
                if (search_with == 'search_with_subject' || search_with == 'importManual') {//nếu xem theo môn học thì fill môn học
                    $('#point_subject_id').html(response.results);
                } else {//nếu xem theo học sinh thì fill học sinh search_with_student
                    $('#point_student_code').html(response.results);
                }

            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    }).trigger("change");

    //Xử lý khi chọn TRẠNG THÁI điểm danh ở màn hình tổng hợp điểm danh lớp
    $('body').on('change', '.js_class-attendance-status', function () {
        var id = $(this).attr('data-id');
        var child_id = $(this).attr('data-child');
        var attendance_status = $(this).val();
        var attendance_date = $(this).attr('data-date');
        var username = $(this).attr('data-username');
        var old_status = $(this).attr('data-status');

        // Xử lý đổi màu background select khi thay đổi trạng thái điểm danh
        if (attendance_status == 1) {
            $(this).css("background", '#fff');
        } else if (attendance_status == 2) {
            $(this).css("background", '#6495ed');
        } else if (attendance_status == 3) {
            $(this).css("background", '#008b8b');
        } else if (attendance_status == 4) {
            $(this).css("background", '#ff1493');
        } else {
            $(this).css("background", '#deb887');
        }


        // Set data-status = giá trị select để thay đổi lần sau đúng
        $(this).attr("data-status", parseInt(attendance_status));

        // Xử lý ô tổng hợp đi học và nghỉ của trẻ
        var presentCnt = $('#present_' + child_id).text();
        var absenceCnt = $('#absence_' + child_id).text();
        presentCnt = parseInt(presentCnt);
        absenceCnt = parseInt(absenceCnt);

        // Tổng đi, nghỉ học của cả lớp
        var classPresentCount = $('#present_count_' + id).text();
        var classAbsenceCnt = $('#absence_count_' + id).text();
        classPresentCount = parseInt(classPresentCount);
        classAbsenceCnt = parseInt(classAbsenceCnt);

        // Tổng trong khoảng thời gian tìm kiếm
        var totalPresent = $('#total_present').text();
        var totalAbsence = $('#total_absence').text();
        totalPresent = parseInt(totalPresent);
        totalAbsence = parseInt(totalAbsence);

        var presentStatus = [1, 2, 3];
        var absenceStatus = [0, 4];
        if ((jQuery.inArray(parseInt(attendance_status), presentStatus) !== -1) && (jQuery.inArray(parseInt(old_status), absenceStatus) !== -1)) {
            // Trẻ
            var newPresentCnt = presentCnt + 1;
            var newAbsenceCnt = absenceCnt - 1;

            $('#present_' + child_id).text(newPresentCnt);
            $('#absence_' + child_id).text(newAbsenceCnt);

            // Cả lớp
            var newClassPresentCount = classPresentCount + 1;
            var newClassAbsenceCnt = classAbsenceCnt - 1;

            $('#present_count_' + id).html(newClassPresentCount);
            $('#absence_count_' + id).html(newClassAbsenceCnt);

            // Tổng trong khoảng thời gian
            var totalPresent = totalPresent + 1;
            var totalAbsence = totalAbsence - 1;

            $('#total_present').html(totalPresent);
            $('#total_absence').html(totalAbsence);
        }
        if ((jQuery.inArray(parseInt(attendance_status), absenceStatus) !== -1) && (jQuery.inArray(parseInt(old_status), presentStatus) !== -1)) {
            // Trẻ
            var newPresentCnt = presentCnt - 1;
            var newAbsenceCnt = absenceCnt + 1;
            $('#present_' + child_id).text(newPresentCnt);
            $('#absence_' + child_id).text(newAbsenceCnt);

            // Cả lớp
            var newClassPresentCount = classPresentCount - 1;
            var newClassAbsenceCnt = classAbsenceCnt + 1;

            $('#present_count_' + id).html(newClassPresentCount);
            $('#absence_count_' + id).html(newClassAbsenceCnt);

            // Tổng trong khoảng thời gian
            var totalPresent = totalPresent - 1;
            var totalAbsence = totalAbsence + 1;

            $('#total_present').html(totalPresent);
            $('#total_absence').html(totalAbsence);
        }

        // Xử lý ô tổng đi học và nghỉ học của cả lớp

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['class/attendance'], {
            'do': 'change_attendance_status',
            'id': id,
            'attendance_date': attendance_date,
            'child_id': child_id,
            'attendance_status': attendance_status,
            'username': username,
            'old_status': old_status
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }

            // school_list.html(response.results);
            // $('#all_month').addClass('x-hidden');
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Xử lý khi search thông tin sức khỏe
    $('body').on('click', '.js_class-chart-search', function () {
        var child_id = $(this).attr('data-child');
        var username = $(this).attr('data-username');
        var begin = $('#begin').val();
        var end = $('#end').val();
        var chart_list = $('#chart_list');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['class/child'], {
            'do': 'search_chart',
            'child_id': child_id,
            'begin': begin,
            'end': end,
            'username': username
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            if (response.results) {
                chart_list.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    $(document).ready(function () {
        var $table = $('.table-pinned');
        var $fixedColumn = $table.clone().insertBefore($table).addClass('fixed-column');
        $fixedColumn.find('th').each(function (i, elem) {
            $(this).width($table.find('th:eq(' + i + ')').width());
        });
        $fixedColumn.find('td').each(function (i, elem) {
            $(this).width($table.find('td:eq(' + i + ')').width());
        });
        $fixedColumn.find('th,td').not('.pinned').hide();
        $fixedColumn.find('[id]').each(function () {
            $(this).removeAttr('id');
        });
        $fixedColumn.find('tr').each(function (i, elem) {
            $(this).height($table.find('tr:eq(' + i + ')').height());
        });

        $(window).resize(function () {
            $fixedColumn.find('tr').each(function (i, elem) {
                $(this).height($table.find('tr:eq(' + i + ')').height());
            });
            $fixedColumn.find('td').each(function (i, elem) {
                $(this).addClass('white-space_nowrap');
                $(this).width($table.find('td:eq(' + i + ')').width());
            });
        });

        //$fixedColumn.find('td').addClass('white-space_nowrap');

        $('body').on('click', '#right', function () {
            var width_col = $('.table-pinned').find('td:eq(' + 2 + ')').width();
            var pos = $('#example').scrollLeft() + width_col + 100;
            $('#example').scrollLeft(pos);
        });
        $('body').on('click', '#left', function () {
            var width_col = $('.table-pinned').find('td:eq(' + 2 + ')').width();
            var pos = $('#example').scrollLeft() - width_col - 100;
            $('#example').scrollLeft(pos);
        });

        function fixDiv() {
            var $cache = $('#getFixed');
            var $button = $('#table_button');
            if ($(window).scrollTop() > 100) {
                $cache.css({
                    'position': 'fixed',
                    'top': '50px'
                });
                $cache.width($('#attendance_list').width() - 1);
            } else
                $cache.css({
                    'position': 'relative',
                    'top': 'auto'
                });

            if ($(window).scrollTop() > 100)
                $button.css({
                    'position': 'fixed',
                    'top': '90px'
                });
            else
                $button.css({
                    'position': 'relative',
                    'top': 'auto'
                });
        }

        $(window).scroll(fixDiv);
        fixDiv();
    });

    /**
     * Tìm kiếm trẻ đã có sẵn trong hệ thống bằng child code
     */
    $('body').on('click', '.js_child-addexisting', function () {
        var school_username = $(this).attr('data-school_username');
        var school_id = $(this).attr('data-id');
        var class_id = $(this).attr('data-class_id');
        var childcode = $('#childcode').val();
        var table_child_addexisting = $('#table_child_addexisting');
        var is_class = 1;

        $('#search').addClass('x-hidden');

        // Show loading
        $('#loading').removeClass('x-hidden');
        $('#loading_full_screen').removeClass('hidden');

        table_child_addexisting.html('');

        $.post(api['school/search'], {
            'school_username': school_username,
            'school_id': school_id,
            'class_id': class_id,
            'func': 'searchchildexistclass',
            'childcode': childcode,
            'is_class': is_class
        }, function (response) {
            if (response.results) {
                $('#search').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                table_child_addexisting.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            } else {
                window.location.reload();
            }
        }, 'json')
            .fail(function () {
                $('#search').removeClass('x-hidden');
                $('#loading').addClass('x-hidden');
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    $('body').on('click', '#beginat_dp', (function () {
        $('#start_date').focus();
        $(this).datetimepicker({
            format: DATE_FORMAT
        });
    }));
    /**
     * Bắt sự kiện khi nhấn nút tìm kiếm ở bảng điểm
     */
    $(document).on('submit', '#point_search_form', function (e) {
        e.preventDefault();
        var submit = $('#submit_id');
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading...']);
        $('#class_list_point').html('');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.ajax({
            url: api['class/point'],
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false
        }).done(function (data) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            submit.prop('disabled', false);
            submit.html(submit.data('text'));
            $('#class_list_point').html(data.results);
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                submit.prop('disabled', false);
                submit.html(submit.data('text'));

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });
    //Bắt sự kiện khi tạo nhật ký cho trẻ bên lớp
    $(document).on('submit', '#add_child_journal_class', function (e) {
        var _this = $(this);
        var submit = _this.find('button[type="submit"]');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        /* show loading */
        submit.data('text', submit.html());
        submit.prop('disabled', true);
        submit.html(__['Loading']);

        e.preventDefault();
        $.ajax({
            url: api['class/child'],
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false
        }).done(function (data) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');

            /* hide loading */
            submit.prop('disabled', false);
            submit.html(submit.data('text'));
            if (data.success) {
                modal('#modal-success', {title: __['Success'], message: data.message});
                modal_hidden(timeModal);
                $("input[name*='file']").val("");
            } else if (data.error) {
                modal('#modal-error', {title: __['Error'], message: data.message});
            } else if (data.callback) {
                modal('#modal-success', {title: __['Success'], message: __['Success']});
                modal_hidden(timeModal);
                $("input[name*='file']").val("");
                //eval(data.callback);
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                /* hide loading */
                submit.prop('disabled', false);
                submit.html(submit.data('text'));
                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Xử lý khi xóa album ảnh
    $('body').on('click', '.js_class-journal-delete-album', function () {
        var journal_id = $(this).attr('data-id');
        var child_id = $(this).attr('data-child');
        var handle = $(this).attr('data-handle');
        var username = $(this).attr('data-username');
        var _this = $(this);

        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['class/child'], {
                'do': handle,
                'child_journal_album_id': journal_id,
                'child_id': child_id,
                'username': username
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                var journal_album = _this.parents('.journal_album');
                journal_album.hide();
                $("#modal").modal('hide');
                $(".modal-backdrop").hide();
                // if (response.callback) {
                //     eval(response.callback);
                // } else if(response.error) {
                //     modal('#modal-error', {title: __['Error'], message: response.message});
                // } else {
                //     window.location.reload();
                // }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    $('body').on('click', '.edit_caption', function () {
        var _this = $(this);
        var journal_album = _this.parents('.journal_album');
        var caption_show = journal_album.find('.caption_show');
        var caption_edit = journal_album.find('.caption_edit');
        caption_show.hide();
        caption_edit.removeClass('x-hidden');
    });

    // Xử lý khi sửa caption album ảnh
    $('body').on('change', '.js_class-journal-edit-caption_change', function () {
        var journal_id = $(this).attr('data-id');
        var child_id = $(this).attr('data-child');
        var username = $(this).attr('data-username');
        var caption = $(this).val();
        var _this = $(this);

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['class/child'], {
            'do': 'edit_caption',
            'child_journal_album_id': journal_id,
            'username': username,
            'child_id': child_id,
            'caption': caption
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            var journal_album = _this.parents('.journal_album');
            var caption_show = journal_album.find('.caption_show');
            var caption_edit = journal_album.find('.caption_edit');
            caption_show.text(caption);
            caption_show.show();
            caption_edit.hide();
            // if (response.callback) {
            //     eval(response.callback);
            // } else if(response.error) {
            //     modal('#modal-error', {title: __['Error'], message: response.message});
            // } else {
            //     window.location.reload();
            // }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Xử lý khi sửa caption album ảnh
    $('body').on('click', '.js_class-journal-edit-caption', function () {
        var journal_id = $(this).attr('data-id');
        var child_id = $(this).attr('data-child');
        var username = $(this).attr('data-username');
        var caption = $('.caption_' + journal_id).val();
        var _this = $(this);

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['class/child'], {
            'do': 'edit_caption',
            'child_journal_album_id': journal_id,
            'username': username,
            'child_id': child_id,
            'caption': caption
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            var journal_album = _this.parents('.journal_album');
            var caption_show = journal_album.find('.caption_show');
            var caption_edit = journal_album.find('.caption_edit');
            caption_show.text(caption);
            caption_show.show();
            caption_edit.hide();
            // if (response.callback) {
            //     eval(response.callback);
            // } else if(response.error) {
            //     modal('#modal-error', {title: __['Error'], message: response.message});
            // } else {
            //     window.location.reload();
            // }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Xử lý khi xoa thông tin nhật ký của trẻ
    $('body').on('click', '.js_class-journal-delete', function () {
        var id = $(this).attr('data-id');
        var child_id = $(this).attr('data-child');
        var username = $(this).attr('data-username');
        var handle = $(this).attr('data-handle');
        var _this = $(this);

        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['class/child'], {
                'do': handle,
                'child_id': child_id,
                'username': username,
                'child_journal_id': id
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');
                var fa_border = _this.parents('.col-sm-3');
                fa_border.hide();
                $("#modal").modal('hide');
                $(".modal-backdrop").hide();
                // if (response.callback) {
                //     eval(response.callback);
                // } else if(response.error) {
                //     modal('#modal-error', {title: __['Error'], message: response.message});
                // } else {
                //     window.location.reload();
                // }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    // Xử lý khi tìm kiếm theo năm góc nhật ký
    $('body').on('click', '.js_class-journal-search', function () {
        var child_id = $(this).attr('data-id');
        var username = $(this).attr('data-username');
        var year = $('#year').val();
        var journal_list = $('#journal_list');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['class/child'], {
            'do': 'search_diary',
            'child_id': child_id,
            'username': username,
            'year': year
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            if (response.results) {
                journal_list.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Xử lý ẩn hiện nhật ký
    $('body').on('click', '.show_or_hidden_diary', function () {
        var id = $(this).attr('data-id');
        $('.album_images_' + id).toggle();
    });

    // Xử lý khi tìm kiếm theo năm góc nhật ký ở màn hình chung
    $('body').on('click', '.js_class-diary-search', function () {
        var child_id = $('#attendance_child_id').val();
        var username = $(this).attr('data-username');
        var is_new = $(this).attr('data-isnew');
        var year = $('#year').val();
        var journal_list = $('#journal_list');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['class/child'], {
            'do': 'search_diary_child',
            'child_id': child_id,
            'username': username,
            'year': year,
            'is_new': is_new
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            if (response.results) {
                journal_list.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    // Xử lý khi tìm kiếm thông tin sức khỏe ở màn hình chung
    $('body').on('click', '.js_class-health-search', function () {
        var child_id = $('#attendance_child_id').val();
        var username = $(this).attr('data-username');
        var is_new = $(this).attr('data-isnew');
        var begin = $('#begin').val();
        var end = $('#end').val();
        var chart_list = $('#chart_list');
        var module = $(this).attr('data-module');

        // Show loading
        $("#loading_full_screen").removeClass('hidden');

        $.post(api['class/child'], {
            'do': 'search_health_child',
            'child_id': child_id,
            'username': username,
            'begin': begin,
            'end': end,
            'is_new': is_new,
            'module': module
        }, function (response) {
            // Hidden loading
            $("#loading_full_screen").addClass('hidden');
            if (response.results) {
                chart_list.html(response.results);
            } else if (response.error) {
                modal('#modal-error', {title: __['Error'], message: response.message});
            }
        }, 'json')
            .fail(function () {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
            });
    });

    //Xử lý khi xóa những sổ liên lạc được chọn
    $('body').on('click', '#js_class-report-delete-all-select', function () {
        var handle = $(this).attr('data-handle');
        var username = $(this).attr('data-username');
        var ids = new Array();
        $('input[name="report_ids"]:checked').each(function () {
            ids.push(this.value);
        });

        confirm(__['Delete'], __['Are you sure you want to delete this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['class/report'], {
                'do': handle,
                'username': username,
                'ids': ids
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    //Xử lý khi thông báo những sổ liên lạc được chọn
    $('body').on('click', '#js_class-report-notify-all-select', function () {
        var handle = $(this).attr('data-handle');
        var username = $(this).attr('data-username');
        var ids = new Array();
        $('input[name="notify_report_ids"]:checked').each(function () {
            ids.push(this.value);
        });

        confirm(__['Actions'], __['Are you sure you want to do this?'], function () {
            // Show loading
            $("#loading_full_screen").removeClass('hidden');

            $.post(api['class/report'], {
                'do': handle,
                'username': username,
                'ids': ids
            }, function (response) {
                // Hidden loading
                $("#loading_full_screen").addClass('hidden');

                if (response.callback) {
                    eval(response.callback);
                } else if (response.error) {
                    modal('#modal-error', {title: __['Error'], message: response.message});
                } else {
                    window.location.reload();
                }
            }, 'json')
                .fail(function () {
                    // Hidden loading
                    $("#loading_full_screen").addClass('hidden');

                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                });
        });
    });

    //xử lý check all xóa sổ liên lạc
    $('body').on('change', '#report_checkall', function () {
        $(".report_delete").prop('checked', $(this).prop("checked"));
    });

    //xử lý check all xóa sổ liên lạc
    $('body').on('change', '#report_notify_checkall', function () {
        $(".report_notify").prop('checked', $(this).prop("checked"));
    });

    //xử lý check all chọn điểm danh về
    $('body').on('change', '#attendance_came_back_checkall', function () {
        $(".attendance_cameback_check").prop('checked', $(this).prop("checked"));
    });

    $('body').on('click', '#came-tab', function () {
        $("#attendance_do").val('save_rollup');
    });

    $('body').on('click', '#go-tab', function () {
        $("#attendance_do").val('save_rollup_back');
    });
});