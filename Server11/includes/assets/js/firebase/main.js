/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

// Initializes FriendlyChat.
function FriendlyChat() {
    this.checkSetup();
    this.initFirebase();
}

// Sets up shortcuts to Firebase features and initiate firebase auth.
FriendlyChat.prototype.initFirebase = function () {
    // TODO(DEVELOPER): Initialize Firebase.
    // Shortcuts to Firebase SDK features.
    this.database = firebase.database();
    this.storage = firebase.storage();
    // Initiates Firebase auth and listen to auth state changes.
    this.loadMessages();
};

// Loads chat messages history and listens for upcoming ones.
FriendlyChat.prototype.loadMessages = function () {
    
    var users = this.database.ref().child('Users');
    var conversations = this.database.ref().child('Conversations');
    var user_id = users.child(58);
    this.user_friends = user_id.child('friends');
    var newHTML ='';

// Reference to the /messages/ database path.
//   this.messagesRef = this.database.ref('messages');
    // Make sure we remove all previous listeners.
    this.user_friends.off();

    // Loads the last 12 messages and listen for new ones.
    var getFriends = function (data) {
        var list_friends;
        var friend_id = data.key;
        var conversation_id = data.val();

        list_friends = users.child(friend_id);

        list_friends.on('value', function(snapshot) {
            var friend_info = snapshot.val();

            if(friend_info['isOnline']) {
                var class_online = 'chat-sidebar-online';
                var info = '';
            } else {
                class_online = 'chat-sidebar-offline';
                info = "<br>" +
                    "<small>" +
                    "Online <span class='js_moment' data-time=" + friend_info['lastLogin'] +"></span>" +
                    "</small>"  ;
            }

            var oldHtml = document.getElementById("item-chat").innerHTML;
            var kChat = document.getElementById('item-'+ snapshot.key);

            if (kChat === null) {
                newHTML = oldHtml +
                    "<li class='feeds-item'>" +
                    "<div id='item-"+ snapshot.key + "'>" +

                    "</div>" +
                    "</li>" ;
            }

            document.getElementById("item-chat").innerHTML = newHTML;

            var append =  "<div class='data-container clickable small js_chat-start' data-uid='"+ snapshot.key + "' data-name='" + friend_info['fullName'] + "' data-picture='" + friend_info['avatar'] + "'>" +
            "<img class='data-avatar' src='" + friend_info['avatar'] + "' alt='" + friend_info['fullName'] + "'>" +
            " <div class='data-content'>" +
            "<div class='pull-right flip'>" +
            "<i class='fa fa-circle "+ class_online + "'></i>" +
            // "<i class='fa fa-circle chat-sidebar-online'></i>" +
            "</div>" +
            "<div>" +
            "<strong>" + friend_info['fullName'] +"</strong>" + info +
            "</div>" +
            "</div>" +
            "</div>" ;

            document.getElementById('item-'+ snapshot.key).innerHTML = append;
            // document.getElementById('item-'+ snapshot.key).innerHTML = append;

            console.log("zzzzz", list_friends);

        });

        // this.displayMessage(data.key, val.name, val.text, val.photoUrl, val.imageUrl);
    }.bind(this);

    this.user_friends.limitToLast(12).on('child_added', getFriends);
    // this.user_friends.limitToLast(12).on('child_changed', getFriends);


};


// Checks that the Firebase SDK has been correctly setup and configured.
FriendlyChat.prototype.checkSetup = function () {
    if (!window.firebase || !(firebase.app instanceof Function) || !window.config) {
        window.alert('You have not configured and imported the Firebase SDK. ' +
            'Make sure you go through the codelab setup instructions.');
    } else if (config.storageBucket === '') {
        window.alert('Your Firebase Storage bucket has not been enabled. Sorry about that. This is ' +
            'actually a Firebase bug that occurs rarely. ' +
            'Please go and re-generate the Firebase initialisation snippet (step 4 of the codelab) ' +
            'and make sure the storageBucket attribute is not empty. ' +
            'You may also need to visit the Storage tab and paste the name of your bucket which is ' +
            'displayed there.');
    }
};

window.onload = function () {
    window.friendlyChat = new FriendlyChat();
};
