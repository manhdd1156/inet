<?php
/**
 * functions
 *
 * @package CINet v1.1
 * @author NOGA Co., Ltd
 */


/* ------------------------------- */
/* Core */
/* ------------------------------- */

/**
 * check_system_requirements
 *
 * @return void
 */
function check_system_requirements()
{
    /* set required php version*/
    $required_php_version = '5.5';
    /* check php version */
    $php_version = phpversion();
    if (version_compare($required_php_version, $php_version, '>')) {
        _error("Installation Error", sprintf('<p class="text-center">Your server is running PHP version %1$s but Coniu requires at least %3$s.</p>', $php_version, $required_php_version));
    }
    /* check if mysqli enabled */
    if (!extension_loaded('mysqli')) {
        _error("Installation Error", '<p class="text-center">Your PHP installation appears to be missing the "mysqli" extension which is required by Coniu.</p><small>Back to your server admin or hosting provider to enable it for you</small>');
    }
    /* check if curl enabled */
    if (!extension_loaded('curl')) {
        _error("Installation Error", '<p class="text-center">Your PHP installation appears to be missing the "cURL" extension which is required by Coniu.</p><small>Back to your server admin or hosting provider to enable it for you</small>');
    }
    /* check if intl enabled */
    if (!extension_loaded('intl')) {
        _error("Installation Error", '<p class="text-center">Your PHP installation appears to be missing the "intl" extension which is required by Coniu.</p><small>Back to your server admin or hosting provider to enable it for you</small>');
    }
    /* check if json_decode enabled */
    if (!function_exists('json_decode')) {
        _error("Installation Error", '<p class="text-center">Your PHP installation appears to be missing the "json_decode()" function which is required by Coniu.</p><small>Back to your server admin or hosting provider to enable it for you</small>');
    }
    /* check if base64_decode enabled */
    if (!function_exists('base64_decode')) {
        _error("Installation Error", '<p class="text-center">Your PHP installation appears to be missing the "base64_decode()" function which is required by Coniu.</p><small>Back to your server admin or hosting provider to enable it for you</small>');
    }
    /* check if mail enabled */
    if (!function_exists('mail')) {
        _error("Installation Error", '<p class="text-center">Your PHP installation appears to be missing the "mail()" function which is required by Coniu.</p><small>Back to your server admin or hosting provider to enable it for you</small>');
    }
    if (!function_exists('getimagesize')) {
        _error("Installation Error", '<p class="text-center">Your PHP installation appears to be missing the "getimagesize()" function which is required by Coniu.</p><small>Back to your server admin or hosting provider to enable it for you</small>');
    }
}


/**
 * get_licence_key
 *
 * @param string $code
 * @return string
 */
function get_licence_key($code)
{
    $url = 'http://www.sngine.com/licence/v2/verify.php';
    $data = "code=" . $code . "&domain=" . $_SERVER['HTTP_HOST'];
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1; rv:5.0) Gecko/20100101 Firefox/5.0 Firefox/5.0');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_TIMEOUT, 30);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    $contents = curl_exec($curl);
    $status = curl_getinfo($curl);
    curl_close($curl);
    if ($status['http_code'] == 200) {
        $contents = json_decode($contents, true);
        if ($contents['error']) {
            throw new Exception($contents['error']['message'] . ' Error Code #' . $contents['error']['code']);
        }
        return $contents['licence_key'];
    } else {
        throw new Exception("Error Processing Request");
    }
}


/**
 * get_system_protocol
 *
 * @return string
 */
function get_system_protocol()
{
    $is_secure = false;
    if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
        $is_secure = true;
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' || !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on') {
        $is_secure = true;
    }
    return $is_secure ? 'https' : 'http';
}


/**
 * get_system_url
 *
 * @return string
 */
function get_system_url()
{
    $protocol = get_system_protocol();
    $system_url = $protocol . "://" . $_SERVER['HTTP_HOST'] . BASEPATH;
    return rtrim($system_url, '/');
}


/**
 * check_system_url
 *
 * @return void
 */
function check_system_url()
{
    $protocol = get_system_protocol();
    $parsed_url = parse_url(SYS_URL);
    if (($parsed_url['scheme'] != $protocol) || ($parsed_url['host'] != $_SERVER['HTTP_HOST'])) {
        header('Location: ' . SYS_URL);
    }
}


/**
 * redirect
 *
 * @param string $url
 * @return void
 */
function redirect($url = '')
{
    if ($url) {
        header('Location: ' . SYS_URL . $url);
    } else {
        header('Location: ' . SYS_URL);
    }
    exit;
}


/* ------------------------------- */
/* Security */
/* ------------------------------- */

/**
 * secure
 *
 * @param string $value
 * @param string $type
 * @param boolean $quoted
 * @return string
 */
function secure($value, $type = "", $quoted = true)
{
    global $db;
    if ($value !== 'null') {
        // [1] Sanitize
        /* Escape all (single-quote, double quote, backslash, NULs) */
        if (get_magic_quotes_gpc()) {
            $value = stripslashes($value);
        }
        /* Convert all applicable characters to HTML entities */
        $value = htmlentities($value, ENT_QUOTES, 'utf-8');
        // [2] Safe SQL
        $value = $db->real_escape_string($value);
        switch ($type) {
            case 'int':
                $value = ($quoted) ? "'" . intval($value) . "'" : intval($value);
                break;
            case 'datetime':
                $value = ($quoted) ? "'" . set_datetime($value) . "'" : set_datetime($value);
                break;
            case 'search':
                if ($quoted) {
                    $value = (!is_empty($value)) ? "'%" . $value . "%'" : "''";
                } else {
                    $value = (!is_empty($value)) ? "'%%" . $value . "%%'" : "''";
                }
                break;
            default:
                $value = (!is_empty($value)) ? "'" . $value . "'" : "''";
                break;
        }
    }
    return $value;
}


/**
 * session_hash
 *
 * @param string $hash
 * @return array
 */
function session_hash($hash)
{
    $hash_tokens = explode('-', $hash);
    if (count($hash_tokens) != 6) {
        _error(__("Error"), __("Your session hash has been broken, Please contact Coniu's support!"));
    }
    $position = array_rand($hash_tokens);
    $token = $hash_tokens[$position];
    return array('token' => $token, 'position' => $position + 1);
}

/**
 * _password_hash
 *
 * @param string $password
 * @return string
 */
function _password_hash($password)
{
    return password_hash($password, PASSWORD_DEFAULT);
}


/**
 * get_hash_key
 *
 * @param integer $length
 * @return string
 */
function get_hash_key($length = 8)
{
    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $count = mb_strlen($chars);
    for ($i = 0, $result = ''; $i < $length; $i++) {
        $index = rand(0, $count - 1);
        $result .= mb_substr($chars, $index, 1);
    }
    return $result;
}


/**
 * get_hash_token
 *
 * @return string
 */
function get_hash_token()
{
    return md5(time() * rand(1, 9999));
}

/* ------------------------------- */
/* Validation */
/* ------------------------------- */

/**
 * is_ajax
 *
 * @return void
 */
function is_ajax()
{
    if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || ($_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest')) {
        redirect();
    }
}

/**
 * is_empty
 *
 * @param string $value
 * @return boolean
 */
function is_empty($value)
{
    if (strlen(trim(preg_replace('/\xc2\xa0/', ' ', $value))) == 0) {
        return true;
    } else {
        return false;
    }
}


/**
 * valid_email
 *
 * @param string $email
 * @return boolean
 */
function valid_email($email)
{
    if (filter_var($email, FILTER_VALIDATE_EMAIL) !== false) {
        return true;
    } else {
        return false;
    }
}

/**
 * valid_url
 *
 * @param string $url
 * @return boolean
 */
function valid_url($url)
{
    if (filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_HOST_REQUIRED) !== false) {
        return true;
    } else {
        return false;
    }
}

/**
 * valid_username
 *
 * @param string $username
 * @return boolean
 */
function valid_username($username, $type = '')
{
    if ($type == "user" && is_numeric($username)) {
        return false;
    }
    if (strtolower($username) != 'admin' && strlen($username) >= 3 && preg_match('/^[a-zA-Z0-9]+([_|.]?[a-zA-Z0-9])*$/', $username)) {
        return true;
    } else {
        return false;
    }
}


/*
 * Check if a string is started with another string
 *
 * @param (string) ($needle) The string being searched for.
 * @param (string) ($haystack) The string being searched
 * @return (boolean) true if $haystack is started with $needle
 */
function start_with($needle, $haystack)
{
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
}

/**
 * valid_username
 *
 * @param string $number
 * @return boolean
 */
function valid_phone($number)
{
    $carriers_number = array(
        '096' => 'Viettel',
        '097' => 'Viettel',
        '098' => 'Viettel',
        '0162' => 'Viettel',
        '0163' => 'Viettel',
        '0164' => 'Viettel',
        '0165' => 'Viettel',
        '0166' => 'Viettel',
        '0167' => 'Viettel',
        '0168' => 'Viettel',
        '0169' => 'Viettel',
        '086' => 'Viettel',

        '032' => 'Viettel',
        '033' => 'Viettel',
        '034' => 'Viettel',
        '035' => 'Viettel',
        '036' => 'Viettel',
        '037' => 'Viettel',
        '038' => 'Viettel',
        '039' => 'Viettel',

        '090' => 'Mobifone',
        '093' => 'Mobifone',
        '0120' => 'Mobifone',
        '0121' => 'Mobifone',
        '0122' => 'Mobifone',
        '0126' => 'Mobifone',
        '0128' => 'Mobifone',
        '089' => 'Mobifone',

        '070' => 'Mobifone',
        '079' => 'Mobifone',
        '077' => 'Mobifone',
        '076' => 'Mobifone',
        '078' => 'Mobifone',

        '091' => 'Vinaphone',
        '094' => 'Vinaphone',
        '0123' => 'Vinaphone',
        '0124' => 'Vinaphone',
        '0125' => 'Vinaphone',
        '0127' => 'Vinaphone',
        '0129' => 'Vinaphone',
        '088' => 'Vinaphone',

        '083' => 'Vinaphone',
        '084' => 'Vinaphone',
        '085' => 'Vinaphone',
        '081' => 'Vinaphone',
        '082' => 'Vinaphone',

        '0993' => 'Gmobile',
        '0994' => 'Gmobile',
        '0995' => 'Gmobile',
        '0996' => 'Gmobile',
        '0997' => 'Gmobile',
        '0199' => 'Gmobile',

        '059' => 'Gmobile',

        '092' => 'Vietnamobile',
        '0186' => 'Vietnamobile',
        '0188' => 'Vietnamobile',

        '056' => 'Vietnamobile',
        '058' => 'Vietnamobile',

        '095' => 'SFone'
    );

    //$number = str_replace(array('-', '.', ' '), '', $number);

    // $number is not a phone number
    if (!preg_match('/^(01[2689]|09|08|07|05|03)[0-9]{8}$/', $number)) return false;
    // Store all start number in an array to search
    $start_numbers = array_keys($carriers_number);

    foreach ($start_numbers as $start_number) {
        // if $start number found in $number then return value of $carriers_number array as carrier name
        if (start_with($start_number, $number)) {
            //return $carriers_number[$start_number];
            return true;
        }
    }

    // if not found, return false
    return false;
}


/**
 * reserved_username
 *
 * @param string $username
 * @return boolean
 */
function reserved_username($username)
{
    //$reserved_usernames = array('install', 'static', 'contact', 'contacts', 'sign', 'signin', 'login', 'signup', 'register', 'signout', 'logout', 'reset', 'activation', 'connect', 'revoke', 'packages', 'started', 'search', 'friends', 'messages', 'message', 'notifications', 'notification', 'settings', 'setting', 'posts', 'post', 'photos', 'photo', 'create', 'pages', 'page', 'groups', 'group', 'games', 'game', 'saved', 'directory', 'products', 'product', 'market', 'admincp', 'admin', 'admins');
    $reserved_usernames = array('install', 'static', 'contact', 'contacts', 'sign', 'signin', 'login', 'signup', 'register', 'signout', 'logout', 'reset', 'activation', 'connect', 'revoke', 'packages', 'started', 'search', 'friends', 'messages', 'message', 'notifications', 'notification', 'settings', 'setting', 'posts', 'post', 'photos', 'photo', 'create', 'pages', 'page', 'groups', 'group', 'games', 'game', 'saved', 'directory', 'products', 'product', 'market', 'admincp', 'admin', 'admins', 'school', 'class', 'teacher');
    if (in_array(strtolower($username), $reserved_usernames)) {
        return true;
    } else {
        return false;
    }
}


/**
 * valid_name
 *
 * @param string $name
 * @return boolean
 */
function valid_name($name)
{
    $name = convert_vi_to_en($name);
    if (preg_match('/[\'^£$%&*()}{@#~?><>,|=+¬]/', $name)) {
        return false;
    } else {
        return true;
    }
}


/**
 * valid_location
 *
 * @param string $location
 * @return boolean
 */
function valid_location($location)
{
    if (preg_match("/^[\\p{L} \-,()0-9]+$/ui", $location)) {
        return true;
    } else {
        return false;
    }
}


/**
 * valid_extension
 *
 * @param string $extension
 * @param array $allowed_extensions
 * @return boolean
 */
function valid_extension($extension, $allowed_extensions)
{
    $extensions = explode(',', $allowed_extensions);
    foreach ($extensions as $key => $value) {
        $extensions[$key] = strtolower(trim($value));
    }
    if (is_array($extensions) && in_array($extension, $extensions)) {
        return true;
    }
    return false;
}


/* ------------------------------- */
/* Date */
/* ------------------------------- */

/**
 * set_datetime
 *
 * @param string $date
 * @return string
 */
function set_datetime($date)
{
    return date("Y-m-d H:i:s", strtotime($date));
}


/**
 * get_datetime
 *
 * @param string $date
 * @return string
 */
function get_datetime($date)
{
    return date("m/d/Y g:i A", strtotime($date));
}


/* ------------------------------- */
/* JSON */
/* ------------------------------- */

/**
 * _json_decode
 *
 * @param string $string
 * @return string
 */
function _json_decode($string)
{
    if (get_magic_quotes_gpc()) $string = stripslashes($string);
    return json_decode($string);
}


/**
 * return_json
 *
 * @param array $response
 * @return json
 */
function return_json($response = '')
{
    global $system;
    header('Content-Type: application/json');
    if ($system['is_mobile']) {
        exit(html2text(json_encode($response)));
    }
    exit(json_encode($response));
}


/* ------------------------------- */
/* Error */
/* ------------------------------- */

/**
 * _error
 *
 * @return void
 */
function _error()
{
    global $system;
    $args = func_get_args();
    if (!$system['is_mobile']) {
        if (count($args) > 1) {
            $title = $args[0];
            $message = $args[1];
        } else {
            switch ($args[0]) {
                case 'DB_ERROR':
                    $title = "Database Error";
                    $message = "<div class='text-left'><h1>" . "Error establishing a database connection" . "</h1>
                        <p>" . "This either means that the username and password information in your config.php file is incorrect or we can't contact the database server at localhost. This could mean your host's database server is down." . "</p>
                        <ul>
                            <li>" . "Are you sure you have the correct username and password?" . "</li>
                            <li>" . "Are you sure that you have typed the correct hostname?" . "</li>
                            <li>" . "Are you sure that the database server is running?" . "</li>
                        </ul>
                        <p>" . "If you're unsure what these terms mean you should probably contact your host. If you still need help you can always visit the" . "</p>
                        </div>";
                    break;

                case 'SQL_ERROR':
                    $title = __("Database Error");
                    $message = __("An error occurred while writing to database. Please try again later");
                    if (DEBUGGING) {
                        $backtrace = debug_backtrace();
                        $line = $backtrace[0]['line'];
                        $file = $backtrace[0]['file'];
                        $message .= "<br><br><small>This error function was called from line $line in file $file</small>";
                    }
                    break;

                case 'SQL_ERROR_THROWEN':
                    $message = __("An error occurred while writing to database. Please try again later");
                    if (DEBUGGING) {
                        $backtrace = debug_backtrace();
                        $line = $backtrace[0]['line'];
                        $file = $backtrace[0]['file'];
                        $message .= "<br><br><small>This error function was called from line $line in file $file</small>";
                    }
                    throw new Exception($message);
                    break;

                case '404':
                    header('HTTP/1.0 404 Not Found');
                    $title = __("404 Not Found");
                    $message = __("The requested URL was not found on this server. That's all we know");
                    if (DEBUGGING) {
                        $backtrace = debug_backtrace();
                        $line = $backtrace[0]['line'];
                        $file = $backtrace[0]['file'];
                        $message .= "<br><br><small>This error function was called from line $line in file $file</small>";
                    }
                    break;

                case '400':
                    header('HTTP/1.0 400 Bad Request');
                    if (DEBUGGING) {
                        $backtrace = debug_backtrace();
                        $line = $backtrace[0]['line'];
                        $file = $backtrace[0]['file'];
                        exit("This error function was called from line $line in file $file");
                    }
                    exit;

                case '403':
                    header('HTTP/1.0 403 Access Denied');
                    if (DEBUGGING) {
                        $backtrace = debug_backtrace();
                        $line = $backtrace[0]['line'];
                        $file = $backtrace[0]['file'];
                        exit("This error function was called from line $line in file $file");
                    }

                    $title = __("403 Access Denied");
                    $message = __("You don't have the right permission to access this");
                    $is_403 = 1;
                    break;

                default:
                    $title = __("Error");
                    $message = __("There is some thing went wrong");
                    if (DEBUGGING) {
                        $backtrace = debug_backtrace();
                        $line = $backtrace[0]['line'];
                        $file = $backtrace[0]['file'];
                        $message .= "<br><br>" . "<small>This error function was called from line $line in file $file</small>";
                    }
                    break;
            }
        }
        if (isset($is_403) && $is_403 == 1) {
            echo '<!DOCTYPE html>
            <html>
                <head>
                    <meta charset="utf-8">
                    <meta name="robots" content="noindex,nofollow">
                    <meta name="viewport" content="width=device-width,maximum-scale=1,user-scalable=no,minimal-ui">
                    <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">
                    <style>
                        body {
                            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
                        }
                        .error-content {
                            position: relative;
                        }
                        .error-content img {
                            max-width: 100%;
                        }
                        .content {
                            position: absolute;
                            top: 60%;
                            left: 0;
                            right: 0;
                            margin: auto;
                        }
                        .content p {
                            font-weight: bold;
                            font-size: 40px;
                            text-transform: uppercase;
                        }
                        .button-home {
                            color: #fff;
                            background: #67B05E;
                            font-size: 30px;
                            padding: 10px 15px;
                            border-radius: 5px;
                            text-decoration: none;
                        }
                    </style>
                    <title>	' . $title . '</title>
                </head>
                <body>
                    <div class="error-content">
                        <img src="' . $system['system_url'] . '/content/themes/' . $system['theme'] . '/images/403.jpg">
                        <div class="content" align="center">
                            <p>' . $message . '</p>
                            <a href="' . $system['system_url'] . '" class="button-home">' . __("Home") . '</a>
                        </div>
                    </div>
                </body>
            </html>';
        } else {
            echo '<!DOCTYPE html>
            <html>
                <head>
                    <meta charset="utf-8">
                    <meta name="robots" content="noindex,nofollow">
                    <meta name="viewport" content="width=device-width,maximum-scale=1,user-scalable=no,minimal-ui">
                    <style>
                        body {
                            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
                        }
                        .error-content {
                            position: relative;
                        }
                        .error-content img {
                            max-width: 100%;
                        }
                        .content {
                            position: absolute;
                            top: 60%;
                            left: 0;
                            right: 0;
                            margin: auto;
                        }
                        .content p {
                            font-weight: bold;
                            font-size: 40px;
                            text-transform: uppercase;
                        }
                        .button-home {
                            color: #fff;
                            background: #67B05E;
                            font-size: 30px;
                            padding: 10px 15px;
                            border-radius: 5px;
                            text-decoration: none;
                        }
                    </style>
                    <title>	' . $title . '</title>
                </head>
                <body>
                    <div class="error-content">
                        <img src="' . $system['system_url'] . '/content/themes/' . $system['theme'] . '/images/404.jpg">
                        <div class="content" align="center">
                            <p>' . $message . '</p>
                            <a href="' . $system['system_url'] . '" class="button-home">' . __("Home") . '</a>
                        </div>
                    </div>
                </body>
            </html>';
        }
        exit;

    } else {
        $code = 0;
        if (count($args) > 1) {
            $title = $args[0];
            $message = $args[1];
        } else {
            switch ($args[0]) {
                case 'DB_ERROR':
                    $code = 0;
                    $message = __("Database Error");
                    break;

                case 'SQL_ERROR':
                    $code = 0;
                    $message = __("An error occurred while writing to database. Please try again later");
                    break;

                case 'SQL_ERROR_THROWEN':
                    throw new Exception(__("An error occurred while writing to database. Please try again later"));
                    break;

                case '404':
                    $code = 404;
                    $message = __("The requested URL was not found on this server. That's all we know");
                    break;

                case '400':
                    $code = 400;
                    $message = __("The requested is incorrect. Please try again later");
                    break;
                case '403':
                    $code = 403;
                    $message = __("You have no permission to do this");
                    break;
                default:
                    $code = 0;
                    $message = __("There is something that went wrong!");
                    break;
            }
        }
        header('HTTP/1.0 400 API Error');
        return_json(array(
            'code' => $code,
            'message' => $message,
            'data' => null
        ));
    }

}


/**
 * _api_error
 *
 * @return void
 */
function _api_error()
{
    $args = func_get_args();
    if (count($args) > 1) {
        $code = $args[0];
        $message = $args[1];
    } else {
        switch ($args[0]) {
            case 'DB_ERROR':
                $code = 0;
                $message = __("Database Error");
                break;

            case 'SQL_ERROR':
                $code = 0;
                $message = __("An error occurred while writing to database. Please try again later");
                break;

            case 'SQL_ERROR_THROWEN':
                throw new Exception(__("An error occurred while writing to database. Please try again later"));
                break;

            case '404':
                $code = 404;
                $message = __("The requested URL was not found on this server. That's all we know");
                break;

            case '400':
                $code = 400;
                $message = __("The requested is incorrect. Please try again later");
                break;
            case '403':
                $code = 403;
                $message = __("You have no permission to do this");
                break;
            default:
                $code = 0;
                $message = __("There is something that went wrong!");
                break;
        }
    }
    header('HTTP/1.0 400 API Error');
    return_json(array(
        'code' => $code,
        'message' => $message,
        'data' => null
    ));
}


/* ------------------------------- */
/* Email */
/* ------------------------------- */

/**
 * _email
 *
 * @param string $email
 * @param string $subject
 * @param string $body
 * @param boolean $is_html
 * @param boolean $only_smtp
 * @return boolean
 */
function _email($email, $subject, $body, $is_html = false, $only_smtp = false)
{
    global $system;
    /* set header */
    $header = "MIME-Version: 1.0\r\n";
    $header .= "Mailer: " . $system['system_title'] . "\r\n";
    if ($system['system_email']) {
        $header .= "From: " . $system['system_email'] . "\r\n";
        $header .= "Reply-To: " . $system['system_email'] . "\r\n";
    }
    if ($is_html) {
        $header .= "Content-Type: text/html; charset=\"utf-8\"\r\n";
    } else {
        $header .= "Content-Type: text/plain; charset=\"utf-8\"\r\n";
    }
    /* send email */
    if ($system['email_smtp_enabled']) {
        /* SMTP */
        require_once(ABSPATH . 'includes/libs/PHPMailer/PHPMailer.php');
        require_once(ABSPATH . 'includes/libs/PHPMailer/SMTP.php');
        require_once(ABSPATH . 'includes/libs/PHPMailer/Exception.php');
        $mail = new PHPMailer\PHPMailer\PHPMailer;
        $mail->CharSet = "UTF-8";
        $mail->isSMTP();
        $mail->Host = $system['email_smtp_server'];
        $mail->SMTPAuth = ($system['email_smtp_authentication']) ? true : false;
        $mail->Username = $system['email_smtp_username'];
        $mail->Password = $system['email_smtp_password'];
        $mail->SMTPSecure = ($system['email_smtp_ssl']) ? 'ssl' : 'tls';
        $mail->Port = $system['email_smtp_port'];
        $setfrom = (is_empty($system['email_smtp_setfrom'])) ? $system['email_smtp_username'] : $system['email_smtp_setfrom'];
        $mail->setFrom($setfrom, $system['system_title']);
        $mail->addAddress($email);
        $mail->Subject = $subject;
        if ($is_html) {
            $mail->isHTML(true);
            $mail->AltBody = strip_tags($body);
        }
        $mail->Body = $body;
        if (!$mail->send()) {
            if ($only_smtp) {
                return false;
            }
            /* send using mail() */
            if (!mail($email, $subject, $body, $header)) {
                return false;
            }
        }
    } else {
        if ($only_smtp) {
            return false;
        }
        /* send using mail() */
        if (!mail($email, $subject, $body, $header)) {
            return false;
        }
    }
    return true;
}


/**
 * email_smtp_test
 *
 * @return void
 */
function email_smtp_test()
{
    global $system;
    /* prepare test email */
    $subject = __("Test SMTP Connection on") . " " . $system['system_title'];
    $body = __("This is a test email");
    $body .= "\r\n\r" . $system['system_title'] . " " . __("Team");
    /* send email */
    if (!_email($system['system_email'], $subject, $body, false, true)) {
        throw new Exception(__("Test email could not be sent. Please check your settings"));
    }
}

/* ------------------------------- */
/* SMS */
/* ------------------------------- */

/**
 * sms_send
 *
 * @param string $phone
 * @param string $message
 * @return boolean
 */
function sms_send($phone, $message)
{
    global $system;
    require_once(ABSPATH . 'includes/libs/Twilio/autoload.php');
    $client = new Twilio\Rest\Client($system['twilio_sid'], $system['twilio_token']);
    $message = $client->account->messages->create(
        $phone,
        array(
            'from' => $system['twilio_phone'],
            'body' => $message
        )
    );
    if (!$message->sid) {
        return false;
    }
    return true;
}

/* ------------------------------- */
/* User Access */
/* ------------------------------- */

/**
 * user_access
 *
 * @param boolean $is_ajax
 * @return void
 */
function user_access($is_ajax = false)
{
    global $user, $system;
    if ($is_ajax) {
        /* check user logged in */
        if (!$user->_logged_in) {
            modal(LOGIN);
        }
        /* check user activated */
        if ($system['activation_enabled'] && !$user->_data['user_activated']) {
            modal(MESSAGE, __("Not Activated"), __("Before you can interact with other users, you need to confirm your email address"));
        }
    } else {
        if (!$user->_logged_in) {
            user_login();
        } else {

            /* check registration type */
            /*if($system['registration_type'] == "paid" && $user->_data['user_group'] > '1' && !$user->_data['user_subscribed']) {
                redirect('/packages');
            }*/
            /* check if getted started */

            /* Check thêm điều kiện chưa đổi pass hoặc nhập đủ thông tin */
            if (($system['getting_started'] && !$user->_data['user_started'])) {
                redirect('/started');
            }
            /*elseif(is_empty($user->_data['user_birthdate']) || is_empty($user->_data['user_id_card_number']) || is_empty($user->_data['date_of_issue']) || is_empty($user->_data['place_of_issue']) ) {
                redirect('/started/?step=2');
            } elseif((($user->_data['facebook_connected'] || $user->_data['google_connected']) && !$user->_data['user_changed_pass']) ) {
                redirect('/started/?step=3');
            }*/
        }
    }
}


/**
 * user_login
 *
 * @return void
 */
function user_login()
{
    global $smarty;
    $smarty->assign('highlight', __("You must sign in to see this page"));
    page_header(__("Sign in"));
    page_footer('signin');
    exit;
}


/* ------------------------------- */
/* Modal */
/* ------------------------------- */

/**
 * modal
 *
 * @return json
 */
function modal()
{
    $args = func_get_args();
    switch ($args[0]) {
        case 'LOGIN':
            return_json(array("callback" => "modal('#modal-login')"));
            break;
        case 'MESSAGE':
            return_json(array("callback" => "modal('#modal-message', {title: '" . $args[1] . "', message: '" . addslashes($args[2]) . "'})"));
            break;
        case 'ERROR':
            return_json(array("callback" => "modal('#modal-error', {title: '" . $args[1] . "', message: '" . addslashes($args[2]) . "'})"));
            break;
        case 'SUCCESS':
            return_json(array("callback" => "modal('#modal-success', {title: '" . $args[1] . "', message: '" . addslashes($args[2]) . "'})"));
            break;
        default:
            if (isset($args[1])) {
                return_json(array("callback" => "modal('" . $args[0] . "', " . $args[1] . ")"));
            } else {
                return_json(array("callback" => "modal('" . $args[0] . "')"));
            }
            break;
    }
}


/* ------------------------------- */
/* Popover */
/* ------------------------------- */

/**
 * popover
 *
 * @param integer $uid
 * @param string $username
 * @param string $name
 * @return string
 */
function popover($uid, $username, $name)
{
    global $system;
    $popover = '<span class="js_user-popover" data-uid="' . $uid . '"><a href="' . $system['system_url'] . '/' . $username . '">' . $name . '</a></span>';
    return $popover;
}

/* ------------------------------- */
/* Pages */
/* ------------------------------- */

/**
 * page_header
 *
 * @param string $title
 * @param string $description
 * @return void
 */
function page_header($title, $description = '', $image = '')
{
    global $smarty, $system;
    $description = ($description != '') ? $description : $system['system_description'];
    if ($image == '') {
        if ($system['system_ogimage']) {
            $image = $system['system_uploads'] . '/' . $system['system_ogimage'];
        } else {
            $image = $system['system_url'] . '/content/themes/' . $system['theme'] . '/images/og-image.jpg';
        }
    }
    $smarty->assign('page_title', $title);
    $smarty->assign('page_description', $description);
    $smarty->assign('page_image', $image);
}


/**
 * page_footer
 *
 * @param string $page
 * @return void
 */
function page_footer($page)
{
    global $smarty;
    $smarty->assign('page', $page);
    $smarty->display("$page.tpl");
}


/* ------------------------------- */
/* Text */
/* ------------------------------- */

/**
 * decode_urls
 *
 * @param string $text
 * @return string
 */
function decode_urls($text)
{
    $text = preg_replace('/(https?:\/\/[^\s]+)/', "<a target='_blank' href=\"$1\">$1</a>", $text);
    return $text;
}


/**
 * decode_hashtag
 *
 * @param string $text
 * @return string
 */
function decode_hashtag($text)
{
    global $system;
    $pattern = '/(#|\x{ff03}){1}([0-9_\p{L}]*[_\p{L}][0-9_\p{L}]*)/u';
    $text = preg_replace($pattern, '<a href="' . $system['system_url'] . '/search/hashtag/$2">$0</a>', $text);
    return $text;
}


/**
 * decode_mention
 *
 * @param string $text
 * @return string
 */
function decode_mention($text)
{
    global $user;
    $text = preg_replace_callback('/\[([a-z0-9._]+)\]/i', array($user, 'get_mentions'), $text);
    return $text;
}


/**
 * decode_text
 *
 * @param string $string
 * @return string
 */
function decode_text($string)
{
    return base64_decode($string);
}

/* ------------------------------- */
/* Censored Words */
/* ------------------------------- */

/**
 * censored_words
 *
 * @param string $text
 * @return string
 */
function censored_words($text)
{
    global $system;
    if ($system['censored_words_enabled']) {
        $bad_words = explode(',', $system['censored_words']);
        if (count($bad_words) > 0) {
            foreach ($bad_words as $word) {
                $pattern = '/' . $word . '/i';
                $text_new = explode(' ', $text);
                if (in_array($word, $text_new)) {
                    $text = preg_replace($pattern, str_repeat('*', strlen($word)), $text);
                }
            }
        }
    }
    return $text;
}


/* ------------------------------- */
/* General */
/* ------------------------------- */

/**
 * get_ip
 *
 * @return string
 */
function get_user_ip()
{
    /* handle CloudFlare IP addresses */
    return (isset($_SERVER["HTTP_CF_CONNECTING_IP"]) ? $_SERVER["HTTP_CF_CONNECTING_IP"] : $_SERVER['REMOTE_ADDR']);
}


/**
 * get_os
 *
 * @return string
 */
function get_user_os()
{
    $os_platform = "Unknown OS Platform";
    $os_array = array(
        '/windows nt 10/i' => 'Windows 10',
        '/windows nt 6.3/i' => 'Windows 8.1',
        '/windows nt 6.2/i' => 'Windows 8',
        '/windows nt 6.1/i' => 'Windows 7',
        '/windows nt 6.0/i' => 'Windows Vista',
        '/windows nt 5.2/i' => 'Windows Server 2003/XP x64',
        '/windows nt 5.1/i' => 'Windows XP',
        '/windows xp/i' => 'Windows XP',
        '/windows nt 5.0/i' => 'Windows 2000',
        '/windows me/i' => 'Windows ME',
        '/win98/i' => 'Windows 98',
        '/win95/i' => 'Windows 95',
        '/win16/i' => 'Windows 3.11',
        '/macintosh|mac os x/i' => 'Mac OS X',
        '/mac_powerpc/i' => 'Mac OS 9',
        '/linux/i' => 'Linux',
        '/ubuntu/i' => 'Ubuntu',
        '/iphone/i' => 'iPhone',
        '/ipod/i' => 'iPod',
        '/ipad/i' => 'iPad',
        '/android/i' => 'Android',
        '/blackberry/i' => 'BlackBerry',
        '/webos/i' => 'Mobile'
    );
    foreach ($os_array as $regex => $value) {
        if (preg_match($regex, $_SERVER['HTTP_USER_AGENT'])) {
            $os_platform = $value;
        }
    }
    return $os_platform;
}


/**
 * get_browser
 *
 * @return string
 */
function get_user_browser()
{
    $browser = "Unknown Browser";
    $browser_array = array(
        '/msie/i' => 'Internet Explorer',
        '/firefox/i' => 'Firefox',
        '/safari/i' => 'Safari',
        '/chrome/i' => 'Chrome',
        '/edge/i' => 'Edge',
        '/opera/i' => 'Opera',
        '/netscape/i' => 'Netscape',
        '/maxthon/i' => 'Maxthon',
        '/konqueror/i' => 'Konqueror',
        '/mobile/i' => 'Handheld Browser'
    );
    foreach ($browser_array as $regex => $value) {
        if (preg_match($regex, $_SERVER['HTTP_USER_AGENT'])) {
            $browser = $value;
        }
    }
    return $browser;
}

/**
 * get_extension
 *
 * @param string $path
 * @return string
 */
function get_extension($path)
{
    return strtolower(pathinfo($path, PATHINFO_EXTENSION));
}

/**
 * ger_origenal_url
 *
 * @param string $url
 * @return string
 */
function ger_origenal_url($url)
{
    stream_context_set_default(array(
        'http' => array(
            'ignore_errors' => true,
            'method' => 'HEAD',
            'user_agent' => @$_SERVER['HTTP_USER_AGENT'],
        )
    ));
    $headers = get_headers($url, 1);
    if ($headers !== false && (isset($headers['location']) || isset($headers['Location']))) {
        $location = (isset($headers['location'])) ? $headers['location'] : $headers['Location'];
        return is_array($location) ? array_pop($location) : $location;
    }
    return $url;
}

/**
 * get_youtube_id
 *
 * @param string $url
 * @return string
 */
function get_youtube_id($url)
{
    preg_match('/youtube\.com\/embed\/([^\&\?\/]+)/', $url, $id);
    return $id[1];
}

/**
 * get_array_key
 *
 * @param array $array
 * @param integer $current
 * @param integer $offset
 * @return mixed
 */
function get_array_key($array, $current, $offset = 1)
{
    $keys = array_keys($array);
    $index = array_search($current, $keys);
    if (isset($keys[$index + $offset])) {
        return $keys[$index + $offset];
    }
    return false;
}


/**
 * get_firstname
 *
 * @param string $fullname
 * @return string
 */
function get_firstname($fullname)
{
    $name = explode(" ", $fullname);
    return $name[0];
}

// uses regex that accepts any word character or hyphen in last name
function split_name($name)
{
    $name = trim($name);
    //$last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
    //$first_name = trim( preg_replace('#'.$last_name.'#', '', $name ) );

    $names = explode(" ", $name);

    $cnt = count($names);
    $first_name = ($cnt > 0) ? $names[$cnt - 1] : "";
    $last_name = "";
    for ($i = 0; $i < $cnt - 1; $i++) {
        $last_name = $last_name . " " . $names[$i];
    }
    $last_name = trim($last_name);

    return array($first_name, $last_name);
}

/* ------------------------------- */
/* CONIU - BEGIN */
/* ------------------------------- */

function getNotificationMessage($notification)
{
    switch ($notification['action']) {
        case NOTIFICATION_ASSIGN_CLASS:
            include_once(DAO_PATH . 'dao_class.php');
            $classDao = new ClassDAO();
            //$class = $classDao->getClass($notification['node_url']);
            $class = getClassData($notification['node_url'], CLASS_INFO);
            return convertText4Web($class['group_title']) . " " . __("is assigned to you");
        case NOTIFICATION_NEW_CLASS:
            return $notification['extra3'] . " " . __("added class") . " " . $notification['extra1'];
        case NOTIFICATION_UPDATE_CLASS:
            return $notification['extra3'] . " " . __("updated class") . " " . $notification['extra1'];
        case NOTIFICATION_NEW_CLASSLEVEL:
            return $notification['extra3'] . " " . __("added class level") . " " . $notification['extra1'];
        case NOTIFICATION_UPDATE_CLASSLEVEL:
            return $notification['extra3'] . " " . __("updated class level") . " " . $notification['extra1'];
        case NOTIFICATION_NEW_CHILD:
            break;
        case NOTIFICATION_UPDATE_CHILD_TEACHER:
            return __("Required confirm edit information of student") . " " . $notification['extra1'];
        case NOTIFICATION_NEW_CHILD_TEACHER:
            return __("Added student") . " " . $notification['extra1'] . " " . __("into class") . " " . $notification['extra3'];
            break;
        case NOTIFICATION_CONFIRM_CHILD_TEACHER:
            return __("Confirmed edit information of student") . " " . $notification['extra1'];
            break;
        case NOTIFICATION_UNCONFIRM_CHILD_TEACHER:
            return __("Unconfirmed edit information of student") . " " . $notification['extra1'];
            break;
        case NOTIFICATION_NEW_TEACHER:
            return $notification['extra1'] . " " . __("has new teacher");
        case NOTIFICATION_NEW_EVENT_SCHOOL:
        case NOTIFICATION_NEW_EVENT_CLASS_LEVEL:
        case NOTIFICATION_NEW_EVENT_CLASS:
            return __("Created event") . ": " . $notification['extra1'];
        case NOTIFICATION_UPDATE_EVENT_SCHOOL:
        case NOTIFICATION_UPDATE_EVENT_CLASS_LEVEL:
        case NOTIFICATION_UPDATE_EVENT_CLASS:
            return __("Updated event") . ": " . $notification['extra1'];
        case NOTIFICATION_CANCEL_EVENT_SCHOOL:
        case NOTIFICATION_CANCEL_EVENT_CLASS_LEVEL:
        case NOTIFICATION_CANCEL_EVENT_CLASS:
            return __("Cancel event") . ": " . $notification['extra1'];
        case NOTIFICATION_REGISTER_EVENT_PARENT:
        case NOTIFICATION_REGISTER_EVENT_CHILD:
            return __("Register event") . " " . $notification['extra1'] . __(" for ") . $notification['extra3'];
        case NOTIFICATION_CANCEL_REGISTRATION_EVENT_PARENT:
        case NOTIFICATION_CANCEL_REGISTRATION_EVENT_CHILD:
            return __("Cancel registration") . " " . $notification['extra1'] . __(" for ") . $notification['extra3'];
        case NOTIFICATION_NEW_MEDICINE:
            return __("New medicine for") . " " . $notification['extra3'];
        case NOTIFICATION_UPDATE_MEDICINE:
            return __("Update medicine for") . " " . $notification['extra3'];
        case NOTIFICATION_USE_MEDICINE:
            return $notification['extra3'] . ": " . __("has been given medicine");
        case NOTIFICATION_CONFIRM_MEDICINE:
            return $notification['extra3'] . ": " . __("Teacher has confirmed the medicine");
        case NOTIFICATION_CANCEL_MEDICINE:
            return __("Cancel medicine for") . " " . $notification['extra3'];
        case NOTIFICATION_NEW_TUITION:
            include_once(DAO_PATH . 'dao_tuition.php');
            $tuitionDao = new TuitionDAO();
            if ($notification['node_type'] == NOTIFICATION_NODE_TYPE_CHILD) {
                $tuitionChild = $tuitionDao->getTuitionChildById($notification['node_url']);
                if (!is_null($tuitionChild)) {
                    return sprintf(__("%s tuition of %s is %s"), $tuitionChild['month'], $tuitionChild['child_name'], moneyFormat($tuitionChild['total_amount'])) . " " . MONEY_UNIT;
                }
                return __("New tuition is created");
            } else if ($notification['node_type'] == NOTIFICATION_NODE_TYPE_SCHOOL) {
                return sprintf(__("created tuition %s for %s"), $notification['extra3'], $notification['extra1']);
            }
            break;
        case NOTIFICATION_UPDATE_TUITION:
            include_once(DAO_PATH . 'dao_tuition.php');
            $tuitionDao = new TuitionDAO();
            if ($notification['node_type'] == NOTIFICATION_NODE_TYPE_CHILD) {
                $tuitionChild = $tuitionDao->getTuitionChildById($notification['node_url']);
                if (!is_null($tuitionChild)) {
                    return sprintf(__("%s tuition of %s is %s"), $tuitionChild['month'], convertText4Web($tuitionChild['child_name']), moneyFormat($tuitionChild['total_amount'])) . " " . MONEY_UNIT;
                }
                return __("New tuition is created");
            } else if ($notification['node_type'] == NOTIFICATION_NODE_TYPE_SCHOOL) {
                return sprintf(__("updated tuition %s for %s"), $notification['extra3'], $notification['extra1']);
            }
            break;
        case NOTIFICATION_CONFIRM_TUITION:
            if ($notification['node_type'] == NOTIFICATION_NODE_TYPE_SCHOOL) {
                return sprintf(__("Parent %s's tuition payment requirement in %s"), $notification['extra3'], $notification['extra1']);
            } else if ($notification['node_type'] == NOTIFICATION_NODE_TYPE_CHILD) {
                return __("Your payment has been confirmed");
            } else {
                return sprintf(__("%s's tuition have been paid in %s"), $notification['extra3'], $notification['extra1']);
            }
            break;
        case NOTIFICATION_CONFIRM_TUITION_SCHOOL:
            if ($notification['node_type'] == NOTIFICATION_NODE_TYPE_SCHOOL) {
                return sprintf(__("%s's tuition have been paid in %s"), $notification['extra3'], $notification['extra1']);
            }
            break;
        case NOTIFICATION_UNCONFIRM_TUITION:
            if ($notification['node_type'] == NOTIFICATION_NODE_TYPE_SCHOOL) {
                return __("Has not confirmed the def's tuition payment") . " " . $notification['extra3'];
            } else if ($notification['node_type'] == NOTIFICATION_NODE_TYPE_CHILD) {
                return __("Your payment has been unconfirmed");
            }
            break;
        case NOTIFICATION_ATTENDANCE_RESIGN:
            include_once(DAO_PATH . 'dao_attendance.php');
            $attendanceDao = new AttendanceDAO();
            $attendanceDetail = $attendanceDao->getAttendanceDetailById($notification['node_url']);
            if (!is_null($attendanceDetail)) {
                if (isset($attendanceDetail['start_date']) && $attendanceDetail['start_date'] != $attendanceDetail['end_date'])
                    return sprintf(__("%s has resigned from date %s to date %s"), convertText4Web($attendanceDetail['child_name']), $attendanceDetail['start_date'], $attendanceDetail['end_date']);
                else
                    return sprintf(__("%s has resigned the date %s"), convertText4Web($attendanceDetail['child_name']), $attendanceDetail['attendance_date']);
            }
            return __("One child has resigned");
        case NOTIFICATION_ATTENDANCE_CONFIRM:
            return __("Your resign has been confirmed");
        case NOTIFICATION_NEW_REPORT:
            if ($notification['node_type'] == NOTIFICATION_NODE_TYPE_SCHOOL) {
                return __("Contact book") . ": " . __("Created") . " " . $notification['extra1'] . " " . __("for") . ' ' . $notification['extra3'];
            } else if ($notification['node_type'] == NOTIFICATION_NODE_TYPE_CHILD) {
                return __("Contact book") . ": " . __("Created") . " " . $notification['extra1'] . ' ' . __("for") . ' ' . $notification['extra3'];
            }
            break;
        case NOTIFICATION_UPDATE_REPORT:
            if ($notification['node_type'] == NOTIFICATION_NODE_TYPE_SCHOOL) {
                return __("Contact book") . ": " . __("Updated") . " " . $notification['extra1'] . ' ' . __("for") . ' ' . $notification['extra3'];
            } else if ($notification['node_type'] == NOTIFICATION_NODE_TYPE_CHILD) {
                return __("Contact book") . ": " . __("Updated") . " " . $notification['extra1'] . ' ' . __("for") . ' ' . $notification['extra3'];
            }
            break;
        case NOTIFICATION_NEW_POINT:
            if ($notification['node_type'] == NOTIFICATION_NODE_TYPE_SCHOOL) {
                return __("Point") . ": " . __("Created") . " " . $notification['extra1'] . " score " . __("for") . ' ' . $notification['extra3'];
            } else if ($notification['node_type'] == NOTIFICATION_NODE_TYPE_CHILD) {
                return __("Point") . ": " . __("Created") . " " . $notification['extra1'] . ' score ' . __("for") . ' ' . $notification['extra3'];
            }
            break;
        case NOTIFICATION_NEW_REPORT_TEMPLATE:
            if ($notification['node_type'] == NOTIFICATION_NODE_TYPE_CLASS) {
                return $notification['extra1'] . ' ' . __("added contact book template") . ' ' . $notification['extra3'];
            }
            break;
        case NOTIFICATION_NEW_FEEDBACK:
            return __("Has given a feedback to school") . ' ' . $notification['extra3'];
        case NOTIFICATION_SERVICE_REGISTER_UNCOUNTBASED:
        case NOTIFICATION_SERVICE_REGISTER_COUNTBASED:
            return __("Change register service") . " " . $notification['extra1'] . __(" for ") . $notification['extra3'];
        // Notify đăng ký dịch vụ theo số lần sử dụng từ class
        case NOTIFICATION_SERVICE_REGISTER_COUNTBASED_MOBILE:
            include_once(DAO_PATH . 'dao_class.php');
            $classDao = new ClassDAO();
            $class = $classDao->getClass($notification['node_url']);
            return __("Change register service") . __(" for ") . convertText4Web($class['group_title']) . ' ' . __("day") . ' ' . $notification['extra3'];
//        case NOTIFICATION_SERVICE_REGISTER_COUNTBASED_CLASS:
//            include_once(DAO_PATH.'dao_service.php');
//            $serviceDao = new ServiceDAO();
//            include_once(DAO_PATH.'dao_class.php');
//            $classDao = new ClassDAO();
//            $service = $serviceDao->getServiceById($notification['node_url']);
//            $class = $classDao->getClass($notification['extra1']);
//            return __("Change register service") . ' ' . convertText4Web($service['service_name']).__(" for ").convertText4Web($class['group_title']) . ' ' . __("day") . ' ' . $notification['extra3'];
        case NOTIFICATION_SERVICE_CANCEL_UNCOUNTBASED:
        case NOTIFICATION_SERVICE_CANCEL_COUNTBASED:
            return __("Cancel service") . " " . $notification['extra1'] . __(" for ") . $notification['extra3'];
        case NOTIFICATION_CONFIRM_FEEDBACK:
            include_once(DAO_PATH . 'dao_feedback.php');
            $feedbackDao = new FeedbackDAO();
            $feedback = $feedbackDao->getFeedback($notification['node_url']);
            if ($feedback['level'] == SCHOOL_LEVEL)
                if ($notification['node_type'] == NOTIFICATION_NODE_TYPE_SCHOOL) {
                    return __("Confirmed feedback");
                } else {
                    return __("The school has received your feedback, thank you.");
                }
            if ($feedback['level'] == CLASS_LEVEL)
                return __("Lớp đã nhận được góp ý, xin cảm ơn quý phụ huynh.");
            break;
        case NOTIFICATION_UPDATE_PICKER:
//            include_once(DAO_PATH.'dao_child.php');
//            $childDao = new ChildDAO();
//            $information = $childDao->getChild($notification['node_url'], false);
//            return __("Update picker for")." ".$information['child_name'];
            return __("Update picker for") . " " . $notification['extra3'];
            break;
        case NOTIFICATION_CONFIRM_PICKER:
            return __("Paid student") . " " . $notification['extra3'];
            break;
        case NOTIFICATION_NEW_SCHEDULE:
            include_once(DAO_PATH . 'dao_schedule.php');
            $scheduleDao = new ScheduleDAO();
            include_once(DAO_PATH . 'dao_school.php');
            $schoolDao = new SchoolDAO();
            $schedule = $scheduleDao->getScheduleDetailById($notification['node_url']);
            //$school = $schoolDao->getSchoolById($schedule['school_id']);
            $school = getSchoolData($schedule['school_id'], SCHOOL_INFO);
            return convertText4Web($school['page_title']) . ' ' . __("created schedule") . ' ' . $notification['extra1'] . ' ' . __("for") . ' ' . $notification['extra3'];
            break;
        case NOTIFICATION_UPDATE_SCHEDULE:
            include_once(DAO_PATH . 'dao_schedule.php');
            $scheduleDao = new ScheduleDAO();
            include_once(DAO_PATH . 'dao_school.php');
            $schoolDao = new SchoolDAO();
            $schedule = $scheduleDao->getScheduleDetailById($notification['node_url']);
            //$school = $schoolDao->getSchoolById($schedule['school_id']);
            $school = getSchoolData($schedule['school_id'], SCHOOL_INFO);
            return convertText4Web($school['page_title']) . ' ' . __("updated schedule") . ' ' . convertText4Web($schedule['schedule_name']) . ' ' . __("for") . ' ' . $notification['extra3'];
            break;
        case NOTIFICATION_NEW_MENU:
            include_once(DAO_PATH . 'dao_menu.php');
            $menuDao = new MenuDAO();
            include_once(DAO_PATH . 'dao_school.php');
            $schoolDao = new SchoolDAO();
            $menu = $menuDao->getMenuDetailById($notification['node_url']);
            //$school = $schoolDao->getSchoolById($menu['school_id']);
            $school = getSchoolData($menu['school_id'], SCHOOL_INFO);
            return convertText4Web($school['page_title']) . ' ' . __("created menu") . ' ' . $notification['extra1'] . ' ' . __("for") . ' ' . $notification['extra3'];
            break;
        case NOTIFICATION_UPDATE_MENU:
            include_once(DAO_PATH . 'dao_menu.php');
            $menuDao = new MenuDAO();
            include_once(DAO_PATH . 'dao_school.php');
            $schoolDao = new SchoolDAO();
            $menu = $menuDao->getMenuDetailById($notification['node_url']);
            //$school = $schoolDao->getSchoolById($menu['school_id']);
            $school = getSchoolData($menu['school_id'], SCHOOL_INFO);
            return convertText4Web($school['page_title']) . ' ' . __("updated menu") . ' ' . $menu['menu_name'] . ' ' . __("for") . ' ' . $notification['extra3'];
            break;

        case NOTIFICATION_UPDATE_ROLE:
            return __("You has been update permission for school") . ': ' . $notification['extra3'];

        case NOTIFICATION_ADD_LATEPICKUP_CLASS:
            return __("Trẻ") . ' ' . $notification['extra3'] . __(" được thêm vào lớp đón muộn ngày " . $notification['extra1']);
        case NOTIFICATION_REMOVE_LATEPICKUP_CLASS:
            return __("Trẻ") . ' ' . $notification['extra3'] . __(" được hủy khỏi lớp đón muộn ngày " . $notification['extra1']);
        case NOTIFICATION_REGISTER_LATEPICKUP:
            return __("Trẻ") . ' ' . $notification['extra3'] . __(" đăng ký đón muộn ngày " . $notification['extra1']);
        case NOTIFICATION_UPDATE_LATEPICKUP_INFO:
            return __("Trẻ") . ' ' . $notification['extra3'] . __(" được cập nhật thông tin đón muộn ngày " . $notification['extra1']);
        case NOTIFICATION_ASSIGN_LATEPICKUP_CLASS:
            return __("Bạn được phân công vào lớp đón muộn") . ' ' . $notification['extra3'] . ' ' . __("ngày") . ' ' . $notification['extra1'];
        case NOTIFICATION_CHILD_PICKEDUP:
            return __("Trẻ") . ' ' . $notification['extra3'] . __(" được trả khỏi lớp đón muộn lúc " . $notification['extra1']);
        case NOTIFICATION_FOETUS_DEVELOPMENT:
            return $notification['extra1'];
        case NOTIFICATION_CHILD_DEVELOPMENT:
            return $notification['extra1'];
        case NOTIFICATION_UPDATE_CHILD_SCHOOL:
            return $notification['extra3'] . ' ' . __("updated information student") . ' ' . $notification['extra1'];

        case NOTIFICATION_NEW_CHILD_EXIST:
            return $notification['extra1'] . ' ' . __("added in to school") . ' ' . $notification['extra3'];

        case NOTIFICATION_NEW_CHILD_HEALTH:
            return __("Added health information for") . ' ' . $notification['extra3'] . ' ' . __("day") . ' ' . $notification['extra1'];

        case NOTIFICATION_UPDATE_CHILD_HEALTH:
            return __("Updated health information for") . ' ' . $notification['extra3'] . ' ' . __("day") . ' ' . $notification['extra1'];
        case NOTIFICATION_NEW_CHILD_EDIT_BY_TEACHER:
            return $notification['extra1'] . ' ' . __("have student updated information by the teacher this week");
            break;

        case NOTIFICATION_LEAVE_SCHOOL_BY_PARENT:
            return __("Child") . ' ' . $notification['extra3'] . ' ' . __("has been asked leave of") . ' ' . $notification['extra1'];
            break;

        case NOTIFICATION_MUST_UPDATE_INFORMATION:
            return __("Hãy nhập đầy đủ thông tin để Coniu hỗ trợ bạn tốt hơn");
            break;

        case NOTIFICATION_CONIU_INFO:
            return $notification['extra1'];
            break;

        case NOTIFICATION_CONIU_CONGRATULATIONS:
            return $notification['extra1'];
            break;

        case NOTIFICATION_REMIND_INPUT_CHILD_HEALTH:
            return __("Hãy nhập thông tin sức khỏe của trẻ, để Coniu chăm sóc tốt hơn");
            break;

        case NOTIFICATION_REMIND_INPUT_FOETUS_INFO:
            return __("Hãy nhập thông tin sức khỏe thai nhi, để Coniu chăm sóc tốt hơn");
            break;

        case NOTIFICATION_INFORMATION_INTERESTED:
            return __("Có thể bạn quan tâm") . ': ' . $notification['extra1'];
            break;
        case NOTIFICATION_UPDATE_INFO:
            return __("We could not identify your email or phone number, please enter your correct email and phone number.");
            break;
        case NOTIFICATION_ADD_DIARY_SCHOOL:
            return __("Child") . ' ' . $notification['extra3'] . ' ' . __("added an image album") . ' ' . convertText4Web($notification['extra1']);
            break;
        case NOTIFICATION_EDIT_CAPTION_DIARY_SCHOOL:
            return sprintf(__("Album %s was changed title"), convertText4Web($notification['extra1']));
            break;
        case NOTIFICATION_DELETE_PHOTO_DIARY_SCHOOL:
            return sprintf(__("Deleted a photo in album %s"), convertText4Web($notification['extra1']));
            break;
        case NOTIFICATION_ADD_PHOTO_DIARY_SCHOOL:
            return sprintf(__("Added photos in album %s"), convertText4Web($notification['extra1']));
            break;
        case NOTIFICATION_NEW_ATTENDANCE:
        case NOTIFICATION_UPDATE_ATTENDANCE:
            if ($notification['node_url'] == ATTENDANCE_ABSENCE || $notification['node_url'] == ATTENDANCE_ABSENCE_NO_REASON) {
                return sprintf(__("%s is absence on %s"), convertText4Web($notification['extra3']), $notification['extra1']);
            } else {
                return sprintf(__("%s was attendance on %s"), convertText4Web($notification['extra3']), $notification['extra1']);
            }
            break;
        case NOTIFICATION_NEW_ATTENDANCE_BACK:
        case NOTIFICATION_UPDATE_ATTENDANCE_BACK:
            if ($notification['extra1'] == '') {
                return sprintf(__("%s came back"), convertText4Web($notification['extra3']));
            } else {
                return sprintf(__("%s came back on %s"), convertText4Web($notification['extra3']), $notification['extra1']);
            }

            break;
        case NOTIFICATION_REMIND_ATTENDANCE:
            if ($notification['node_type'] == NOTIFICATION_NODE_TYPE_SCHOOL) {
                return $notification['extra1'];
            } else {
                return sprintf(__("%s class has not been attendance on %s"), $notification['extra3'], $notification['extra1']);
            }
            break;
        case NOTIFICATION_REMIND_SCHEDULE:
            if ($notification['node_type'] == NOTIFICATION_NODE_TYPE_SCHOOL) {
                return $notification['extra1'];
            } elseif ($notification['node_type'] == NOTIFICATION_NODE_TYPE_CLASS) {
                return sprintf(__("%s class has not schedules in this week"), $notification['extra3']);
            } else {
                if ($notification['node_url'] == 0) {
                    return sprintf(__("%s has not schedules in this week"), $notification['extra3']);
                } else {
                    return sprintf(__("%s's study schedules on %s"), $notification['extra3'], $notification['extra1']);
                }
            }
            break;
        case NOTIFICATION_REMIND_MENU:
            if ($notification['node_type'] == NOTIFICATION_NODE_TYPE_SCHOOL) {
                return $notification['extra1'];
            } elseif ($notification['node_type'] == NOTIFICATION_NODE_TYPE_CLASS) {
                return sprintf(__("%s class has not menus in this week"), $notification['extra3']);
            } else {
                if ($notification['node_url'] > 0) {
                    return sprintf(__("%s's study menus on %s"), $notification['extra3'], $notification['extra1']);
                } else {
                    return sprintf(__("%s has not menus in this week"), $notification['extra3']);
                }
            }
            break;
        case NOTIFICATION_REMIND_REPORT:
            if ($notification['node_type'] == NOTIFICATION_NODE_TYPE_SCHOOL) {
                return $notification['extra1'];
            } else {
                return $notification['extra1'];
            }
            break;
    }
}

/**
 * Gửi thông báo đến quẩn lý trường và tất cả giáo viên của trẻ.
 * LƯU Ý CÁC TRƯỜNG TRUYỀN VÀO.
 * CHỈ DÙNG TRONG PHẦN QUẢN LÝ CỦA PHỤ HUYNH
 *
 * Hàm này hình như hiện tại không dùng đến nữa
 *
 * @param $childId
 * @param $action
 * @param string $node_url
 * @param string $extra1
 * @param string $extra3 : Nếu không truyền vào extra3 thì lấy giá trị mặc định là child_name
 */
function notifySchoolManagerAndTearcher($childId, $action, $node_url = '', $extra1 = '', $extra3 = '')
{
    include_once(DAO_PATH . 'dao_child.php');
    include_once(DAO_PATH . 'dao_user.php');
    $childDao = new ChildDAO();
    $userDao = new UserDAO();

    //$childInfo = $childDao->getFullInfoOfChild($childId);
    //$child = $childInfo['child'];
    //$school = $childInfo['school'];
    //$class = $childInfo['class'];
    //$teachers = $childInfo['teacher'];
    $child = getChildData($childId, CHILD_INFO);
    $school = getChildData($childId, CHILD_SCHOOL);
    $class = getChildData($childId, CHILD_CLASS);
    $teachers = getClassData($child['class_id'], CLASS_TEACHERS);

    //Nếu không truyền vào extra3 thì lấy giá trị mặc định là child_name
    $extra3 = ($extra3 != '') ? $extra3 : convertText4Web($child['child_name']);

    $userDao->postNotifications($school['page_admin'], $action, NOTIFICATION_NODE_TYPE_SCHOOL,
        $node_url, $extra1, $school['page_name'], $extra3);

    //Thông báo giáo viên của lớp
    if (count($teachers) > 0) {
        $teacherIds = array();
        foreach ($teachers as $teacher) {
            $teacherIds[] = $teacher['user_id'];
        }
        $userDao->postNotifications($teacherIds, $action, NOTIFICATION_NODE_TYPE_CLASS,
            $node_url, $extra1, $class['group_name'], $extra3);
    }
}

/**
 * Gửi thông báo đến quẩn lý trường và tất cả giáo viên của trẻ theo $view
 * @param $childId
 * @param $action
 * @param string $node_url
 * @param string $extra1
 * @param string $extra3
 */
function notifySchoolManagerAndTearcherOfView($childId, $action, $node_url = '', $extra1 = '', $extra3 = '', $view)
{
    include_once(DAO_PATH . 'dao_child.php');
    include_once(DAO_PATH . 'dao_user.php');
    include_once(DAO_PATH . 'dao_school.php');
    $childDao = new ChildDAO();
    $userDao = new UserDAO();
    $schoolDao = new SchoolDAO();

    //$childInfo = $childDao->getFullInfoOfChild($childId);
    //$child = $childInfo['child'];
    //$school = $childInfo['school'];
    //$class = $childInfo['class'];
    //$teachers = $childInfo['teacher'];

    $child = getChildData($childId, CHILD_INFO);
    $school = getChildData($childId, CHILD_SCHOOL);
    $class = getChildData($childId, CHILD_CLASS);
    $teachers = getClassData($child['class_id'], CLASS_TEACHERS);
    // Lấy cấu hình thông báo của những user quản lý trường
    //$notify_settings = $schoolDao->getUserNotificationSettingOfSchool($school['page_id']);
    $notify_settings = getSchoolData($school['page_id'], SCHOOL_NOTIFICATIONS);

    //Nếu không truyền vào extra3 thì lấy giá trị mặc định là child_name
    $extra3 = ($extra3 != '') ? $extra3 : convertText4Web($child['child_name']);

    // Lấy id của những user quản lý được nhận thông báo
    $userIds = getUserIdsReceiveNotifyOfSchoolForChild($notify_settings, $view, $school['page_id'], $school['page_admin']);

    $userDao->postNotifications($userIds, $action, NOTIFICATION_NODE_TYPE_SCHOOL,
        $node_url, $extra1, $school['page_name'], $extra3);

    //Thông báo giáo viên của lớp
    if (count($teachers) > 0) {
        $teacherIds = array();
        foreach ($teachers as $teacher) {
            $teacherIds[] = $teacher['user_id'];
        }

        $userDao->postNotifications($teacherIds, $action, NOTIFICATION_NODE_TYPE_CLASS,
            $node_url, $extra1, $class['group_name'], $extra3);
    }
}

/**
 * Gửi thông báo đến quản lý trường theo $view
 * @param $childId
 * @param $action
 * @param string $node_url
 * @param string $extra1
 * @param string $extra3
 */
function notifySchoolManagerOfView($childId, $action, $node_url = '', $extra1 = '', $extra3 = '', $view)
{
    include_once(DAO_PATH . 'dao_child.php');
    include_once(DAO_PATH . 'dao_user.php');
    include_once(DAO_PATH . 'dao_school.php');
    $childDao = new ChildDAO();
    $userDao = new UserDAO();
    $schoolDao = new SchoolDAO();

//    $childInfo = $childDao->getFullInfoOfChild($childId);
//    $child = $childInfo['child'];
//    $school = $childInfo['school'];
    //$class = $childInfo['class'];
    //$teachers = $childInfo['teacher'];
    $child = getChildData($childId, CHILD_INFO);
    $school = getChildData($childId, CHILD_SCHOOL);

    //$class = getChildData($childId, CHILD_CLASS);
    //$teachers = getClassData($child['class_id'], CLASS_TEACHERS);
    // Lấy cấu hình thông báo của những user quản lý trường
    // $notify_settings = $schoolDao->getUserNotificationSettingOfSchool($school['page_id']);
    $notify_settings = getSchoolData($school['page_id'], SCHOOL_NOTIFICATIONS);

    //Nếu không truyền vào extra3 thì lấy giá trị mặc định là child_name
    $extra3 = ($extra3 != '') ? $extra3 : convertText4Web($child['child_name']);

    // Lấy id của những user quản lý được nhận thông báo
    $userIds = getUserIdsReceiveNotifyOfSchoolForChild($notify_settings, $view, $school['page_id'], $school['page_admin']);

    $userDao->postNotifications($userIds, $action, NOTIFICATION_NODE_TYPE_SCHOOL,
        $node_url, $extra1, $school['page_name'], $extra3);
}

/**
 * Lấy ra các đối tượng liên quan đến user hiện tại. Bao gồm:
 * - Các trường có liên quan (là admin, là giáo viên, là nhân viên).
 * - Các lớp được phân giảng dạy.
 * - Trẻ: mà mình là phụ huynh.
 */
function getRelatedObjects()
{
    global $user, $relatedObjects;

    /*$key = "related_objects_".$user->_data['user_id'];
    $objects = $_SESSION[$key];
    if (is_null($objects)) {
        include_once(DAO_PATH . 'dao_school.php');
        include_once(DAO_PATH . 'dao_class.php');
        include_once(DAO_PATH . 'dao_child.php');
        include_once(DAO_PATH . 'dao_role.php');
        $schoolDao = new SchoolDAO();
        $classDao = new ClassDAO();
        $childDao = new ChildDAO();
        $roleDao = new RoleDao();

        $objects = array();
        $schools = $schoolDao->getSchoolsHaveRole($user->_data['user_id']);
        //Load tất cả quyền của user với từng trường.
        $newSchools = array();
        foreach ($schools as $school) {
            $newSchool = $school;
            if ($school['role_id'] == -1) {
                $newSchool['is_admin'] = true;
                $newSchool['is_teacher'] = false;
            } else if($school['role_id'] == 0) {
                $newSchool['is_admin'] = false;
                $newSchool['is_teacher'] = true;
            } else {
                $newSchool['is_admin'] = false;
                $newSchool['is_teacher'] = false;
                $newSchool['permissions'] = $roleDao->getPermissionOfSchool($school['page_id'], $user->_data['user_id']);
            }
            $newSchools[] = $newSchool;
        }
        $objects['schools'] = $newSchools;

        $classes = $classDao->getClasses($user->_data['user_id']);
        $objects['classes'] = $classes;
        $children = $childDao->getChildren($user->_data['user_id']);
        $objects['children'] = $children;

        $_SESSION[$key] = $objects;
    }*/

    //Lấy ra danh sách schools, classes, children mà user quản lý
    $relatedObjects = getUserManageData($user->_data['user_id']);
    return $relatedObjects;
}

function getRelatedObjectsOnUserId($user_id)
{
    global $relatedObjects;


    //Lấy ra danh sách schools, classes, children mà user quản lý
    $relatedObjects = getUserManageData($user_id);
    return $relatedObjects;
}

/**
 * Kiểm tra user hiện tại có quản lý một đối tượng nào ko?
 */
function isManager()
{
    global $user;
    include_once(DAO_PATH . 'dao_school.php');
    include_once(DAO_PATH . 'dao_class.php');
    include_once(DAO_PATH . 'dao_child.php');
    $schoolDao = new SchoolDAO();
    $classDao = new ClassDAO();
    $childDao = new ChildDAO();

    $schools = $schoolDao->getSchoolsHaveRole($user->_data['user_id']);
    $classes = $classDao->getClasses($user->_data['user_id']);
    $children = $childDao->getChildren($user->_data['user_id']);
    return ((count($schools) + count($classes) + count($$children)) > 0);
}

function unsetRelatedObjects()
{
    global $user;
    unset($_SESSION["related_objects_" . $user->_data['user_id']]);
}

/**
 * Kiểm tra user hiện tại có vai trò gì với trường không.
 *
 * @param $schoolName
 * @return bool
 */
function hasRoleInSchool($schoolName)
{
    global $relatedObjects;
    if (is_null($relatedObjects)) {
        $relatedObjects = getRelatedObjects();
    }
    foreach ($relatedObjects['schools'] as $school) {
        if ($school['page_name'] == $schoolName) {
            return true;
        }
    }
    return false;
}

/**
 * Lấy ra thông tin trường của user hiện tại theo school_name
 *
 * @param $schoolName
 * @return bool
 */
function getSchool($schoolName)
{
    global $relatedObjects;
    if (is_null($relatedObjects)) {
        $relatedObjects = getRelatedObjects();
    }
    foreach ($relatedObjects['schools'] as $school) {
        if ($school['page_name'] == $schoolName) {
            return $school;
        }
    }

    //Check 2 thông tin kia nếu true cả, thì lấy thông tin trường theo $username
    //Nếu false thì chạy như dưới
    if ($_SESSION['isGovManager'] && in_array($schoolName, $_SESSION['schools'])) {
        // Lấy thông tin trường theo username
        include_once(DAO_PATH . 'dao_school.php');
        $schoolDao = new SchoolDAO();
        $school_id = $schoolDao->getSchoolIdByUsername($schoolName);
        $result = getSchoolData($school_id, SCHOOL_DATA);

        return $result;
    }

    _error(403);
}

/**
 * Kiểm tra user hiện tại có quyền gì với hệ thống.
 *
 * @param $schoolName
 * @param $module
 * @return int
 */
function checkSchoolPermission($schoolName, $module)
{
    global $relatedObjects, $user;
    if (is_null($relatedObjects)) {
        $relatedObjects = getRelatedObjects();
    }
    foreach ($relatedObjects['schools'] as $school) {
        if (!is_empty($school['principal']) && $school['page_name'] == $schoolName) {
            $principal = explode(',', $school['principal']);
            if (in_array($user->_data['user_id'], $principal)) {
                return PERM_EDIT;
            }
        }
        if ($school['page_name'] == $schoolName) {
            if ($school['is_admin']) {
                //Nếu là admin thì bất kể module gì cũng toàn quyền.
                return PERM_EDIT;
            } else if ($school['is_teacher']) {
                //Nếu chỉ là giáo viên đơn thuần, chỉ xem được thông báo.
//                if ($module == 'events') {
//                    return PERM_VIEW;
//                }
                return PERM_NONE;
            } else {
                //Nếu không phải giáo viên và admin.
                foreach ($school['permissions'] as $permission) {
                    if ($permission['module'] == $module) {
                        if ($module != 'settings') {
                            return $permission['value'];
                        } else {
                            if ($permission['value'] == 0) {
                                return PERM_VIEW;
                            } else {
                                return $permission['value'];
                            }
                        }
                    }
                }
                return PERM_NONE;
            }
        }
    }
    return PERM_NONE;
}

/**
 * Kiểm tra user hiện tại có phải là admin của trường không.
 *
 * @param $schoolName
 * @return bool
 */
function isAdmin($schoolName)
{
    global $relatedObjects;
    if (is_null($relatedObjects)) {
        $relatedObjects = getRelatedObjects();
    }
    foreach ($relatedObjects['schools'] as $school) {
        if ($school['page_name'] == $schoolName) {
            return $school['is_admin'];
        }
    }
    return false;
}

/**
 * Kiểm tra user hiện tại có quyền sửa module không
 *
 * @param $schoolName
 * @param $module
 * @return bool
 */
function canEdit($schoolName, $module)
{
    return (PERM_EDIT == checkSchoolPermission($schoolName, $module));
}

/**
 * Kiểm tra user hiện tại có quyền view module không ( bên giao diện hiệu trưởng) .
 *
 * @param $schoolName
 * @param $module
 * @return bool
 */
function canView($schoolName, $module)
{
    if (PERM_VIEW <= checkSchoolPermission($schoolName, $module)) {
        return true;
    } else {
        if ($_SESSION['isGovManager'] && in_array($schoolName, $_SESSION['schools'])) {
            return true;
        } else {
            return false;
        }
    }
}
/**
 * Kiểm tra user hiện tại có quyền view module không ( bên class ).
 *
 * @param $user_id
 * @param $class_id
 * @return bool
 */
function canView_class($user_id, $class_id)
{
    include_once(DAO_PATH . 'dao_teacher.php');
    $teacherDao = new TeacherDAO();
    $homeroom_teacher = $teacherDao->getHomeroomTeacher($class_id,$user_id);
    if(isset($homeroom_teacher)) {
       return true;
    }
   return false;
}
/**
 * Lấy ra vai trò của user hiện tại với đối tượng
 * @param $object_id
 * @param $object_type
 * @return int
 */
function get_role($object_id, $object_type)
{
    global $user;
    include_once(DAO_PATH . 'dao_user_manage.php');
    $userManageDao = new UserManageDAO();

    return $userManageDao->getRole($user->_data['user_id'], $object_id, $object_type);
}

/**
 * Kiểm tra xem user login và activated chưa.
 */
function check_login()
{
    global $user, $system;
    if (!$user->_logged_in) {
        modal(LOGIN);
    }

    // check user activated
    if ($system['activation_enabled'] && !$user->_data['user_activated']) {
        modal(MESSAGE, __("Not Activated"), __("Before you can interact with other users, you need to confirm your email address"));
    }
}

/**
 * Chuyển đổi tiếng Việt có dấu thành không dấu
 *
 * @param $str
 * @return mixed
 */
function convert_vi_to_en($str)
{
    $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
    $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
    $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
    $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
    $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
    $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
    $str = preg_replace("/(đ)/", 'd', $str);
    $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
    $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
    $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
    $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
    $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
    $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
    $str = preg_replace("/(Đ)/", 'D', $str);
    //$str = str_replace(" ", "-", str_replace("&*#39;”,”",$str));
    return $str;
}

/**
 * Convert một string tiếng Việt thành tiếng Anh. CHỈ DÙNG CHO MỤC ĐÍNH SẮP XẾP
 *
 * @param $str
 * @return mixed
 */
function convert_to_en_4sort($str)
{
    $map = array(
        'Ă' => 'Az',
        'Ằ' => 'Azz',
        'Ắ' => 'Azzz',
        'Ẳ' => 'Azzzz',
        'Ẵ' => 'Azzzzz',
        'Ặ' => 'Azzzzzz',
        'Â' => 'Azzzzzzz',
        'Ầ' => 'Azzzzzzz',
        'Ấ' => 'Azzzzzzzz',
        'Ẩ' => 'Azzzzzzzzz',
        'Ẫ' => 'Azzzzzzzzzz',
        'Ậ' => 'Azzzzzzzzzzz',
        'ă' => 'az',
        'ằ' => 'azz',
        'ắ' => 'azzz',
        'ẳ' => 'azzzz',
        'ẵ' => 'azzzzz',
        'ặ' => 'azzzzzz',
        'â' => 'azzzzzzz',
        'ầ' => 'azzzzzzzz',
        'ấ' => 'azzzzzzzzz',
        'ẩ' => 'azzzzzzzzzz',
        'ẫ' => 'azzzzzzzzzzz',
        'ậ' => 'azzzzzzzzzzzz',
        'Đ' => 'Dz',
        'đ' => 'dz',
        'Ê' => 'Ez',
        'Ề' => 'Ezz',
        'Ế' => 'Ezzz',
        'Ể' => 'Ezzzz',
        'Ễ' => 'Ezzzzz',
        'Ệ' => 'Ezzzzzz',
        'ê' => 'ezzzzzzz',
        'ề' => 'ezzzzzzzz',
        'ê' => 'ezzzzzzzzz',
        'ể' => 'ezzzzzzzzzz',
        'ễ' => 'ezzzzzzzzzzz',
        'ệ' => 'ezzzzzzzzzzzz',
        'Ô' => 'Oz',
        'Ồ' => 'Ozz',
        'Ố' => 'Ozzz',
        'Ổ' => 'Ozzzz',
        'Ỗ' => 'Ozzzzz',
        'Ộ' => 'Ozzzzzz',
        'Ơ' => 'Ozzzzzzz',
        'Ờ' => 'Ozzzzzzzz',
        'Ớ' => 'Ozzzzzzzzz',
        'Ở' => 'Ozzzzzzzzz',
        'Ỡ' => 'Ozzzzzzzzzz',
        'Ợ' => 'Ozzzzzzzzzzz',
        'ô' => 'oz',
        'ồ' => 'ozz',
        'ố' => 'ozzz',
        'ổ' => 'ozzzz',
        'ỗ' => 'ozzzzz',
        'ộ' => 'ozzzzzz',
        'ơ' => 'ozzzzzzz',
        'ờ' => 'ozzzzzzzz',
        'ớ' => 'ozzzzzzzzz',
        'ở' => 'ozzzzzzzzzz',
        'ỡ' => 'ozzzzzzzzzzz',
        'ợ' => 'ozzzzzzzzzzzz',
        'Ư' => 'Uz',
        'Ừ' => 'Uzz',
        'Ứ' => 'Uzzz',
        'Ử' => 'Uzzzz',
        'Ữ' => 'Uzzzzz',
        'Ự' => 'Uzzzzzz',
        'ư' => 'uz',
        'ừ' => 'uzz',
        'ứ' => 'uzzz',
        'ử' => 'uzzzz',
        'ữ' => 'uzzzzz',
        'ự' => 'uzzzzzz'
    );
    $keys = array_keys($map);
    $vals = array_values($map);
    return str_replace($keys, $vals, $str);
}

/* ------------------------------- */
/* Picture & Cover */
/* ------------------------------- */

/**
 * get_picture
 *
 * @param string $picture
 * @param string $type
 * @return string
 */
function get_picture($picture, $type)
{
    global $system;
    if ($picture == "") {
        switch ($type) {
            case MALE:
                $picture = $system['system_url'] . '/content/themes/' . $system['theme'] . '/images/blank_profile_male.jpg';
                break;

            case FEMALE:
                $picture = $system['system_url'] . '/content/themes/' . $system['theme'] . '/images/blank_profile_female.jpg';
                break;

            case 'school':
                $picture = $system['system_url'] . '/content/themes/' . $system['theme'] . '/images/blank_page.png';
                break;

            case 'class':
                $picture = $system['system_url'] . '/content/themes/' . $system['theme'] . '/images/blank_group.png';
                break;

            case 'page':
                $picture = $system['system_url'] . '/content/themes/' . $system['theme'] . '/images/blank_page.png';
                break;

            case 'group':
                $picture = $system['system_url'] . '/content/themes/' . $system['theme'] . '/images/blank_group.png';
                break;

            case 'game':
                $picture = $system['system_url'] . '/content/themes/' . $system['theme'] . '/images/blank_game.png';
                break;
        }
    } else {
        $picture = $system['system_uploads'] . '/' . $picture;
    }
    return $picture;
}

function get_picture_size($picture, $width = '')
{
    global $system;

    $arrCheck = ['blank_profile_male', 'blank_profile_female', 'blank_profile_other', 'blank_page', 'blank_group',
        'blank_event', 'blank_game', 'blank_package', 'blank_article'
    ];

    foreach ($arrCheck as $check) {
        if (strpos($picture, $check) !== false) {
            $picture = str_replace("/images/", "/images/small/", $picture);
            return $picture;
        }
    }

    if ($width) {
        $path = '_' . $width;
        $pi = pathinfo($picture);
        $fileName = $pi['filename'];
        $fileExt = $pi['extension'];
        $newPath = $fileName . $path . '.' . $fileExt;
        $picture = $system['system_uploads'] . '/' . $newPath;
    } else {
        $picture = $system['system_uploads'] . '/' . $picture;
    }

    return $picture;
}


/* ------------------------------- */
/* Upload */
/* ------------------------------- */

/**
 * upload
 *
 * @param $files
 * @param string $type
 * @param string $folder
 * @param string $depth
 * @return string
 */
function upload($files, $type, $folder)
{
    global $system;
    include_once(ABSPATH . 'includes/class-image.php');

    $depth = ABSPATH;
    $return = array();
    switch ($type) {
        case 'images':
            // check file upload enabled
            if (!$system['photos_enabled']) {
                throw new Exception(__('This feature has been disabled'));
            }

            // check file size
            $max_allowed_size = $system['max_photo_size'] * 1024;

            /* check & create uploads dir */
            if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder)) {
                @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder, 0777, true);
            }
            if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'))) {
                @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'), 0777, true);
            }
            if (!file_exists($system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'))) {
                @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'), 0777, true);
            }
            $files_num = count($files['name']);

            if ($files_num > 1) {
                $arrFiles = array();
                foreach ($files as $key => $val) {
                    for ($i = 0; $i < count($val); $i++) {
                        $arrFiles[$i][$key] = $val[$i];
                    }
                }

                //$files_num = count($files);
                foreach ($arrFiles as $file) {

                    // valid inputs
                    if (!isset($file) || $file["error"] != UPLOAD_ERR_OK) {
                        if ($files_num > 1) {
                            continue;
                        } else {
                            throw new Exception(__("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                        }
                    }

                    // check file size
                    if ($file["size"] > $max_allowed_size) {
                        if ($files_num > 1) {
                            continue;
                        } else {
                            throw new Exception(__("The file size is so big"));
                        }
                    }

                    /* prepare new file name */
                    $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
                    $prefix = $system['uploads_prefix'] . '_' . get_hash_token();
                    $image = new Image($file["tmp_name"]);
                    $image_tmp_name = $directory . $prefix . '_tmp' . $image->_img_ext;
                    $image_new_name = $directory . $prefix . $image->_img_ext;
                    $path_tmp = $depth . $system['system_uploads_directory'] . '/' . $image_tmp_name;
                    $path_new = $depth . $system['system_uploads_directory'] . '/' . $image_new_name;

                    /* check if the file uploaded successfully */
                    if (!@move_uploaded_file($file['tmp_name'], $path_tmp)) {
                        if ($files_num > 1) {
                            continue;
                        } else {
                            throw new Exception(__("Sorry, can not upload the file"));
                        }
                    }

                    /* save the new image */
                    $image->save($path_new, $path_tmp);

                    /* delete the tmp image */
                    unlink($path_tmp);

                    /* return */
                    $return[] = $image_new_name;
                }

            } else {
                // valid inputs
                if ($files["error"] != UPLOAD_ERR_OK) {
                    throw new Exception(__("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                }

                // check file size
                if ($files["size"] > $max_allowed_size) {
                    throw new Exception(__("The file size is so big"));
                }

                /* prepare new file name */
                $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'] . '_' . get_hash_token();
                $image = new Image($files["tmp_name"]);
                $image_tmp_name = $directory . $prefix . '_tmp' . $image->_img_ext;
                $image_new_name = $directory . $prefix . $image->_img_ext;
                $path_tmp = $depth . $system['system_uploads_directory'] . '/' . $image_tmp_name;
                $path_new = $depth . $system['system_uploads_directory'] . '/' . $image_new_name;

                /* check if the file uploaded successfully */
                if (!@move_uploaded_file($files['tmp_name'], $path_tmp)) {
                    throw new Exception(__("Sorry, can not upload the file"));
                }

                /* save the new image */
                $image->save($path_new, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);
                $return = $image_new_name;
            }

            break;
    }
    return $return;
}

/**
 * Convert kiểu ngày tháng trong DB thành ngày tháng hệ thống
 *
 * @param $dbDate
 * @return bool|string
 */
function toSysDate($dbDate)
{
    global $system;

    $date = strtotime($dbDate);
    return ($date ? date($system['date_format'], $date) : "");
}

function toSysDatetime($dbDate)
{
    global $system;

    $date = strtotime($dbDate);
    return ($date ? date($system['datetime_format'], $date) : "");
}

/**
 * Chuyển kiểu ngày tháng thành kiểu MM/YYYY
 *
 * @param $dbDate
 * @return bool|string
 */
function toSysMMYYYY($dbDate)
{
    global $system;

    if (($dbDate == null) || (trim($dbDate) == '')) return '';
    if (strtotime($dbDate) < strtotime("1970-01-01")) return '';

    $date = strtotime($dbDate);
    return ($date ? date($system['month_format'], $date) : "");
}

/**
 * Chuyển kiểu ngày tháng thành kiểu HH:mm
 *
 * @param $dbDate
 * @return bool|string
 */
function toSysTime($dbTime)
{
    global $system;

    $time = strtotime($dbTime);
    return ($time ? date($system['time_format'], $time) : "");
}

/**
 * Convert kiểu ngày tháng của hệ thống sang kiểu lưu trong DB
 *
 * @param $sysDate
 * @return bool|string
 */
function toDBTime($sysTime)
{
    global $system;

    if (!isset($sysTime) || $sysTime == '') {
        return '';
    }

    $date = DateTime::createFromFormat($system['time_format'], $sysTime);
    if (!$date) return '';

    return $date->format('H:i:s');
}

/**
 * Convert kiểu ngày tháng của hệ thống sang kiểu lưu trong DB
 *
 * @param $sysDate
 * @return bool|string
 */
function toDBDate($sysDate)
{
    global $system;

    if (!isset($sysDate) || $sysDate == '') {
        return '';
    }

    $date = DateTime::createFromFormat($system['date_format'], $sysDate);
    if (!$date) return '';

    return $date->format('Y-m-d');
}

function toSysDDMM($dbDate)
{
    global $system;

    $date = strtotime($dbDate);
    return ($date ? date($system['day_format'], $date) : "");
}

/**
 * Cộng 1 tháng vào date kiểu DBDate
 *
 * @param $dbDate
 * @return string
 */
function addMonthToDBDate($dbDate)
{
    return (new DateTime($dbDate))->add(new DateInterval('P1M'))->format('Y-m-d');
}

/**
 * Tìm ra tháng trước của 1 tháng. Tháng truyền vào có dạng MM/yyyy.
 *
 * @param $month
 * @return string
 */
function getPreMonth($month)
{
    global $system;
    $date = DateTime::createFromFormat($system['date_format'], '01/' . $month);
    $date->modify('-1 month');
    return $date->format($system['month_format']);
}

/**
 * Convert kiểu datetime của hệ thống sang kiểu lưu trong DB
 *
 * @param $sysDate
 * @return bool|string
 */
function toDBDatetime($sysDate)
{
    global $system;

    if (!isset($sysDate) || $sysDate == '') {
        return '';
    }

    $date = DateTime::createFromFormat($system['datetime_format'], $sysDate);
    if (!$date) return '';

    return $date->format('Y-m-d H:i:s');
}

function validateDate($date)
{
    global $system;
    $arrs = explode("/", $date);
    $cond1 = checkdate($arrs[1], $arrs[0], $arrs[2]);
    if ($cond1 && DateTime::createFromFormat($system['date_format'], $date)) {
        return true;
    }
    return false;
}

function validateDateTime($datetime)
{
    global $system;
    $d = DateTime::createFromFormat($system['datetime_format'], $datetime);
    return $d && ($d->format($system['datetime_format']) == $datetime);
}

/**
 * Lưu file log chương trình
 * @param $message
 */
function cilog($message)
{
    $strMsg = $_SERVER['REMOTE_ADDR'] . '-' . date("F j, Y, g:i:s a") . ": " . $message . '---------\n';
    error_log($strMsg, 3, CI_LOG_FILE);
}

/**
 * Sinh ra mã trẻ từ tên của trẻ đó.
 *
 * @param $name
 * @return mixed
 */
function generateCode($name)
{
    $arrWords = explode(" ", $name);
    $code = "";
    foreach ($arrWords as $word) {
        $code = $code . substr(trim(convert_vi_to_en($word)), 0, 1);
    }
    $code = $code . time();
    return strtoupper($code);
}

/**
 * Tự sinh ra username cho user
 *
 * @param $name
 * @return string
 */
function generateUsername($name)
{
    $arrWords = explode(" ", $name);
    $code = "";
    foreach ($arrWords as $word) {
        $code = $code . substr(trim(convert_vi_to_en($word)), 0, 1);
    }
    $time = time() + rand(1, 99);
    $code = $code . $time;
    return strtolower($code);
}

function generateEmail()
{
    $time = time() + rand(1, 99);
    $code = $time . '@inet.vn';
    return strtolower($code);
}

/**
 * Lấy ra thời gian hiện tại
 *
 * @param $format
 * @return $date
 */
function getCurrentTime($format)
{
    $minutes_to_add = 0;
    $DateTime = new DateTime();
    $DateTime->add(new DateInterval('PT' . $minutes_to_add . 'M'));
    $date = $DateTime->format($format);
//
    return $date;
}

/**
 * Chuẩn hóa số điện thoại mà người dùng nhập vào
 *
 * @param $phone
 * @return string
 */
function standardizePhone($phone)
{
    $standardPhone = "";
    if (is_empty($phone) || is_empty(trim($phone))) {
        return $standardPhone;
    }

    $phone_array = (str_split($phone));
    for ($i = 0; $i < count($phone_array); $i++) {
        if (is_numeric($phone_array[$i])) {
            $standardPhone = $standardPhone . $phone_array[$i];
        }
    }
    if (substr($standardPhone, 0, 3) == '840') {
        $standardPhone = "0" . substr($standardPhone, 3);
    }

    if (substr($standardPhone, 0, 2) == '84') {
        $standardPhone = "0" . substr($standardPhone, 2);
    }

    return $standardPhone;
}

/**
 * Kiểm tra số điện thoại sau khi đã chuẩn hóa
 *
 * @param $phone
 * @return boolean
 */
function validatePhone($phone)
{
    if (is_empty($phone) || preg_match("/^(0|)(1\d{9}|9\d{8})$/", $phone)) {
        return true;
    }
    return false;
}

/**
 * Trả về 1 mảng các số cũ và được chuyển đổi (nếu có)
 *
 * @param $phone
 * @return array
 */
function getArrayPhone($phone)
{

    $firstOldPhone = substr($phone, 0, 4);
    $firstNewPhone = substr($phone, 0, 3);

    $arrOldPhone = array('0162', '0163', '0164', '0165', '0166', '0167', '0168', '0169',
        '0120', '0121', '0122', '0126', '0128',
        '0123', '0124', '0125', '0127', '0129', '0199', '0186', '0188');
    $arrNewPhone = array('032', '033', '034', '035', '036', '037', '038', '039',
        '070', '079', '077', '076', '078',
        '083', '084', '085', '081', '082', '059', '056', '058');

    $return[] = $phone;
    if (($i = array_search($firstOldPhone, $arrOldPhone)) !== FALSE) {
        $return[] = $arrNewPhone[$i] . substr($phone, 4);
    } elseif (($j = array_search($firstNewPhone, $arrNewPhone)) !== FALSE) {
        $return[] = $arrOldPhone[$j] . substr($phone, 3);
    }

    return $return;
}

/**
 * Mobile convert html về định dạng plain text
 *
 * @param $html
 * @return mixed
 */
function html2text($html)
{
    $html_decode = html_entity_decode(
        trim(strip_tags($html)),
        ENT_NOQUOTES,
        'UTF-8'
    );
    $html_replace = str_replace("&quot;", "''", $html_decode);
    $text = str_replace("&#039;", "'", $html_replace);

    return $text;
}

/**
 * Web convert html về định dạng plain text
 *
 * @param $html
 * @return mixed
 */
function convertText4Web($html)
{
    return html_entity_decode(
        trim(strip_tags(preg_replace('/<(head|title|style|script)[^>]*>.*?<\/\\1>/si', '', $html))),
        ENT_QUOTES,
        'UTF-8'
    );
}

/**
 * Trả về số tiền theo định dạng tiền tệ.
 *
 * @param $money
 * @return string
 */
function moneyFormat($money)
{
    return number_format($money, 0, '.', ',');
}

/**
 * Trả về định dạng số từ tiền tệ
 *
 * @param $money
 * @return string
 */
function convertMoneyToNumber($money)
{
    return str_replace(',', '', $money);
}

/**
 * Khởi tạo firebase
 *
 * @return
 */
function firebase()
{
    //Firebase
    require_once(ABSPATH . 'includes/libs/FirebaseAdmin/autoload.php');
    $firebase = Firebase::fromServiceAccount(ABSPATH . FIREBASE_ADMIN_KEY);
    $db_firebase = $firebase->getDatabase();
    return $db_firebase;
}

function push_notification($token, $data, $message, $badge = 1)
{
    //$target = 'single tocken id or topic name';
    //or
    //$target = array('token1','token2','...'); // up to 1000 in one request

    //FCM api URL
    $url = 'https://fcm.googleapis.com/fcm/send';
    //Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
    $server_key = GOOGLE_API_KEY;

    $fields = array();
    $fields['notification'] = array(
        //'title' => 'Coniu',
        'body' => $message,
        'badge' => $badge,
        'sound' => 'default'
    );
    $fields['content_available'] = true;
    $fields['priority'] = 'high';
    $fields['data'] = $data;

    if (is_array($token)) {
        $fields['registration_ids'] = $token;
    } else {
        $fields['to'] = $token;
    }
    //header with content_type api key
    $headers = array(
        'Content-Type:application/json',
        'Authorization:key=' . $server_key
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    curl_close($ch);

    return $result;
}

function styleArray($name, $bold, $italic, $size, $horizontal, $vertical, $wrap)
{
    $styleArray = array(
        'font' => array(
            'name' => $name,
            'bold' => $bold,
            'italic' => $italic,
            'size' => $size),
        'alignment' => array(
            'horizontal' => $horizontal,
            'vertical' => $vertical,
            'wrap' => $wrap
        )
    );
    return $styleArray;
}

function addDotString($strNum)
{

    $len = strlen($strNum);
    $counter = 3;
    $result = "";
    while ($len - $counter >= 0) {
        $con = substr($strNum, $len - $counter, 3);
        $result = ',' . $con . $result;
        $counter += 3;
    }
    $con = substr($strNum, 0, 3 - ($counter - $len));
    $result = $con . $result;
    if (substr($result, 0, 1) == ',') {
        $result = substr($result, 1, $len + 1);
    }
    return $result;
}

function getHeightCell($text)
{
    $cnt = strlen($text);
    $line = $cnt / 90;
    $line = (int)$line;
    $height = ($line + 3) * 15;

    return $height;
}

/**
 * Lấy danh sách user nhận được thông báo theo view và school_id từ giáo viên
 * @param array $notify_settings
 * @param $view
 * @param $school_id
 */
//function getUserIdsReceiveNotifyOfSchoolForClass($notify_settings, $view, $school_id, $school_admin) {
//    include_once(DAO_PATH . 'dao_role.php');
//    $roleDao = new RoleDAO();
//    // Lấy danh sách user có quyền đối với module $view
//    $userIdsYesPermission = $roleDao->getUserIdsOfModule($school_id, $view);
//    $userIdsYesPermission[] = $school_admin;
//    $userIdsYesPermission = array_unique($userIdsYesPermission);
//    // Lấy danh sách user không nhận thông báo từ giáo viên đối với module $view
//    $userIdsNoneNotify = array();
//    foreach ($notify_settings as $notify_setting) {
//        if(($notify_setting[$view] == NOTIFY_NO) || ($notify_setting[$view] == NOTIFY_ONLY_PARENT)) {
//            $userIdsNoneNotify[] = $notify_setting['user_id'];
//        }
//    }
//    // Dánh sách user_id nhận thông báo
//    $userIds = array_diff($userIdsYesPermission, $userIdsNoneNotify);
//    return $userIds;
//}

function getUserIdsReceiveNotifyOfSchoolForClass($notify_settings, $view, $school_id, $school_admin)
{
    include_once(DAO_PATH . 'dao_role.php');
    $roleDao = new RoleDAO();
    // Lấy danh sách user có quyền đối với module $view
    $userIdsYesPermission = $roleDao->getUserIdsOfModule($school_id, $view);
    $userIdsYesPermission[] = $school_admin;

    // Lấy danh sách hiệu trưởng của trường
    $schoolConfig = getSchoolData($school_id, SCHOOL_CONFIG);
    $principals = $schoolConfig['principal'];
    if (!is_empty($principals)) {
        $principals = explode(',', $principals);
        foreach ($principals as $row) {
            $userIdsYesPermission[] = $row;
        }
    }
    $userIdsYesPermission = array_unique($userIdsYesPermission);

    // Lấy danh sách user nhận thông báo từ module $view
    $userIdsRecive = array();
    foreach ($notify_settings as $notify_setting) {
        if (($notify_setting[$view] == NOTIFY_ALL) || ($notify_setting[$view] == NOTIFY_ONLY_TEACHER)) {
            $userIdsRecive[] = $notify_setting['user_id'];
        }
    }

    // Dánh sách userId nhận được thông báo
    $userIdsReciveNotify = array_intersect($userIdsRecive, $userIdsYesPermission);

    return $userIdsReciveNotify;
}

/**
 * Lấy danh sách user_id nhận được thông báo từ phụ huynh
 *
 * @param $notify_settings
 * @param $view
 * @param $school_id
 * @param $school_admin
 * @return array
 */
//function getUserIdsReceiveNotifyOfSchoolForChild($notify_settings, $view, $school_id, $school_admin) {
//    include_once(DAO_PATH . 'dao_role.php');
//    $roleDao = new RoleDAO();
//    // Lấy danh sách user có quyền đối với module $view
//    $userIdsYesPermission = $roleDao->getUserIdsOfModule($school_id, $view);
//    $userIdsYesPermission[] = $school_admin;
//    $userIdsYesPermission = array_unique($userIdsYesPermission);
//
//    // Lấy danh sách user không nhận thông báo từ phụ huynh đối với module $view
//    $userIdsNoneNotify = array();
//    foreach ($notify_settings as $notify_setting) {
//        if(($notify_setting[$view] == NOTIFY_NO) || ($notify_setting[$view] == NOTIFY_ONLY_TEACHER)) {
//            $userIdsNoneNotify[] = $notify_setting['user_id'];
//        }
//    }
//    // Dánh sách user_id nhận thông báo
//    $userIds = array_diff($userIdsYesPermission, $userIdsNoneNotify);
//    return $userIds;
//}

/**
 * Lấy danh sách user_id nhận được thông báo từ phụ huynh
 *
 * @param $notify_settings
 * @param $view
 * @param $school_id
 * @param $school_admin
 * @return array
 */
function getUserIdsReceiveNotifyOfSchoolForChild($notify_settings, $view, $school_id, $school_admin)
{
    include_once(DAO_PATH . 'dao_role.php');
    $roleDao = new RoleDAO();
    // Lấy danh sách user có quyền đối với module $view
    $userIdsYesPermission = $roleDao->getUserIdsOfModule($school_id, $view);
    $userIdsYesPermission[] = $school_admin;

    // Lấy danh sách hiệu trưởng của trường
    $schoolConfig = getSchoolData($school_id, SCHOOL_CONFIG);
    $principals = $schoolConfig['principal'];
    if (!is_empty($principals)) {
        $principals = explode(',', $principals);
        foreach ($principals as $row) {
            $userIdsYesPermission[] = $row;
        }
    }

    $userIdsYesPermission = array_unique($userIdsYesPermission);

    // Nếu view == tuitions hoặc view == feedback thì mặc định gửi thông báo
    if ($view == "tuitions" || $view == "feedback") {
        // Lấy danh sách user không nhận thông báo từ phụ huynh đối với module $view
        $userIdsNoneNotify = array();
        foreach ($notify_settings as $notify_setting) {
            if (($notify_setting[$view] == NOTIFY_NO) || ($notify_setting[$view] == NOTIFY_ONLY_TEACHER)) {
                $userIdsNoneNotify[] = $notify_setting['user_id'];
            }
        }
        // Dánh sách user_id nhận thông báo
        $userIdsReciveNotify = array_diff($userIdsYesPermission, $userIdsNoneNotify);
    } else {
        // Nếu không phải tuitions hoặc feedback
        // Lấy danh sách user nhận thông báo từ phụ huynh đối với module $view
        $userIdsRecive = array();
        foreach ($notify_settings as $notify_setting) {
            if (($notify_setting[$view] == NOTIFY_ALL) || ($notify_setting[$view] == NOTIFY_ONLY_PARENT)) {
                $userIdsRecive[] = $notify_setting['user_id'];
            }
        }

        // Dánh sách userId nhận được thông báo
        $userIdsReciveNotify = array_intersect($userIdsRecive, $userIdsYesPermission);
    }

    return $userIdsReciveNotify;
}

/**
 * Hàm get user nhận thông báo từ quản lý trường
 *
 * @param $school_id
 * @param $view
 * @param $school_admin
 * @return array
 */
function getUserIdsManagerReceiveNotify($school_id, $view, $school_admin)
{
    include_once(DAO_PATH . 'dao_role.php');
    $roleDao = new RoleDAO();
    //Lấy danh sách user có quyền đối với module $view
    $userIds = $roleDao->getUserIdsOfModule($school_id, $view);
    $userIds[] = $school_admin;
    $userIds = array_unique($userIds);
    return $userIds;
}

/*  ---------- BEGIN - Memcached  ---------- */

//---------- SCHOOL - MEMCACHED ----------//
/**
 * Lấy ra data memcached của trường.
 *
 * @param $schoolId
 * @return $sData
 */
function getSchoolData($schoolId, $getKey = ALL)
{
    global ${'sData_' . $schoolId}, $school_keys;
    $sData = ${'sData_' . $schoolId};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    $memcache = new CacheMemcache();

    $result = null;
    if ($memcache->bEnabled) {// if Memcache enabled
        if (is_null($sData)) {
            $sData = $memcache->getData(SCHOOL_KEY . $schoolId);
        }

        $keys = is_null($sData) ? array() : array_keys($sData);
        // Nếu getAll mà memcache không đủ dữ liệu thì lấy ra từ database
        if (!array_equal($keys, $school_keys)) {
            $sData = updateSchoolData($schoolId);
        }

        switch ($getKey) {
            case SCHOOL_DATA :
                if (isset($sData[SCHOOL_INFO]) && isset($sData[SCHOOL_CONFIG])) {
                    $result = array_merge($sData[SCHOOL_INFO], $sData[SCHOOL_CONFIG]);
                }
                break;

            case SCHOOL_INFO :
                if (isset($sData[SCHOOL_INFO])) {
                    $result = $sData[SCHOOL_INFO];
                }
                break;

            case SCHOOL_CONFIG :
                if (isset($sData[SCHOOL_CONFIG])) {
                    $result = $sData[SCHOOL_CONFIG];
                }
                break;

            case SCHOOL_TEACHERS :
                if (isset($sData[SCHOOL_TEACHERS])) {
                    $teacherIds = $sData[SCHOOL_TEACHERS];
                    foreach ($teacherIds as $teacherId) {
                        $teacher_info = getTeacherData($teacherId, TEACHER_INFO);
                        if (!empty($teacher_info)) {
                            $result[$teacherId] = $teacher_info;
                        }
                    }
                }
                break;

            case SCHOOL_CLASSES :
                if (isset($sData[SCHOOL_CLASSES])) {
                    $classIds = $sData[SCHOOL_CLASSES];
                    foreach ($classIds as $classId) {
                        $class_info = getClassData($classId, CLASS_INFO);
                        if (!empty($class_info)) {
                            $result[$classId] = $class_info;
                        }
                    }
                }
                break;

            case SCHOOL_CLASS_LEVELS :
                if (isset($sData[SCHOOL_CLASS_LEVELS])) {
                    $classLevelIds = $sData[SCHOOL_CLASS_LEVELS];
                    foreach ($classLevelIds as $classLevelId) {
                        $class_level_info = getClassLevelData($classLevelId, CLASS_LEVEL_INFO);
                        if (!empty($class_level_info)) {
                            $result[$classLevelId] = $class_level_info;
                        }
                    }
                }
                break;

            case SCHOOL_CHILDREN :
                if (isset($sData[SCHOOL_CHILDREN])) {
                    $childIds = $sData[SCHOOL_CHILDREN];
                    foreach ($childIds as $childId) {
                        $child_info = getChildData($childId, CHILD_INFO);
                        if (!empty($child_info)) {
                            $result[$childId] = $child_info;
                        }
                    }
                }
                break;

            case SCHOOL_NOTIFICATIONS :
                if (isset($sData[SCHOOL_NOTIFICATIONS])) {
                    $result = $sData[SCHOOL_NOTIFICATIONS];
                }
                break;

            case SCHOOL_LATE_PICKUP :
                if (isset($sData[SCHOOL_LATE_PICKUP])) {
                    $result = $sData[SCHOOL_LATE_PICKUP];
                }
                break;

            case SCHOOL_SERVICES :
                if (isset($sData[SCHOOL_SERVICES])) {
                    $result = $sData[SCHOOL_SERVICES];
                }
                break;

            case ALL :

                $result = array();
                $result[SCHOOL_DATA] = array_merge($sData[SCHOOL_INFO], $sData[SCHOOL_CONFIG]);
                $result[SCHOOL_INFO] = $sData[SCHOOL_INFO];
                $result[SCHOOL_CONFIG] = $sData[SCHOOL_CONFIG];
                $result[SCHOOL_NOTIFICATIONS] = $sData[SCHOOL_NOTIFICATIONS];
                $result[SCHOOL_LATE_PICKUP] = $sData[SCHOOL_LATE_PICKUP];
                $result[SCHOOL_SERVICES] = $sData[SCHOOL_SERVICES];

                // Lấy ra thông tin lớp của trường
                $classIds = $sData[SCHOOL_CLASSES];
                foreach ($classIds as $classId) {
                    $class_info = getClassData($classId, CLASS_INFO);
                    if (!empty($class_info)) {
                        $result[SCHOOL_CLASSES][$classId] = $class_info;
                    }
                }

                // Lấy ra thông tin giáo viên trường
                $teacherIds = $sData[SCHOOL_TEACHERS];
                foreach ($teacherIds as $teacherId) {
                    $teacher_info = getTeacherData($teacherId, TEACHER_INFO);
                    if (!empty($teacher_info)) {
                        $result[SCHOOL_TEACHERS][$teacherId] = $teacher_info;
                    }
                }

                // Lấy ra thông tin khối lớp của trường
                $classLevelIds = $sData[SCHOOL_CLASS_LEVELS];
                foreach ($classLevelIds as $classLevelId) {
                    $class_level_info = getClassLevelData($classLevelId, CLASS_LEVEL_INFO);
                    if (!empty($class_level_info)) {
                        $result[SCHOOL_CLASS_LEVELS][$classLevelId] = $class_level_info;
                    }
                }

                // Lấy ra thông tin trẻ của trường
                $childIds = $sData[SCHOOL_CHILDREN];
                foreach ($childIds as $childId) {
                    $child_info = getChildData($childId, CHILD_INFO);
                    if (!empty($child_info)) {
                        $result[SCHOOL_CHILDREN][$childId] = $child_info;
                    }
                }
                break;
        }
    }

    if (is_null($result)) {
        include_once(DAO_PATH . 'dao_school.php');
        include_once(DAO_PATH . 'dao_teacher.php');
        include_once(DAO_PATH . 'dao_class.php');
        include_once(DAO_PATH . 'dao_class_level.php');
        include_once(DAO_PATH . 'dao_child.php');
        include_once(DAO_PATH . 'dao_pickup.php');
        include_once(DAO_PATH . 'dao_service.php');

        $schoolDao = new SchoolDAO();
        $teacherDao = new TeacherDAO();
        $classDao = new ClassDAO();
        $classLevelDao = new ClassLevelDAO();
        $childDao = new ChildDAO();
        $pickupDao = new PickupDAO();
        $serviceDao = new ServiceDAO();

        switch ($getKey) {
            case SCHOOL_DATA :
                $info = $schoolDao->getSchoolById($schoolId);
                $config = $schoolDao->getConfiguration($schoolId);
                $result = (!empty($info) && !empty($config)) ? array_merge($info, $config) : null;
                break;

            case SCHOOL_INFO :
                $result = $schoolDao->getSchoolById($schoolId);
                break;

            case SCHOOL_CONFIG :
                $result = $schoolDao->getConfiguration($schoolId);
                break;

            case SCHOOL_TEACHERS :
                $teachers = $teacherDao->getAllTeachers4Memcache($schoolId);
                $result = $teachers['lists'];
                break;

            case SCHOOL_CLASSES :
                $classes = $classDao->getClassesOfSchool4Memcache($schoolId);
                $result = $classes['lists'];
                break;

            case SCHOOL_CLASS_LEVELS :
                $class_levels = $classLevelDao->getClassLevels4Memcache($schoolId);
                $result = $class_levels['lists'];
                break;

            case SCHOOL_CHILDREN :
                $children = $childDao->getChildrenOfSchool4Memcache($schoolId);
                $result = $children['lists'];
                break;

            case SCHOOL_NOTIFICATIONS :
                $result = $schoolDao->getUserNotificationSetting4Memcache($schoolId);
                break;

            case SCHOOL_LATE_PICKUP :
                $result = $pickupDao->getLatePickup4Memcached($schoolId);
                break;

            case SCHOOL_SERVICES :
                $result = $serviceDao->getServices4Memcahe($schoolId);
                break;

            case ALL :
                $info = $schoolDao->getSchoolById($schoolId);
                $config = $schoolDao->getConfiguration($schoolId);
                $teachers = $teacherDao->getAllTeachers4Memcache($schoolId);
                $classes = $classDao->getClassesOfSchool4Memcache($schoolId);
                $class_levels = $classLevelDao->getClassLevels4Memcache($schoolId);
                $children = $childDao->getChildrenOfSchool4Memcache($schoolId);
                $notify = $schoolDao->getUserNotificationSetting4Memcache($schoolId);
                $pickup = $pickupDao->getLatePickup4Memcached($schoolId);
                $services = $serviceDao->getServices4Memcahe($schoolId);

                $result[SCHOOL_INFO] = $info;
                $result[SCHOOL_CONFIG] = $config;
                $result[SCHOOL_TEACHERS] = $teachers['lists'];
                $result[SCHOOL_CLASSES] = $classes['lists'];
                $result[SCHOOL_CLASS_LEVELS] = $class_levels['lists'];
                $result[SCHOOL_CHILDREN] = $children['lists'];
                $result[SCHOOL_NOTIFICATIONS] = $notify;
                $result[SCHOOL_LATE_PICKUP] = $pickup;
                $result[SCHOOL_SERVICES] = $services;
                break;

        }
    }
    ${'sData_' . $schoolId} = $sData;
    return $result;
}

/**
 * Lấy ra data memcached của trường theo username
 *
 * @param $schoolId
 * @return $sData
 */
function getSchoolDataByUsername($username, $getKey = ALL)
{
    $result = null;
    global $relatedObjects;

    if (is_null($relatedObjects)) {
        $relatedObjects = getRelatedObjects();
    }

    foreach ($relatedObjects['schools'] as $school) {
        if ($school['page_name'] == $username) {
            $result = getSchoolData($school['page_id'], $getKey);
        }
    }

    if (!$result) {
        //Check 2 thông tin kia nếu true cả, thì lấy thông tin trường theo $username
        //Nếu false thì chạy như dưới
        if ($_SESSION['isGovManager'] && in_array($username, $_SESSION['schools'])) {
            // Lấy thông tin trường theo username
            include_once(DAO_PATH . 'dao_school.php');
            $schoolDao = new SchoolDAO();
            $school_id = $schoolDao->getSchoolIdByUsername($username);
            $result = getSchoolData($school_id, SCHOOL_DATA);
        }
    }
    return $result;
}


/**
 * Add dữ liệu memcached của trường
 *
 * @param $schoolId
 */
function addSchoolData($schoolId, $key = ALL, $value = null)
{
    global ${'sData_' . $schoolId}, $school_keys;
    $sData = ${'sData_' . $schoolId};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    $memcache = new CacheMemcache();

    //$result = null;
    if ($memcache->bEnabled) { // if Memcache enabled
        if (is_null($sData)) {
            $sData = $memcache->getData(SCHOOL_KEY . $schoolId);
        }

        $keys = is_null($sData) ? array() : array_keys($sData);
        // Nếu getAll mà memcache không đủ dữ liệu thì lấy ra từ database
        if (!array_equal($keys, $school_keys)) {
            $sData = updateSchoolData($schoolId);
        }

        switch ($key) {
            case SCHOOL_INFO :
                if (!is_null($value)) {
                    $sData[SCHOOL_INFO] = $value;
                }
                //$result = $sData[SCHOOL_INFO];
                break;

            case SCHOOL_CONFIG :
                if (!is_null($value)) {
                    $sData[SCHOOL_CONFIG] = $value;
                }
                //$result = $sData[SCHOOL_CONFIG];
                break;

            case SCHOOL_TEACHERS :
                if (!is_null($value) && is_array($sData[SCHOOL_TEACHERS])) {
                    $teacherIds = $sData[SCHOOL_TEACHERS];
                    $teacherIds[] = $value;
                    updateTeacherData($value);

                    $sData[SCHOOL_TEACHERS] = array_unique($teacherIds);
                    //$result = $sData[SCHOOL_TEACHERS];
                }
                break;

            case SCHOOL_CLASSES :
                if (!is_null($value) && is_array($sData[SCHOOL_CLASSES])) {
                    $classIds = $sData[SCHOOL_CLASSES];
                    $classIds[] = $value;
                    updateClassData($value);

                    $sData[SCHOOL_CLASSES] = array_unique($classIds);
                    //$result = $sData[SCHOOL_CLASSES];
                }
                break;

            case SCHOOL_CLASS_LEVELS :
                if (!is_null($value) && is_array($sData[SCHOOL_CLASS_LEVELS])) {
                    $classLevelIds = $sData[SCHOOL_CLASS_LEVELS];
                    $classLevelIds[] = $value;
                    updateClassLevelData($value);

                    $sData[SCHOOL_CLASS_LEVELS] = array_unique($classLevelIds);
                    //$result = $sData[SCHOOL_CLASS_LEVELS];
                }
                break;

            case SCHOOL_CHILDREN :
                if (!is_null($value) && is_array($sData[SCHOOL_CHILDREN])) {
                    $childIds = $sData[SCHOOL_CHILDREN];
                    $childIds[] = $value;
                    updateChildData($value);

                    $sData[SCHOOL_CHILDREN] = array_unique($childIds);
                    //$result = $sData[SCHOOL_CHILDREN];
                }
                break;

            case SCHOOL_NOTIFICATIONS :
                if (!is_null($value)) {
                    $sData[SCHOOL_NOTIFICATIONS] = $value;
                }
                //$result = $sData[SCHOOL_NOTIFICATIONS];
                break;

            case SCHOOL_LATE_PICKUP :
                if (!is_null($value)) {
                    $sData[SCHOOL_LATE_PICKUP] = $value;
                }
                //$result = $sData[SCHOOL_LATE_PICKUP];
                break;

            case SCHOOL_SERVICES :
                if (!is_null($value) && is_array($sData[SCHOOL_SERVICES])) {
                    include_once(DAO_PATH . 'dao_service.php');
                    $serviceDao = new ServiceDAO();

                    $services = $sData[SCHOOL_SERVICES];
                    $srv = $serviceDao->getServiceById($value);
                    $services[$value] = $srv;
                    $sData[SCHOOL_SERVICES] = $services;
                }
                //$result = $sData[SCHOOL_SERVICES];
                break;

        }
        $memcache->setData(SCHOOL_KEY . $schoolId, $sData);
    }
    ${'sData_' . $schoolId} = $sData;
    //return $result;
}


/**
 * Cập nhật dữ liệu memcached của trường
 *
 * @param $schoolId
 */
function updateSchoolData($schoolId, $key = ALL, $value = null)
{
    if (!is_numeric($schoolId) || $schoolId <= 0) {
        return null;
    }

    global ${'sData_' . $schoolId};
    $sData = ${'sData_' . $schoolId};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    include_once(DAO_PATH . 'dao_school.php');
    include_once(DAO_PATH . 'dao_teacher.php');
    include_once(DAO_PATH . 'dao_class.php');
    include_once(DAO_PATH . 'dao_class_level.php');
    include_once(DAO_PATH . 'dao_child.php');
    include_once(DAO_PATH . 'dao_pickup.php');
    include_once(DAO_PATH . 'dao_service.php');

    $memcache = new CacheMemcache();
    $schoolDao = new SchoolDAO();
    $teacherDao = new TeacherDAO();
    $classDao = new ClassDAO();
    $classLevelDao = new ClassLevelDAO();
    $childDao = new ChildDAO();
    $pickupDao = new PickupDAO();
    $serviceDao = new ServiceDAO();

    $result = null;
    if ($memcache->bEnabled) { // if Memcache enabled
        if (is_null($sData)) {
            $sData = $memcache->getData(SCHOOL_KEY . $schoolId);
        }

        switch ($key) {
            case SCHOOL_INFO :
                if (!is_null($value) && isset($sData[SCHOOL_INFO])) {
                    foreach ($value as $k => $v) {
                        $sData[SCHOOL_INFO][$k] = $v;
                    }
                } else {
                    $info = $schoolDao->getSchoolById($schoolId);
                    $sData[SCHOOL_INFO] = $info;
                }
                $result = $sData[SCHOOL_INFO];
                break;

            case SCHOOL_CONFIG :
                if (!is_null($value) && isset($sData[SCHOOL_CONFIG])) {
                    foreach ($value as $k => $v) {
                        $sData[SCHOOL_CONFIG][$k] = $v;
                    }
                } else {
                    $config = $schoolDao->getConfiguration($schoolId);
                    $sData[SCHOOL_CONFIG] = $config;
                }
                $result = $sData[SCHOOL_CONFIG];
                break;

            case SCHOOL_TEACHERS :
                if (!is_null($value) && isset($sData[SCHOOL_TEACHERS])) {
                    $teacherIds = $sData[SCHOOL_TEACHERS];
                    updateTeacherData($value);
                    if (!in_array($value, $teacherIds)) {
                        $teachers = $teacherDao->getAllTeachers4Memcache($schoolId);
                        $sData[SCHOOL_TEACHERS] = $teachers['ids'];
                    }
                } else {
                    $teachers = $teacherDao->getAllTeachers4Memcache($schoolId);
                    $sData[SCHOOL_TEACHERS] = $teachers['ids'];
                }
                $result = $sData[SCHOOL_TEACHERS];
                break;

            case SCHOOL_CLASSES :
                if (!is_null($value) && isset($sData[SCHOOL_CLASSES])) {
                    $classIds = $sData[SCHOOL_CLASSES];
                    updateClassData($value);
                    if (!in_array($value, $classIds)) {
                        $classes = $classDao->getClassesOfSchool4Memcache($schoolId);
                        $sData[SCHOOL_CLASSES] = $classes['ids'];
                    }
                } else {
                    $classes = $classDao->getClassesOfSchool4Memcache($schoolId);
                    $sData[SCHOOL_CLASSES] = $classes['ids'];
                }
                $result = $sData[SCHOOL_CLASSES];
                break;

            case SCHOOL_CLASS_LEVELS :
                if (!is_null($value) && isset($sData[SCHOOL_CLASS_LEVELS])) {
                    $classLevelIds = $sData[SCHOOL_CLASS_LEVELS];
                    updateClassLevelData($value);
                    if (!in_array($value, $classLevelIds)) {
                        $class_levels = $classLevelDao->getClassLevels4Memcache($schoolId);
                        $sData[SCHOOL_CLASS_LEVELS] = $class_levels['ids'];
                    }
                } else {
                    $class_levels = $classLevelDao->getClassLevels4Memcache($schoolId);
                    $sData[SCHOOL_CLASS_LEVELS] = $class_levels['ids'];
                }
                $result = $sData[SCHOOL_CLASS_LEVELS];
                break;

            case SCHOOL_CHILDREN :
                if (!is_null($value) && isset($sData[SCHOOL_CHILDREN])) {
                    $childIds = $sData[SCHOOL_CHILDREN];
                    updateChildData($value);
                    if (!in_array($value, $childIds)) {
                        $children = $childDao->getChildrenOfSchool4Memcache($schoolId);
                        $sData[SCHOOL_CHILDREN] = $children['ids'];
                    }
                    $sData[SCHOOL_CHILDREN] = $childIds;
                } else {
                    $children = $childDao->getChildrenOfSchool4Memcache($schoolId);
                    $sData[SCHOOL_CHILDREN] = $children['ids'];
                }
                $result = $sData[SCHOOL_CHILDREN];
                break;

            case SCHOOL_NOTIFICATIONS :
                if (!is_null($value) && isset($sData[SCHOOL_NOTIFICATIONS])) {
                    foreach ($value as $k => $v) {
                        $sData[SCHOOL_NOTIFICATIONS][$k] = $v;
                    }
                } else {
                    $notify = $schoolDao->getUserNotificationSetting4Memcache($schoolId);
                    $sData[SCHOOL_NOTIFICATIONS] = $notify;
                }
                $result = $sData[SCHOOL_NOTIFICATIONS];
                break;

            case SCHOOL_LATE_PICKUP :
                if (!is_null($value) && isset($sData[SCHOOL_LATE_PICKUP])) {
                    foreach ($value as $k => $v) {
                        $sData[SCHOOL_LATE_PICKUP][$k] = $v;
                    }
                } else {
                    $pickup = $pickupDao->getLatePickup4Memcached($schoolId);
                    $sData[SCHOOL_LATE_PICKUP] = $pickup;
                }
                $result = $sData[SCHOOL_LATE_PICKUP];
                break;

            case SCHOOL_SERVICES :
                if (!is_null($value) && isset($sData[SCHOOL_SERVICES])) {
                    foreach ($value as $serviceId => $service) {
                        foreach ($service as $k => $v) {
                            $sData[SCHOOL_SERVICES][$serviceId][$k] = $v;
                        }
                    }
                } else {
                    $services = $serviceDao->getServices4Memcahe($schoolId);
                    $sData[SCHOOL_SERVICES] = $services;
                }
                $result = $sData[SCHOOL_SERVICES];
                break;

            case ALL:
                $info = $schoolDao->getSchoolById($schoolId);
                $config = $schoolDao->getConfiguration($schoolId);
                $teachers = $teacherDao->getAllTeachers4Memcache($schoolId);
                $classes = $classDao->getClassesOfSchool4Memcache($schoolId);
                $class_levels = $classLevelDao->getClassLevels4Memcache($schoolId);
                $children = $childDao->getChildrenOfSchool4Memcache($schoolId);
                $notify = $schoolDao->getUserNotificationSetting4Memcache($schoolId);
                $pickup = $pickupDao->getLatePickup4Memcached($schoolId);
                $services = $serviceDao->getServices4Memcahe($schoolId);

                $sData[SCHOOL_INFO] = $info;
                $sData[SCHOOL_CONFIG] = $config;
                $sData[SCHOOL_TEACHERS] = $teachers['ids'];
                $sData[SCHOOL_CLASSES] = $classes['ids'];
                $sData[SCHOOL_CLASS_LEVELS] = $class_levels['ids'];
                $sData[SCHOOL_CHILDREN] = $children['ids'];
                $sData[SCHOOL_NOTIFICATIONS] = $notify;
                $sData[SCHOOL_LATE_PICKUP] = $pickup;
                $sData[SCHOOL_SERVICES] = $services;

                $result = $sData;
                break;

        }
        $memcache->setData(SCHOOL_KEY . $schoolId, $sData);
    } else {
        $info = $schoolDao->getSchoolById($schoolId);
        $config = $schoolDao->getConfiguration($schoolId);
        $teachers = $teacherDao->getAllTeachers4Memcache($schoolId);
        $classes = $classDao->getClassesOfSchool4Memcache($schoolId);
        $class_levels = $classLevelDao->getClassLevels4Memcache($schoolId);
        $children = $childDao->getChildrenOfSchool4Memcache($schoolId);
        $notify = $schoolDao->getUserNotificationSetting4Memcache($schoolId);
        $pickup = $pickupDao->getLatePickup4Memcached($schoolId);
        $services = $serviceDao->getServices4Memcahe($schoolId);

        $result[SCHOOL_INFO] = $info;
        $result[SCHOOL_CONFIG] = $config;
        $result[SCHOOL_TEACHERS] = $teachers['lists'];
        $result[SCHOOL_CLASSES] = $classes['lists'];
        $result[SCHOOL_CLASS_LEVELS] = $class_levels['lists'];
        $result[SCHOOL_CHILDREN] = $children['lists'];
        $result[SCHOOL_NOTIFICATIONS] = $notify;
        $result[SCHOOL_LATE_PICKUP] = $pickup;
        $result[SCHOOL_SERVICES] = $services;
    }

    ${'sData_' . $schoolId} = $sData;
    return $result;
}

/**
 * Set dữ liệu memcached của trường
 *
 * @param $schoolId
 */
function deleteSchoolData($schoolId, $key = ALL, $value = null)
{
    global ${'sData_' . $schoolId}, $school_keys;
    $sData = ${'sData_' . $schoolId};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    $memcache = new CacheMemcache();

    //$result = null;
    if ($memcache->bEnabled) { // if Memcache enabled
        if (is_null($sData)) {
            $sData = $memcache->getData(SCHOOL_KEY . $schoolId);
        }
        $keys = is_null($sData) ? array() : array_keys($sData);
        // Nếu getAll mà memcache không đủ dữ liệu thì lấy ra từ database
        if (!array_equal($keys, $school_keys)) {
            $sData = updateSchoolData($schoolId);
        }

        if (!is_null($sData)) {
            switch ($key) {
                case SCHOOL_TEACHERS :
                case SCHOOL_CLASSES :
                case SCHOOL_CLASS_LEVELS :
                case SCHOOL_CHILDREN :
                    if (isset($sData[$key]) && !is_null($value)) {
                        if (($key_del = array_search($value, $sData[$key])) !== false) {
                            unset($sData[$key][$key_del]);
                        }
                    } else {
                        unset($sData[$key]);
                    }
                    break;

                case SCHOOL_INFO :
                case SCHOOL_CONFIG :
                case SCHOOL_NOTIFICATIONS :
                case SCHOOL_LATE_PICKUP :
                case SCHOOL_SERVICES :
                    if (isset($sData[$key]) && !is_null($value)) {
                        unset($sData[$key][$value]);
                    } else {
                        unset($sData[$key]);
                    }
                    break;

                case ALL :
                    $sData = null;
                    break;
            }
        }
        $memcache->setData(SCHOOL_KEY . $schoolId, $sData);
    }
    ${'sData_' . $schoolId} = $sData;
}

//---------- CLASS - MEMCACHED ----------//

/**
 * Lấy ra data memcached của trường.
 *
 * @param $schoolId
 * @return $sData
 */
function getClassData($classId, $getKey = ALL)
{
    global $class_keys, ${'cData_' . $classId};
    $cData = ${'cData_' . $classId};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    $memcache = new CacheMemcache();

    $result = null;
    if ($memcache->bEnabled) { // if Memcache enabled
        if (is_null($cData)) {
            $cData = $memcache->getData(CLASS_KEY . $classId);
        }
        $keys = is_null($cData) ? array() : array_keys($cData);
        // Nếu getAll mà memcache không đủ dữ liệu thì lấy ra từ database
        if (!array_equal($keys, $class_keys)) {
            $cData = updateClassData($classId);
        }

        switch ($getKey) {
            // Lấy thông tin của lớp
            case CLASS_INFO :
                if (isset($cData[CLASS_INFO])) {
                    $result = $cData[CLASS_INFO];
                }
                break;

            // Lấy danh sách giáo viên lớp
            case CLASS_TEACHERS :
                if (isset($cData[CLASS_TEACHERS])) {
                    $teacherIds = $cData[CLASS_TEACHERS];
                    foreach ($teacherIds as $teacherId) {
                        $teacher_info = getTeacherData($teacherId, TEACHER_INFO);
                        if (!empty($teacher_info)) {
                            $result[$teacherId] = $teacher_info;
                        }
                    }
                }
                break;

            // Lấy danh sách trẻ
            case CLASS_CHILDREN :
                if (isset($cData[CLASS_CHILDREN])) {
                    $childIds = $cData[CLASS_CHILDREN];
                    foreach ($childIds as $childId) {
                        $child_info = getChildData($childId, CHILD_INFO);
                        if (!empty($child_info)) {
                            $result[$childId] = $child_info;
                        }
                    }
                }
                break;

            case ALL:
                $result[CLASS_INFO] = $cData[CLASS_INFO];

                $teacherIds = $cData[CLASS_TEACHERS];
                foreach ($teacherIds as $teacherId) {
                    $teacher_info = getTeacherData($teacherId, TEACHER_INFO);
                    if (!empty($teacher_info)) {
                        $result[CLASS_TEACHERS][$teacherId] = $teacher_info;
                    }
                }

                $childIds = $cData[CLASS_CHILDREN];
                foreach ($childIds as $childId) {
                    $child_info = getChildData($childId, CHILD_INFO);
                    if (!empty($child_info)) {
                        $result[CLASS_CHILDREN][$childId] = $child_info;
                    }
                }
                break;

        }
    }

    if (is_null($result)) {
        include_once(DAO_PATH . 'dao_class.php');
        include_once(DAO_PATH . 'dao_child.php');
        include_once(DAO_PATH . 'dao_teacher.php');
        include_once(DAO_PATH . 'dao_class.php');

        $classDao = new ClassDAO();
        $childDao = new ChildDAO();
        $teacherDao = new TeacherDAO();

        switch ($getKey) {
            case CLASS_INFO :
                $result = $classDao->getClass4Memcache($classId);
                break;

            case CLASS_TEACHERS :
                $teachers = $teacherDao->getTeacherOfClass4Memcache($classId);
                $result = $teachers['lists'];
                break;

            case CLASS_CHILDREN :
                $children = $childDao->getChildrenOfClass4Memcache($classId);
                $result = $children['lists'];
                break;

            case ALL :
                $info = $classDao->getClass4Memcache($classId);
                $teachers = $teacherDao->getTeacherOfClass4Memcache($classId);
                $children = $childDao->getChildrenOfClass4Memcache($classId);

                $result[CLASS_INFO] = $info;
                $result[CLASS_TEACHERS] = $teachers['lists'];
                $result[CLASS_CHILDREN] = $children['lists'];
                break;
        }
    }
    ${'cData_' . $classId} = $cData;
    return $result;
}

/**
 * Lấy ra data memcached của trường theo username
 *
 * @param $schoolId
 * @return $sData
 */
function getClassDataByUsername($username, $getKey = ALL)
{
    global $user;
    $result = null;

    $classes = getUserManageData($user->_data['user_id'], USER_MANAGE_CLASSES);
    foreach ($classes as $class) {
        if ($class['group_name'] == $username) {
            $result = getClassData($class['group_id'], $getKey);
        }
    }
    return $result;
}

/**
 * Add dữ liệu memcached của lớp
 *
 * @param $schoolId
 */
function addClassData($classId, $key = ALL, $value = null)
{
    global ${'cData_' . $classId}, $class_keys;
    $cData = ${'cData_' . $classId};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    $memcache = new CacheMemcache();

    //$result = null;
    if ($memcache->bEnabled) { // if Memcache enabled
        if (is_null($cData)) {
            $cData = $memcache->getData(CLASS_KEY . $classId);
        }

        $keys = is_null($cData) ? array() : array_keys($cData);
        // Nếu getAll mà memcache không đủ dữ liệu thì lấy ra từ database
        if (!array_equal($keys, $class_keys)) {
            $cData = updateClassData($classId);
        }

        switch ($key) {
            case CLASS_INFO :
                if (!is_null($value)) {
                    $cData[CLASS_INFO] = $value;
                }
                //$result = $cData[CLASS_INFO];
                break;

            case CLASS_TEACHERS :
                if (!is_null($value) && is_array($cData[CLASS_TEACHERS])) {
                    $teacherIds = $cData[CLASS_TEACHERS];
                    $teacherIds[] = $value;
                    updateTeacherData($value);

                    $cData[CLASS_TEACHERS] = array_unique($teacherIds);
                }
                break;

            case CLASS_CHILDREN :
                if (!is_null($value) && is_array($cData[CLASS_CHILDREN])) {
                    $childIds = $cData[CLASS_CHILDREN];
                    $childIds[] = $value;
                    updateChildData($value);

                    $cData[CLASS_CHILDREN] = array_unique($childIds);
                }
                break;
        }
        $memcache->setData(CLASS_KEY . $classId, $cData);
    }
    ${'cData_' . $classId} = $cData;
    //return $result;
}

/**
 * Update dữ liệu memcached của lớp
 *
 * @param $schoolId
 */
function updateClassData($classId, $key = ALL, $value = null)
{
    if (!is_numeric($classId) || $classId <= 0) {
        return null;
    }
    global ${'cData_' . $classId};
    $cData = ${'cData_' . $classId};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    include_once(DAO_PATH . 'dao_class.php');
    include_once(DAO_PATH . 'dao_child.php');
    include_once(DAO_PATH . 'dao_teacher.php');
    include_once(DAO_PATH . 'dao_class.php');

    $memcache = new CacheMemcache();
    $classDao = new ClassDAO();
    $childDao = new ChildDAO();
    $teacherDao = new TeacherDAO();

    $result = null;
    if ($memcache->bEnabled) { // if Memcache enabled
        if (is_null($cData)) {
            $cData = $memcache->getData(CLASS_KEY . $classId);
        }

        switch ($key) {
            case CLASS_INFO :
                if (!is_null($value) && isset($cData[CLASS_INFO])) {
                    foreach ($value as $k => $v) {
                        $cData[CLASS_INFO][$k] = $v;
                    }
                } else {
                    $info = $classDao->getClass4Memcache($classId);
                    $cData[CLASS_INFO] = $info;
                }
                $result = $cData[CLASS_INFO];
                break;

            case CLASS_TEACHERS :
                if (!is_null($value) && isset($cData[CLASS_TEACHERS])) {
                    $teacherIds = $cData[CLASS_TEACHERS];
                    updateTeacherData($value);
                    if (!in_array($value, $teacherIds)) {
                        $teachers = $teacherDao->getTeacherOfClass4Memcache($classId);
                        $cData[CLASS_TEACHERS] = $teachers['ids'];
                    }
                } else {
                    $teachers = $teacherDao->getTeacherOfClass4Memcache($classId);
                    $cData[CLASS_TEACHERS] = $teachers['ids'];
                }
                $result = $cData[CLASS_TEACHERS];
                break;

            case CLASS_CHILDREN :
                if (!is_null($value) && isset($cData[CLASS_CHILDREN])) {
                    $childIds = $cData[CLASS_CHILDREN];
                    updateChildData($value);
                    if (!in_array($value, $childIds)) {
                        $children = $childDao->getChildrenOfClass4Memcache($classId);
                        $cData[CLASS_CHILDREN] = $children['ids'];
                    }
                } else {
                    $children = $childDao->getChildrenOfClass4Memcache($classId);
                    $cData[CLASS_CHILDREN] = $children['ids'];
                }
                $result = $cData[CLASS_CHILDREN];
                break;

            case ALL :
                $info = $classDao->getClass4Memcache($classId);
                $teachers = $teacherDao->getTeacherOfClass4Memcache($classId);
                $children = $childDao->getChildrenOfClass4Memcache($classId);
                $cData[CLASS_INFO] = $info;
                $cData[CLASS_TEACHERS] = $teachers['ids'];
                $cData[CLASS_CHILDREN] = $children['ids'];
                $result = $cData;
                break;
        }

        $memcache->setData(CLASS_KEY . $classId, $cData);
    } else {
        $info = $classDao->getClass4Memcache($classId);
        $teachers = $teacherDao->getTeacherOfClass4Memcache($classId);
        $children = $childDao->getChildrenOfClass4Memcache($classId);
        $result[CLASS_INFO] = $info;
        $result[CLASS_TEACHERS] = $teachers['ids'];
        $result[CLASS_CHILDREN] = $children['ids'];
    }
    ${'cData_' . $classId} = $cData;
    return $result;
}

/**
 * Set dữ liệu memcached của lớp
 *
 * @param $schoolId
 */
function deleteClassData($classId, $key = ALL, $value = null)
{
    global ${'cData_' . $classId}, $class_keys;
    $cData = ${'cData_' . $classId};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    $memcache = new CacheMemcache();

    if ($memcache->bEnabled) { // if Memcache enabled
        if (is_null($cData)) {
            $cData = $memcache->getData(CLASS_KEY . $classId);
        }
        $keys = is_null($cData) ? array() : array_keys($cData);
        // Nếu getAll mà memcache không đủ dữ liệu thì lấy ra từ database
        if (!array_equal($keys, $class_keys)) {
            $cData = updateClassData($classId);
        }

        if (!is_null($cData)) {
            switch ($key) {
                case CLASS_INFO :
                case CLASS_TEACHERS :
                case CLASS_CHILDREN :
                    if (isset($value)) {
                        if (($key_del = array_search($value, $cData[$key])) !== false) {
                            unset($cData[$key][$key_del]);
                        }
                    } else {
                        unset($cData[$key]);
                    }
                    break;

                case ALL :
                    $cData = null;
                    break;
            }
        }
        $memcache->setData(CLASS_KEY . $classId, $cData);
    }
    ${'cData_' . $classId} = $cData;
}


//---------- CLASS LEVEL - MEMCACHED ----------//

/**
 * Lấy ra data memcached của khối.
 *
 * @param $schoolId
 * @return $sData
 */
function getClassLevelData($classLevelId, $getKey = ALL)
{
    global $class_level_keys, ${'clData_' . $classLevelId};
    $clData = ${'clData_' . $classLevelId};
    error_log('function.php 4249 - $clData1 :'.json_encode($clData));
    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');

    $memcache = new CacheMemcache();
    error_log('function.php 4253 - $clData2 :'.json_encode($clData));
    error_log('function.php 4254 - $memcache :'.json_encode($memcache).';$getKey ='.$getKey);
    $result = null;
    if ($memcache->bEnabled) { // if Memcache enabled
        if (is_null($clData)) {
            error_log('function.php 4258 - is null $clData :'.json_encode($clData));
            $clData = $memcache->getData('class_level_' . $classLevelId);
        }
        error_log('function.php 4258 - $clData3 :'.json_encode($clData));
        $keys = is_null($clData) ? array() : array_keys($clData);
        // Nếu getAll mà memcache không đủ dữ liệu thì lấy ra từ database
        if (!array_equal($keys, $class_level_keys)) {
            $clData = updateClassLevelData($classLevelId);
        }
        error_log('function.php 4267 - $clData4 :'.json_encode($clData).';$getKey ='.$getKey);
        switch ($getKey) {
            case CLASS_LEVEL_INFO :
                if (isset($clData[CLASS_LEVEL_INFO])) {
                    $result = $clData[CLASS_LEVEL_INFO];
                }
                break;

            case CLASS_LEVEL_CLASSES :
                if (isset($clData[CLASS_LEVEL_CLASSES])) {
                    $classIds = $clData[CLASS_LEVEL_CLASSES];
                    foreach ($classIds as $classId) {
                        $class_info = getClassData($classId, CLASS_INFO);
                        if (!empty($class_info)) {
                            $result[$classId] = $class_info;
                        }
                    }
                }
                break;

            case ALL:
                if (isset($clData[CLASS_LEVEL_INFO]) && isset($clData[CLASS_LEVEL_CLASSES])) {
                    $result[CLASS_LEVEL_INFO] = $clData[CLASS_LEVEL_INFO];

                    $classIds = $clData[CLASS_LEVEL_CLASSES];
                    foreach ($classIds as $classId) {
                        $class_info = getClassData($classId, CLASS_INFO);
                        if (!empty($class_info)) {
                            $result[CLASS_LEVEL_CLASSES][$classId] = $class_info;
                        }
                    }
                }

                break;
        }
    }

    if (is_null($result)) {
        include_once(DAO_PATH . 'dao_class.php');
        include_once(DAO_PATH . 'dao_class_level.php');

        $classDao = new ClassDAO();
        $classLevelDao = new ClassLevelDAO();
        error_log('is_null result - ');
        switch ($getKey) {
            case CLASS_LEVEL_INFO :
                $class_level = $classLevelDao->getClassLevel4Memcache($classLevelId);
                error_log('is_null result -  4311 - $class_level :'.json_encode($class_level));
                $result = $class_level;
                break;

            case CLASS_LEVEL_CLASSES :
                $classes = $classDao->getClassesOfLevel4Memcache($classLevelId);
                $result = $classes['lists'];
                break;

            case ALL :
                $class_level = $classLevelDao->getClassLevel4Memcache($classLevelId);
                $classes = $classDao->getClassesOfLevel4Memcache($classLevelId);

                $result[CLASS_LEVEL_INFO] = $class_level;
                $result[CLASS_LEVEL_CLASSES] = $classes['lists'];
                break;
        }

    }
    ${'clData_' . $classLevelId} = $clData;

    return $result;
}

/**
 * Add dữ liệu memcached của khối
 *
 * @param $schoolId
 */
function addClassLevelData($classLevelId, $key = ALL, $value = null)
{
    global ${'clData_' . $classLevelId}, $class_level_keys;
    $clData = ${'clData_' . $classLevelId};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    $memcache = new CacheMemcache();

    //$result = null;
    if ($memcache->bEnabled) { // if Memcache enabled
        if (!is_null($clData)) {
            $clData = $memcache->getData('class_level_' . $classLevelId);
        }
        $keys = is_null($clData) ? array() : array_keys($clData);
        // Nếu getAll mà memcache không đủ dữ liệu thì lấy ra từ database
        if (!array_equal($keys, $class_level_keys)) {
            $clData = updateClassLevelData($classLevelId);
        }

        switch ($key) {
            case CLASS_LEVEL_INFO :
                if (!is_null($value)) {
                    foreach ($value as $k => $v) {
                        $clData[CLASS_LEVEL_INFO] = $value;
                    }
                }
                //$result = $clData[CLASS_LEVEL_INFO];
                break;

            case CLASS_LEVEL_CLASSES :
                if (!is_null($value) && is_array($clData[CLASS_LEVEL_CLASSES])) {
                    $classIds = $clData[CLASS_LEVEL_CLASSES];
                    $classIds[] = $value;
                    updateClassData($value);
                    $clData[CLASS_LEVEL_CLASSES] = array_unique($classIds);
                }
                break;
        }
        $memcache->setData('class_level_' . $classLevelId, $clData);
    }
    ${'clData_' . $classLevelId} = $clData;
    //return $result;
}

/**
 * Update dữ liệu memcached của khối
 *
 * @param $schoolId
 */
function updateClassLevelData($classLevelId, $key = ALL, $value = null)
{
    if (!is_numeric($classLevelId) || $classLevelId <= 0) {
        return null;
    }

    global ${'clData_' . $classLevelId};
    $clData = ${'clData_' . $classLevelId};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    include_once(DAO_PATH . 'dao_class.php');
    include_once(DAO_PATH . 'dao_child.php');
    include_once(DAO_PATH . 'dao_teacher.php');
    include_once(DAO_PATH . 'dao_class.php');
    include_once(DAO_PATH . 'dao_class_level.php');

    $memcache = new CacheMemcache();
    $classDao = new ClassDAO();
    $classLevelDao = new ClassLevelDAO();
    $childDao = new ChildDAO();
    $teacherDao = new TeacherDAO();

    $result = null;
    if ($memcache->bEnabled) { // if Memcache enabled
        if (!is_null($clData)) {
            $clData = $memcache->getData('class_level_' . $classLevelId);
        }

        switch ($key) {
            case CLASS_LEVEL_INFO :
                if (!is_null($value) && isset($clData[CLASS_LEVEL_INFO])) {
                    foreach ($value as $k => $v) {
                        $clData[CLASS_LEVEL_INFO][$k] = $v;
                    }
                } else {
                    $info = $classLevelDao->getClassLevel4Memcache($classLevelId);
                    $clData[CLASS_LEVEL_INFO] = $info;
                }
                $result = $clData[CLASS_LEVEL_INFO];
                break;

            case CLASS_LEVEL_CLASSES :
                if (!is_null($value) && isset($clData[CLASS_LEVEL_CLASSES])) {
                    $classIds = $clData[CLASS_LEVEL_CLASSES];
                    updateClassData($value);
                    if (!in_array($value, $classIds)) {
                        $classes = $classDao->getClassesOfLevel4Memcache($classLevelId);
                        $clData[CLASS_LEVEL_CLASSES] = $classes['ids'];
                    }
                } else {
                    $classes = $classDao->getClassesOfLevel4Memcache($classLevelId);
                    $clData[CLASS_LEVEL_CLASSES] = $classes['ids'];
                }
                $result = $clData[CLASS_LEVEL_CLASSES];
                break;

            case ALL :
                $info = $classLevelDao->getClassLevel4Memcache($classLevelId);
                $classes = $classDao->getClassesOfLevel4Memcache($classLevelId);
                $clData[CLASS_LEVEL_INFO] = $info;
                $clData[CLASS_LEVEL_CLASSES] = $classes['ids'];

                $result = $clData;
                break;
        }
        $memcache->setData('class_level_' . $classLevelId, $clData);
    } else {
        $info = $classLevelDao->getClassLevel4Memcache($classLevelId);
        $classes = $classDao->getClassesOfLevel4Memcache($classLevelId);
        $result[CLASS_LEVEL_INFO] = $info;
        $result[CLASS_LEVEL_CLASSES] = $classes['ids'];
    }
    ${'clData_' . $classLevelId} = $clData;
    return $result;
}


/**
 * Delete dữ liệu memcached của khối
 *
 * @param $schoolId
 */
function deleteClassLevelData($classLevelId, $key = ALL, $value = null)
{
    global ${'clData_' . $classLevelId}, $class_level_keys;
    $clData = ${'clData_' . $classLevelId};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    $memcache = new CacheMemcache();

    //$result = null;
    if ($memcache->bEnabled) { // if Memcache enabled
        if (!is_null($clData)) {
            $clData = $memcache->getData('class_level_' . $classLevelId);
        }
        $keys = is_null($clData) ? array() : array_keys($clData);
        // Nếu getAll mà memcache không đủ dữ liệu thì lấy ra từ database
        if (!array_equal($keys, $class_level_keys)) {
            $clData = updateClassLevelData($classLevelId);
        }

        if (!is_null($clData)) {
            switch ($key) {
                case CLASS_LEVEL_INFO :
                case CLASS_LEVEL_CLASSES :
                    if (isset($value)) {
                        if (($key_del = array_search($value, $clData[$key])) !== false) {
                            unset($clData[$key][$key_del]);
                        }
                    } else {
                        unset($clData[$key]);
                    }
                    break;

                case ALL :
                    $clData = null;
                    break;
            }
        }
        $memcache->setData('class_level_' . $classLevelId, $clData);
    }
    ${'clData_' . $classLevelId} = $clData;
    //return $result;
}

/* ADD START - ManhDD 05/04/2021 */
//---------- SUBJECT - MEMCACHED ----------//
/**
 * Lấy ra data memcached của môn học.
 *
 * @param $subjectId
 * @return $sData
 */
function getSubjectData($subjectId, $getKey = ALL)
{
    global $subject_keys, ${'tData_' . $subjectId};
    $tData = ${'tData_' . $subjectId};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    $memcache = new CacheMemcache();

    $result = null;
    if ($memcache->bEnabled) { // if Memcache enabled
        if (is_null($tData)) {
            $tData = $memcache->getData(SUBJECT_KEY . $subjectId);
        }
        $keys = is_null($tData) ? array() : array_keys($tData);
        // Nếu getAll mà memcache không đủ dữ liệu thì lấy ra từ database
        if (!array_equal($keys, $subject_keys)) {
            $tData = updateSubjectData($subjectId);
        }

        switch ($getKey) {
            case SUBJECT_INFO :
                if (isset($tData[SUBJECT_INFO])) {
                    $result = $tData[SUBJECT_INFO];
                }
                break;

//            case SUBJECT_GRADES :
//                if (isset($tData[SUBJECT_GRADES])) {
//                    $gradeIds = $tData[SUBJECT_GRADES];
//                    foreach ($gradeIds as $gradeId) {
//                        $grade_info = getGradeData($gradeId, GRADE_INFO);
//                        if (!empty($grade_info)) {
//                            $result[$gradeId] = $grade_info;
//                        }
//                    }
//                }
//                break;
//
//            case SUBJECT_SCHOOL :
//                if (isset($tData[SUBJECT_SCHOOL])) {
//                    $schoolIds = $tData[SUBJECT_SCHOOL];
//                    foreach ($schoolIds as $schoolId) {
//                        $school_info = getSchoolData($schoolId, SCHOOL_INFO);
//                        if (!empty($school_info)) {
//                            $result[$schoolId] = $school_info;
//                        }
//                    }
//                }
//                break;

            case ALL:

                //1. Lấy ra thông tin của môn học = key [SUBJECT_INFO]
                $result[SUBJECT_INFO] = $tData[SUBJECT_INFO];

//                //2. Lấy ra thông tin khối của môn học = key [SUBJECT_GRADES]
//                $gradeIds = $tData[SUBJECT_GRADES];
//                foreach ($gradeIds as $gradeId) {
//                    $grade_info = getGradeData($gradeId, GRADE_INFO);
//                    if (!empty($class_info)) {
//                        $result[SUBJECT_GRADES][$gradeId] = $grade_info;
//                    }
//                }
//
//                //3. Lấy ra thông tin trường của môn học
//                $schoolIds = $tData[SUBJECT_SCHOOL];
//                foreach ($schoolIds as $schoolId) {
//                    $school_info = getSchoolData($schoolId, SCHOOL_INFO);
//                    if (!empty($school_info)) {
//                        $result[SUBJECT_SCHOOL][$schoolId] = $school_info;
//                    }
//                }
                break;
        }
    }

    if (is_null($result)) {
//        include_once(DAO_PATH . 'dao_school.php');
        include_once(DAO_PATH . 'dao_subject.php');
//        include_once(DAO_PATH . 'dao_grade.php');

//        $schoolDao = new SchoolDAO();

        $subjectDao = new SubjectDAO();
//        $roleDao = new RoleDAO();

        switch ($getKey) {
            case SUBJECT_INFO :
                $info = $subjectDao->getSubject4Memcache($subjectId);
                $tData[SUBJECT_INFO] = $info;
                $result = $info;
                break;

//            case SUBJECT_GRADES :
//                $grades = $subjectDao->getGrades4Memcache($subjectId);
//                $tData[SUBJECT_GRADES] = $grades['ids'];
//                $result = $grades['lists'];
//                break;

//            case SUBJECT_SCHOOL :
//                $schools = $schoolDao->getSchoolsHaveRole($teacherId);
//                //Load tất cả quyền của user với từng trường.
//                $schoolIds = array();
//                $newSchools = array();
//                foreach ($schools as $school) {
//                    $schoolIds[] = $school['page_id'];
//                    $newSchool = $school;
//                    if ($school['role_id'] == -1) {
//                        $newSchool['is_admin'] = true;
//                        $newSchool['is_teacher'] = false;
//                    } else if($school['role_id'] == 0) {
//                        $newSchool['is_admin'] = false;
//                        $newSchool['is_teacher'] = true;
//                    } else {
//                        $newSchool['is_admin'] = false;
//                        $newSchool['is_teacher'] = false;
//                        $newSchool['permissions'] = $roleDao->getPermissionOfSchool($school['page_id'], $teacherId);
//                    }
//                    $newSchools[] = $newSchool;
//                }
//
//                $tData[TEACHER_SCHOOL] = $schoolIds;
//                $result = $newSchools;
//                break;

            case ALL :
                $info = $subjectDao->getSubject4Memcache($subjectId);
//                $grades = $subjectDao->getGrades4Memcache($subjectId);
//                //Load tất cả quyền của user với từng trường.
//                $schools = $schoolDao->getSchoolsHaveRole($teacherId);
//                $schoolIds = array();
//                $newSchools = array();
//                foreach ($schools as $school) {
//                    $schoolIds[] = $school['page_id'];
//                    $newSchool = $school;
//                    if ($school['role_id'] == -1) {
//                        $newSchool['is_admin'] = true;
//                        $newSchool['is_teacher'] = false;
//                    } else if($school['role_id'] == 0) {
//                        $newSchool['is_admin'] = false;
//                        $newSchool['is_teacher'] = true;
//                    } else {
//                        $newSchool['is_admin'] = false;
//                        $newSchool['is_teacher'] = false;
//                        $newSchool['permissions'] = $roleDao->getPermissionOfSchool($school['page_id'], $teacherId);
//                    }
//                    $newSchools[] = $newSchool;
//                }

                $tData[SUBJECT_INFO] = $info;
//                $tData[SUBJECT_GRADES] = $grades['ids'];
//                $tData[TEACHER_SCHOOL] = $schoolIds;

                $result[TEACHER_INFO] = $info;
//                $result[TEACHER_GRADES] = $grades['lists'];
//                $result[TEACHER_SCHOOL] = $newSchools;
                break;
        }
    }
    ${'tData_' . $subjectId} = $tData;
    return $result;
}

/**
 * Add dữ liệu memcached của môn học
 *
 * @param $subjectId
 */
function addSubjectData($subjectId, $key = ALL, $value = null)
{
    global ${'tData_' . $subjectId}, $subject_keys;
    $tData = ${'tData_' . $subjectId};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    include_once(DAO_PATH . 'dao_subject.php');
//    include_once(DAO_PATH . 'dao_class.php');
//    include_once(DAO_PATH . 'dao_child.php');
//    include_once(DAO_PATH . 'dao_teacher.php');
//    include_once(DAO_PATH . 'dao_class.php');
//    include_once(DAO_PATH . 'dao_role.php');

    $memcache = new CacheMemcache();
    $subjectDao = new SubjectDAO();


    //$result = null;
    if ($memcache->bEnabled) { // if Memcache enabled
        if (is_null($tData)) {
            $tData = $memcache->getData(SUBJECT_KEY . $subjectId);
        }

        $keys = is_null($tData) ? array() : array_keys($tData);
        // Nếu getAll mà memcache không đủ dữ liệu thì lấy ra từ database
        if (!array_equal($keys, $subject_keys)) {
            $tData = updateSubjectData($subjectId);
        }

        switch ($key) {
            case SUBJECT_INFO :
                if (!is_null($value)) {
                    $tData[SUBJECT_INFO] = $value;
                } else {
                    $info = $subjectDao->getSubject4Memcache($subjectId);
                    $tData[SUBJECT_INFO] = $info;
                }
                //$result = $tData[TEACHER_INFO];
                break;

//            case TEACHER_CLASSES :
//                if (!is_null($value) && is_array($tData[TEACHER_CLASSES])) {
//                    $classIds = $tData[TEACHER_CLASSES];
//                    $classIds[] = $value;
//                    $tData[TEACHER_CLASSES] =  array_unique($classIds);
//                } else {
//                    $classes = $teacherDao->getClasses4Memcache($teacherId);
//                    $tData[TEACHER_CLASSES] = $classes['ids'];
//                    //$result = $classes['lists'];
//                }
//                break;
//
//            case TEACHER_SCHOOL :
//                if (!is_null($value) && is_array($tData[TEACHER_SCHOOL])) {
//                    $schoolIds = $tData[TEACHER_SCHOOL];
//                    $schoolIds[] = $value;
//                    $tData[TEACHER_SCHOOL] =  array_unique($schoolIds);
//                } else {
//                    $schools = $schoolDao->getSchoolsHaveRole($teacherId);
//                    //Load tất cả quyền của user với từng trường.
//                    $schoolIds = array();
//                    foreach ($schools as $school) {
//                        $schoolIds[] = $school['page_id'];
//                    }
//
//                    $tData[TEACHER_SCHOOL] = $schoolIds;
//                    //$result = $newSchools;
//                }
//                break;

        }

        $memcache->setData(SUBJECT_KEY . $subjectId, $tData);
    }
    ${'tData_' . $subjectId} = $tData;
    //return $result;
}

/**
 * Update dữ liệu memcached của môn học
 *
 * @param $subjectId
 */
function updateSubjectData($subjectId, $key = ALL, $value = null)
{
    if (!is_numeric($subjectId) || $subjectId <= 0) {
        return null;
    }

    global ${'tData_' . $subjectId};
    $tData = ${'tData_' . $subjectId};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
//    include_once(DAO_PATH . 'dao_school.php');
    include_once(DAO_PATH . 'dao_subject.php');
//    include_once(DAO_PATH . 'dao_role.php');

    $memcache = new CacheMemcache();
//    $schoolDao = new SchoolDAO();
    $subjectDao = new SubjectDAO();
//    $roleDao = new RoleDAO();

    $result = null;
    if ($memcache->bEnabled) { // if Memcache enabled
        if (is_null($tData)) {
            $tData = $memcache->getData(SUBJECT_KEY . $subjectId);
        }

        switch ($key) {
            case SUBJECT_INFO :
                if (!is_null($value) && isset($tData[SUBJECT_INFO])) {
                    foreach ($value as $k => $v) {
                        $tData[SUBJECT_INFO][$k] = $v;
                    }
                } else {
                    $info = $subjectDao->getSubject4Memcache($subjectId);
                    $tData[SUBJECT_INFO] = $info;
                }
                break;
            case ALL :
                $info = $subjectDao->getSubject4Memcache($subjectId);
                $tData[SUBJECT_INFO] = $info;

                $result = $tData;
                break;
        }

        $memcache->setData(SUBJECT_KEY . $subjectId, $tData);
    } else {
        $info = $subjectDao->getSubject4Memcache($subjectId);
        $result[SUBJECT_INFO] = $info;
    }
    ${'tData_' . $subjectId} = $tData;
    return $result;
}

/**
 * Delete dữ liệu memcached của môn học
 *
 * @param $subjectId
 */
function deleteSubjectData($subjectId, $key = ALL, $value = null)
{
    global ${'tData_' . $subjectId};
    $tData = ${'tData_' . $subjectId};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    $memcache = new CacheMemcache();

    $result = null;
    if ($memcache->bEnabled) { // if Memcache enabled
        if (is_null($tData)) {
            $tData = $memcache->getData(SUBJECT_KEY . $subjectId);
        }

        if (!is_null($tData)) {
            switch ($key) {
                case SUBJECT_INFO :
                    if (!is_null($value)) {
                        if (($key_del = array_search($value, $tData[$key])) !== false) {
                            unset($tData[$key][$key_del]);
                        }
                    } else {
                        unset($tData[$key]);
                    }
                    break;

//                case TEACHER_CLASSES :
//                    if (is_array($value)) {
//                        if(($key_del = array_search($value, $tData[$key][$value])) !== false) {
//                            unset($tData[$key][$key_del]);
//                        }
//                    } else {
//                        unset($tData[$key]);
//                    }
//                    break;

                case ALL :
                    $tData = null;
                    break;

            }
        }
        $memcache->setData(SUBJECT_KEY . $subjectId, $tData);
    }
    ${'tData_' . $subjectId} = $tData;

    return $result;
}

/* ADD END - ManhDD 05/04/2021 */

//---------- TEACHER - MEMCACHED ----------//

/**
 * Lấy ra data memcached của giáo viên , nhân viên.
 *
 * @param $schoolId
 * @return $sData
 */
function getTeacherData($teacherId, $getKey = ALL)
{
    global $teacher_keys, ${'tData_' . $teacherId};
    $tData = ${'tData_' . $teacherId};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    $memcache = new CacheMemcache();

    $result = null;
    if ($memcache->bEnabled) { // if Memcache enabled
        if (is_null($tData)) {
            $tData = $memcache->getData(TEACHER_KEY . $teacherId);
        }
        $keys = is_null($tData) ? array() : array_keys($tData);
        // Nếu getAll mà memcache không đủ dữ liệu thì lấy ra từ database
        if (!array_equal($keys, $teacher_keys)) {
            $tData = updateTeacherData($teacherId);
        }

        switch ($getKey) {
            case TEACHER_INFO :
                if (isset($tData[TEACHER_INFO])) {
                    $result = $tData[TEACHER_INFO];
                }
                break;

            case TEACHER_CLASSES :
                if (isset($tData[TEACHER_CLASSES])) {
                    $classIds = $tData[TEACHER_CLASSES];
                    foreach ($classIds as $classId) {
                        $class_info = getClassData($classId, CLASS_INFO);
                        if (!empty($class_info)) {
                            $result[$classId] = $class_info;
                        }
                    }
                }
                break;

            case TEACHER_SCHOOL :
                if (isset($tData[TEACHER_SCHOOL])) {
                    $schoolIds = $tData[TEACHER_SCHOOL];
                    foreach ($schoolIds as $schoolId) {
                        $school_info = getSchoolData($schoolId, SCHOOL_INFO);
                        if (!empty($school_info)) {
                            $result[$schoolId] = $school_info;
                        }
                    }
                }
                break;

            case ALL:

                //1. Lấy ra thông tin của giáo viên = key [TEACHER_INFO]
                $result[TEACHER_INFO] = $tData[TEACHER_INFO];

                //2. Lấy ra thông tin lớp của giáo viên = key [TEACHER_CLASSES]
                $classIds = $tData[TEACHER_CLASSES];
                foreach ($classIds as $classId) {
                    $class_info = getClassData($classId, CLASS_INFO);
                    if (!empty($class_info)) {
                        $result[TEACHER_CLASSES][$classId] = $class_info;
                    }
                }

                //3. Lấy ra thông tin trường của giáo viên
                $schoolIds = $tData[TEACHER_SCHOOL];
                foreach ($schoolIds as $schoolId) {
                    $school_info = getSchoolData($schoolId, SCHOOL_INFO);
                    if (!empty($school_info)) {
                        $result[TEACHER_SCHOOL][$schoolId] = $school_info;
                    }
                }
                break;
        }
    }

    if (is_null($result)) {
        include_once(DAO_PATH . 'dao_school.php');
        include_once(DAO_PATH . 'dao_teacher.php');
        include_once(DAO_PATH . 'dao_role.php');

        $schoolDao = new SchoolDAO();
        $teacherDao = new TeacherDAO();
        $roleDao = new RoleDAO();

        switch ($getKey) {
            case TEACHER_INFO :
                $info = $teacherDao->getTeacher4Memcache($teacherId);
                $tData[TEACHER_INFO] = $info;
                $result = $info;
                break;

            case TEACHER_CLASSES :
                $classes = $teacherDao->getClasses4Memcache($teacherId);
                $tData[TEACHER_CLASSES] = $classes['ids'];
                $result = $classes['lists'];
                break;

            case TEACHER_SCHOOL :
                $schools = $schoolDao->getSchoolsHaveRole($teacherId);
                //Load tất cả quyền của user với từng trường.
                $schoolIds = array();
                $newSchools = array();
                foreach ($schools as $school) {
                    $schoolIds[] = $school['page_id'];
                    $newSchool = $school;
                    if ($school['role_id'] == -1) {
                        $newSchool['is_admin'] = true;
                        $newSchool['is_teacher'] = false;
                    } else if ($school['role_id'] == 0) {
                        $newSchool['is_admin'] = false;
                        $newSchool['is_teacher'] = true;
                    } else {
                        $newSchool['is_admin'] = false;
                        $newSchool['is_teacher'] = false;
                        $newSchool['permissions'] = $roleDao->getPermissionOfSchool($school['page_id'], $teacherId);
                    }
                    $newSchools[] = $newSchool;
                }

                $tData[TEACHER_SCHOOL] = $schoolIds;
                $result = $newSchools;
                break;

            case ALL :
                $info = $teacherDao->getTeacher4Memcache($teacherId);
                $classes = $teacherDao->getClasses4Memcache($teacherId);
                //Load tất cả quyền của user với từng trường.
                $schools = $schoolDao->getSchoolsHaveRole($teacherId);
                $schoolIds = array();
                $newSchools = array();
                foreach ($schools as $school) {
                    $schoolIds[] = $school['page_id'];
                    $newSchool = $school;
                    if ($school['role_id'] == -1) {
                        $newSchool['is_admin'] = true;
                        $newSchool['is_teacher'] = false;
                    } else if ($school['role_id'] == 0) {
                        $newSchool['is_admin'] = false;
                        $newSchool['is_teacher'] = true;
                    } else {
                        $newSchool['is_admin'] = false;
                        $newSchool['is_teacher'] = false;
                        $newSchool['permissions'] = $roleDao->getPermissionOfSchool($school['page_id'], $teacherId);
                    }
                    $newSchools[] = $newSchool;
                }

                $tData[TEACHER_INFO] = $info;
                $tData[TEACHER_CLASSES] = $classes['ids'];
                $tData[TEACHER_SCHOOL] = $schoolIds;

                $result[TEACHER_INFO] = $info;
                $result[TEACHER_CLASSES] = $classes['lists'];
                $result[TEACHER_SCHOOL] = $newSchools;
                break;
        }
    }
    ${'tData_' . $teacherId} = $tData;
    return $result;
}

/**
 * Add dữ liệu memcached của giáo viên
 *
 * @param $teacherId
 */
function addTeacherData($teacherId, $key = ALL, $value = null)
{
    global ${'tData_' . $teacherId}, $teacher_keys;
    $tData = ${'tData_' . $teacherId};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    include_once(DAO_PATH . 'dao_school.php');
    include_once(DAO_PATH . 'dao_class.php');
    include_once(DAO_PATH . 'dao_child.php');
    include_once(DAO_PATH . 'dao_teacher.php');
    include_once(DAO_PATH . 'dao_class.php');
    include_once(DAO_PATH . 'dao_role.php');

    $memcache = new CacheMemcache();
    $schoolDao = new SchoolDAO();
    $teacherDao = new TeacherDAO();


    //$result = null;
    if ($memcache->bEnabled) { // if Memcache enabled
        if (is_null($tData)) {
            $tData = $memcache->getData(TEACHER_KEY . $teacherId);
        }

        $keys = is_null($tData) ? array() : array_keys($tData);
        // Nếu getAll mà memcache không đủ dữ liệu thì lấy ra từ database
        if (!array_equal($keys, $teacher_keys)) {
            $tData = updateTeacherData($teacherId);
        }

        switch ($key) {
            case TEACHER_INFO :
                if (!is_null($value)) {
                    $tData[TEACHER_INFO] = $value;
                } else {
                    $info = $teacherDao->getTeacher4Memcache($teacherId);
                    $tData[TEACHER_INFO] = $info;
                }
                //$result = $tData[TEACHER_INFO];
                break;

            case TEACHER_CLASSES :
                if (!is_null($value) && is_array($tData[TEACHER_CLASSES])) {
                    $classIds = $tData[TEACHER_CLASSES];
                    $classIds[] = $value;
                    $tData[TEACHER_CLASSES] = array_unique($classIds);
                } else {
                    $classes = $teacherDao->getClasses4Memcache($teacherId);
                    $tData[TEACHER_CLASSES] = $classes['ids'];
                    //$result = $classes['lists'];
                }
                break;

            case TEACHER_SCHOOL :
                if (!is_null($value) && is_array($tData[TEACHER_SCHOOL])) {
                    $schoolIds = $tData[TEACHER_SCHOOL];
                    $schoolIds[] = $value;
                    $tData[TEACHER_SCHOOL] = array_unique($schoolIds);
                } else {
                    $schools = $schoolDao->getSchoolsHaveRole($teacherId);
                    //Load tất cả quyền của user với từng trường.
                    $schoolIds = array();
                    foreach ($schools as $school) {
                        $schoolIds[] = $school['page_id'];
                    }

                    $tData[TEACHER_SCHOOL] = $schoolIds;
                    //$result = $newSchools;
                }
                break;

        }

        $memcache->setData(TEACHER_KEY . $teacherId, $tData);
    }
    ${'tData_' . $teacherId} = $tData;
    //return $result;
}

/**
 * Update dữ liệu memcached của giáo viên
 *
 * @param $teacherId
 */
function updateTeacherData($teacherId, $key = ALL, $value = null)
{
    if (!is_numeric($teacherId) || $teacherId <= 0) {
        return null;
    }

    global ${'tData_' . $teacherId};
    $tData = ${'tData_' . $teacherId};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    include_once(DAO_PATH . 'dao_school.php');
    include_once(DAO_PATH . 'dao_teacher.php');
    include_once(DAO_PATH . 'dao_role.php');

    $memcache = new CacheMemcache();
    $schoolDao = new SchoolDAO();
    $teacherDao = new TeacherDAO();
    $roleDao = new RoleDAO();

    $result = null;
    if ($memcache->bEnabled) { // if Memcache enabled
        if (is_null($tData)) {
            $tData = $memcache->getData(TEACHER_KEY . $teacherId);
        }

        switch ($key) {
            case TEACHER_INFO :
                if (!is_null($value) && isset($tData[TEACHER_INFO])) {
                    foreach ($value as $k => $v) {
                        $tData[TEACHER_INFO][$k] = $v;
                    }
                } else {
                    $info = $teacherDao->getTeacher4Memcache($teacherId);
                    $tData[TEACHER_INFO] = $info;
                }
                //$result = $tData[TEACHER_INFO];
                break;

            case TEACHER_CLASSES :
                if (!is_null($value) && isset($tData[TEACHER_CLASSES])) {
                    $classIds = $tData[TEACHER_CLASSES];
                    updateClassData($value);
                    if (!in_array($value, $classIds)) {
                        $classes = $teacherDao->getClasses4Memcache($teacherId);
                        $tData[TEACHER_CLASSES] = $classes['ids'];
                    }
                } else {
                    $classes = $teacherDao->getClasses4Memcache($teacherId);
                    $tData[TEACHER_CLASSES] = $classes['ids'];
                }
                $result = $tData[TEACHER_CLASSES];
                break;

            case TEACHER_SCHOOL :
                if (!is_null($value) && isset($tData[TEACHER_SCHOOL])) {
                    $schoolIds = $tData[TEACHER_SCHOOL];
                    updateSchoolData($value);
                    if (!in_array($value, $schoolIds)) {
                        $schools = $schoolDao->getSchoolsHaveRole($teacherId);
                        //Load tất cả quyền của user với từng trường.
                        $schoolIds = array();
                        foreach ($schools as $school) {
                            $schoolIds[] = $school['page_id'];
                        }

                        $tData[TEACHER_SCHOOL] = $schoolIds;
                    }
                } else {
                    $schools = $schoolDao->getSchoolsHaveRole($teacherId);
                    //Load tất cả quyền của user với từng trường.
                    $schoolIds = array();
                    foreach ($schools as $school) {
                        $schoolIds[] = $school['page_id'];
                    }

                    $tData[TEACHER_SCHOOL] = $schoolIds;
                }
                $result = $tData[TEACHER_SCHOOL];
                break;

            case ALL :
                $info = $teacherDao->getTeacher4Memcache($teacherId);
                $classes = $teacherDao->getClasses4Memcache($teacherId);

                $schools = $schoolDao->getSchoolsHaveRole($teacherId);
                $schoolIds = array();
                foreach ($schools as $school) {
                    $schoolIds[] = $school['page_id'];
                }

                $tData[TEACHER_INFO] = $info;
                $tData[TEACHER_CLASSES] = $classes['ids'];
                $tData[TEACHER_SCHOOL] = $schoolIds;

                $result = $tData;
                break;
        }

        $memcache->setData(TEACHER_KEY . $teacherId, $tData);
    } else {
        $info = $teacherDao->getTeacher4Memcache($teacherId);
        $classes = $teacherDao->getClasses4Memcache($teacherId);

        $schools = $schoolDao->getSchoolsHaveRole($teacherId);
        $schoolIds = array();
        foreach ($schools as $school) {
            $schoolIds[] = $school['page_id'];
        }

        $result[TEACHER_INFO] = $info;
        $result[TEACHER_CLASSES] = $classes['lists'];
        $result[TEACHER_SCHOOL] = $schoolIds;
    }
    ${'tData_' . $teacherId} = $tData;
    return $result;
}

/**
 * Delete dữ liệu memcached của giáo viên
 *
 * @param $teacherId
 */
function deleteTeacherData($teacherId, $key = ALL, $value = null)
{
    global ${'tData_' . $teacherId};
    $tData = ${'tData_' . $teacherId};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    $memcache = new CacheMemcache();

    $result = null;
    if ($memcache->bEnabled) { // if Memcache enabled
        if (is_null($tData)) {
            $tData = $memcache->getData(TEACHER_KEY . $teacherId);
        }

        if (!is_null($tData)) {
            switch ($key) {
                case TEACHER_INFO :
                    if (!is_null($value)) {
                        if (($key_del = array_search($value, $tData[$key])) !== false) {
                            unset($tData[$key][$key_del]);
                        }
                    } else {
                        unset($tData[$key]);
                    }
                    break;

                case TEACHER_CLASSES :
                    if (is_array($value)) {
                        if (($key_del = array_search($value, $tData[$key][$value])) !== false) {
                            unset($tData[$key][$key_del]);
                        }
                    } else {
                        unset($tData[$key]);
                    }
                    break;

                case ALL :
                    $tData = null;
                    break;

            }
        }
        $memcache->setData(TEACHER_KEY . $teacherId, $tData);
    }
    ${'tData_' . $teacherId} = $tData;

    return $result;
}

//---------- CHILD - MEMCACHED ----------//

/**
 * Lấy ra data memcached của trẻ.
 *
 * @param $schoolId
 * @return $sData
 */
function getChildData($childId, $getKey = ALL)
{
    global $child_keys, ${'chData_' . $childId};
    $chData = ${'chData_' . $childId};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    $memcache = new CacheMemcache();

    $result = null;
    if ($memcache->bEnabled) { // if Memcache enabled
        if (is_null($chData)) {
            $chData = $memcache->getData(CHILD_KEY . $childId);
        }

        $keys = is_null($chData) ? array() : array_keys($chData);
        // Nếu getAll mà memcache không đủ dữ liệu thì lấy ra từ database
        if (!array_equal($keys, $child_keys)) {
            $chData = updateChildData($childId);
        }

        switch ($getKey) {
            // Thông tin cơ bản của trẻ
            case CHILD_INFO :
                if (isset($chData[CHILD_INFO])) {
                    $result = $chData[CHILD_INFO];
                }
                break;

            // Phụ huynh của trẻ
            case CHILD_PARENTS :
                if (isset($chData[CHILD_PARENTS])) {
                    $result = $chData[CHILD_PARENTS];
                }
                break;

            // Thông tin lớp của trẻ
            case CHILD_CLASS :
                if (isset($chData[CHILD_CLASS])) {
                    $classId = $chData[CHILD_CLASS];
                    if ($classId > 0) {
                        $class_info = getClassData($classId, CLASS_INFO);
                        $result = $class_info;
                    }
                }
                break;

            // Thông tin trường của trẻ
            case CHILD_SCHOOL :
                if (isset($chData[CHILD_SCHOOL])) {
                    $schoolId = $chData[CHILD_SCHOOL];
                    if ($schoolId > 0) {
                        $school_info = getSchoolData($schoolId, SCHOOL_INFO);
                        $result = $school_info;
                    }
                }
                break;

            // Thông tin người đón trẻ
            case CHILD_PICKER_INFO :
                if (isset($chData[CHILD_PICKER_INFO])) {
                    $result = $chData[CHILD_PICKER_INFO];
                }
                break;

            case ALL:

                $result[CHILD_INFO] = $chData[CHILD_INFO];
                $result[CHILD_PARENTS] = $chData[CHILD_PARENTS];

                // Thông tin lớp của trẻ
                $classId = $chData[CHILD_CLASS];
                if ($classId > 0) {
                    $class_info = getClassData($classId, CLASS_INFO);
                    $result[CHILD_CLASS] = $class_info;
                }

                // Thông tin trường của trẻ
                $schoolId = $chData[CHILD_SCHOOL];
                if ($schoolId > 0) {
                    $school_info = getSchoolData($schoolId, SCHOOL_INFO);
                    $result[CHILD_SCHOOL] = $school_info;
                }

                // Thông tin người đón trẻ
                $result[CHILD_PICKER_INFO] = $chData[CHILD_PICKER_INFO];
                break;

        }
    }

    if (is_null($result)) {
        include_once(DAO_PATH . 'dao_class.php');
        include_once(DAO_PATH . 'dao_child.php');
        include_once(DAO_PATH . 'dao_class.php');

        $classDao = new ClassDAO();
        $childDao = new ChildDAO();

        switch ($getKey) {
            case CHILD_INFO :
                $result = $childDao->getChild($childId);
                break;

            case CHILD_PARENTS :
                $result = $childDao->getParent4Memcache($childId);
                break;

            case CHILD_CLASS :
                $classes = $classDao->getClassOfChild4Memcache($childId);
                $result = $classes['info'];
                break;

            case CHILD_SCHOOL :
                $school = $childDao->getCurrentSchoolOfChild4Memcache($childId);
                $result = $school['info'];
                break;

            case CHILD_PICKER_INFO :
                $result = $childDao->getChildInformation($childId);
                break;

            case ALL :
                $info = $childDao->getChild($childId);
                $parents = $childDao->getParent4Memcache($childId);
                $classes = $classDao->getClassOfChild4Memcache($childId);
                $school = $childDao->getCurrentSchoolOfChild4Memcache($childId);
                $picker_info = $childDao->getChildInformation($childId);

                $result[CHILD_INFO] = $info;
                $result[CHILD_PARENTS] = $parents;
                $result[CHILD_SCHOOL] = $school['info'];
                $result[CHILD_CLASS] = $classes['info'];
                $result[CHILD_PICKER_INFO] = $picker_info;
                break;
        }
    }
    ${'chData_' . $childId} = $chData;

    return $result;
}

/* Lấy ra thông tin trẻ từ userId và childId */
function getChildDataById($childId, $getKey = ALL, $type = null)
{
    global $relatedObjects;
    $result = null;
    if (is_null($relatedObjects)) {
        $relatedObjects = getRelatedObjects();
    }
    if ($type) {
        foreach ($relatedObjects['itself'] as $child) {
            if ($child['child_id'] == $childId) {
                $result = getChildData($childId, $getKey);

            }
        }
    } else {
        foreach ($relatedObjects['children'] as $child) {
            if ($child['child_id'] == $childId) {
                $result = getChildData($childId, $getKey);
            }
        }
    }
    return $result;
}

/**
 * Add dữ liệu memcached của trẻ
 *
 * @param $teacherId
 */
function addChildData($childId, $key = ALL, $value = null)
{
    global ${'chData_' . $childId}, $child_keys;
    $chData = ${'chData_' . $childId};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    $memcache = new CacheMemcache();

    //$result = null;
    if ($memcache->bEnabled) { // if Memcache enabled
        if (is_null($chData)) {
            $chData = $memcache->getData(CHILD_KEY . $childId);
        }

        $keys = is_null($chData) ? array() : array_keys($chData);
        // Nếu getAll mà memcache không đủ dữ liệu thì lấy ra từ database
        if (!array_equal($keys, $child_keys)) {
            $chData = updateChildData($childId);
        }

        switch ($key) {
            case CHILD_INFO :
                if (!is_null($value)) {
                    $chData[CHILD_INFO] = $value;
                }
                break;

            case CHILD_PARENTS :
                if (!is_null($value)) {
                    foreach ($value as $parentId => $parentInfo) {
                        $chData[CHILD_PARENTS][$parentId] = $parentInfo;
                    }
                }
                break;

            case CHILD_CLASS :
                if (!is_null($value)) {
                    updateClassData($value);
                    $chData[CHILD_CLASS] = $value;
                }
                break;

            case CHILD_SCHOOL :
                if (!is_null($value)) {
                    updateSchoolData($value);
                    $chData[CHILD_SCHOOL] = $value;
                }
                break;

            case CHILD_PICKER_INFO :
                if (!is_null($value)) {
                    $chData[CHILD_PICKER_INFO] = $value;
                }
                break;
        }
        $memcache->setData(CHILD_KEY . $childId, $chData);
    }
    ${'chData_' . $childId} = $chData;
}

/**
 * Update dữ liệu memcached của trẻ
 *
 * @param $teacherId
 */
function updateChildData($childId, $key = ALL, $value = null)
{
    if (!is_numeric($childId) || $childId <= 0) {
        return null;
    }
    global ${'chData_' . $childId}, $child_keys;
    $chData = ${'chData_' . $childId};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    include_once(DAO_PATH . 'dao_class.php');
    include_once(DAO_PATH . 'dao_child.php');
    include_once(DAO_PATH . 'dao_class.php');

    $memcache = new CacheMemcache();
    $classDao = new ClassDAO();
    $childDao = new ChildDAO();

    $result = null;
    if ($memcache->bEnabled) { // if Memcache enabled
        if (is_null($chData)) {
            $chData = $memcache->getData(CHILD_KEY . $childId);
        }

        switch ($key) {
            case CHILD_INFO :
                if (!is_null($value) && isset($chData[CHILD_INFO])) {
                    foreach ($value as $k => $v) {
                        $chData[CHILD_INFO][$k] = $v;
                    }
                } else {
                    $info = $childDao->getChild($childId);
                    $chData[CHILD_INFO] = $info;
                }
                $result = $chData[CHILD_INFO];
                break;

            case CHILD_PARENTS :
                if (!is_null($value) && isset($chData[CHILD_PARENTS])) {
                    $parentIds = $chData[CHILD_PARENTS];
                    foreach ($value as $parentId => $parentInfo) {
                        if (!in_array($parentId, $parentIds)) {
                            $parents = $childDao->getParent4Memcache($childId);
                            $chData[CHILD_PARENTS] = $parents;
                            break;
                        } else {
                            foreach ($parentInfo as $k => $v) {
                                $chData[CHILD_PARENTS][$parentId][$k] = $v;
                            }
                        }
                    }
                } else {
                    $parents = $childDao->getParent4Memcache($childId);
                    $chData[CHILD_PARENTS] = $parents;
                }
                $result = $chData[CHILD_PARENTS];
                break;

            case CHILD_CLASS :
                if (!is_null($value) && isset($chData[CHILD_CLASS])) {
                    $classId = $chData[CHILD_CLASS];
                    if ($value == $classId) {
                        updateClassData($value);
                    } else {
                        $classes = $classDao->getClassOfChild4Memcache($childId);
                        $chData[CHILD_CLASS] = $classes['id'];
                    }
                } else {
                    $classes = $classDao->getClassOfChild4Memcache($childId);
                    $chData[CHILD_CLASS] = $classes['id'];
                }
                $result = $chData[CHILD_CLASS];
                break;

            case CHILD_SCHOOL :
                if (!is_null($value) && isset($chData[CHILD_SCHOOL])) {
                    $schoolId = $chData[CHILD_SCHOOL];
                    if ($value == $schoolId) {
                        updateSchoolData($value);
                    } else {
                        $school = $childDao->getCurrentSchoolOfChild4Memcache($childId);
                        $chData[CHILD_SCHOOL] = $school['id'];
                    }
                } else {
                    $school = $childDao->getCurrentSchoolOfChild4Memcache($childId);
                    $chData[CHILD_SCHOOL] = $school['id'];
                }
                $result = $chData[CHILD_SCHOOL];
                break;

            case CHILD_PICKER_INFO :
                if (!is_null($value) && isset($chData[CHILD_PICKER_INFO])) {
                    foreach ($value as $k => $v) {
                        $chData[CHILD_PICKER_INFO][$k] = $v;
                    }
                } else {
                    $picker_info = $childDao->getChildInformation($childId);
                    $chData[CHILD_PICKER_INFO] = $picker_info;
                }
                $result = $chData[CHILD_PICKER_INFO];
                break;

            case ALL :
                $info = $childDao->getChild($childId);
                $school = $childDao->getCurrentSchoolOfChild4Memcache($childId);
                $classes = $classDao->getClassOfChild4Memcache($childId);
                $parents = $childDao->getParent4Memcache($childId);
                $picker_info = $childDao->getChildInformation($childId);
                $chData[CHILD_INFO] = $info;
                $chData[CHILD_SCHOOL] = $school['id'];
                $chData[CHILD_CLASS] = $classes['id'];
                $chData[CHILD_PARENTS] = $parents;
                $chData[CHILD_PICKER_INFO] = $picker_info;

                $result = $chData;
                break;
        }

        $memcache->setData(CHILD_KEY . $childId, $chData);
    } else {
        $info = $childDao->getChild($childId);
        $school = $childDao->getCurrentSchoolOfChild4Memcache($childId);
        $classes = $classDao->getClassOfChild4Memcache($childId);
        $parents = $childDao->getParent4Memcache($childId);
        $picker_info = $childDao->getChildInformation($childId);
        $result[CHILD_INFO] = $info;
        $result[CHILD_SCHOOL] = $school['id'];
        $result[CHILD_CLASS] = $classes['id'];
        $result[CHILD_PARENTS] = $parents;
        $result[CHILD_PICKER_INFO] = $picker_info;
    }
    ${'chData_' . $childId} = $chData;
    return $result;
}


/**
 * Delete dữ liệu memcached của trẻ
 *
 * @param $childId
 */
function deleteChildData($childId, $key = ALL, $value = null)
{
    global ${'chData_' . $childId};
    $chData = ${'chData_' . $childId};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    $memcache = new CacheMemcache();

    //$result = null;
    if ($memcache->bEnabled) { // if Memcache enabled
        if (is_null($chData)) {
            $chData = $memcache->getData(CHILD_KEY . $childId);
        }

        if (!is_null($chData)) {
            switch ($key) {
                case CHILD_INFO :
                    unset($chData[$key]);
                    break;

                case CHILD_SCHOOL :
                    unset($chData[$key]);
                    break;

                case CHILD_CLASS :
                    unset($chData[$key]);
                    break;

                case CHILD_PARENTS :
                    if (!is_null($value)) {
                        if (($key_del = array_search($value, $chData[$key])) !== false) {
                            unset($chData[$key][$key_del]);
                        }
                    } else {
                        unset($chData[$key]);
                    }
                    break;

                case CHILD_PICKER_INFO :
                    unset($chData[$key]);
                    break;

                case ALL :
                    $chData = null;
                    break;

            }
        }
        $memcache->setData(CHILD_KEY . $childId, $chData);
    }
    ${'chData_' . $childId} = $chData;
    //return $result;
}

//---------- USER MANAGE - MEMCACHED ----------//

/**
 * Lấy ra data memcached của 1 đối tượng quản lý
 *
 * @param $schoolId
 * @return $sData
 */
function getUserManageData($userId, $getKey = ALL)
{
    global $user_manage_keys, ${'umData_' . $userId};
    $umData = ${'umData_' . $userId};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    $memcache = new CacheMemcache();
    $result = null;
    if ($memcache->bEnabled) { // if Memcache enabled
        if (is_null($umData)) {
            $umData = $memcache->getData(USER_MANAGE_KEY . $userId);
        }
        $keys = is_null($umData) ? array() : array_keys($umData);
        // Nếu getAll mà memcache không đủ dữ liệu thì lấy ra từ database
        if (!array_equal($keys, $user_manage_keys)) {
            $umData = updateUserManageData($userId);
        }

        switch ($getKey) {
            case USER_MANAGE_SCHOOL :
                if (isset($umData[USER_MANAGE_SCHOOL])) {
                    $schools = $umData[USER_MANAGE_SCHOOL];
                    foreach ($schools as $schoolId => $schoolData) {
                        $school_info = getSchoolData($schoolId, SCHOOL_DATA);
                        if (!empty($school_info)) {
                            $result[$schoolId] = array_merge($schoolData, $school_info);
                        }
                    }
                }
                break;

            case USER_MANAGE_CLASSES :
                if (isset($umData[USER_MANAGE_CLASSES])) {
                    $classIds = $umData[USER_MANAGE_CLASSES];

                    foreach ($classIds as $classId) {
                        $class_info = getClassData($classId, CLASS_INFO);
                        if (!empty($class_info)) {
                            $result[$classId] = $class_info;
                        }
                    }
                }
                break;

            case USER_MANAGE_CHILDREN :
                if (isset($umData[USER_MANAGE_CHILDREN])) {
                    $childIds = $umData[USER_MANAGE_CHILDREN];

                    foreach ($childIds as $childId) {
                        $child_info = getChildData($childId, CHILD_INFO);
                        if (!empty($child_info)) {
                            $result[$childId] = $child_info;
                        }
                    }
                }
                break;
            case USER_MANAGE_CHILDREN_ITSELF :
                if (isset($umData[USER_MANAGE_CHILDREN_ITSELF])) {
                    $childIds = $umData[USER_MANAGE_CHILDREN_ITSELF];

                    foreach ($childIds as $childId) {
                        $child_info = getChildData($childId, CHILD_INFO);
                        if (!empty($child_info)) {
                            $result[$childId] = $child_info;
                        }
                    }
                }
                break;
            case ALL:
                $result[USER_MANAGE_SCHOOL] = array();
                $result[USER_MANAGE_CLASSES] = array();
                $result[USER_MANAGE_CHILDREN] = array();
                $result[USER_MANAGE_CHILDREN_ITSELF] = array();

                //1. Lấy ra thông tin của trường mà user quản lý, key [USER_MANAGE_SCHOOL]
                $schools = $umData[USER_MANAGE_SCHOOL];
                foreach ($schools as $schoolId => $schoolData) {
                    $school_info = getSchoolData($schoolId, SCHOOL_DATA);
                    if (!empty($school_info)) {
                        $result[USER_MANAGE_SCHOOL][$schoolId] = array_merge($schoolData, $school_info);
                    }
                }

                //2. Lấy ra thông tin lớp mà user quản lý, key [TEACHER_CLASSES]
                $classIds = $umData[USER_MANAGE_CLASSES];

                foreach ($classIds as $classId) {
                    $class_info = getClassData($classId, CLASS_INFO);
                    if (!empty($class_info)) {
                        $result[USER_MANAGE_CLASSES][$classId] = $class_info;
                    }
                }

                //3. Lấy ra thông tin trẻ mà user quản lý, key [USER_MANAGE_CHILDREN]
                $childIds = $umData[USER_MANAGE_CHILDREN];

                foreach ($childIds as $childId) {
                    $child_info = getChildData($childId, CHILD_INFO);
                    if (!empty($child_info)) {
                        $result[USER_MANAGE_CHILDREN][$childId] = $child_info;
                    }
                }
                //3. Lấy ra thông tin bản thân, key [USER_MANAGE_CHILDREN_ITSELF]
                $childIds = $umData[USER_MANAGE_CHILDREN_ITSELF];

                foreach ($childIds as $childId) {
                    $child_info = getChildData($childId, CHILD_INFO);
                    if (!empty($child_info)) {
                        $result[USER_MANAGE_CHILDREN_ITSELF][$childId] = $child_info;
                    }
                }

                break;
        }
    }

    if (is_null($result)) {
        include_once(DAO_PATH . 'dao_school.php');
        include_once(DAO_PATH . 'dao_class.php');
        include_once(DAO_PATH . 'dao_child.php');
        include_once(DAO_PATH . 'dao_role.php');

        $schoolDao = new SchoolDAO();
        $classDao = new ClassDAO();
        $childDao = new ChildDAO();
        $roleDao = new RoleDao();

        switch ($getKey) {
            case USER_MANAGE_SCHOOL :
                $schools = $schoolDao->getSchoolsHaveRole4Memcache($userId);
                //Load tất cả quyền của user với từng trường.
                //$newSchools = array();
                foreach ($schools as $school) {
                    $schoolData = getSchoolData($school['page_id'], SCHOOL_DATA);
                    $principals = array();
                    if (!is_empty($schoolData['principal'])) {
                        $principals = explode(',', $schoolData['principal']);
                    }
                    $newSchool = $school;
                    if ($school['role_id'] == -1) {
                        $newSchool['is_admin'] = true;
                        $newSchool['is_teacher'] = false;
                    } else if (($school['role_id'] == 0) && !in_array($userId, $principals)) {
                        $newSchool['is_admin'] = false;
                        $newSchool['is_teacher'] = true;
                    } else {
                        $newSchool['is_admin'] = false;
                        $newSchool['is_teacher'] = false;
                        $newSchool['permissions'] = $roleDao->getPermissionOfSchool($school['page_id'], $userId);
                    }
                    //$newSchools[$newSchool['page_id']] = $newSchool;
                    $result[$newSchool['page_id']] = array_merge($schoolData, $newSchool);
                }
                break;

            case USER_MANAGE_CLASSES :
                $classes = $classDao->getClasses4Memcache($userId);
                $result = $classes['lists'];
                break;

            case USER_MANAGE_CHILDREN :
                $children = $childDao->getChildren4Memcache($userId);
                $result = $children['lists'];
                break;
            case USER_MANAGE_CHILDREN_ITSELF :
                $children_itself = $childDao->getChildren4Memcache($userId, 0);
                $result = $children_itself['lists'];
                break;

            case ALL :
                $schools = $schoolDao->getSchoolsHaveRole4Memcache($userId);
                //Load tất cả quyền của user với từng trường.
                //$newSchools = array();
                $data = array();
                foreach ($schools as $school) {
                    $schoolData = getSchoolData($school['page_id'], SCHOOL_DATA);
                    $principals = array();
                    if (!is_empty($schoolData['principal'])) {
                        $principals = explode(',', $schoolData['principal']);
                    }
                    $newSchool = $school;
                    if ($school['role_id'] == -1) {
                        $newSchool['is_admin'] = true;
                        $newSchool['is_teacher'] = false;
                    } else if ($school['role_id'] == 0 && !in_array($userId, $principals)) {
                        $newSchool['is_admin'] = false;
                        $newSchool['is_teacher'] = true;
                    } else {
                        $newSchool['is_admin'] = false;
                        $newSchool['is_teacher'] = false;
                        $newSchool['permissions'] = $roleDao->getPermissionOfSchool($school['page_id'], $userId);
                    }
                    //$newSchools[$newSchool['page_id']] = $newSchool;
                    $data[$newSchool['page_id']] = array_merge($schoolData, $newSchool);
                }

                $classes = $classDao->getClasses4Memcache($userId);
                $children = $childDao->getChildren4Memcache($userId);
                $children_itself = $childDao->getChildren4Memcache($userId, 0);
                $result[USER_MANAGE_SCHOOL] = $data;
                $result[USER_MANAGE_CLASSES] = $classes['lists'];
                $result[USER_MANAGE_CHILDREN] = $children['lists'];
                $result[USER_MANAGE_CHILDREN_ITSELF] = $children_itself['lists'];
                break;

        }
    }
    ${'umData_' . $userId} = $umData;
    return $result;
}


/**
 * Add dữ liệu memcached của đối tượng user quản lý
 *
 * @param $userId
 */
function addUserManageData($userId, $key = ALL, $value = null)
{
    global ${'umData_' . $userId}, $user_manage_keys;
    $umData = ${'umData_' . $userId};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    $memcache = new CacheMemcache();

    //$result = null;
    if ($memcache->bEnabled) { // if Memcache enabled
        if (is_null($umData)) {
            $umData = $memcache->getData(USER_MANAGE_KEY . $userId);
        }

        $keys = is_null($umData) ? array() : array_keys($umData);
        // Nếu getAll mà memcache không đủ dữ liệu thì lấy ra từ database
        if (!array_equal($keys, $user_manage_keys)) {
            $umData = updateUserManageData($userId);
        }

        switch ($key) {
            case USER_MANAGE_SCHOOL :
                if (!is_null($value)) {
                    foreach ($value as $schoolId => $schoolData) {
                        $umData[USER_MANAGE_SCHOOL][$schoolId] = $schoolData;
                    }
                }
                //$result = $umData[USER_MANAGE_SCHOOL];
                break;

            case USER_MANAGE_CLASSES :
                if (!is_null($value) && is_array($umData[USER_MANAGE_CLASSES])) {
                    $classIds = $umData[USER_MANAGE_CLASSES];
                    $classIds[] = $value;
                    updateClassData($value);

                    $umData[USER_MANAGE_CLASSES] = array_unique($classIds);
                }
                break;

            case USER_MANAGE_CHILDREN :
                if (!is_null($value) && is_array($umData[USER_MANAGE_CHILDREN])) {
                    $childIds = $umData[USER_MANAGE_CHILDREN];
                    $childIds[] = $value;
                    updateChildData($value);

                    $umData[USER_MANAGE_CHILDREN] = array_unique($childIds);
                }
                break;

        }

        $memcache->setData(USER_MANAGE_KEY . $userId, $umData);
    }
    ${'umData_' . $userId} = $umData;
    //return $result;
}

/**
 * Set dữ liệu memcached của đối tượng user quản lý
 *
 * @param $userId
 */
function updateUserManageData($userId, $key = ALL, $value = null)
{
    if (!is_numeric($userId) || $userId <= 0) {
        return null;
    }

    global ${'umData_' . $userId};
    $umData = ${'umData_' . $userId};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    include_once(DAO_PATH . 'dao_school.php');
    include_once(DAO_PATH . 'dao_class.php');
    include_once(DAO_PATH . 'dao_child.php');
    include_once(DAO_PATH . 'dao_role.php');

    $memcache = new CacheMemcache();
    $schoolDao = new SchoolDAO();
    $classDao = new ClassDAO();
    $childDao = new ChildDAO();
    $roleDao = new RoleDao();
    $result = null;
    if ($memcache->bEnabled) { // if Memcache enabled
        if (is_null($umData)) {
            $umData = $memcache->getData(USER_MANAGE_KEY . $userId);
        }

        switch ($key) {
            case USER_MANAGE_SCHOOL :
                if (!is_null($value) && isset($umData[USER_MANAGE_SCHOOL])) {
                    foreach ($value as $schoolId => $schoolData) {
                        foreach ($schoolData as $k => $v) {
                            $umData[USER_MANAGE_SCHOOL][$schoolId][$k] = $v;
                        }
                    }
                } else {
                    $schools = $schoolDao->getSchoolsHaveRole4Memcache($userId);
                    //Load tất cả quyền của user với từng trường.
                    $newSchools = array();
                    foreach ($schools as $school) {
                        //$schoolData = getSchoolData($school['page_id'], SCHOOL_DATA);
                        $schoolData = getSchoolData($school['page_id'], SCHOOL_DATA);
                        $principals = array();
                        if (!is_empty($schoolData['principal'])) {
                            $principals = explode(',', $schoolData['principal']);
                        }
                        $newSchool = $school;
                        if ($school['role_id'] == -1) {
                            $newSchool['is_admin'] = true;
                            $newSchool['is_teacher'] = false;
                        } else if (($school['role_id'] == 0) && !in_array($userId, $principals)) {
                            $newSchool['is_admin'] = false;
                            $newSchool['is_teacher'] = true;
                        } else {
                            $newSchool['is_admin'] = false;
                            $newSchool['is_teacher'] = false;
                            $newSchool['permissions'] = $roleDao->getPermissionOfSchool($school['page_id'], $userId);
                        }
                        $newSchools[$newSchool['page_id']] = $newSchool;
                        //$result[$newSchool['page_id']] = array_merge($schoolData, $newSchool);
                    }

                    $umData[USER_MANAGE_SCHOOL] = $newSchools;
                }
                $result = $umData[USER_MANAGE_SCHOOL];
                break;

            case USER_MANAGE_CLASSES :
                if (!is_null($value) && isset($umData[USER_MANAGE_CLASSES])) {
                    $classIds = $umData[USER_MANAGE_CLASSES];
                    updateClassData($value);
                    if (!in_array($value, $classIds)) {
                        $classes = $classDao->getClasses4Memcache($userId);
                        $umData[USER_MANAGE_CLASSES] = $classes['ids'];
                    }
                    $umData[USER_MANAGE_CLASSES] = $classIds;
                } else {
                    $classes = $classDao->getClasses4Memcache($userId);
                    $umData[USER_MANAGE_CLASSES] = $classes['ids'];
                    //$result = $classes['lists'];
                }
                $result = $umData[USER_MANAGE_CLASSES];
                break;

            case USER_MANAGE_CHILDREN :
                if (!is_null($value) && isset($umData[USER_MANAGE_CHILDREN])) {
                    $childIds = $umData[USER_MANAGE_CHILDREN];
                    updateChildData($value);
                    if (!in_array($value, $childIds)) {
                        $children = $childDao->getChildren4Memcache($userId);
                        $umData[USER_MANAGE_CHILDREN] = $children['ids'];
                    }
                    $umData[USER_MANAGE_CHILDREN] = $childIds;
                } else {
                    $children = $childDao->getChildren4Memcache($userId);
                    $umData[USER_MANAGE_CHILDREN] = $children['ids'];
                    //$result = $children['lists'];
                }
                $result = $umData[USER_MANAGE_CHILDREN];
                break;
            case USER_MANAGE_CHILDREN_ITSELF :
                if (!is_null($value) && isset($umData[USER_MANAGE_CHILDREN_ITSELF])) {
                    $childIds = $umData[USER_MANAGE_CHILDREN_ITSELF];
                    updateChildData($value);
                    if (!in_array($value, $childIds)) {
                        $children = $childDao->getChildren4Memcache($userId, 0);
                        $umData[USER_MANAGE_CHILDREN_ITSELF] = $children['ids'];
                    }
                    $umData[USER_MANAGE_CHILDREN_ITSELF] = $childIds;
                } else {
                    $children = $childDao->getChildren4Memcache($userId, 0);
                    $umData[USER_MANAGE_CHILDREN_ITSELF] = $children['ids'];
                    //$result = $children['lists'];
                }
                $result = $umData[USER_MANAGE_CHILDREN_ITSELF];
                break;
            case ALL :
                //1.Cập nhật thông tin quản lý trường của 1 user
                $schools = $schoolDao->getSchoolsHaveRole4Memcache($userId);
                //Load tất cả quyền của user với từng trường.
                $newSchools = array();
                foreach ($schools as $school) {
                    //$schoolData = getSchoolData($school['page_id'], SCHOOL_DATA);
                    $schoolData = getSchoolData($school['page_id'], SCHOOL_DATA);
                    $principals = array();
                    if (!is_empty($schoolData['principal'])) {
                        $principals = explode(',', $schoolData['principal']);
                    }
                    $newSchool = $school;
                    if ($school['role_id'] == -1) {
                        $newSchool['is_admin'] = true;
                        $newSchool['is_teacher'] = false;
                    } else if (($school['role_id'] == 0) && !in_array($userId, $principals)) {
                        $newSchool['is_admin'] = false;
                        $newSchool['is_teacher'] = true;
                    } else {
                        $newSchool['is_admin'] = false;
                        $newSchool['is_teacher'] = false;
                        $newSchool['permissions'] = $roleDao->getPermissionOfSchool($school['page_id'], $userId);
                    }
                    $newSchools[$newSchool['page_id']] = $newSchool;
                    //$result[$newSchool['page_id']] = array_merge($schoolData, $newSchool);
                }

                //2.Cập nhật thông tin quản lý lớp của 1 user
                $classes = $classDao->getClasses4Memcache($userId);

                //3.Cập nhật thông tin quản lý trẻ của 1 user
                $children = $childDao->getChildren4Memcache($userId);

                //4.Cập nhật thông tin quản lý bản thân của 1 user
                $children_itself = $childDao->getChildren4Memcache($userId,0);
                $umData[USER_MANAGE_SCHOOL] = $newSchools;
                $umData[USER_MANAGE_CLASSES] = $classes['ids'];
                $umData[USER_MANAGE_CHILDREN] = $children['ids'];
                $umData[USER_MANAGE_CHILDREN_ITSELF] = $children_itself['ids'];
                $result = $umData;
                break;

        }

        $memcache->setData(USER_MANAGE_KEY . $userId, $umData);
    } else {
        //1.Cập nhật thông tin quản lý trường của 1 user
        $schools = $schoolDao->getSchoolsHaveRole4Memcache($userId);
        //Load tất cả quyền của user với từng trường.
        $newSchools = array();
        foreach ($schools as $school) {
            //$schoolData = getSchoolData($school['page_id'], SCHOOL_DATA);
            $schoolData = getSchoolData($school['page_id'], SCHOOL_DATA);
            $principals = array();
            if (!is_empty($schoolData['principal'])) {
                $principals = explode(',', $schoolData['principal']);
            }
            $newSchool = $school;
            if ($school['role_id'] == -1) {
                $newSchool['is_admin'] = true;
                $newSchool['is_teacher'] = false;
            } else if (($school['role_id'] == 0) && !in_array($userId, $principals)) {
                $newSchool['is_admin'] = false;
                $newSchool['is_teacher'] = true;
            } else {
                $newSchool['is_admin'] = false;
                $newSchool['is_teacher'] = false;
                $newSchool['permissions'] = $roleDao->getPermissionOfSchool($school['page_id'], $userId);
            }
            $newSchools[$newSchool['page_id']] = $newSchool;
            //$result[$newSchool['page_id']] = array_merge($schoolData, $newSchool);
        }

        //2.Cập nhật thông tin quản lý lớp của 1 user
        $classes = $classDao->getClasses4Memcache($userId);

        //3.Cập nhật thông tin quản lý trẻ của 1 user
        $children = $childDao->getChildren4Memcache($userId);

        //4.Cập nhật thông tin quản lý bản thân của 1 user
        $children_itself = $childDao->getChildren4Memcache($userId,0);

        $result[USER_MANAGE_SCHOOL] = $newSchools;
        $result[USER_MANAGE_CLASSES] = $classes['ids'];
        $result[USER_MANAGE_CHILDREN] = $children['ids'];
        $result[USER_MANAGE_CHILDREN_ITSELF] = $children_itself['ids'];
    }
    ${'umData_' . $userId} = $umData;
    return $result;
}

/**
 * Delete dữ liệu memcached của đối tượng user quản lý
 *
 * @param $userId
 */
function deleteUserManageData($userId, $key = ALL, $value = null)
{
    global ${'umData_' . $userId};
    $umData = ${'umData_' . $userId};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    $memcache = new CacheMemcache();

    //$result = null;
    if ($memcache->bEnabled) { // if Memcache enabled
        if (is_null($umData)) {
            $umData = $memcache->getData(USER_MANAGE_KEY . $userId);
        }

        if (!is_null($umData)) {
            switch ($key) {
                case USER_MANAGE_SCHOOL :
                    unset($umData[$key]);
                    break;

                case USER_MANAGE_CLASSES :
                case USER_MANAGE_CHILDREN :
                    if (!is_null($value)) {
                        if (($key_del = array_search($value, $umData[$key])) !== false) {
                            unset($umData[$key][$key_del]);
                        }
                    } else {
                        unset($umData[$key]);
                    }
                    break;

                case ALL:
                    $umData = null;
                    break;
            }
            $memcache->setData(USER_MANAGE_KEY . $userId, $umData);
        }
    }
    ${'umData_' . $userId} = $umData;
    //return $result;
}

/**
 * Update User last active
 *
 * @param $schoolId
 */
function updateUserLastActive($user_id)
{
    global $userLastActive;
    $time = date('Y-m-d H:i:s');

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    $memcache = new CacheMemcache();

    if ($memcache->bEnabled) { // if Memcache enabled
        if (is_null($userLastActive)) {
            $tmp = $memcache->getData('last_active');
            $userLastActive = is_null($tmp) ? array() : $tmp;
        }
        // Cập nhật user vào memcache
        $userLastActive[$user_id] = $time;
        $memcache->setData('last_active', $userLastActive);
    }
    return $userLastActive;
}

/**
 * get User last active
 *
 * @param $schoolId
 */
function getUserLastActive()
{
    global $userLastActive;

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    $memcache = new CacheMemcache();

    if ($memcache->bEnabled) { // if Memcache enabled
        if (is_null($userLastActive)) {
            $userLastActive = $memcache->getData('last_active');
        }
    }
    return $userLastActive;
}

/* ---------- MEMCACHE - Phụ huynh đánh giá ----------*/

/**
 * Cập nhật thông tin phụ huynh đã đánh giá
 *
 * @param $user_id
 */
function updateParentReview($user_id)
{
    global $parentReview;

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    include_once(DAO_PATH . 'dao_review.php');
    $memcache = new CacheMemcache();
    $reviewDao = new ReviewDAO();

    $curMonth = date('Y-m-01');
    //$result = null;
    if ($memcache->bEnabled) { // if Memcache enabled
        if (is_null($parentReview)) {
            $parentReview = $memcache->getData(PARENT_REVIEW);
        }
        if (is_null($parentReview)) {
            $parentReview = $reviewDao->getParentHasReviewed();
        }
        // Cập nhật user vào memcache
        $parentReview[$user_id] = $curMonth;
        $memcache->setData(PARENT_REVIEW, $parentReview);
    }
}

/**
 * Kiểm tra xem phụ huynh đã đánh giá trong tháng hay chưa
 *
 * @param $user_id
 */
function checkParentReviewInMonth($user_id)
{
    global $parentReview;

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    include_once(DAO_PATH . 'dao_review.php');
    $reviewDao = new ReviewDAO();
    $memcache = new CacheMemcache();

    $curMonth = date('Y-m-01');
    //$result = null;
    if ($memcache->bEnabled) { // if Memcache enabled
        if (is_null($parentReview)) {
            $parentReview = $memcache->getData(PARENT_REVIEW);
        }
        if (is_null($parentReview)) {
            $parentReview = $reviewDao->getParentHasReviewed();
        }
        $memcache->setData(PARENT_REVIEW, $parentReview);
    } elseif (is_null($parentReview)) {
        $parentReview = $reviewDao->getParentHasReviewed();
    }

    if (isset($parentReview[$user_id]) && $parentReview[$user_id] == $curMonth) {
        return true;
    }
    return false;
}

/* ---------- END - Phụ huynh đánh giá ----------*/

/**
 * Thống kê tương tác của phụ huynh đối với mỗi module
 *
 * @param $schoolId
 */
function getSchoolStatistics($schoolId, $viewType = ALL, $fromDate = null, $toDate = null)
{
    global ${'statistic_' . $schoolId}, $school_statistic_keys;
    $stData = ${'statistic_' . $schoolId};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    include_once(DAO_PATH . 'dao_statistic.php');
    $memcache = new CacheMemcache();
    $statisticDao = new StatisticDAO();

    if (is_null($fromDate)) {
        $strFromDate = strtotime('-30 days');
        $strToDate = strtotime(date('Y-m-d'));
    } else {
        $strFromDate = strtotime(date('Y-m-d', strtotime(toDBDate($fromDate))));
        $strToDate = strtotime(date('Y-m-d', strtotime(toDBDate($toDate))));
    }

    $result = array(
        'page' => array(
            'count_views' => 0
        ),
        'post' => array(
            'count_created' => 0,
            'count_views' => 0
        ),
        'event' => array(
            'count_created' => 0,
            'count_views' => 0
        ),
        'schedule' => array(
            'count_created' => 0,
            'count_views' => 0
        ),
        'menu' => array(
            'count_created' => 0,
            'count_views' => 0
        ),
        'report' => array(
            'count_created' => 0,
            'count_views' => 0
        ),
        'tuition' => array(
            'count_created' => 0,
            'count_views' => 0
        )
    );
    if ($memcache->bEnabled) { // if Memcache enabled
        if (is_null($stData)) {
            $stData = $memcache->getData(SCHOOL_STATISTIC . $schoolId);
        }

        $keys = is_null($stData) ? array() : array_keys($stData);
        // Nếu getAll mà memcache không đủ dữ liệu thì lấy ra từ database
        if (!array_equal($keys, $school_statistic_keys)) {
            $stData = updateSchoolStatistic($schoolId);
        }

        switch ($viewType) {
            case 'page' :
                if (is_array($stData['page'])) {
                    $cntViewPage = 0;
                    foreach ($stData['page'] as $date => $viewPage) {
                        if (strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
                            $cntViewPage = $cntViewPage + $viewPage;
                        }
                    }
                    $result['page']['count_views'] = $cntViewPage;
                }
                break;

            case 'post' :
                if (is_array($stData['post'])) {
                    $cntViewPost = 0;
                    foreach ($stData['post'] as $date => $arrPost) {
                        if (strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
                            foreach ($arrPost as $postId => $viewPost) {
                                $cntViewPost = $cntViewPost + $viewPost;
                            }
                        }
                    }
                    $result['post']['count_views'] = $cntViewPost;
                }
                break;

            case 'event' :
                if (is_array($stData['event'])) {
                    $cntViewEvent = 0;
                    foreach ($stData['event'] as $date => $arrEvent) {
                        if (strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
                            foreach ($arrEvent as $eventId => $viewEvent) {
                                $cntViewEvent = $cntViewEvent + $viewEvent;
                            }
                        }
                    }
                    $result['event']['count_views'] = $cntViewEvent;
                }
                break;

            case 'schedule' :
                if (is_array($stData['schedule'])) {
                    $cntViewSchedule = 0;
                    foreach ($stData['schedule'] as $date => $arrSchedule) {
                        if (strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
                            foreach ($arrSchedule as $scheduleId => $viewSchedule) {
                                $cntViewSchedule = $cntViewSchedule + $viewSchedule;
                            }
                        }
                    }
                    $result['schedule']['count_views'] = $cntViewSchedule;
                }
                break;

            case 'menu' :
                if (is_array($stData['menu'])) {
                    $cntViewMenu = 0;
                    foreach ($stData['menu'] as $date => $arrMenu) {
                        if (strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
                            foreach ($arrMenu as $menuId => $viewMenu) {
                                $cntViewMenu = $cntViewMenu + $viewMenu;
                            }
                        }
                    }
                    $result['menu']['count_views'] = $cntViewMenu;
                }
                break;

            case 'report' :
                if (is_array($stData['report'])) {
                    $cntViewReport = 0;
                    foreach ($stData['report'] as $date => $arrReport) {
                        if (strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
                            foreach ($arrReport as $reportId => $viewReport) {
                                $cntViewReport = $cntViewReport + $viewReport;
                            }
                        }
                    }
                    $result['report']['count_views'] = $cntViewReport;
                }
                break;

            case 'tuition' :
                if (is_array($stData['tuition'])) {
                    $cntViewTuition = 0;
                    foreach ($stData['tuition'] as $date => $arrTuition) {
                        if (strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
                            foreach ($arrTuition as $tuitionChildId => $viewTuition) {
                                $cntViewTuition = $cntViewTuition + $viewTuition;
                            }
                        }
                    }
                    $result['tuition']['count_views'] = $cntViewTuition;
                }
                break;

            case 'post_created' :
                if (is_array($stData['post_created'])) {
                    $cntPost = 0;
                    foreach ($stData['post_created'] as $date => $arrPost) {
                        if (strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
                            $cntPost = $cntPost + count($arrPost);
                        }
                    }
                    $result['post']['count_created'] = $cntPost;
                }
                break;

            case 'event_created' :
                if (is_array($stData['event_created'])) {
                    $cntEvent = 0;
                    foreach ($stData['event_created'] as $date => $arrEvent) {
                        if (strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
                            $cntEvent = $cntEvent + count($arrEvent);
                        }
                    }
                    $result['event']['count_created'] = $cntEvent;
                }
                break;

            case 'schedule_created' :
                if (is_array($stData['schedule_created'])) {
                    $cntSchedule = 0;
                    foreach ($stData['schedule_created'] as $date => $arrSchedule) {
                        if (strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
                            $cntSchedule = $cntSchedule + count($arrSchedule);
                        }
                    }
                    $result['schedule']['count_created'] = $cntSchedule;
                }
                break;

            case 'menu_created' :
                if (is_array($stData['menu_created'])) {
                    $cntMenu = 0;
                    foreach ($stData['menu_created'] as $date => $arrMenu) {
                        if (strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
                            $cntMenu = $cntMenu + count($arrMenu);
                        }
                    }
                    $result['menu']['count_created'] = $cntMenu;
                }
                break;

            case 'report_created' :
                if (is_array($stData['report_created'])) {
                    $cntReport = 0;
                    foreach ($stData['report_created'] as $date => $arrReport) {
                        if (strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
                            $cntReport = $cntReport + count($arrReport);
                        }
                    }
                    $result['report']['count_created'] = $cntReport;
                }
                break;

            case 'tuition_created' :
                if (is_array($stData['tuition_created'])) {
                    $cntTuition = 0;
                    foreach ($stData['tuition_created'] as $date => $arrTuition) {
                        if (strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
                            $cntTuition = $cntTuition + count($arrTuition);
                        }
                    }
                    $result['tuition']['count_created'] = $cntTuition;
                }
                break;

            case ALL :
                if (is_array($stData['page'])) {
                    $cntViewPage = 0;
                    foreach ($stData['page'] as $date => $viewPage) {
                        if (strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
                            $cntViewPage = $cntViewPage + $viewPage;
                        }
                    }
                    $result['page']['count_views'] = $cntViewPage;
                }

                if (is_array($stData['post'])) {
                    $cntViewPost = 0;
                    foreach ($stData['post'] as $date => $arrPost) {
                        if (strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
                            foreach ($arrPost as $postId => $viewPost) {
                                $cntViewPost = $cntViewPost + $viewPost;
                            }
                        }
                    }
                    $result['post']['count_views'] = $cntViewPost;
                }

                if (is_array($stData['event'])) {
                    $cntViewEvent = 0;
                    foreach ($stData['event'] as $date => $arrEvent) {
                        if (strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
                            foreach ($arrEvent as $eventId => $viewEvent) {
                                $cntViewEvent = $cntViewEvent + $viewEvent;
                            }
                        }
                    }
                    $result['event']['count_views'] = $cntViewEvent;
                }

                if (is_array($stData['schedule'])) {
                    $cntViewSchedule = 0;
                    foreach ($stData['schedule'] as $date => $arrSchedule) {
                        if (strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
                            foreach ($arrSchedule as $scheduleId => $viewSchedule) {
                                $cntViewSchedule = $cntViewSchedule + $viewSchedule;
                            }
                        }
                    }
                    $result['schedule']['count_views'] = $cntViewSchedule;
                }

                if (is_array($stData['menu'])) {
                    $cntViewMenu = 0;
                    foreach ($stData['menu'] as $date => $arrMenu) {
                        if (strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
                            foreach ($arrMenu as $menuId => $viewMenu) {
                                $cntViewMenu = $cntViewMenu + $viewMenu;
                            }
                        }
                    }
                    $result['menu']['count_views'] = $cntViewMenu;
                }

                if (is_array($stData['report'])) {
                    $cntViewReport = 0;
                    foreach ($stData['report'] as $date => $arrReport) {
                        if (strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
                            foreach ($arrReport as $reportId => $viewReport) {
                                $cntViewReport = $cntViewReport + $viewReport;
                            }
                        }
                    }
                    $result['report']['count_views'] = $cntViewReport;
                }

                if (is_array($stData['tuition'])) {
                    $cntViewTuition = 0;
                    foreach ($stData['tuition'] as $date => $arrTuition) {
                        if (strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
                            foreach ($arrTuition as $tuitionChildId => $viewTuition) {
                                $cntViewTuition = $cntViewTuition + $viewTuition;
                            }
                        }
                    }
                    $result['tuition']['count_views'] = $cntViewTuition;
                }

                if (is_array($stData['post_created'])) {
                    $cntPost = 0;
                    foreach ($stData['post_created'] as $date => $arrPost) {
                        if (strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
                            $cntPost = $cntPost + count($arrPost);
                        }
                    }
                    $result['post']['count_created'] = $cntPost;
                }

                if (is_array($stData['event_created'])) {
                    $cntEvent = 0;
                    foreach ($stData['event_created'] as $date => $arrEvent) {
                        if (strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
                            $cntEvent = $cntEvent + count($arrEvent);
                        }
                    }
                    $result['event']['count_created'] = $cntEvent;
                }

                if (is_array($stData['schedule_created'])) {
                    $cntSchedule = 0;
                    foreach ($stData['schedule_created'] as $date => $arrSchedule) {
                        if (strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
                            $cntSchedule = $cntSchedule + count($arrSchedule);
                        }
                    }
                    $result['schedule']['count_created'] = $cntSchedule;
                }

                if (is_array($stData['menu_created'])) {
                    $cntMenu = 0;
                    foreach ($stData['menu_created'] as $date => $arrMenu) {
                        if (strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
                            $cntMenu = $cntMenu + count($arrMenu);
                        }
                    }
                    $result['menu']['count_created'] = $cntMenu;
                }

                if (is_array($stData['report_created'])) {
                    $cntReport = 0;
                    foreach ($stData['report_created'] as $date => $arrReport) {
                        if (strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
                            $cntReport = $cntReport + count($arrReport);
                        }
                    }
                    $result['report']['count_created'] = $cntReport;
                }

                if (is_array($stData['tuition_created'])) {
                    $cntTuition = 0;
                    foreach ($stData['tuition_created'] as $date => $arrTuition) {
                        if (strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
                            $cntTuition = $cntTuition + count($arrTuition);
                        }
                    }
                    $result['tuition']['count_created'] = $cntTuition;
                }
                break;

        }
    }
    if (is_null($stData)) {
        $result = $statisticDao->getSchoolStatisticInLast30Days($schoolId);
    }
    return $result;
}

/**
 * Thống kê tương tác của phụ huynh và trường đối với mỗi module (làm lại) - TaiLA
 *
 * @param $schoolId
 */
function getStatisticsInteractive($schoolId, $module = ALL, $fromDate = null, $toDate = null)
{
    global ${STATISTIC_INTERACTIVE . $schoolId}, $statistic_keys;
    $stData = ${STATISTIC_INTERACTIVE . $schoolId};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    include_once(DAO_PATH . 'dao_statistic.php');
    $memcache = new CacheMemcache();
    $statisticDao = new StatisticDAO();

    if (is_null($fromDate)) {
        $strFromDate = strtotime('-30 days');
        $strToDate = strtotime(date('Y-m-d'));
    } else {
        $strFromDate = strtotime(date('Y-m-d', strtotime(toDBDate($fromDate))));
        $strToDate = strtotime(date('Y-m-d', strtotime(toDBDate($toDate))));
    }
    $result = array();
//    $result = array(
//        'page' => array(
//            'school_view' => 0,
//            'parent_view' => 0
//        ),
//        'post' => array(
//            'school_view' => 0,
//            'parent_view' => 0,
//            'school_created' => 0
//        ),
//        'school_dashboard' => array(
//            'school_view' => 0
//        ),
//        'attendance' => array(
//            'school_view' => 0,
//            'parent_view' => 0,
//            'school_created' => 0
//        ),
//        'service' => array(
//            'school_view' => 0,
//            'parent_view' => 0,
//            'school_created' => 0
//        ),
//        'pickup' => array(
//            'school_view' => 0,
//            'parent_view' => 0,
//            'school_created' => 0
//        ),
//        'tuition' => array(
//            'school_view' => 0,
//            'parent_view' => 0,
//            'school_created' => 0
//        ),
//        'event' => array(
//            'school_view' => 0,
//            'parent_view' => 0,
//            'school_created' => 0
//        ),
//        'schedule' => array(
//            'school_view' => 0,
//            'parent_view' => 0,
//            'school_created' => 0
//        ),
//        'menu' => array(
//            'school_view' => 0,
//            'parent_view' => 0,
//            'school_created' => 0
//        ),
//        'report' => array(
//            'school_view' => 0,
//            'parent_view' => 0,
//            'school_created' => 0
//        ),
//        'medicine' => array(
//            'school_view' => 0,
//            'parent_view' => 0,
//            'school_created' => 0,
//            'parent_created' => 0
//        ),
//        'feedback' => array(
//            'school_view' => 0,
//            'parent_view' => 0,
//            'parent_created' => 0
//        ),
//        'diary' => array(
//            'school_view' => 0,
//            'parent_view' => 0,
//            'school_created' => 0,
//            'parent_created' => 0
//        ),
//        'health' => array(
//            'school_view' => 0,
//            'parent_view' => 0,
//            'school_created' => 0,
//            'parent_created' => 0
//        ),
//        'birthday' => array(
//            'school_view' => 0
//        ),
//        'picker' => array(
//            'school_view' => 0,
//            'parent_view' => 0
//        )
//    );
    if ($memcache->bEnabled) { // if Memcache enabled
        if (is_null($stData)) {
            $stData = $memcache->getData(STATISTIC_INTERACTIVE . $schoolId);
        }

        $keys = is_null($stData) ? array() : array_keys($stData);
        // Nếu getAll mà memcache không đủ dữ liệu thì lấy ra từ database
        if (!array_equal($keys, $statistic_keys)) {
            $stData = updateStatisticInteractive($schoolId);
        }
        if ($module == ALL) {
            foreach ($statistic_keys as $key) {
                if (is_array($stData[$key])) {
                    if ($key == 'post') {
                        $cntSchoolCreated = 0;
                        $cntParentCreated = 0;
                        $cntSchoolView = 0;
                        foreach ($stData[$key] as $date => $arrPage) {
                            if (strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
                                foreach ($arrPage as $row) {
                                    if (isset($row['is_created']) && $row['is_created'] == 1) {
                                        $cntSchoolCreated = $cntSchoolCreated + 1;
                                    } else if (isset($row['is_created']) && $row['is_created'] == 2) {
                                        $cntParentCreated = $cntParentCreated + 1;
                                    }
                                    if ($row['school_view'] > 0) {
                                        $cntSchoolView = $cntSchoolView + $row['school_view'];
                                    }
                                }
                            }
                        }
                        $result[$key]['school_createds'] = $cntSchoolCreated;
                        $result[$key]['parent_createds'] = $cntParentCreated;
                        $result[$key]['school_views'] = $cntSchoolView;
                    } else {
                        $cntSchoolCreated = 0;
                        $cntParentCreated = 0;
                        foreach ($stData[$key] as $date => $arrPage) {
                            if (strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
                                // Lượt view bằng lượt view của thằng object_id = 0.
                                $result[$key]['school_views'] = (isset($arrPage[0]['school_view'])) ? $arrPage[0]['school_view'] : 0;
                                $result[$key]['parent_views'] = (isset($arrPage[0]['parent_view'])) ? $arrPage[0]['parent_view'] : 0;
                                foreach ($arrPage as $row) {
                                    if (isset($row['is_created']) && $row['is_created'] == 1) {
                                        $cntSchoolCreated = $cntSchoolCreated + 1;
                                    } else if (isset($row['is_created']) && $row['is_created'] == 2) {
                                        $cntParentCreated = $cntParentCreated + 1;
                                    }
                                }
                            }
                        }
                        $result[$key]['school_createds'] = $cntSchoolCreated;
                        $result[$key]['parent_createds'] = $cntParentCreated;
                    }
                }
            }
        } else {
            if (is_array($stData[$module])) {
                if ($module == 'post') {
                    $cntSchoolCreated = 0;
                    $cntParentCreated = 0;
                    $cntSchoolView = 0;
                    foreach ($stData[$module] as $date => $arrPage) {
                        if (strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
                            foreach ($arrPage as $row) {
                                if (isset($row['is_created']) && $row['is_created'] == 1) {
                                    $cntSchoolCreated = $cntSchoolCreated + 1;
                                } else if (isset($row['is_created']) && $row['is_created'] == 2) {
                                    $cntParentCreated = $cntParentCreated + 1;
                                }
                                if ($row['school_view'] > 0) {
                                    $cntSchoolView = $cntSchoolView + $row['school_view'];
                                }
                            }
                        }
                    }
                    $result[$module]['school_createds'] = $cntSchoolCreated;
                    $result[$module]['parent_createds'] = $cntParentCreated;
                    $result[$module]['school_views'] = $cntSchoolView;
                } else {
                    $cntSchoolCreated = 0;
                    $cntParentCreated = 0;
                    foreach ($stData[$module] as $date => $arrPage) {
                        if (strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
                            // Lượt view bằng lượt view của thằng object_id = 0.
                            $result[$module]['school_views'] = isset($arrPage[0]['school_view']) ? $arrPage[0]['school_view'] : 0;
                            $result[$module]['parent_views'] = isset($arrPage[0]['parent_view']) ? $arrPage[0]['parent_view'] : 0;
                            foreach ($arrPage as $row) {
                                if (isset($row['is_created']) && $row['is_created'] == 1) {
                                    $cntSchoolCreated = $cntSchoolCreated + 1;
                                } else if (isset($row['is_created']) && $row['is_created'] == 1) {
                                    $cntParentCreated = $cntParentCreated + 1;
                                }
                            }
                        }
                    }
                    $result[$module]['school_createds'] = $cntSchoolCreated;
                    $result[$module]['parent_createds'] = $cntParentCreated;
                }
            }
        }
//        switch ($module) {
//            case 'page' :
//                if (is_array($stData['page'])) {
//                    $cntSchoolViewPage = 0;
//                    $cntParentViewPage = 0;
//                    foreach ($stData['page'] as $date => $row) {
//                        if(strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
//                            $cntSchoolViewPage = $cntSchoolViewPage + $row['school_view'];
//                            $cntParentViewPage = $cntParentViewPage + $row['parent_view'];
//                        }
//                    }
//                    $result['page']['school_views'] = $cntSchoolViewPage;
//                    $result['page']['parent_views'] = $cntParentViewPage;
//                }
//                break;
//
//            case 'post' :
//                if (is_array($stData['post'])) {
//                    $cntParentViewPost = 0;
//                    foreach ($stData['post'] as $date => $arrPost) {
//                        foreach ($arrPost as)
//                        if(strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
//                            foreach ($arrPost as $postId => $viewPost) {
//                                $cntViewPost = $cntViewPost + $viewPost;
//                            }
//                        }
//                    }
//                    $result['post']['count_views'] = $cntViewPost;
//                }
//                break;
//
//            case 'event' :
//                if (is_array($stData['event'])) {
//                    $cntViewEvent = 0;
//                    foreach ($stData['event'] as $date => $arrEvent) {
//                        if(strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
//                            foreach ($arrEvent as $eventId => $viewEvent) {
//                                $cntViewEvent = $cntViewEvent + $viewEvent;
//                            }
//                        }
//                    }
//                    $result['event']['count_views'] = $cntViewEvent;
//                }
//                break;
//
//            case 'schedule' :
//                if (is_array($stData['schedule'])) {
//                    $cntViewSchedule = 0;
//                    foreach ($stData['schedule'] as $date => $arrSchedule ) {
//                        if(strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
//                            foreach ($arrSchedule  as $scheduleId => $viewSchedule ) {
//                                $cntViewSchedule = $cntViewSchedule + $viewSchedule ;
//                            }
//                        }
//                    }
//                    $result['schedule']['count_views'] = $cntViewSchedule;
//                }
//                break;
//
//            case 'menu' :
//                if (is_array($stData['menu'])) {
//                    $cntViewMenu = 0;
//                    foreach ($stData['menu'] as $date => $arrMenu) {
//                        if(strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
//                            foreach ($arrMenu  as $menuId => $viewMenu ) {
//                                $cntViewMenu = $cntViewMenu + $viewMenu ;
//                            }
//                        }
//                    }
//                    $result['menu']['count_views'] = $cntViewMenu;
//                }
//                break;
//
//            case 'report' :
//                if (is_array($stData['report'])) {
//                    $cntViewReport = 0;
//                    foreach ($stData['report'] as $date => $arrReport) {
//                        if(strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
//                            foreach ($arrReport  as $reportId => $viewReport) {
//                                $cntViewReport = $cntViewReport + $viewReport ;
//                            }
//                        }
//                    }
//                    $result['report']['count_views'] = $cntViewReport;
//                }
//                break;
//
//            case 'tuition' :
//                if (is_array($stData['tuition'])) {
//                    $cntViewTuition = 0;
//                    foreach ($stData['tuition'] as $date => $arrTuition) {
//                        if(strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
//                            foreach ($arrTuition  as $tuitionChildId => $viewTuition) {
//                                $cntViewTuition = $cntViewTuition + $viewTuition ;
//                            }
//                        }
//                    }
//                    $result['tuition']['count_views'] = $cntViewTuition;
//                }
//                break;
//
//            case 'post_created' :
//                if (is_array($stData['post_created'])) {
//                    $cntPost = 0;
//                    foreach ($stData['post_created'] as $date => $arrPost) {
//                        if(strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
//                            $cntPost = $cntPost + count($arrPost);
//                        }
//                    }
//                    $result['post']['count_created'] = $cntPost;
//                }
//                break;
//
//            case 'event_created' :
//                if (is_array($stData['event_created'])) {
//                    $cntEvent = 0;
//                    foreach ($stData['event_created'] as $date => $arrEvent) {
//                        if(strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
//                            $cntEvent = $cntEvent + count($arrEvent);
//                        }
//                    }
//                    $result['event']['count_created'] = $cntEvent;
//                }
//                break;
//
//            case 'schedule_created' :
//                if (is_array($stData['schedule_created'])) {
//                    $cntSchedule = 0;
//                    foreach ($stData['schedule_created'] as $date => $arrSchedule ) {
//                        if(strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
//                            $cntSchedule = $cntSchedule + count($arrSchedule);
//                        }
//                    }
//                    $result['schedule']['count_created'] = $cntSchedule;
//                }
//                break;
//
//            case 'menu_created' :
//                if (is_array($stData['menu_created'])) {
//                    $cntMenu = 0;
//                    foreach ($stData['menu_created'] as $date => $arrMenu) {
//                        if(strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
//                            $cntMenu = $cntMenu + count($arrMenu);
//                        }
//                    }
//                    $result['menu']['count_created'] = $cntMenu;
//                }
//                break;
//
//            case 'report_created' :
//                if (is_array($stData['report_created'])) {
//                    $cntReport = 0;
//                    foreach ($stData['report_created'] as $date => $arrReport) {
//                        if(strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
//                            $cntReport = $cntReport + count($arrReport);
//                        }
//                    }
//                    $result['report']['count_created'] = $cntReport;
//                }
//                break;
//
//            case 'tuition_created' :
//                if (is_array($stData['tuition_created'])) {
//                    $cntTuition = 0;
//                    foreach ($stData['tuition_created'] as $date => $arrTuition) {
//                        if(strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
//                            $cntTuition = $cntTuition + count($arrTuition);
//                        }
//                    }
//                    $result['tuition']['count_created'] = $cntTuition;
//                }
//                break;
//
//            case ALL :
//                if (is_array($stData['page'])) {
//                    $cntViewPage = 0;
//                    foreach ($stData['page'] as $date => $viewPage) {
//                        if(strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
//                            $cntViewPage = $cntViewPage + $viewPage;
//                        }
//                    }
//                    $result['page']['count_views'] = $cntViewPage;
//                }
//
//                if (is_array($stData['post'])) {
//                    $cntViewPost = 0;
//                    foreach ($stData['post'] as $date => $arrPost) {
//                        if(strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
//                            foreach ($arrPost as $postId => $viewPost) {
//                                $cntViewPost = $cntViewPost + $viewPost;
//                            }
//                        }
//                    }
//                    $result['post']['count_views'] = $cntViewPost;
//                }
//
//                if (is_array($stData['event'])) {
//                    $cntViewEvent = 0;
//                    foreach ($stData['event'] as $date => $arrEvent) {
//                        if(strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
//                            foreach ($arrEvent as $eventId => $viewEvent) {
//                                $cntViewEvent = $cntViewEvent + $viewEvent;
//                            }
//                        }
//                    }
//                    $result['event']['count_views'] = $cntViewEvent;
//                }
//
//                if (is_array($stData['schedule'])) {
//                    $cntViewSchedule = 0;
//                    foreach ($stData['schedule'] as $date => $arrSchedule ) {
//                        if(strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
//                            foreach ($arrSchedule  as $scheduleId => $viewSchedule ) {
//                                $cntViewSchedule = $cntViewSchedule + $viewSchedule ;
//                            }
//                        }
//                    }
//                    $result['schedule']['count_views'] = $cntViewSchedule;
//                }
//
//                if (is_array($stData['menu'])) {
//                    $cntViewMenu = 0;
//                    foreach ($stData['menu'] as $date => $arrMenu) {
//                        if(strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
//                            foreach ($arrMenu  as $menuId => $viewMenu ) {
//                                $cntViewMenu = $cntViewMenu + $viewMenu ;
//                            }
//                        }
//                    }
//                    $result['menu']['count_views'] = $cntViewMenu;
//                }
//
//                if (is_array($stData['report'])) {
//                    $cntViewReport = 0;
//                    foreach ($stData['report'] as $date => $arrReport) {
//                        if(strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
//                            foreach ($arrReport  as $reportId => $viewReport) {
//                                $cntViewReport = $cntViewReport + $viewReport ;
//                            }
//                        }
//                    }
//                    $result['report']['count_views'] = $cntViewReport;
//                }
//
//                if (is_array($stData['tuition'])) {
//                    $cntViewTuition = 0;
//                    foreach ($stData['tuition'] as $date => $arrTuition) {
//                        if(strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
//                            foreach ($arrTuition  as $tuitionChildId => $viewTuition) {
//                                $cntViewTuition = $cntViewTuition + $viewTuition ;
//                            }
//                        }
//                    }
//                    $result['tuition']['count_views'] = $cntViewTuition;
//                }
//
//                if (is_array($stData['post_created'])) {
//                    $cntPost = 0;
//                    foreach ($stData['post_created'] as $date => $arrPost) {
//                        if(strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
//                            $cntPost = $cntPost + count($arrPost);
//                        }
//                    }
//                    $result['post']['count_created'] = $cntPost;
//                }
//
//                if (is_array($stData['event_created'])) {
//                    $cntEvent = 0;
//                    foreach ($stData['event_created'] as $date => $arrEvent) {
//                        if(strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
//                            $cntEvent = $cntEvent + count($arrEvent);
//                        }
//                    }
//                    $result['event']['count_created'] = $cntEvent;
//                }
//
//                if (is_array($stData['schedule_created'])) {
//                    $cntSchedule = 0;
//                    foreach ($stData['schedule_created'] as $date => $arrSchedule ) {
//                        if(strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
//                            $cntSchedule = $cntSchedule + count($arrSchedule);
//                        }
//                    }
//                    $result['schedule']['count_created'] = $cntSchedule;
//                }
//
//                if (is_array($stData['menu_created'])) {
//                    $cntMenu = 0;
//                    foreach ($stData['menu_created'] as $date => $arrMenu) {
//                        if(strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
//                            $cntMenu = $cntMenu + count($arrMenu);
//                        }
//                    }
//                    $result['menu']['count_created'] = $cntMenu;
//                }
//
//                if (is_array($stData['report_created'])) {
//                    $cntReport = 0;
//                    foreach ($stData['report_created'] as $date => $arrReport) {
//                        if(strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
//                            $cntReport = $cntReport + count($arrReport);
//                        }
//                    }
//                    $result['report']['count_created'] = $cntReport;
//                }
//
//                if (is_array($stData['tuition_created'])) {
//                    $cntTuition = 0;
//                    foreach ($stData['tuition_created'] as $date => $arrTuition) {
//                        if(strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
//                            $cntTuition = $cntTuition + count($arrTuition);
//                        }
//                    }
//                    $result['tuition']['count_created'] = $cntTuition;
//                }
//                break;
//
//        }
    }
    if (is_null($stData)) {
        $result = $statisticDao->getInteractiveByDate($schoolId, $module, $strFromDate, $strToDate);
    }
    return $result;
}

/**
 * Cập nhật số lượng tương tác
 *
 * @param $schoolId
 */
function updateSchoolStatistic($schoolId)
{
    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    include_once(DAO_PATH . 'dao_statistic.php');
    $memcache = new CacheMemcache();
    $statisticDao = new StatisticDAO();

    if ($memcache->bEnabled) { // if Memcache enabled
        $result = $statisticDao->getSchoolStatistic($schoolId);
        $memcache->setData(SCHOOL_STATISTIC . $schoolId, $result);
        return $result;
    }
    return null;
}

/**
 * Cập nhật số lượng tương tác của phụ huynh và trường (từ db cập nhật vào memcache) - TaiLA - làm lại
 *
 * @param $schoolId
 */
function updateStatisticInteractive($schoolId)
{
    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    include_once(DAO_PATH . 'dao_statistic.php');
    $memcache = new CacheMemcache();
    $statisticDao = new StatisticDAO();

    if ($memcache->bEnabled) { // if Memcache enabled
        $result = $statisticDao->getStatisticInteractive($schoolId);
        $memcache->setData(STATISTIC_INTERACTIVE . $schoolId, $result);
        return $result;
    }
    return null;
}

/**
 * Tăng số lượng tương tác
 *
 * @param $schoolId
 */
function increaseCountViews($schoolId, $viewType, $objectIds = array())
{
    global ${SCHOOL_STATISTIC . $schoolId}, $school_statistic_keys;
    $stData = ${SCHOOL_STATISTIC . $schoolId};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    $memcache = new CacheMemcache();

    $curDate = date('Y-m-d');
    $result = null;
    if ($memcache->bEnabled) { // if Memcache enabled

        if (is_null($stData)) {
            $stData = $memcache->getData(SCHOOL_STATISTIC . $schoolId);
        }
        $keys = is_null($stData) ? array() : array_keys($stData);

        // Nếu getAll mà memcache không đủ dữ liệu thì lấy ra từ database
        if (!array_equal($keys, $school_statistic_keys)) {
            $stData = updateSchoolStatistic($schoolId);
        }

        switch ($viewType) {
            case 'page' :
                $stData['page'][$curDate] = is_numeric($stData['page'][$curDate]) ? $stData['page'][$curDate] + 1 : 1;
                break;

            case 'post' :
                foreach ($objectIds as $objectId) {
                    $stData['post'][$curDate][$objectId] = is_numeric($stData['post'][$curDate][$objectId]) ? $stData['post'][$curDate][$objectId] + 1 : 1;
                }
                break;

            case 'event' :
                foreach ($objectIds as $objectId) {
                    $stData['event'][$curDate][$objectId] = is_numeric($stData['event'][$curDate][$objectId]) ? $stData['event'][$curDate][$objectId] + 1 : 1;
                }
                break;

            case 'schedule' :
                foreach ($objectIds as $objectId) {
                    $stData['schedule'][$curDate][$objectId] = is_numeric($stData['schedule'][$curDate][$objectId]) ? $stData['schedule'][$curDate][$objectId] + 1 : 1;
                }
                break;

            case 'menu' :
                foreach ($objectIds as $objectId) {
                    $stData['menu'][$curDate][$objectId] = is_numeric($stData['menu'][$curDate][$objectId]) ? $stData['menu'][$curDate][$objectId] + 1 : 1;
                }
                break;

            case 'report' :
                foreach ($objectIds as $objectId) {
                    $stData['report'][$curDate][$objectId] = is_numeric($stData['report'][$curDate][$objectId]) ? $stData['report'][$curDate][$objectId] + 1 : 1;
                }
                break;

            case 'tuition' :
                foreach ($objectIds as $objectId) {
                    $stData['tuition'][$curDate][$objectId] = is_numeric($stData['tuition'][$curDate][$objectId]) ? $stData['tuition'][$curDate][$objectId] + 1 : 1;
                }
                break;

        }
        $memcache->setData(SCHOOL_STATISTIC . $schoolId, $stData);
    }
    ${SCHOOL_STATISTIC . $schoolId} = $stData;
}


/**
 * Set thông tin là bài viết được tạo mới
 *
 * @param $schoolId
 */
function setIsAddNew($schoolId, $viewType, $objectIds = array())
{
    global ${SCHOOL_STATISTIC . $schoolId}, $school_statistic_keys;
    $stData = ${SCHOOL_STATISTIC . $schoolId};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    $memcache = new CacheMemcache();

    $curDate = date('Y-m-d');
    $result = null;
    if ($memcache->bEnabled) { // if Memcache enabled

        if (is_null($stData)) {
            $stData = $memcache->getData(SCHOOL_STATISTIC . $schoolId);
        }
        $keys = is_null($stData) ? array() : array_keys($stData);

        // Nếu getAll mà memcache không đủ dữ liệu thì lấy ra từ database
        if (!array_equal($keys, $school_statistic_keys)) {
            $stData = updateSchoolStatistic($schoolId);
        }

        switch ($viewType) {
            case 'post_created' :
                foreach ($objectIds as $objectId) {
                    $stData['post_created'][$curDate][] = $objectId;
                }
                break;

            case 'event_created' :
                foreach ($objectIds as $objectId) {
                    $stData['event_created'][$curDate][] = $objectId;
                }
                break;

            case 'schedule_created' :
                foreach ($objectIds as $objectId) {
                    $stData['schedule_created'][$curDate][] = $objectId;
                }
                break;

            case 'menu_created' :
                foreach ($objectIds as $objectId) {
                    $stData['menu_created'][$curDate][] = $objectId;
                }
                break;

            case 'report_created' :
                foreach ($objectIds as $objectId) {
                    $stData['report_created'][$curDate][] = $objectId;
                }
                break;

            case 'tuition_created' :
                foreach ($objectIds as $objectId) {
                    $stData['tuition_created'][$curDate][] = $objectId;
                }
                break;
        }
        $memcache->setData(SCHOOL_STATISTIC . $schoolId, $stData);
    }
    ${SCHOOL_STATISTIC . $schoolId} = $stData;
}

function increase_members_groups($group_name, $group_members)
{
    switch ($group_name) {
        case GROUP_NAME_ANDAM:
            $increase_members = $group_members + 5761;
            break;
        case 'dinhduongthaiky':
            $increase_members = $group_members + 6415;
            break;
        case 'daycondungcach':
            $increase_members = $group_members + 5185;
            break;
        case 'thaigiao':
            $increase_members = $group_members + 6823;
            break;
        case 'hiemmuon':
            $increase_members = $group_members + 4221;
            break;
        case 'tamsuhonnhangiadinh':
            $increase_members = $group_members + 6645;
            break;
        case 'thucphamorganic':
            $increase_members = $group_members + 4752;
            break;
        default:
            $increase_members = $group_members;
            break;
    }
    return $increase_members;
}

/* ---------- BEGIN - Thống kê lượt tương tác của trường, người dùng ---------- */

function getSchoolStatisticsForNoga($schoolId)
{
    global ${NOGA_STATISTIC_SCHOOL . $schoolId}, $noga_statistic_school_keys;
    $stData = ${NOGA_STATISTIC_SCHOOL . $schoolId};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    $memcache = new CacheMemcache();

    $result = array(
        'dashboard' => [],
        'attendance' => [],
        'service' => [],
        'pickup' => [],
        'tuition' => [],
        'event' => [],
        'schedule' => [],
        'menu' => [],
        'report' => [],
        'medicine' => [],
        'feedback' => [],
        'birthday' => []
    );

    if ($memcache->bEnabled) { // if Memcache enabled
        if (is_null($stData)) {
            $stData = $memcache->getData(NOGA_STATISTIC_SCHOOL . $schoolId);
        }

        $keys = is_null($stData) ? array() : array_keys($stData);
        // Nếu getAll mà memcache không đủ dữ liệu thì lấy ra từ database
        if (!array_equal($keys, $noga_statistic_school_keys)) {
            $stData = updateSchoolStatisticsForNoga($schoolId);
        }
        $result = $stData;
    }

    return $result;
}


/**
 * Cập nhật số lượng tương tác
 *
 * @param $schoolId
 */
function updateSchoolStatisticsForNoga($schoolId)
{
    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    include_once(DAO_PATH . 'dao_statistic.php');
    $memcache = new CacheMemcache();
    $statisticDao = new StatisticDAO();

    if ($memcache->bEnabled) { // if Memcache enabled
        $result = $statisticDao->getSchoolStatisticsDetailForNoga($schoolId);
        $memcache->setData(NOGA_STATISTIC_SCHOOL . $schoolId, $result);
        return $result;
    }
    return null;
}


/**
 * Tăng số lượng tương tác
 *
 * @param $schoolId
 */
function increaseSchoolInteractive($schoolId, $viewType)
{
    global ${NOGA_STATISTIC_SCHOOL . $schoolId}, $noga_statistic_school_keys;
    $stData = ${NOGA_STATISTIC_SCHOOL . $schoolId};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    $memcache = new CacheMemcache();

    $curDate = date('Y-m-d');
    $result = null;
    if ($memcache->bEnabled) { // if Memcache enabled

        if (is_null($stData)) {
            $stData = $memcache->getData(NOGA_STATISTIC_SCHOOL . $schoolId);
        }
        $keys = is_null($stData) ? array() : array_keys($stData);

        // Nếu getAll mà memcache không đủ dữ liệu thì lấy ra từ database
        if (!array_equal($keys, $noga_statistic_school_keys)) {
            $stData = updateSchoolStatisticsForNoga($schoolId);
        }

        switch ($viewType) {
            case 'dashboard' :
                $stData['dashboard'][$curDate] = is_numeric($stData['dashboard'][$curDate]) ? $stData['dashboard'][$curDate] + 1 : 1;
                break;
            case 'attendance' :
                $stData['attendance'][$curDate] = is_numeric($stData['attendance'][$curDate]) ? $stData['attendance'][$curDate] + 1 : 1;
                break;

            case 'service' :
                $stData['service'][$curDate] = is_numeric($stData['service'][$curDate]) ? $stData['service'][$curDate] + 1 : 1;
                break;
            case 'pickup' :
                $stData['pickup'][$curDate] = is_numeric($stData['pickup'][$curDate]) ? $stData['pickup'][$curDate] + 1 : 1;
                break;

            case 'tuition' :
                $stData['tuition'][$curDate] = is_numeric($stData['tuition'][$curDate]) ? $stData['tuition'][$curDate] + 1 : 1;
                break;
            case 'event' :
                $stData['event'][$curDate] = is_numeric($stData['event'][$curDate]) ? $stData['event'][$curDate] + 1 : 1;
                break;

            case 'schedule' :
                $stData['schedule'][$curDate] = is_numeric($stData['schedule'][$curDate]) ? $stData['schedule'][$curDate] + 1 : 1;
                break;
            case 'menu' :
                $stData['menu'][$curDate] = is_numeric($stData['menu'][$curDate]) ? $stData['menu'][$curDate] + 1 : 1;
                break;

            case 'report' :
                $stData['report'][$curDate] = is_numeric($stData['report'][$curDate]) ? $stData['report'][$curDate] + 1 : 1;
                break;
            case 'medicine' :
                $stData['medicine'][$curDate] = is_numeric($stData['medicine'][$curDate]) ? $stData['medicine'][$curDate] + 1 : 1;
                break;

            case 'feedback' :
                $stData['feedback'][$curDate] = is_numeric($stData['feedback'][$curDate]) ? $stData['feedback'][$curDate] + 1 : 1;
                break;
            case 'birthday' :
                $stData['birthday'][$curDate] = is_numeric($stData['birthday'][$curDate]) ? $stData['birthday'][$curDate] + 1 : 1;
                break;

        }
        $memcache->setData(NOGA_STATISTIC_SCHOOL . $schoolId, $stData);
    }
    ${NOGA_STATISTIC_SCHOOL . $schoolId} = $stData;
}

/**
 * Tăng số lượng tương tác của trường (TaiLA - làm lại)
 *
 * @param $schoolId
 */
function addInteractive($schoolId, $module, $object_view, $object_id = 0, $is_created = 0)
{
    global ${STATISTIC_INTERACTIVE . $schoolId}, $statistic_keys;
    $stData = ${STATISTIC_INTERACTIVE . $schoolId};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    $memcache = new CacheMemcache();

    $curDate = date('Y-m-d');
    $result = null;
    if ($memcache->bEnabled) { // if Memcache enabled

        if (is_null($stData)) {
            $stData = $memcache->getData(STATISTIC_INTERACTIVE . $schoolId);
        }
        $keys = is_null($stData) ? array() : array_keys($stData);

        // Nếu getAll mà memcache không đủ dữ liệu thì lấy ra từ database
        if (!array_equal($keys, $statistic_keys)) {
            $stData = updateStatisticInteractive($schoolId);
        }

        $stData[$module][$curDate][$object_id][$object_view] = is_numeric($stData[$module][$curDate][$object_id][$object_view]) ? $stData[$module][$curDate][$object_id][$object_view] + 1 : 1;
        if ($is_created > 0) {
            $stData[$module][$curDate][$object_id]['is_created'] = $is_created;
        }
//        switch ($module) {
//            case 'page':
//                $stData['page'][$curDate][$object_id][$object_view] = is_numeric($stData['page'][$curDate][$object_id][$object_view]) ? $stData['page'][$curDate][$object_view] + 1 : 1;
//                break;
//            case 'school_dashboard':
//                $stData['school_dashboard'][$curDate] = is_numeric($stData['school_dashboard'][$curDate]) ? $stData['school_dashboard'][$curDate] + 1 : 1;
//                break;
//            case 'attendance':
//                $stData['attendance'][$curDate][$object_id][$object_view] = is_numeric($stData['attendance'][$curDate][$object_id][$object_view]) ? $stData['attendance'][$curDate][$object_id][$object_view] + 1 : 1;
//                if($is_created > 0) {
//                    $stData['attendance'][$curDate][$object_id]['is_created'] = $is_created;
//                }
//                break;
//
//            case 'service' :
//                $stData['service'][$curDate][$object_id][$object_view] = is_numeric($stData['service'][$curDate][$object_view]) ? $stData['service'][$curDate][$object_view] + 1 : 1;
//                if($is_created > 0) {
//                    $stData['service'][$curDate][$object_id]['is_created'] = $is_created;
//                }
//                break;
//            case 'pickup' :
//                $stData['pickup'][$curDate][$object_id][$object_view] = is_numeric($stData['pickup'][$curDate][$object_view]) ? $stData['pickup'][$curDate][$object_view] + 1 : 1;
//                if($is_created > 0) {
//                    $stData['service'][$curDate][$object_id]['is_created'] = $is_created;
//                }
//                break;
//
//            case 'tuition' :
//                $stData['tuition'][$curDate][$object_id][$object_view] = is_numeric($stData['tuition'][$curDate][$object_view]) ? $stData['pickup'][$curDate][$object_view] + 1 : 1;
//                if($is_created > 0) {
//                    $stData['service'][$curDate][$object_id]['is_created'] = $is_created;
//                }
//                break;
//            case 'event' :
//                $stData['event'][$curDate] = is_numeric($stData['event'][$curDate]) ? $stData['event'][$curDate] + 1 : 1;
//                break;
//
//            case 'schedule' :
//                $stData['schedule'][$curDate][$object_id]['school_view'] = is_numeric($stData['schedule'][$curDate][$object_id]['school_view']) ? $stData['schedule'][$curDate][$object_id]['school_view'] + 1 : 1;
//                break;
//            case 'menu' :
//                $stData['menu'][$curDate] = is_numeric($stData['menu'][$curDate]) ? $stData['menu'][$curDate] + 1 : 1;
//                break;
//
//            case 'report' :
//                $stData['report'][$curDate] = is_numeric($stData['report'][$curDate]) ? $stData['report'][$curDate] + 1 : 1;
//                break;
//            case 'medicine' :
//                $stData['medicine'][$curDate] = is_numeric($stData['medicine'][$curDate]) ? $stData['medicine'][$curDate] + 1 : 1;
//                break;
//
//            case 'feedback' :
//                $stData['feedback'][$curDate] = is_numeric($stData['feedback'][$curDate]) ? $stData['feedback'][$curDate] + 1 : 1;
//                break;
//            case 'birthday' :
//                $stData['birthday'][$curDate] = is_numeric($stData['birthday'][$curDate]) ? $stData['birthday'][$curDate] + 1 : 1;
//                break;
//
//        }
        $memcache->setData(STATISTIC_INTERACTIVE . $schoolId, $stData);
    }
    ${STATISTIC_INTERACTIVE . $schoolId} = $stData;
}

/* ---------- END - Thống kê ---------- */

/* ---------- BEGIN - Thống kê lượt tương tác người dùng với group, topic ---------- */

function getTopicStatisticsForNoga()
{
    global ${NOGA_STATISTIC_TOPIC}, $noga_statistic_topic_keys;
    $stData = ${NOGA_STATISTIC_TOPIC};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    $memcache = new CacheMemcache();

    $result = array(
        GROUP_NAME_CONIU => [],
        GROUP_NAME_ANDAM => [],
        GROUP_NAME_DINHDUONG => [],
        GROUP_NAME_DAYCON => [],
        GROUP_NAME_THAIGIAO => [],
        GROUP_NAME_HIEMMUON => [],
        GROUP_NAME_HONNHAN => [],
        GROUP_NAME_THUCPHAMORGANIC => []
    );

    if ($memcache->bEnabled) { // if Memcache enabled
        if (is_null($stData)) {
            $stData = $memcache->getData(NOGA_STATISTIC_TOPIC);
        }

        $keys = is_null($stData) ? array() : array_keys($stData);
        // Nếu getAll mà memcache không đủ dữ liệu thì lấy ra từ database
        if (!array_equal($keys, $noga_statistic_topic_keys)) {
            $stData = updateTopicStatisticsForNoga();
        }
        $result = $stData;
    }

    return $result;
}

/**
 * Cập nhật số lượng tương tác
 *
 * @param $schoolId
 */
function updateTopicStatisticsForNoga()
{
    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    include_once(DAO_PATH . 'dao_statistic.php');
    $memcache = new CacheMemcache();
    $statisticDao = new StatisticDAO();

    if ($memcache->bEnabled) { // if Memcache enabled
        $result = $statisticDao->getTopicStatisticsDetailForNoga();
        $memcache->setData(NOGA_STATISTIC_TOPIC, $result);
        return $result;
    }
    return null;
}

/**
 * Tăng số lượng tương tác
 *
 * @param $schoolId
 */
function increaseTopicInteractive($group_name)
{
    global ${NOGA_STATISTIC_TOPIC}, $noga_statistic_topic_keys;
    $stData = ${NOGA_STATISTIC_TOPIC};

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    $memcache = new CacheMemcache();

    $curDate = date('Y-m-d');
    $result = null;
    if ($memcache->bEnabled) { // if Memcache enabled

        if (is_null($stData)) {
            $stData = $memcache->getData(NOGA_STATISTIC_TOPIC);
        }
        $keys = is_null($stData) ? array() : array_keys($stData);

        // Nếu getAll mà memcache không đủ dữ liệu thì lấy ra từ database
        if (!array_equal($keys, $noga_statistic_topic_keys)) {
            $stData = updateTopicStatisticsForNoga();
        }

        switch ($group_name) {
            case GROUP_NAME_CONIU :
                $stData[GROUP_NAME_CONIU][$curDate] = is_numeric($stData[GROUP_NAME_CONIU][$curDate]) ? $stData[GROUP_NAME_CONIU][$curDate] + 1 : 1;
                break;

            case GROUP_NAME_ANDAM :
                $stData[GROUP_NAME_ANDAM][$curDate] = is_numeric($stData[GROUP_NAME_ANDAM][$curDate]) ? $stData[GROUP_NAME_ANDAM][$curDate] + 1 : 1;
                break;

            case GROUP_NAME_DINHDUONG :
                $stData[GROUP_NAME_DINHDUONG][$curDate] = is_numeric($stData[GROUP_NAME_DINHDUONG][$curDate]) ? $stData[GROUP_NAME_DINHDUONG][$curDate] + 1 : 1;
                break;
            case GROUP_NAME_DAYCON :
                $stData[GROUP_NAME_DAYCON][$curDate] = is_numeric($stData[GROUP_NAME_DAYCON][$curDate]) ? $stData[GROUP_NAME_DAYCON][$curDate] + 1 : 1;
                break;

            case GROUP_NAME_THAIGIAO :
                $stData[GROUP_NAME_THAIGIAO][$curDate] = is_numeric($stData[GROUP_NAME_THAIGIAO][$curDate]) ? $stData[GROUP_NAME_THAIGIAO][$curDate] + 1 : 1;
                break;
            case GROUP_NAME_HIEMMUON :
                $stData[GROUP_NAME_HIEMMUON][$curDate] = is_numeric($stData[GROUP_NAME_HIEMMUON][$curDate]) ? $stData[GROUP_NAME_HIEMMUON][$curDate] + 1 : 1;
                break;

            case GROUP_NAME_HONNHAN :
                $stData[GROUP_NAME_HONNHAN][$curDate] = is_numeric($stData[GROUP_NAME_HONNHAN][$curDate]) ? $stData[GROUP_NAME_HONNHAN][$curDate] + 1 : 1;
                break;

            case GROUP_NAME_THUCPHAMORGANIC :
                $stData[GROUP_NAME_THUCPHAMORGANIC][$curDate] = is_numeric($stData[GROUP_NAME_THUCPHAMORGANIC][$curDate]) ? $stData[GROUP_NAME_THUCPHAMORGANIC][$curDate] + 1 : 1;
                break;

        }
        $memcache->setData(NOGA_STATISTIC_TOPIC, $stData);
    }
    ${NOGA_STATISTIC_TOPIC} = $stData;
}

/* ---------- END - Thống kê ---------- */

/* ---------- BEGIN - Lấy ra danh sách trẻ nghỉ nhiều liên tục  ---------- */
function getConsecutiveAbsentChild($schoolId)
{
    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    include_once(DAO_PATH . 'dao_attendance.php');
    $memcache = new CacheMemcache();
    $attendanceDao = new AttendanceDAO();

    $data = array();
    if ($memcache->bEnabled) { // if Memcache enabled
        $data = $memcache->getData(SCHOOL_CONSECUTIVE_ABSENT . $schoolId);
    }

    if (empty($data)) {
        $daynum = 3;
        $children = $attendanceDao->getConsecutiveAbsentChild($schoolId, $daynum);

        $tmpArray = array();
        while ((count($children) > 0) && (MAX_CONSECUTIVE_ABSENT_DAY >= $daynum)) {
            $tmpArray[$daynum] = $children;
            $daynum++;
            if (MAX_CONSECUTIVE_ABSENT_DAY >= $daynum) {
                $children = $attendanceDao->getConsecutiveAbsentChild($schoolId, $daynum);
            }
        }

        $newArrays = array();
        while ($daynum > 3) {
            $daynum--;
            foreach ($tmpArray[$daynum] as $childOut) {
                $found = false;
                foreach ($newArrays as $childIn) {
                    if ($childOut['child_id'] == $childIn['child_id']) {
                        $found = true;
                        break;
                    }
                }
                if (!$found) {
                    $newChild = $childOut;
                    $newChild['number_of_absent_day'] = $daynum;
                    $newArrays[] = $newChild;
                }
            }
        }
        // lặp danh sách trẻ nghỉ nhiều liên tục lấy lý do nghỉ
        // Lấy ngày điểm danh cuối cùng của trường
        $dates = $attendanceDao->getLastAttendanceDates($schoolId, $daynum);
        $toDate = $dates[0];
        $endDate = toSysDate($toDate);

        $data['list'] = array();
        // Khai báo mảng mới
        foreach ($newArrays as $row) {
            $beginDate = date('Y-m-d', strtotime('- ' . ($row['number_of_absent_day'] + 2) . ' days', strtotime($toDate)));
            $beginDate = toSysDate($beginDate);
            $attendances = $attendanceDao->getAttendanceChild($row['child_id'], $beginDate, $endDate);
            $reason = '';

            // Lặp mảng thông tin điểm danh của trẻ lấy lý do nghỉ (lấy lần cuối cùng có lý do)
            foreach ($attendances['attendance'] as $value) {
                if ($value['reason'] != '') {
                    $reason = $value['reason'];
                    break;
                }
            }
            $row['reason'] = $reason;
            $data['list'][] = $row;
        }

        $data['updated_at'] = date('H:i d/m/Y');
        $memcache->setData(SCHOOL_CONSECUTIVE_ABSENT . $schoolId, $data);

    }
    return $data;
}

/* ---------- END - Trẻ nghỉ nhiều liên tục ---------- */

/* So sánh 2 mảng có value bằng nhau hay không */
function array_equal($a, $b)
{
    return (is_array($a) && is_array($b) && array_diff($a, $b) === array_diff($b, $a));
}

/* Sắp xếp mảng theo giá trị và kiểu sắp xếp */
function sortArray($data, $type = "ASC", $field)
{
    usort($data, function ($a, $b) use ($type, $field) {
        $retval = 0;
        if ($retval == 0) {
            if ($type == "ASC") {
                $retval = strnatcmp($a[$field], $b[$field]);
            } elseif ($type == "DESC") {
                $retval = strnatcmp($b[$field], $a[$field]);
            }

        }

        return $retval;
    });
    return $data;
}

/**
 * Sắp xếp mảng theo nhiều field. Mỗi field theo mỗi cách sắp xếp khác nhau.
 *
 * @param $data
 * @param $fields [['fieldname1','order1'],['fieldname2','order2'],...]
 * @return mixed
 */
function sortMultiOrderArray($data, $fields)
{
    usort($data, function ($a, $b) use ($fields) {
        $retval = 0;
        foreach ($fields as $field) {
            if ($retval == 0) {
                if ($field[1] == "ASC") {
                    $retval = strnatcmp($a[$field[0]], $b[$field[0]]);
                } elseif ($field[1] == "DESC") {
                    $retval = strnatcmp($b[$field[0]], $a[$field[0]]);
                }

            }
        }
        return $retval;
    });
    return $data;
}


//Lấy ra 1 array mới theo các key truyền vào
function getArrayFromKeys($data, $fields)
{
    $result = array();
    foreach ($fields as $field) {
        $result[$field] = $data[$field];
    }
    return $result;
}


/*  ---------- END - Memcached  ---------- */


/* ---------- BACKGROUND - Firebase ---------- */
function insertBackgroundFirebase(array $args = array())
{
    global $system;

    if ($system['sync_firebase']) {
        include_once(DAO_PATH . 'dao_firebase.php');
        $firebaseDao = new FirebaseDAO();
        $firebaseDao->insertFirebase($args);
    }
}

/* ---------- END - Firebase ---------- */


/**
 * Lấy danh sách phụ huynh của thai nhi nhận được thông báo
 *
 * @param $day
 * @return array
 */
function getChildIdsPushNotify($day)
{
    include_once(DAO_PATH . 'dao_foetus_development.php');
    $foetusDevelopmentDao = new FoetusDevelopmentDAO();

    // Lấy danh sách thai nhi có ngày bằng $day
    $childParentIds = $foetusDevelopmentDao->getChildIdsOfDay($day);

    return $childParentIds;
}

/**
 * Lấy danh sách phụ huynh của trẻ nhận được thông báo
 *
 * @param $day
 * @return array
 */
function getChildIdsPushNotifyChildDevelopment($day)
{
    include_once(DAO_PATH . 'dao_child_development.php');
    $childDevelopmentDao = new ChildDevelopmentDAO();

    // Lấy danh sách trẻ có tháng = $month, ngày = $day
    $childParentIds = $childDevelopmentDao->getChildIdsOfDay($day);

    return $childParentIds;
}

/**
 * Chuyển dữ liệu chuẩn từ dạng json sang dạng mảng
 *
 * @param $json
 * @return mixed|null|string|string[]
 */
function convertDataGrowthStandard($json)
{
    $b = str_replace(' ', '', $json);
    $b = preg_replace("/\r|\n/", "", $b);
    $b = str_replace("=", ":", $b);
    $b = str_replace(";}", "}", $b);
    //$b = str_replace("{", "[", $b);
    //$b = str_replace("}", "]", $b);
    $b = str_replace("highest", '"highest"', $b);
    $b = str_replace("lowest", '"lowest"', $b);
    $b = str_replace("month", '"month"', $b);
    $b = str_replace("standard", '"standard"', $b);

    //$b =  explode("," , $b);
    $b = str_replace(";", ",", $b);
    $b = json_decode($b, true);
    return $b;
}

/**
 * Chuyển dữ liệu chuẩn thai nhi từ dạng json sang dạng mảng
 *
 * @param $json
 * @return mixed|null|string|string[]
 */
function convertDataFoetusGrowthStandard($json)
{
    $b = str_replace(' ', '', $json);
    $b = preg_replace("/\r|\n/", "", $b);
    $b = str_replace("=", ":", $b);
    $b = str_replace(";}", "}", $b);
    //$b = str_replace("{", "[", $b);
    //$b = str_replace("}", "]", $b);
    $b = str_replace("height", '"height"', $b);
    $b = str_replace("weight", '"weight"', $b);
    $b = str_replace("week", '"week"', $b);

    //$b =  explode("," , $b);
    $b = str_replace(";", ",", $b);
    $b = json_decode($b, true);
    return $b;
}

/**
 * Set dữ liệu chuẩn vẽ biểu đồ trẻ (từ tháng -> ngày)
 *
 * @param $json
 * @return array
 */
function getHightDataForChart($json)
{
    $b = str_replace(' ', '', $json);
    $b = preg_replace("/\r|\n/", "", $b);
    $b = str_replace("=", ":", $b);
    $b = str_replace(";}", "}", $b);
    //$b = str_replace("{", "[", $b);
    //$b = str_replace("}", "]", $b);
    $b = str_replace("highest", '"highest"', $b);
    $b = str_replace("lowest", '"lowest"', $b);
    $b = str_replace("month", '"month"', $b);
    $b = str_replace("standard", '"standard"', $b);

    //$b =  explode("," , $b);
    $b = str_replace(";", ",", $b);
    $b = json_decode($b, true);

    // lặp từ 1 đến 1826 (Số ngày khi trẻ tròn 5 tuổi)
    $hightStandardDay = array();
    $k = 0;
    for ($i = 1; $i <= DAY_AGE_MAX_FOR_CHART; $i++) {
        if ($i % 30 == 1) {
            $temp = array();
            $temp = $b[$k];
            $temp['day'] = $i;
            $k++;
        } else {
            $temp = array();
            $temp['highest'] = 'null';
            $temp['lowest'] = 'null';
            $temp['month'] = $k - 1;
            $temp['day'] = $i;
        }
        $hightStandardDay[] = $temp;
    }
    return $hightStandardDay;
}

/**
 * Hàm chuyển dữ liệu chuẩn thai nhi trẻ thành dữ liệu vẽ biểu đồ (từ tuần -> ngày)
 *
 * @param $json
 * @return array
 */
function getFoetusDataForChart($json)
{
    $b = str_replace(' ', '', $json);
    $b = preg_replace("/\r|\n/", "", $b);
    $b = str_replace("=", ":", $b);
    $b = str_replace(";}", "}", $b);
    //$b = str_replace("{", "[", $b);
    //$b = str_replace("}", "]", $b);
    $b = str_replace("height", '"height"', $b);
    $b = str_replace("weight", '"weight"', $b);
    $b = str_replace("week", '"week"', $b);

    //$b =  explode("," , $b);
    $b = str_replace(";", ",", $b);
    $b = json_decode($b, true);

    // lặp từ 50 đến 273 (Số ngày khi thai nhi 8 tuần đến 40 tuần tuổi)
    $foetusStandardDay = array();
    $k = 0;
    for ($i = 50; $i <= 274; $i++) {
        $week = $i / 7;
        if ($i % 7 == 0) {
            $week = (int)$week;
        } else {
            $week = (int)$week + 1;
        }
        if ($i % 7 == 1) {
            $temp = array();
            $temp = $b[$k];
            $temp['day'] = $i;
            $k++;
        } else {
            $temp = array();
            $temp['height'] = 'null';
            $temp['weight'] = 'null';
            $temp['week'] = $week;
            $temp['day'] = $i;
        }
        $foetusStandardDay[] = $temp;
    }
    return $foetusStandardDay;
}

/**
 * Hàm chuyển dữ liệu người dùng nhập vào thành dữ liệu vẽ biểu đồ (trẻ)
 *
 * @param $childGrowth
 * @param $begin_day
 * @param $end_day
 * @param $birthday
 * @param $type
 * @return string
 */
function getDataJsonForChart($childGrowth, $begin_day, $end_day, $birthday, $type)
{
    // Khởi tạo mảng lưu growth của trẻ (lưu thêm day_age)
    $temps = array();

    // Mảng lưu day_age;
    $day_age_arr = array();
    foreach ($childGrowth as $row) {
        // THêm vào bản ghi ngày thứ bao nhiêu
        $day_age = (strtotime(toDBDate($row['recorded_at'])) - strtotime($birthday)) / (60 * 60 * 24);
        $day_age = (int)$day_age;
        if ($day_age > 0) {
            $row['day_age'] = $day_age;
            $temps[] = $row;
            $day_age_arr[] = $day_age;
        }
    }

    // Dữ liệu người dùng nhập vào
    $growths = array();
    if (count($temps) > 0) {
        for ($i = $begin_day; $i <= $end_day; $i++) {
            $growth = array();
            if (in_array($i, $day_age_arr)) {
                foreach ($temps as $temp) {
                    if ($i == $temp['day_age']) {
                        $growth['height'] = $temp['height'];
                        $growth['weight'] = $temp['weight'];
                        $growth['bmi'] = $temp['bmi'];
                    }
                }
            } else {
                $growth['height'] = 'null';
                $growth['weight'] = 'null';
                $growth['bmi'] = 'null';
            }
            $growths[] = $growth;
        }
    }

    // Chuyển sữ liệu người dùng nhập vào theo dạng data vẽ biểu đồ
    $heightForChart = '';
    $i = $begin_day;
    foreach ($growths as $row) {
        $heightForChart .= "{x:" . $i . ", y:" . $row[$type] . "},";
        $i++;
    }

    return $heightForChart;
}

/**
 * Hàm chuyển dữ liệu người dùng nhập vào thành dữ liệu vẽ biểu đồ (trẻ - thư viện  charts.js)
 *
 * @param $childGrowth
 * @param $begin_day
 * @param $end_day
 * @param $birthday
 * @param $type
 * @return string
 */
function getDataJsonForChartChartJS($childGrowth, $begin_day, $end_day, $birthday, $type)
{
    // Khởi tạo mảng lưu growth của trẻ (lưu thêm day_age)
    $temps = array();

    // Mảng lưu day_age;
    $day_age_arr = array();
    foreach ($childGrowth as $row) {
        // THêm vào bản ghi ngày thứ bao nhiêu
        $day_age = (strtotime(toDBDate($row['recorded_at'])) - strtotime($birthday)) / (60 * 60 * 24);
        $day_age = (int)$day_age;
        if ($day_age > 0) {
            $row['day_age'] = $day_age;
            $temps[] = $row;
            $day_age_arr[] = $day_age;
        }
    }

    // Dữ liệu người dùng nhập vào
    $growths = array();
    if (count($temps) > 0) {
        for ($i = $begin_day; $i <= $end_day; $i++) {
            $growth = array();
            if (in_array($i, $day_age_arr)) {
                foreach ($temps as $temp) {
                    if ($i == $temp['day_age']) {
                        $growth['height'] = $temp['height'];
                        $growth['weight'] = $temp['weight'];
                        $growth['bmi'] = $temp['bmi'];
                    }
                }
            } else {
                $growth['height'] = 'null';
                $growth['weight'] = 'null';
                $growth['bmi'] = 'null';
            }
            $growths[] = $growth;
        }
    }

    // Chuyển sữ liệu người dùng nhập vào theo dạng data vẽ biểu đồ
    $heightForChart = '';
    foreach ($growths as $row) {
        if ($row[$type] != 'null') {
            $heightForChart .= ' ' . $row[$type] . ",";
        } else {
            $heightForChart .= ' ,';
        }
    }

    return $heightForChart;
}

/**
 * Hàm chuyển dữ liệu người dùng nhập vào thành dữ liệu vẽ biểu đồ (thai nhi)
 *
 * @param $foetusGrowth
 * @param $foetusBeginDate
 * @param $type
 * @return string
 */
function getFoetusDataJsonForChart($foetusGrowth, $foetusBeginDate, $type)
{
    // Khởi tạo mảng lưu growth của thai nhi (lưu thêm day_age)
    $temps = array();

    // Mảng lưu day_age;
    $day_age_arr = array();
    foreach ($foetusGrowth as $row) {
        // THêm vào bản ghi ngày thứ bao nhiêu
        $day_age = (strtotime(toDBDate($row['recorded_at'])) - strtotime($foetusBeginDate)) / (60 * 60 * 24);
        $day_age = (int)$day_age;
        if ($day_age > 0) {
            $row['day_age'] = $day_age;
            $temps[] = $row;
            $day_age_arr[] = $day_age;
        }
    }

    // Dữ liệu người dùng nhập vào
    $growths = array();
    if (count($temps) > 0) {
        for ($i = 50; $i <= 274; $i++) {
            $growth = array();
            if (in_array($i, $day_age_arr)) {
                foreach ($temps as $temp) {
                    if ($i == $temp['day_age']) {
                        $growth['height'] = $temp['from_head_to_hips_length'];
                        $growth['weight'] = $temp['weight'];
                    }
                }
            } else {
                $growth['height'] = 'null';
                $growth['weight'] = 'null';
            }
            $growths[] = $growth;
        }
    }

    // Chuyển sữ liệu người dùng nhập vào theo dạng data vẽ biểu đồ
    $heightForChart = '';
    $i = 50;
    foreach ($growths as $row) {
        $heightForChart .= "{x:" . $i . ", y:" . $row[$type] . "},";
        $i++;
    }

    return $heightForChart;
}

/**
 * Hàm chuyển dữ liệu người dùng nhập vào thành dữ liệu vẽ biểu đồ (thai nhi) for charts.js
 *
 * @param $foetusGrowth
 * @param $foetusBeginDate
 * @param $type
 * @return string
 */
function getFoetusDataJsonForChartChartJS($foetusGrowth, $foetusBeginDate, $type)
{
    // Khởi tạo mảng lưu growth của thai nhi (lưu thêm day_age)
    $temps = array();

    // Mảng lưu day_age;
    $day_age_arr = array();
    foreach ($foetusGrowth as $row) {
        // THêm vào bản ghi ngày thứ bao nhiêu
        $day_age = (strtotime(toDBDate($row['recorded_at'])) - strtotime($foetusBeginDate)) / (60 * 60 * 24);
        $day_age = (int)$day_age;
        if ($day_age > 0) {
            $row['day_age'] = $day_age;
            $temps[] = $row;
            $day_age_arr[] = $day_age;
        }
    }

    // Dữ liệu người dùng nhập vào
    $growths = array();
    if (count($temps) > 0) {
        for ($i = 50; $i <= 274; $i++) {
            $growth = array();
            if (in_array($i, $day_age_arr)) {
                foreach ($temps as $temp) {
                    if ($i == $temp['day_age']) {
                        $growth['height'] = $temp['from_head_to_hips_length'];
                        $growth['weight'] = $temp['weight'];
                    }
                }
            } else {
                $growth['height'] = 'null';
                $growth['weight'] = 'null';
            }
            $growths[] = $growth;
        }
    }

    // Chuyển sữ liệu người dùng nhập vào theo dạng data vẽ biểu đồ
    $heightForChart = '';
    foreach ($growths as $row) {
        if ($row[$type] != 'null') {
            $heightForChart .= ' ' . $row[$type] . ",";
        } else {
            $heightForChart .= ' ,';
        }
    }

    return $heightForChart;
}

/**
 * Hàm lấy dữ liệu chuẩn thành dữ liệu trả về trong biểu đồ (json) (trẻ)
 *
 * @param $dataArray
 * @param $begin_day
 * @param $end_day
 * @param $type
 */
function getStandardDataForChart($dataArray, $begin_day, $end_day, $type)
{
    $standardForChart = '';
    $i = $begin_day;
    foreach ($dataArray as $row) {
        if ($row['day'] >= $begin_day && $row['day'] <= $end_day) {
            $standardForChart .= "{x:" . $i . ", y:" . $row[$type] . "},";
            $i++;
        }
    }

    return $standardForChart;
}

/**
 * Lấy dữ liệu chuẩn cho thư viện charts.js
 *
 * @param $dataArray
 * @param $begin_day
 * @param $end_day
 * @param $type
 * @return string
 */
function getStandardDataForChartChartJS($dataArray, $begin_day, $end_day, $type)
{
    $standardForChart = '';
    foreach ($dataArray as $k => $row) {
        if ($row['day'] >= $begin_day + 1 && $row['day'] <= $end_day + 1) {
            if (($k == $begin_day) || ($k % 30 == 0) || ($k == $end_day)) {
                $standardForChart .= ' ' . $row[$type] . ',';
            } else {
                $standardForChart .= ' ,';
            }
        }
    }
    return $standardForChart;
}


/**
 * Hàm chuyển dữ liệu vẽ biểu đồ dạng json (thai nhi)
 *
 * @param $dataArray
 * @param $type
 * @return string
 */
function getFoetusStandardDataForChart($dataArray, $type)
{
    $standardForChart = '';
    $i = 50;
    foreach ($dataArray as $row) {
        if ($row['day'] >= 50 && $row['day'] <= 274) {
            $standardForChart .= "{x:" . $i . ", y:" . $row[$type] . "},";
            $i++;
        }
    }

    return $standardForChart;
}

/**
 * Hàm chuyển dữ liệu vẽ biểu đồ dạng json (thai nhi) for charts.js
 *
 * @param $dataArray
 * @param $type
 * @return string
 */
function getFoetusStandardDataForChartChartJS($dataArray, $type)
{
    $standardForChart = '';
    foreach ($dataArray as $row) {
        if ($row['day'] >= 50 && $row['day'] <= 274) {
            if ($row[$type] != 'null') {
                $standardForChart .= ' ' . $row[$type] . ",";
            } else {
                $standardForChart .= ' ,';
            }
        }
    }

    return $standardForChart;
}

/**
 * Hàm lấy điểm chuẩn bắt đầu và chuẩn kết thúc
 *
 * @param $dataArray
 * @param $begin_day
 * @param $end_day
 */
function getStandardDataBeginAndEnd($dataArray, $day, $type)
{
    // Lấy 2 ngày gần nhất của $begin_day có dữ liệu, nội suy chuẩn của ngày bắt đầu
    $dataArray[$day - 1][$type] = 'null';
    if ($day < 1826) {
        $monthBetween = $day / 30;
        $dayPrev = (int)$monthBetween * 30;
        $dayNext = ((int)$monthBetween + 1) * 30;
        $dayBetween = $day - $dayPrev;
        if ($dataArray[$dayNext][$type] != null) {
            $dataArray[$day - 1][$type] = $dataArray[$dayPrev][$type] + (($dataArray[$dayNext][$type] - $dataArray[$dayPrev][$type]) * $dayBetween / 30);

            $dataArray[$day - 1][$type] = number_format($dataArray[$day - 1][$type], 2, '.', '');
        }
    }

    return $dataArray[$day - 1][$type];
}

function getUserEmailOrPhoneNotValid($begin)
{
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    // include SMTP Email Validation Class
    require_once(ABSPATH . 'includes/libs/SMTPValidateEmail/VerifyEmail.php');

    // include userDao
    include_once(DAO_PATH . 'dao_user.php');
    $userDao = new UserDAO();

    // instantiate the class
    //$SMTP_Validator = new SMTP_validateEmail();

    $limit = 100;
    // Lấy danh sách toàn bộ user của hệ thống
    $users = $userDao->getAllUser($begin, $limit);


    $results = array();
    // Lặp danh sách user
    foreach ($users as $k => $user) {
        $ve = new hbattat\VerifyEmail($user['user_email'], 'hoaddconiu@gmail.com');
        if (!$ve->verify() || $user['user_phone'] == '') {
            $results[] = $user;
        }
    }

    return $results;
}

function getUserEmailNotValidNotBegin()
{
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    // include SMTP Email Validation Class
    require_once(ABSPATH . 'includes/libs/SMTPValidateEmail/VerifyEmail.php');

    // include userDao
    include_once(DAO_PATH . 'dao_user.php');
    $userDao = new UserDAO();

    // instantiate the class
    //$SMTP_Validator = new SMTP_validateEmail();

    $limit = 100;
    // Lấy danh sách toàn bộ user của hệ thống
    //$users = $userDao->getAllUser($begin, $limit);
    $users = $userDao->getAllUsers();


    $results = array();
    // Lặp danh sách user
    foreach ($users as $k => $user) {
        $ve = new hbattat\VerifyEmail($user['user_email'], 'hoaddconiu@gmail.com');
        if (!$ve->verify()) {
            $results[] = $user;
        }
    }

    return $results;
}

/**
 * Tính số ngày đi làm (số ngày điểm danh) trong một tháng.
 *
 * @param $month có kiểu là "MM/YYYY"
 * @return int
 */
function workingDayInMonth($month)
{
    $firstDateOfMonth = toDBDate("01/" . $month);
    $firstDateOfNextMonth = addMonthToDBDate($firstDateOfMonth);

    $fromDate = strtotime($firstDateOfMonth);
    $toDate = strtotime($firstDateOfNextMonth);
    $count = 0;
    while ($fromDate < $toDate) {
        $weekDay = date('w', $fromDate);
        if (($weekDay != 0) && ($weekDay != 6)) {
            $count++;
        }
        $fromDate = strtotime("+1 day", $fromDate);
    }
    return $count;
}

/**
 * Thêm style max-width: 100% cho thẻ IMG
 *
 * @param $text
 * @return mixed
 */
function addMaxWidthForImgTag($text)
{
    $text = str_replace('&lt;img ', '&lt;img style=&quot;max-width: 100%;&quot; ', $text);
//    $text = str_replace("''", '""', $text);
    return $text;
}

/**
 * Check xem user có phải giáo viên hoặc quản lý trường không
 *
 * @param $userId
 * @return bool
 */
function checkManagerOrTeacher($userId)
{
    $classes = getUserManageData($userId, USER_MANAGE_CLASSES);
    $schools = getUserManageData($userId, USER_MANAGE_SCHOOL);
    if (count($classes) > 0 || count($schools) > 0) {
        return true;
    }
    return false;
}

/**
 * Cập nhật user_online từ memcache vào db
 *
 * @return array|null
 */
function updateUserOnlineFromDB()
{
    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    include_once(DAO_PATH . 'dao_statistic.php');
    $memcache = new CacheMemcache();
    $statisticDao = new StatisticDAO();

    if ($memcache->bEnabled) { // if Memcache enabled
        $result = $statisticDao->getUserOnline();
        $memcache->setData(USERS_ONLINE, $result);
        return $result;
    }
    return null;
}

/**
 * Cập nhật user_online vào memcache
 *
 * @param $userId
 * @return array|null
 */
function updateUserOnline($userId)
{
    global ${USERS_ONLINE};
    $sData = ${USERS_ONLINE};
    $today = date('Y-m-d');

    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    $memcache = new CacheMemcache();

    if ($memcache->bEnabled) { // if Memcache enabled
        if (is_null($sData)) {

            $tmp = $memcache->getData(USERS_ONLINE);
            $sData = is_null($tmp) ? array() : $tmp;
        }
        if (count($sData) == 0) {
            $sData = updateUserOnlineFromDB();
        }
        // Kiểm tra ngày hôm nay user đã online chưa
        $userIds = $sData[$today];
        if (count($userIds) > 0) {
            if (!in_array($userId, $userIds)) {
                $sData[$today][] = $userId;
            }
        } else {
            $sData[$today][] = $userId;
        }
        // Cập nhật user vào memcache
        $memcache->setData(USERS_ONLINE, $sData);
    }
    return $sData;
}

function getUserOnline($fromDate = null, $toDate = null)
{
    global ${USERS_ONLINE};
    $sData = ${USERS_ONLINE};

    include_once(DAO_PATH . 'dao_user.php');
    $userDao = new UserDAO();
    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    $memcache = new CacheMemcache();

    if (is_null($fromDate)) {
        $strFromDate = strtotime('Y-m-01');
        $strToDate = strtotime(date('Y-m-d'));
    } else {
        $strFromDate = strtotime(date('Y-m-d', strtotime(toDBDate($fromDate))));
        $strToDate = strtotime(date('Y-m-d', strtotime(toDBDate($toDate))));
    }

    if ($memcache->bEnabled) { // if Memcache enabled
        if (is_null($sData)) {

            $tmp = $memcache->getData(USERS_ONLINE);
            $sData = is_null($tmp) ? array() : $tmp;
        }
        if (count($sData) == 0) {
            $sData = updateUserOnlineFromDB();
        }
    }
    $result = array();
    foreach ($sData as $date => $data) {
        if (strtotime($date) >= $strFromDate && strtotime($date) <= $strToDate) {
            $result[$date] = $data;
        }
    }
    $resultLast = array();
    foreach ($result as $date => $data) {
        $users = $userDao->getUsersForUserOnline($data);
        $resultLast[$date] = $users;
    }
    return $resultLast;
}

/**
 * Lấy ra thứ theo ngày
 *
 * @param $date
 * @return integer
 * @throws Exception
 */
function getDayShortOfDateFun($strTodate)
{
    $day = date('w', $strTodate);
    switch ($day) {
        case '0':
            return 'CN';
        case '1':
            return 'H';
        case '2':
            return 'B';
        case '3':
            return 'T';
        case '4':
            return 'N';
        case '5':
            return 'S';
        case '6':
            return 'B';
        default:
            return 0;
    }
}


// ConIu - END
?>