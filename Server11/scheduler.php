<?php
/**
 * Hàm chạy các các nhiệm vụ ngầm theo lịch
 * 
 * @package ConIu v1
 * @author QuanND
 */

echo "test";

// set override_shutdown
$override_shutdown = true;

// fetch bootstrap
require('bootstrap.php');

include_once(DAO_PATH.'dao_school.php');
//include_once(DAO_PATH.'dao_teacher.php');
include_once(DAO_PATH.'dao_child.php');
include_once(DAO_PATH.'dao_user.php');
include_once(DAO_PATH.'dao_tuition.php');
include_once(DAO_PATH.'dao_parent.php');

$schoolDao = new SchoolDAO();
//$teacherDao = new TeacherDAO();
$childDao = new ChildDAO();
//$notificationDao = new NotificationDAO();
$userDao = new UserDAO();
$parentDao = new ParentDao();
$tuitionDao = new TuitionDAO();

//Lấy ra danh sách cấu hình của các trường
$configs = $schoolDao->getConfigurations();

$tuitionChilds = null;
foreach ($configs as $config) {
    if ($config['notification_tuition_interval'] > 0) {
        //Lấy ra danh sách trẻ nợ học phí đến ngày phải nhắc nhở.
        $tuitionChilds = $tuitionDao->getUnpaidSchoolTuitions($config['school_id'], $config['notification_tuition_interval']);
        foreach ($tuitionChilds as $tuitionChild) {
            //Lấy ra danh sách cha mẹ của trẻ
            //$parentIds = $parentDao->getParentIds($tuitionChild['child_id']);
            $parents = getChildData($tuitionChild['child_id'], CHILD_PARENTS);
            $parentIds = array_keys($parents);
            //Thông báo cha mẹ nộp tiền
            $userDao->postSystemNotifications($parentIds, NOTIFICATION_NEW_TUITION, NOTIFICATION_NODE_TYPE_CHILD, $tuitionChild['tuition_child_id'], $config['page_admin'], $tuitionChild['child_id']);
            //Cập nhật thời gian thông báo cuối cùng
            $tuitionDao->updateNotifyTime($tuitionChild['tuition_child_id']);
        }
    }
}

?>