#!/bin/sh

#! --------------------NOTIFICATION--------------------
#! Kiểm tra tiến trình gửi thông báo có đang chạy background không? Nếu ko thì relaunching
if ps ax | grep -v grep | grep send_notification.php
then
    echo "notification_script is running..."
else
    echo "notification_script not running, relaunching..."
    #/usr/bin/php /var/www/edu.mascom.com.vn/public_html/includes/ajax/ci/php_script/run_background/send_notification.php &
	nohup /usr/bin/php /var/www/edu.mascom.com.vn/public_html/includes/ajax/ci/php_script/run_background/send_notification.php >/dev/null 2>&1 &
fi

#! --------------------MAIL--------------------
#! Kiểm tra tiến trình gửi thông báo có đang chạy background không? Nếu ko thì relaunching
if ps ax | grep -v grep | grep send_mail.php
then
    echo "mail_script is running..."
else
    echo "mail_script not running, relaunching..."
    #/usr/bin/php /var/www/edu.mascom.com.vn/public_html/includes/ajax/ci/php_script/run_background/send_mail.php &
	nohup /usr/bin/php /var/www/edu.mascom.com.vn/public_html/includes/ajax/ci/php_script/run_background/send_mail.php >/dev/null 2>&1 &
fi


#! --------------------FIREBASE--------------------
#! Kiểm tra tiến trình gửi thông báo có đang chạy background không? Nếu ko thì relaunching
if ps ax | grep -v grep | grep firebase.php
then
    echo "firebase_script is running..."
else
    echo "firebase_script not running, relaunching..."
    #/usr/bin/php /var/www/edu.mascom.com.vn/public_html/includes/ajax/ci/php_script/run_background/firebase.php &
	nohup /usr/bin/php /var/www/edu.mascom.com.vn/public_html/includes/ajax/ci/php_script/run_background/firebase.php >/var/log/testlog.txt &
fi


